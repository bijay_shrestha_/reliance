/*
Navicat MySQL Data Transfer

Source Server         : phpmyamin
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-10-02 09:40:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_gypsum_chemical_record
-- ----------------------------
DROP TABLE IF EXISTS `tbl_gypsum_chemical_record`;
CREATE TABLE `tbl_gypsum_chemical_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chemical_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `gypsum_record_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_gypsum_chemical_record
-- ----------------------------
INSERT INTO `tbl_gypsum_chemical_record` VALUES ('1', '5', '1.5', '1');
INSERT INTO `tbl_gypsum_chemical_record` VALUES ('2', '4', '2.5', '1');
INSERT INTO `tbl_gypsum_chemical_record` VALUES ('3', null, null, '0');
INSERT INTO `tbl_gypsum_chemical_record` VALUES ('4', null, null, '0');
