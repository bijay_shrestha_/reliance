/*
Navicat MySQL Data Transfer

Source Server         : phpmyamin
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-10-02 09:40:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_master_chemicals
-- ----------------------------
DROP TABLE IF EXISTS `tbl_master_chemicals`;
CREATE TABLE `tbl_master_chemicals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_master_chemicals
-- ----------------------------
INSERT INTO `tbl_master_chemicals` VALUES ('4', 'L.O.I', '1', 'L.O.I');
INSERT INTO `tbl_master_chemicals` VALUES ('5', 'I.R.', '2', 'I.R.');
INSERT INTO `tbl_master_chemicals` VALUES ('6', 'Cao', '3', 'Cao');
INSERT INTO `tbl_master_chemicals` VALUES ('7', 'Mgo', '4', 'Mgo');
INSERT INTO `tbl_master_chemicals` VALUES ('8', 'Fe2o3', '5', 'Fe2o3');
INSERT INTO `tbl_master_chemicals` VALUES ('9', 'Al2o3', '6', 'Al2o3');
