/*
Navicat MySQL Data Transfer

Source Server         : phpmyamin
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-10-02 09:40:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_gypsum_record
-- ----------------------------
DROP TABLE IF EXISTS `tbl_gypsum_record`;
CREATE TABLE `tbl_gypsum_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_sample` date DEFAULT NULL,
  `date_of_testing` date DEFAULT NULL,
  `moisture` varchar(255) DEFAULT NULL,
  `purity` float(255,0) DEFAULT NULL,
  `source_of_supplier` int(11) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `remarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_gypsum_record
-- ----------------------------
INSERT INTO `tbl_gypsum_record` VALUES ('1', '2017-09-26', '2017-09-26', '1.2', '2', '3', '1', 'test');
