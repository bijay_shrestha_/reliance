/*
Navicat MySQL Data Transfer

Source Server         : phpmyamin
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-10-02 10:13:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_report_lists
-- ----------------------------
DROP TABLE IF EXISTS `tbl_report_lists`;
CREATE TABLE `tbl_report_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_report_lists
-- ----------------------------
INSERT INTO `tbl_report_lists` VALUES ('1', 'Gypsum Record');
INSERT INTO `tbl_report_lists` VALUES ('3', 'Clinker Record');
INSERT INTO `tbl_report_lists` VALUES ('4', 'Slag Record');
