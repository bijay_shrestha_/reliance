/*
Navicat MySQL Data Transfer

Source Server         : phpmyamin
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-10-02 09:40:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_supplier
-- ----------------------------
DROP TABLE IF EXISTS `tbl_supplier`;
CREATE TABLE `tbl_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_supplier
-- ----------------------------
INSERT INTO `tbl_supplier` VALUES ('1', 'Sanish Chemical Mart', '1');
INSERT INTO `tbl_supplier` VALUES ('2', 'Anish Chemical Shop', '2');
INSERT INTO `tbl_supplier` VALUES ('3', 'Bijay Chemical Shop', '3');
