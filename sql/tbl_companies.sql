/*
Navicat MySQL Data Transfer

Source Server         : phpmyamin
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : db_reliance

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-10-02 09:39:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_companies
-- ----------------------------
DROP TABLE IF EXISTS `tbl_companies`;
CREATE TABLE `tbl_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_companies
-- ----------------------------
INSERT INTO `tbl_companies` VALUES ('1', 'Pagodalabs', '1');
INSERT INTO `tbl_companies` VALUES ('2', 'Reliance', '2');
