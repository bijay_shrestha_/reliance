<?php

class Attendance extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('attendance','attendance_model');
        $this->load->module_model('department_report','department_report_model');

        $this->lang->module_load('attendance','attendance');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->attendance_model->joins = array('DEPARTMENTS');
		$this->db->order_by('name');
		$data['rows'] = $this->attendance_model->getAttendances()->result_array();
		// Display Page
		$data['header'] = 'Attendance Report';
		$data['view_page'] = "attendance/index";
		$data['module'] = 'attendance';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		// echo '<pre>';
		// print_r($this->input->post());echo '</pre>';
		$data['edit'] = $this->input->post('edit');
		$data['add'] = $this->input->post('add');
		// echo $edit;
		$date = $this->input->post('date');
		$date_en = $this->input->post('date_en');
		$where['date_np'] = $date;
		$this->_get_search_param();	
		$total=$this->attendance_model->count($where);
		$this->_get_search_param();	
		$this->attendance_model->joins = array('DEPARTMENTS');
		$records=$this->attendance_model->getAttendances($where)->result_array();
		$rows = array();
		foreach($records as $record){
			$rows[$record['department_id']] = $record;
		}
		// print_r($this->db->last_query());
		// print_r($rows);
		// echo json_encode(array('total'=>$total,'rows'=>$rows));
		// if(count($rows)>0){
			$data['rows'] = $rows;
			$data['total'] = $total;
			if($data['edit'] != 1 && $data['add'] != 1){
				$this->load->view('attendance/report',$data);
			}else{
				$data['date_np'] = $date;
				$data['date_en'] = $date_en;
				$this->department_report_model->joins = array('DEPARTMENTS');
				$data['departments'] = $this->department_report_model->getDepartmentReports(array('report_id' => 1))->result_array();
				// print_r($data);exit;
				$this->load->view('attendance/form',$data);	
				// echo json_encode($data);
			}
		// }else{
		// 	$data['date_np'] = $date;
		// 	$data['date_en'] = $date_en;
		// 	$this->department_report_model->joins = array('DEPARTMENTS');
		// 	$data['departments'] = $this->department_report_model->getDepartmentReports(array('report_id' => 1))->result_array();
		// 	// print_r($data);exit;
		// 	$this->load->view('attendance/form',$data);
		// }
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['day_staff']!='')?$this->db->like('day_staff',$params['search']['day_staff']):'';
($params['search']['day_contract']!='')?$this->db->like('day_contract',$params['search']['day_contract']):'';
($params['search']['night_staff']!='')?$this->db->like('night_staff',$params['search']['night_staff']):'';
($params['search']['night_contract']!='')?$this->db->like('night_contract',$params['search']['night_contract']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->attendance_model->getAttendances()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
  //   	$id=$this->input->post('id');
		// if($id && is_array($id))
		// {
  //       	foreach($id as $row):
		// 		$this->attendance_model->delete('ATTENDANCE',array('id'=>$row));
  //           endforeach;
		// }
		$where['date_np'] = $this->input->post('date');
		$success = $this->attendance_model->delete('ATTENDANCE',$where);
		echo json_encode(array('success'=>$success));
	}    

	public function save()
	{
	// echo '<pre>';	
   // print_r($this->input->post());
        $data=$this->_get_posted_data(); //Retrive Posted Data		
// print_r($data);
// exit;
		$success = $this->attendance_model->delete('ATTENDANCE',array('date_np'=>$data['date_np']));
			$value['date_np'] = $data['date_np'];
			$value['date_en'] = $data['date_en'];
			$i = 0;
		foreach($data['department_id'] as $department){
			$value['department_id'] = $department;
			$value['day_staff'] = $data['day_staff'][$i];
			$value['day_contract'] = $data['day_contract'][$i];
			$value['night_staff'] = $data['night_staff'][$i];
			$value['night_contract'] = $data['night_contract'][$i];
			$i++;
			$success=$this->attendance_model->insert('ATTENDANCE',$value);
			// print_r($this->db->last_query());
			// echo '<br>';
		}
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['department_id'] = $this->input->post('department_id');
$data['day_staff'] = $this->input->post('day_staff');
$data['day_contract'] = $this->input->post('day_contract');
$data['night_staff'] = $this->input->post('night_staff');
$data['night_contract'] = $this->input->post('night_contract');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');

        return $data;
   }
   
   	
	    
}