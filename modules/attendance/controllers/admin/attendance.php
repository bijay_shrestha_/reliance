<?php

class Attendance extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('attendance','attendance_model');
        $this->lang->module_load('attendance','attendance');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'attendance';
		$data['page'] = $this->config->item('template_admin') . "attendance/index";
		$data['module'] = 'attendance';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->attendance_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->attendance_model->getAttendances()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['day_staff']!='')?$this->db->like('day_staff',$params['search']['day_staff']):'';
($params['search']['day_contract']!='')?$this->db->like('day_contract',$params['search']['day_contract']):'';
($params['search']['night_staff']!='')?$this->db->like('night_staff',$params['search']['night_staff']):'';
($params['search']['night_contract']!='')?$this->db->like('night_contract',$params['search']['night_contract']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->attendance_model->getAttendances()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->attendance_model->delete('ATTENDANCE',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->attendance_model->insert('ATTENDANCE',$data);
        }
        else
        {
            $success=$this->attendance_model->update('ATTENDANCE',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['department_id'] = $this->input->post('department_id');
$data['day_staff'] = $this->input->post('day_staff');
$data['day_contract'] = $this->input->post('day_contract');
$data['night_staff'] = $this->input->post('night_staff');
$data['night_contract'] = $this->input->post('night_contract');

        return $data;
   }
   
   	
	    
}