<?php


$lang['id'] = 'Id';
$lang['department_id'] = 'Department Id';
$lang['day_staff'] = 'Day Staff';
$lang['day_contract'] = 'Day Contract';
$lang['night_staff'] = 'Night Staff';
$lang['night_contract'] = 'Night Contract';

$lang['create_attendance']='Create Attendance';
$lang['edit_attendance']='Edit Attendance';
$lang['delete_attendance']='Delete Attendance';
$lang['attendance_search']='Attendance Search';

$lang['attendance']='Attendance';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

