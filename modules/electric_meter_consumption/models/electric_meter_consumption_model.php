<?php
class Electric_meter_consumption_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array(
            'ELECTRIC_METER_CONSUMPTION'=>$this->prefix.'electric_meter_consumption',
            'ELECTRIC_METER'=>$this->prefix.'electric_meter',
            );
		$this->_JOINS=array(
            'ELECTRIC_METER'=>array('join_type'=>'LEFT','join_field'=>'electric_meter_consumptions.meter_id=electric_meter.id',
                                           'select'=>'electric_meter.meter','alias'=>'electric_meter'),
                           
                            );        
    }
    
    public function getElectricMeterConsumptions($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''),$fields = NULL)
    {
        if($fields == NULL){
            $fields='electric_meter_consumptions.*';
        }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['ELECTRIC_METER_CONSUMPTION']. ' electric_meter_consumptions');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['ELECTRIC_METER_CONSUMPTION'].' electric_meter_consumptions');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

    public function getReport($date){
        $sql = "SELECT meter, SUM(opening) AS opening,SUM(opening + consumption) AS closing, SUM(consumption) AS consumption FROM (
                SELECT
                    `electric_meter`.`meter`,
                    0 AS opening,
                    coalesce(consumption,0) AS consumption
                FROM
                    (
                        `tbl_electric_meter_consumption` electric_meter_consumptions
                    )
                LEFT JOIN `tbl_electric_meter` electric_meter ON `electric_meter_consumptions`.`meter_id` = `electric_meter`.`id`
                 WHERE `date_np` = '$date'
                GROUP BY meter
                UNION
                SELECT
                    `electric_meter`.`meter`,
                    coalesce(consumption,0) AS opening,
                    0 AS consumption
                FROM
                    (
                        `tbl_electric_meter_consumption` electric_meter_consumptions
                    )
                LEFT JOIN `tbl_electric_meter` electric_meter ON `electric_meter_consumptions`.`meter_id` = `electric_meter`.`id`
                 WHERE `date_np` < '$date'
                GROUP BY meter
                ) AS report
                GROUP BY meter";
        $data = $this->db->query($sql);
        return $data;
    }

    public function getOpening($date_en=NULL)
    {
        if($date_en != NULL){
            $sql = 'SELECT meter_id, SUM(consumption) AS opening FROM tbl_electric_meter_consumption WHERE date_en < "' . $date_en .'" GROUP BY meter_id';
        }else{
            $sql = 'SELECT meter_id, SUM(consumption) AS opening FROM tbl_electric_meter_consumption  GROUP BY meter_id';
        }
        $this->db->group_by('meter_id');
        $opening = $this->db->query($sql)->result_array();

        return $opening;
    }
}