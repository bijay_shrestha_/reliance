<?php

class Electric_meter_consumption extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('electric_meter_consumption','electric_meter_consumption_model');
        $this->lang->module_load('electric_meter_consumption','electric_meter_consumption');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'electric_meter_consumption';
		$data['page'] = $this->config->item('template_admin') . "electric_meter_consumption/index";
		$data['module'] = 'electric_meter_consumption';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->electric_meter_consumption_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->electric_meter_consumption_model->getElectricMeterConsumptions()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['meter_id']!='')?$this->db->where('meter_id',$params['search']['meter_id']):'';
($params['search']['consumption']!='')?$this->db->like('consumption',$params['search']['consumption']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->electric_meter_consumption_model->getElectricMeterConsumptions()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->electric_meter_consumption_model->delete('ELECTRIC_METER_CONSUMPTION',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->electric_meter_consumption_model->insert('ELECTRIC_METER_CONSUMPTION',$data);
        }
        else
        {
            $success=$this->electric_meter_consumption_model->update('ELECTRIC_METER_CONSUMPTION',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['meter_id'] = $this->input->post('meter_id');
$data['consumption'] = $this->input->post('consumption');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');

        return $data;
   }
   
   	
	    
}