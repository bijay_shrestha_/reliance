<?php

class Electric_meter_consumption extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('electric_meter_consumption','electric_meter_consumption_model');
        $this->load->module_model('electric_meter','electric_meter_model');
        $this->lang->module_load('electric_meter_consumption','electric_meter_consumption');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$date_en = $this->input->post('date_en');
		if($date_en == ''){
			$this->db->order_by('date_en','desc');
			$latest_input = $this->electric_meter_consumption_model->getElectricMeterConsumptions()->row_array();
			if(count($latest_input)>0){
				$date_en = $latest_input['date_en'];
			}else{
				$date_en = date('Y-m-d');
			}
		}
		$previous_input = $this->get_previous_day_input($date_en);
		if(count($previous_input) > 0){
			$previous_day_data = $this->electric_meter_consumption_model->getElectricMeterConsumptions(array('date_en'=>$previous_input['date_en']))->result_array();
		}

		$this->electric_meter_consumption_model->joins = array('ELECTRIC_METER');
		$this->db->where('date_en', $date_en);
		$data['rows'] = $this->electric_meter_consumption_model->getElectricMeterConsumptions()->result_array();

		if(count($previous_input) > 0){
			foreach($data['rows'] as $key => $value){
				foreach ($previous_day_data as $i => $v) {
					if($value['meter_id'] == $v['meter_id']){
						$data['rows'][$key]['consumption'] -= $v['consumption'];
					}
				}
			}
		}

		$data['meters'] = $this->electric_meter_model->getElectricMeters()->result_array();
		// Display Page
		$data['header'] = 'Electric Meter consumption';
		$data['view_page'] = "electric_meter_consumption/index";
		$data['module'] = 'electric_meter_consumption';
		$this->load->view($this->_container,$data);		
	}
	private function get_previous_day_input($date_en){
		$where['date_en < '] = $date_en;
		$this->db->order_by('date_en desc');
		$data = $this->electric_meter_consumption_model->getElectricMeterConsumptions($where)->row_array();
		return $data;
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->electric_meter_consumption_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->electric_meter_consumption_model->getElectricMeterConsumptions()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['meter_id']!='')?$this->db->where('meter_id',$params['search']['meter_id']):'';
($params['search']['consumption']!='')?$this->db->like('consumption',$params['search']['consumption']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->electric_meter_consumption_model->getElectricMeterConsumptions()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->electric_meter_consumption_model->delete('ELECTRIC_METER_CONSUMPTION',array('id'=>$row));
            endforeach;
		}else{
			$data = $this->electric_meter_consumption_model->getElectricMeterConsumptions(array('id' => $id))->row_array();
			$this->electric_meter_consumption_model->delete('ELECTRIC_METER_CONSUMPTION',array('date_np'=>$data['date_np']));
		}

		$success = TRUE;
		echo json_encode($success);
	}    

	public function save()
	{
		
		// print_r($this->input->post());
        $data=$this->_get_posted_data(); //Retrive Posted Data		
		// print_r($data);
		// exit;
		$this->electric_meter_consumption_model->delete('ELECTRIC_METER_CONSUMPTION',array('date_np' => $data['date_np']));

		$val['date_np'] = $data['date_np'];
		$val['date_en'] = $data['date_en'];
		foreach ($data['consumption'] as $key => $value){
			$val['meter_id'] = $key;
			$val['consumption'] = $value;
			$this->electric_meter_consumption_model->insert('ELECTRIC_METER_CONSUMPTION',$val);
		}

		$success = TRUE;

		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));	


  //       $data=$this->_get_posted_data(); //Retrive Posted Data		
  //       $val['date_np'] = $data['date_np'];
  //       $val['date_en'] = $data['date_en'];

  //       $this->db->group_by('meter_id');
  //       $today_consumed = $this->electric_meter_consumption_model->getElectricMeterConsumptions(array('date_np' => $val['date_np']))->result_array();
  //       $this->db->order_by('date_np desc');
  //       $next_day = $this->electric_meter_consumption_model->getElectricMeterConsumptions(array('date_np >' => $val['date_np']))->row_array();
  //       if(count($next_day) > 0){
	 //        foreach ($today_consumed as $key => $value) {
	 //        	$sql = 'UPDATE tbl_electric_meter_consumption SET consumption = consumption +'. $value['consumption']. ' WHERE date_en ="' .$next_day['date_en']. '" AND meter_id = '. $value['meter_id'];
	 //        	$this->db->query($sql);
	 //        }
  //       }

  //       $this->electric_meter_consumption_model->delete('ELECTRIC_METER_CONSUMPTION',array('date_np' => $val['date_np']));
  //       $opening = $this->electric_meter_consumption_model->getOpening($val['date_en']);

  //       foreach ($data['meter_id'] as $key => $value) {
  //       	$val['meter_id'] = $data['meter_id'][$key];
  //       	$val['consumption'] = $data['consumption'][$data['meter_id'][$key]];
  //       	// subtracting opening
  //       	foreach ($opening as $index => $open) {
  //       		if($open['meter_id'] == $val['meter_id'] && $val['consumption'] != 0){
  //       			$val['consumption'] -= $open['opening'];
  //       		}elseif($open['meter_id'] == $val['meter_id'] && $val['consumption'] > $open['opening']){
  //       			$val['consumption'] -= 0;
  //       		}
  //       	}
  //       	$success=$this->electric_meter_consumption_model->insert('ELECTRIC_METER_CONSUMPTION',$val);
  //       	if(count($next_day) > 0){
	 //        	if($val['consumption'] > 0 ){
		//         	$sql = 'UPDATE tbl_electric_meter_consumption SET 
		//         	consumption = CASE '.
		//         		'WHEN consumption > '. $val['consumption'] .' THEN consumption -'. $val['consumption']. 
		//         		' ELSE 0 END
		//         		WHERE date_en = "' .$next_day['date_en'] . '" AND meter_id = '. $val['meter_id'];
		//         	$this->db->query($sql);
	 //        	}
  //       	}

  //       }

  //       // if(!$this->input->post('id'))
  //       // {
  //       //     $success=$this->electric_meter_consumption_model->insert('ELECTRIC_METER_CONSUMPTION',$data);
  //       // }
  //       // else
  //       // {
  //       //     $success=$this->electric_meter_consumption_model->update('ELECTRIC_METER_CONSUMPTION',$data,array('id'=>$data['id']));
  //       // }
        
		// if($success)
		// {
		// 	$success = TRUE;
		// 	$msg=lang('success_message'); 
		// } 
		// else
		// {
		// 	$success = FALSE;
		// 	$msg=lang('failure_message');
		// }
		 
		//  echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['meter_id'] = $this->input->post('meter_id');
$data['consumption'] = $this->input->post('consumption');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');

        return $data;
   }
   
   	public function get_value()
    {
        $where['id'] = $this->input->post('id');
        $val = $this->electric_meter_consumption_model->getElectricMeterConsumptions($where)->row_array();

        // $where_opening['date_np <= '] = $val['date_np'];
        // $this->db->group_by('meter_id');
        // $this->db->order_by('date_np');
        // $fields = 'date_np, date_en, meter_id, SUM(consumption) AS consumption';
        // $opening = $this->electric_meter_consumption_model->getElectricMeterConsumptions($where_opening,NULL,NULL,$fields)->result_array();

        $where_date['date_np'] = $val['date_np'];
        $data = $this->electric_meter_consumption_model->getElectricMeterConsumptions($where_date)->result_array();
        $data['date_np'] = $val['date_np'];
        $data['date_en'] = $val['date_en'];

        echo json_encode($data);
    }

//     public function convert(){
// 	   	echo '<pre>';
// 	   		$this->db->order_by('id asc');
// 	   		$data = $this->electric_meter_consumption_model->getElectricMeterConsumptions()->result_array();
// $val = array();
// 	   		foreach ($data as $key => $value) {
// 	   			if(array_key_exists($value['meter_id'], $val)){
// 	   				$val[$value['meter_id']]['consumption'] += $value['consumption'];
// 	   				echo $val[$value['meter_id']]['consumption'].'<br>';
// 	   				$this->electric_meter_consumption_model->update('ELECTRIC_METER_CONSUMPTION',$val[$value['meter_id']],array('id' => $value['id']));
// 	   			}else{
// 	   				$val[$value['meter_id']]['consumption'] = $value['consumption'];
// 	   				$this->electric_meter_consumption_model->update('ELECTRIC_METER_CONSUMPTION',$val[$value['meter_id']],array('id' => $value['id']));
// 	   			}
// 	   		}
// 	   			print_r($val);

// 	   		echo $this->db->last_query();
// 	   		// print_r($data);exit;
// 	   } 
	    
}