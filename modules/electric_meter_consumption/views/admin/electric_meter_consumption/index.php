<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('electric_meter_consumption_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="electric_meter_consumption-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('meter_id')?></label>:</td>
<td><input type="text" name="search[meter_id]" id="search_meter_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('consumption')?></label>:</td>
<td><input type="text" name="search[consumption]" id="search_consumption"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('date_np')?></label>:</td>
<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('date_en')?></label>:</td>
<td><input type="text" name="date[date_en][from]" id="search_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_en][to]" id="search_date_en_to"  class="easyui-datebox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="electric_meter_consumption-table" data-options="pagination:true,title:'<?php  echo lang('electric_meter_consumption')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'meter_id',sortable:true" width="50"><?php echo lang('meter_id')?></th>
<th data-options="field:'consumption',sortable:true" width="50"><?php echo lang('consumption')?></th>
<th data-options="field:'date_np',sortable:true" width="50"><?php echo lang('date_np')?></th>
<th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_electric_meter_consumption')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_electric_meter_consumption')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit electric_meter_consumption form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-electric_meter_consumption" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('meter_id')?>:</label></td>
					  <td width="66%"><input name="meter_id" id="meter_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('consumption')?>:</label></td>
					  <td width="66%"><input name="consumption" id="consumption" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datebox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#electric_meter_consumption-search-form').form('clear');
			$('#electric_meter_consumption-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#electric_meter_consumption-table').datagrid({
				queryParams:{data:$('#electric_meter_consumption-search-form').serialize()}
				});
		});		
		$('#electric_meter_consumption-table').datagrid({
			url:'<?php  echo site_url('electric_meter_consumption/admin/electric_meter_consumption/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_electric_meter_consumption')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeelectric_meter_consumption('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_electric_meter_consumption')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-electric_meter_consumption').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_electric_meter_consumption')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#electric_meter_consumption-table').datagrid('getRows')[index];
		if (row){
			$('#form-electric_meter_consumption').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_electric_meter_consumption')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeelectric_meter_consumption(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#electric_meter_consumption-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('electric_meter_consumption/admin/electric_meter_consumption/delete_json')?>', {id:[row.id]}, function(){
					$('#electric_meter_consumption-table').datagrid('deleteRow', index);
					$('#electric_meter_consumption-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#electric_meter_consumption-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('electric_meter_consumption/admin/electric_meter_consumption/delete_json')?>',{id:selected},function(data){
						$('#electric_meter_consumption-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-electric_meter_consumption').form('submit',{
			url: '<?php  echo site_url('electric_meter_consumption/admin/electric_meter_consumption/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-electric_meter_consumption').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#electric_meter_consumption-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>