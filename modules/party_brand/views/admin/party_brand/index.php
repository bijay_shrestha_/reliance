<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('party_brand_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="party_brand-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('party_id')?></label>:</td>
<td><input type="text" name="search[party_id]" id="search_party_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('brand_type_id')?></label>:</td>
<td><input type="text" name="search[brand_type_id]" id="search_brand_type_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('brand_id')?></label>:</td>
<td><input type="text" name="search[brand_id]" id="search_brand_id"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="party_brand-table" data-options="pagination:true,title:'<?php  echo lang('party_brand')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'party_id',sortable:true" width="50"><?php echo lang('party_id')?></th>
<th data-options="field:'brand_type_id',sortable:true" width="50"><?php echo lang('brand_type_id')?></th>
<th data-options="field:'brand_id',sortable:true" width="50"><?php echo lang('brand_id')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_party_brand')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_party_brand')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit party_brand form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-party_brand" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('party_id')?>:</label></td>
					  <td width="66%"><input name="party_id" id="party_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('brand_type_id')?>:</label></td>
					  <td width="66%"><input name="brand_type_id" id="brand_type_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('brand_id')?>:</label></td>
					  <td width="66%"><input name="brand_id" id="brand_id" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#party_brand-search-form').form('clear');
			$('#party_brand-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#party_brand-table').datagrid({
				queryParams:{data:$('#party_brand-search-form').serialize()}
				});
		});		
		$('#party_brand-table').datagrid({
			url:'<?php  echo site_url('party_brand/admin/party_brand/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_party_brand')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeparty_brand('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_party_brand')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-party_brand').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_party_brand')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#party_brand-table').datagrid('getRows')[index];
		if (row){
			$('#form-party_brand').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_party_brand')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeparty_brand(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#party_brand-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('party_brand/admin/party_brand/delete_json')?>', {id:[row.id]}, function(){
					$('#party_brand-table').datagrid('deleteRow', index);
					$('#party_brand-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#party_brand-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('party_brand/admin/party_brand/delete_json')?>',{id:selected},function(data){
						$('#party_brand-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-party_brand').form('submit',{
			url: '<?php  echo site_url('party_brand/admin/party_brand/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-party_brand').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#party_brand-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>