<?php
class Party_brand_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('PARTY_BRAND'=>$this->prefix.'party_brand','BRAND_TYPE'=>$this->prefix.'brand_type','BRAND'=>$this->prefix.'brand');
		$this->_JOINS=array('BRAND'=>array('join_type'=>'LEFT','join_field'=>'party_brands.brand_id=brand.id',
                                           'select'=>'brand.brand_name','alias'=>'brand'),
                            'BRAND_TYPE'=>array('join_type'=>'LEFT','join_field'=>'brand.type_id=brand_type.id',
                                           'select'=>'brand_type.name as type_name','alias'=>'brand_type'),
                           
                            );        
    }
    
    public function getPartyBrands($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='party_brands.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['PARTY_BRAND']. ' party_brands');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['PARTY_BRAND'].' party_brands');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}