<?php

class Party_brand extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('party_brand','party_brand_model');
        $this->lang->module_load('party_brand','party_brand');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'party_brand';
		$data['page'] = $this->config->item('template_admin') . "party_brand/index";
		$data['module'] = 'party_brand';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->party_brand_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->party_brand_model->getPartyBrands()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['party_id']!='')?$this->db->where('party_id',$params['search']['party_id']):'';
($params['search']['brand_type_id']!='')?$this->db->where('brand_type_id',$params['search']['brand_type_id']):'';
($params['search']['brand_id']!='')?$this->db->where('brand_id',$params['search']['brand_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->party_brand_model->getPartyBrands()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->party_brand_model->delete('PARTY_BRAND',array('id'=>$row));
            endforeach;
		}else{
			$success = $this->party_brand_model->delete('PARTY_BRAND',array('id'=>$id));
			echo json_encode($success);
		}
	}    

	public function save()
	{

        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->party_brand_model->insert('PARTY_BRAND',$data);
        }
        else
        {
            $success=$this->party_brand_model->update('PARTY_BRAND',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['party_id'] = $this->input->post('party_id');
$data['brand_type_id'] = $this->input->post('brand_type_id');
$data['brand_id'] = $this->input->post('brand_id');

        return $data;
   }
   
   	function get_party_brand_json(){
   		$where['party_id'] = $this->input->post('party_id');
   		$this->party_brand_model->joins = array('BRAND','BRAND_TYPE');

   		$data = $this->party_brand_model->getPartyBrands($where)->result_array();

   		echo json_encode($data);

   	}

   	function getAssignedBrand($party_id = NULL){
   		$where['party_id'] = $this->input->post('party_id');

   		$this->party_brand_model->joins = array('BRAND','BRAND_TYPE');
		$data['rows'] = $this->party_brand_model->getPartyBrands($where)->result_array();
		
   		$this->load->view('party/brand_list',$data);

   	}

   	function getAssignedBrandForm($party_id = NULL){
   		$where['party_id'] = $this->input->post('party_id');

   		// $this->party_brand_model->joins = array('BRAND','BRAND_TYPE');
		$data = $this->party_brand_model->getPartyBrands($where)->result_array();

		echo json_encode($data);
   	}

   	public function saveAssignedBrand(){
   		$brand_types = $this->input->post('type');
   		$brand = $this->input->post('brand');
   		$data['party_id'] = $this->input->post('party_id');

   		$this->party_brand_model->delete('PARTY_BRAND',$data);

   		foreach($brand_types as $key => $value){
   			$data['brand_type_id'] = $value;
   			$data['brand_id'] = $brand[$key];

   			$this->party_brand_model->insert('PARTY_BRAND',$data);
   		}
   	}
	    
}