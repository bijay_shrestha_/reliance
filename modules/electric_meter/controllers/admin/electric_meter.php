<?php

class Electric_meter extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('electric_meter','electric_meter_model');
        $this->lang->module_load('electric_meter','electric_meter');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'electric_meter';
		$data['page'] = $this->config->item('template_admin') . "electric_meter/index";
		$data['module'] = 'electric_meter';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->electric_meter_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->electric_meter_model->getElectricMeters()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['meter']!='')?$this->db->like('meter',$params['search']['meter']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->electric_meter_model->getElectricMeters()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->electric_meter_model->delete('ELECTRIC_METER',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->electric_meter_model->insert('ELECTRIC_METER',$data);
        }
        else
        {
            $success=$this->electric_meter_model->update('ELECTRIC_METER',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['meter'] = $this->input->post('meter');

        return $data;
   }
   
   	
	    
}