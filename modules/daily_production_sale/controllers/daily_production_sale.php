<?php

class Daily_production_sale extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('daily_production_sale','daily_production_sale_model');
        $this->load->module_model('brand','brand_model');
        $this->load->module_model('brand_type','brand_type_model');

        $this->lang->module_load('daily_production_sale','daily_production_sale');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->daily_production_sale_model->joins = array('BRAND','BRAND_TYPE');
		$data['rows'] = $this->daily_production_sale_model->getDailyProductionSales()->result_array();
		$data['brands'] = $this->brand_model->getBrands()->result_array();
		$data['brand_types'] = $this->brand_type_model->getBrandTypes()->result_array();
		// Display Page
		$data['header'] = 'Daily Production Sale';
		$data['view_page'] = "daily_production_sale/index";
		$data['module'] = 'daily_production_sale';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->daily_production_sale_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->daily_production_sale_model->getDailyProductionSales()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['type_id']!='')?$this->db->where('type_id',$params['search']['type_id']):'';
($params['search']['brand_id']!='')?$this->db->where('brand_id',$params['search']['brand_id']):'';
($params['search']['production']!='')?$this->db->where('production',$params['search']['production']):'';
($params['search']['sales']!='')?$this->db->where('sales',$params['search']['sales']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->daily_production_sale_model->getDailyProductionSales()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->daily_production_sale_model->delete('DAILY_PRODUCTION_SALES',array('id'=>$row));
            endforeach;
		}else{
			$success = $this->daily_production_sale_model->delete('DAILY_PRODUCTION_SALES',array('id'=>$id));
			echo json_encode($success);
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->daily_production_sale_model->insert('DAILY_PRODUCTION_SALES',$data);
        }
        else
        {
            $success=$this->daily_production_sale_model->update('DAILY_PRODUCTION_SALES',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['type_id'] = $this->input->post('type_id');
$data['brand_id'] = $this->input->post('brand_id');
$data['production'] = $this->input->post('production');
$data['sales'] = $this->input->post('sales');

        return $data;
   }
   
   public function get_value()
   {
   		$where['id'] = $this->input->post('id');
   		$data = $this->daily_production_sale_model->getDailyProductionSales($where)->row_array();
   		echo json_encode($data);
   }
   	
	    
}