<?php
class Daily_production_sale_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array(
            'DAILY_PRODUCTION_SALES'=>$this->prefix.'daily_production_sales',
            'BRAND'=>$this->prefix.'brand',
            'BRAND_TYPE'=>$this->prefix.'brand_type'
            );
		$this->_JOINS=array(
            'BRAND'=>array('join_type'=>'LEFT','join_field'=>'daily_production_sales.brand_id=brand.id','select'=>'brand.brand_name','alias'=>'brand'),
            'BRAND_TYPE'=>array('join_type'=>'LEFT','join_field'=>'daily_production_sales.type_id=brand_type.id','select'=>'brand_type.name as brand_type','alias'=>'brand_type'),
                           
                            );        
    }
    
    public function getDailyProductionSales($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''),$fields = NULL)
    {
        if($fields == NULL){
            $fields='daily_production_sales.*';
        }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['DAILY_PRODUCTION_SALES']. ' daily_production_sales');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['DAILY_PRODUCTION_SALES'].' daily_production_sales');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

    public function getReport($wheredates, $group_by = 'brand_name')
    {
        $sql = "SELECT
    brand_name,
    brand_type,
    SUM(
                report.total_production
            ) AS total_production,
    SUM(
                report.total_sales
            ) AS total_sales,
    SUM(
                report.monthly_total_production
            ) AS monthly_total_production,
    SUM(
        report.monthly_total_sales
    ) AS monthly_total_sales,
    SUM(
            report.yearly_total_production
        ) AS yearly_total_production,
    SUM(
            report.yearly_total_sales
        ) AS yearly_total_sales
FROM
    (
        SELECT
            `brand_name`,
            SUM(
                daily_production_sales.production
            ) AS total_production,
            SUM(
                daily_production_sales.sales
            ) AS total_sales,
            0 AS monthly_total_production,
            0 AS monthly_total_sales,
            0 AS yearly_total_production,
            0 AS yearly_total_sales,
            `brand_type`.`name` AS brand_type
        FROM
            (
                `tbl_daily_production_sales` daily_production_sales
            )
        LEFT JOIN `tbl_brand` brand ON `daily_production_sales`.`brand_id` = `brand`.`id`
        LEFT JOIN `tbl_brand_type` brand_type ON `daily_production_sales`.`type_id` = `brand_type`.`id`
        WHERE ".
            $wheredates['day'] ."
        GROUP BY
            `daily_production_sales`.`brand_id`
        UNION
            SELECT
                `brand_name`,
                0 AS total_production,
                0 AS total_sales,
                SUM(
                    daily_production_sales.production
                ) AS monthly_total_production,
                SUM(
                    daily_production_sales.sales
                ) AS monthly_total_sales,
                0 AS yearly_total_production,
                0 AS yearly_total_sales,
                `brand_type`.`name` AS brand_type
            FROM
                (
                    `tbl_daily_production_sales` daily_production_sales
                )
            LEFT JOIN `tbl_brand` brand ON `daily_production_sales`.`brand_id` = `brand`.`id`
            LEFT JOIN `tbl_brand_type` brand_type ON `daily_production_sales`.`type_id` = `brand_type`.`id`
            WHERE ".
                $wheredates['month'] ."
            GROUP BY
                `daily_production_sales`.`brand_id`
            UNION
                SELECT
                    `brand_name`,
                    0 AS total_production,
                    0 AS total_sales,
                    0 AS monthly_total_production,
                    0 AS monthly_total_sales,
                    SUM(
                        daily_production_sales.production
                    ) AS yearly_total_production,
                    SUM(
                        daily_production_sales.sales
                    ) AS yearly_total_sales,
                    `brand_type`.`name` AS brand_type
                FROM
                    (
                        `tbl_daily_production_sales` daily_production_sales
                    )
                LEFT JOIN `tbl_brand` brand ON `daily_production_sales`.`brand_id` = `brand`.`id`
                LEFT JOIN `tbl_brand_type` brand_type ON `daily_production_sales`.`type_id` = `brand_type`.`id`
                WHERE ".
                    $wheredates['year'] ."
                GROUP BY
                    `daily_production_sales`.`brand_id`
    ) AS report
GROUP BY
    $group_by";

            $data = $this->db->query($sql);
            return $data;
    }

    /*for stock report*/
    public function production_sales_stock($date)
    {
        $sql = "SELECT
                    brand_name,
                    brand_type,
                    SUM(opening) AS opening,
                    SUM(production) AS production,
                    SUM(sales) AS sales
                FROM
                    (SELECT
                        `brand_name`,
                        `name` AS brand_type,
                        SUM(production - sales) AS opening,
                        0 AS production,
                        0 AS sales
                    FROM
                        (
                            `tbl_daily_production_sales` daily_production_sales
                        )
                    LEFT JOIN `tbl_brand` brand ON `daily_production_sales`.`brand_id` = `brand`.`id`
                    LEFT JOIN `tbl_brand_type` brand_type ON `daily_production_sales`.`type_id` = `brand_type`.`id`
                    WHERE
                        `date_np` < '$date'
                    GROUP BY brand_type
                    UNION
                    SELECT 
                        `brand_name`,
                        `name` AS brand_type,
                        0 AS opening,
                        SUM(production) AS production,
                        SUM(sales) AS sales
                    FROM
                        `tbl_daily_production_sales` daily_production_sales
                    LEFT JOIN `tbl_brand` brand ON `daily_production_sales`.`brand_id` = `brand`.`id`
                    LEFT JOIN `tbl_brand_type` brand_type ON `daily_production_sales`.`type_id` = `brand_type`.`id`
                    WHERE
                        `date_np` = '$date'
                    GROUP BY brand_type) AS result
                    GROUP BY brand_name";

        $data = $this->db->query($sql);
        return $data;
    }
}