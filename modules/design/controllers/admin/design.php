<?php

class Design extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('design','design_model');
        $this->lang->module_load('design','design');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'design';
		$data['page'] = $this->config->item('template_admin') . "design/index";
		$data['module'] = 'design';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->design_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->design_model->getDesigns()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['design_name']!='')?$this->db->like('design_name',$params['search']['design_name']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->design_model->getDesigns()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->design_model->delete('DESIGNS',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('design_id'))
        {
            $success=$this->design_model->insert('DESIGNS',$data);
        }
        else
        {
            $success=$this->design_model->update('DESIGNS',$data,array('id'=>$data['design_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('design_id');
$data['design_name'] = $this->input->post('design_name');

        return $data;
   }
   
   	public function combobox_design_id()
    {
    	$this->db->order_by('design_name');
    	$rows = $this->design_model->getDesigns()->result_array();
    	echo json_encode($rows);
    }
	    
}