<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('mst_lab_requirement_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="mst_lab_requirement-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('parameter')?></label>:</td>
<td><input type="text" name="search[parameter]" id="search_parameter"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('value')?></label>:</td>
<td><input type="text" name="search[value]" id="search_value"  class="easyui-numberbox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="mst_lab_requirement-table" data-options="pagination:true,title:'<?php  echo lang('mst_lab_requirement')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'parameter',sortable:true" width="50"><?php echo lang('parameter')?></th>
<th data-options="field:'value',sortable:true" width="50"><?php echo lang('value')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
   <!--  <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_mst_lab_requirement')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_mst_lab_requirement')?>"><?php  echo lang('remove_selected')?></a>
    </p> -->

</div> 

<!--for create and edit mst_lab_requirement form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-mst_lab_requirement" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('parameter')?>:</label></td>
					  <td width="66%"><input name="parameter" id="parameter" class="easyui-validatebox" required="true" disabled></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('value')?>:</label></td>
					  <td width="66%"><input name="value" id="value" class="easyui-numberbox" required="true" data-options="precision:4"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#mst_lab_requirement-search-form').form('clear');
			$('#mst_lab_requirement-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#mst_lab_requirement-table').datagrid({
				queryParams:{data:$('#mst_lab_requirement-search-form').serialize()}
				});
		});		
		$('#mst_lab_requirement-table').datagrid({
			url:'<?php  echo site_url('mst_lab_requirement/admin/mst_lab_requirement/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_mst_lab_requirement')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removemst_lab_requirement('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_mst_lab_requirement')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-mst_lab_requirement').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_mst_lab_requirement')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#mst_lab_requirement-table').datagrid('getRows')[index];
		if (row){
			$('#form-mst_lab_requirement').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_mst_lab_requirement')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removemst_lab_requirement(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#mst_lab_requirement-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('mst_lab_requirement/admin/mst_lab_requirement/delete_json')?>', {id:[row.id]}, function(){
					$('#mst_lab_requirement-table').datagrid('deleteRow', index);
					$('#mst_lab_requirement-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#mst_lab_requirement-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('mst_lab_requirement/admin/mst_lab_requirement/delete_json')?>',{id:selected},function(data){
						$('#mst_lab_requirement-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-mst_lab_requirement').form('submit',{
			url: '<?php  echo site_url('mst_lab_requirement/admin/mst_lab_requirement/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-mst_lab_requirement').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#mst_lab_requirement-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>