<?php

class Mst_lab_requirement extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('mst_lab_requirement','mst_lab_requirement_model');
        $this->lang->module_load('mst_lab_requirement','mst_lab_requirement');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'mst_lab_requirement';
		$data['page'] = $this->config->item('template_admin') . "mst_lab_requirement/index";
		$data['module'] = 'mst_lab_requirement';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->mst_lab_requirement_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->mst_lab_requirement_model->getMstLabRequirements()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['parameter']!='')?$this->db->like('parameter',$params['search']['parameter']):'';
($params['search']['value']!='')?$this->db->where('value',$params['search']['value']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->mst_lab_requirement_model->getMstLabRequirements()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->mst_lab_requirement_model->delete('MST_LAB_REQUIREMENT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->mst_lab_requirement_model->insert('MST_LAB_REQUIREMENT',$data);
        }
        else
        {
            $success=$this->mst_lab_requirement_model->update('MST_LAB_REQUIREMENT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
// $data['parameter'] = $this->input->post('parameter');
$data['value'] = $this->input->post('value');

        return $data;
   }
   
   	
	    
}