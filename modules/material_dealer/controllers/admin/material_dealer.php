<?php

class Material_dealer extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('material_dealer','material_dealer_model');
        $this->load->module_model('material','material_model');
        $this->lang->module_load('material_dealer','material_dealer');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'material_dealer';
		$data['page'] = $this->config->item('template_admin') . "material_dealer/index";
		$data['module'] = 'material_dealer';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->material_dealer_model->joins = array('DEALERS','MATERIALS');
		$this->_get_search_param();	
		$total=$this->material_dealer_model->count();
		paging('md_id');
		$this->_get_search_param();	
		$rows=$this->material_dealer_model->getMaterialDealers()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['dealer_id']!='')?$this->db->where('dealer_id',$params['search']['dealer_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->material_dealer_model->getMaterialDealers()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->material_dealer_model->delete('MATERIAL_DEALERS',array('md_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('md_id'))
        {
            $success=$this->material_dealer_model->insert('MATERIAL_DEALERS',$data);
        }
        else
        {
            $success=$this->material_dealer_model->update('MATERIAL_DEALERS',$data,array('md_id'=>$data['md_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['md_id'] = $this->input->post('md_id');
$data['material_id'] = $this->input->post('material_id');
$data['dealer_id'] = $this->input->post('dealer_id');

        return $data;
   }
   
   public function getMaterialDealer(){
   		$where['material_dealers.dealer_id'] = $this->input->post('dealer_id');
   		$this->material_dealer_model->joins = array('DEALERS','MATERIALS');
   		$rows=$this->material_dealer_model->getMaterialDealers($where)->result_array();
   		$materials = $this->material_model->getMaterials()->result_array();
		echo json_encode(array('materials'=>$materials,'rows'=>$rows));
   }  

   public function saveAll(){
   		echo '<pre>here';
   		print_r($this->input->post());exit;
   		$data['dealer_id'] = $this->input->post('dealer_id');
   		$success = $this->material_dealer_model->delete('MATERIAL_DEALERS',$data);
   		$material_ids = $this->input->post('material_id');
   		if($success){
	   		foreach($material_ids as $material_id){
	   			$data['material_id'] = $material_id;
	   			// print_r($data);
	   			$success = $this->material_dealer_model->insert('MATERIAL_DEALERS',$data);
	   		}
   		}
   		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));
   } 	
	    
}