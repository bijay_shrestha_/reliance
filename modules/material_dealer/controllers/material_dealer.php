<?php

class Material_dealer extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('material_dealer','material_dealer_model');
        $this->load->module_model('material','material_model');
        $this->lang->module_load('material_dealer','material_dealer');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'material_dealer';
		$data['page'] = $this->config->item('template_admin') . "material_dealer/index";
		$data['module'] = 'material_dealer';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->material_dealer_model->count();
		paging('md_id');
		$this->_get_search_param();	
		$rows=$this->material_dealer_model->getMaterialDealers()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['dealer_id']!='')?$this->db->where('dealer_id',$params['search']['dealer_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->material_dealer_model->getMaterialDealers()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->material_dealer_model->delete('MATERIAL_DEALERS',array('md_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        // $data=$this->_get_posted_data(); //Retrive Posted Data		
		$data=array();
        $data['md_id'] = $this->input->post('md_id');
        $material_ids = $this->input->post('material_id');
		$data['material_id'] = $this->input->post('material_id');
		$data['dealer_id'] = $this->input->post('dealer_id');
print_r($data);exit;

        if(!$data['dealer_id'] == ""){
        	return false;
        }else{
	        if(!$this->input->post('md_id'))
	        {
	            $success=$this->material_dealer_model->insert('MATERIAL_DEALERS',$data);
	        }
	        else
	        {
	            $success=$this->material_dealer_model->update('MATERIAL_DEALERS',$data,array('md_id'=>$data['md_id']));
	        }
	        
			if($success)
				$success = TRUE;
			{
				$msg=lang('success_message'); 
			} 
			 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        }
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['md_id'] = $this->input->post('md_id');
        $material_ids = $this->input->post('material_id');
$data['material_id'] = $this->input->post('material_id');
$data['dealer_id'] = $this->input->post('dealer_id');

        return $data;
   }
   
   public function getformmaterial() {
       $data['metrials'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
       $id = $this->input->post('id');
       $data['result'] = $this->material_dealer_model->getMaterialDealers(array('dealer_id'=>$id))->result_array();
       echo json_encode($data);
   }

   	public function savemd(){
   		$data['dealer_id'] = $this->input->post('dealer_id');
   		$msg = 'Select Dealer';
		$success = FALSE;
   		if($data['dealer_id'] == ''){
   			$msg = 'Select Dealer';
			$success = FALSE;
   		}else{
   			if($this->input->post('material')){
   				$material_ids = $this->input->post('material'); 
   				$this->material_dealer_model->delete('MATERIAL_DEALERS',(array('dealer_id'=>$data['dealer_id'])));
   				foreach($material_ids as $material_id){
   					$data['material_id'] = $material_id;
   					$success = $this->material_dealer_model->insert('MATERIAL_DEALERS',$data);
   				}
   				$msg = 'Successfully Added';
   			}else {
   				$msg = 'Select Material';
   				$success = FALSE;
   			}
   		}
   		echo json_encode(array('msg'=>$msg,'success'=>$success));
   	}
   	
   	public function getDealer(){
   		$id = $this->input->post('id');
   		$this->material_dealer_model->joins = array('DEALERS');
   		$result = $this->material_dealer_model->getMaterialDealers(array('material_id'=>$id))->result_array();
   		echo json_encode($result);
   	}
	    
}