<?php
class Material_dealer_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('MATERIAL_DEALERS'=>$this->prefix.'material_dealers','DEALERS'=>$this->prefix.'dealers','MATERIALS'=>$this->prefix.'materials');
		$this->_JOINS=array('DEALERS'=>array('join_type'=>'LEFT','join_field'=>'material_dealers.dealer_id=dealers.dealer_id',
                                           'select'=>'dealers.*','alias'=>'dealers'),
                            'MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'material_dealers.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials')
                           
                            );        
    }
    
    public function getMaterialDealers($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='material_dealers.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['MATERIAL_DEALERS']. ' material_dealers');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['MATERIAL_DEALERS'].' material_dealers');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}