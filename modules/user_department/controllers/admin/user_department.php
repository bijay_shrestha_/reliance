<?php

class User_department extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('user_department','user_department_model');
        $this->lang->module_load('user_department','user_department');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'user_department';
		$data['page'] = $this->config->item('template_admin') . "user_department/index";
		$data['module'] = 'user_department';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->user_department_model->joins = array('DEPARTMENTS','Users');
		$this->_get_search_param();	
		$total=$this->user_department_model->count();
		paging('ud_id');
		$this->_get_search_param();	
		$rows=$this->user_department_model->getUserDepartments()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['user_id']!='')?$this->db->like('username',$params['search']['user_id']):'';
($params['search']['department_id']!='')?$this->db->like('name',$params['search']['department_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->user_department_model->getUserDepartments()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->user_department_model->delete('USER_DEPARTMENT',array('ud_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('ud_id'))
        {
            $success=$this->user_department_model->insert('USER_DEPARTMENT',$data);
        }
        else
        {
            $success=$this->user_department_model->update('USER_DEPARTMENT',$data,array('ud_id'=>$data['ud_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['ud_id'] = $this->input->post('ud_id');
$data['user_id'] = $this->input->post('user_id');
$data['department_id'] = $this->input->post('department_id');

        return $data;
   }
   
   	
	    
}