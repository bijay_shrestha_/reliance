<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('user_department_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="user_department-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('user_id')?></label>:</td>
<td><input type="text" name="search[user_id]" id="search_user_id"  class="easyui-validbox"/></td>
<td><label><?php echo lang('department_id')?></label>:</td>
<td><input type="text" name="search[department_id]" id="search_department_id"  class="easyui-validbox"/></td>
</tr>
<tr>
</tr>
  <tr>
	<td colspan="4">
	<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
	<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
	</td>
	</tr>
</table>

</form>
</div>
<br/>
<table id="user_department-table" data-options="pagination:true,title:'<?php  echo lang('user_department')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
	<thead>
	<th data-options="field:'checkbox',checkbox:true"></th>
	<!-- <th data-options="field:'ud_id',sortable:true" width="30"><?php echo lang('ud_id')?></th> -->
<th data-options="field:'username',sortable:true" width="50"><?php echo lang('user_name')?></th>
<th data-options="field:'name',sortable:true" width="50"><?php echo lang('department')?></th>

	<th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
	</thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
	<p>
	<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_user_department')?>"><?php  echo lang('create')?></a>
	<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_user_department')?>"><?php  echo lang('remove_selected')?></a>
	</p>

</div> 

<!--for create and edit user_department form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
		data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
	<form id="form-user_department" method="post" >
	<table>
		<tr>
					  <td width="34%" ><label><?php echo lang('user_name')?>:</label></td>
					  <td width="66%"><input name="user_id" id="user_id" class="easyui-combobox" required="true"></td>
			   </tr><tr>
					  <td width="34%" ><label><?php echo lang('department')?>:</label></td>
					  <td width="66%"><input name="department_id" id="department_id" class="easyui-combobox" required="true"></td>
			   </tr><input type="hidden" name="ud_id" id="ud_id"/>
	</table>
	</form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#user_department-search-form').form('clear');
			$('#user_department-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#user_department-table').datagrid({
				queryParams:{data:$('#user_department-search-form').serialize()}
				});
		});		
		$('#user_department-table').datagrid({
			url:'<?php  echo site_url('user_department/admin/user_department/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
		$('#department_id').combobox({
			url:'<?php echo site_url('department/admin/department/combo_json'); ?>',
			multiple: false,
			valueField: 'id',
			textField: 'name',
			panelHeight:'200',
			editable: false,
			onChange:function(category_id)
			{
				getMaterials(category_id);
			}

		});
		$('#user_id').combobox({
			url:'<?php echo site_url('auth/admin/members/combo_json'); ?>',
			multiple: false,
			valueField: 'id',
			textField: 'username',
			panelHeight:'200',
			editable: false,
			onChange:function(category_id)
			{
				getMaterials(category_id);
			}

		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning"  title="<?php  echo lang('edit_user_department')?>"><span class="l-btn-left">Edit</span></a>';
		var d = '<a href="#" onclick="removeuser_department('+index+')" class="btn btn-danger"  title="<?php  echo lang('delete_user_department')?>"><span class="l-btn-left">Delete</span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-user_department').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_user_department')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#user_department-table').datagrid('getRows')[index];
		if (row){
			$('#form-user_department').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_user_department')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeuser_department(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#user_department-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('user_department/admin/user_department/delete_json')?>', {id:[row.ud_id]}, function(){
					$('#user_department-table').datagrid('deleteRow', index);
					$('#user_department-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#user_department-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].ud_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('user_department/admin/user_department/delete_json')?>',{id:selected},function(data){
						$('#user_department-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-user_department').form('submit',{
			url: '<?php  echo site_url('user_department/admin/user_department/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-user_department').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#user_department-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>