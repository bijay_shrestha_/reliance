<?php


$lang['id'] = 'Id';
$lang['plant_id'] = 'Plant';
$lang['working_hour'] = 'Working Hour';

$lang['create_plant_operation']='Create/Edit Plant Operation';
$lang['edit_plant_operation']='Edit Plant Operation';
$lang['delete_plant_operation']='Delete Plant Operation';
$lang['plant_operation_search']='Plant Operation Search';

$lang['plant_operation']='Plant Operation';
$lang['date']='Date';
$lang['date_np']='Nepali Date';
$lang['date_en']='English Date';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

