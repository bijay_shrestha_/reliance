<?php
class Plant_operation_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('PLANT_OPERATION'=>$this->prefix.'plant_operation','PLANT'=>$this->prefix.'plant');
		$this->_JOINS=array('PLANT'=>array('join_type'=>'LEFT','join_field'=>'plant_operations.plant_id=plant.id',
                                           'select'=>'plant.name','alias'=>'plant'),
                           
                            );        
    }
    
    public function getPlantOperations($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''),$fields = NULL)
    {
        if($fields == NULL){
            $fields='plant_operations.*';
        }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['PLANT_OPERATION']. ' plant_operations');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['PLANT_OPERATION'].' plant_operations');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

    public function getReport($date)
    {
        $sql = "SELECT name, SUM(start_hour) AS start_hour, SUM(start_hour + working_hour) AS finish_hour, SUM(working_hour) AS working_hour FROM (
            SELECT
                name,
                0 AS start_hour,
                working_hour
            FROM
                (
                    `tbl_plant_operation` plant_operations
                )
            LEFT JOIN `tbl_plant` plant ON `plant_operations`.`plant_id` = `plant`.`id`
            WHERE
                `date_np` = '$date'
            GROUP BY
            `name`
            UNION
            SELECT
                name,
                SUM(working_hour) AS start_hour,
                0 AS working_hour
            FROM
                (
                    `tbl_plant_operation` plant_operations
                )
            LEFT JOIN `tbl_plant` plant ON `plant_operations`.`plant_id` = `plant`.`id`
            WHERE
                `date_np` < '$date'
            GROUP BY name
            ) AS report

            GROUP BY name";

        $data = $this->db->query($sql);
        return $data;
    }

    public function getOpening($date_en=NULL)
    {
        if($date_en != NULL){
            $sql = 'SELECT plant_id, SUM(working_hour) AS opening FROM tbl_plant_operation WHERE date_en < "' . $date_en .'" GROUP BY plant_id';
        }else{
            $sql = 'SELECT plant_id, SUM(working_hour) AS opening FROM tbl_plant_operation  GROUP BY plant_id';
        }
        $this->db->group_by('plant_id');
        $opening = $this->db->query($sql)->result_array();

        return $opening;
    }

}