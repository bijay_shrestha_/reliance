<?php

class Plant_operation extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('plant_operation','plant_operation_model');
        $this->load->module_model('plant','plant_model');
        $this->lang->module_load('plant_operation','plant_operation');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$date_en = $this->input->post('date_en');
		if($date_en == ''){
			$this->db->order_by('date_en','desc');
			$latest_input = $this->plant_operation_model->getPlantOperations()->row_array();
			if(count($latest_input) > 0){
				$date_en = $latest_input['date_en'];
			}else{
				$date_en = '0000-00-00';
			}
			if(count($latest_input)>0){
				$date_en = $latest_input['date_en'];
			}else{
				$date_en = date('Y-m-d');
			}
		}
		$previous_input = $this->get_previous_day_input($date_en);

		if(count($previous_input) > 0){
			$previous_day_data = $this->plant_operation_model->getPlantOperations(array('date_en'=>$previous_input['date_en']))->result_array();
		}

		$this->db->where('date_en', $date_en);
		$this->plant_operation_model->joins = array('PLANT');
		$data['rows'] = $this->plant_operation_model->getPlantOperations()->result_array();
		
		if(count($previous_input) > 0){
			foreach($data['rows'] as $key => $value){
				foreach ($previous_day_data as $i => $v) {
					if($value['plant_id'] == $v['plant_id']){
						$data['rows'][$key]['working_hour'] -= $v['working_hour'];
					}
				}
			}
		}

		$data['plants'] = $this->plant_model->getPlants()->result_array();
		// Display Page
		$data['header'] = 'Plant Operation';
		$data['view_page'] = "plant_operation/index";
		$data['module'] = 'plant_operation';
		$this->load->view($this->_container,$data);		
	}

	private function get_previous_day_input($date_en){
		$where['date_en < '] = $date_en;
		$this->db->order_by('date_en desc');
		$data = $this->plant_operation_model->getPlantOperations($where)->row_array();
		return $data;
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->plant_operation_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->plant_operation_model->getPlantOperations()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['plant_id']!='')?$this->db->where('plant_id',$params['search']['plant_id']):'';
($params['search']['working_hour']!='')?$this->db->like('working_hour',$params['search']['working_hour']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->plant_operation_model->getPlantOperations()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->plant_operation_model->delete('PLANT_OPERATION',array('id'=>$row));
            endforeach;
		}else{
			$data = $this->plant_operation_model->getPlantOperations(array('id' => $id))->row_array();
			$this->plant_operation_model->delete('PLANT_OPERATION',array('date_np'=>$data['date_np']));
		}
		echo json_encode(array('success'=>TRUE));
	}    

	public function save()
	{
		
        // print_r($this->input->post());
        $data=$this->_get_posted_data(); //Retrive Posted Data		
		// print_r($data);
		// exit;
		$this->plant_operation_model->delete('PLANT_OPERATION',array('date_np' => $data['date_np']));

		$val['date_np'] = $data['date_np'];
		$val['date_en'] = $data['date_en'];
		foreach ($data['working_hour'] as $key => $value){
			$val['plant_id'] = $key;
			$val['working_hour'] = $value;
			$this->plant_operation_model->insert('PLANT_OPERATION',$val);
		}

		$success = TRUE;

        // if(!$this->input->post('id'))
        // {
        //     $success=$this->plant_operation_model->insert('PLANT_OPERATION',$data);
        // }
        // else
        // {
        //     $success=$this->plant_operation_model->update('PLANT_OPERATION',$data,array('id'=>$data['id']));
        // }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['plant_id'] = $this->input->post('plant_id');
$data['working_hour'] = $this->input->post('working_hour');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');

        return $data;
   }
   

    public function get_value()
    {
        $where['id'] = $this->input->post('id');
        $val = $this->plant_operation_model->getPlantOperations($where)->row_array();

        $where_date['date_np'] = $val['date_np'];

        // $this->db->group_by('plant_id');
        // $fields = ('plant_id, SUM(working_hour) working_hour');
        // $this->db->group_by('plant_id');
        $date['date_np'] = $val['date_np'];
        $date['date_en'] = $val['date_en'];

        $data = $this->plant_operation_model->getPlantOperations($where_date)->result_array();
        $row = array('date' => $date,'data' => $data);

        echo json_encode($row);
    }
   	
	   
// 	   public function convert(){
// 	   	echo '<pre>';
// 	   		$this->db->order_by('id asc');
// 	   		$data = $this->plant_operation_model->getPlantOperations()->result_array();
// $val = array();
// 	   		foreach ($data as $key => $value) {
// 	   			if(array_key_exists($value['plant_id'], $val)){
// 	   				$val[$value['plant_id']]['working_hour'] += $value['working_hour'];
// 	   				echo $val[$value['plant_id']]['working_hour'].'<br>';
// 	   				$this->plant_operation_model->update('PLANT_OPERATION',$val[$value['plant_id']],array('id' => $value['id']));
// 	   			}else{
// 	   				$val[$value['plant_id']]['working_hour'] = $value['working_hour'];
// 	   				$this->plant_operation_model->update('PLANT_OPERATION',$val[$value['plant_id']],array('id' => $value['id']));
// 	   			}
// 	   		}
// 	   			print_r($val);

// 	   		echo $this->db->last_query();
// 	   		// print_r($data);exit;
// 	   } 
}