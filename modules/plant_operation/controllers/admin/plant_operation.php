<?php

class Plant_operation extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('plant_operation','plant_operation_model');
        $this->load->module_model('plant','plant_model');

        $this->lang->module_load('plant_operation','plant_operation');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['plants'] = $this->plant_model->getPlants()->result_array();

		// Display Page
		$data['header'] = 'plant_operation';
		$data['page'] = $this->config->item('template_admin') . "plant_operation/index";
		$data['module'] = 'plant_operation';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->plant_operation_model->count();
		paging('id');
		$this->_get_search_param();	
		$this->plant_operation_model->joins = array('PLANT');
		$rows=$this->plant_operation_model->getPlantOperations()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);

		if(!empty($params['search']))
		{
			($params['search']['plant_id']!='')?$this->db->where('plant_id',$params['search']['plant_id']):'';
($params['search']['working_hour']!='')?$this->db->like('working_hour',$params['search']['working_hour']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->plant_operation_model->getPlantOperations()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->plant_operation_model->delete('PLANT_OPERATION',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		// print_r($this->input->post());
        $data=$this->_get_posted_data(); //Retrive Posted Data		
		// print_r($data);
		// exit;
		$this->plant_operation_model->delete('PLANT_OPERATION',array('date_np' => $data['date_np']));

		$val['date_np'] = $data['date_np'];
		$val['date_en'] = $data['date_en'];
		foreach ($data['working_hour'] as $key => $value){
			$val['plant_id'] = $key;
			$val['working_hour'] = $value;
			$this->plant_operation_model->insert('PLANT_OPERATION',$val);
		}

		$success = TRUE;

        // if(!$this->input->post('id'))
        // {
        //     $success=$this->plant_operation_model->insert('PLANT_OPERATION',$data);
        // }
        // else
        // {
        //     $success=$this->plant_operation_model->update('PLANT_OPERATION',$data,array('id'=>$data['id']));
        // }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['plant_id'] = $this->input->post('plant_id');
$data['working_hour'] = $this->input->post('working_hour');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');

        return $data;
   }
   
   	public function get_data(){
   		$where['date_np'] = $this->input->post('date_np');
   		$data = $this->plant_operation_model->getPlantOperations($where)->result_array();
   		echo json_encode($data);
   	}
	    
}