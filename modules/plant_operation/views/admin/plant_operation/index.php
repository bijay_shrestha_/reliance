<!-- Nepali Datepicker -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/nepali.datepicker.v2.min.css" />

<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('plant_operation_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="plant_operation-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('plant_id')?></label>:</td>
<td>
<select class="easyui-validatebox" name="search[plant_id]">
<option value=""></option>
	<?php foreach($plants as $key => $value){?>
	<option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
	<?php }?>
</select>
<!-- <input type="text" name="search[plant_id]" id="search_plant_id"  class="easyui-combobox"/></td> -->
<td><label><?php echo lang('working_hour')?></label>:</td>
<td><input type="text" name="search[working_hour]" id="search_working_hour"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('date_np')?></label></td>
<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="plant_operation-table" data-options="pagination:true,title:'<?php  echo lang('plant_operation')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
    <th data-options="field:'date_np',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'name',sortable:true" width="50"><?php echo lang('plant_id')?></th>
<th data-options="field:'working_hour',sortable:true" width="50"><?php echo lang('working_hour')?></th>

    <!-- <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th> -->
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_plant_operation')?>"><?php  echo lang('create').'/Edit'?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_plant_operation')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit plant_operation form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-plant_operation" method="post" >
    <table>
    	<tr>
    		<td width="34%">
    			<label><?php echo lang('date')?>:</label>
    		</td>
    		<td width="66%">
    			<input type="text" name="date_np" id="date_np" class="" required="true">
    			<input type="hidden" name="date_en" id="date_en">
    			<script>
                    $('#date_np').change(function(){
                          $('#date_en').val(BS2AD($('#date_np').val()));
                          $('.working_hour').val('');
                          $.post('<?php echo site_url("plant_operation/admin/plant_operation/get_data")?>',{date_np:$('#date_np').val()},function(data){
			                  	$.each(data, function( index, value ) {
								  $('#working_hour-'+value.plant_id).val(value.working_hour);
								});
			              	},'json');
                     });
                </script>
    		</td>
    	</tr>
    	<?php
    	foreach ($plants as $key => $value) {?>
			<tr>
			            <tr>
			              <td width="34%" ><label><?php echo $value['name']?>:</label></td>
						  <td width="66%"><input name="working_hour[<?php echo $value['id']?>]" id="working_hour-<?php echo $value['id']?>" class="easyui-validatebox working_hour"></td>
    	<?php }?>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#plant_operation-search-form').form('clear');
			$('#plant_operation-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#plant_operation-table').datagrid({
				queryParams:{data:$('#plant_operation-search-form').serialize()}
				});
		});		
		$('#plant_operation-table').datagrid({
			url:'<?php  echo site_url('plant_operation/admin/plant_operation/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
		// nepali date picker
        $('#date_np').nepaliDatePicker({
			ndpEnglishInput: 'date_en',
			onChange: function(){
				$('.working_hour').val('');
				$.post('<?php echo site_url("plant_operation/admin/plant_operation/get_data")?>',{date_np:$('#date_np').val()},function(data){
                  	$.each(data, function( index, value ) {
                  		$('#working_hour-'+value.plant_id).val(value.working_hour);
								});
              	},'json');
			}
		});
		$('#search_date_np').nepaliDatePicker();
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_plant_operation')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeplant_operation('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_plant_operation')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-plant_operation').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_plant_operation')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#plant_operation-table').datagrid('getRows')[index];
		if (row){
			$('#form-plant_operation').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_plant_operation')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeplant_operation(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#plant_operation-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('plant_operation/admin/plant_operation/delete_json')?>', {id:[row.id]}, function(){
					$('#plant_operation-table').datagrid('deleteRow', index);
					$('#plant_operation-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#plant_operation-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('plant_operation/admin/plant_operation/delete_json')?>',{id:selected},function(data){
						$('#plant_operation-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-plant_operation').form('submit',{
			url: '<?php  echo site_url('plant_operation/admin/plant_operation/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-plant_operation').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#plant_operation-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>