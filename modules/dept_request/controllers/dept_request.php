<?php

class Dept_request extends Member_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->module_model('dept_request', 'dept_request_model');
        $this->lang->module_load('dept_request', 'dept_request');
        $this->load->module_model('inventory', 'inventory_model');
        $this->load->module_model('material_department', 'material_department_model');
        $this->load->module_model('stock', 'stock_model');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }

    public function index()
    {
        // Display Page
        $data['header'] = 'dept_request';
        $data['page'] = $this->config->item('template_admin') . "dept_request/index";
        $data['module'] = 'dept_request';
        $this->load->view($this->_container, $data);
    }

    public function showData()
    {
        $this->dept_request_model->joins = array('MATERIALS');
        $date = $this->input->post('inquiry-date');
        $data['data'] = $this->dept_request_model->getDeptRequests(array('request_np_date'=>$date))->result_array();
        echo json_encode($data);
    }

    public function showAllData()
    {
        $this->dept_request_model->joins = array('MATERIALS');
        $data['data'] = $this->dept_request_model->getDeptRequests()->result_array();
        echo json_encode($data);
    }

    public function save_demand()
    {
        $demands = $this->_get_posted_data();
        foreach($demands as $demand):
            $success = $this->dept_request_model->insert('DEPT_REQUEST', $demand);
        endforeach;

        if($success){
            $success = TRUE;
        }else{
            $success = FALSE;
        }
        echo json_encode($success);
    }

    public function approve_request()
    {
        $id = $this->input->post('id');
        $dept_english_date = $this->input->post('dept_english_date');
        $dept_nepali_date = $this->input->post('dept_nepali_date');
        $result = $this->dept_request_model->update('DEPT_REQUEST',array('department_status'=>1, 'dept_confirm_np_date'=>$dept_nepali_date, 'dept_confirm_en_date'=>$dept_english_date), array('request_id'=>$id));

        if($result){
            $session_department_id = $this->session->userdata('department_id');
            $request = $this->dept_request_model->getDeptRequests(array('request_id'=>$id))->row_array();
            $current_stock = $this->stock_model->getStocks(array('material_id'=>$request['material_id'],'department_id'=>$session_department_id))->row_array();
            if(count($current_stock)>0){
            // echo '<pre>';print_r($current_stock);exit;
                $new_stock =$current_stock['amount'] + $request['accepted_quantity'];
                $this->stock_model->update('STOCK',array('amount'=>$new_stock),array('stock_id' =>$current_stock['stock_id']));
            }else{
                // echo '<pre>';print_r('here');exit;
                $stock['amount'] = $request['accepted_quantity'];
                $stock['material_id'] = $request['material_id'];
                $stock['department_id'] = $session_department_id;
                $this->stock_model->insert('STOCK',$stock);
                $stock_id = $this->db->insert_id();
                $this->material_department_model->update('MATERIAL_DEPARTMENTS',array('stock_id'=>$stock_id),array('material_id'=>$stock['material_id'],'group_id'=>$stock['department_id']));
            }
            $success = TRUE;
        }else{
            $success = FALSE;
        }
        echo json_encode($success);
    }

    public function disapprove_request()
    {
        $id = $this->input->post('id');
        $dept_english_date = $this->input->post('dept_english_date');
        $dept_nepali_date = $this->input->post('dept_nepali_date');
        $request = $this->dept_request_model->getDeptRequests(array('request_id' => $id))->row_array();

        $result = $this->dept_request_model->update('DEPT_REQUEST',array('department_status'=>2, 'dept_confirm_np_date'=>$dept_nepali_date, 'dept_confirm_en_date'=>$dept_english_date), array('request_id'=>$id));

        if($result){
            $success = TRUE;
            $stock = $this->inventory_model->getInventories(array('material_id' => $request['material_id']))->row_array();
            $new_stock = $stock['current_stock'] + $request['accepted_quantity'];
            $this->inventory_model->update('INVENTORY',array('current_stock' => $new_stock),array('material_id'=>$request['material_id']));
        }else{
            $success = FALSE;
        }
        echo json_encode($success);
    }

    private function _get_posted_data()
    {
        $data = array();
        $department_ids=$this->session->userdata('department_id');
        $material_ids=$this->input->post('material_id');
        $request_quantities=$this->input->post('quantity');
        $request_np_dates=$this->input->post('nepali_date');
        $request_en_dates=$this->input->post('english_date');

        $i=0;
        foreach($material_ids as $material_id){
            $data[$i]['material_id'] = $material_id;
            $data[$i]['department_id'] = $this->session->userdata('department_id');
            $data[$i]['request_quantity'] = $request_quantities[$i];
            $data[$i]['request_np_date'] = $request_np_dates[$i];
            $data[$i]['request_en_date'] = $request_en_dates[$i];
            $i++;
        }
        return $data;
    }
    public function getRequest(){
        $id = $this->input->post('id');
        $this->dept_request_model->joins = array('MATERIALS');
        $data = $this->dept_request_model->getDeptRequests(array('request_id'=>$id))->row_array();
        echo json_encode($data);
    }

    // edit request
    public function editRequest(){
        $id = $this->input->post('request_id');
        $data['material_id'] = $this->input->post('material_id');
        $data['request_quantity'] = $this->input->post('requested_qty');
        $data['request_np_date'] = $this->input->post('edit_requested_date');
        $data['request_en_date'] = $this->input->post('edit_requested_en_date');

        $success = $this->dept_request_model->update('DEPT_REQUEST',$data,array('request_id'=>$id));
        echo json_encode($success);
    }
    // delete request
    public function deleteRequest(){
        $id = $this->input->post('id');
        $data = $this->dept_request_model->getDeptRequests(array('request_id'=>$id))->row_array();
        if($data['store_status'] == 0){
            $success = $this->dept_request_model->delete('DEPT_REQUEST',array('request_id'=>$id));
        }else{
            $success = 2;
        }

        echo json_encode($success);
    }
}