<?php

class Dept_request extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('dept_request','dept_request_model');
        $this->lang->module_load('dept_request','dept_request');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'dept_request';
		$data['page'] = $this->config->item('template_admin') . "dept_request/index";
		$data['module'] = 'dept_request';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->dept_request_model->count();
		paging('request_id');
		$this->_get_search_param();	
		$rows=$this->dept_request_model->getDeptRequests()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['request_quantity']!='')?$this->db->where('request_quantity',$params['search']['request_quantity']):'';
($params['search']['request_np_date']!='')?$this->db->like('request_np_date',$params['search']['request_np_date']):'';
($params['search']['request_en_date']!='')?$this->db->like('request_en_date',$params['search']['request_en_date']):'';
($params['search']['store_status']!='')?$this->db->where('store_status',$params['search']['store_status']):'';
($params['search']['store_confirm_np_date']!='')?$this->db->like('store_confirm_np_date',$params['search']['store_confirm_np_date']):'';
($params['search']['store_confirm_en_date']!='')?$this->db->like('store_confirm_en_date',$params['search']['store_confirm_en_date']):'';
($params['search']['department_status']!='')?$this->db->where('department_status',$params['search']['department_status']):'';
($params['search']['dept_confirm_np_date']!='')?$this->db->like('dept_confirm_np_date',$params['search']['dept_confirm_np_date']):'';
($params['search']['dept_confirm_en_date']!='')?$this->db->like('dept_confirm_en_date',$params['search']['dept_confirm_en_date']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->dept_request_model->getDeptRequests()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->dept_request_model->delete('DEPT_REQUEST',array('request_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('request_id'))
        {
            $success=$this->dept_request_model->insert('DEPT_REQUEST',$data);
        }
        else
        {
            $success=$this->dept_request_model->update('DEPT_REQUEST',$data,array('request_id'=>$data['request_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['request_id'] = $this->input->post('request_id');
$data['material_id'] = $this->input->post('material_id');
$data['department_id'] = $this->input->post('department_id');
$data['request_quantity'] = $this->input->post('request_quantity');
$data['request_np_date'] = $this->input->post('request_np_date');
$data['request_en_date'] = $this->input->post('request_en_date');
$data['store_status'] = $this->input->post('store_status');
$data['store_confirm_np_date'] = $this->input->post('store_confirm_np_date');
$data['store_confirm_en_date'] = $this->input->post('store_confirm_en_date');
$data['department_status'] = $this->input->post('department_status');
$data['dept_confirm_np_date'] = $this->input->post('dept_confirm_np_date');
$data['dept_confirm_en_date'] = $this->input->post('dept_confirm_en_date');

        return $data;
   }
   
   	
	    
}