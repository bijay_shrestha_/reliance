<?php
class Dept_request_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('DEPT_REQUEST'=>$this->prefix.'dept_request','MATERIALS'=>$this->prefix.'materials','DEPARTMENTS'=>$this->prefix.'departments','INVENTORY'=>$this->prefix.'inventory');
		$this->_JOINS=array('MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'dept_requests.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
                            'DEPARTMENTS'=>array('join_type'=>'LEFT','join_field'=>'dept_requests.department_id=departments.id',
                                           'select'=>'departments.*','alias'=>'departments'),
                            'INVENTORY'=>array('join_type'=>'LEFT','join_field'=>'dept_requests.material_id=inventory.material_id',
                                           'select'=>'inventory.current_stock as current','alias'=>'inventory'),
                           
                            );        
    }
    
    public function getDeptRequests($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''), $fields = NULL)
    {
        if($fields == NULL){
            $fields='dept_requests.*';
        }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['DEPT_REQUEST']. ' dept_requests');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['DEPT_REQUEST'].' dept_requests');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
    // get all dispatch
    public function getAllDispatches($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='dept_requests.*, SUM(accepted_quantity) as all_dispatched';
       
        foreach($this->joins as $key):
            $fields=$fields . ','.$this->_JOINS[$key]['select'];
        endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['DEPT_REQUEST']. ' dept_requests');
        
        foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;         
        
        (! is_null($where))?$this->db->where($where):NULL;
        (! is_null($order_by))?$this->db->order_by($order_by):NULL;

        if( ! is_null($limit['limit']))
        {
            $this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
        }
        return $this->db->get();        
    }

    // get dispatch count daywise
    public function getDaywiseCount($where=NULL){
        $fields='COUNT(dept_requests.request_id) as total';
       
        foreach($this->joins as $key):
            $fields=$fields . ','.$this->_JOINS[$key]['select'];
        endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['DEPT_REQUEST']. ' dept_requests');
        
        foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;         
        
        (! is_null($where))?$this->db->where($where):NULL;

        $total_array = $this->db->get()->result_array();
        $days = 0;
        foreach($total_array as $array){
            // $days += $array['total'];
            $days += 1;
        }
        return $days;
    }

}