<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('dept_request_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="dept_request-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('material_id')?></label>:</td>
<td><input type="text" name="search[material_id]" id="search_material_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('department_id')?></label>:</td>
<td><input type="text" name="search[department_id]" id="search_department_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('request_quantity')?></label>:</td>
<td><input type="text" name="search[request_quantity]" id="search_request_quantity"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('request_np_date')?></label>:</td>
<td><input type="text" name="search[request_np_date]" id="search_request_np_date"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('request_en_date')?></label>:</td>
<td><input type="text" name="search[request_en_date]" id="search_request_en_date"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('store_status')?></label>:</td>
<td><input type="text" name="search[store_status]" id="search_store_status"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('store_confirm_np_date')?></label>:</td>
<td><input type="text" name="search[store_confirm_np_date]" id="search_store_confirm_np_date"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('store_confirm_en_date')?></label>:</td>
<td><input type="text" name="search[store_confirm_en_date]" id="search_store_confirm_en_date"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('department_status')?></label>:</td>
<td><input type="text" name="search[department_status]" id="search_department_status"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('dept_confirm_np_date')?></label>:</td>
<td><input type="text" name="search[dept_confirm_np_date]" id="search_dept_confirm_np_date"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('dept_confirm_en_date')?></label>:</td>
<td><input type="text" name="search[dept_confirm_en_date]" id="search_dept_confirm_en_date"  class="easyui-validatebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="dept_request-table" data-options="pagination:true,title:'<?php  echo lang('dept_request')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'request_id',sortable:true" width="30"><?php echo lang('request_id')?></th>
<th data-options="field:'material_id',sortable:true" width="50"><?php echo lang('material_id')?></th>
<th data-options="field:'department_id',sortable:true" width="50"><?php echo lang('department_id')?></th>
<th data-options="field:'request_quantity',sortable:true" width="50"><?php echo lang('request_quantity')?></th>
<th data-options="field:'request_np_date',sortable:true" width="50"><?php echo lang('request_np_date')?></th>
<th data-options="field:'request_en_date',sortable:true" width="50"><?php echo lang('request_en_date')?></th>
<th data-options="field:'store_status',sortable:true,formatter:formatStatus" width="30" align="center"><?php echo lang('store_status')?></th>
<th data-options="field:'store_confirm_np_date',sortable:true" width="50"><?php echo lang('store_confirm_np_date')?></th>
<th data-options="field:'store_confirm_en_date',sortable:true" width="50"><?php echo lang('store_confirm_en_date')?></th>
<th data-options="field:'department_status',sortable:true,formatter:formatStatus" width="30" align="center"><?php echo lang('department_status')?></th>
<th data-options="field:'dept_confirm_np_date',sortable:true" width="50"><?php echo lang('dept_confirm_np_date')?></th>
<th data-options="field:'dept_confirm_en_date',sortable:true" width="50"><?php echo lang('dept_confirm_en_date')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_dept_request')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_dept_request')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit dept_request form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-dept_request" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('material_id')?>:</label></td>
					  <td width="66%"><input name="material_id" id="material_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('department_id')?>:</label></td>
					  <td width="66%"><input name="department_id" id="department_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('request_quantity')?>:</label></td>
					  <td width="66%"><input name="request_quantity" id="request_quantity" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('request_np_date')?>:</label></td>
					  <td width="66%"><input name="request_np_date" id="request_np_date" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('request_en_date')?>:</label></td>
					  <td width="66%"><input name="request_en_date" id="request_en_date" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('store_status')?>:</label></td>
					  <td width="66%"><input name="store_status" id="store_status" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('store_confirm_np_date')?>:</label></td>
					  <td width="66%"><input name="store_confirm_np_date" id="store_confirm_np_date" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('store_confirm_en_date')?>:</label></td>
					  <td width="66%"><input name="store_confirm_en_date" id="store_confirm_en_date" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('department_status')?>:</label></td>
					  <td width="66%"><input name="department_status" id="department_status" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('dept_confirm_np_date')?>:</label></td>
					  <td width="66%"><input name="dept_confirm_np_date" id="dept_confirm_np_date" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('dept_confirm_en_date')?>:</label></td>
					  <td width="66%"><input name="dept_confirm_en_date" id="dept_confirm_en_date" class="easyui-validatebox" required="true"></td>
		       </tr><input type="hidden" name="request_id" id="request_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#dept_request-search-form').form('clear');
			$('#dept_request-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#dept_request-table').datagrid({
				queryParams:{data:$('#dept_request-search-form').serialize()}
				});
		});		
		$('#dept_request-table').datagrid({
			url:'<?php  echo site_url('dept_request/admin/dept_request/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_dept_request')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removedept_request('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_dept_request')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-dept_request').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_dept_request')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#dept_request-table').datagrid('getRows')[index];
		if (row){
			$('#form-dept_request').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_dept_request')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removedept_request(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#dept_request-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('dept_request/admin/dept_request/delete_json')?>', {id:[row.request_id]}, function(){
					$('#dept_request-table').datagrid('deleteRow', index);
					$('#dept_request-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#dept_request-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].request_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('dept_request/admin/dept_request/delete_json')?>',{id:selected},function(data){
						$('#dept_request-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-dept_request').form('submit',{
			url: '<?php  echo site_url('dept_request/admin/dept_request/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-dept_request').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#dept_request-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>