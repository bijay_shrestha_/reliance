<?php


$lang['request_id'] = 'Request Id';
$lang['material_id'] = 'Material Id';
$lang['department_id'] = 'Department Id';
$lang['request_quantity'] = 'Request Quantity';
$lang['request_np_date'] = 'Request Np Date';
$lang['request_en_date'] = 'Request En Date';
$lang['store_status'] = 'Store Status';
$lang['store_confirm_np_date'] = 'Store Confirm Np Date';
$lang['store_confirm_en_date'] = 'Store Confirm En Date';
$lang['department_status'] = 'Department Status';
$lang['dept_confirm_np_date'] = 'Dept Confirm Np Date';
$lang['dept_confirm_en_date'] = 'Dept Confirm En Date';

$lang['create_dept_request']='Create Dept Request';
$lang['edit_dept_request']='Edit Dept Request';
$lang['delete_dept_request']='Delete Dept Request';
$lang['dept_request_search']='Dept Request Search';

$lang['dept_request']='Dept Request';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

