<?php

class Report_type extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('report_type','report_type_model');
        $this->load->module_model('material','material_model');

        $this->lang->module_load('report_type','report_type');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['report_types'] = $this->report_type_model->getReportTypes()->result_array();
		$this->db->order_by('material_name','asc');
        $data['materials'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
		// Display Page
		$data['header'] = 'Report Type';
		$data['view_page'] = "report_type/index";
		$data['module'] = 'report_type';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->report_type_model->count();
		paging('rt_id');
		$this->_get_search_param();	
		$rows=$this->report_type_model->getReportTypes()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['type']!='')?$this->db->like('type',$params['search']['type']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->report_type_model->getReportTypes()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->report_type_model->delete('REPORT_TYPE',array('rt_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('rt_id'))
        {
            $success=$this->report_type_model->insert('REPORT_TYPE',$data);
        }
        else
        {
            $success=$this->report_type_model->update('REPORT_TYPE',$data,array('rt_id'=>$data['rt_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['rt_id'] = $this->input->post('rt_id');
$data['type'] = $this->input->post('type');

        return $data;
   }
   // for edit form
   public function getReportType(){
   		$where['rt_id'] = $this->input->post('id');
   		$data=$this->report_type_model->getReportTypes($where)->row_array();
   		
   		echo json_encode($data);
   }
   // saving data
    public function add() {

        $data = $this->_get_posted_data(); //Retrive Posted Data

        if($data['type'] == ''){
            $success = FALSE;
        }else{
            if(!$this->input->post('rt_id'))
	        {
	            $success=$this->report_type_model->insert('REPORT_TYPE',$data);
	        }
	        else
	        {
	            $success=$this->report_type_model->update('REPORT_TYPE',$data,array('rt_id'=>$data['rt_id']));
	        }
        }

        if ($success) {
            $success = TRUE;
            $msg = lang('success_message');
        } else {
            $success = FALSE;
            $msg = 'Type is required';
        }

        echo json_encode(array('msg' => $msg, 'success' => $success));
    }
   	
	    
}