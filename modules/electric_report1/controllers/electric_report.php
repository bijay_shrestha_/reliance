<?php

class Electric_report extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('electric_report','electric_report_model');
        $this->lang->module_load('electric_report','electric_report');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['rows'] = $this->electric_report_model->getElectricReports()->result_array();
		// Display Page
		$data['header'] = 'electric_report';
		$data['view_page'] = "electric_report/index";
		$data['module'] = 'electric_report';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->electric_report_model->count();
		paging('');
		$this->_get_search_param();	
		$rows=$this->electric_report_model->getElectricReports()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['id']!='')?$this->db->where('id',$params['search']['id']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['r1']!='')?$this->db->where('r1',$params['search']['r1']):'';
($params['search']['r2']!='')?$this->db->where('r2',$params['search']['r2']):'';
($params['search']['r3']!='')?$this->db->where('r3',$params['search']['r3']):'';
($params['search']['total']!='')?$this->db->where('total',$params['search']['total']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->electric_report_model->getElectricReports()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->electric_report_model->delete('ELECTRIC_REPORT',array(''=>$row));
            endforeach;
		}elseif($id){
			$success = $this->electric_report_model->delete('ELECTRIC_REPORT',array('id'=>$id));
			if($success){
				echo json_encode(array('success'=>$success, 'msg'=>'Record deleted'));
			}else{
				echo json_encode(array('success'=>$success, 'msg'=>'Record is not deleted. Please try again.'));
			}
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		
        // echo '<pre>';
        // print_r($this->input->post());
        // print_r($data);
        // exit;
        if(!$this->input->post('id'))
        {
            $success=$this->electric_report_model->insert('ELECTRIC_REPORT',$data);
        }
        else
        {
            $success=$this->electric_report_model->update('ELECTRIC_REPORT',$data,array('id'=>$data['id']));
        }
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['r1'] = $this->input->post('r1');
$data['r2'] = $this->input->post('r2');
$data['r3'] = $this->input->post('r3');
$data['total'] = $data['r1']+$data['r2']+$data['r3'];

        return $data;
   }
   	public function getData()
   	{
   		$id = $this->input->post('id');
   		$where['id'] = $id;
   		$data = $this->electric_report_model->getElectricReports($where)->row_array();
   		echo json_encode($data);
   	}
   	
	    
}