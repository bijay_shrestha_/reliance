<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('electric_report_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="electric_report-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('id')?></label>:</td>
<td><input type="text" name="search[id]" id="search_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('date_np')?></label>:</td>
<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('date_en')?></label>:</td>
<td><input type="text" name="date[date_en][from]" id="search_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_en][to]" id="search_date_en_to"  class="easyui-datebox"/></td>
<td><label><?php echo lang('r1')?></label>:</td>
<td><input type="text" name="search[r1]" id="search_r1"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('r2')?></label>:</td>
<td><input type="text" name="search[r2]" id="search_r2"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('r3')?></label>:</td>
<td><input type="text" name="search[r3]" id="search_r3"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('total')?></label>:</td>
<td><input type="text" name="search[total]" id="search_total"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="electric_report-table" data-options="pagination:true,title:'<?php  echo lang('electric_report')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="50"><?php echo lang('id')?></th>
<th data-options="field:'date_np',sortable:true" width="50"><?php echo lang('date_np')?></th>
<th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th>
<th data-options="field:'r1',sortable:true" width="50"><?php echo lang('r1')?></th>
<th data-options="field:'r2',sortable:true" width="50"><?php echo lang('r2')?></th>
<th data-options="field:'r3',sortable:true" width="50"><?php echo lang('r3')?></th>
<th data-options="field:'total',sortable:true" width="50"><?php echo lang('total')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_electric_report')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_electric_report')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit electric_report form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-electric_report" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('id')?>:</label></td>
					  <td width="66%"><input name="id" id="id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('r1')?>:</label></td>
					  <td width="66%"><input name="r1" id="r1" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('r2')?>:</label></td>
					  <td width="66%"><input name="r2" id="r2" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('r3')?>:</label></td>
					  <td width="66%"><input name="r3" id="r3" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('total')?>:</label></td>
					  <td width="66%"><input name="total" id="total" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="" id=""/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#electric_report-search-form').form('clear');
			$('#electric_report-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#electric_report-table').datagrid({
				queryParams:{data:$('#electric_report-search-form').serialize()}
				});
		});		
		$('#electric_report-table').datagrid({
			url:'<?php  echo site_url('electric_report/admin/electric_report/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_electric_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeelectric_report('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_electric_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-electric_report').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_electric_report')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#electric_report-table').datagrid('getRows')[index];
		if (row){
			$('#form-electric_report').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_electric_report')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeelectric_report(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#electric_report-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('electric_report/admin/electric_report/delete_json')?>', {id:[row.]}, function(){
					$('#electric_report-table').datagrid('deleteRow', index);
					$('#electric_report-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#electric_report-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('electric_report/admin/electric_report/delete_json')?>',{id:selected},function(data){
						$('#electric_report-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-electric_report').form('submit',{
			url: '<?php  echo site_url('electric_report/admin/electric_report/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-electric_report').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#electric_report-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>