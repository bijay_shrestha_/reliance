<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('report_list_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="report_list-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('name')?></label>:</td>
<td><input type="text" name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="btn btn-flat btn-xs btn-success" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="btn btn-flat btn-xs btn-warning" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="report_list-table" data-options="pagination:true,title:'<?php  echo lang('report_list')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'name',sortable:true" width="50"><?php echo lang('name')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="btn btn-flat btn-xs btn-primary" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_report_list')?>"><?php  echo lang('create')?></a>
    <a href="#" class="btn btn-flat btn-xs btn-danger" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_report_list')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit report_list form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-report_list" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('name')?>:</label></td>
					  <td width="66%"><input name="name" id="name" class="easyui-validatebox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="btn btn-flat btn-xs btn-success" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="btn btn-flat btn-xs btn-danger" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#report_list-search-form').form('clear');
			$('#report_list-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#report_list-table').datagrid({
				queryParams:{data:$('#report_list-search-form').serialize()}
				});
		});		
		$('#report_list-table').datagrid({
			url:'<?php  echo site_url('report_list/admin/report_list/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-flat btn-xs btn-success" iconcls="icon-edit"  title="<?php  echo lang('edit_report_list')?>"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a>';
		var d = '<a href="#" onclick="removereport_list('+index+')" class="btn btn-flat btn-xs btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_report_list')?>"><span><i class="fa fa-times" aria-hidden="true"></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-report_list').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_report_list')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#report_list-table').datagrid('getRows')[index];
		if (row){
			$('#form-report_list').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_report_list')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removereport_list(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#report_list-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('report_list/admin/report_list/delete_json')?>', {id:[row.id]}, function(){
					$('#report_list-table').datagrid('deleteRow', index);
					$('#report_list-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#report_list-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('report_list/admin/report_list/delete_json')?>',{id:selected},function(data){
						$('#report_list-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-report_list').form('submit',{
			url: '<?php  echo site_url('report_list/admin/report_list/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-report_list').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#report_list-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>