<?php
class Direct_dispatch_material_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('DIRECT_DISPATCH_MATERIAL'=>$this->prefix.'direct_dispatch_material','DEPARTMENTS'=>$this->prefix.'departments','MATERIALS'=>$this->prefix.'materials');
		$this->_JOINS=array('DEPARTMENTS'=>array('join_type'=>'LEFT','join_field'=>'direct_dispatch_materials.department_id=departments.id',
                                           'select'=>'departments.*','alias'=>'departments'),
                            'MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'direct_dispatch_materials.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
                           
                            );        
    }
    
    public function getDirectDispatchMaterials($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='direct_dispatch_materials.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['DIRECT_DISPATCH_MATERIAL']. ' direct_dispatch_materials');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['DIRECT_DISPATCH_MATERIAL'].' direct_dispatch_materials');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}