<?php

class Direct_dispatch_material extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('direct_dispatch_material','direct_dispatch_material_model');
        $this->load->module_model('department','department_model');
        $this->load->module_model('material','material_model');
        $this->load->module_model('inventory','inventory_model');
        $this->load->module_model('dept_request','dept_request_model');
        $this->load->module_model('purchase','purchase_model');

        $this->lang->module_load('direct_dispatch_material','direct_dispatch_material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['materials'] = $this->material_model->getMaterials()->result_array();
		$data['departments'] = $this->department_model->getdepartments()->result_array();

		// Display Page
		$data['header'] = 'Dispatch Material';
		$data['view_page'] = "direct_dispatch_material/index";
		$data['module'] = 'direct_dispatch_material';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->direct_dispatch_material_model->count();
		paging('ddm_id');
		$this->_get_search_param();	
		$rows=$this->direct_dispatch_material_model->getDirectDispatchMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->direct_dispatch_material_model->getDirectDispatchMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->direct_dispatch_material_model->delete('DIRECT_DISPATCH_MATERIAL',array('ddm_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{

        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('ddm_id'))
        {
            $success=$this->direct_dispatch_material_model->insert('DIRECT_DISPATCH_MATERIAL',$data);
        }
        else
        {
            $success=$this->direct_dispatch_material_model->update('DIRECT_DISPATCH_MATERIAL',$data,array('ddm_id'=>$data['ddm_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['ddm_id'] = $this->input->post('ddm_id');
$data['department_id'] = $this->input->post('department_id');
$data['material_id'] = $this->input->post('material_id');

        return $data;
   }
   // get material od selected department
   	public function getformmaterial(){
   		$where['department_id'] = $this->input->post('id');
   		$result = $this->direct_dispatch_material_model->getDirectDispatchMaterials($where)->result_array();
   		echo json_encode($result);
   	}
//save material and department
   		public function savemd(){
   			// print_r($this->input->post());exit;
   		$data['department_id'] = $this->input->post('department_id');
   		$msg = 'Select Department';
		$success = FALSE;
   		if($data['department_id'] == ''){
   			$msg = 'Select Department';
			$success = FALSE;
   		}else{
   			if($this->input->post('material')){
   				$material_ids = $this->input->post('material'); 
   				$this->direct_dispatch_material_model->delete('DIRECT_DISPATCH_MATERIAL',(array('department_id'=>$data['department_id'])));
   				foreach($material_ids as $material_id){
   					$data['material_id'] = $material_id;
   					$success = $this->direct_dispatch_material_model->insert('DIRECT_DISPATCH_MATERIAL',$data);
   				}
   				$msg = 'Successfully Added';
   			}else {
   				$msg = 'Select Material';
   				$success = FALSE;
   			}
   		}
   		echo json_encode(array('msg'=>$msg,'success'=>$success));
   	}
   	//material for direct cispatch
   	public function getMaterial(){
   		$this->db->order_by('material_name','asc');
   		$where['department_id'] = $this->input->post('id');
   		$this->direct_dispatch_material_model->joins = array('MATERIALS');
   		$result = $this->direct_dispatch_material_model->getDirectDispatchMaterials($where)->result_array();

// echo $this->db->last_query();

   		echo json_encode($result);
   	}
   	//material stock
   	public function get_stock(){
   		$where['material_id'] = $this->input->post('material_id');
   		$total_purchase = $this->purchase_model->getAllPurchases($where)->row_array();
   		$where['direct_dispatch'] = 1;
   		$total_dispatch = $this->dept_request_model->getAllDispatches($where)->row_array();

   		// print_r($total_dispatch);exit;
   		// $result = $this->inventory_model->getInventories($where)->row_array();
   		$result['current_stock'] = number_format($total_purchase['total_purchase'] - $total_dispatch['all_dispatched'],3);

   		echo json_encode($result);
   	}
	    
    public function savedd()// dd =>direct dispatch
	{
// echo '<pre>';
// print_r($this->input->post());
        $datas=$this->_get_posted_form_data(); //Retrive Posted Data
//         foreach($datas as $data){
//         	echo $data['current_stock'] - $data['accepted_quantity'].'<br>';
//         }		
// print_r($datas);exit;
		foreach($datas as $data){
	        if(!$this->input->post('ddm_id'))
	        {
	            $success=$this->dept_request_model->insert('DEPT_REQUEST',$data);
	            if($success){
	            	$stock['current_stock'] = $data['current_stock'] - $data['accepted_quantity'];
	            	$this->inventory_model->update('INVENTORY',$stock,array('material_id'=>$data['material_id']));
	            }
	        }
	        else
	        {
	            $success=$this->direct_dispatch_material_model->update('DIRECT_DISPATCH_MATERIAL',$data,array('ddm_id'=>$data['ddm_id']));
	        }
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
	private function _get_posted_form_data(){

        $data = array();
        $department_ids=$this->input->post('department_id');
        $material_ids=$this->input->post('material_id');
        $request_quantities=$this->input->post('added_qty');
        $request_np_dates=$this->input->post('date_np');
        $request_en_dates=$this->input->post('date_en');
        $current_stocks=$this->input->post('current_stock');

        $i=0;
        foreach($department_ids as $department_id){
        	if($request_quantities[$i] != '' &&  $request_en_dates[$i] != '' && $current_stocks[$i] != '' && $department_ids[$i] != ''){
	            $data[$i]['material_id'] = $material_ids[$i];
	            $data[$i]['department_id'] = $department_id;
	            $data[$i]['accepted_quantity'] = str_replace(',', '', $request_quantities[$i]);
	            $data[$i]['store_confirm_np_date'] = $data[$i]['dept_confirm_np_date'] = $request_np_dates[$i];
	            $data[$i]['store_confirm_en_date'] = $data[$i]['dept_confirm_en_date'] = $request_en_dates[$i];
	            $data[$i]['current_stock'] = str_replace(',', '', $current_stocks[$i]);
	            $data[$i]['direct_dispatch'] = 1;
	            $data[$i]['store_status'] = 1;
	            $data[$i]['department_status'] = 1;
	            $i++;
        	}
        }
        return $data;
	}
	public function addRow(){
		$data['i'] = $this->input->post('i')+1;

		//department for direct dispatch
      $this->db->group_by('department_id');
      $this->direct_dispatch_material_model->joins = array('DEPARTMENTS');
      $data['departments'] = $this->direct_dispatch_material_model->getDirectDispatchMaterials()->result_array();

		// Display Page
		$data['header'] = 'Direct Dispatch Material';
		$data['view_page'] = "direct_dispatch_material/add";
		$data['module'] = 'direct_dispatch_material';
		$this->load->view($data['view_page'],$data);

	}
	public function getStock(){
		$where['material_id'] = $this->input->post('id');
		$data = $this->inventory_model->getInventories($where)->row_array();
		$data['current_stock'] = number_format($data['current_stock'],3);
		echo json_encode($data);
	}

	// departments assigned to material
	public function getDepartmentsToMaterial()
	{
		$where['material_id'] = $this->input->post('id');
		$this->direct_dispatch_material_model->joins = array('DEPARTMENTS');
		$data['departments'] = $this->direct_dispatch_material_model->getDirectDispatchMaterials($where)->result_array();
		// echo json_encode($data);
		$this->load->view('direct_dispatch_material/department_list',$data);
	}
}