<?php

class Direct_dispatch_material extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('direct_dispatch_material','direct_dispatch_material_model');
        $this->lang->module_load('direct_dispatch_material','direct_dispatch_material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'direct_dispatch_material';
		$data['page'] = $this->config->item('template_admin') . "direct_dispatch_material/index";
		$data['module'] = 'direct_dispatch_material';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->direct_dispatch_material_model->count();
		paging('ddm_id');
		$this->_get_search_param();	
		$rows=$this->direct_dispatch_material_model->getDirectDispatchMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->direct_dispatch_material_model->getDirectDispatchMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->direct_dispatch_material_model->delete('DIRECT_DISPATCH_MATERIAL',array('ddm_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('ddm_id'))
        {
            $success=$this->direct_dispatch_material_model->insert('DIRECT_DISPATCH_MATERIAL',$data);
        }
        else
        {
            $success=$this->direct_dispatch_material_model->update('DIRECT_DISPATCH_MATERIAL',$data,array('ddm_id'=>$data['ddm_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['ddm_id'] = $this->input->post('ddm_id');
$data['department_id'] = $this->input->post('department_id');
$data['material_id'] = $this->input->post('material_id');

        return $data;
   }
   
   	
	    
}