<?php

class Department_materialproperty extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('department_materialproperty','department_materialproperty_model');
        $this->lang->module_load('department_materialproperty','department_materialproperty');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'department_materialproperty';
		$data['page'] = $this->config->item('template_admin') . "department_materialproperty/index";
		$data['module'] = 'department_materialproperty';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->department_materialproperty_model->count();
		paging('dmp_id');
		$this->_get_search_param();	
		$rows=$this->department_materialproperty_model->getDepartmentMaterialproperties()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['material_property']!='')?$this->db->like('material_property',$params['search']['material_property']):'';
($params['search']['order']!='')?$this->db->where('order',$params['search']['order']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->department_materialproperty_model->getDepartmentMaterialproperties()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->department_materialproperty_model->delete('DEPARTMENT_MATERIALPROPERTIES',array('dmp_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('dmp_id'))
        {
            $success=$this->department_materialproperty_model->insert('DEPARTMENT_MATERIALPROPERTIES',$data);
        }
        else
        {
            $success=$this->department_materialproperty_model->update('DEPARTMENT_MATERIALPROPERTIES',$data,array('dmp_id'=>$data['dmp_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['dmp_id'] = $this->input->post('dmp_id');
$data['department_id'] = $this->input->post('department_id');
$data['material_property'] = $this->input->post('material_property');
$data['order'] = $this->input->post('order');

        return $data;
   }
   
   	
	    
}