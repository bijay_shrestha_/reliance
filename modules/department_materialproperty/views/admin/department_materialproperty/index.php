<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('department_materialproperty_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="department_materialproperty-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('department_id')?></label>:</td>
<td><input type="text" name="search[department_id]" id="search_department_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('material_property')?></label>:</td>
<td><input type="text" name="search[material_property]" id="search_material_property"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('order')?></label>:</td>
<td><input type="text" name="search[order]" id="search_order"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="department_materialproperty-table" data-options="pagination:true,title:'<?php  echo lang('department_materialproperty')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'dmp_id',sortable:true" width="30"><?php echo lang('dmp_id')?></th>
<th data-options="field:'department_id',sortable:true" width="50"><?php echo lang('department_id')?></th>
<th data-options="field:'material_property',sortable:true" width="50"><?php echo lang('material_property')?></th>
<th data-options="field:'order',sortable:true" width="50"><?php echo lang('order')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_department_materialproperty')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_department_materialproperty')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit department_materialproperty form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-department_materialproperty" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('department_id')?>:</label></td>
					  <td width="66%"><input name="department_id" id="department_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('material_property')?>:</label></td>
					  <td width="66%"><input name="material_property" id="material_property" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('order')?>:</label></td>
					  <td width="66%"><input name="order" id="order" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="dmp_id" id="dmp_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#department_materialproperty-search-form').form('clear');
			$('#department_materialproperty-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#department_materialproperty-table').datagrid({
				queryParams:{data:$('#department_materialproperty-search-form').serialize()}
				});
		});		
		$('#department_materialproperty-table').datagrid({
			url:'<?php  echo site_url('department_materialproperty/admin/department_materialproperty/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning" iconcls="icon-edit"  title="<?php  echo lang('edit_department_materialproperty')?>">Edit</a>';
		var d = '<a href="#" onclick="removedepartment_materialproperty('+index+')" class="btn btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_department_materialproperty')?>">Delete</a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-department_materialproperty').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_department_materialproperty')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#department_materialproperty-table').datagrid('getRows')[index];
		if (row){
			$('#form-department_materialproperty').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_department_materialproperty')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removedepartment_materialproperty(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#department_materialproperty-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('department_materialproperty/admin/department_materialproperty/delete_json')?>', {id:[row.dmp_id]}, function(){
					$('#department_materialproperty-table').datagrid('deleteRow', index);
					$('#department_materialproperty-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#department_materialproperty-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].dmp_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('department_materialproperty/admin/department_materialproperty/delete_json')?>',{id:selected},function(data){
						$('#department_materialproperty-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-department_materialproperty').form('submit',{
			url: '<?php  echo site_url('department_materialproperty/admin/department_materialproperty/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-department_materialproperty').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#department_materialproperty-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>