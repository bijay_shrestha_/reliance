<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('report_chemical_usage_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
			<form action="" method="post" id="report_chemical_usage-search-form">
				<table width="100%" cellspacing="1" cellpadding="1">
					<tr><td><label><?php echo lang('report_id')?></label>:</td>
						<td><input type="text" name="search[report_id]" id="search_report_id"  class="easyui-numberbox"/></td>
						<td><label><?php echo lang('chemical_id')?></label>:</td>
						<td><input type="text" name="search[chemical_id]" id="search_chemical_id"  class="easyui-numberbox"/></td>
					</tr>
					<tr>
						<td><label><?php echo lang('standard_amount')?></label>:</td>
						<td><input type="text" name="search[standard_amount]" id="search_standard_amount"  class="easyui-validatebox"/></td>
					</tr>
					<tr>
						<td colspan="4">
							<a href="javascript:void(0)" class="btn btn-xs btn-flat btn-success" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
							<a href="javascript:void(0)" class="btn btn-xs btn-flat btn-warning" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
						</td>
					</tr>
				</table>

			</form>
		</div>
		<br/>
		<table id="report_chemical_usage-table" data-options="pagination:true,title:'<?php  echo lang('report_chemical_usage')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
			<thead>
				<th data-options="field:'checkbox',checkbox:true"></th>
				<th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
				<th data-options="field:'report_name',sortable:true" width="50"><?php echo lang('report_name')?></th>
				<th data-options="field:'chemical_code',sortable:true" width="50"><?php echo lang('chemical_code')?></th>
				<th data-options="field:'standard_amount',sortable:true" width="50"><?php echo lang('standard_amount')?></th>

				<th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
			</thead>
		</table>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<a href="#" class="btn btn-xs btn-flat btn-primary" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_report_chemical_usage')?>"><?php  echo lang('create')?></a>
				<a href="#" class="btn btn-xs btn-flat btn-danger" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_report_chemical_usage')?>"><?php  echo lang('remove_selected')?></a>
			</p>

		</div> 

		<!--for create and edit report_chemical_usage form-->
		<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
		data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
		<form id="form-report_chemical_usage" method="post" >
			<table>
				<tr>
					<td width="34%" ><label><?php echo lang('report_id')?>:</label></td>
					<td width="66%"><input name="report_id" id="report_id" class="easyui-combobox" required="true" placeholder="Select Report"></td>
				</tr><tr>
					<td width="34%" ><label><?php echo lang('chemical_id')?>:</label></td>
					<td width="66%"><input name="chemical_id" id="chemical_id" class="easyui-combobox" required="true" placeholder="Select Chemicals"></td>
				</tr><tr id="duplicate" style="display: none">
					<td width="34%" ></td>
					<td width="66%" style="color: red"><h4>Duplicate Value</h4></td>
				</tr><tr>
					<td width="34%" ><label><?php echo lang('standard_amount')?>:</label></td>
					<td width="66%"><input name="standard_amount" id="standard_amount" class="easyui-validatebox" required="true"></td>
				</tr><input type="hidden" name="id" id="id"/>
			</table>
		</form>
		<div id="dlg-buttons">
			<a href="#" class="btn btn-xs btn-flat btn-success" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
			<a href="#" class="btn btn-xs btn-flat btn-danger" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
		</div>    
	</div>
	<!--div ends-->

</div>
</div>
<script language="javascript" type="text/javascript">
	var form_submit_checker = 0;
	$(function(){

		$('#report_id').combobox({
			url:'<?php echo site_url('report_chemical_usage/admin/report_chemical_usage/get_report_list')?>',
			valueField:'id',
			textField:'name'
		});

		$('#chemical_id').combobox({
			url:'<?php echo site_url('report_chemical_usage/admin/report_chemical_usage/get_chemicals')?>',
			valueField:'id',
			textField:'code',
			onChange: function(){
				var report_id = $('#report_id').combobox('getValue');
				var chemical_id = $('#chemical_id').combobox('getValue');
				$.post('<?php echo site_url('report_chemical_usage/admin/report_chemical_usage/check_duplicate'); ?>',{report_id:report_id , chemical_id : chemical_id },function(result)
				{
					if(result.checker == false)
					{
						form_submit_checker = 1;
						$('#duplicate').show();
					}
					else
					{
						form_submit_checker = 0;
						$('#duplicate').hide();
					}
				},'JSON');
			}	
		});

		$('#clear').click(function(){
			$('#report_chemical_usage-search-form').form('clear');
			$('#report_chemical_usage-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#report_chemical_usage-table').datagrid({
				queryParams:{data:$('#report_chemical_usage-search-form').serialize()}
			});
		});		
		$('#report_chemical_usage-table').datagrid({
			url:'<?php  echo site_url('report_chemical_usage/admin/report_chemical_usage/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-xs btn-flat btn-success" iconcls="icon-edit"  title="<?php  echo lang('edit_report_chemical_usage')?>"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a>';
		var d = '<a href="#" onclick="removereport_chemical_usage('+index+')" class="btn btn-xs btn-flat btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_report_chemical_usage')?>"><span><i class="fa fa-times" aria-hidden="true"></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-report_chemical_usage').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_report_chemical_usage')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#report_chemical_usage-table').datagrid('getRows')[index];
		if (row){
			$('#form-report_chemical_usage').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_report_chemical_usage')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	

	function removereport_chemical_usage(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#report_chemical_usage-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('report_chemical_usage/admin/report_chemical_usage/delete_json')?>', {id:[row.id]}, function(){
					$('#report_chemical_usage-table').datagrid('deleteRow', index);
					$('#report_chemical_usage-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#report_chemical_usage-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('report_chemical_usage/admin/report_chemical_usage/delete_json')?>',{id:selected},function(data){
						$('#report_chemical_usage-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		if(form_submit_checker == 1)
		{
			alert('Form cannot be submitted. Duplicate Entry');
			return False;
		}
		else
		{
			$('#form-report_chemical_usage').form('submit',{

				url: '<?php  echo site_url('report_chemical_usage/admin/report_chemical_usage/save')?>',
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success)
					{
						$('#form-report_chemical_usage').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#report_chemical_usage-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		});		
		}
	}


</script>
