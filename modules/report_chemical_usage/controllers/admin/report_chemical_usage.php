<?php

class Report_chemical_usage extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->module_model('report_chemical_usage','report_chemical_usage_model');
        $this->load->module_model('report_list','report_list_model');
        $this->load->module_model('master_chemical','master_chemical_model');
		$this->lang->module_load('report_chemical_usage','report_chemical_usage');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		// Display Page
		$data['header'] = 'report_chemical_usage';
		$data['page'] = $this->config->item('template_admin') . "report_chemical_usage/index";
		$data['module'] = 'report_chemical_usage';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		
		paging('id');
		$this->_get_search_param();	
		$rows=$this->db->get('view_report_chemical_usage')->result_array();
		$total=count($rows);
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['report_id']!='')?$this->db->where('report_id',$params['search']['report_id']):'';
			($params['search']['chemical_id']!='')?$this->db->where('chemical_id',$params['search']['chemical_id']):'';
			($params['search']['standard_amount']!='')?$this->db->like('standard_amount',$params['search']['standard_amount']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}


	}



	public function combo_json()
	{
		$rows=$this->report_chemical_usage_model->getReportChemicalUsages()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->report_chemical_usage_model->delete('REPORT_CHEMICAL_USAGE',array('id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
        	$success=$this->report_chemical_usage_model->insert('REPORT_CHEMICAL_USAGE',$data);
        }
        else
        {
        	$success=$this->report_chemical_usage_model->update('REPORT_CHEMICAL_USAGE',$data,array('id'=>$data['id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$data['id'] = $this->input->post('id');
    	$data['report_id'] = $this->input->post('report_id');
    	$data['chemical_id'] = $this->input->post('chemical_id');
    	$data['standard_amount'] = $this->input->post('standard_amount');

    	return $data;
    }

    public function check_duplicate()
    {
    	$report_id = $this->input->post('report_id');
    	$chemical_id = $this->input->post('chemical_id');
    	$this->db->where('report_id',$report_id);
    	$this->db->where('chemical_id',$chemical_id);
    	$result = $this->report_chemical_usage_model->count();

    	if($result)
    	{
    		$checker = FALSE;
    	}
    	else
    	{
    		$checker = TRUE;
    	}

    	echo json_encode(array('checker'=>$checker));
    }

    public function get_report_list()
    {
    	$rows = $this->report_list_model->getReportLists()->result_array();
    	echo json_encode($rows);
    }

    public function get_chemicals()
    {
    	$rows = $this->master_chemical_model->getMasterChemicals()->result_array();
    	echo json_encode($rows);
    }

}