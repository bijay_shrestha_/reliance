<?php


$lang['id'] = 'Id';
$lang['report_id'] = 'Report Id';
$lang['chemical_id'] = 'Chemical Id';
$lang['chemical_code'] = 'Chemical';
$lang['report_name'] = 'Report';
$lang['standard_amount'] = 'Standard Amount';

$lang['create_report_chemical_usage']='Create Report Chemical Usage';
$lang['edit_report_chemical_usage']='Edit Report Chemical Usage';
$lang['delete_report_chemical_usage']='Delete Report Chemical Usage';
$lang['report_chemical_usage_search']='Report Chemical Usage Search';

$lang['report_chemical_usage']='Report Chemical Usage';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

