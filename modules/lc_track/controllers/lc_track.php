<?php

class Lc_track extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('lc_track','lc_track_model');
        $this->load->module_model('lc','lc_model');
        $this->load->module_model('lc_material','lc_material_model');
        $this->load->module_model('partial_material','partial_material_model');
        $this->load->module_model('purchase','purchase_model');
        $this->load->module_model('dept_request','dept_request_model');
        $this->lang->module_load('lc_track','lc_track');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->db->order_by('id','desc');
		$data['lcs'] = $this->lc_model->getLcs(array('closed'=>0))->result_array();
		$this->lc_model->joins = array('LC_TRACK','DEALERS');
		$this->db->order_by('dd_lc_no');
		$data['lc_details'] = $this->lc_model->getLcs(array('closed'=>0))->result_array();
		$this->lc_material_model->joins = array('MATERIALS');
		foreach ($data['lc_details'] as $key => $value) {
			$data['lc_details'][$key]['materials'] = $this->lc_material_model->getLcMaterials(array('lc_id'=>$value['id']))->result_array();
		}
		// echo '<pre>';print_r($data['lc_details']);exit;
		// Display Page
		$data['header'] = 'Track LC';
		$data['view_page'] = "lc_track/index";
		$data['module'] = 'lc_track';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->lc_track_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->lc_track_model->getLcTracks()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['lc_id']!='')?$this->db->where('lc_id',$params['search']['lc_id']):'';
(isset($params['search']['reached_kolkata']))?$this->db->where('reached_kolkata',$params['search']['reached_kolkata']):'';
(isset($params['search']['reached_nepal']))?$this->db->where('reached_nepal',$params['search']['reached_nepal']):'';
(isset($params['search']['reached_factory']))?$this->db->where('reached_factory',$params['search']['reached_factory']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
(isset($params['search']['partial']))?$this->db->where('partial',$params['search']['partial']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->lc_track_model->getLcTracks()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
    	$success = FALSE;
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->lc_track_model->delete('LC_TRACK',array('id'=>$row));
            endforeach;
		}else{
			$success = $this->lc_track_model->delete('LC_TRACK',array('id'=>$id));
			if($success){
				$this->partial_material_model->delete('PARTIAL_MATERIAL',array('lc_track_id'=>$id));
				$this->purchase_model->delete('PURCHASE',array('lc_track_id'=>$id));
			}
		}
		echo json_encode(array('success'=>$success));
	}    

	public function save()
	{
		// echo '<pre>';
		$lc_id = $this->input->post('lc_id');
		// $type = 1;
        $data=$this->_get_posted_data(); //Retrive Posted Data	
        // print_r($data);
       	// print_r($this->input->post());
       	// exit;
		$table_data = array();
		$success = FALSE;
		// foreach($data['eta_kolkata'] as $key => $value){
		// 	if($value != ''){
			if($data['eta_kolkata'] != '' ||  $data['current_status'] !='' || $data['reached_nepal'] != '' || $data['reached_factory'] != ''){
				$table_data['lc_id'] = $lc_id;
				$table_data['eta_kolkata'] = $data['eta_kolkata'];
				$table_data['reached_kolkata'] = $data['reached_kolkata'];
				$table_data['reached_nepal'] = $data['reached_nepal'];
				// $table_data['border'] = $data['border'];
				$table_data['reached_factory'] = $data['reached_factory'];
				$table_data['date_en'] = $data['date_en'];
				$table_data['date_np'] = $data['date_np'];
				$table_data['current_status'] = $data['current_status'];
		if(!$this->input->post('id')){
			$type = 1;
				$success=$this->lc_track_model->insert('LC_TRACK',$table_data);
				$material_data['lc_track_id'] = $id = $this->db->insert_id();
				foreach($data['material_name'] as $i => $material){
					$material_data['material_id'] = $material;
					$material_data['quantity'] = $data['quantity'][$i];
					$this->partial_material_model->insert('PARTIAL_MATERIAL',$material_data);
				}
			}else{
				// echo 'here';
					$id = $this->input->post('id');
						$type = 2;
						$success=$this->lc_track_model->update('LC_TRACK',$table_data,array('id'=>$id));
						// echo $this->db->last_query();
						// $material_data['lc_track_id'] = $id = $this->db->insert_id();
						foreach($data['material_name'] as $i => $material){
							$material_data['material_id'] = $material;
							$material_data['quantity'] = $data['quantity'][$i];
							$this->partial_material_model->update('PARTIAL_MATERIAL',$material_data,array('material_id'=>$material,'lc_track_id'=>$id));
						}
					}
				}
				
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		// echo $id;
		 
		 if($success){
		 	echo json_encode(array('msg'=>$msg,'success'=>$success,'id'=>$id,'type'=>$type));		
		 }
		 else{
		 	echo json_encode(array('msg'=>$msg,'success'=>$success));		
		 }
        
	}

	// save track
	public function saveTrack()
	{
		echo '<pre>';
		print_r($this->input->post());
		$lc_id = $this->input->post('lc_id');
		$data['id'] = $this->input->post('id');
$data['lc_id'] = $lc_id;
$data['eta_kolkata'] = $this->input->post('eta_kolkata');
$data['reached_kolkata'] = $this->input->post('reached_kolkata');
// $data['reached_nepal'] = $this->input->post('reached_nepal');
// $data['reached_factory'] = $this->input->post('reached_factory');
		// $data=$this->_get_posted_data(); //Retrive Posted Data	
		print_r($data);
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['lc_id'] = $this->input->post('lc_id');
$data['eta_kolkata'] = $this->input->post('eta_kol');
$data['reached_kolkata'] = $this->input->post('reached_kol');
$data['reached_nepal'] = $this->input->post('reached_nepal');
$data['reached_factory'] = $this->input->post('reached_factory');
// $data['border'] = $this->input->post('border');
$data['date_en'] = $this->input->post('date_en');
$data['date_np'] = $this->input->post('date_np');
$data['current_status'] = $this->input->post('current_status');
// $data['partial'] = $this->input->post('partial');
$data['material_name'] = $this->input->post('material_name');
$data['quantity'] = $this->input->post('quantity');

        return $data;
   }
   public function getValue(){
   		$where['lc_id'] = $this->input->post('id');
   		$data['lc'] = $this->lc_model->getLcs(array('id'=>$where['lc_id']))->row_array();
   		$this->lc_material_model->joins = array('MATERIALS');
   		$data['materials'] = $this->lc_material_model->getLcMaterials($where)->result_array();
   		$data['tracks'] = $this->lc_track_model->getLcTracks($where)->result_array();
   		foreach($data['tracks'] as $key => $track){
   			$materials = $this->partial_material_model->getPartialMaterials(array('lc_track_id'=>$track['id']))->result_array();
   			foreach ($materials as $key1 => $value1) {
   				$data['tracks'][$key]['material'][$value1['material_id']] = $value1['quantity'];
   			}
   		}
   		// echo json_encode($data);
   		// echo '<pre>';
   		// print_r($data);
   		// echo '</pre>';
   		// if(count($data['tracks']) > 0){

   			$this->load->view('lc_track/tracking_form',$data);
   		// }else{
   		// 	$this->load->view('lc_track/track_form',$data);
   		// }
   }

   public function getClosedValue(){
   		$where['lc_id'] = $this->input->post('id');
   		$data['lc'] = $this->lc_model->getLcs(array('id'=>$where['lc_id']))->row_array();
   		$this->lc_material_model->joins = array('MATERIALS');
   		$data['materials'] = $this->lc_material_model->getLcMaterials($where)->result_array();
   		$data['tracks'] = $this->lc_track_model->getLcTracks($where)->result_array();
   		foreach($data['tracks'] as $key => $track){
   			$materials = $this->partial_material_model->getPartialMaterials(array('lc_track_id'=>$track['id']))->result_array();
   			foreach ($materials as $key1 => $value1) {
   				$data['tracks'][$key]['material'][$value1['material_id']] = $value1['quantity'];
   			}
   		}
   		// echo json_encode($data);
   		// echo '<pre>';
   		// print_r($data);
   		// echo '</pre>';
   		// if(count($data['tracks']) > 0){

   			$this->load->view('lc_track/closed_tracking_form',$data);
   		// }else{
   		// 	$this->load->view('lc_track/track_form',$data);
   		// }
   }
   public function addForm()
   {
   		$data['i'] = $this->input->post('i');
   		$where['lc_id'] = $this->input->post('id');
   		$this->lc_material_model->joins = array('MATERIALS');
   		$data['materials'] = $this->lc_material_model->getLcMaterials($where)->result_array();
   		$this->load->view('lc_track/add_form',$data);
   }
   public function addDetail()
   {
   		$id = $this->input->post('id');
   		$data['track'] = $this->lc_track_model->getLcTracks(array('id'=>$id))->row_array();
   		$this->partial_material_model->joins = array('MATERIALS');
   		$data['materials'] = $this->partial_material_model->getPartialMaterials(array('lc_track_id'=>$id))->result_array();
   		// echo '<pre>';print_r($data);exit;
   		$this->load->view('lc_track/detail',$data);
   }
   
   	public function getData()
   	{
   		$id = $this->input->post('id');
   		// echo $id;
   		$data['track'] = $this->lc_track_model->getLcTracks(array('id'=>$id))->row_array();
   		$this->partial_material_model->joins = array('MATERIALS');
   		$data['materials'] = $this->partial_material_model->getPartialMaterials(array('lc_track_id'=>$id))->result_array();
   		echo json_encode($data);
   	}

   	public function saveData()
   	{
   		echo '<pre>';
   		print_r($this->input->post());
   		echo 'this is try';
   		$data = $this->_get_posted_data();
   		print_r($data);
   	} 
   	public function closed()
   	{
   		$this->db->order_by('id','desc');
		$data['lcs'] = $this->lc_model->getLcs(array('closed'=>1))->result_array();
		$this->lc_model->joins = array('LC_TRACK','DEALERS');
		$this->db->order_by('dd_lc_no');
		$data['lc_details'] = $this->lc_model->getLcs(array('closed'=>1))->result_array();
		$this->lc_material_model->joins = array('MATERIALS');
		foreach ($data['lc_details'] as $key => $value) {
			$data['lc_details'][$key]['materials'] = $this->lc_material_model->getLcMaterials(array('lc_id'=>$value['id']))->result_array();
		}
		// echo '<pre>';print_r($data['lc_details']);exit;
		// Display Page
		$data['header'] = 'Closed LC';
		$data['view_page'] = "lc_track/closed";
		$data['module'] = 'lc_track';
		$this->load->view($this->_container,$data);	
   	}
}