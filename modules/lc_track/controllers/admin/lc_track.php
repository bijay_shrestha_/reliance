<?php

class Lc_track extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('lc_track','lc_track_model');
        $this->lang->module_load('lc_track','lc_track');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'lc_track';
		$data['page'] = $this->config->item('template_admin') . "lc_track/index";
		$data['module'] = 'lc_track';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->lc_track_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->lc_track_model->getLcTracks()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['lc_id']!='')?$this->db->where('lc_id',$params['search']['lc_id']):'';
(isset($params['search']['reached_kolkata']))?$this->db->where('reached_kolkata',$params['search']['reached_kolkata']):'';
(isset($params['search']['reached_nepal']))?$this->db->where('reached_nepal',$params['search']['reached_nepal']):'';
(isset($params['search']['reached_factory']))?$this->db->where('reached_factory',$params['search']['reached_factory']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
(isset($params['search']['partial']))?$this->db->where('partial',$params['search']['partial']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->lc_track_model->getLcTracks()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->lc_track_model->delete('LC_TRACK',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->lc_track_model->insert('LC_TRACK',$data);
        }
        else
        {
            $success=$this->lc_track_model->update('LC_TRACK',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['lc_id'] = $this->input->post('lc_id');
$data['eta_kolkata'] = $this->input->post('eta_kolkata');
$data['reached_kolkata'] = $this->input->post('reached_kolkata');
$data['reached_nepal'] = $this->input->post('reached_nepal');
$data['reached_factory'] = $this->input->post('reached_factory');
$data['date_en'] = $this->input->post('date_en');
$data['date_np'] = $this->input->post('date_np');
$data['partial'] = $this->input->post('partial');

        return $data;
   }
   
   	
	    
}