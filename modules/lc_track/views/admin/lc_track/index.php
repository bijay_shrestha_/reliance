<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('lc_track_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="lc_track-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('lc_id')?></label>:</td>
<td><input type="text" name="search[lc_id]" id="search_lc_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('eta_kolkata')?></label>:</td>
<td><input type="text" name="date[eta_kolkata][from]" id="search_eta_kolkata_from"  class="easyui-datebox"/> ~ <input type="text" name="date[eta_kolkata][to]" id="search_eta_kolkata_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('reached_kolkata')?></label>:</td>
<td><input type="radio" name="search[reached_kolkata]" id="search_reached_kolkata1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[reached_kolkata]" id="search_reached_kolkata0" value="0"/><?php echo lang('general_no')?></td>
<td><label><?php echo lang('reached_nepal')?></label>:</td>
<td><input type="radio" name="search[reached_nepal]" id="search_reached_nepal1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[reached_nepal]" id="search_reached_nepal0" value="0"/><?php echo lang('general_no')?></td>
</tr>
<tr>
<td><label><?php echo lang('reached_factory')?></label>:</td>
<td><input type="radio" name="search[reached_factory]" id="search_reached_factory1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[reached_factory]" id="search_reached_factory0" value="0"/><?php echo lang('general_no')?></td>
<td><label><?php echo lang('date_en')?></label>:</td>
<td><input type="text" name="date[date_en][from]" id="search_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_en][to]" id="search_date_en_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('date_np')?></label>:</td>
<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('partial')?></label>:</td>
<td><input type="radio" name="search[partial]" id="search_partial1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[partial]" id="search_partial0" value="0"/><?php echo lang('general_no')?></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="lc_track-table" data-options="pagination:true,title:'<?php  echo lang('lc_track')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'lc_id',sortable:true" width="50"><?php echo lang('lc_id')?></th>
<th data-options="field:'eta_kolkata',sortable:true" width="50"><?php echo lang('eta_kolkata')?></th>
<th data-options="field:'reached_kolkata',sortable:true" width="50"><?php echo lang('reached_kolkata')?></th>
<th data-options="field:'reached_nepal',sortable:true" width="50"><?php echo lang('reached_nepal')?></th>
<th data-options="field:'reached_factory',sortable:true" width="50"><?php echo lang('reached_factory')?></th>
<th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th>
<th data-options="field:'date_np',sortable:true" width="50"><?php echo lang('date_np')?></th>
<th data-options="field:'partial',sortable:true" width="50"><?php echo lang('partial')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_lc_track')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_lc_track')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit lc_track form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-lc_track" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('lc_id')?>:</label></td>
					  <td width="66%"><input name="lc_id" id="lc_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('eta_kolkata')?>:</label></td>
					  <td width="66%"><input name="eta_kolkata" id="eta_kolkata" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('reached_kolkata')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="reached_kolkata" id="reached_kolkata1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="reached_kolkata" id="reached_kolkata0" /><?php echo lang("general_no")?></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('reached_nepal')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="reached_nepal" id="reached_nepal1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="reached_nepal" id="reached_nepal0" /><?php echo lang("general_no")?></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('reached_factory')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="reached_factory" id="reached_factory1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="reached_factory" id="reached_factory0" /><?php echo lang("general_no")?></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('partial')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="partial" id="partial1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="partial" id="partial0" /><?php echo lang("general_no")?></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#lc_track-search-form').form('clear');
			$('#lc_track-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#lc_track-table').datagrid({
				queryParams:{data:$('#lc_track-search-form').serialize()}
				});
		});		
		$('#lc_track-table').datagrid({
			url:'<?php  echo site_url('lc_track/admin/lc_track/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_lc_track')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removelc_track('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_lc_track')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-lc_track').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_lc_track')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#lc_track-table').datagrid('getRows')[index];
		if (row){
			$('#form-lc_track').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_lc_track')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removelc_track(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#lc_track-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('lc_track/admin/lc_track/delete_json')?>', {id:[row.id]}, function(){
					$('#lc_track-table').datagrid('deleteRow', index);
					$('#lc_track-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#lc_track-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('lc_track/admin/lc_track/delete_json')?>',{id:selected},function(data){
						$('#lc_track-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-lc_track').form('submit',{
			url: '<?php  echo site_url('lc_track/admin/lc_track/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-lc_track').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#lc_track-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>