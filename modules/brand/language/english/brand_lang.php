<?php


$lang['id'] = 'Id';
$lang['brand_name'] = 'Brand Name';

$lang['create_brand']='Create Brand';
$lang['edit_brand']='Edit Brand';
$lang['delete_brand']='Delete Brand';
$lang['brand_search']='Brand Search';

$lang['brand']='Brand';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

$lang['brand_type_name'] = 'Type';
