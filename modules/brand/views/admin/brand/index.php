<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('brand_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="brand-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr>
	<td><label><?php echo lang('brand_name')?></label>:</td>
	<td><input type="text" name="search[brand_name]" id="search_brand_name"  class="easyui-textbox"/></td>
	<td><label><?php echo lang('brand_type_name')?></label>:</td>
	<td><input type="text" name="search[type_id]" id="search_type"  class="easyui-combobox"/></td>
</tr>
<tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="brand-table" data-options="pagination:true,title:'<?php  echo lang('brand')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
<th data-options="field:'brand_name',sortable:true" width="50"><?php echo lang('brand_name')?></th>
<th data-options="field:'brand_type',sortable:true" width="50"><?php echo lang('brand_type_name')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_brand')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_brand')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit brand form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-brand" method="post" >
    <table>
		<tr>
          	<td width="34%" ><label><?php echo lang('brand_name')?>:</label></td>
		  	<td width="66%"><input name="brand_name" id="brand_name" class="easyui-texbox" required="true"></td>
       	</tr>
       	<tr>
       		<td width="34%" ><label><?php echo lang('brand_type_name')?>:</label></td>
		  	<td width="66%"><input name="type_id" id="brand_type_id_assign" class="easyui-combobox" required="true"></td>
	  	</tr>
       	<input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#brand-search-form').form('clear');
			$('#brand-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#brand-table').datagrid({
				queryParams:{data:$('#brand-search-form').serialize()}
				});
		});		
		$('#brand-table').datagrid({
			url:'<?php  echo site_url('brand/admin/brand/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});

		$('#brand_type_id_assign,#search_type').combobox({
			url:'<?php echo site_url('brand_type/admin/brand_type/combo_json'); ?>',
			multiple: false,
			valueField: 'id',
			textField: 'name',
			panelHeight:'100',
			editable: false,

		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning"  title="<?php  echo lang('edit_brand')?>">Edit</a>';
		var d = '<a href="#" onclick="removebrand('+index+')" class="btn btn-danger" title="<?php  echo lang('delete_brand')?>">Delete</a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-brand').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_brand')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#brand-table').datagrid('getRows')[index];
		if (row){
			$('#form-brand').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_brand')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removebrand(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#brand-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('brand/admin/brand/delete_json')?>', {id:[row.id]}, function(){
					$('#brand-table').datagrid('deleteRow', index);
					$('#brand-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#brand-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('brand/admin/brand/delete_json')?>',{id:selected},function(data){
						$('#brand-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-brand').form('submit',{
			url: '<?php  echo site_url('brand/admin/brand/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-brand').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#brand-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>