<?php

class Brand extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('brand','brand_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->lang->module_load('brand','brand');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->brand_model->joins = array('BRAND_TYPE');
		$data['rows']=$this->brand_model->getBrands()->result_array();

		$data['brand_types']=$this->brand_type_model->getBrandtypes()->result_array();

		// Display Page
		$data['header'] = 'Brand';
		$data['view_page'] = "brand/index";
		$data['module'] = 'brand';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->brand_model->joins = array('BRAND_TYPE');

		$this->_get_search_param();	
		$total=$this->brand_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->brand_model->getBrands()->result_array();

		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['brand_name']!='')?$this->db->like('brand_name',$params['search']['brand_name']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->brand_model->getBrands()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->brand_model->delete('BRAND',array('id'=>$row));
            endforeach;
		}else{
			$data['success'] = $this->brand_model->delete('BRAND',array('id'=>$id));
			echo json_encode($data);
		}
	}    

	public function save()
	{

        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->brand_model->insert('BRAND',$data);
        }
        else
        {
            $success=$this->brand_model->update('BRAND',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
	$data['brand_name'] = $this->input->post('brand_name');
	$data['type_id'] = $this->input->post('type_id');

        return $data;
   }
   
   	public function combobox_brand_id()
    {
    	$this->db->order_by('brand_name');
    	$rows = $this->brand_model->getBrands()->result_array();
    	echo json_encode($rows);
    }

    function get_brand(){
    	$where['type_id'] = $this->input->post('type_id');

    	$data['brands'] = $this->brand_model->getBrands($where)->result_array();
    		
    	echo json_encode($data);
    }

    function get_value(){
    	$where['id'] = $this->input->post('id');
    	$row = $this->brand_model->getBrands($where)->row_array();
    	echo json_encode($row);
    }
	    
}