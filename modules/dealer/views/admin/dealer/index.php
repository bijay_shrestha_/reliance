<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('dealer_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
			<form action="" method="post" id="dealer-search-form">
				<table width="100%" cellspacing="1" cellpadding="1">
					<tr><td><label><?php echo lang('dealer_name')?></label>:</td>
						<td><input type="text" name="search[dealer_name]" id="search_dealer_name"  class="easyui-validatebox"/></td>
						<td><label><?php echo lang('contact')?></label>:</td>
						<td><input type="text" name="search[contact]" id="search_contact"  class="easyui-validatebox"/></td>
					</tr>
					<tr>
						<td><label><?php echo lang('mobile')?></label>:</td>
						<td><input type="text" name="search[mobile]" id="search_mobile"  class="easyui-validatebox"/></td>
						<td><label><?php echo lang('created_date_np')?></label>:</td>
						<td><input type="text" name="date[created_date_np][from]" id="search_created_date_np_from"  class="easyui-datebox"/> ~ <input type="text" name="date[created_date_np][to]" id="search_created_date_np_to"  class="easyui-datebox"/></td>
					</tr>
					<tr>
						<td><label><?php echo lang('created_date_en')?></label>:</td>
						<td><input type="text" name="date[created_date_en][from]" id="search_created_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[created_date_en][to]" id="search_created_date_en_to"  class="easyui-datebox"/></td>
					</tr>
					<tr>
						<td colspan="4">
							<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
							<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
						</td>
					</tr>
				</table>

			</form>
		</div>
		<br/>
		<div class="btn-group">
			<a class="btn btn-primary" id="dealer_btn">Add Dealer</a>
			<a class="btn btn-default" id="assign_btn">Assign Dealer</a>
		</div>

		<div id="dealer_table">
			
			<table id="dealer-table" data-options="pagination:true,title:'<?php  echo lang('dealer')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
				<thead>
					<th data-options="field:'checkbox',checkbox:true"></th>
					<th data-options="field:'dealer_id',sortable:true" width="30"><?php echo lang('dealer_id')?></th>
					<th data-options="field:'dealer_name',sortable:true" width="50"><?php echo lang('dealer_name')?></th>
					<th data-options="field:'contact',sortable:true" width="50"><?php echo lang('contact')?></th>
					<th data-options="field:'mobile',sortable:true" width="50"><?php echo lang('mobile')?></th>
					<th data-options="field:'created_date_np',sortable:true" width="50"><?php echo lang('created_date_np')?></th>
					<th data-options="field:'created_date_en',sortable:true" width="50"><?php echo lang('created_date_en')?></th>

					<th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
				</thead>
			</table>
		</div>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_dealer')?>"><?php  echo lang('create')?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected(0)"  title="<?php  echo lang('delete_dealer')?>"><?php  echo lang('remove_selected')?></a>
			</p>

		</div>
<?php /*
		<div id="assign_table" hidden>
			<table id="assign-table" data-options="pagination:true,title:'<?php  echo 'Assign Material to Dealer'?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar2',collapsible:true,fitColumns:true">
				<thead>
					<th data-options="field:'checkbox',checkbox:true"></th>
					<th data-options="field:'dealer_name',sortable:true" width="50"><?php echo lang('dealer_name')?></th>
					<th data-options="field:'material_name',sortable:true" width="50"><?php echo 'Material Name'?></th>

					<th field="action" width="100" formatter="getActions_assigned"><?php  echo lang('action')?></th>
				</thead>
			</table>
		</div>

		<div id="toolbar2" style="padding:5px;height:auto">
			<p>
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="assign()" title="<?php  echo lang('create_dealer')?>"><?php  echo lang('create')?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected(1)"  title="<?php  echo lang('delete_dealer')?>"><?php  echo lang('remove_selected')?></a>
			</p>

		</div> 
*/?>
		<!-- assigning material to dealer -->
		<div id="assign_table" hidden>
			<form id="dealer-material-form">
				<input name="dealer_id" id="dealer_id_assign" class="easyui-combobox" required="true">
				<table id='material_list'></table>
				<button type="button" class="btn btn-primary" id="btn-submit">Save</button>
			</form>
		</div>

		<!--for create and edit dealer form-->
		<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"	data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
			<form id="form-dealer" method="post" >
				<table>
					<tr>
						<td width="34%" ><label><?php echo lang('dealer_name')?>:</label></td>
						<td width="66%"><input name="dealer_name" id="dealer_name" class="easyui-validatebox" required="true"></td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('contact')?>:</label></td>
						<td width="66%"><input name="contact" id="contact" class="easyui-validatebox" required="true"></td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('mobile')?>:</label></td>
						<td width="66%"><input name="mobile" id="mobile" class="easyui-validatebox" required="true"></td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('created_date_np')?>:</label></td>
						<td width="66%"><input name="created_date_np" id="created_date_np" class="easyui-datebox" required="true"></td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('created_date_en')?>:</label></td>
						<td width="66%"><input name="created_date_en" id="created_date_en" class="easyui-datebox" required="true"></td>
					</tr>
					<input type="hidden" name="dealer_id" id="dealer_id"/>
				</table>
			</form>
			<div id="dlg-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save(0)"><?php  echo  lang('general_save')?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
			</div>    
		</div>
		<!--div ends-->

		<!--for create and edit material_dealer form-->
		<div id="dlg2" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px" data-options="closed:true,collapsible:true,buttons:'#dlg2-buttons',modal:true">
			<form id="form-material_dealer" method="post" >
				<table>
					<tr>
						<td width="34%" ><label><?php echo 'Material Name'?>:</label></td>
						<td width="66%"><input name="material_id" id="material_id" class="easyui-combobox" required="true"></td>
					</tr>
					<tr>
						<td width="34%" ><label><?php echo lang('dealer_name')?>:</label></td>
						<td width="66%"><input name="dealer_id" id="dealer_id_assign" class="easyui-combobox" required="true"></td>
					</tr><input type="hidden" name="md_id" id="md_id"/>
				</table>
			</form>
			<div id="dlg2-buttons">
				<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save(1)"><?php  echo  lang('general_save')?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg2').window('close')"><?php  echo  lang('general_cancel')?></a>
			</div>    
		</div>
		<!--div ends-->

	</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#dealer-search-form').form('clear');
			$('#dealer-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#dealer-table').datagrid({
				queryParams:{data:$('#dealer-search-form').serialize()}
			});
		});		
		$('#dealer-table').datagrid({
			url:'<?php  echo site_url('dealer/admin/dealer/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});

		$('#assign_btn').click(function(){
			$(this).removeClass('btn-default').addClass('btn-primary');
			$('#dealer_btn').addClass('btn-default').removeClass('btn-primary');
			$('#dealer_table').hide();
			$('#assign_table').show();
			$('#assign-table').datagrid({
				url:'<?php echo site_url('material_dealer/admin/material_dealer/json'); ?>',
				height: 'auto',
				width: 'auto'
			});
		});
		$('#dealer_btn').click(function(){
			$(this).removeClass('btn-default').addClass('btn-primary');
			$('#assign_btn').addClass('btn-default').removeClass('btn-primary');
			$('#assign_table').hide();
			$('#dealer_table').show();
			$('#dealer-table').datagrid({
				url:'<?php echo site_url('dealer/admin/dealer/json'); ?>',
				height: 'auto',
				width: 'auto'
			});
		});

		
		$('#dealer_id_assign').combobox({
			url:'<?php echo site_url('dealer/admin/dealer/combobox_dealer_id'); ?>',
			multiple: false,
			valueField: 'dealer_id',
			textField: 'dealer_name',
			panelHeight:'200',
			onChange:function(dealer_id_assign)
			{
				material_dealer_form(dealer_id_assign);
			}

		});
		$('#material_id').combobox({
			url:'<?php echo site_url('material/admin/material/combobox_material_id'); ?>',
			multiple: false,
			valueField: 'material_id',
			textField: 'material_name',
			panelHeight:'200'

		});

		
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-primary btn-sm"  title="<?php  echo lang('edit_dealer')?>">Edit</a>';
		var d = '<a href="#" onclick="removedealer('+index+')" class="btn btn-danger btn-sm"  title="<?php  echo lang('delete_dealer')?>">Delete</a>';
		return e+d;		
	}

	function getActions_assigned(value,row,index)
	{
		var e = '<a href="#" onclick="edit_assigned('+index+')" class="btn btn-primary btn-xs margin"   title="<?php  echo lang('edit_category')?>">Edit</a>';
		var d = '<a href="#" onclick="remove_assigned('+index+')" class="btn btn-danger btn-xs "  title="<?php  echo lang('delete_category')?>">Delete</a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-dealer').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_dealer')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function assign(){
		//Create code here
		$('#form-material_dealer').form('clear');
		$('#dlg2').window('open').window('setTitle','<?php  echo "Assign Dealer to Materials"?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}

	function edit(index)
	{
		var row = $('#dealer-table').datagrid('getRows')[index];
		if (row){
			$('#form-dealer').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_dealer')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
	function edit_assigned(index)
	{
		var row = $('#assign-table').datagrid('getRows')[index];
		if (row){
			$('#form-material_dealer').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg2').window('open').window('setTitle','<?php  echo "Edit Assigned"?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	function removedealer(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#dealer-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('dealer/admin/dealer/delete_json')?>', {id:[row.dealer_id]}, function(){
					$('#dealer-table').datagrid('deleteRow', index);
					$('#dealer-table').datagrid('reload');
				});

			}
		});
	}

	function remove_assigned(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#assign-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('material_dealer/admin/material_dealer/delete_json')?>', {id:[row.id]}, function(){
					$('#assign-table').datagrid('deleteRow', index);
					$('#assign-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected(n)
	{
		if(!n)
		{
			var rows=$('#dealer-table').datagrid('getSelections');
			if(rows.length>0)
			{
				selected=[];
				for(i=0;i<rows.length;i++)
				{
					selected.push(rows[i].dealer_id);
				}

				$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
					if(r){				
						$.post('<?php  echo site_url('dealer/admin/dealer/delete_json')?>',{id:selected},function(data){
							$('#dealer-table').datagrid('reload');
						});
					}

				});

			}
			else
			{
				$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
			}
		}	
		else
		{
			var rows=$('#assign-table').datagrid('getSelections');
			if(rows.length>0)
			{
				selected=[];
				for(i=0;i<rows.length;i++)
				{
					selected.push(rows[i].md_id);
				}
				
				$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
					if(r){				
						$.post('<?php  echo site_url('material_dealer/admin/material_dealer/delete_json')?>',{id:selected},function(data){
							$('#assign-table').datagrid('reload');
						});
					}
					
				});
				
			}
			else
			{
				$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
			}
		}	
	}
	
	function save(n)
	{
		if(!n) {
			$('#form-dealer').form('submit',{
				url: '<?php  echo site_url('dealer/admin/dealer/save')?>',
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success)
					{
						$('#form-dealer').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#dealer-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		});	

		}
		else {
			$('#form-material_dealer').form('submit',{
				url: '<?php  echo site_url('material_dealer/admin/material_dealer/save')?>',
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success)
					{
						$('#form-material_dealer').form('clear');
						$('#dlg2').window('close');		// close the dialog
						$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
						$('#assign-table').datagrid('reload');	// reload the user data
					} 
					else 
					{
						$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
					} //if close
				}//success close
			});	
			
		}
		
	}
	
	
</script>
<script type="text/javascript">
	function material_dealer_form(dealer_id){
		$('#material_list').empty();
		$.post('<?php echo site_url("material_dealer/admin/material_dealer/getMaterialDealer")?>',{dealer_id:dealer_id},function(data){
			var i=0;
			var html = '';
			$.each(data.materials,function(index,value){
				if(i == 0){
					html += '<tr><td><input type="checkbox" name="material_id[]" id="material-'+value.material_id+'" value="'+value.material_id+'">'+value.material_name+'</td>';
					i++;
				}
				else if(i == 4){
					html += '<td><input type="checkbox" name="material_id[]" id="material-'+value.material_id+'" value="'+value.material_id+'">'+value.material_name+'</td></tr>';
					i = 0;
				}else{
					html +='<td><input type="checkbox" name="material_id[]" id="material-'+value.material_id+'" value="'+value.material_id+'">'+value.material_name+'</td>';
					i++;
				}
			});
			$('#material_list').append(html);
			$.each(data.rows,function(index,value){
				$('#material-'+value.material_id).prop('checked',true);
			})
		},'json');
		
	}
</script>
<script type="text/javascript">
	$('#btn-submit').click(function(){
		$.post("<?php echo site_url('material_dealer/admin/material_dealer/saveAll')?>",$('#dealer-material-form').serializeArray(),function(result){
				console.log(result);
				
					if (result.success)
					{
						$('#material_list').empty();
						// $('#form-material_dealer').form('clear');
						// $('#dlg2').window('close');		// close the dialog
						$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
						$('#assign-table').datagrid('reload');	// reload the user data
					} 
					else 
					{
						$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
					} //if close
		},'json');
	});
</script>