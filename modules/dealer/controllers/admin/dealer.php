<?php

class Dealer extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->module_model('dealer','dealer_model');
		$this->lang->module_load('dealer','dealer');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		// Display Page
		$data['header'] = 'dealer';
		$data['page'] = $this->config->item('template_admin') . "dealer/index";
		$data['module'] = 'dealer';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->dealer_model->count();
		paging('dealer_id');
		$this->_get_search_param();	
		$rows=$this->dealer_model->getDealers()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['dealer_name']!='')?$this->db->like('dealer_name',$params['search']['dealer_name']):'';
			($params['search']['contact']!='')?$this->db->like('contact',$params['search']['contact']):'';
			($params['search']['mobile']!='')?$this->db->like('mobile',$params['search']['mobile']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}


	}

	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
				"' and '".date('Y-m-d',strtotime($to))."')");
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}	

	public function combo_json()
	{
		$rows=$this->dealer_model->getDealers()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->dealer_model->delete('DEALERS',array('dealer_id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('dealer_id'))
        {
        	$success=$this->dealer_model->insert('DEALERS',$data);
        }
        else
        {
        	$success=$this->dealer_model->update('DEALERS',$data,array('dealer_id'=>$data['dealer_id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$data['dealer_id'] = $this->input->post('dealer_id');
    	$data['dealer_name'] = $this->input->post('dealer_name');
    	$data['contact'] = $this->input->post('contact');
    	$data['mobile'] = $this->input->post('mobile');
    	$data['created_date_np'] = $this->input->post('created_date_np');
    	$data['created_date_en'] = $this->input->post('created_date_en');

    	return $data;
    }

    public function combobox_dealer_id()
    {
    	$this->db->order_by('dealer_name');
    	$rows = $this->dealer_model->getDealers()->result_array();
    	echo json_encode($rows);
    }


}