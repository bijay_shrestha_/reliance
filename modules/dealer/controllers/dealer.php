<?php

class Dealer extends Member_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->module_model('dealer', 'dealer_model');
        $this->lang->module_load('dealer', 'dealer');
        $this->load->module_model('material','material_model');
        $this->load->module_model('material_dealer','material_dealer_model');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }

    public function index() {
        // Display Page
        $this->db->order_by('dealer_name','asc');
        $data['dealers'] = $this->dealer_model->getDealers(array('status'=>1))->result_array();
        $this->db->order_by('material_name','asc');
        $data['materials'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
        $data['header'] = 'Raw Material Dealer';
        $data['view_page'] = "dealer/index";
        $data['module'] = 'dealer';
        $this->load->view($this->_container, $data);
    }
    
    public function getDealer(){
        $id = $this->input->post('dealer_id');
        $data = $this->dealer_model->getDealers(array('dealer_id'=>$id))->row_array();
        echo json_encode($data);
    }

    public function add() {

        $data = $this->_get_posted_data(); //Retrive Posted Data
        if($data['dealer_name'] == '' || $data['contact'] == "" || $data['mobile'] == ""){
            $success = FALSE;
        }else{
            if(!$this->input->post('dealer_id')) {
                $success = $this->dealer_model->insert('DEALERS', $data);
            }else {
                $success = $this->dealer_model->update('DEALERS', $data, array('dealer_id' => $data['dealer_id']));
            }
        }

        if ($success) {
            $success = TRUE;
            $msg = lang('success_message');
        } else {
            $success = FALSE;
            $msg['dealer_name'] = 'Dealer name is required';
            $msg['contact'] = 'Contact is required';
            $msg['mobile'] = 'Cobile is required';
        }

        echo json_encode(array('msg' => $msg, 'success' => $success));
    }

    private function _get_posted_data() {
        $data = array();
        $data['dealer_id'] = $this->input->post('dealer_id');
        $data['dealer_name'] = $this->input->post('dealer_name');
        $data['contact'] = $this->input->post('contact');
        $data['mobile'] = $this->input->post('mobile');
        $data['created_date_np'] = $this->input->post('created_date_np');
        $data['created_date_en'] = $this->input->post('created_date_en');
        $data['dealer_address'] = $this->input->post('dealer_address');
        $data['email'] = $this->input->post('email');
        //$data['status'] = $this->input->post('status');
        $data['status'] = 1;

        return $data;
    }

}
