<?php

class Partial_material extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('partial_material','partial_material_model');
        $this->lang->module_load('partial_material','partial_material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'partial_material';
		$data['page'] = $this->config->item('template_admin') . "partial_material/index";
		$data['module'] = 'partial_material';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->partial_material_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->partial_material_model->getPartialMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['lc_track_id']!='')?$this->db->where('lc_track_id',$params['search']['lc_track_id']):'';
($params['search']['quantity']!='')?$this->db->where('quantity',$params['search']['quantity']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->partial_material_model->getPartialMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->partial_material_model->delete('PARTIAL_MATERIAL',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->partial_material_model->insert('PARTIAL_MATERIAL',$data);
        }
        else
        {
            $success=$this->partial_material_model->update('PARTIAL_MATERIAL',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['material_id'] = $this->input->post('material_id');
$data['lc_track_id'] = $this->input->post('lc_track_id');
$data['quantity'] = $this->input->post('quantity');

        return $data;
   }
   
   	
	    
}