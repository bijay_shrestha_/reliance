<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('partial_material_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="partial_material-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('material_id')?></label>:</td>
<td><input type="text" name="search[material_id]" id="search_material_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('lc_track_id')?></label>:</td>
<td><input type="text" name="search[lc_track_id]" id="search_lc_track_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('quantity')?></label>:</td>
<td><input type="text" name="search[quantity]" id="search_quantity"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="partial_material-table" data-options="pagination:true,title:'<?php  echo lang('partial_material')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'material_id',sortable:true" width="50"><?php echo lang('material_id')?></th>
<th data-options="field:'lc_track_id',sortable:true" width="50"><?php echo lang('lc_track_id')?></th>
<th data-options="field:'quantity',sortable:true" width="50"><?php echo lang('quantity')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_partial_material')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_partial_material')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit partial_material form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-partial_material" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('material_id')?>:</label></td>
					  <td width="66%"><input name="material_id" id="material_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('lc_track_id')?>:</label></td>
					  <td width="66%"><input name="lc_track_id" id="lc_track_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('quantity')?>:</label></td>
					  <td width="66%"><input name="quantity" id="quantity" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#partial_material-search-form').form('clear');
			$('#partial_material-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#partial_material-table').datagrid({
				queryParams:{data:$('#partial_material-search-form').serialize()}
				});
		});		
		$('#partial_material-table').datagrid({
			url:'<?php  echo site_url('partial_material/admin/partial_material/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_partial_material')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removepartial_material('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_partial_material')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-partial_material').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_partial_material')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#partial_material-table').datagrid('getRows')[index];
		if (row){
			$('#form-partial_material').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_partial_material')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removepartial_material(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#partial_material-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('partial_material/admin/partial_material/delete_json')?>', {id:[row.id]}, function(){
					$('#partial_material-table').datagrid('deleteRow', index);
					$('#partial_material-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#partial_material-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('partial_material/admin/partial_material/delete_json')?>',{id:selected},function(data){
						$('#partial_material-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-partial_material').form('submit',{
			url: '<?php  echo site_url('partial_material/admin/partial_material/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-partial_material').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#partial_material-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>