<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Member_Controller
{
	var $months = array(
		'01' => 'Baishak',
		'02' => 'Jesth',
		'03' => 'Ashadh',
		'04' => 'Shrawan',
		'05' => 'Bhadra',
		'06' => 'Ashoj',
		'07' => 'Kartik',
		'08' => 'Mangshir',
		'09' => 'Poush',
		'10' => 'Magh',
		'11' => 'Falgun',
		'12' => 'Chaitra'
		);
	function __construct()
	{
		parent::__construct();
		$this->load->module_model('material', 'material_model');
		$this->load->module_model('purchase', 'purchase_model');
		$this->load->module_model('material_department', 'material_department_model');
		$this->load->module_model('daily_production_sale', 'daily_production_sale_model');
		$this->load->module_model('brand', 'brand_model');
		$this->load->module_model('brand_type', 'brand_type_model');
		$this->load->module_model('category', 'category_model');
		$this->load->module_model('dept_request', 'dept_request_model');
		$this->load->module_model('plant_operation', 'plant_operation_model');
		$this->load->module_model('electric_meter_consumption', 'electric_meter_consumption_model');
		$this->load->module_model('silo_report', 'silo_report_model');
		$this->load->module_model('plant', 'plant_model');
		$this->load->module_model('electric_meter', 'electric_meter_model');
		$this->load->module_model('electric_report', 'electric_report_model');
		// $this->load->module_model('target', 'target_model');
		$this->load->module_model('department_materialproperty','department_materialproperty_model');

		// for pdf
		$this->load->library('fpdf/fpdf.php');

	}

	private function getTimeWhere($given_date,$i,$index){
		$dates = explode('-', $given_date);
		$where = array();
		if($i == 0){
			if($dates[1] > 3){
				$where[$index .' >= "'. $dates[0].'-04-01"'] = NULL;
				$where[$index. ' < "'.($dates[0]+1) .'-03-35"'] = NULL;
				$where[$index.' <='. $given_date];
			}else{
				$where[$index .' >= "'. ($dates[0]-1).'-04-01"'] = NULL;
				$where[$index. ' < "'.($dates[0]) .'-03-35"'] = NULL;
				$where[$index.' <='. $given_date];
			}
		}else if($i == 1){
			$this->db->like($index,$dates['0'].'-'.$dates['1']);
			$this->db->where($index.' <='. $given_date);
		}else{
			$where[$index] = $given_date;
			$where[$index.' <='. $given_date];
		}
		return $where;
	}

	private function whereDate($date, $index = NULL){
		$dates = explode('-', $date);
		if($index == NULL){
			$index = 'date_np';
		}

		if($dates[1] > 3){
			$where['year_array'][$index .' >= "'. $dates[0].'-04-01"'] = NULL;
			$where['year_array'][$index. ' < "'.($dates[0]+1) .'-03-35"'] = NULL;
			$where['year_array'][$index. ' <= "'.$date.'"'] = NULL;
		}else{
			$where['year_array'][$index .' >= "'. ($dates[0]-1).'-04-01"'] = NULL;
			$where['year_array'][$index. ' < "'.($dates[0]) .'-03-35"'] = NULL;
			$where['year_array'][$index. ' <= "'.$date.'"'] = NULL;
		}
		$where['month_array'][$index .' >= "'.($dates[0]) . '-' . $dates[1] . '-01"'] = NULL;
		$where['month_array'][$index .' <= "'.($dates[0]) . '-' . ($dates[1]+1) . '-01"'] = NULL;
		$where['month_array'][$index .' <= "'.$date.'"'] = NULL;
		$where['day_array'][$index] = $date;

		if($dates[1] > 3){
			$where['year'] = $index .' >= "'. $dates[0].'-04-01" AND '. $index. ' < "'.($dates[0]+1) .'-03-35" AND '.$index.' <= "'.$date .'"';
		}else{
			$where['year'] = $index .' >= "'. ($dates[0]-1).'-04-01" AND '. $index. ' < "'.($dates[0]) .'-03-35" AND '.$index.' <= "'.$date .'"';
		}
		$where['month'] = $index .' >= "'.($dates[0]) . '-' . $dates[1] . '-01" AND '. $index .' <= "'.($dates[0]) . '-' . ($dates[1]+1) . '-01" AND '.$index.' <= "'.$date .'"';
		$where['day'] = $index .' = "' . $date . '"';

		return $where;
	}

	function index($report_dept = NULL)
	{
		if($this->session->userdata('department_id') == 32){
			redirect('sales_register/report');
		}
		$data['dep'] = NULL;
		if($report_dept == NULL && $this->session->userdata('department_id') == 7){
			$report_dept = 'store';
		}
		$this->db->order_by('date_np desc');
		$input['plant'] = $this->plant_operation_model->getPlantOperations()->row_array();
		$this->db->order_by('date_np desc');
		$input['purchase'] = $this->purchase_model->getpurchases()->row_array();
		$this->db->order_by('dept_confirm_np_date desc');
		$input['dispatch'] = $this->dept_request_model->getDeptRequests()->row_array();
		$this->db->order_by('date_np desc');
		$input['daily_production_sale'] = $this->daily_production_sale_model->getDailyProductionSales()->row_array();
		$this->db->order_by('date_np desc');
		$input['silo_report'] = $this->silo_report_model->getSiloReports()->row_array();
		$this->db->order_by('date_np desc');
		$input['electric_report'] = $this->electric_report_model->getElectricReports()->row_array();

		if(!array_key_exists('date_np', $input['plant'])){
			$last_input['date_np'] = '2074-03-31';
			$last_input['date_en'] = '2017-07-15';
		}else{
			$last_input = $input['plant'];
		}
		$last_date = NULL;
		foreach ($input as $key => $value) {
			if(array_key_exists('date_np', $value) && $last_date < $value['date_np']){
				$last_input = $value;
				$last_date = $last_input['date_np'];
			}else if(array_key_exists('dept_confirm_np_date', $value) && $last_date < $value['dept_confirm_np_date']){
				$last_input = $value;
				$last_date = $last_input['dept_confirm_np_date'];
			}
		}

		if(!array_key_exists('date_en', $last_input) && !array_key_exists('dept_confirm_en_date', $last_input)){
			$last_input['date_en'] = '2017-07-15';
		}

		if(count($last_input) > 0 && array_key_exists('date_en',$last_input)){
			$data['date'] = $last_input['date_en'];
		}elseif(count($last_input) > 0 && array_key_exists('dept_confirm_en_date',$last_input)){
			$data['date'] = $last_input['dept_confirm_en_date'];
		}else{
			$data['date'] = date('Y-m-d');
		}
		
		$data['header'] = "Home";
		$data['view_page'] = 'home/index';
		$this->load->view($this->_container,$data);

	}

	/*for store report*/
	public function getStoreReport()
	{
		$date_np = $this->input->post('date');
		$data['sale_production'] = $this->getProductionSales($date_np);
		$data['silo_report'] = $this->getSiloReport($date_np);
		$data['inventory_report'] = $this->getInventoryReport($date_np);
		$data['operational_report'] = $this->getOperationalReport($date_np);
		$data['electric_meter_report'] = $this->getElectricMeterReport($date_np);
		$data['electric_report'] = $this->getElectricReport($date_np);

		// echo '<pre>';print_r($data);
	}

	private function getProductionSales($date_np)
	{

		$dates = explode('-', $date_np);
		$data['month'] = $this->months[$dates[1]];
		$data['year'] = $dates[0];
		$data['date'] = $date_np;

		$wheredates = $this->whereDate($date_np);
		$brand_type = $this->brand_type_model->getBrandTypes()->result_array();
		$brand = $this->brand_model->getBrands()->result_array();

		$data['brand_type'] = $this->daily_production_sale_model->getReport($wheredates,'brand_type')->result_array();
		$data['brand'] = $this->daily_production_sale_model->getReport($wheredates)->result_array();

		$data['opening_stock'] = $this->daily_production_sale_model->production_sales_stock($date_np)->result_array();
		$this->load->view('home/store/sale_production',$data);

		return $data;
	}

	private function getInventoryReport($date_np)
	{
		$data['diesel_consumption'] = array();
		$wheredates = $this->whereDate($date_np);

		$data['categories'] = $this->category_model->getCategories()->result_array();
		$this->purchase_model->joins = array('MATERIALS');
		$this->dept_request_model->joins = array('MATERIALS');

		$where_op['date_np <'] = $date_np;
		$this->db->group_by('purchases.material_id');
		$fields_p = "purchases.material_id, SUM(added_qty) AS total_purchase, category_id";
		$opening_purchase = $this->purchase_model->getPurchases($where_op,NULL,NULL,$fields_p)->result_array();

		$where_od['dept_confirm_np_date <'] = $date_np;
		$this->db->group_by('dept_requests.material_id');
		$fields_d = "dept_requests.material_id, SUM(accepted_quantity) AS total_dispatched, category_id";
		$temp_opening_dispatch = $this->dept_request_model->getDeptRequests($where_od,NULL,NULL,$fields_d)->result_array();

		$this->silo_report_model->joins  = array('MATERIALS');
		$where_osilo['date_np <'] = $date_np;
		$this->db->group_by('silo_reports.material_id');
		$fields_silo = "silo_reports.material_id, SUM(amount) AS total_dispatched, category_id";
		$opening_dispatch_silo = $this->silo_report_model->getSiloReports($where_osilo,NULL,NULL,$fields_silo)->result_array();
		foreach ($opening_dispatch_silo as $key => $value) {
			$temp_opening_dispatch[] = $value;
		}
		$opening_dispatch = array();
		foreach ($temp_opening_dispatch as $key => $value) {
			$material_id = $value['material_id'];
			if(array_key_exists($material_id, $opening_dispatch)){
				$opening_dispatch[$material_id]['total_dispatched'] += $value['total_dispatched'];
			}else{
				$opening_dispatch[$material_id] = $value;
			}
		}

		$where_p['date_np'] = $date_np;
		$this->db->group_by('purchases.material_id');
		$purchase = $this->purchase_model->getPurchases($where_p,NULL,NULL,$fields_p)->result_array();

		$where_d['dept_confirm_np_date'] = $date_np;
		$this->db->group_by('dept_requests.material_id');
		$dispatch = $this->dept_request_model->getDeptRequests($where_d,NULL,NULL,$fields_d)->result_array();

		$where_silo['date_np'] = $date_np;
		$this->db->group_by('silo_reports.material_id');
		$dispatch[] = $this->silo_report_model->getSiloReports($where_silo,NULL,NULL,$fields_silo)->result_array();

		foreach ($data['categories'] as $key => $value) {
			$where_m['category_id'] = $value['category_id'];
			$this->db->order_by('material_name');
			$materials = $this->material_model->getMaterials($where_m)->result_array();
			foreach ($materials as $index => $val) {
				$data['rows'][$val['category_id']][$val['material_id']] = array(
					'material_name' => $val['material_name']
				);
				// for diesel consumption according to the department
				if(strtoupper($val['material_name']) == 'DIESEL'){
					$this->dept_request_model->joins = array('DEPARTMENTS');
					$where_dept_dispatch['material_id'] = $val['material_id'];
					$where_dept_dispatch['store_confirm_np_date'] = $date_np;
					$this->db->group_by('department_id');
					$fields = 'name, accepted_quantity as consumption';
					$dept_dispatch = $this->dept_request_model->getDeptRequests($where_dept_dispatch, NULL, NULL, $fields)->result_array();
					$data['diesel_consumption'] = $dept_dispatch;
				}
			}
			foreach($opening_purchase as $index => $val){
				$data['rows'][$val['category_id']][$val['material_id']]['opening_purchase'] = $val['total_purchase'];
			}

			foreach($opening_dispatch as $index => $val){
					$data['rows'][$val['category_id']][$val['material_id']]['opening_dispatch'] = $val['total_dispatched'];
			}

			foreach($purchase as $index => $val){
				$data['rows'][$val['category_id']][$val['material_id']]['purchase'] = $val['total_purchase'];
			}
			// echo '<pre>';
			// print_r($value);
			// print_r($dispatch);
			// echo '</pre>';
			foreach($dispatch as $index => $val){
				if(count($val) > 0){
					if(array_key_exists('category_id', $val)){
						if($val['category_id'] == $value['category_id']){
							$data['rows'][$val['category_id']][$val['material_id']]['dispatch'] = $val['total_dispatched'];
						}
					}
					else{
						foreach ($val as $k => $v) {
							if($v['category_id'] == $value['category_id']){
								if(array_key_exists('dispatch', $data['rows'][$v['category_id']][$v['material_id']])){
									$data['rows'][$v['category_id']][$v['material_id']]['dispatch'] += $v['total_dispatched'];
								}else{
									$data['rows'][$v['category_id']][$v['material_id']]['dispatch'] = $v['total_dispatched'];
								}
							}
						}
					}
				}
			}
		}

		$this->load->view('home/store/inventory_report',$data);

		return $data;
	}

	private function getOperationalReport($date)
	{
		$this->db->order_by('date_np desc');
		$previous_date = $this->plant_operation_model->getPlantOperations(array('date_np <'=>$date))->row_array();
		if(count($previous_date) > 0){
			$previous_data = $this->plant_operation_model->getPlantOperations(array('date_np'=>$previous_date['date_np']))->result_array();
		}else{
			$previous_data = array();
		}
		$today_data = $this->plant_operation_model->getPlantOperations(array('date_np'=>$date))->result_array();
		$data['rows'] = $this->plant_model->getPlants()->result_array();
		foreach ($data['rows'] as $key => $value) {
			$data['rows'][$key]['start_hour'] = 0;
			$data['rows'][$key]['finish_hour'] = 0;
			foreach($previous_data as $k => $v){
				if($v['plant_id'] == $value['id']){
					$data['rows'][$key]['start_hour'] = $v['working_hour'];
					$data['rows'][$key]['finish_hour'] = $v['working_hour'];
				}
			}
			foreach($today_data as $k => $v){
				if($v['plant_id'] == $value['id']){
					$data['rows'][$key]['finish_hour'] = $v['working_hour'];
				}
			}
		}
		// $data['rows'] = $this->plant_operation_model->getReport($date)->result_array();
		$this->load->view('home/store/operation_report',$data);
		return $data;
	}

	private function getElectricMeterReport($date)
	{
		$this->db->order_by('date_np desc');
		$previous_date = $this->electric_meter_consumption_model->getElectricMeterConsumptions(array('date_np <'=>$date))->row_array();
		if(count($previous_date) > 0){
			$previous_data = $this->electric_meter_consumption_model->getElectricMeterConsumptions(array('date_np'=>$previous_date['date_np']))->result_array();
		}else{
			$previous_data = array();
		}
		$today_data = $this->electric_meter_consumption_model->getElectricMeterConsumptions(array('date_np'=>$date))->result_array();
		$data['rows'] = $this->electric_meter_model->getElectricMeters()->result_array();
		foreach ($data['rows'] as $key => $value) {
			$data['rows'][$key]['opening'] = 0;
			$data['rows'][$key]['closing'] = 0;
			foreach($previous_data as $k => $v){
				if($v['meter_id'] == $value['id']){
					$data['rows'][$key]['opening'] = $v['consumption'];
					$data['rows'][$key]['closing'] = $v['consumption'];
				}
			}
			foreach($today_data as $k => $v){
				if($v['meter_id'] == $value['id']){
					$data['rows'][$key]['closing'] = $v['consumption'];
				}
			}
		}
		// $data['rows'] = $this->electric_meter_consumption_model->getReport($date)->result_array();
		$this->load->view('home/store/meter_electric_report',$data);
		return $data;
	}

	// private function getElectricMeterReport($date)
	// {
	// 	// $this->db->order_by('date_np desc');
	// 	// $this->db->where('date_np',$date);
	// 	// $previous_data = $this->electric_meter_consumption_model->getE

	// 	$data['rows'] = $this->electric_meter_consumption_model->getReport($date)->result_array();
	// 	$this->load->view('home/store/meter_electric_report',$data);
	// 	return $data;
	// }

	// silo report

	private function getSiloReport($date_np)
	{
		$records['brand_types'] = $this->brand_type_model->getBrandTypes()->result_array();
		
		$this->db->group_by('type_id');
		$this->silo_report_model->joins = array('BRAND_TYPE');
		$fields = 'SUM(amount) AS production';
		$where['date_np < '] = $date_np;
		$records['opening_production'] = $this->silo_report_model->getSiloReports($where,NULL,NULL,$fields)->result_array();
		// echo $this->db->last_query();
		
		$this->db->group_by('type_id');
		$records['production'] = $this->silo_report_model->getSiloReports(array('date_np' => $date_np),NULL,NULL,$fields)->result_array();

		$fields = 'SUM(sales) AS sales, SUM(production) AS production';
		$this->db->group_by('type_id');
		$this->daily_production_sale_model->joins = array('BRAND_TYPE');
		$records['opening_sales'] = $this->daily_production_sale_model->getDailyProductionSales($where,NULL,NULL,$fields)->result_array();

		$this->db->group_by('type_id');
		$records['sales'] = $this->daily_production_sale_model->getDailyProductionSales(array('date_np' => $date_np),NULL,NULL,$fields)->result_array();

		$this->db->where('key','silo');
		$year_openings = $this->db->get('settings')->result_array();
// echo '<pre>';print_r($year_openings);exit;

		foreach($records['brand_types'] as $key => $value){
			$data['rows'][$key]['type_name'] = $value['name'];
			$data['rows'][$key]['opening'] = 0;
			$data['rows'][$key]['production'] = 0;
			$data['rows'][$key]['sales'] = 0;
			$data['rows'][$key]['opening'] = 0;
			$data['rows'][$key]['balance'] = 0;

			foreach ($year_openings as $k => $val) {
				if($value['name'] == $val['code']){
					$data['rows'][$key]['opening'] += $val['value'];
					$data['rows'][$key]['balance'] += $val['value'];
					break;
				}
			}
			foreach ($records['opening_production'] as $k => $val) {
				if($value['name'] == $val['type']){
					$data['rows'][$key]['opening'] += $val['production'];
					$data['rows'][$key]['balance'] += $val['production'];
					break;
				}
			}
			foreach ($records['opening_sales'] as $k => $val) {
				if($value['name'] == $val['brand_type']){
					$data['rows'][$key]['opening'] -= ($val['production'] * 50 / 1000);
					$data['rows'][$key]['balance'] -= ($val['production'] * 50 / 1000);
					break;
				}
			}
			foreach ($records['production'] as $k => $val) {
				if($value['name'] == $val['type']){
					$data['rows'][$key]['production'] += $val['production'];
					$data['rows'][$key]['balance'] += $val['production'];
					break;
				}
			}
			foreach ($records['sales'] as $k => $val) {
				if($value['name'] == $val['brand_type']){
					$data['rows'][$key]['sales'] += ($val['production'] * 50 / 1000);
					$data['rows'][$key]['balance'] -= ($val['production'] * 50 / 1000);
					break;
				}
			}
		}

		$this->load->view('home/store/silo_report',$data);

		return $data;

	}

	public function getElectricReport($date_np)
	{
		$where['date_np'] = $date_np;
		$data['rows'] = $this->electric_report_model->getElectricReports($where)->result_array();
		$this->load->view('home/store/electric_report',$data);
		return $data;
	}

	// printing pdf
	public function pdf(){
		$date_np = $this->input->post('date_np');

		$data['sale_production'] = $this->getProductionSales($date_np);
		$data['silo_report'] = $this->getSiloReport($date_np);
		$data['inventory_report'] = $this->getInventoryReport($date_np);
		$data['operational_report'] = $this->getOperationalReport($date_np);
		$data['electric_meter_report'] = $this->getElectricMeterReport($date_np);
		$data['electric_report'] = $this->getElectricReport($date_np);

		// echo '<pre>';
		// print_r($data);exit;

		$filename = "Daily_Summary_overview_report($date_np).pdf";

		// Send headers
		header("Content-Type: application/pdf");
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header('Content-Disposition: attachment; filename="'.'"');
		header("Content-Transfer-Encoding: binary ");

		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',20);

		$pdf->Cell(80);
	    // Title
	    $pdf->Cell(30,10,'Reliance Cement Pvt. Ltd.',0,0,'C');
		$pdf->SetFont('Arial','B',12);
		$pdf->Ln();//new line

		$pdf->Cell(80);
		$pdf->Cell(30,10,'Daily Summary Factory OverView Report',0,0,'C');
		$pdf->SetFont('Arial','B',8);
		$pdf->Ln();//new line
		
		$pdf->Cell(40,10,'Date : '.$date_np);
		$pdf->Ln();

		// production sales report
		// brand_type
		$pdf->Cell(103,6, ('Type Wise Production Summary'));
		$pdf->Cell(100,6, ('Type Wise Sales Summary'));
		$pdf->Ln();//new line

		$pdf->Cell(22,6, ('Type'),1);
		$pdf->Cell(22,6, ('Daily'),1);
		$pdf->Cell(22,6, ('Monthly'),1);
		$pdf->Cell(22,6, ('Yearly'),1);
		$pdf->Cell(15,6, (''));
		$pdf->Cell(22,6, ('Type'),1);
		$pdf->Cell(22,6, ('Daily'),1);
		$pdf->Cell(22,6, ('Monthly'),1);
		$pdf->Cell(22,6, ('Yearly'),1);
		$pdf->Ln();//new line
		$total_production = 0;
		$monthly_total_production = 0;
		$yearly_total_production = 0;
		$total_sales = 0;
		$monthly_total_sales = 0;
		$yearly_total_sales = 0;
		foreach ($data['sale_production']['brand_type'] as $key => $value) {
			$pdf->Cell(22,6, ($value['brand_type']),1);
			$pdf->Cell(22,6, (number_format($value['total_production'],2)),1);
			$pdf->Cell(22,6, (number_format($value['monthly_total_production'],2)),1);
			$pdf->Cell(22,6, (number_format($value['yearly_total_production'],2)),1);
			$pdf->Cell(15,6, (''));
			$pdf->Cell(22,6, ($value['brand_type']),1);
			$pdf->Cell(22,6, (number_format($value['total_sales'],2)),1);
			$pdf->Cell(22,6, (number_format($value['monthly_total_sales'],2)),1);
			$pdf->Cell(22,6, (number_format($value['yearly_total_sales'],2)),1);
			$pdf->Ln();//new line

			$total_production += $value['total_production'];
			$monthly_total_production += $value['monthly_total_production'] ;
			$yearly_total_production +=  $value['yearly_total_production'];
			$total_sales +=  $value['total_sales'];
			$monthly_total_sales +=  $value['monthly_total_sales'];
			$yearly_total_sales +=  $value['yearly_total_sales'];
		}
		$pdf->Cell(22,6, ('Total'),1);
		$pdf->Cell(22,6, (number_format($total_production,2)),1);
		$pdf->Cell(22,6, (number_format($monthly_total_production,2)),1);
		$pdf->Cell(22,6, (number_format($yearly_total_production,2)),1);
		$pdf->Cell(15,6, (''));
		$pdf->Cell(22,6, ('Total'),1);
		$pdf->Cell(22,6, (number_format($total_sales,2)),1);
		$pdf->Cell(22,6, (number_format($monthly_total_sales,2)),1);
		$pdf->Cell(22,6, (number_format($yearly_total_sales,2)),1);
		$pdf->Ln();//new line

		$pdf->Ln();//new line
		$pdf->Cell(103,6, ('Brand Wise Production Summary'));
		$pdf->Cell(100,6, ('Brand Wise Sales Summary'));
		$pdf->Ln();//new line

		// brand_name
		$pdf->Cell(22,6, ('Brand'),1);
		$pdf->Cell(22,6, ('Daily'),1);
		$pdf->Cell(22,6, ('monthly'),1);
		$pdf->Cell(22,6, ('Yearly'),1);
		$pdf->Cell(15,6, (''));
		$pdf->Cell(22,6, ('Brand'),1);
		$pdf->Cell(22,6, ('Daily'),1);
		$pdf->Cell(22,6, ('monthly'),1);
		$pdf->Cell(22,6, ('Yearly'),1);
		$pdf->Ln();//new line
		$total_production = 0;
		$monthly_total_production = 0;
		$yearly_total_production = 0;
		$total_sales = 0;
		$monthly_total_sales = 0;
		$yearly_total_sales = 0;

		$total_production = 0;
		$monthly_total_production = 0;
		$yearly_total_production = 0;
		$total_sales = 0;
		$monthly_total_sales = 0;
		$yearly_total_sales = 0;

		foreach ($data['sale_production']['brand'] as $key => $value) {
			$pdf->Cell(22,6, ($value['brand_name']),1);
			$pdf->Cell(22,6, (number_format($value['total_production'],2)),1);
			$pdf->Cell(22,6, (number_format($value['monthly_total_production'],2)),1);
			$pdf->Cell(22,6, (number_format($value['yearly_total_production'],2)),1);
			$pdf->Cell(15,6, (''));
			$pdf->Cell(22,6, ($value['brand_name']),1);
			$pdf->Cell(22,6, (number_format($value['total_sales'],2)),1);
			$pdf->Cell(22,6, (number_format($value['monthly_total_sales'],2)),1);
			$pdf->Cell(22,6, (number_format($value['yearly_total_sales'],2)),1);
			$pdf->Ln();//new line

			$total_production += $value['total_production'];
			$monthly_total_production += $value['monthly_total_production'] ;
			$yearly_total_production +=  $value['yearly_total_production'];
			$total_sales +=  $value['total_sales'];
			$monthly_total_sales +=  $value['monthly_total_sales'];
			$yearly_total_sales +=  $value['yearly_total_sales'];
		}
		$pdf->Cell(22,6, ('Total'),1);
		$pdf->Cell(22,6, (number_format($total_production,2)),1);
		$pdf->Cell(22,6, (number_format($monthly_total_production,2)),1);
		$pdf->Cell(22,6, (number_format($yearly_total_production,2)),1);
		$pdf->Cell(15,6, (''));
		$pdf->Cell(22,6, ('Total'),1);
		$pdf->Cell(22,6, (number_format($total_sales,2)),1);
		$pdf->Cell(22,6, (number_format($monthly_total_sales,2)),1);
		$pdf->Cell(22,6, (number_format($yearly_total_sales,2)),1);
		$pdf->Ln();//new line

		$pdf->Ln();//new line

		// opening
		$pdf->Cell(100,6, ('Sale/Production Summary'));
		$pdf->Ln();//new line

		$pdf->Cell(38,6, ('Type'),1);
		$pdf->Cell(38,6, ('Opening'),1);
		$pdf->Cell(38,6, ('Production'),1);
		$pdf->Cell(38,6, ('sales'),1);
		$pdf->Cell(38,6, ('Balance'),1);
		$pdf->Ln();//new line
		foreach ($data['sale_production']['opening_stock'] as $key => $value) {
			$pdf->Cell(38,6, ($value['brand_type']),1);
			$pdf->Cell(38,6, (number_format($value['opening'],2)),1);
			$pdf->Cell(38,6, (number_format($value['production'],2)),1);
			$pdf->Cell(38,6, (number_format($value['sales'],2)),1);
			$pdf->Cell(38,6, (number_format($value['opening'] + $value['production'] - $value['sales'],2)),1);
			$pdf->Ln();//new line
		}
		$pdf->Ln();//new line

		// silo report
		$pdf->Cell(100,6, ('Silo Summary'));
		$pdf->Ln();//new line

		$pdf->Cell(38,6, ('Type'),1);
		$pdf->Cell(38,6, ('Opening'),1);
		$pdf->Cell(38,6, ('Production'),1);
		$pdf->Cell(38,6, ('sales'),1);
		$pdf->Cell(38,6, ('Balance'),1);
		$pdf->Ln();//new line
		foreach ($data['silo_report']['rows'] as $key => $value) {
			$pdf->Cell(38,6, ($value['type_name']),1);
			$pdf->Cell(38,6, (number_format($value['opening'],3)),1);
			$pdf->Cell(38,6, (number_format($value['production'],3)),1);
			$pdf->Cell(38,6, (number_format($value['sales'],3)),1);
			$pdf->Cell(38,6, (number_format($value['balance'],3)),1);
			$pdf->Ln();//new line
		}
		$pdf->Ln();//new line

		//inventory report
		foreach ($data['inventory_report']['categories'] as $index => $val) {
			$pdf->Cell(100,6, (ucwords($val['category_name']) . ' Stock Report'));
			$pdf->Ln();//new line

			$pdf->Cell(38,6, ('Material'),1);
			$pdf->Cell(38,6, ('Opening'),1);
			$pdf->Cell(38,6, ('Purchase'),1);
			$pdf->Cell(38,6, ('Consumed'),1);
			$pdf->Cell(38,6, ('Balance'),1);
			$pdf->Ln();//new line

			$total_opening = 0;
			$total_purchase = 0;
			$total_consumed = 0;
			$total_balance = 0;
			$diesel_exist = 0;
			foreach ($data['inventory_report']['rows'][$val['category_id']] as $key => $value) {
                $opening = (array_key_exists('opening_purchase', $value))?$value['opening_purchase']:0;
                if(array_key_exists('opening_dispatch', $value)){
                    $opening = $opening - $value['opening_dispatch'];
                } 
				$pdf->Cell(38,6, ($value['material_name']),1);
				$pdf->Cell(38,6, (number_format($opening,2)),1);
				$pdf->Cell(38,6, number_format((array_key_exists('purchase', $value)?$value['purchase']:0),2) ,1);
				$pdf->Cell(38,6, number_format((array_key_exists('dispatch', $value)?$value['dispatch']:0),2) ,1);
				$balance = $opening +((array_key_exists('purchase', $value))?$value['purchase']:0) - ((array_key_exists('dispatch', $value))?$value['dispatch']:0);
				$pdf->Cell(38,6, (number_format($balance,2)),1);
				$pdf->Ln();//new line
				if(strtoupper($value['material_name']) == 'DIESEL'){
					$diesel_exist = 1;
					
				}

				$total_opening += $opening;
				$total_purchase += array_key_exists('purchase', $value)?$value['purchase']:0;
				$total_consumed += array_key_exists('dispatch', $value)?$value['dispatch']:0;
				$total_balance += $balance;

				// for disele consumption according to the department
			}

			$pdf->Cell(38,6, ('Total'),1);
			$pdf->Cell(38,6, (number_format($total_opening,2)),1);
			$pdf->Cell(38,6, (number_format($total_purchase,2)),1);
			$pdf->Cell(38,6, (number_format($total_consumed,2)),1);
			$pdf->Cell(38,6, (number_format($total_balance,2)),1);
			$pdf->Ln();//new line
			
			$pdf->Ln();//new line
			
			if($diesel_exist == 1){
				$pdf->Cell(100,6, (' Diesel Consumption'));
				$pdf->Ln();//new line
				$sn = 'a';
				$pdf->Cell(38,6, ('SN.'),1);
				$pdf->Cell(38,6, ('Department'),1);
				$pdf->Cell(38,6, ('Consumption'),1);
				$pdf->Ln();//new line

					foreach ($data['inventory_report']['diesel_consumption'] as $dept) {
						$pdf->Cell(38,6, ($sn),1);
						$pdf->Cell(38,6, ($dept['name']),1);
						$pdf->Cell(38,6, (number_format($dept['consumption'],2)),1);
						$sn++;
						$pdf->Ln();//new line
					}

				$pdf->Ln();//new line
			}
		}

		// operation report
		$pdf->Cell(100,6, ('Operational Report (Plant Running Report)'));
		$pdf->Ln();//new line

		$pdf->Cell(47,6, ('Plant'),1);
		$pdf->Cell(47,6, ('Opening'),1);
		$pdf->Cell(47,6, ('closing'),1);
		$pdf->Cell(47,6, ('Running Hour'),1);
		$total_opening = 0;
		$totsl_closing = 0;
		$total_running = 0;
		$pdf->Ln();//new line
		foreach ($data['operational_report']['rows'] as $key => $value) {
			$pdf->Cell(47,6, ($value['name']),1);
			$pdf->Cell(47,6, (number_format($value['start_hour'],2)),1);
			$pdf->Cell(47,6, (number_format($value['finish_hour'],2)),1);
			$pdf->Cell(47,6, (number_format($value['finish_hour'] - $value['start_hour'],2)),1);
			$pdf->Ln();//new line

			$total_opening += $value['start_hour'];
			$totsl_closing += $value['finish_hour'];
			$total_running += $value['finish_hour'] - $value['start_hour'];
		}
		$pdf->Cell(47,6, ('Total'),1);
		$pdf->Cell(47,6, (number_format($total_opening,2)),1);
		$pdf->Cell(47,6, (number_format($totsl_closing,2)),1);
		$pdf->Cell(47,6, (number_format($total_running,2)),1);
		$pdf->Ln();//new line
		$pdf->Ln();//new line

		// electric meter report
		$pdf->Cell(100,6, ('Electrical Unit Report (Meter Reding)'));
		$pdf->Ln();//new line

		$pdf->Cell(47,6, ('Meter'),1);
		$pdf->Cell(47,6, ('Opening'),1);
		$pdf->Cell(47,6, ('closing'),1);
		$pdf->Cell(47,6, ('Consumed'),1);
		$total_opening = 0;
		$totsl_closing = 0;
		$total_consumed = 0;
		$pdf->Ln();//new line
		foreach ($data['electric_meter_report']['rows'] as $key => $value) {
			$pdf->Cell(47,6, ($value['meter']),1);
			$pdf->Cell(47,6, (number_format($value['opening'],2)),1);
			$pdf->Cell(47,6, (number_format($value['closing'],2)),1);
			$pdf->Cell(47,6, (number_format($value['closing'] - $value['opening'],2)),1);
			$pdf->Ln();//new line

			$total_opening += $value['opening'];
			$totsl_closing += $value['closing'];
			$total_consumed += $value['closing'] - $value['opening'];
		}
		$pdf->Cell(47,6, ('Total'),1);
		$pdf->Cell(47,6, (number_format($total_opening,2)),1);
		$pdf->Cell(47,6, (number_format($totsl_closing,2)),1);
		$pdf->Cell(47,6, (number_format($total_consumed,2)),1);
		$pdf->Ln();//new line
		$pdf->Ln();//new line

		// electric report
		$pdf->Cell(100,6, ('Electrical Unit Report'));
		$pdf->Ln();//new line

		$pdf->Cell(20,6, ('Shift'),1);
		$pdf->Cell(20,6, ('Power Cut'),1);
		$pdf->Cell(20,6, ('Mill(NEA)'),1);
		$pdf->Cell(20,6, ('Mill(DG)'),1);
		$pdf->Cell(20,6, ('NEA Light'),1);
		$pdf->Cell(20,6, ('Breakdown'),1);
		$pdf->Cell(60,6, ('Reason'),1);
		$pdf->Cell(20,6, ('Total'),1);
		$pdf->Ln();//new line

		$pdf->SetWidths(array(20,20,20,20,20,20,60,20));
		srand(microtime()*1000000);
		foreach ($data['electric_report']['rows'] as $key => $value) {
		    $pdf->Row(
		    	array(
		    		($value['shift']),
			    	(number_format($value['power_cut'],2)),
			    	(number_format($value['cement_mill_operation_nea'],2)),
			    	(number_format($value['cement_mill_operation_dg'],2)),
			    	(number_format($value['only_lighiting_nea'],2)),
			    	(number_format($value['breakdown'],2)),
			    	$value['reason'],
			    	(number_format($value['total'],2))
		    	));
		}

		$pdf->Output($filename,'D');
   	
	}
	
}

/* End of file welcome.php */
/* Location: ./modules/welcome/controllers/welcome.php */