<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();
		
		
		$this->load->library('custom_database');

	}

	public function index(){
		
	}

	public function database_transfer(){
		$this->custom_database->create_db();
	}
}