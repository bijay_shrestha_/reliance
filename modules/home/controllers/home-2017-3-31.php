<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Member_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->module_model('material', 'material_model');
		$this->load->module_model('purchase', 'purchase_model');
		$this->load->module_model('material_department', 'material_department_model');
		$this->load->module_model('material_property', 'material_property_model');
		$this->load->module_model('consumption', 'consumption_model');
		$this->load->module_model('scrap_type', 'scrap_type_model');
		$this->load->module_model('scrap_record', 'scrap_record_model');
		$this->load->module_model('calander_shift_report', 'calander_shift_report_model');
		$this->load->module_model('tL_reason_full', 'tL_reason_full_model');
		$this->load->module_model('tL_type', 'tL_type_model');
		$this->load->module_model('time_loss', 'time_loss_model');
		$this->load->module_model('target', 'target_model');
		$this->load->module_model('calander_shift_report', 'calander_shift_report_model');
		$this->load->module_model('laminating_shift_report', 'laminating_shift_report_model');
		$this->load->module_model('print_shift_report', 'print_shift_report_model');
		$this->load->module_model('rexin_coating_report', 'rexin_coating_report_model');
		$this->load->module_model('printloss_type', 'printloss_type_model');
		$this->load->module_model('printloss_full', 'printloss_full_model');
		$this->load->module_model('print_report_reason', 'print_report_reason_model');
		$this->load->module_model('department_materialproperty','department_materialproperty_model');
		$this->load->module_model('rexin_product','rexin_product_model');
		$this->load->module_model('target_wt','target_wt_model');

		// for pdf
		$this->load->library('fpdf/fpdf.php');

	}

	private function getTimeWhere($given_date,$i,$index){
		$dates = explode('-', $given_date);
		$where = array();
		if($i == 0){
			if($dates[1] > 3){
				$where[$index .' > "'. $dates[0].'-04-01"'] = NULL;
				$where[$index. ' < "'.($dates[0]+1) .'-03-35"'] = NULL;
			}else{
				$where[$index .' > "'. ($dates[0]-1).'-04-01"'] = NULL;
				$where[$index. ' < "'.($dates[0]) .'-03-35"'] = NULL;
			}
		}else if($i == 1){
			$this->db->like($index,$dates['0'].'-'.$dates['1']);
		}else{
			$where[$index] = $given_date;
		}
		return $where;
	}

	function index($report_dept = NULL)
	{
		$data['dep'] = NULL;
		if($this->session->userdata('department_id') == 20 || $report_dept == 'office'){
			redirect('office');
		}
		if($report_dept == NULL && $this->session->userdata('department_id') == 31){
			$report_dept = 'store';
		}
		if($this->session->userdata('department_id') == 7 || $report_dept == 'store'){
			$this->material_department_model->joins = array('MATERIALS');
			$this->db->order_by('material_name');
			$this->db->group_by('material_name');
			$this->db->where('group_id', 1)->or_where('group_id', 3)->or_where('group_id', 4)->or_where('group_id', 5); 
			$data['materials'] = $this->material_department_model->getMaterialDepartments()->result_array();
			$i = 0;
			foreach($data['materials'] as $material){
				$data['materials'][$i]['total'] = 0;

				$where = array();
				$where['material_id'] = $material['material_id'];

				// total purchase
				$purchase = $this->purchase_model->getAllPurchases($where)->row_array();
				// total dispatch
				$where['store_status'] = 1;
				$where['department_status'] = 1;
				$dispatch = $this->dept_request_model->getAllDispatches($where)->row_array();
				$data['materials'][$i]['stock'] = $purchase['total_purchase'] - $dispatch['all_dispatched'];

				$where = array('material_id'=>$material['material_id']);
				// for mixing
				$where['department_id'] = 1;
				$mixing = $this->consumption_model->getAllConsumptions($where)->row_array();
				 $this->db->group_by('nepali_date');
				$count = $this->consumption_model->getDaywiseCount(array('department_id'=>$where['department_id']));

				if($count != 0){
					$data['materials'][$i]['mixing'] = $mixing['item_amount']/$count;
					$data['materials'][$i]['total'] += $mixing['item_amount']/$count;
				}else{
					$data['materials'][$i]['mixing'] = 0;
				}

				// for printing
				$where['department_id'] = 3;
				$printing = $this->consumption_model->getAllConsumptions($where)->row_array();
				 $this->db->group_by('nepali_date');
				$count = $this->consumption_model->getDaywiseCount(array('department_id'=>$where['department_id']));
				if($count != 0){
					$data['materials'][$i]['printing'] = $printing['item_amount']/$count;
					$data['materials'][$i]['total'] += $printing['item_amount']/$count;
				}else{
					$data['materials'][$i]['printing'] = 0;
				}

				// for lamination
				$where['department_id'] = 4;
				if($material['category_id'] != 16){
					$lamination = $this->consumption_model->getAllConsumptions($where)->row_array();
				}else{
					$lamination = $this->dept_request_model->getAllDispatches(array('department_id'=>4,'material_id'=>$material['material_id']))->row_array();
				}
				if($material['category_id'] != 16){
				 	$this->db->group_by('nepali_date');
					$count = $this->consumption_model->getDaywiseCount(array('department_id'=>$where['department_id']));
				}else{
					// echo $this->db->last_query();
					$this->db->group_by('date-np');
					$this->db->where('date-np <> ', '0000-00-00');
					$count = $this->laminating_shift_report_model->getDaywiseCount();
				}
				if($count != 0){
					if($material['category_id'] != 16){
						$data['materials'][$i]['lamination'] = $lamination['item_amount']/$count;
						$data['materials'][$i]['total'] += $lamination['item_amount']/$count;
					}else{
						$data['materials'][$i]['lamination'] = $lamination['all_dispatched']/$count;
						$data['materials'][$i]['total'] += $lamination['all_dispatched']/$count;
					}
				}else{
					$data['materials'][$i]['lamination'] = 0;
				}

				// for rexin
				$where['department_id'] = 5;
				$rexin = $this->consumption_model->getAllConsumptions($where)->row_array();
				 $this->db->group_by('nepali_date');
				$count = $this->consumption_model->getDaywiseCount(array('department_id'=>$where['department_id']));
				
				if($count != 0){
					$data['materials'][$i]['rexin'] = $rexin['item_amount']/$count;
					$data['materials'][$i]['total'] += $rexin['item_amount']/$count;
				}else{
					$data['materials'][$i]['rexin'] = 0;
				}

				$i++;
			}
			// for fabric of rexin
			$fabrics = $this->material_model->getMaterials(array('category_id'=>18))->result_array();
			
			foreach($fabrics as $fabric){
				$data['materials'][$i]['total'] = 0;
				$data['materials'][$i]['material_id'] = $fabric['material_id'];
				$rexin = $this->dept_request_model->getAllDispatches(array('department_id'=>5,'material_id'=>$fabric['material_id']))->row_array();
				 $this->db->group_by('date_np');
				$count = $this->rexin_coating_report_model->getDaywiseCount();
				if($count != 0){
					$data['materials'][$i]['material_name'] = $fabric['material_name'];
					$data['materials'][$i]['rexin'] = $rexin['all_dispatched']/$count;
					$data['materials'][$i]['mixing'] = 0;
					$data['materials'][$i]['lamination'] = 0;
					$data['materials'][$i]['printing'] = 0;
					$data['materials'][$i]['total'] += $rexin['all_dispatched']/$count;
				}else{
					$data['materials'][$i]['rexin'] = 0;
					$data['materials'][$i]['rexin'] = 0;
					$data['materials'][$i]['mixing'] = 0;
					$data['materials'][$i]['lamination'] = 0;
					$data['materials'][$i]['printing'] = 0;
					$data['materials'][$i]['total'] += 0;
				}
				$where = array();
				$where['material_id'] = $fabric['material_id'];

				// total purchase
				$purchase = $this->purchase_model->getAllPurchases($where)->row_array();
				// total dispatch
				$where['store_status'] = 1;
				$where['department_status'] = 1;
				$dispatch = $this->dept_request_model->getAllDispatches($where)->row_array();
				$data['materials'][$i]['stock'] = $purchase['total_purchase'] - $dispatch['all_dispatched'];
				
				$i++;
			}
			$this->load->module_model('lc_material','lc_material_model');
			$this->lc_material_model->joins = array('LC');
			$this->db->group_by('material_id');
			$lc = $this->lc_material_model->getLcMaterials(array('closed' => 0))->result_array();
			foreach ($lc as $key => $value) {
				$data['lc_list'][] = $value['material_id'];
			}

			$data['header'] = "Home";
			$data['view_page'] = 'home/store_index';
			$this->load->view($this->_container,$data);
		}else if($this->session->userdata('department_id') == 1 || $report_dept == 'mixing'){

			$data['header'] = "Home";
			$data['view_page'] = 'home/mixing/index';
			$this->load->view($this->_container,$data);
		}else if($this->session->userdata('department_id') == 2 || $report_dept == 'calendar'){

			$data['header'] = "Home";
			$data['view_page'] = 'home/calendar/index';
			$this->load->view($this->_container,$data);
		}else if($this->session->userdata('department_id') == 4 || $report_dept == 'lamination'){

			$data['header'] = "Home";
			$data['view_page'] = 'home/laminating/index';
			$this->load->view($this->_container,$data);
		}else if($this->session->userdata('department_id') == 3 || $report_dept == 'printing'){

			$data['dep'] = 3;
			$data['header'] = "Home";
			$data['view_page'] = 'home/index';
			$this->load->view($this->_container,$data);
		}else if($this->session->userdata('department_id') == 5 || $report_dept == 'rexin'){

			$data['dep'] = 5;
			$data['header'] = "Home";
			$data['view_page'] = 'home/index';
			$this->load->view($this->_container,$data);
		}else if($this->session->userdata('department_id') == 21 || $report_dept == 'ktm_office'){
			$data['dep'] = 21;
			if($this->input->get('month')){
				$data['month'] = $this->input->get('month');
			}else{
				$data['month'] = '00';
			}
			$data['header'] = "Home";
			$data['view_page'] = 'home/index';
			$this->load->view($this->_container,$data);
		}else{
			$data['header'] = "Home";
			$data['view_page'] = 'home/index';
			$this->load->view($this->_container,$data);
		}

	}
	// for purchase/dispatch report for store
	public function getDispatchPurchase(){
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$options = $this->input->post('options');
			$where['material_id'] = $this->input->post('material_id');
			if($where['material_id'] != '' && $options != ''){
				if($options == 'purchase'){
					if($start_date != ''){
						echo $where['date_np >= "'.$start_date.'"'] = NULL;
					}
					if($end_date != ''){
						echo $where['date_np <= "'.$end_date.'"'] = NULL;
					}
					$this->purchase_model->joins = array('DEALERS');
					$data['purchases'] = $this->purchase_model->getPurchases($where)->result_array();
					$this->load->view('home/store_purchase_table',$data);
				}else{
					if($start_date != ''){
						echo $where['store_confirm_np_date >= "'.$start_date.'"'] = NULL;
					}
					if($end_date != ''){
						echo $where['store_confirm_np_date <= "'.$end_date.'"'] = NULL;
					}
					$where['department_status'] = 1;
					$this->dept_request_model->joins = array('DEPARTMENTS');
					$data['dispatches'] = $this->dept_request_model->getDeptRequests($where)->result_array();
					$this->load->view('home/store_dispatch_table',$data);
				}
			}else{
				echo '<span style="color:red">Please select material</span>';
			}
	}
	public function getAllDispatchPurchase(){
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
			$this->material_department_model->joins = array('MATERIALS');
			$this->db->group_by('material_name');
			$data['materials'] = $this->material_department_model->getMaterialDepartments()->result_array();
			foreach($data['materials'] as $key => $material){
				$where_dispatch['material_id'] = $where['material_id'] = $material['material_id'];
				if($start_date != ''){
					echo $where['date_np >= "'.$start_date.'"'] = NULL;
				}
				if($end_date != ''){
					echo $where['date_np <= "'.$end_date.'"'] = NULL;
				}
				$opening = 0;
				if($start_date != 0){
					$opening_purchase = $this->purchase_model->getAllPurchases(array('date_np <'=>$start_date,'material_id'=>$material['material_id'], ))->row_array();
					$opening_dispatch = $this->dept_request_model->getAllDispatches(array('store_confirm_np_date <'=>$start_date,'material_id'=>$material['material_id'],'department_status'=>1))->row_array();
					$opening = $opening_purchase['total_purchase'] - $opening_dispatch['all_dispatched'];
				}
				$data['materials'][$key]['opening'] = $opening;
				$purchase = $this->purchase_model->getAllPurchases($where)->row_array();
				$data['materials'][$key]['purchases'] = $purchase['total_purchase'];

				if($start_date != ''){
					echo $where_dispatch['store_confirm_np_date >= "'.$start_date.'"'] = NULL;
				}
				if($end_date != ''){
					echo $where_dispatch['store_confirm_np_date <= "'.$end_date.'"'] = NULL;
				}
				$where_dispatch['department_status']=1;
				$dispatch = $this->dept_request_model->getAllDispatches($where_dispatch)->row_array();
				$data['materials'][$key]['dispatches'] = $dispatch['all_dispatched'];
			}
			$fabrics = $this->material_model->getMaterials(array('category_id'=>18))->result_array();
			foreach($fabrics as $fabric){
				$data['materials'][++$key] = $fabric;
				$where_dispatch['material_id'] = $where['material_id'] = $fabric['material_id'];
				if($start_date != ''){
					echo $where['date_np >= "'.$start_date.'"'] = NULL;
				}
				if($end_date != ''){
					echo $where['date_np <= "'.$end_date.'"'] = NULL;
				}
				$opening = 0;
				if($start_date != 0){
					$opening_purchase = $this->purchase_model->getAllPurchases(array('date_np <'=>$start_date,'material_id'=>$fabric['material_id']))->row_array();
					$opening_dispatch = $this->dept_request_model->getAllDispatches(array('store_confirm_np_date <'=>$start_date,'material_id'=>$fabric['material_id']))->row_array();
					$opening = $opening_purchase['total_purchase'] - $opening_dispatch['all_dispatched'];
				}
				$data['materials'][$key]['opening'] = $opening;
				$purchase = $this->purchase_model->getAllPurchases($where)->row_array();
				$data['materials'][$key]['purchases'] = $purchase['total_purchase'];

				if($start_date != ''){
					echo $where_dispatch['store_confirm_np_date >= "'.$start_date.'"'] = NULL;
				}
				if($end_date != ''){
					echo $where_dispatch['store_confirm_np_date <= "'.$end_date.'"'] = NULL;
				}
				$where_dispatch['department_status']=1;
				$dispatch = $this->dept_request_model->getAllDispatches($where_dispatch)->row_array();
				$data['materials'][$key]['dispatches'] = $dispatch['all_dispatched'];
			}

			$this->load->view('home/store_all_table',$data);
		}
	//for mixing report
	public function getMixingReport(){
		// echo '<pre>';
		$date = $this->input->post('date');
		$data['date'] = $date;
		$data['working_days'] = $this->consumption_model->getWorkingDays(1,$date);
		$where['group_id'] = 1;
		$this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
		$this->db->order_by('order');
		$data['materials'] = $this->material_department_model->getMaterialDepartments($where)->result_array();
		$dates = explode('-',$date);
		 $date_where = '';

				$this->db->order_by('nepali_date','desc');
		$last_entry = $this->consumption_model->getConsumptions(array('department_id'=>1))->row_array();
		$data['last_entry_date'] = explode('-',$last_entry['nepali_date']);
		foreach($data['last_entry_date'] as $i => $date){
			if($i == 2){
				$date_where .= "-".$date;
				$index = 'daily';
			}else if($i == 1){
				$date_where .= "-".$date;
				$index = 'monthly';
			}else{
				$date_where .= $date;
				$index = 'yearly';
			}
			$data[$index] = $this->getTotal($data['materials'],$date_where,1,$dates[2]);
		}
		// for monthly data
		$this->material_property_model->joins = array('BRAND');
		$this->db->group_by('brand_name');
		$monthly_data['brands'] = $this->material_property_model->getMaterialProperties(array('department_id' => 1))->result_array();
		$monthly_data['date'] = $this->input->post('date');
print_r($data['last_entry_date']);
print_r($last_entry);
exit;
		$this->load->view('home/mixing/day_report',$data);
		$this->load->view('home/mixing/monthly_report',$monthly_data);
	}
	// for monthly report
	public function monthlyReport(){
		$month = $this->input->post('month');
		$date = $this->input->post('date');
		$department_id = $this->input->post('department_id');
		$dates = explode('-',$date);
		$this->material_department_model->joins = array('CATEGORY_DEPARTMENT');
		$this->material_department_model->joins = array('MATERIALS');
		$this->db->order_by('cd_id');
		$report['materials'] = $this->material_department_model->getMaterialDepartments(array('group_id'=>$department_id))->result_array();
		if($month != 0){
			if($dates[1] > 3 && $month <= 3){//if selected month is before asar and current date is after asar then month from next year
				$dates[0]++;
			}else if($dates[1] <= 3 && $month > 3){//if selected month is after asar and current date is before asar then month from previous year
				$dates[0]--;
			}
		}else{
			if($dates[1] < 3){
				$dates[0]--;// if month is before asar start date is a year before
			}
		}
		$report['raw_material']['total'] = '';
		$report['bought_scrap']['total'] = '';
		$report['factory_scrap']['total'] = '';
		$report['total'] = '';
		foreach($report['materials'] as $index => $material){
			if($material['category_id'] == 2){
				$index_category = 'bought_scrap';
			}else if($material['category_id'] == 4){
				$index_category = 'factory_scrap';
			}else{
				$index_category = 'raw_material';
			}

			if($month != 0){
				$this->db->like('nepali_date', $dates[0].'-'.$month);
			}else{
				$this->db->where('nepali_date >=', $dates[0].'-03-01');
				$this->db->where('nepali_date <=', ($dates[0]+1).'-04-35');
			}

			$report[$index_category]['materials'][$index]['consumption'] = $this->consumption_model->getAllConsumptions(array('department_id'=>$department_id,'material_id'=>$material['material_id']))->row_array();
			$report[$index_category]['total'] += $report[$index_category]['materials'][$index]['consumption']['item_amount'];
			$report['total'] += $report[$index_category]['materials'][$index]['consumption']['item_amount'];
		}
		$report['date'] = $dates[0].'-'.$month;

		if($this->session->userdata('department_id') == 5){
			$this->load->view('home/rexin/monthly_detail',$report);
		}else{
			$this->load->view('home/mixing/monthly_detail',$report);
		}
	}

	// for total consumption
	private function getTotal($materials,$full_date,$department_id,$month=NULL){
		$where = array('department_id' => $department_id);
		$data['factory_scrap'] = 0;
		$data['bought_scrap'] = 0;
		$data['raw_material'] = 0;
		$date = explode('-',$full_date);
		
		if(array_key_exists(1, $date) ){
			foreach($materials as $material){
				$this->db->like('nepali_date', $full_date);
				$where['consumptions.material_id'] = $material['material_id'];
				if($material['category_id'] == 4){
					$index = 'factory_scrap';
				}else if($material['category_id'] == 2){
					$index = 'bought_scrap';
				}else{
					$index = 'raw_material';
				}
				$amount = $this->consumption_model->getAllConsumptions($where)->row_array();
				$data[$index] += $amount['item_amount'];
			}
		}else{
			foreach($materials as $material){
				
				if($month > 2){
					$this->db->where('nepali_date >=',($full_date).'-04-01');
					$this->db->where('nepali_date <=',($full_date+1).'-03-35');
				}else{
					$this->db->where('nepali_date >=',($full_date-1).'-04-01');
					$this->db->where('nepali_date <=',($full_date).'-03-35');
				}

				$year_where['consumptions.material_id'] = $material['material_id'];
				
				if($material['category_id'] == 4){
					$index = 'factory_scrap';
				}else if($material['category_id'] == 2){
					$index = 'bought_scrap';
				}else{
					$index = 'raw_material';
				}

				$amount = $this->consumption_model->getAllConsumptions($year_where)->row_array();
				$data[$index] += $amount['item_amount'];
			}
		}

		return $data;
	}
	//for mixing brand report
	function monthlyMixingBrandReport(){
		$month = $this->input->post('month');
		$date = $this->input->post('date');
		$department_id = $this->input->post('department_id');
		$dates = explode('-',$date);
		$this->material_department_model->joins = array('CATEGORY_DEPARTMENT');
		$this->material_department_model->joins = array('MATERIALS');
		$this->db->order_by('cd_id');
		$this->db->order_by('material_name');
		$report['materials'] = $this->material_department_model->getMaterialDepartments(array('group_id'=>$department_id))->result_array();
		if($month != 0){
			if($dates[1] > 3 && $month <= 3){//if selected month is before asar and current date is after asar then month from next year
				$dates[0]++;
			}else if($dates[1] <= 3 && $month > 3){//if selected month is after asar and current date is before asar then month from previous year
				$dates[0]--;
			}
		}else{
			if($dates[1] < 3){
				$dates[0]--;// if month is before asar start date is a year before
			}
		}
		$report['raw_material']['total'] = '';
		$report['bought_scrap']['total'] = '';
		$report['factory_scrap']['total'] = '';
		$report['total'] = '';
		foreach($report['materials'] as $index => $material){
			if($material['category_id'] == 2){
				$index_category = 'bought_scrap';
			}else if($material['category_id'] == 4){
				$index_category = 'factory_scrap';
			}else{
				$index_category = 'raw_material';
			}
			if($month != 0){
				$this->db->like('nepali_date', $dates[0].'-'.$month);
			}else{
				$this->db->where('nepali_date >=', $dates[0].'-03-01');
				$this->db->where('nepali_date <=', ($dates[0]+1).'-04-35');
			}
			$brand_id = $this->input->post('brand_id');
			$dimension_id = $this->input->post('dimension_id');
			$coating_id = $this->input->post('coating_id');

			$this->consumption_model->joins = array('MATERIAL_PROPERTY','BRAND','DIMENSION');
			
			if($department_id == 5){
				$report[$index_category]['materials'][$index]['consumption'] = $this->consumption_model->getAllConsumptions(array(
						'consumptions.department_id'=>$department_id,
						'material_id'=>$material['material_id'],
						'brand_id'=>$brand_id,
						'coating_id'=>$coating_id
					))->row_array();
			}else{
				$report[$index_category]['materials'][$index]['consumption'] = $this->consumption_model->getAllConsumptions(array(
						'consumptions.department_id'=>$department_id,
						'material_id'=>$material['material_id'],
						'brand_id'=>$brand_id,
						'dimension_id'=>$dimension_id
					))->row_array();
			}
			
			$report[$index_category]['total'] += $report[$index_category]['materials'][$index]['consumption']['item_amount'];
			$report['total'] += $report[$index_category]['materials'][$index]['consumption']['item_amount'];
		}
		$report['date'] = $dates[0].'-'.$month;
		$this->load->view('home/mixing/monthly_brand_detail',$report);
	}

	// for calendar report
	public function getCalendarReport(){
		$date = $this->input->post('date');
		$data['date'] = $date;
		$this->db->order_by('date-np','desc');
		$last_entry = $this->calander_shift_report_model->getCalanderShiftReports()->row_array();
		$date = $last_entry['date-np'];
		$data['working_days'] = $this->consumption_model->getWorkingDays(1,$date);
		$where['group_id'] = 2;
		$this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
		$this->db->order_by('order');
		$data['materials'] = $this->material_department_model->getMaterialDepartments($where)->result_array();
		
		$scrap_types = $this->scrap_type_model->getScraptypes(array('department_id'=>2))->result_array();
		
		// $this->load->view('home/pdf_button');
		$this->load->view('home/calendar/day_report',$data);

		$this->getInputAnalysis(2,$scrap_types);
		$this->getSemiFinishedGoodReport(1);
		$this->time_loss_report(2,$date);
		$this->hour_report(2);
		$data_department['department_id'] = 2;
		$this->load->view('home/calendar/target_form',$data_department);
	}

   // for report
   private function getInputAnalysis($department_id, $types){
   		$this->db->order_by('date-np','desc');
   		$last_entry = $this->calander_shift_report_model->getCalanderShiftReports()->row_array();
   		$last_entry_date = $last_entry['date-np'];
   		$dates = explode('-',$last_entry_date);
   		$report['date'] = $last_entry_date;
   		$this->calander_shift_report_model->joins = array('CONSUMPTION');
   		foreach($dates as $i => $date){
   			if($i == 0){
   				$index = 'yearly';
   				$this->db->like('date-np',$dates[0]);
   				// $this->db->where('calander_report_id <> ','');
   				$report[$index] = $this->calander_shift_report_model->getAllReport()->row_array();
   				
   				$this->db->like('nepali_date',$dates[0]);
   				$this->db->where('calander_report_id <> ', 0);
   				$this->db->group_by('calander_report_id');
   				$this->db->where('department_id',1);
   				$consumptions = $this->consumption_model->getAllConsumptions()->result_array();
   				$report[$index]['total_length'] = 0;
   				$report[$index]['net_weight'] = 0;
   				$report[$index]['mixing_tot'] = 0;

   				foreach ($consumptions as $consumption) {
   					$calendar_reports = array();
   					$calendar_reports = $this->calander_shift_report_model->getCalanderShiftReports(array('report_id'=>$consumption['calander_report_id']))->result_array();
   						$report[$index]['total_length'] += $calendar_reports[0]['length'];
   						$report[$index]['net_weight'] += $calendar_reports[0]['net_wt'];
   						$report[$index]['mixing_tot'] += $consumption['item_amount'];
   					
   				}
   				
   				foreach($types as $type){
   					$this->db->like('date_np',$dates[0]);
   					$report[$index][$type['type_name']] = $this->scrap_record_model->getScrapTotal(array('department_id'=>$department_id,'type_id'=>$type['type_id']))->row_array();
   				}

   			}else if($i == 1){
   				$index = 'monthly';
   				$this->db->like('date-np',$dates[0].'-'.$dates[1]);
   				$this->db->where('calander_report_id <> ','');
   				$report[$index] = $this->calander_shift_report_model->getAllReport()->row_array();

   				$this->db->like('nepali_date',$dates[0].'-'.$dates[1]);
   				$this->db->where('calander_report_id <> ', 0);
   				$this->db->group_by('calander_report_id');
   				$this->db->where('department_id',1);
   				$consumptions = $this->consumption_model->getAllConsumptions()->result_array();
   				$report[$index]['total_length'] = 0;
   				$report[$index]['net_weight'] = 0;
   				$report[$index]['mixing_tot'] = 0;

   				foreach ($consumptions as $consumption) {
   					$calendar_reports = array();
   					$calendar_reports = $this->calander_shift_report_model->getCalanderShiftReports(array('report_id'=>$consumption['calander_report_id']))->result_array();
   						$report[$index]['total_length'] += $calendar_reports[0]['length'];
   						$report[$index]['net_weight'] += $calendar_reports[0]['net_wt'];
   						$report[$index]['mixing_tot'] += $consumption['item_amount'];
   				}

   				foreach($types as $type){
   					$this->db->like('date_np',$dates[0].'-'.$dates[1]);
   					$report[$index][$type['type_name']] = $this->scrap_record_model->getScrapTotal(array('department_id'=>$department_id,'type_id'=>$type['type_id']))->row_array();
   				}
   			}else{
   				$index = 'daily';
   				$where['date-np'] = $dates[0].'-'.$dates[1].'-'.$dates[2];
   				$this->db->where('calander_report_id <> ','');
   				$report[$index] = $this->calander_shift_report_model->getAllReport($where)->row_array();

   				$this->db->like('nepali_date',$dates[0].'-'.$dates[1].'-'.$dates[2]);
   				$this->db->where('calander_report_id <> ', 0);
   				$this->db->group_by('calander_report_id');
   				$this->db->where('department_id',1);
   				$consumptions = $this->consumption_model->getAllConsumptions()->result_array();
   				$report[$index]['total_length'] = 0;
   				$report[$index]['net_weight'] = 0;
   				$report[$index]['mixing_tot'] = 0;

   				foreach ($consumptions as $consumption) {
   					$calendar_reports = array();
   					$calendar_reports = $this->calander_shift_report_model->getCalanderShiftReports(array('report_id'=>$consumption['calander_report_id']))->result_array();
   						$report[$index]['total_length'] += $calendar_reports[0]['length'];
   						$report[$index]['net_weight'] += $calendar_reports[0]['net_wt'];
   						$report[$index]['mixing_tot'] += $consumption['item_amount'];
   				}
   				foreach($types as $type){
	   				$this->db->where('date_np',$dates[0].'-'.$dates[1].'-'.$dates[2]);
   					$report[$index][$type['type_name']] = $this->scrap_record_model->getScrapTotal(array('department_id'=>$department_id,'type_id'=>$type['type_id']))->row_array();
   				}
   			}
   		}
   		$this->load->view('home/calendar/io_analysis',$report);

   }
   //for semi finished good
   public function getSemiFinishedGoodReport($department_id){
   		$this->calander_shift_report_model->joins = array('MATERIAL_PROPERTY','BRAND','DIMENSION','QUALITY','CONSUMPTION');
   		$this->consumption_model->joins = array('MATERIAL_PROPERTY','BRAND','DIMENSION','QUALITY');
   		$this->db->order_by('date-np','desc');
   		$last_entry = $this->calander_shift_report_model->getCalanderShiftReports()->row_array();
   		$last_entry_date = $last_entry['date-np'];
   		$this->material_property_model->joins = array('DIMENSION','BRAND','QUALITY');
   		$where['department_id'] = $department_id;
   		$this->db->group_by('dimension_id,brand_id,quality_id');
   		$this->db->order_by('dimension_id')->order_by('quality_id')->order_by('brand_id');
   		$data['material_properties'] = $this->material_property_model->getMaterialProperties($where)->result_array();
   		$dates = explode('-',$last_entry_date);
   		foreach($data['material_properties'] as $i => $property){
   			foreach($dates as $x=>$date){
   				if($x == 0){
					$yearly_total = 0;
   					$material_where = $this->getTimeWhere($last_entry_date,$x,'nepali_date');
		   			$material_where['dimension_id'] = $property['dimension_id'];
		   			$material_where['brand_id'] = $property['brand_id'];
		   			$material_where['quality_id'] = $property['quality_id'];
					$material_where['consumptions.department_id'] = $department_id;
					$this->db->group_by('group');
					$consumptions = $this->consumption_model->getAllConsumptions($material_where)->result_array();
   					foreach($consumptions as $consumption){
   						$consume = $this->calander_shift_report_model->getAllReport(array('report_id'=>$consumption['calander_report_id']))->row_array();
   						 $yearly_total += $consume['net_wt'];
   					}
   						$data['material_properties'][$i]['yearly'] = $yearly_total;
   				}
   				else if($x == 1){
					$monthly_total = 0;
   					$material_where = $this->getTimeWhere($last_entry_date,$x,'nepali_date');
		   			$material_where['dimension_id'] = $property['dimension_id'];
		   			$material_where['brand_id'] = $property['brand_id'];
		   			$material_where['quality_id'] = $property['quality_id'];
					$material_where['consumptions.department_id'] = $department_id;
					$this->db->group_by('group');
					$consumptions = $this->consumption_model->getAllConsumptions($material_where)->result_array();
   					foreach($consumptions as $consumption){
   						$consume = $this->calander_shift_report_model->getAllReport(array('report_id'=>$consumption['calander_report_id']))->row_array();
   						 $monthly_total += $consume['net_wt'];
   					}
   						$data['material_properties'][$i]['monthly'] = $monthly_total;

   				}else{
					$daily_total = 0;
   					$material_where = $this->getTimeWhere($last_entry_date,$x,'nepali_date');
		   			$material_where['dimension_id'] = $property['dimension_id'];
		   			$material_where['brand_id'] = $property['brand_id'];
		   			$material_where['quality_id'] = $property['quality_id'];
					$material_where['consumptions.department_id'] = $department_id;
					$this->db->group_by('group');
					$consumptions = $this->consumption_model->getAllConsumptions($material_where)->result_array();
   					foreach($consumptions as $consumption){
   						$consume = $this->calander_shift_report_model->getAllReport(array('report_id'=>$consumption['calander_report_id']))->row_array();
   						 $daily_total += $consume['net_wt'];
   					}
   						$data['material_properties'][$i]['daily'] = $daily_total;
   				}
   			}
   		}

   		$this->load->view('home/calendar/semi_product_report',$data);
   }
   //timeloss report
   private function time_loss_report($department_id,$date = NULL){
		if($date != NULL && $date != ''){
			$last_entry_date = $date;
   			$dates = explode('-',$date);
		}else{
			$where['department_id'] = $department_id;
	   		$this->time_loss_model->joins = array('TL_REASON_FULL');
	   		$this->db->order_by('date_np','desc');
	   		$last_entry = $this->time_loss_model->getTimeLosses($where)->row_array();
	   		$last_entry_date = $last_entry['date_np'];
   			$dates = explode('-',$last_entry_date);
		}

   		$timeloss_types = $this->tL_type_model->getTlTypes()->result_array();
   		foreach($dates as $x => $date){
	   		foreach($timeloss_types as $type){
	   			$where = NULL;
	   			if($x == 0){
	   				$index = 'yearly_loss';
	   				$where = $this->getTimeWhere($last_entry_date,$x,'date_np');
	   			}else if($x == 1){
	   				$index = 'monthly_loss';
	   				$this->db->like('date_np',$dates['0'].'-'.$dates['1']); 
	   			}else{
	   				$index = 'daily_loss';
	   				$this->db->like('date_np',$dates['0'].'-'.$dates['1'].'-'.$dates['2']);
	   			}
	   			$where['department_id'] = $department_id;

		   		$this->time_loss_model->joins = array('TL_REASON_FULL','TL_TYPES','TL_REASONINGS');
	   			$this->db->group_by('reason');
		   		$where['tl_reason_full.type_id'] = $type['type_id'];
		   		$reason_total = $this->time_loss_model->getTimeLosseReports($where)->result_array();
		   		foreach ($reason_total as $reason){
		   			$data[$index][$type['loss_type']][$reason['reason_id']] = $reason;
		   		}
		   		if($x == 0){
	   				$index = 'yearly_loss';
	   				$where = $this->getTimeWhere($last_entry_date,$x,'date_np');
		   			$where['tl_reason_full.type_id'] = $type['type_id'];
		   			$where['department_id'] = $department_id;
	   			}else if($x == 1){
	   				$index = 'monthly_loss';
	   				$this->db->like('date_np',$dates['0'].'-'.$dates['1']); 
	   			}else{
	   				$index = 'daily_loss';
	   				$this->db->like('date_np',$dates['0'].'-'.$dates['1'].'-'.$dates['2']);
	   			}
	   			$data[$index][$type['loss_type']]['total'] = $this->time_loss_model->getTimeLosseReports($where)->row_array();
	   		}
   		}
   		foreach($timeloss_types as $type){
   			$this->tL_reason_full_model->joins = array('TL_REASONINGS');
   			$reason_where['department_id'] = $department_id;
   			$reason_where['type_id'] = $type['type_id'];
   			$data['reasons'][$type['loss_type']] = $this->tL_reason_full_model->getTlReasonFulls($reason_where)->result_array();
   		}
   		if($department_id == 2){
   			$data['working_days_for_timeloss'] = $this->calander_shift_report_model->getWorkingDays($department_id,$last_entry_date);
   		}elseif($department_id == 4){
   			$data['working_days_for_timeloss'] = $this->laminating_shift_report_model->getWorkingDays($last_entry_date);
   		}else{
	   		$data['working_days_for_timeloss'] = $this->consumption_model->getWorkingDays($department_id,$last_entry_date);
   		}
   		$data['types'] = $timeloss_types;
   		$data['date'] = $last_entry_date;
   		$this->load->view('home/timeloss',$data);
   }
   	function hour_report($department_id){
	   	$where['department_id'] = $department_id;
	   	$this->db->order_by('date_np','desc');
	   	$last_entry = $this->time_loss_model->getTimeLosses($where)->row_array();
		$data['date'] = $last_entry['date_np'];
		$this->load->view('home/hour_report',$data);
   	}
   	function target_report($department_id){
   		$dates = $this->input->post('date');
   		$month = $this->input->post('month');
   		$date = explode('-',$dates);
   		$report = array();
   		if($month == 0){
   			$required_date = $date['0'];
   		}else{
   			$required_date = $date['0'].'-'.$month;
   		}
   		if($department_id == 2){
   			$this->target_model->joins = array('BRAND','DIMENSION','QUALITY');
	   		$this->db->order_by('dimension_name');
	   		$targets = $this->target_model->getTargets(array('department_id'=>2))->result_array();
	   		foreach($targets as $target){
	   			$total = 0;
   				$total_length = 0;
	   			$where['brand_id'] = $target['brand'];
	   			$where['dimension_id'] = $target['dimension'];
	   			$where['quality_id'] = $target['type'];
	   			if($department_id == 2){
	   				$where['department_id'] = 1;
	   			}
	   			$material_properties = $this->material_property_model->getMaterialProperties($where)->result_array();
	   			$i = 0;
	   			foreach($material_properties as $material_property){
	   				$this->db->like('nepali_date',$required_date);
	   				$consumptions = $this->consumption_model->getConsumptions(array('department_id' => 1, 'material_property_id' => $material_property['mp_id']))->result_array();
	   				$total = 0;
	   				$total_length = 0;
	   				foreach($consumptions as $consumption){
	   					$report_con = $this->calander_shift_report_model->getCalanderShiftReports(array('report_id'=>$consumption['calander_report_id']))->row_array();
	   					$total += @$report_con['net_wt'];
	   					$total_length += @$report_con['length'];
	   				}
	   				$i++;
					$report[$target['target_id']][$i]['net_weight'] = $total;
	   				$report[$target['target_id']][$i]['total_length'] = $total_length;
	   			}
	   		}
	   	}else if($department_id == 3){
	   		$this->target_model->joins = array('DIMENSION');
	   		$this->db->order_by('dimension_name');
	   		$targets = $this->target_model->getTargets(array('department_id'=>3))->result_array();
	   		foreach($targets as $target){
	   			$where['dimension_id'] = $target['dimension'];
	   			$where['department_id'] = $department_id;
	   			$material_properties = $this->material_property_model->getMaterialProperties($where)->result_array();
	   			$i = 0;
	   			foreach($material_properties as $material_property){
	   				$this->db->like('date_np',$required_date);
	   				$report[$target['id']][$i] = $this->print_shift_report_model->getAllReport(array('material_property_id'=>$material_property['mp_id']))->row_array();
	   				$i++;
	   			}
	   		}
	   	}else if($department_id == 5){

	   		//////////////////////////////////////////////////////////////////////////////////
	   		$this->target_model->joins = array('BRAND');
	   		$this->db->order_by('brand_name');
	   		$targets = $this->target_model->getTargets(array('department_id'=>5))->result_array();
	   		// echo '<pre>';print_r($targets);
	   		foreach($targets as $target){
	   			$this->rexin_coating_report_model->joins = array('REXIN_PRODUCT');
	   			$where['brand_id'] = $target['brand'];
	   			// $where['department_id'] = $department_id;
	   			$material_properties = $this->rexin_product_model->getRexinProducts($where)->result_array();
	   			// echo '<pre>';print_r($material_properties);
	   			// echo $this->db->last_query().'<br>';
	   			$i = 0;
	   			foreach($material_properties as $material_property){
	   				$this->db->like('date_np',$required_date);
	   				$report[$target['id']][$i] = $this->rexin_coating_report_model->getAllReport(array('rexin_coating_reports.rp_id'=>$material_property['rp_id']))->row_array();
	   				$i++;
	   				// echo $this->db->last_query().'<br>';exit;
	   			}
	   		}
	   		/////////////////////////////////////////////////////////////////////////////////////
	   		// $targets = array();
	   		// $this->target_model->joins = array('BRAND');
	   		// $this->db->order_by('brand_name');
	   		// $this->db->group_by('brand_name');
	   		// $this->rexin_product_model->joins = array('BRAND');
	   		// $brands = $this->rexin_product_model->getRexinProducts()->result_array();
	   		// foreach($brands as $key => $brand){
	   		// 	$target = $this->target_model->getTargets(array('department_id'=>5,'brand'=>$brand['brand_id']))->row_array();
	   		// 	if(count($target)){
	   		// 		$targets[] = $target;	
	   		// 	}
	   		// }
	   		// // echo '<pre>';print_r($targets);exit;
	   		// if(count($targets)){
		   	// 	foreach($targets as $target){
		   	// 		$where['brand_id'] = $target['brand'];
		   	// 		$where['department_id'] = $department_id;
		   	// 		$material_properties = $this->material_property_model->getMaterialProperties($where)->result_array();
		   	// 		// print_r($material_properties);
		   	// 		// echo $this->db->last_query().'<br>';
		   	// 		$i = 0;
		   	// 		foreach($material_properties as $material_property){
		   	// 			$this->db->like('date_np',$required_date);
		   	// 			$report[$target['id']][$i] = $this->rexin_coating_report_model->getAllReport()->row_array();
		   	// 			$i++;
		   	// 			// echo $this->db->last_query().'<br>';exit;
		   	// 		}
		   	// 	}
   			// }
	   	}
   		$data['targets'] = $targets;
   		$data['reports'] = $report;
   		$data['department_id'] = $department_id;
   		$this->load->view('home/target_report',$data);
   }
   // function to i/o ratio target of rexin
   public function io_target_report()
   {
   		$dates = $this->input->post('date');
   		$month = $this->input->post('month');
   		$date = explode('-',$dates);
   		$report = array();
   		if($month == 0){
   			$required_date = $date['0'];
   		}else{
   			$required_date = $date['0'].'-'.$month;
   		}
   		$department_id = $this->input->post('department_id');
   		$this->target_wt_model->joins = array('BRAND');
   		$this->db->order_by('brand_name');
   		$targets = $this->target_wt_model->getTargetWts()->result_array();
   		foreach($targets as $target){
   			$this->rexin_coating_report_model->joins = array('REXIN_PRODUCT');
   			$where['brand_id'] = $target['brand'];
   			// $where['department_id'] = $department_id;
   			$material_properties = $this->rexin_product_model->getRexinProducts($where)->result_array();
   			$i = 0;
   			foreach($material_properties as $material_property){
   				$this->db->like('date_np',$required_date);
   				$report[$target['id']][$i] = $this->rexin_coating_report_model->getAllReport(array('rexin_coating_reports.rp_id'=>$material_property['rp_id']))->row_array();
   				$i++;
   			}
   		}
   		$data['targets'] = $targets;
   		$data['reports'] = $report;
   		$data['department_id'] = $department_id;
   		$this->load->view('home/rexin/io_target_report',$data);
   }
   // for laminating report
   public function getLaminatingReport(){
   		$date = $this->input->post('date');
		$data['date'] = $date;
		$data['working_days'] = $this->laminating_shift_report_model->getWorkingDays($date);
		$where['group_id'] = 4;
		$this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
		$this->db->order_by('order');
		$data['materials'] = $this->material_department_model->getMaterialDepartments($where)->result_array();
		
		$scrap_types = $this->scrap_type_model->getScraptypes(array('department_id'=>4))->result_array();
		$this->load->view('home/day_report',$data);

		$this->time_loss_report(4,$date);
		$this->getShiftSummary();
		$this->load->view('home/laminating/brand_scrap');
   }
   //for shift summary of lamination
   	public function getShiftSummary(){
   		$this->db->order_by('date-np','desc');
   		$last_entry = $this->laminating_shift_report_model->getLaminatingShiftReports()->row_array();
   		$last_entry_date = $last_entry['date-np'];
   		$dates = explode('-', $last_entry_date);
   		foreach($dates as $key => $date){
			$where = array();
   			if($key == 0){
   				$index = 'yearly_report';
   				$where = $this->getTimeWhere($last_entry_date,$key,'`date-np`');
   			}else if($key == 1){
   				$index = 'monthly_report';
   				$where = $this->getTimeWhere($last_entry_date,$key,'date-np');
   			}else{
   				$index = 'daily_report';
   				$where = $this->getTimeWhere($last_entry_date,$key,'date-np');
   			}
   			$data[$index] = $this->laminating_shift_report_model->getAllReport($where)->row_array();
   		}
		$item = '';
   		for($i = 0; $i <= 5; $i++){
			foreach($dates as $key => $date){
				$where = array();
	   			if($key == 0){
	   				$index = 'yearly_total';
	   				$where = $this->getTimeWhere($last_entry_date,$key,'`date-np`');
	   			}else if($key == 1){
	   				$index = 'monthly_total';
	   				$where = $this->getTimeWhere($last_entry_date,$key,'date-np');
	   			}else{
	   				$index = 'daily_total';
	   				$where = $this->getTimeWhere($last_entry_date,$key,'date-np');
	   			}
	   			if($i == 0){
	   				$item = 'base_ut';
	   			}
	   			else if($i == 1){
	   				$item = 'base_mt';
	   			}
	   			else if($i == 2){
	   				$item = 'base_ot';
	   			}
	   			else if($i == 3){
	   				$item = 'print_film';
	   			}
	   			else if($i == 4){
	   				$item = 'ct';
	   			}
	   			else if($i == 5){
	   				$item = 'dull_ct';
	   			}
	   			$where[$item.' <>'] = 0;
	   			$total = $this->laminating_shift_report_model->getAllReport($where)->row_array();
	   			$data['total'][$index][$item] = $total['total_output'];
	   		}
	   	}
   		$data['date'] = $last_entry_date;
   		// echo '<pre>';print_r($data['total']);echo '</pre>';
   		$this->load->view('home/laminating/shift_summary_report',$data);
   	}

   	public function getScrapBrand(){
   		// echo '<pre>';
   		$date = $this->input->post('date');
   		$month = $this->input->post('month');

   		$dates = explode('-',$date);
   		if($month < 4 && $dates[1] > 3){
   			$dates[0]++;
   		}else if($month >3 && $dates[1] <4){
   			$date[0]--;
   		}
   		$this->target_model->joins = array('BRAND','LAMINATING_BRAND_MATERIAL');
   		$this->db->order_by('brand_name','asc');
   		$data['targets'] = $this->target_model->getTargets(array('department_id'=>4))->result_array();
   		$this->laminating_shift_report_model->joins = array('LAMINATING_BRAND_COLOR');
   		if($month != 0){
   			$where['date-np LIKE "%'.$dates[0].'-'.$month.'%"'] = NULL;
   		}else{
   			$where = $this->getTimeWhere($date,0,'`date-np`');
   		}
   		foreach($data['targets'] as $key => $target){
   			$output = 0;
   			$where['brand'] = $target['brand'];
   			if($target['print_film'] == 1){
   				$total = $this->laminating_shift_report_model->getAllReport($where)->row_array();
   				$output = ($total['total_print_film']>0)?$total['total_output']/$total['total_print_film']:0;
   			}else{
   				$total = $this->laminating_shift_report_model->getAllReport($where)->row_array();
   				$output = ($total['total_base_ot'])?$total['total_output']/$total['total_base_ot']:0;
   			}
   			$data['targets'][$key]['output'] = $output;
   		}
   		$this->load->view('home/laminating/brand_scrap_report',$data);
   	}
   	public function getprintingReport()
   	{
   		$date = $this->input->post('date');
		$data['date'] = $date;
		$data['working_days'] = $this->print_shift_report_model->getWorkingDays($date);
		$data['department_id'] = 3;

		$where['group_id'] = 3;
		$this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
		$this->db->order_by('order');
		$data['materials'] = $this->material_department_model->getMaterialDepartments($where)->result_array();
		
		$scrap_types = $this->scrap_type_model->getScraptypes(array('department_id'=>3))->result_array();
		$this->load->view('home/day_report',$data);

		$this->time_loss_report(3,$date);
		$this->io_report(3);
		$this->load->view('home/target_form',$data);
		$this->print_loss();
		$this->load->view('home/printing/monthly_consumption_form',$data);
   	}
   	public function monthly_consumption_report()// for printing
   	{
   		$date = $this->input->post('date');
   		$month = $this->input->post('month');
   		$dates = explode('-', $date);

   		$where['group_id'] = 3;
   		$this->material_department_model->joins = array('CATEGORY_DEPARTMENT','MATERIALS');
   		$this->db->order_by('order');
   		$this->db->order_by('material_name');
   		$data['materials'] = $this->material_department_model->getMaterialDepartments($where)->result_array();

   		$this->consumption_model->joins = array('MATERIALS');
   		$data['total'] = 0;

   		foreach ($data['materials'] as $i => $materail) {
   			$where = array();
   			$where['department_id'] = 3;
   			if($month != 0){
		   		if($month < 4 && $dates[2] > 3){
		   			$dates[0]++;
		   		}
		   		if($month > 0 && $dates[2] < 3){
		   			$dates[0]--;
		   		}
   				$where['nepali_date like "%'.$dates[0].'-'.$month.'%"'] = NULL;
   			}else{
   					$where = $this->getTimeWhere($date,0,'nepali_date');
   			}
   			$where['consumptions.material_id'] = $materail['material_id'];
   			$consumption = $this->consumption_model->getAllConsumptions($where)->row_array();
   			$data['total'] += $consumption['item_amount'];
   			$data['materials'][$i]['consumption'] = $consumption['item_amount']; 
   		}

   		$this->load->view('home/printing/consumption_table',$data);
   	}
   	public function print_loss()
   	{
   		$this->print_report_reason_model->joins = array('PRINT_SHIFT_REPORT','PRINTLOSS_FULL');
   		$this->db->order_by('date_np','desc');
   		$last_entry = $this->print_report_reason_model->getPrintReportReasons()->row_array();
   		$last_entry_date = $last_entry['date_np'];
   		$dates = explode('-', $last_entry_date);

		$data['total'][0]['yearly'] = 0;
		$data['total'][1]['yearly'] = 0;
		$data['total'][0]['monthly'] = 0;
		$data['total'][1]['monthly'] = 0;
		$data['total'][0]['daily'] = 0;
		$data['total'][1]['daily'] = 0;
   		$data['print_types'] = $this->printloss_type_model->getPrintlossTypes()->result_array();
   		foreach ($data['print_types'] as $key => $type) {
   			$where = array();
   			$this->printloss_full_model->joins = array('PRINTLOSS_REASON');
   			$where['type_id'] = $type['plt_id'];
   			$data['print_types'][$key]['reasons'] = $this->printloss_full_model->getPrintlossFulls($where)->result_array();

   			foreach ($data['print_types'][$key]['reasons'] as $key1 => $reason) {
	   			foreach ($dates as $key2 => $date) {
	   				if($key2 == 0){
	   					$index = 'yearly';
	   				}else if($key2 == 1){
	   					$index = 'monthly';
	   				}else{
	   					$index = 'daily';
	   				}
	   				$where = array();
	   				$amount = array();
	   				$where = $this->getTimeWhere($last_entry_date,$key2,'date_np');
	   				$where['type'] = $type['plt_id'];
	   				$where['reason_id'] = $reason['reason_id'];
	   				$amount = $this->print_report_reason_model->getAllReport($where)->row_array();
	   				$data['print_types'][$key]['reasons'][$key1][$index] = $amount['total_amount'];
	   				$data['total'][$key][$index] += $amount['total_amount'];
	   				if($key2 == 0){
	   				}
	   			}
   			}

   		}
   		$data['date'] = $last_entry_date;
   		$this->load->view('home/printing/scrap_report',$data);
   	}
   	public function getrexinReport()
   	{
   		$date = $this->input->post('date');
   		$this->db->order_by('date_np','desc');
   		$last_entry = $this->rexin_coating_report_model->getRexinCoatingReports()->row_array();
   		$date = $last_entry['date_np'];
		$data['date'] = $date;
		$data['working_days'] = $this->rexin_coating_report_model->getWorkingDays($date);
		$data['department_id'] = 5;

		$where['group_id'] = 5;
		$this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
		$this->db->order_by('order');
		$data['materials'] = $this->material_department_model->getMaterialDepartments($where)->result_array();
		
		$scrap_types = $this->scrap_type_model->getScraptypes(array('department_id'=>5))->result_array();
		$this->load->view('home/day_report',$data);

		$this->time_loss_report(5,$date);
		$this->io_report(5,$date);

		$this->rexin_product_model->joins = array('BRAND');
		$this->db->group_by('brand_id');
		$this->db->order_by('brand_name');
		$data['brands'] = $this->rexin_product_model->getRexinProducts()->result_array();
		$this->load->view('home/rexin/brand_form',$data);
		// target form
		$this->load->view('home/rexin/target_form');
   	}
   	public function io_report($department_id=NULL,$date=NULL)
   	{
   		// echo '<pre>';
   		if($department_id == 3){
   			$this->db->order_by('date_np','desc');
   			$last_entry = $this->print_shift_report_model->getPrintShiftReports()->row_array();
   			$date = $last_entry['date_np'];
   			$dates = explode('-',$date);
   			$this->print_shift_report_model->joins = array('MATERIAL_PROPERTY');
	   		foreach ($dates as $key => $value) {
	   			$index = '';
	   			if($key == 0){
	   				$index = 'yearly';
	   				$where = $this->getTimeWhere($date,$key,'date_np');
	   			}else if($key == 1){
	   				$index = 'monthly';
	   				$where = $this->getTimeWhere($date,$key,'date_np');
	   			}else{
	   				$index = 'daily';
	   				$where = $this->getTimeWhere($date,$key,'date_np');
	   			}
	   			// print_r($where).'<br>';
	   			$data['total'][$index] = $this->print_shift_report_model->getAllReport($where)->row_array();
	   			$this->db->group_by('color_id');
	   			$data['total'][$index]['no_of_color'] = $this->print_shift_report_model->count($where);
	   			// echo $this->db->last_query().'<br>';
	   		}
	   		// echo '<pre>';print_r($data);exit;
   			$this->load->view('home/printing/io_analysis',$data);
   		}else if($department_id == 5){
   			$this->db->order_by('date_np','desc');
   			$last_entry = $this->rexin_coating_report_model->getRexinCoatingReports()->row_array();
   			$date = $last_entry['date_np'];
   			$dates = explode('-',$date);
   			foreach ($dates as $key => $value) {
	   			$index = '';
	   			if($key == 0){
	   				$index = 'yearly';
	   				$where = $this->getTimeWhere($date,$key,'date_np');
	   			}else if($key == 1){
	   				$index = 'monthly';
	   				$where = $this->getTimeWhere($date,$key,'date_np');
	   			}else{
	   				$index = 'daily';
	   				$where = $this->getTimeWhere($date,$key,'date_np');
	   			}
	   			// print_r($where).'<br>';
	   			$data['total'][$index] = $this->rexin_coating_report_model->getAllReport($where)->row_array();
	   			// $this->db->group_by('color_id');
	   			// $data['total'][$index]['no_of_color'] = $this->rexin_coating_report_model->count($where);
	   			// echo $this->db->last_query().'<br>';
	   		}
	   			// echo '<pre>';
	   			// print_r($data['total']);exit;
	   		$this->load->view('home/rexin/io_analysis',$data);
   		}
	   			// print_r($data);
   		// echo '</pre>';
   	}
   	// for pdf download
   	public function pdf($report_type = NULL, $date = NULL, $department_id = NULL){
   		if($department_id == NULL){
   			$department_id = $this->session->userdata('department_id');
   		}

   		if($department_id == 1 || $report_type == 1){
   			if($date != NULL){
   				$last_entry_date = $date;
   				// print_r($date);
   			}else{
	   			$this->db->order_by('nepali_date','desc');
	   			$where['department_id'] = $department_id;
   				$last_entry = $this->consumption_model->getConsumptions($where)->row_array();
  				// print_r($where);echo 'here';
  				// print_r($last_entry);
   				$last_entry_date = $last_entry['nepali_date'];
   			}
// exit;
   			$this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
   			$this->db->order_by('order');
   			$this->db->order_by('material_name');
   			$materials = $this->material_department_model->getMaterialDepartments(array('group_id' => $department_id))->result_array();
   			$where['nepali_date'] = $last_entry_date;
   			$date = $last_entry_date;
   			foreach($materials as $i=>$material){
   				$where['material_id'] = $material['material_id'];
   				// $where['nepali_date'] = $date;
   				$recived = $this->dept_request_model->getAllDispatches(
   					array('material_id' => $material['material_id'],
   						'department_id'=>$department_id,'store_confirm_np_date < '=> $date,'department_status'=>1))->row_array();
   				// echo '<pre>';
   				// print_r($this->db->last_query());
   				$consumption = $this->consumption_model->getAllConsumptions(array('material_id' => $material['material_id'],
   						'department_id'=>$department_id,'nepali_date < '=>$date))->row_array();
   				$materials[$i]['opening'] = $recived['all_dispatched'] - $consumption['item_amount'];
   				$materials[$i]['recived'] = $this->dept_request_model->getAllDispatches(
   					array('material_id' => $material['material_id'],
   						'department_id'=>$department_id,'department_status'=>1,'store_confirm_np_date'=>$date))->row_array();
   				$where['department_id'] = $department_id;
   				$materials[$i]['consumption'] = $this->consumption_model->getAllConsumptions($where)->row_array();
   			}
   			// echo $this->db->last_query();
   			// echo '<pre>';print_r($materials[1]['consumption']);exit;
   			$i = 0;
   			$j = 0;
   			$material_opening = 0;
   			$material_recived = 0;
   			$material_consumed = 0;
   			$material_closing = 0;
   			$scrap_opening = 0;
   			$scrap_recived = 0;
   			$scrap_consumed = 0;
   			$scrap_closing = 0;
   			$scrap_record = array();
   			foreach($materials as $key => $material){
   				if($material['category_id'] != 2 && $material['category_id'] != 4){
	   				$material_record[$i]['0'] = $material['material_name'];
	   				$material_record[$i]['1'] = $material['opening'];
	   				$material_record[$i]['2'] = ($material['recived']['all_dispatched'] != 0)?$material['recived']['all_dispatched']:0;
	   				$material_record[$i]['3'] = ($material['consumption']['item_amount'] != 0)?$material['consumption']['item_amount']:0;
	   				$material_record[$i]['4'] = $material['opening'] + $material['recived']['all_dispatched'] - $material['consumption']['item_amount'];
	   				$material_opening += $material_record[$i]['1'];
	   				$material_recived += $material_record[$i]['2'] ;
	   				$material_consumed += $material_record[$i]['3'];
	   				$material_closing += $material_record[$i]['4'];
	   				$i++;
   				}else{
   					$scrap_record[$j]['0'] = $material['material_name'];
	   				$scrap_record[$j]['1'] = ($material['category_id'] != 4)?$material['opening']:0;
	   				$scrap_record[$j]['2'] = ($material['recived']['all_dispatched'] != 0)?$material['recived']['all_dispatched']:0;
	   				$scrap_record[$j]['3'] = ($material['consumption']['item_amount'] != 0)?$material['consumption']['item_amount']:0;
	   				$scrap_record[$j]['4'] = $scrap_record[$j]['1'] + $scrap_record[$j]['2'] - $scrap_record[$j]['3'];
	   				$scrap_opening += $scrap_record[$j]['1'];
	   				$scrap_recived += $scrap_record[$j]['2'] ;
	   				$scrap_consumed += $scrap_record[$j]['3'];
	   				$scrap_closing += $scrap_record[$j]['4'];
	   				$j++;
   				}

   			}
   			// echo '<pre>';
   			// print_r($materials);exit;
   				$material_count = count($material_record)+1;
   				$scrap_count = count($scrap_record);

   				$material_record[$material_count][0] = 'Total';
   				$material_record[$material_count][1] = $material_opening;
   				$material_record[$material_count][2] = $material_recived;
   				$material_record[$material_count][3] = $material_consumed;
   				$material_record[$material_count][4] = $material_closing;

   				if($scrap_count > 0){
	   				$scrap_record[$scrap_count][0] = 'Total';
	   				$scrap_record[$scrap_count][1] = $scrap_opening;
	   				$scrap_record[$scrap_count][2] = $scrap_recived;
	   				$scrap_record[$scrap_count][3] = $scrap_consumed;
	   				$scrap_record[$scrap_count][4] = $scrap_closing;
	   			}

   				$consumptions = $this->getconsumption($department_id,$date);
   				$time_consumption = $this->getTimeConsumption($department_id,$date);
// echo '<pre>';print_r($consumptions);exit;
   				if($department_id == 1){
					$filename = "Mixing_Report.pdf";
   				}elseif ($department_id == 3) {
   					$filename = "Printing_Consumption_Report.pdf";
   				}elseif ($department_id == 5) {
   					$filename = "Rexin_ConsumptionReport.pdf";
   				}
			// Send headers
	header("Content-Type: application/pdf");
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header('Content-Disposition: attachment; filename="'.'"');
	header("Content-Transfer-Encoding: binary ");

			$pdf = new FPDF();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',20);

			if($department_id == 1){
			// $pdf->Cell(80);
		    // Title
		    $pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.');
			$pdf->SetFont('Arial','B',8);

				$pdf->Cell(85);
				$pdf->Cell(15,7,'',1);
				$pdf->Cell(30,7,'Without Scrap',1);
				$pdf->Cell(30,7,'With Scrap',1);
			$pdf->Ln();
			$pdf->Cell(115);
			$pdf->Cell(15,7,'Daily',1);
			$pdf->Cell(30,7,number_format($time_consumption['daily_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Cell(30,7,number_format($time_consumption['daily_consumption']['with_scrap']['item_amount'],3),1);
			$pdf->Ln();
			
			$pdf->SetFont('Arial','B',12);
	// $pdf->Ln();//new line

			// $pdf->Cell(80);
				$pdf->Cell(30,10,'Mixing Raw Maretial Consumption And Stock Position');
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(85);
			$pdf->Cell(15,7,'Monthly',1);
			$pdf->Cell(30,7,number_format($time_consumption['monthly_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Cell(30,7,number_format($time_consumption['monthly_consumption']['with_scrap']['item_amount'],3),1);
			// $pdf->Ln();
			
			// $pdf->Ln();
	$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			$pdf->Cell(75);
			$pdf->Cell(15,7,'Yearly',1);
			$pdf->Cell(30,7,number_format($time_consumption['yearly_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Cell(30,7,number_format($time_consumption['yearly_consumption']['with_scrap']['item_amount'],3),1);
			$pdf->Ln();
			// for consumption report
			$pdf->Cell(40,10,'Stock Report');
			$pdf->Ln();
		}else{
			$pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.');
			$pdf->SetFont('Arial','B',8);
			// $pdf->Ln();
			$pdf->Cell(100);
			$pdf->Cell(15,7,'Consumption');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',12);
	// $pdf->Ln();//new line

			if ($this->session->userdata('department_id') == 3) {
				$pdf->Cell(30,10,'Printing Raw Maretial Consumption And Stock Position');
			}elseif ($this->session->userdata('department_id') == 5) {
				$pdf->Cell(30,10,'Rexin Raw Maretial Consumption And Stock Position');
			}
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(100);
			$pdf->Cell(15,7,'Daily',1);
			$pdf->Cell(30,7,number_format($time_consumption['daily_consumption']['without_scrap']['item_amount'],3),1);
			// $pdf->Ln();
			
	$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			$pdf->Cell(90);
			$pdf->Cell(15,7,'Monthly',1);
			$pdf->Cell(30,7,number_format($time_consumption['monthly_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Ln();
			// for consumption report
			$pdf->Cell(40,10,'Stock Report');
			$pdf->Cell(90);
			$pdf->Cell(15,7,'Yearly',1);
			$pdf->Cell(30,7,number_format($time_consumption['yearly_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Ln();
		}
			
			$pdf->SetFont('Arial','B',6);

			$header = array('Material Name','Opening', 'Recived', 'Consumed', 'Closing');

			// Header
			$pdf->Ln();//new line
			$count = count($header);
			for($i = 0; $i < $count; $i++){
				if($i == 0){
		        	$pdf->Cell(70,7,$header[$i],1);
				}else{
		        	$pdf->Cell(30,7,$header[$i],1);
				}
			}
	    	$pdf->Ln();//new line
	    	// Data
		    foreach($material_record as $row)
		    {
		    	$count = count($row);
		    	for($i = 0; $i < $count; $i++){
					if($i == 0){
						$pdf->Cell(70,5.8,$row[$i],1);
					}else{
						$pdf->Cell(30,5.8,number_format($row[$i],3),1);
					}
				}
				$pdf->Ln();
			}
			$pdf->Ln();
			if($scrap_count != 0){
				foreach($scrap_record as $row)
			    {
			    	$count = count($row);
			    	for($i = 0; $i < $count; $i++){
						if($i == 0){
							$pdf->Cell(70,5.8,$row[$i],1);
						}else{
							$pdf->Cell(30,5.8,number_format($row[$i],3),1);
						}
					}
					$pdf->Ln();
				}
			}
			// for consumptions report
			$material_total = array();
			$count = ceil((count($consumptions['materials']['Material']))/6);
			for($d = 0; $d < $count; $d++){
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(40,10,'Consumption Report');
			$pdf->Ln();
			$pdf->SetFont('Arial','B',6);
				foreach ($consumptions as $x => $consumption) {
					foreach ($consumption as $key => $prop) {
						if(!array_key_exists($key, $material_total)){
							$material_total[$key] = 0;
						}
						$pdf->Cell(60,5,$key,1);
						$total = 0;
						foreach ($prop as $i => $value) {
							if($i > ($d+1)*6-6 && $i <= ($d+1)*6 && $i <= count($prop)){
								if($x != 'properties'){
									if($key == 'Material'){
										$pdf->Cell(22,5,$value,1);
									}else{
										// $pdf->Cell(20,6,number_format($value,3),1);
										$pdf->Cell(22,5,number_format($prop[$i],3),1);
									}
									$material_total[$key] += $value;
								}else{
									$pdf->Cell(22,5,$value,1);
								}
							}
						}
						// echo count($prop)/6;
						if($d+1 > count($prop)/5){
							if($x != 'properties'){
								if($key != 'Material')
									$pdf->Cell(22,5,number_format($material_total[$key],3),1);
								else{
									// $pdf->Cell(20,6,'Total',1);
								}
							}else{
								$pdf->Cell(22,5,'',1);
							}
						}
						$pdf->Ln();
					}
					$pdf->Ln();
				}
			}

   		}else if($department_id == 2){
   			
   			$data = $this->getProductionReport($date);
   			if($date == '' || $date == null){
   				$date = $data['consumptions']['0']['nepali_date'];
   			}

   			$consummption_reports = array();
   			$material_report['Raw Material'] = $data['total_raw_material'];
   			$material_report['Bought Scrap'] = $data['total_bought_scrap'];
   			$material_report['Factory Scrap'] = $data['total_factory_scrap'];
   			$material_report['Total'] = $data['total_raw_material'] + $data['total_bought_scrap'] + $data['total_factory_scrap'];

   			$scrap_report['Reusable'] = $data['total_reusable_scrap'];
   			$scrap_report['Lamps and Plates'] = $data['total_lumps_plates_scrap'];
   			$scrap_report['Total'] = $data['total_reusable_scrap'] + $data['total_lumps_plates_scrap'];

   			$total_length = 0;
   			$total_wt = 0;
   			$total_mixing_total = 0;
   			foreach($data['consumptions'] as $i => $report){
   				$consummption_reports[$i]['shift'] = $report['shift'];
   				$consummption_reports[$i]['brand'] = $report['brand_name'];
   				$consummption_reports[$i]['quality'] = $report['quality_name'];
   				$consummption_reports[$i]['color'] = $report['color_name'];
   				$consummption_reports[$i]['emboss'] = $report['emboss_name'];
   				$consummption_reports[$i]['dimension'] = $report['dimension_name'];
   				$consummption_reports[$i]['length'] = $report['length'];
   				$consummption_reports[$i]['nt_wt'] = $report['net_wt'];
   				$total_length += $report['length'];
   				$total_wt += $report['net_wt'];
   				$total_mixing_total += $report['item_amount'];
   			}
   			$time_loss = $this->timeloss_pdf($date);
   			$timeloss_total = 0;
   			foreach($time_loss as $key => $value){
   				$total_time[$key] = 0;
   				foreach($value as $i => $losses){
   					if($key != 'total'){
	   					$loss[$key][$i]['reason'] = $losses['reason'];
	   					$loss[$key][$i]['start_time'] = $losses['start_hr'].'hr '.$losses['start_min'].'min';
	   					$loss[$key][$i]['end_time'] = $losses['end_hr'].'hr '.$losses['end_min'].'min';
	   					$loss[$key][$i]['duration'] = floor($losses['duration']/60).'hr '.($losses['duration']%60).'min';
	   					$total_time[$key] += $losses['duration'];
	   					$timeloss_total += $losses['duration'];
   					}
   				}
   				$loss[$key]['total'] = floor($total_time[$key]/60).'hr '.($total_time[$key]%60).'min';
   			}
   			$total_tl = floor($timeloss_total/60).'hr '.($timeloss_total%60).'min';
   			$working_total = 24*60 - $timeloss_total;
   			$working_time = floor($working_total/60).'hr '.($working_total%60).'min';
   			
   			$filename = "Calender_Report.pdf";
			// Send headers
	header("Content-Type: application/pdf");
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header('Content-Disposition: attachment; filename="'.'"');
	header("Content-Transfer-Encoding: binary ");

   			$pdf = new FPDF();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',20);

			$pdf->Cell(80);
		    // Title
		    $pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.',0,0,'C');
			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();//new line

			$pdf->Cell(80);
			$pdf->Cell(30,10,'Calender Production Report',0,0,'C');
			$pdf->SetFont('Arial','B',8);
			$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			$pdf->Ln();

			$pdf->cell(40,10, 'Material Report');
			$pdf->Cell(60);
			$pdf->cell(40,10, 'Time Loss Report');
			$pdf->Ln();
			$i = 0;
			foreach($material_report as $index => $value){
				$pdf->Cell(40,6, $index,1);
				$pdf->Cell(40,6, number_format($value,3),1);
				$pdf->Cell(20);
				if($i == 0){
					$pdf->Cell(40,6, ('Break Down'),1);
					$pdf->Cell(40,6, ($loss['BreakDown']['total']),1);
				}elseif ($i == 1) {
					$pdf->Cell(40,6, ('Loss Hour'),1);
					$pdf->Cell(40,6, ($loss['LossHour']['total']),1);
				}elseif ($i == 2) {
					$pdf->Cell(40,6, ('Total'),1);
					$pdf->Cell(40,6, ($total_tl),1);
				}elseif ($i == 3) {
					$pdf->Cell(40,6, ('Working Hour'),1);
					$pdf->Cell(40,6, ($working_time),1);
				}
				$pdf->Ln();
				$i++;
			}
			$pdf->cell(40,10, 'Scrap Detail');
			$pdf->Ln();
			foreach($scrap_report as $index => $value){
				$pdf->Cell(40,6, $index,1);
				$pdf->Cell(40,6, number_format($value,3),1);
				$pdf->Ln();
			}
			$pdf->cell(40,10, 'Production Detail');
			$pdf->Ln();
			$header = array('Shift','Brand','Quality','Color','Emboss','Dimension','Length','NT WT');
			foreach($header as $text){
				$pdf->Cell(23,6, $text,1);
			}
			$pdf->Ln();
			foreach($consummption_reports as $index => $value){
				$pdf->Cell(23,6, $value['shift'],1);
				$pdf->Cell(23,6, $value['brand'],1);
				$pdf->Cell(23,6, $value['quality'],1);
				$pdf->Cell(23,6, $value['color'],1);
				$pdf->Cell(23,6, $value['emboss'],1);
				$pdf->Cell(23,6, $value['dimension'],1);
				$pdf->Cell(23,6, number_format($value['length'],3),1);
				$pdf->Cell(23,6, number_format($value['nt_wt'],3),1);
				$pdf->Ln();
			}
			$pdf->Cell(138,6, 'Total',1);
			$pdf->Cell(23,6, number_format($total_length,3),1);
			$pdf->Cell(23,6, number_format($total_wt,3),1);
			$pdf->Ln();
			// monthly total
			$pdf->Cell(138,6, 'Monthly Total',1);
			$pdf->Cell(23,6, number_format($data['total_monthly']['total_length'],3),1);
			$pdf->Cell(23,6, number_format($data['total_monthly']['net_weight'],3),1);
			$pdf->Ln();
			// yearly total
			$pdf->Cell(138,6, 'Yearly Total',1);
			$pdf->Cell(23,6, number_format($data['total_yerarly']['total_length'],3),1);
			$pdf->Cell(23,6, number_format($data['total_yerarly']['net_weight'],3),1);
			$pdf->Ln();
			// Unaccounted Loss
			$pdf->Ln();
			$pdf->Cell(138,6, 'Unaccounted Loss',1);
			$pdf->Cell(23,6, number_format($total_mixing_total - $total_wt - $scrap_report['Total'],3),1);
			$pdf->Cell(23,6, number_format(($total_wt > 0)?(($total_mixing_total - $total_wt - $scrap_report['Total'])*100/$total_wt):0,2).'%',1);
			$pdf->Ln();
			// time loss
			$pdf->AddPage();
			$pdf->cell(40,10, 'Time Loss');
			$pdf->Ln();
			$header_tl = array('Reason','Start Time','End Time','Duration');
			// print_r($loss);
			foreach ($loss as $key => $tls) {
				$pdf->cell(40,10, $key);
				$pdf->Ln();
				foreach ($header_tl as $t => $header) {
					if($t == 0){
						$pdf->Cell(80,6, ($header),1);
					}else{
						$pdf->Cell(23,6, ($header),1);
					}
				}
				$pdf->Ln();
				foreach ($tls as $h => $tl) {
					if($h != 'total'){
						$pdf->Cell(80,6, ($tl['reason']),1);
						$pdf->Cell(23,6, ($tl['start_time']),1);
						$pdf->Cell(23,6, ($tl['end_time']),1);
						$pdf->Cell(23,6, ($tl['duration']),1);
						$pdf->Ln();
					}
				}
				$pdf->Cell(126,6, 'Total',1);
				$pdf->Cell(23,6, $tls['total'],1);
				$pdf->Ln();
			}
   		}else if($department_id == 4){
   			// echo '<pre>';
   			// echo $date;
   			$last_entry = array();
   			if($date != NULL){
   				$last_entry = $this->laminating_shift_report_model->getLaminatingShiftReports(array('date-np'=>$date))->row_array();
   			}
   			if($date == NULL || count($last_entry) == 0){
	   			$this->db->order_by('date-np','desc');
	    		$last_entry = $this->laminating_shift_report_model->getLaminatingShiftReports()->row_array();
    			$date = $last_entry['date-np'];
   			}
   			// echo $date;exit;
			$shift_reports = $this->getShiftReports($date);
			// echo '<pre>';print_r($shift_reports);exit;
   			foreach($shift_reports as $key => $report){
   				if($key != 'total'){
	   				foreach ($report as $i => $value) {
	   					$shifts[$key][$i]['brand'] = $value['brand_name'];
	   					$shifts[$key][$i]['color'] = $value['color'];
	   					$shifts[$key][$i]['base_ut'] = $value['base_ut'];
	   					$shifts[$key][$i]['base_mt'] = $value['base_mt'];
	   					$shifts[$key][$i]['base_ot'] = $value['base_ot'];
	   					$shifts[$key][$i]['print_film'] = $value['print_film'];
	   					$shifts[$key][$i]['ct'] = $value['ct'];
	   					$shifts[$key][$i]['dull_ct'] = $value['dull_ct'];
	   					$shifts[$key][$i]['output'] = $value['output'];
	   				}
   				}
   			}
   			// $timeloss['A'] = $this->timeloss_pdf($date,'A');
   			// $timeloss['B'] = $this->timeloss_pdf($date,'B');
   			$timeloss[] = $this->timeloss_pdf($date);
   			// $timeloss_report['A'] = $this->timeloss_all_pdf($date,'A');
   			// $timeloss_report['B'] = $this->timeloss_all_pdf($date,'B');
   			$timeloss_report = $this->timeloss_all_pdf($date);
   			// $total_tl['breakdown'] = $timeloss['A']['BreakDown']['total']['total'] + $timeloss['B']['BreakDown']['total']['total'];
   			$total_tl['breakdown'] = $timeloss[0]['BreakDown']['total']['total'] + $timeloss[0]['BreakDown']['total']['total'];
   			// $total_tl['losshour'] = $timeloss['A']['LossHour']['total']['total'] + $timeloss['B']['LossHour']['total']['total'];
   			$total_tl['losshour'] = $timeloss[0]['LossHour']['total']['total'] + $timeloss[0]['LossHour']['total']['total'];
   			$total_tl['working'] = (24*60) - ($total_tl['breakdown'] + $total_tl['losshour']);
   			// echo '<pre>';
   			// print_r($timeloss);
   			// print_r($timeloss_report);
   			// exit;
			$filename = "Laminating_Report.pdf";
			// Send headers
	header("Content-Type: application/pdf");
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header('Content-Disposition: attachment; filename="'.'"');
	header("Content-Transfer-Encoding: binary ");

   			$pdf = new FPDF();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',20);

			// $pdf->Cell(80);
		    // Title
		    $pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.',0,0);

			$pdf->SetFont('Arial','B',8);
		    $pdf->cell(60);
		    $pdf->cell(30, 6, 'Time Report');
		    $pdf->Ln();
		    $pdf->cell(90);
		    $pdf->cell(30, 6, 'Total BreakDown',1);
		    $pdf->cell(30, 6, floor($total_tl['breakdown']/60).'hr '.($total_tl['breakdown']%60).'min',1);


			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();//new line

			// $pdf->Cell(80);
			$pdf->Cell(30,10,'Laminating Shift Production Report',0,0);
			$pdf->SetFont('Arial','B',8);
		    $pdf->cell(60);
		    $pdf->cell(30, 6, 'Total LossHour',1);
		    $pdf->cell(30, 6, floor($total_tl['losshour']/60).'hr '.($total_tl['losshour']%60).'min',1);
		    $pdf->Ln();
		    $pdf->cell(90);
		    $pdf->cell(30, 6, 'Total Working',1);
		    $pdf->cell(30, 6, floor($total_tl['working']/60).'hr '.($total_tl['working']%60).'min',1);

			$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			// $pdf->Ln();
			foreach ($shifts as $key => $shift) {
				$pdf->Ln();
				$pdf->Cell(80);
				$pdf->Cell(40,10,'Shift : '.$key);
				$pdf->Ln();//new line
				$pdf->Cell(40,10,'Shift Incharge:');
				$pdf->Ln();

				$header = array('Shift','Brand','Color','Base-UT','Base-MT','Base-OT','Print Film','CT','Dull CT','Output');
				foreach ($header as $x => $value) {
					if($x == 0){
						$pdf->Cell(10,6, ($value),1);
					}else{
						$pdf->Cell(20,6, ($value),1);
					}
				}
				$pdf->Ln();
				foreach ($shift as $value) {
					$pdf->Cell(10,6, $key,1);
					$pdf->Cell(20,6, ($value['brand']),1);
					$pdf->Cell(20,6, ($value['color']),1);
					$pdf->Cell(20,6, number_format($value['base_ut'],3),1);
					$pdf->Cell(20,6, number_format($value['base_mt'],3),1);
					$pdf->Cell(20,6, number_format($value['base_ot'],3),1);
					$pdf->Cell(20,6, number_format($value['print_film'],3),1);
					$pdf->Cell(20,6, number_format($value['ct'],3),1);
					$pdf->Cell(20,6, number_format($value['dull_ct'],3),1);
					$pdf->Cell(20,6, number_format($value['output'],3),1);
					$pdf->Ln();
				}
				$pdf->Cell(50,6, 'Total',1);
				// echo $shift_reports['total'][$key]['daily']['total_base_mt'].'<br>';
				$pdf->Cell(20,6, number_format($shift_reports['total'][$key]['daily']['total_base_ut'],3),1);
				$pdf->Cell(20,6, number_format($shift_reports['total'][$key]['daily']['total_base_mt'],3),1);
				$pdf->Cell(20,6, number_format($shift_reports['total'][$key]['daily']['total_base_ot'],3),1);
				$pdf->Cell(20,6, number_format($shift_reports['total'][$key]['daily']['total_print_film'],3),1);
				$pdf->Cell(20,6, number_format($shift_reports['total'][$key]['daily']['total_ct'],3),1);
				$pdf->Cell(20,6, number_format($shift_reports['total'][$key]['daily']['total_dull_ct'],3),1);
				$pdf->Cell(20,6, number_format($shift_reports['total'][$key]['daily']['total_output'],3),1);
				$pdf->Ln();
			}
			$pdf->Ln();
			$pdf->Cell(50,6, 'Total(A + B)',1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['daily']['total_base_ut'] + $shift_reports['total']['B']['daily']['total_base_ut']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['daily']['total_base_mt'] + $shift_reports['total']['B']['daily']['total_base_mt']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['daily']['total_base_ot'] + $shift_reports['total']['B']['daily']['total_base_ot']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['daily']['total_print_film'] + $shift_reports['total']['B']['daily']['total_print_film']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['daily']['total_ct'] + $shift_reports['total']['B']['daily']['total_ct']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['daily']['total_dull_ct'] + $shift_reports['total']['B']['daily']['total_dull_ct']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['daily']['total_output'] + $shift_reports['total']['B']['daily']['total_output']),3),1);
			$pdf->Ln();
			// monthly
			$pdf->Cell(50,6, 'This Month',1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['monthly']['total_base_ut'] + $shift_reports['total']['B']['monthly']['total_base_ut']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['monthly']['total_base_mt'] + $shift_reports['total']['B']['monthly']['total_base_mt']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['monthly']['total_base_ot'] + $shift_reports['total']['B']['monthly']['total_base_ot']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['monthly']['total_print_film'] + $shift_reports['total']['B']['monthly']['total_print_film']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['monthly']['total_ct'] + $shift_reports['total']['B']['monthly']['total_ct']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['monthly']['total_dull_ct'] + $shift_reports['total']['B']['monthly']['total_dull_ct']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['monthly']['total_output'] + $shift_reports['total']['B']['monthly']['total_output']),3),1);
			$pdf->Ln();
			// yearly
			$pdf->Cell(50,6, 'This Year',1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['yearly']['total_base_ut'] + $shift_reports['total']['B']['yearly']['total_base_ut']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['yearly']['total_base_mt'] + $shift_reports['total']['B']['yearly']['total_base_mt']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['yearly']['total_base_ot'] + $shift_reports['total']['B']['yearly']['total_base_ot']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['yearly']['total_print_film'] + $shift_reports['total']['B']['yearly']['total_print_film']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['yearly']['total_ct'] + $shift_reports['total']['B']['yearly']['total_ct']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['yearly']['total_dull_ct'] + $shift_reports['total']['B']['yearly']['total_dull_ct']),3),1);
			$pdf->Cell(20,6, number_format(($shift_reports['total']['A']['yearly']['total_output'] + $shift_reports['total']['B']['yearly']['total_output']),3),1);
			$pdf->Ln();

			$pdf->Ln();
			$pdf->AddPage();
			$pdf->Cell(40,10,'Time Loss Report');
			$pdf->Ln();
			
			$z = array();
			$total_type_loss = array();
			$total_loss_duration = 0;
			$headers = array('Reason','Start Time','End Time','Duration');
			// foreach ($timeloss_report as $key => $value) {
			foreach ($timeloss_report as $type => $loss) {
					$total_loss_duration = 0;
				// foreach ($value as $type => $loss) {
				// 	if(!in_array($type,$z)){
				// 		$z[$type] = $type;
						$total_type_loss[$type] = 0;
				// 	}
					// $pdf->Cell(60,10,'Shift : '.$key);
					// $pdf->Cell(80);
					$pdf->Cell(60,10,'Loss Type : '.$type);
					$pdf->Ln();
					foreach ($headers as $header){
						($header == 'Reason')?$pdf->Cell(100,6, $header,1):$pdf->Cell(30,6, $header,1);
					}
					$pdf->Ln();
					foreach ($loss as $i => $reason) {
						$pdf->Cell(100,6, $reason['reason'],1);
						$pdf->Cell(30,6, ($reason['start_hr'].'hr '.$reason['start_min'].'min'),1);
						$pdf->Cell(30,6, ($reason['end_hr']).'hr '.$reason['end_min'].'min',1);
						$pdf->Cell(30,6, (floor($reason['duration']/60).'hr '.($reason['duration']%60).'min'),1);
						$pdf->Ln();

						$total_loss_duration += $reason['duration'];
						$total_type_loss[$type] += $reason['duration'];
					}
					$pdf->Cell(160,6, 'Total',1);
					$pdf->Cell(30,6, (floor($total_type_loss[$type]/60).'hr '.($total_type_loss[$type]%60).'min'),1);
					$pdf->Ln();
				// }
				// $pdf->Ln();
				// $pdf->Cell(65,6, 'Total Loss Hour in Shift',1);
				// $pdf->Cell(30,6, (floor($total_loss_duration/60).'hr '.($total_loss_duration%60).'min'),1);
				// $total_working_hour = 24*60 - $total_loss_duration;
				// $pdf->Cell(65,6, 'Total Working Hour in Shift',1);
				// $pdf->Cell(30,6, (floor($total_working_hour/60).'hr '.($total_working_hour%60).'min'),1);
				$pdf->Ln();
			}
   		}elseif ($department_id == 3) {
   			if($date == NULL){
	   			$this->db->order_by('date_np','desc');
	   			$last_entry = $this->print_shift_report_model->getPrintShiftReports()->row_array();
	   			$date = $last_entry['date_np'];
   			}
   			$report = $this->getPrintingPdf($date);
   			$timeloss_report = $this->timeloss_pdf($date);
   			$timeloss_all_report = $this->timeloss_all_pdf($date);
   			// echo '<pre>';print_r($timeloss_report);
   			// exit;
   			$filename = "Printing_Report.pdf";
			// Send headers
	header("Content-Type: application/pdf");
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header('Content-Disposition: attachment; filename="'.'"');
	header("Content-Transfer-Encoding: binary ");

   			$pdf = new FPDF();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',20);

			$pdf->Cell(80);
		    // Title
		    $pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.',0,0,'C');
			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();//new line
// 
			$pdf->Cell(80);
			$pdf->Cell(30,10,'Printing Production Report',0,0,'C');
			$pdf->SetFont('Arial','B',8);
			$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			$pdf->Ln();

			$pdf->Cell(40,10,'Shift Report');
			$pdf->Ln();

			$headers = array('Shift', 'Dimension', 'COlor', 'Input', 'Output', 'Unprinted Scrap', 'Printed Scrap');
			foreach($headers as $text){
				$pdf->Cell(27,6, $text,1);
			}
			$pdf->Ln();
			foreach ($report['shift_reports'] as $key => $shift_report) {
				$pdf->Cell(27,6, $shift_report['shift'],1);
				$pdf->Cell(27,6, $shift_report['dimension_name'],1);
				$pdf->Cell(27,6, $shift_report['color_name'],1);
				$pdf->Cell(27,6, number_format($shift_report['input'],3),1);
				$pdf->Cell(27,6, number_format($shift_report['output'],3),1);
				$pdf->Cell(27,6, number_format($shift_report['unprinted_scrap'],3),1);
				$pdf->Cell(27,6, number_format($shift_report['printed_scrap'],3),1);
				$pdf->Ln();
			}
			foreach ( array_reverse($report['total']) as $key => $value) {
				$pdf->Cell(81,6, $key,1);
				$pdf->Cell(27,6, number_format($value['total_input'],3),1);
				$pdf->Cell(27,6, number_format($value['total_output'],3),1);
				$pdf->Cell(27,6, number_format($value['total_unprinted_scrap'],3),1);
				$pdf->Cell(27,6, number_format($value['total_printed_scrap'],3),1);
				$pdf->Ln();
			}
			$pdf->Cell(40,10,'Input Output Report');
			$pdf->Ln();
			$pdf->Cell(63,6, '',1);
			$pdf->Cell(63,6, 'Input',1);
			$pdf->Cell(63,6, 'Output',1);
			$pdf->Ln();
			foreach ( array_reverse($report['total']) as $key => $value) {
				$pdf->Cell(63,6, $key,1);
				$pdf->Cell(63,6, number_format($value['total_input'],3),1);
				$pdf->Cell(63,6, number_format($value['total_output'],3),1);
				$pdf->Ln();
			}
			$pdf->AddPage();
			$pdf->Cell(40,10,'Time Loss Report');
			$pdf->Ln();
			$total_working_hour = 24*60;
			foreach ($timeloss_report as $key => $value) {
				$pdf->Cell(63,6, $key,1);
				$pdf->Cell(63,6, (floor($value['total']['total']/60).'hr '.($value['total']['total']%60).'min'),1);
					$total_working_hour -= $value['total']['total'];
				$pdf->Ln();
			}
			$pdf->Cell(63,6, 'Working Hour',1);
			$pdf->Cell(63,6, (floor($total_working_hour/60).'hr '.($total_working_hour%60).'min'),1);
			$pdf->Ln();
			$pdf->Cell(40,10, 'Time Loss Detail' );
			$pdf->Ln();
			foreach ($timeloss_all_report as $key => $loss) {
				$pdf->Cell(40,10, $key);
				$pdf->Ln();
				$headers = array('Reason','Start Time','End Time','Duration');
				foreach($headers as $text){
					($text == 'Reason')?$pdf->Cell(80,6, $text,1):$pdf->Cell(27,6, $text,1);
				}
				$pdf->Ln();
				foreach ($loss as $i => $value) {
					$pdf->Cell(80,6, $value['reason'],1);
					$pdf->Cell(27,6, $value['start_hr'].'hr '.$value['start_min'].'min',1);
					$pdf->Cell(27,6, $value['end_hr'].'hr '.$value['end_min'].'min',1);
					$pdf->Cell(27,6, (floor($value['duration']/60).'hr '.($value['duration']%60).'min'),1);
					$pdf->Ln();
				}
				$pdf->Cell(134,6, 'Total',1);
				$pdf->Cell(27,6, (floor($timeloss_report[$key]['total']['total']/60).'hr '.($timeloss_report[$key]['total']['total']%60).'min'),1);
				$pdf->Ln();
			}
   		}elseif ($department_id == 5){
			$last_entry = $this->consumption_model->getConsumptions(array('nepali_date'=>$date))->row_array();
   			if($date != NULL && $date !='' && count($last_entry)>0){
   				$last_entry_date = $date;
   				
   			}else{
	   			$this->db->order_by('nepali_date','desc');
	   			$where['department_id'] = 5;
   				$last_entry = $this->consumption_model->getConsumptions($where)->row_array();
   				$date = $last_entry['nepali_date'];
   			}
   			// echo $date;exit;
   			$consumptions = $this->getconsumption(5,$date);
   				$time_consumption = $this->getTimeConsumption(5,$date);
   			// echo '<pre>';print_r($time_consumption);exit;
   				// time loss
   				$time_loss = $this->timeloss_pdf($date);
   			$timeloss_total = 0;
   			foreach($time_loss as $key => $value){
   				// echo '<pre>';print_r($value);
   				$total_time[$key] = 0;
   				foreach($value as $i => $losses){
   					if($i != 'total'){
	   					$loss[$key][$i]['reason'] = $losses['reason'];
	   					$loss[$key][$i]['start_time'] = $losses['start_hr'].'hr '.$losses['start_min'].'min';
	   					$loss[$key][$i]['end_time'] = $losses['end_hr'].'hr '.$losses['end_min'].'min';
	   					$loss[$key][$i]['duration'] = floor($losses['duration']/60).'hr '.($losses['duration']%60).'min';
	   					$total_time[$key] += $losses['duration'];
	   					$timeloss_total += $losses['duration'];
	   					// echo $total_time[$key];
   					}
   				}
   				$loss[$key]['total'] = floor($total_time[$key]/60).'hr '.($total_time[$key]%60).'min';
   			}
   			$total_tl = floor($timeloss_total/60).'hr '.($timeloss_total%60).'min';
   			$working_total = 24*60 - $timeloss_total;
   			$working_time = floor($working_total/60).'hr '.($working_total%60).'min';

   			// coating report
   			$reports = $this->getRexinPdf($date);
   			
   			$filename = "Rexin_Report.pdf";
			// Send headers
	header("Content-Type: application/pdf");
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header('Content-Disposition: attachment; filename="'.'"');
	header("Content-Transfer-Encoding: binary ");

   			$pdf = new FPDF();
			$pdf->AddPage('L','A4');
			$pdf->SetFont('Arial','B',20);

			$pdf->Cell(80);
		    // Title
		    $pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.',0,0,'C');
			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();//new line

			$pdf->Cell(80);
			$pdf->Cell(30,10,'Rexin Report',0,0,'C');
			$pdf->SetFont('Arial','B',8);
			$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			$pdf->Ln();

			$pdf->Cell(40,10,'Consumption Report');
			$pdf->Cell(70);
			$pdf->cell(40,10, 'Time Loss Report');
			$pdf->Ln();
			$pdf->Cell(30,6,'Daily',1);
			$pdf->Cell(30,6,number_format($time_consumption['daily_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Cell(30,6,number_format($time_consumption['daily_consumption']['with_scrap']['item_amount'],3),1);
			$pdf->Cell(20);
			$pdf->Cell(40,6, ('Break Down'),1);
			$pdf->Cell(40,6, ($loss['BreakDown']['total']),1);
			$pdf->Ln();
			$pdf->Cell(30,6,'Monthly',1);
			$pdf->Cell(30,6,number_format($time_consumption['monthly_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Cell(30,6,number_format($time_consumption['monthly_consumption']['with_scrap']['item_amount'],3),1);
			$pdf->Cell(20);
			$pdf->Cell(40,6, ('Loss Hour'),1);
			$pdf->Cell(40,6, ($loss['LossHour']['total']),1);
			$pdf->Ln();
			$pdf->Cell(30,6,'Yearly',1);
			$pdf->Cell(30,6,number_format($time_consumption['yearly_consumption']['without_scrap']['item_amount'],3),1);
			$pdf->Cell(30,6,number_format($time_consumption['yearly_consumption']['with_scrap']['item_amount'],3),1);
			$pdf->Cell(20);
			$pdf->Cell(40,6, ('Total'),1);
			$pdf->Cell(40,6, ($total_tl),1);
			$pdf->Ln();
			$pdf->Cell(110);
			$pdf->Cell(40,6, ('Working Hour'),1);
			$pdf->Cell(40,6, ($working_time),1);
			$pdf->Ln();
			$pdf->Ln();
			$header_coating = array('Brand Name','Color','Release Paper', 'Fabric','Thickness','Width','Production','TC','FC','AC','Fabric Wt.','Net Wt.','Remark');
			$e = array();
			foreach($header_coating as $text){
				if($text == 'TC' || $text == 'FC' || $text == 'AC' || $text == 'Width' || $text == 'Fabric Wt.' || $text == 'Net Wt.' || $text == 'Production'){
					$pdf->Cell(20,6, $text,1);
					$e[] = 20;
				}elseif($text == 'Thickness'){
					$pdf->Cell(15,6, $text,1);
					$e[] = 15;
				}else{
					$pdf->Cell(25,6, $text,1);
					$e[] = 25;
				}
			}
			$pdf->Ln();
			$pdf->SetWidths(array($e[0],$e[1],$e[2],$e[3],$e[4],$e[5],$e[6],$e[7],$e[8],$e[9],$e[10],$e[11],$e[12]));
		srand(microtime()*1000000);
		foreach ($reports['reports'] as $key => $value) {
		    $pdf->Row(array($value['material']['brand_name'],
		    	$value['material']['color_name'],
		    	$value['material']['rp_name'],
		    	$value['material']['fabric_name'],
		    	$value['material']['thickness_name'],
		    	$value['material']['weight'],
		    	number_format($value['production'],3),
		    	number_format($value['top_coat'],3),
		    	number_format($value['foam_coat'],3),
		    	number_format($value['adhesive_coat'],3),
		    	number_format($value['fabric_wt'] * $value['production'],3),
		    	number_format($value['net_wt'],3),
		    	''
		    	));
		}
		$pdf->Row(array('',
    	'',
    	'',
    	'',
    	'',
    	'Total',
    	number_format($reports['daily']['total_production'],3),
    	number_format($reports['daily']['total_top_coat'],3),
    	number_format($reports['daily']['total_foam_coat'],3),
    	number_format($reports['daily']['total_adhesive_coat'],3),
    	number_format($reports['daily']['total_gross_wt'],3),
    	number_format($reports['daily']['total_net_wt'],3),
    	''
    	));
		$pdf->Ln();
		$pdf->Ln();
			$header_report = array('','production','Production Kg','Fabric Mtr.','Fabric Kg');
			foreach($header_report as $text){
				$pdf->Cell(25,6, $text,1);
			}
			$pdf->Ln();
			$pdf->cell(25,8,'Today',1);
			$pdf->cell(25,8,number_format($reports['daily']['total_production'],3),1);
			$pdf->cell(25,8,number_format($reports['daily']['total_net_wt'],3),1);
			$pdf->cell(25,8,number_format($reports['daily']['total_production'],3),1);
			$pdf->cell(25,8,number_format($reports['daily']['total_gross_wt'],3),1);
	   		
	   		$pdf->Ln();
	   		$pdf->cell(25,8,'Monthly',1);
			$pdf->cell(25,8,number_format($reports['monthly']['total_production'],3),1);
			$pdf->cell(25,8,number_format($reports['monthly']['total_net_wt'],3),1);
			$pdf->cell(25,8,number_format($reports['monthly']['total_production'],3),1);
			$pdf->cell(25,8,number_format($reports['monthly']['total_gross_wt'],3),1);
	   		
	   		$pdf->Ln();
	   		$pdf->cell(25,8,'Yearly',1);
			$pdf->cell(25,8,number_format($reports['yearly']['total_production'],3),1);
			$pdf->cell(25,8,number_format($reports['yearly']['total_net_wt'],3),1);
			$pdf->cell(25,8,number_format($reports['yearly']['total_production'],3),1);
			$pdf->cell(25,8,number_format($reports['yearly']['total_gross_wt'],3),1);
			$pdf->Ln();
			foreach ($loss as $key => $tls) {
				$pdf->cell(40,10, $key);
				$pdf->Ln();
				foreach ($tls as $h => $tl) {
					if($h != 'total'){
						$pdf->Cell(80,5, ($tl['reason']),1);
						$pdf->Cell(23,5, ($tl['start_time']),1);
						$pdf->Cell(23,5, ($tl['end_time']),1);
						$pdf->Cell(23,5, ($tl['duration']),1);
						$pdf->Ln();
					}
				}
				$pdf->Cell(126,5, 'Total',1);
				$pdf->Cell(23,5, $tls['total'],1);
				$pdf->Ln();
			}
	   		}
	   		$pdf->Ln();
		$pdf->Output($filename,'D');
   	}
   	// get timewise consumption record
	private function getTimeConsumption($department_id,$date)// for daily, weekly & yearly consumption
	{
		$this->consumption_model->joins = array('MATERIALS');
		$dates = explode('-', $date);

		$where = array();
		$where = $this->getTimeWhere($date,0,'nepali_date');
		$where['consumptions.department_id'] = $department_id;
		$consumption['yearly_consumption']['with_scrap'] = $this->consumption_model->getAllConsumptions($where)->row_array();

		$where['category_id <> 2 && category_id <> 4'] = NULL;
		$consumption['yearly_consumption']['without_scrap'] = $this->consumption_model->getAllConsumptions($where)->row_array();

		$where = array();
		$where = $this->getTimeWhere($date,1,'nepali_date');
		$where['consumptions.department_id'] = $department_id;
		$consumption['monthly_consumption']['with_scrap'] = $this->consumption_model->getAllConsumptions($where)->row_array();

		$where = array();
		$where['category_id <> 2 && category_id <> 4'] = NULL;
		$where = $this->getTimeWhere($date,1,'nepali_date');
		$where['consumptions.department_id'] = $department_id;
		$where['category_id <> 2 && category_id <> 4'] = NULL;
		$consumption['monthly_consumption']['without_scrap'] = $this->consumption_model->getAllConsumptions($where)->row_array();

		$where = array();
		$where = $this->getTimeWhere($date,2,'nepali_date');
		$where['consumptions.department_id'] = $department_id;
		$consumption['daily_consumption']['with_scrap'] = $this->consumption_model->getAllConsumptions($where)->row_array();

		$where['category_id <> 2 && category_id <> 4'] = NULL;
		$consumption['daily_consumption']['without_scrap'] = $this->consumption_model->getAllConsumptions($where)->row_array();

		return $consumption;
	}
	// get material consumption record
	private function getconsumption($department_id,$date)
	{
		$this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
		$this->db->order_by('order');
		$this->db->order_by('material_name');
		$materials = $this->material_department_model->getMaterialDepartments(array('group_id' => $department_id))->result_array();
		$this->consumption_model->joins = array('MATERIAL_PROPERTY','BRAND','QUALITY','DIMENSION','COLOR','EMBOSS','DESIGNS','COATING');
		$this->db->where('consumptions.department_id',$department_id);
		$this->db->where('consumptions.nepali_date',$date);
		$this->db->order_by('group');
		$rows = $this->consumption_model->getConsumptions()->result_array();
		$data = array();
        $properties = $this->getProperty();
        foreach ($rows as $row) {
            $data[$row['group']]['shifts'][$row['shift']]['Brand'] = $row['brand_name'];
            $data[$row['group']]['shifts'][$row['shift']]['Quality'] = $row['quality_name'];
            $data[$row['group']]['shifts'][$row['shift']]['Dimension'] = $row['dimension_name'];
            $data[$row['group']]['shifts'][$row['shift']]['Color'] = $row['color_name'];
            $data[$row['group']]['shifts'][$row['shift']]['Emboss'] = $row['emboss_name'];
            $data[$row['group']]['shifts'][$row['shift']]['mat'] = $row['material'];
            $data[$row['group']]['shifts'][$row['shift']]['materials'][$row['material_id']] = $row['consumption_amount'];
            $data[$row['group']]['shifts'][$row['shift']]['Coating'] = $row['coating_name'];
        }

        $this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
   			$this->db->order_by('order');
   			$this->db->order_by('material_name');
   			$materials = $this->material_department_model->getMaterialDepartments(array('group_id' => $department_id))->result_array();

		$i = 1;
		$consumption['properties'] = array();
		// $consumption['materials']['Material'][0] = 'Material';
		$count = count($data);
		for($j=1;$j<=$count;$j++){
			$consumption['materials']['Material'][$j] = '';
		}
		$consumption['materials']['Material'][$count+1] = 'Total';
		foreach ($data as $key => $groups) {
			foreach ($groups['shifts'] as $index => $shifts){
				if($department_id == 3){
					$consumption['properties']['Materials'][$i] = $shifts['mat'];
					$consumption['properties']['shift'][$i] = $index;
				}elseif($department_id == 5){
					$consumption['properties']['brand'][$i] = $shifts['Brand'];
					$consumption['properties']['coating'][$i] = $shifts['Coating'];
					$consumption['properties']['shift'][$i] = $index;
				}else{
					$consumption['properties']['quality'][$i] = $shifts['Quality'];
					$consumption['properties']['Color'][$i] = $shifts['Color'];
					$consumption['properties']['dimension'][$i] = $shifts['Dimension'];
					$consumption['properties']['shift'][$i] = $index;
				}

				$material_total = 0;
				$scrap_total = 0;
				foreach($materials as $material){
					if($material['category_id'] != 2 && $material['category_id'] != 4){
						$consumption['materials'][$material['material_name']][$i] = (@$shifts['materials'][$material['material_id']])?$shifts['materials'][$material['material_id']]:0;
						$material_total += $consumption['materials'][$material['material_name']][$i];
					}else{
						$consumption['scraps'][$material['material_name']][$i] = (@$shifts['materials'][$material['material_id']])?$shifts['materials'][$material['material_id']]:0;
						$scrap_total += $consumption['scraps'][$material['material_name']][$i];
					}
				}
			}
					$consumption['materials']['Total'][$i] = $material_total;
					if(array_key_exists('scrap', $consumption)){
						$consumption['scraps']['Total'][$i] = $scrap_total;
					}
					$i++;
		}
		
		return $consumption;
	}
	private function getProperty(){
        $department_id = $this->session->userdata('department_id');
        $this->db->order_by('order','asc');
        $result = $this->department_materialproperty_model->getDepartmentMaterialproperties(array('department_id'=>$department_id))->result_array();
        return $result;
    }
    // calender production report
    private function getProductionReport($date = NULL)
    {
    	$this->db->order_by('date-np','desc');
		$last_input = $this->calander_shift_report_model->getCalanderShiftReports()->row_array();
		$last_entry_date = $last_input['date-np'];
		if($date){
			// echo '<pre>date:';print_r($date);echo '</pre>';exit;
		}else{
			$date = $last_entry_date;
		}

		$total_length = 0;
	   	$total_wt = 0;
	   	$total_mixing = 0;
	   	$where_date = $this->getTimeWhere($date,1,'nepali_date');
	   	$this->db->where('nepali_date <=',$date);
	   	$this->db->group_by('group');
	   	$monthly = $this->consumption_model->getAllConsumptions($where_date)->result_array();
	   	foreach($monthly as $total){
	   		$report = $this->calander_shift_report_model->getCalanderShiftReports(array('report_id' => $total['calander_report_id']))->row_array();
	   		$total_length += @$report['length'];
	   		$total_wt += @$report['net_wt'];
	   		$total_mixing += $total['consumption_amount'];
	   	}
	   	$data['total_monthly']['total_length'] = $total_length;
	   	$data['total_monthly']['net_weight'] = $total_wt;

	   	$total_length = 0;
	   	$total_wt = 0;
	   	$total_mixing = 0;
	   	$where_date = $this->getTimeWhere($date,0,'`nepali_date`');
	   	$this->db->where('nepali_date <=',$date);
	   	$this->db->group_by('group');
	   	$yearly = $this->consumption_model->getAllConsumptions($where_date)->result_array();
	   	foreach($yearly as $total){
	   		$report = $this->calander_shift_report_model->getCalanderShiftReports(array('report_id' => $total['calander_report_id']))->row_array();
	   		$total_length += @$report['length'];
	   		$total_wt += @$report['net_wt'];
	   		$total_mixing += $total['consumption_amount'];
	   	}
	   	$data['total_yerarly']['total_length'] = $total_length;
	   	$data['total_yerarly']['net_weight'] = $total_wt;

		$data['total_raw_material'] = 0;
		$data['total_bought_scrap'] = 0;
		$data['total_factory_scrap'] = 0;
		$reusable = $this->scrap_record_model->getScrapRecords(array('department_id'=>2,'date_np'=>$date,'type_id'=>7))->row_array();
		$data['total_reusable_scrap'] = (@$reusable['amount'])?$reusable['amount']:0;
		$lump = $this->scrap_record_model->getScrapRecords(array('department_id'=>2,'date_np'=>$date,'type_id'=>8))->row_array();
		$data['total_lumps_plates_scrap'] = (@$lump['amount'])?$lump['amount']:0;

		$where = array('consumptions.department_id'=>1,'calander_report_id <>'=>0);
		$this->db->like('date-np',$date);
	   	$this->db->group_by('group');
	   	$this->consumption_model->joins = array('MATERIAL_PROPERTY','CALANDER_SHIFT_REPORT','MATERIALS','BRAND','QUALITY','DIMENSION','COLOR','EMBOSS');
	   	$data['consumptions'] = $this->consumption_model->getAllConsumptions($where)->result_array();
	   	$i = 0;
	   	foreach($data['consumptions'] as $consumption){
	   		$materials = $this->consumption_model->getConsumptions(array('group'=>$consumption['group']))->result_array();
	   		foreach ($materials as $key => $material) {
	   			if ($material['category_id'] != 2 && $material['category_id'] != 4) {
	   				$data['total_raw_material'] += $material['consumption_amount'];
	   			}else if($material['category_id'] == 2){
	   				$data['total_bought_scrap'] += $material['consumption_amount'];
	   			}else{
	   				$data['total_factory_scrap'] += $material['consumption_amount'];
	   			}
	   		}
	   		$i++;
	   	}
	   	
    	return $data;
    }
    // for timeloss to pdf
    private function timeloss_pdf($date = NULL,$shift = NULL)
    {
    	$department_id = $this->session->userdata('department_id');
    	$where['department_id'] = $department_id;
   		if($date == NULL || $date == ''){
	   		$this->time_loss_model->joins = array('TL_REASON_FULL');
	   		$this->db->order_by('date_np','desc');
	   		$last_entry = $this->time_loss_model->getTimeLosses($where)->row_array();
	   		$last_entry_date = $last_entry['date_np'];
   		}else{
   			$last_entry_date = $date;
   		}
   		$dates = explode('-',$last_entry_date);

   		$timeloss_types = $this->tL_type_model->getTlTypes()->result_array();
	   		foreach($timeloss_types as $type){
	   			$where = NULL;
	   			if($shift != NULL){
	   				$where['shift'] = $shift;
	   			}
	   				   				$index = 'daily_loss';
	   				$this->db->like('date_np',$dates['0'].'-'.$dates['1'].'-'.$dates['2']);
	   			$where['department_id'] = $department_id;

		   		$this->time_loss_model->joins = array('TL_REASON_FULL','TL_TYPES','TL_REASONINGS');
	   			$this->db->group_by('reason');
		   		$where['tl_reason_full.type_id'] = $type['type_id'];
		   		$reason_total = $this->time_loss_model->getTimeLosseReports($where)->result_array();
		   		foreach ($reason_total as $reason){
		   			$data[$index][$type['loss_type']][$reason['reason_id']] = $reason;
		   		}
	   				$index = 'daily_loss';
	   				$this->db->like('date_np',$dates['0'].'-'.$dates['1'].'-'.$dates['2']);
	   			$data[$index][$type['loss_type']]['total'] = $this->time_loss_model->getTimeLosseReports($where)->row_array();
	   		}
   		return $data['daily_loss'];
    }
    private function timeloss_all_pdf($date,$shift = NULL)
    {
    	// echo $date;
    	$this->time_loss_model->joins = array('TL_REASON_FULL','TL_TYPES','TL_REASONINGS');
    	$department_id = $this->session->userdata('department_id');
    	$timeloss_types = $this->tL_type_model->getTlTypes()->result_array();
    	foreach($timeloss_types as $type){
	    	$where = array('date_np'=>$date,'department_id'=>$department_id,'loss_type'=>$type['loss_type']);
	    	if($shift != NULL){
				$where['shift'] = $shift;
			}
   			$data[$type['loss_type']] = $this->time_loss_model->getTimeLosses($where)->result_array();
   		}
   		return $data;
    }
    private function getShiftReports($date=NULL)
    {
    	if($date == NULL){
    		$this->db->order_by('date-np','desc');
    		$last_entry = $this->laminating_shift_report_model->getLaminatingShiftReports()->row_array();
    		$date = $last_entry['date-np'];
    	}
    	$shifts['total'] = array();
    	$this->laminating_shift_report_model->joins = array('LAMINATING_BRAND_COLOR','BRAND');
    	$where['date-np'] = $date;
    	$where['shift'] = 'A';
    	$shifts['A'] = $this->laminating_shift_report_model->getLaminatingShiftReports($where)->result_array();
		$dates = explode('-', $date);
		foreach ($dates as $key => $value) {
			if($key == 0){
				$index = 'yearly';
				$where = $this->getTimeWhere($date,$key,'date-np');
			}else if($key == 1){
				$index = 'monthly';
				$where = $this->getTimeWhere($date,$key,'date-np');
			}else{
				$index = 'daily';
				$where = $this->getTimeWhere($date,$key,'date-np');
			}
			$where['shift'] = 'A';
			$shifts['total']['A'][$index] = $this->laminating_shift_report_model->getAllReport($where)->row_array();
		}
    	$where['shift'] = 'B';
    	$shifts['B'] = $this->laminating_shift_report_model->getLaminatingShiftReports($where)->result_array();
    	foreach ($dates as $key => $value) {
			if($key == 0){
				$index = 'yearly';
				$where = $this->getTimeWhere($date,$key,'date-np');
			}else if($key == 1){
				$index = 'monthly';
				$where = $this->getTimeWhere($date,$key,'date-np');
			}else{
				$index = 'daily';
				$where = $this->getTimeWhere($date,$key,'date-np');
			}
			$where['shift'] = 'B';
			$shifts['total']['B'][$index] = $this->laminating_shift_report_model->getAllReport($where)->row_array();
		}
    	return $shifts;
    }
    // for printing pdf report
    private function getPrintingPdf($date)
    {
    	$this->print_shift_report_model->joins = array('MATERIAL_PROPERTY','DIMENSION','COLOR','DESIGNS');
    	$where['date_np'] = $date;
    	$report['shift_reports'] = $this->print_shift_report_model->getPrintShiftReports($where)->result_array();

    	$dates = explode('-', $date);
    	foreach ($dates as $key => $value) {
    		$date_where = $this->getTimeWhere($date,$key,'date_np');
    		$index = ($key == 0)?'This Year':(($key == 1)?'This Month':'Today');
    		$report['total'][$index] = $this->print_shift_report_model->getAllReport($date_where)->row_array();
    	}
    	return $report;
    }
    // for rexin pdf report
    private function getRexinPdf($date = NULL)
    {
    	$this->db->where('date_np',$date);
    	$data['reports'] = $this->rexin_coating_report_model->getRexinCoatingReports()->result_array();
		$i = 0;
		foreach($data['reports'] as $report){
			$this->rexin_product_model->joins = array('COLOR','EMBOSS','RELEASE_PAPER','FABRIC','THICKNESS','BRAND');
			$where['rp_id'] = $report['rp_id'];
			$data['reports'][$i]['material'] = $this->rexin_product_model->getRexinProducts($where)->row_array();
			$i++;
		}
		$dates = explode('-', $date);
		foreach ($dates as $key => $value) {
			$where = $this->getTimeWhere($date,$key,'date_np');
			if($key == 0){
				$index = 'yearly';
			}elseif($key == 1){
				$index = 'monthly';
			}else{
				$index = 'daily';
			}
			$data[$index] = $this->rexin_coating_report_model->getAllReport($where)->row_array();
		}
		return $data;
    }
    public function getLcReport(){
    	$date = $this->input->post('date');
    	$this->load->module_model('lc','lc_model');
    	$this->load->module_model('lc_material','lc_material_model');
    	$this->load->module_model('cost_sheet','cost_sheet_model');
    	$this->load->module_model('cost_material','cost_material_model');

    	if($this->input->post('month') && $this->input->post('month') != '00'){
    		$dates = explode('-',$date);
			$month =  $this->input->post('month');
			if($month > 3 ){
	    		$where = $this->getTimeWhere($dates['0'].'-0'.$month.'-00',1,'cost_sheet.date_np');
			}else{
	    		$where = $this->getTimeWhere(($dates['0']+1).'-0'.$month.'-00',1,'cost_sheet.date_np');
			}
    	}else{
    		$where = $this->getTimeWhere($date,0,'cost_sheet.date_np');
    	}

    	$data = $this->get_lc_report($date,$where);
    	$data['date'] = $date;
    	$data['month'] = $this->input->post('month');
   		
   		$this->load->view('home/lc/index',$data);
    }
    private function get_lc_report($date,$where)
    {
    	$this->lc_model->joins = array('COST_SHEET');
    	$this->db->order_by('pp_no','desc');
   		$data['rows'] = $this->lc_model->getLcs($where)->result_array();
   		foreach ($data['rows'] as $key => $value) {
	   		$count = $this->cost_sheet_model->count(array('lc_id'=>$value['id']));
	   		if($count){
	   				$total_cost = 0;
	   				$total_quantity = 0;
		   			$this->cost_material_model->joins = array('MATERIALS');
			    	$where = array();
		   			$data['material'][$key] = $this->cost_material_model->getCostMaterials(array('cost_id'=>$value['cost_id']))->result_array();
		   			foreach($data['material'][$key] as $material){
		   				$total = $material['quantity'] * $material['price'] * $value['currency_exchange_rate'];
		   				$total_cost += $total;
		   				$total_quantity += $material['quantity'];
		   			}
		   			$data['rows'][$key]['bill_amt'] = $total_cost;
		   			$data['rows'][$key]['total_charge'] = $value['bank_charge'] + $value['insurance'] + $value['transport_charge'] + $value['local_transport'] + $value['custom_fee'] +$value['agency_fee'] + $value['fee_fine'] +$value['dry_port_fee'] + $value['csf'] + $value['addition_fee'];
		   			// echo $data['rows'][$key]['total_charge'];
		   			$data['rows'][$key]['quantity'] = $total_quantity;
		   	}else{
		   		$data['material'][$key] = array();
		   	}
   		}
   		$data['report_materials'] = array();
   		foreach ($data['rows'] as $i => $row) {
            foreach ($data['material'][$i] as $key => $value) {
            	$total = $row['total_charge']/$row['bill_amt'] * $value['quantity'] * $value['price'] * $row['currency_exchange_rate'];
            	$tt = $total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise'];
            	$avg = ($total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise']) / $value['quantity'];


            	if(!array_key_exists($value['material_id'],$data['report_materials'])){
            		$data['report_materials'][$value['material_id']]['landed_cost'] = $tt;
            		$data['report_materials'][$value['material_id']]['avg_cost'] = $avg;
            		$data['report_materials'][$value['material_id']]['count'] = 1;
            		$data['report_materials'][$value['material_id']]['material_name'] = $value['material_name'];
            		$data['report_materials'][$value['material_id']]['highest'] = $avg;
            		$data['report_materials'][$value['material_id']]['lowest'] = $avg;
            		$data['report_materials'][$value['material_id']]['last'] = $avg;
            		$data['report_materials'][$value['material_id']]['last_date'] = $row['cost_date_np'];

            	}else{
            		$data['report_materials'][$value['material_id']]['landed_cost'] += $tt;
            		$data['report_materials'][$value['material_id']]['avg_cost'] += $avg;
            		$data['report_materials'][$value['material_id']]['count']++;
            		if($data['report_materials'][$value['material_id']]['highest'] < $avg){
            			$data['report_materials'][$value['material_id']]['highest'] = $avg;
            		}
            		if($data['report_materials'][$value['material_id']]['lowest'] > $avg){
            			$data['report_materials'][$value['material_id']]['lowest'] = $avg;
            		}
            		if($row['cost_date_np'] > $data['report_materials'][$value['material_id']]['last_date']){
	            		$data['report_materials'][$value['material_id']]['last'] = $avg;
	            		$data['report_materials'][$value['material_id']]['last_date'] = $row['cost_date_np'];
            		}
            	}
            }
        }
        return $data;
    }
    public function pdfLc($date,$month = NULL)
    {
    	
    	$this->load->module_model('lc','lc_model');
    	$this->load->module_model('lc_material','lc_material_model');
    	$this->load->module_model('cost_sheet','cost_sheet_model');
    	$this->load->module_model('cost_material','cost_material_model');

    	if($month && $month != '00'){
    		$dates = explode('-',$date);
			if($month > 3 ){
	    		$where = $this->getTimeWhere($dates['0'].'-0'.$month.'-00',1,'cost_sheet.date_np');
			}else{
	    		$where = $this->getTimeWhere(($dates['0']+1).'-0'.$month.'-00',1,'cost_sheet.date_np');
			}
    	}else{
    		$where = $this->getTimeWhere($date,0,'cost_sheet.date_np');
    	}
    	$header1 = array('PP no', 'Materila Name', 'quantity', 'Per Unit Cost', 'Price', 'Price NRP', 'Total Landed Cost', 'Per KG Cost');
    	$length1 = array(15,45,20,20,25,25,25,20);
    	$header2 = array('Material Name', 'Toatal Landed Cost', 'Average Per KG Price', 'Higest Per KG Cost', 'Lowest Per KG Cost', 'Last Per KG Cost');
    	$length2 = array(45,30,30,30,30,30,30);
    	$fields = array('material_name','landed_cost','avg_cost','highest','lowest','last');
    	$data = $this->get_lc_report($date,$where);
   		////////////////////////////////// pdf //////////////////
   		$filename = "LC_Report.pdf";
			// Send headers
	header("Content-Type: application/pdf");
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header('Content-Disposition: attachment; filename="'.'"');
	header("Content-Transfer-Encoding: binary ");

   			$pdf = new FPDF();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',20);
			$pdf->Cell(80);
		    // Title
		    $pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.',0,0,'C');
			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();//new line
// 
			$pdf->Cell(80);
			$pdf->Cell(30,10,'LC Report',0,0,'C');
			$pdf->SetFont('Arial','B',8);
			$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			$pdf->Ln();
			$pdf->cell(40,10, 'LC Report');
			$pdf->Ln();
			foreach ($header1 as $key => $value) {
				$pdf->Cell($length1[$key],6, $value,1);
			}
			$pdf->ln();
			foreach ($data['rows'] as $i => $row) {
				foreach ($data['material'][$i] as $key => $value) {
					$pdf->Cell($length1[0],6,$row['pp_no'],1);
					$pdf->Cell($length1[1],6,$value['material_name'],1);
					$pdf->Cell($length1[2],6,number_format($value['quantity'],3),1);
					$pdf->Cell($length1[3],6,number_format($value['price'],3),1);
					$pdf->Cell($length1[4],6,number_format($value['quantity'] * $value['price'],3),1);
					$pdf->Cell($length1[5],6,number_format($value['quantity'] * $value['price'] * $row['currency_exchange_rate'],3),1);
					$total = $row['total_charge']/$row['bill_amt'] * $value['quantity'] * $value['price'] * $row['currency_exchange_rate'];
					$pdf->Cell($length1[6],6,number_format($total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise'],3),1);
					$pdf->Cell($length1[7],6,number_format(($total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise']) / $value['quantity'],3),1);
					$pdf->ln();
				}
			}
			$pdf->ln();
			$pdf->cell(40,10, 'Mateial Cost Report');
			$pdf->Ln();
			foreach ($header2 as $key => $value) {
				$pdf->Cell($length2[$key],6, $value,1);
			}
					$pdf->ln();
			foreach ($data['report_materials'] as $key => $value) {
				foreach ($fields as $i => $field) {
					if($i == 0){
						$pdf->Cell($length2[$i],6,$value[$field],1);
					}else{
						$pdf->Cell($length2[$i],6,number_format($value[$field],3),1);
					}
				}
					$pdf->ln();
			}



   		$pdf->Output($filename,'D');//download as pdf
    }
}

/* End of file welcome.php */
/* Location: ./modules/welcome/controllers/welcome.php */