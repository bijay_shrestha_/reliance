<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/*
 * Rename the file to Customer.php
 * and Define Module Library Function (if any)
 */

class Custom_database {
	
    public $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->dbforge();
        $this->CI->load->dbutil();
    }

    public function getDatabaseArray(){
        $dbs = $this->CI->dbutil->list_databases();

        $i = 1;
        foreach ($dbs as $db)
        {
            echo $i++ . ') ' . $db . '<br>';
        }

        return $dbs;
    }

    public function create_db(){
        $database_array = $this->getDatabaseArray();
        if(!in_array('my_test_db',$database_array)){
            if ($this->CI->dbforge->create_database('my_test_db'))
            {
                echo 'Database created!';
            }
        }else{
            echo 'Database exists';
        }
    }
}