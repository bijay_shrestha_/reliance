<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('diesel_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="diesel-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('department_id')?></label>:</td>
<td><input type="text" name="search[department_id]" id="search_department_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('amount')?></label>:</td>
<td><input type="text" name="search[amount]" id="search_amount"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('unit')?></label>:</td>
<td><input type="text" name="search[unit]" id="search_unit"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('hour')?></label>:</td>
<td><input type="text" name="search[hour]" id="search_hour"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('min')?></label>:</td>
<td><input type="text" name="search[min]" id="search_min"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="diesel-table" data-options="pagination:true,title:'<?php  echo lang('diesel')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'department_id',sortable:true" width="50"><?php echo lang('department_id')?></th>
<th data-options="field:'amount',sortable:true" width="50"><?php echo lang('amount')?></th>
<th data-options="field:'unit',sortable:true" width="50"><?php echo lang('unit')?></th>
<th data-options="field:'hour',sortable:true" width="50"><?php echo lang('hour')?></th>
<th data-options="field:'min',sortable:true" width="50"><?php echo lang('min')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_diesel')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_diesel')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit diesel form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-diesel" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('department_id')?>:</label></td>
					  <td width="66%"><input name="department_id" id="department_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('amount')?>:</label></td>
					  <td width="66%"><input name="amount" id="amount" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('unit')?>:</label></td>
					  <td width="66%"><input name="unit" id="unit" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('hour')?>:</label></td>
					  <td width="66%"><input name="hour" id="hour" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('min')?>:</label></td>
					  <td width="66%"><input name="min" id="min" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#diesel-search-form').form('clear');
			$('#diesel-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#diesel-table').datagrid({
				queryParams:{data:$('#diesel-search-form').serialize()}
				});
		});		
		$('#diesel-table').datagrid({
			url:'<?php  echo site_url('diesel/admin/diesel/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_diesel')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removediesel('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_diesel')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-diesel').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_diesel')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#diesel-table').datagrid('getRows')[index];
		if (row){
			$('#form-diesel').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_diesel')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removediesel(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#diesel-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('diesel/admin/diesel/delete_json')?>', {id:[row.id]}, function(){
					$('#diesel-table').datagrid('deleteRow', index);
					$('#diesel-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#diesel-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('diesel/admin/diesel/delete_json')?>',{id:selected},function(data){
						$('#diesel-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-diesel').form('submit',{
			url: '<?php  echo site_url('diesel/admin/diesel/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-diesel').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#diesel-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>