<?php

class Diesel extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('diesel','diesel_model');
        $this->load->module_model('department_report','department_report_model');
        $this->lang->module_load('diesel','diesel');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->diesel_model->joins = array('DEPARTMENTS');
		$data['rows'] = $this->diesel_model->getDiesels()->result_array();
		$this->department_report_model->joins = array('DEPARTMENTS');
		$data['departments'] = $this->department_report_model->getDepartmentReports(array('report_id'=>2))->result_array();
		// echo '<pre>';print_r($data);
		// Display Page
		$data['header'] = 'Diesel Consumption';
		$data['view_page'] = "diesel/index";
		$data['module'] = 'diesel';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->diesel_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->diesel_model->getDiesels()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['amount']!='')?$this->db->like('amount',$params['search']['amount']):'';
($params['search']['unit']!='')?$this->db->like('unit',$params['search']['unit']):'';
($params['search']['hour']!='')?$this->db->where('hour',$params['search']['hour']):'';
($params['search']['min']!='')?$this->db->where('min',$params['search']['min']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->diesel_model->getDiesels()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->diesel_model->delete('DIESEL',array('id'=>$row));
            endforeach;
		}elseif($id){
			$success = $this->diesel_model->delete('DIESEL',array('id'=>$id));
			if($success){
				echo json_encode(array('success'=>$success, 'msg'=>'Record deleted'));
			}else{
				echo json_encode(array('success'=>$success, 'msg'=>'Record is not deleted. Please try again.'));
			}
		}
	}    

	public function save()
	{
		// echo '<pre>';
        $data=$this->_get_posted_data(); //Retrive Posted Data		
		// print_r($this->input->post());
		// print_r($data);
		// exit;

        if(!$this->input->post('id'))
        {
            $success=$this->diesel_model->insert('DIESEL',$data);
        }
        else
        {
            $success=$this->diesel_model->update('DIESEL',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
        $data['date_np'] = $this->input->post('date_np');
        $data['date_en'] = $this->input->post('date_en');
$data['department_id'] = $this->input->post('department_id');
$data['amount'] = $this->input->post('amount');
$data['unit'] = $this->input->post('unit');
$data['hour'] = $this->input->post('hour');
$data['min'] = $this->input->post('min');

        return $data;
   }
   // for edit form
   public function getValue(){
   		$where['id'] = $this->input->post('id');
   		$data = $this->diesel_model->getDiesels($where)->row_array();
   		echo json_encode($data);
   }
   	
	    
}