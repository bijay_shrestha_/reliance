<?php

class Master_chemical extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('master_chemical','master_chemical_model');
        $this->lang->module_load('master_chemical','master_chemical');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'master_chemical';
		$data['page'] = $this->config->item('template_admin') . "master_chemical/index";
		$data['module'] = 'master_chemical';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->master_chemical_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->master_chemical_model->getMasterChemicals()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
($params['search']['rank']!='')?$this->db->where('rank',$params['search']['rank']):'';
($params['search']['code']!='')?$this->db->like('code',$params['search']['code']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->master_chemical_model->getMasterChemicals()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->master_chemical_model->delete('MASTER_CHEMICALS',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->master_chemical_model->insert('MASTER_CHEMICALS',$data);
        }
        else
        {
            $success=$this->master_chemical_model->update('MASTER_CHEMICALS',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['name'] = $this->input->post('name');
$data['rank'] = $this->input->post('rank');
$data['code'] = $this->input->post('code');

        return $data;
   }
   
   	
	    
}