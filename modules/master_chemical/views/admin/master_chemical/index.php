<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('master_chemical_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
			<form action="" method="post" id="master_chemical-search-form">
				<table width="100%"  cellspacing="1" cellpadding="1">
					<tr><td><label><?php echo lang('name')?></label>:</td>
						<td><input type="text" name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
						<td><label><?php echo lang('rank')?></label>:</td>
						<td><input type="text" name="search[rank]" id="search_rank"  class="easyui-numberbox"/></td>
					</tr>
					<tr>
						<td><label><?php echo lang('code')?></label>:</td>
						<td><input type="text" name="search[code]" id="search_code"  class="easyui-validatebox"/></td>
					</tr>
					<tr>
						<td colspan="4">
							<a href="javascript:void(0)" class="bnt btn-xs btn-flat btn-success" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
							<a href="javascript:void(0)" class="bnt btn-xs btn-flat btn-warning" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
						</td>
					</tr>
				</table>

			</form>
		</div>
		<br/>
		<table id="master_chemical-table" data-options="pagination:true,title:'<?php  echo lang('master_chemical')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
			<thead>
				<th data-options="field:'checkbox',checkbox:true"></th>
				<th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
				<th data-options="field:'name',sortable:true" width="50"><?php echo lang('name')?></th>
				<th data-options="field:'code',sortable:true" width="50"><?php echo lang('code')?></th>
				<th data-options="field:'rank',sortable:true" width="50"><?php echo lang('rank')?></th>

				<th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
			</thead>
		</table>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<a href="#" class="bnt btn-xs btn-flat btn-success" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_master_chemical')?>"><?php  echo lang('create')?></a>
				<a href="#" class="bnt btn-xs btn-flat btn-danger" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_master_chemical')?>"><?php  echo lang('remove_selected')?></a>
			</p>

		</div> 

		<!--for create and edit master_chemical form-->
		<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
		data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
		<form id="form-master_chemical" method="post" >
			<table>
				<tr>
					<td width="34%" ><label><?php echo lang('name')?>:</label></td>
					<td width="66%"><input name="name" id="name" class="easyui-validatebox" required="true"></td>
				</tr><tr>
					<td width="34%" ><label><?php echo lang('rank')?>:</label></td>
					<td width="66%"><input name="rank" id="rank" class="easyui-numberbox" required="true"></td>
				</tr><tr>
					<td width="34%" ><label><?php echo lang('code')?>:</label></td>
					<td width="66%"><input name="code" id="code" class="easyui-validatebox" required="true"></td>
				</tr><input type="hidden" name="id" id="id"/>
			</table>
		</form>
		<div id="dlg-buttons">
			<a href="#" class="bnt btn-xs btn-flat btn-success" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
			<a href="#" class="bnt btn-xs btn-flat btn-danger" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
		</div>    
	</div>
	<!--div ends-->
	
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#master_chemical-search-form').form('clear');
			$('#master_chemical-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#master_chemical-table').datagrid({
				queryParams:{data:$('#master_chemical-search-form').serialize()}
			});
		});		
		$('#master_chemical-table').datagrid({
			url:'<?php  echo site_url('master_chemical/admin/master_chemical/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-xs btn-flat btn-primary" iconcls="icon-edit"  title="<?php  echo lang('edit_master_chemical')?>"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a>';
		var d = '<a href="#" onclick="removemaster_chemical('+index+')" class="btn btn-xs btn-flat btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_master_chemical')?>"><span><i class="fa fa-times" aria-hidden="true"></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-master_chemical').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_master_chemical')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#master_chemical-table').datagrid('getRows')[index];
		if (row){
			$('#form-master_chemical').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_master_chemical')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
	
	function removemaster_chemical(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#master_chemical-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('master_chemical/admin/master_chemical/delete_json')?>', {id:[row.id]}, function(){
					$('#master_chemical-table').datagrid('deleteRow', index);
					$('#master_chemical-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#master_chemical-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('master_chemical/admin/master_chemical/delete_json')?>',{id:selected},function(data){
						$('#master_chemical-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-master_chemical').form('submit',{
			url: '<?php  echo site_url('master_chemical/admin/master_chemical/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-master_chemical').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#master_chemical-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
			
		});		
		
	}
	
	
</script>