<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('report_order_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="report_order-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('report_id')?></label>:</td>
<td><input type="text" name="search[report_id]" id="search_report_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('category_id')?></label>:</td>
<td><input type="text" name="search[category_id]" id="search_category_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('report_order')?></label>:</td>
<td><input type="text" name="search[report_order]" id="search_report_order"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="report_order-table" data-options="pagination:true,title:'<?php  echo lang('report_order')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'ro_id',sortable:true" width="30"><?php echo lang('ro_id')?></th>
<th data-options="field:'report_id',sortable:true" width="50"><?php echo lang('report_id')?></th>
<th data-options="field:'category_id',sortable:true" width="50"><?php echo lang('category_id')?></th>
<th data-options="field:'report_order',sortable:true" width="50"><?php echo lang('report_order')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_report_order')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_report_order')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit report_order form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-report_order" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('report_id')?>:</label></td>
					  <td width="66%"><input name="report_id" id="report_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('category_id')?>:</label></td>
					  <td width="66%"><input name="category_id" id="category_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('report_order')?>:</label></td>
					  <td width="66%"><input name="report_order" id="report_order" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="ro_id" id="ro_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#report_order-search-form').form('clear');
			$('#report_order-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#report_order-table').datagrid({
				queryParams:{data:$('#report_order-search-form').serialize()}
				});
		});		
		$('#report_order-table').datagrid({
			url:'<?php  echo site_url('report_order/admin/report_order/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_report_order')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removereport_order('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_report_order')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-report_order').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_report_order')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#report_order-table').datagrid('getRows')[index];
		if (row){
			$('#form-report_order').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_report_order')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removereport_order(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#report_order-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('report_order/admin/report_order/delete_json')?>', {id:[row.ro_id]}, function(){
					$('#report_order-table').datagrid('deleteRow', index);
					$('#report_order-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#report_order-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].ro_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('report_order/admin/report_order/delete_json')?>',{id:selected},function(data){
						$('#report_order-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-report_order').form('submit',{
			url: '<?php  echo site_url('report_order/admin/report_order/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-report_order').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#report_order-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>