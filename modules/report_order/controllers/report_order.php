<?php

class Report_order extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('report_order','report_order_model');
        $this->lang->module_load('report_order','report_order');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'report_order';
		$data['page'] = $this->config->item('template_admin') . "report_order/index";
		$data['module'] = 'report_order';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->report_order_model->count();
		paging('ro_id');
		$this->_get_search_param();	
		$rows=$this->report_order_model->getReportOrders()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['report_id']!='')?$this->db->where('report_id',$params['search']['report_id']):'';
($params['search']['category_id']!='')?$this->db->where('category_id',$params['search']['category_id']):'';
($params['search']['report_order']!='')?$this->db->where('report_order',$params['search']['report_order']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->report_order_model->getReportOrders()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->report_order_model->delete('REPORT_ORDER',array('ro_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('ro_id'))
        {
            $success=$this->report_order_model->insert('REPORT_ORDER',$data);
        }
        else
        {
            $success=$this->report_order_model->update('REPORT_ORDER',$data,array('ro_id'=>$data['ro_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['ro_id'] = $this->input->post('ro_id');
$data['report_id'] = $this->input->post('report_id');
$data['category_id'] = $this->input->post('category_id');
$data['report_order'] = $this->input->post('report_order');

        return $data;
   }

   public function up(){
   	
   		$where['report_id'] = $this->input->post('report_id');
   		$where['report_orders.category_id'] = $this->input->post('id');
   		$report_order = $this->report_order_model->getReportOrders($where)->row_array();
   		$order = $report_order['report_order'];
   		$new_order = $order - 1;
   		$report_order1 = $this->report_order_model->getReportOrders(array('report_id'=>$where["report_id"], 'report_order'=>$new_order))->row_array();
		if(count($report_order1)>0){
   		$this->report_order_model->update('REPORT_ORDER',array('report_order'=>$new_order),array('ro_id'=>$report_order['ro_id']));
   		$this->report_order_model->update('REPORT_ORDER',array('report_order'=>$order),array('ro_id'=>$report_order1['ro_id']));
		}

   		$where['report_id'] = $this->input->post('report_id');
   		$this->db->order_by('report_order');
   		$this->report_order_model->joins = array('CATEGORY');
   		$data['categories'] = $this->report_order_model->getReportOrders(array('report_id'=>$where['report_id']))->result_array();
   		// print_r($this->db->last_query());
   		// print_r($data);
   		$this->load->view('report_order/index',$data);
   }
    public function down(){
   	
   		$where['report_id'] = $this->input->post('report_id');
   		$where['report_orders.category_id'] = $this->input->post('id');
   		$report_order = $this->report_order_model->getReportOrders($where)->row_array();
   		$order = $report_order['report_order'];
   		$new_order = $order + 1;
   		$report_order1 = $this->report_order_model->getReportOrders(array('report_id'=>$where["report_id"], 'report_order'=>$new_order))->row_array();

   		if(count($report_order1)>0){
   		$this->report_order_model->update('REPORT_ORDER',array('report_order'=>$new_order),array('ro_id'=>$report_order['ro_id']));
   		$this->report_order_model->update('REPORT_ORDER',array('report_order'=>$order),array('ro_id'=>$report_order1['ro_id']));
   		}

   		$where['report_id'] = $this->input->post('report_id');
   		$this->db->order_by('report_order');
   		$this->report_order_model->joins = array('CATEGORY');
   		$data['categories'] = $this->report_order_model->getReportOrders(array('report_id'=>$where['report_id']))->result_array();
   		$this->load->view('report_order/index',$data);
   }
   
   	
	    
}