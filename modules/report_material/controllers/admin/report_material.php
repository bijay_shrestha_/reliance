<?php

class Report_material extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('report_material','report_material_model');
        $this->lang->module_load('report_material','report_material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'report_material';
		$data['page'] = $this->config->item('template_admin') . "report_material/index";
		$data['module'] = 'report_material';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->report_material_model->count();
		paging('rm_id');
		$this->_get_search_param();	
		$rows=$this->report_material_model->getReportMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['report_type_id']!='')?$this->db->where('report_type_id',$params['search']['report_type_id']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->report_material_model->getReportMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->report_material_model->delete('REPORT_MATERIAL',array('rm_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('rm_id'))
        {
            $success=$this->report_material_model->insert('REPORT_MATERIAL',$data);
        }
        else
        {
            $success=$this->report_material_model->update('REPORT_MATERIAL',$data,array('rm_id'=>$data['rm_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['rm_id'] = $this->input->post('rm_id');
$data['report_type_id'] = $this->input->post('report_type_id');
$data['material_id'] = $this->input->post('material_id');

        return $data;
   }
   
   	
	    
}