<?php

class Report_material extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('report_material','report_material_model');
        $this->load->module_model('category','category_model');
        $this->load->module_model('report_order','report_order_model');

        $this->lang->module_load('report_material','report_material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'report_material';
		$data['page'] = $this->config->item('template_admin') . "report_material/index";
		$data['module'] = 'report_material';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->report_material_model->count();
		paging('rm_id');
		$this->_get_search_param();	
		$rows=$this->report_material_model->getReportMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['report_type_id']!='')?$this->db->where('report_type_id',$params['search']['report_type_id']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->report_material_model->getReportMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->report_material_model->delete('REPORT_MATERIAL',array('rm_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('rm_id'))
        {
            $success=$this->report_material_model->insert('REPORT_MATERIAL',$data);
        }
        else
        {
            $success=$this->report_material_model->update('REPORT_MATERIAL',$data,array('rm_id'=>$data['rm_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['rm_id'] = $this->input->post('rm_id');
$data['report_type_id'] = $this->input->post('report_type_id');
$data['material_id'] = $this->input->post('material_id');

        return $data;
   }
   //material for form
    public function getformmaterial() {
       // $data['metrials'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
       $id = $this->input->post('id');
       $data['result'] = $this->report_material_model->getReportMaterials(array('report_type_id'=>$id))->result_array();
       echo json_encode($data);
   }
   //save report material
   public function saverm(){
   		$data['report_type_id'] = $this->input->post('type_id');
   		$msg = 'Select Department';
		$success = FALSE;
   		if($data['report_type_id'] == ''){
   			$msg = 'Select Department';
			$success = FALSE;
   		}else{
   			if($this->input->post('material')){
   				$material_ids = $this->input->post('material');
   				$this->report_material_model->delete('REPORT_MATERIAL',(array('report_type_id'=>$data['report_type_id'])));
   				foreach($material_ids as $material_id){
   					$data['material_id'] = $material_id;
   					$success = $this->report_material_model->insert('REPORT_MATERIAL',$data);
   				}

   				$where['report_type_id'] = $this->input->post('type_id');
		   		$this->db->group_by('category_id');
		   		$this->report_material_model->joins = array('MATERIALS');
		   		$categories = $this->report_material_model->getReportMaterials($where)->result_array();
		   		$this->db->order_by('report_order','desc');
		   		$last_report_category = $this->report_order_model->getReportOrders(array('report_id' => $data['report_type_id']))->row_array();
		   		if(count($last_report_category) == 0){
		   			$i=1;
		   		}else{
		   			$i = $last_report_category['report_order'] +1;
		   		}
		   		// echo $i;
		   		foreach($categories as $category){
		   			$count = $this->report_order_model->count(array('report_id' => $data['report_type_id'],'category_id' => $category['category_id']));
		   			if($count == 0){
			   			$this->report_order_model->insert('REPORT_ORDER',array('report_id' => $data['report_type_id'],'category_id' => $category['category_id'],'report_order'=>$i));
		   				$i++;
		   			}
		   		}

   				$msg = 'Successfully Added';
   			}else {
   				$msg = 'Select Material';
   				$success = FALSE;
   			}
   		}
   		echo json_encode(array('msg'=>$msg,'success'=>$success));
   	}
   	//for category of report
   	public function getCategory(){
   		// echo '<pre>';
   		$where['report_id'] = $this->input->post('id');
   		$this->db->order_by('report_order');
   		$this->report_order_model->joins = array('CATEGORY');
   		$data['categories'] = $this->report_order_model->getReportOrders($where)->result_array();
   		$this->load->view('report_order/index',$data);
   		// print_r($data);echo '</pre>';
   	}
   	
	    
}