<?php


$lang['id'] = 'Id';
$lang['date_of_sample'] = 'Date Of Sample';
$lang['date_of_testing'] = 'Date Of Testing';
$lang['moisture'] = 'Moisture';
$lang['ir'] = 'Ir';
$lang['so'] = 'So';
$lang['purity'] = 'Purity';
$lang['source_of_supplier'] = 'Source Of Supplier';
$lang['company'] = 'Company';
$lang['remarks'] = 'Remarks';

$lang['create_gypsum_record']='Create Gypsum Record';
$lang['edit_gypsum_record']='Edit Gypsum Record';
$lang['delete_gypsum_record']='Delete Gypsum Record';
$lang['gypsum_record_search']='Gypsum Record Search';

$lang['gypsum_record']='Gypsum Record';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

