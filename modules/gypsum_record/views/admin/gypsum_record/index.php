<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('gypsum_record_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="gypsum_record-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('date_of_sample')?></label>:</td>
<td><input type="text" name="date[date_of_sample][from]" id="search_date_of_sample_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_of_sample][to]" id="search_date_of_sample_to"  class="easyui-datebox"/></td>
<td><label><?php echo lang('date_of_testing')?></label>:</td>
<td><input type="text" name="date[date_of_testing][from]" id="search_date_of_testing_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_of_testing][to]" id="search_date_of_testing_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('moisture')?></label>:</td>
<td><input type="text" name="search[moisture]" id="search_moisture"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('ir')?></label>:</td>
<td><input type="text" name="search[ir]" id="search_ir"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('so')?></label>:</td>
<td><input type="text" name="search[so]" id="search_so"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('purity')?></label>:</td>
<td><input type="text" name="search[purity]" id="search_purity"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('source_of_supplier')?></label>:</td>
<td><input type="text" name="search[source_of_supplier]" id="search_source_of_supplier"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('company')?></label>:</td>
<td><input type="text" name="search[company]" id="search_company"  class="easyui-numberbox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="gypsum_record-table" data-options="pagination:true,title:'<?php  echo lang('gypsum_record')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'date_of_sample',sortable:true" width="50"><?php echo lang('date_of_sample')?></th>
<th data-options="field:'date_of_testing',sortable:true" width="50"><?php echo lang('date_of_testing')?></th>
<th data-options="field:'moisture',sortable:true" width="50"><?php echo lang('moisture')?></th>
<th data-options="field:'ir',sortable:true" width="50"><?php echo lang('ir')?></th>
<th data-options="field:'so',sortable:true" width="50"><?php echo lang('so')?></th>
<th data-options="field:'purity',sortable:true" width="50"><?php echo lang('purity')?></th>
<th data-options="field:'source_of_supplier',sortable:true" width="50"><?php echo lang('source_of_supplier')?></th>
<th data-options="field:'company',sortable:true" width="50"><?php echo lang('company')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_gypsum_record')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_gypsum_record')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit gypsum_record form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-gypsum_record" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('date_of_sample')?>:</label></td>
					  <td width="66%"><input name="date_of_sample" id="date_of_sample" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_of_testing')?>:</label></td>
					  <td width="66%"><input name="date_of_testing" id="date_of_testing" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('moisture')?>:</label></td>
					  <td width="66%"><input name="moisture" id="moisture" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('ir')?>:</label></td>
					  <td width="66%"><input name="ir" id="ir" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('so')?>:</label></td>
					  <td width="66%"><input name="so" id="so" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('purity')?>:</label></td>
					  <td width="66%"><input name="purity" id="purity" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('source_of_supplier')?>:</label></td>
					  <td width="66%"><input name="source_of_supplier" id="source_of_supplier" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('company')?>:</label></td>
					  <td width="66%"><input name="company" id="company" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('remarks')?>:</label></td>
					  <td width="66%"><textarea name="remarks" id="remarks" class="easyui-validatebox" required="true" style="width:300px;height:100px"></textarea></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#gypsum_record-search-form').form('clear');
			$('#gypsum_record-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#gypsum_record-table').datagrid({
				queryParams:{data:$('#gypsum_record-search-form').serialize()}
				});
		});		
		$('#gypsum_record-table').datagrid({
			url:'<?php  echo site_url('gypsum_record/admin/gypsum_record/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_gypsum_record')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removegypsum_record('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_gypsum_record')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-gypsum_record').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_gypsum_record')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#gypsum_record-table').datagrid('getRows')[index];
		if (row){
			$('#form-gypsum_record').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_gypsum_record')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removegypsum_record(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#gypsum_record-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('gypsum_record/admin/gypsum_record/delete_json')?>', {id:[row.id]}, function(){
					$('#gypsum_record-table').datagrid('deleteRow', index);
					$('#gypsum_record-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#gypsum_record-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('gypsum_record/admin/gypsum_record/delete_json')?>',{id:selected},function(data){
						$('#gypsum_record-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-gypsum_record').form('submit',{
			url: '<?php  echo site_url('gypsum_record/admin/gypsum_record/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-gypsum_record').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#gypsum_record-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>