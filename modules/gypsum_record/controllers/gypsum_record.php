<?php 
class Gypsum_record extends Member_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->module_model('gypsum_record','gypsum_record_model');
		$this->load->module_model('company','company_model');
		$this->load->module_model('supplier','supplier_model');
		$this->load->module_model('report_chemical_usage','report_chemical_usage_model');
		$this->load->module_model('gypsum_chemical_record','gypsum_chemical_record_model');

		$this->lang->module_load('gypsum_record','gypsum_record');
	}
	
	public function index($report_id = NULL)
	{
		$this->db->where('report_id',$report_id);
		$data['input_fields'] = $this->db->get('view_report_chemical_usage')->result_array();


		/*$data['rows'] = $this->db->get('view_gypsum_record')->result_array();*/
		$concat_length = "set session group_concat_max_len = 500000";
		$this->db->query($concat_length);
		$make_varialble = "SET @SQL = NULL;";
		$this->db->query($make_varialble);
		$concat = "SET @sql_dynamic = (
			SELECT
			GROUP_CONCAT(
				DISTINCT CONCAT(
					'SUM( IF(chemical_id = ',
					chemical_id,
					', amount,0) ) AS `',
					chemical_name,
					'`'
				)
			)
			FROM
			view_gypsum_chemical_record
		)";

		$this->db->query($concat);

		$set = "
		SET @SQL = CONCAT(
			'SELECT id, date_of_sample AS \"Sample Date \", date_of_testing AS \"Testing Date\", moisture as Moisture, purity AS Putiry, source_of_supplier AS Supplier, company AS Company, remarks AS Remarks, ',
			@sql_dynamic,
			'  FROM view_gypsum_chemical_record
			GROUP BY date_of_sample'
		)";

		$sql = $this->db->query($set);
		$sql = $this->db->query("SELECT @SQL")->row_array();
		
		$data['rows'] = $this->db->query($sql['@SQL'])->result_array();

		$data['company'] = $this->company_model->getCompanies()->result_array();
		$data['suppliers'] = $this->supplier_model->getSuppliers()->result_array();

		$data['header'] = 'Gypsum Record';
		$data['view_page'] = "gypsum_record/index";
		$data['module'] = 'gypsum_record';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->gypsum_record_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->gypsum_record_model->getGypsumRecords()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function save()
	{
		$post_data = $this->_get_post_data();

		$chemical_data = $this->input->post();
		foreach ($chemical_data as $key => $value) 
		{
			if(!is_numeric($key))
			{
				unset($chemical_data[$key]);
			}
		}
		foreach ($chemical_data as $key => $value) 
		{
			$chemical_record['chemical_id'] = $key;
			$chemical_record['amount'] = $value;
		}

		if(!$this->input->post('id'))
		{
			$success = $this->gypsum_record_model->insert('GYPSUM_RECORD',$post_data);
		}
		else
		{
			$success = $this->gypsum_record_model->update('GYPSUM_RECORD',$post_data,array('id'=>$post_data['id']));
		}

		if($success)
		{	
			$chemical_record['gypsum_record_id'] = $this->db->insert_id();
			$this->gypsum_chemical_record_model->insert('GYPSUM_CHEMICAL_RECORD',$chemical_record);
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
	}

	private function _get_post_data()
	{
		$data = array();
		if($this->input->post('id'))
		{
			$data['id'] = $this->input->post('id');
		}
		$data['date_of_sample'] = $this->input->post('sample_date_np');
		$data['date_of_testing'] = $this->input->post('testing_date_np');
		$data['company'] = $this->input->post('company_id');
		$data['moisture'] = $this->input->post('moisture');
		$data['purity'] = $this->input->post('purity');
		$data['source_of_supplier'] = $this->input->post('supplier_id');
		$data['remarks'] = $this->input->post('remarks');

		return $data; 
	}

	public function get_gypsum_records()
	{
		$id = $this->input->post('id');
		$this->db->where('id',$id);
		$rows = $this->db->get('view_gypsum_chemical_record')->row_array();
		echo json_encode($rows); 
	}

	public function delete_json()
	{
		$id = $this->input->post('id');
		$this->db->where('id',$id);
		$success = $this->db->delete('tbl_gypsum_record');
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}

		echo json_encode(array('msg'=>$msg,'success'=>$success));
	}
}































































?>