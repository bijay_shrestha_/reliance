<?php

class Gypsum_record extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->module_model('gypsum_record','gypsum_record_model');
		$this->lang->module_load('gypsum_record','gypsum_record');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}
	
	public function index()
	{
		// Display Page
		$data['header'] = 'gypsum_record';
		$data['page'] = $this->config->item('template_admin') . "gypsum_record/index";
		$data['module'] = 'gypsum_record';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->gypsum_record_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->gypsum_record_model->getGypsumRecords()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['moisture']!='')?$this->db->like('moisture',$params['search']['moisture']):'';
			($params['search']['ir']!='')?$this->db->like('ir',$params['search']['ir']):'';
			($params['search']['so']!='')?$this->db->like('so',$params['search']['so']):'';
			($params['search']['purity']!='')?$this->db->like('purity',$params['search']['purity']):'';
			($params['search']['source_of_supplier']!='')?$this->db->where('source_of_supplier',$params['search']['source_of_supplier']):'';
			($params['search']['company']!='')?$this->db->where('company',$params['search']['company']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		
		
	}

	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
				"' and '".date('Y-m-d',strtotime($to))."')");
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}	
	
	public function combo_json()
	{
		$rows=$this->gypsum_record_model->getGypsumRecords()->result_array();
		echo json_encode($rows);    	
	}    
	
	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->gypsum_record_model->delete('GYPSUM_RECORD',array('id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
        	$success=$this->gypsum_record_model->insert('GYPSUM_RECORD',$data);
        }
        else
        {
        	$success=$this->gypsum_record_model->update('GYPSUM_RECORD',$data,array('id'=>$data['id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }
        
        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }
    
    private function _get_posted_data()
    {
    	$data=array();
    	$data['id'] = $this->input->post('id');
    	$data['date_of_sample'] = $this->input->post('date_of_sample');
    	$data['date_of_testing'] = $this->input->post('date_of_testing');
    	$data['moisture'] = $this->input->post('moisture');
    	$data['ir'] = $this->input->post('ir');
    	$data['so'] = $this->input->post('so');
    	$data['purity'] = $this->input->post('purity');
    	$data['source_of_supplier'] = $this->input->post('source_of_supplier');
    	$data['company'] = $this->input->post('company');
    	$data['remarks'] = $this->input->post('remarks');

    	return $data;
    }
    
    
    
}