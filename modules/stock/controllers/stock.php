<?php

class Stock extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('stock','stock_model');
        $this->load->module_model('dept_request','dept_request_model');
        $this->load->module_model('consumption','consumption_model');
        $this->load->module_model('material_department','material_department_model');
        $this->lang->module_load('stock','stock');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// print_r($this->input->post());exit;
		$department_id = $this->session->userdata('department_id');
   		$this->material_department_model->joins = array('CATEGORY_DEPARTMENT','MATERIALS');
   		$this->db->order_by('order');
   		$data['materials'] = $this->material_department_model->getMaterialDepartments(array('group_id'=>$department_id))->result_array();
// echo '<pre>';print_r($data['materials']);exit;
		if($this->input->post('en-date')){
			$date = $this->input->post('en-date');
		}else{
			$date = date('Y-m-d');
		}
		//get opening stock
		$data['opening_stocks'] = $this->getOpeningStocks($date);
		//get recived item
		$data['recived_items'] = $this->getRecivedItem($date);
		//get consumed
		$data['consumptions'] = $this->getConsumed($date);

		// Display Page
		$data['header'] = 'Stock';
		$data['view_page'] = "stock/index";
		$data['module'] = 'stock';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->stock_model->count();
		paging('stock_id');
		$this->_get_search_param();	
		$rows=$this->stock_model->getStocks()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->like('material_id',$params['search']['material_id']):'';
($params['search']['amount']!='')?$this->db->where('amount',$params['search']['amount']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->stock_model->getStocks()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->stock_model->delete('STOCK',array('stock_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('stock_id'))
        {
            $success=$this->stock_model->insert('STOCK',$data);
        }
        else
        {
            $success=$this->stock_model->update('STOCK',$data,array('stock_id'=>$data['stock_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['stock_id'] = $this->input->post('stock_id');
$data['material_id'] = $this->input->post('material_id');
$data['amount'] = $this->input->post('amount');

        return $data;
   }
   
   	public function getOpeningStocks($date){
   		$department_id = $this->session->userdata('department_id');
   		$this->material_department_model->joins = array('CATEGORY_DEPARTMENT');
   		$this->db->order_by('order');
   		$materials = $this->material_department_model->getMaterialDepartments(array('group_id'=>$department_id))->result_array();
   		$result = array();
   		foreach($materials as $material){
	   		$this->db->group_by('material_id');
	   		$this->db->where('department_id',$department_id);
	   		$this->db->where('store_confirm_en_date <',$date);
	   		$this->db->where('material_id', $material['material_id']);
	   		$this->db->where('store_status',1);
        	$this->db->where('department_status',1);
	   		$recived_item = $this->dept_request_model->getAllDispatches()->row_array();

	   		$this->db->where('department_id',$department_id);
	   		$this->db->where('english_date <',$date);
	   		$this->db->where('material_id', $material['material_id']);
	   		$consumed_item = $this->consumption_model->getAllConsumptions()->row_array();
			
			if(array_key_exists('all_dispatched', $recived_item) && array_key_exists('item_amount', $consumed_item)){
	   			$result[$material['material_id']] = $recived_item['all_dispatched'] - $consumed_item['item_amount'];
			}else if(array_key_exists('all_dispatched', $recived_item)){
				$result[$material['material_id']] = $recived_item['all_dispatched'];
			}
   		}
   		return $result;
   	}

   	public function getRecivedItem($date){
   		$department_id = $this->session->userdata('department_id');
   		$this->material_department_model->joins = array('CATEGORY_DEPARTMENT');
   		$this->db->order_by('order');
   		$materials = $this->material_department_model->getMaterialDepartments(array('group_id'=>$department_id))->result_array();
   		$result = array();

   		foreach($materials as $material){
	   		$this->db->group_by('material_id');
	   		$this->db->where('department_id',$department_id);
	   		$this->db->where('store_confirm_en_date',$date);
	   		$this->db->where('material_id', $material['material_id']);
		 	$this->db->where('store_status',1);
	        $this->db->where('department_status',1);
	   		$item_recived = $this->dept_request_model->getAllDispatches()->row_array();

	   		if(array_key_exists('all_dispatched', $item_recived)){
	   			$result[$material['material_id']] = $item_recived['all_dispatched'];
	   		}
   		}
   		return $result;
   	}

   	public function getConsumed($date){
   		$department_id = $this->session->userdata('department_id');
   		$this->material_department_model->joins = array('CATEGORY_DEPARTMENT');
   		$this->db->order_by('order');
   		$materials = $this->material_department_model->getMaterialDepartments(array('group_id'=>$department_id))->result_array();
   		$result = array();

   		foreach($materials as $material){
	   		$this->db->group_by('material_id');
	   		$this->db->where('department_id',$department_id);
	   		$this->db->where('english_date',$date);
	   		$this->db->where('material_id',$material['material_id']);
	   		$consumed_item = $this->consumption_model->getAllConsumptions()->row_array();

	   		if(array_key_exists('item_amount', $consumed_item))
	   		$result[$material['material_id']] = $consumed_item['item_amount'];
   		}
   		return $result;
   	}
	    
}