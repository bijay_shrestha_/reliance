<?php

class Stock extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('stock','stock_model');
        $this->lang->module_load('stock','stock');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'stock';
		$data['page'] = $this->config->item('template_admin') . "stock/index";
		$data['module'] = 'stock';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->stock_model->count();
		paging('stock_id');
		$this->_get_search_param();	
		$rows=$this->stock_model->getStocks()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->like('material_id',$params['search']['material_id']):'';
($params['search']['amount']!='')?$this->db->where('amount',$params['search']['amount']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->stock_model->getStocks()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->stock_model->delete('STOCK',array('stock_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('stock_id'))
        {
            $success=$this->stock_model->insert('STOCK',$data);
        }
        else
        {
            $success=$this->stock_model->update('STOCK',$data,array('stock_id'=>$data['stock_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['stock_id'] = $this->input->post('stock_id');
$data['material_id'] = $this->input->post('material_id');
$data['amount'] = $this->input->post('amount');

        return $data;
   }
   
   	
	    
}