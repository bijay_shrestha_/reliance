<?php


$lang['id'] = 'Id';
$lang['material_id'] = 'Material Id';
$lang['cost_id'] = 'Cost Id';
$lang['quantity'] = 'Quantity';
$lang['duty'] = 'Duty';
$lang['excise'] = 'Excise';

$lang['create_cost_material']='Create Cost Material';
$lang['edit_cost_material']='Edit Cost Material';
$lang['delete_cost_material']='Delete Cost Material';
$lang['cost_material_search']='Cost Material Search';

$lang['cost_material']='Cost Material';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

