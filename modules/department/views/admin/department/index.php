<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('department_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
			<form action="" method="post" id="department-search-form">
				<table width="100%" cellspacing="1" cellpadding="1">
					<tr><td><label><?php echo lang('name')?></label>:</td>
						<td><input type="text" name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
					</tr>
					<tr>
						<td colspan="4">
							<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
							<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
						</td>
					</tr>
				</table>

			</form>
		</div>
		<br/>
		<table id="department-table" data-options="pagination:true,title:'<?php  echo lang('department')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
			<thead>
				<th data-options="field:'checkbox',checkbox:true"></th>
				<!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
				<th data-options="field:'name',sortable:true" width="79%"><?php echo lang('name')?></th>

				<th field="action" width="17%" formatter="getActions"><?php  echo lang('action')?></th>
			</thead>
		</table>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_department')?>"><?php  echo lang('create')?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_department')?>"><?php  echo lang('remove_selected')?></a>
			</p>

		</div> 

		<!--for create and edit department form-->
		<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
		data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
		<form id="form-department" method="post" >
			<table>
				<tr>
					<td width="34%" ><label><?php echo lang('name')?>:</label></td>
					<td width="66%"><input name="name" id="name" class="easyui-validatebox" required="true"></td>
				</tr><input type="hidden" name="id" id="id"/>
			</table>
		</form>
		<div id="dlg-buttons">
			<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
			<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
		</div>    
	</div>
	<!--div ends-->

</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#department-search-form').form('clear');
			$('#department-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#department-table').datagrid({
				queryParams:{data:$('#department-search-form').serialize()}
			});
		});		
		$('#department-table').datagrid({
			url:'<?php  echo site_url('department/admin/department/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning btn-sm"  title="<?php  echo lang('edit_department')?>">Edit</a>';
		var d = '<a href="#" onclick="removedepartment('+index+')" class="btn btn-danger btn-sm"  title="<?php  echo lang('delete_department')?>">Delete</a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-department').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_department')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#department-table').datagrid('getRows')[index];
		if (row){
			$('#form-department').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_department')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	

	function removedepartment(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#department-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('department/admin/department/delete_json')?>', {id:[row.id]}, function(){
					$('#department-table').datagrid('deleteRow', index);
					$('#department-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#department-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('department/admin/department/delete_json')?>',{id:selected},function(data){
						$('#department-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-department').form('submit',{
			url: '<?php  echo site_url('department/admin/department/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-department').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#department-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close

		});		
		
	}
	
	
</script>