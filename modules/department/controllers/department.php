<?php

class Department extends Member_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->module_model('department', 'department_model');
        $this->lang->module_load('department', 'department');
        $this->load->module_model('category','category_model');
        $this->load->module_model('material','material_model');
        $this->load->module_model('material_department','material_department_model');
        $this->load->module_model('stock','stock_model');
        $this->load->module_model('dept_request', 'dept_request_model');
        $this->lang->module_load('dept_request', 'dept_request');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }

    public function index()
    {
        // Display Page
//        $data['categories'] = $this->category_model->getCategories()->result_array();
        $department_id = $this->session->userdata('department_id');
        $this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
        // $this->db->group_by('materials.material_id');
        $this->db->order_by('order','asc');
        $this->db->order_by('material_name','asc');
        $this->db->where('group_id',$department_id);
        $data['materials'] = $this->material_department_model->getMaterialDepartments()->result_array();
        // echo '<pre>';
        // print_r($data['materials']);exit;

        $this->dept_request_model->joins = array('MATERIALS');
        $this->db->order_by('request_en_date','desc');
        $this->db->order_by('request_id','desc');
        $this->db->where('department_id',$department_id);
        $data['store_approvals'] = $this->dept_request_model->getDeptRequests(array('store_status'=>0))->result_array();

        $this->db->where('department_id',$department_id);
        $this->db->order_by('request_en_date','desc');
        $this->db->order_by('request_id','desc');
        $data['store_approvals_no'] = $this->dept_request_model->count(array('store_status'=>0));
        
        $this->db->order_by('request_en_date','desc');
        $this->db->order_by('request_id','desc');
        $this->db->where('department_id',$department_id);
        $data['pending_requests'] = $this->dept_request_model->getDeptRequests(array('department_status' => 0, 'store_status'=>1))->result_array();
        
        $this->db->where('department_id',$department_id);
        $data['pending_no'] = $this->dept_request_model->count(array('department_status' => 0, 'store_status'=>1));
        
        $this->db->order_by('request_en_date','desc');
        $this->db->order_by('request_id','desc');
        $this->db->where('department_id',$department_id);
        $data['approved_requests'] = $this->dept_request_model->getDeptRequests(array('department_status' => 1, 'store_status'=>1))->result_array();
        
        $this->db->where('department_id',$department_id);
        $data['approved_no'] = $this->dept_request_model->count(array('department_status' => 1, 'store_status'=>1));
        
        $this->db->order_by('request_en_date','desc');
        $this->db->order_by('request_id','desc');
        $this->db->where('department_id',$department_id);
        $this->db->where('department_status', 2);
        $this->db->or_where('store_status',2);
        $data['disapproved_requests'] = $this->dept_request_model->getDeptRequests()->result_array();
        
        $this->db->where('department_id',$department_id);
        $data['disapproved_no'] = $this->dept_request_model->count(array('department_status' => 2, 'store_status'=>1));
        
        $data['header'] = 'Demand Request';
        $data['view_page'] = "department/index";
        $data['module'] = 'department';
        $this->load->view($this->_container, $data);
    }

    public function get_material()
    {
        $id = $_POST['category_id'];

        //Preventing injection attack
        if(!is_numeric($id)){
            echo "Invalid Character";
            exit();
        }

        $materials=$this->material_model->getMaterials(array('status'=>1,'category_id'=>$id))->result_array();
        echo json_encode($materials);
    }

    public function get_unit()
    {
        $mat_id = $_POST['material_id'];

        //Preventing injection attack
        if(!is_numeric($mat_id)){
            echo "Invalid Character";
            exit();
        }

        $unit=$this->material_model->getMaterials(array('material_id'=>$mat_id))->row_array();
        echo json_encode($unit);
    }

    public function get_stock()
    {
        $mat_id = $_POST['material_id'];
        $dept_id = $this->session->userdata('department_id');

        //Preventing injection attack
        if(!is_numeric($mat_id) || !is_numeric($dept_id)){
            echo "Invalid Character";
            exit();
        }

        $stock=$this->stock_model->getStocks(array('material_id'=>$mat_id,'department_id'=>$dept_id))->row_array();
        $stock['amount'] = number_format($stock['amount']);
        // echo $this->db->last_query();
        echo json_encode($stock);
    }

    public function add_row(){
        $data['i'] = $this->input->post('index');
        // $data['materials'] = $this->material_model->getMaterials()->result_array();
        $this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT');
        // $this->db->group_by('materials.material_id');
        $this->db->order_by('order','asc');
        $this->db->order_by('material_name','asc');
        $this->db->where('group_id',$this->session->userdata('department_id'));
        $data['materials'] = $this->material_department_model->getMaterialDepartments()->result_array();
        $this->load->view('department/add_more',$data);
    }



}