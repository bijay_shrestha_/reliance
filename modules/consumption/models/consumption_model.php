<?php
class Consumption_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('CONSUMPTION'=>$this->prefix.'consumption','MATERIALS'=>$this->prefix.'materials','MATERIAL_PROPERTY'=>$this->prefix.'material_property','BRAND'=>$this->prefix.'brand','QUALITY'=>$this->prefix.'quality','DIMENSION'=>$this->prefix.'dimension','COLOR'=>$this->prefix.'color','EMBOSS'=>$this->prefix.'emboss','DESIGNS'=>$this->prefix.'designs','COATING'=>$this->prefix.'coating','CALANDER_SHIFT_REPORT'=>$this->prefix.'calander_shift_report');
		$this->_JOINS=array('MATERIAL_PROPERTY'=>array('join_type'=>'LEFT','join_field'=>'consumptions.material_property_id=material_property.id',
                                           'select'=>'material_property.*','alias'=>'material_property'),
        'BRAND'=>array('join_type'=>'LEFT','join_field'=>'material_property.brand_id=brand.id',
                                           'select'=>'brand.*','alias'=>'brand'),
        'QUALITY'=>array('join_type'=>'LEFT','join_field'=>'material_property.quality_id=quality.id',
                                           'select'=>'quality.*','alias'=>'quality'),
        'DIMENSION'=>array('join_type'=>'LEFT','join_field'=>'material_property.dimension_id=dimension.id',
                                           'select'=>'dimension.*','alias'=>'dimension'),
        'COLOR'=>array('join_type'=>'LEFT','join_field'=>'material_property.color_id=color.id',
                                           'select'=>'color.*','alias'=>'color'),
        'EMBOSS'=>array('join_type'=>'LEFT','join_field'=>'material_property.emboss_id=emboss.id',
                                           'select'=>'emboss.*','alias'=>'emboss'),
        'MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'consumptions.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
        'DESIGNS'=>array('join_type'=>'LEFT','join_field'=>'material_property.design_id=designs.id',
                                           'select'=>'designs.*','alias'=>'designs'),
        'COATING'=>array('join_type'=>'LEFT','join_field'=>'material_property.coating_id=coating.id',
                                           'select'=>'coating.*','alias'=>'coating'),
        'CALANDER_SHIFT_REPORT'=>array('join_type'=>'LEFT','join_field'=>'consumptions.calander_report_id=calander_shift_report.report_id',
                                           'select'=>'calander_shift_report.*','alias'=>'calander_shift_report'),
                           
                            );        
    }
    
    public function getConsumptions($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='consumptions.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['CONSUMPTION']. ' consumptions');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['CONSUMPTION'].' consumptions');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

    public function getAllConsumptions($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='consumptions.*,SUM(consumption_amount) as item_amount';
       
    foreach($this->joins as $key):
      $fields=$fields . ','.$this->_JOINS[$key]['select'];
    endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['CONSUMPTION']. ' consumptions');
    
    foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
    endforeach;         
        
    (! is_null($where))?$this->db->where($where):NULL;
    (! is_null($order_by))?$this->db->order_by($order_by):NULL;

    if( ! is_null($limit['limit']))
    {
      $this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
    }
    return $this->db->get();      
    }
    //for calander report
    public function getForcalanderReport(){
      
    }

    // get dispatch count daywise
    public function getDaywiseCount($where=NULL){
        $fields='COUNT(consumptions.consumption_id) as total';
       
        foreach($this->joins as $key):
            $fields=$fields . ','.$this->_JOINS[$key]['select'];
        endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['CONSUMPTION']. ' consumptions');
        
        foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;         
        
        (! is_null($where))?$this->db->where($where):NULL;

        $total_array = $this->db->get()->result_array();
        $days = 0;
        foreach($total_array as $array){
            // $days += $array['total'];
            $days += 1;
        }
        return $days;
    }
    // number of working day
    public function getWorkingDays($department_id = NULL,$date = NULL){
        $fields='COUNT(consumptions.consumption_id) as total';

        ($department_id)?$this->db->where('department_id',$department_id):NULL;
        $dates = explode('-',$date);
        $result = array();
        
        $like = $dates['0'];
        $this->db->select($fields);
        $this->db->from($this->_TABLES['CONSUMPTION']. ' consumptions');
        // if($dates[1]<3){
        //   $this->db->like('nepali_date',$like);
        // }
        if($dates[1] > 2){
          $this->db->where('nepali_date >=',($dates['0']).'-04-01');
          $this->db->where('nepali_date <=',($dates['0']+1).'-03-35');
        }else{
          $this->db->where('nepali_date >=',($dates['0']-1).'-04-01');
          $this->db->where('nepali_date <=',($dates['0']).'-03-35');
        }

        $this->db->group_by('nepali_date');
        $days = $this->db->get()->result_array();
        $result['yearly_working_day']  = count($days);

        $like .= '-'.$dates['1'];
        $this->db->select($fields);
        $this->db->from($this->_TABLES['CONSUMPTION']. ' consumptions');
        $this->db->like('nepali_date',$like);
        ($department_id)?$this->db->where('department_id',$department_id):NULL;
        $this->db->group_by('nepali_date');
        $days = $this->db->get()->result_array();
        $result['monthly_working_day']  = count($days);

        return $result;
    }
}