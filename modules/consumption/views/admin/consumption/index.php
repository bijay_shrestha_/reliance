<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('consumption_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="consumption-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('english_date')?></label>:</td>
<td><input type="text" name="date[english_date][from]" id="search_english_date_from"  class="easyui-datebox"/> ~ <input type="text" name="date[english_date][to]" id="search_english_date_to"  class="easyui-datebox"/></td>
<td><label><?php echo lang('nepali_date')?></label>:</td>
<td><input type="text" name="search[nepali_date]" id="search_nepali_date"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('material_property_id')?></label>:</td>
<td><input type="text" name="search[material_property_id]" id="search_material_property_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('material_id')?></label>:</td>
<td><input type="text" name="search[material_id]" id="search_material_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('consumption_amount')?></label>:</td>
<td><input type="text" name="search[consumption_amount]" id="search_consumption_amount"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="consumption-table" data-options="pagination:true,title:'<?php  echo lang('consumption')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'english_date',sortable:true" width="50"><?php echo lang('english_date')?></th>
<th data-options="field:'nepali_date',sortable:true" width="50"><?php echo lang('nepali_date')?></th>
<th data-options="field:'material_property_id',sortable:true" width="50"><?php echo lang('material_property_id')?></th>
<th data-options="field:'material_id',sortable:true" width="50"><?php echo lang('material_id')?></th>
<th data-options="field:'consumption_amount',sortable:true" width="50"><?php echo lang('consumption_amount')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_consumption')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_consumption')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit consumption form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-consumption" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('english_date')?>:</label></td>
					  <td width="66%"><input name="english_date" id="english_date" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('nepali_date')?>:</label></td>
					  <td width="66%"><input name="nepali_date" id="nepali_date" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('material_property_id')?>:</label></td>
					  <td width="66%"><input name="material_property_id" id="material_property_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('material_id')?>:</label></td>
					  <td width="66%"><input name="material_id" id="material_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('consumption_amount')?>:</label></td>
					  <td width="66%"><input name="consumption_amount" id="consumption_amount" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#consumption-search-form').form('clear');
			$('#consumption-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#consumption-table').datagrid({
				queryParams:{data:$('#consumption-search-form').serialize()}
				});
		});		
		$('#consumption-table').datagrid({
			url:'<?php  echo site_url('consumption/admin/consumption/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_consumption')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removeconsumption('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_consumption')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-consumption').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_consumption')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#consumption-table').datagrid('getRows')[index];
		if (row){
			$('#form-consumption').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_consumption')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeconsumption(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#consumption-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('consumption/admin/consumption/delete_json')?>', {id:[row.id]}, function(){
					$('#consumption-table').datagrid('deleteRow', index);
					$('#consumption-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#consumption-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('consumption/admin/consumption/delete_json')?>',{id:selected},function(data){
						$('#consumption-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-consumption').form('submit',{
			url: '<?php  echo site_url('consumption/admin/consumption/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-consumption').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#consumption-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>