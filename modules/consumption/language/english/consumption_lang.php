<?php


$lang['id'] = 'Id';
$lang['english_date'] = 'English Date';
$lang['nepali_date'] = 'Nepali Date';
$lang['material_property_id'] = 'Material Property Id';
$lang['material_id'] = 'Material Id';
$lang['consumption_amount'] = 'Consumption Amount';

$lang['create_consumption']='Create Consumption';
$lang['edit_consumption']='Edit Consumption';
$lang['delete_consumption']='Delete Consumption';
$lang['consumption_search']='Consumption Search';

$lang['consumption']='Consumption';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

