<?php

class Consumption extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('consumption','consumption_model');
        $this->lang->module_load('consumption','consumption');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'consumption';
		$data['page'] = $this->config->item('template_admin') . "consumption/index";
		$data['module'] = 'consumption';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->consumption_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->consumption_model->getConsumptions()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['nepali_date']!='')?$this->db->like('nepali_date',$params['search']['nepali_date']):'';
($params['search']['material_property_id']!='')?$this->db->where('material_property_id',$params['search']['material_property_id']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['consumption_amount']!='')?$this->db->where('consumption_amount',$params['search']['consumption_amount']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->consumption_model->getConsumptions()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->consumption_model->delete('CONSUMPTION',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->consumption_model->insert('CONSUMPTION',$data);
        }
        else
        {
            $success=$this->consumption_model->update('CONSUMPTION',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['english_date'] = $this->input->post('english_date');
$data['nepali_date'] = $this->input->post('nepali_date');
$data['material_property_id'] = $this->input->post('material_property_id');
$data['material_id'] = $this->input->post('material_id');
$data['consumption_amount'] = $this->input->post('consumption_amount');

        return $data;
   }
   
   	
	    
}