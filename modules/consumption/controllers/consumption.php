<?php

class Consumption extends Member_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->module_load('consumption', 'consumption');
        $this->load->module_model('consumption', 'consumption_model');
        $this->load->module_model('material', 'material_model');
        $this->load->module_model('material_department', 'material_department_model');
        $this->load->module_model('material_property','material_property_model');
        $this->load->module_model('department_materialproperty','department_materialproperty_model');
        $this->load->module_model('stock', 'stock_model');
        $this->load->module_model('dept_request', 'dept_request_model');
                
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }

    public function index()
    {
        // Display Page
        $data['header'] = 'Consumption';
        $data['view_page'] = "consumption/index";
        $data['module'] = 'consumption';
        $data['rows'] = $this->getConsumptions();
        $data['materials'] = $this->getMaterials();

        $data['properties'] = $this->getProperty();
        
        if(array_key_exists(0, $data['properties'])){
            $init_property = str_replace(" ","_",strtolower($data['properties'][0]['material_property'])); 
            $data['init_property'] = $init_property;  
            $data['init_properties'] = $this->getmaterialProperty($init_property);
            
        }
        // echo '<pre>';
        // foreach($data['init_properties'] as $a){
        // print_r($a['brand_name']);echo '<br>';
        // }
        // print_r($data);
        // exit;
        $this->load->view($this->_container, $data);
    }

    public function getConsumptions()
    {
        $this->consumption_model->joins = array('MATERIAL_PROPERTY','BRAND','QUALITY','DIMENSION','COLOR','EMBOSS','MATERIALS','DESIGNS','COATING');
        $department_id = $this->session->userdata('department_id');
        $this->db->where('consumptions.department_id',$department_id);
        $this->db->order_by('consumption_id','desc');
        $rows =$this->consumption_model->getConsumptions()->result_array(); //offset 7 limit 7 wehre thisweek
$data = array();
        $properties = $this->getProperty();
        foreach ($rows as $row) {
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['Brand'] = $row['brand_name'];
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['Quality'] = $row['quality_name'];
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['Dimension'] = $row['dimension_name'];
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['Color'] = $row['color_name'];
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['Emboss'] = $row['emboss_name'];
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['mat'] = $row['material'];
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['materials'][$row['material_id']] = $row['consumption_amount'];
            $data[$row['nepali_date']][$row['group']]['shifts'][$row['shift']]['Coating'] = $row['coating_name'];
        }


        return $data;
    }

    public function getMaterials()
    {
        $this->material_department_model->joins = array('MATERIALS','CATEGORY_DEPARTMENT','STOCK');
        $this->db->order_by('order');
        $this->db->order_by('material_name');
        $id = $this->session->userdata('department_id');//id of department from session
        $rows = $this->material_department_model->getMaterialDepartments(array('group_id'=>$id))->result_array(); //must pull dept id from session
        $stocks = $this->getOpeningStocks();
        $i = 0;
        foreach($rows as $row){
            if(array_key_exists($row['materialid'], $stocks)){
                $rows[$i]['amount'] = $stocks[$row['materialid']];
            }
            $i++;
        }
        // echo '<pre>';print_r($rows);
        // print_r($stocks);exit;
        return $rows;
    }

    public function getOpeningStocks(){
        $date = date('Y-m-d');
        $department_id = $this->session->userdata('department_id');
        $this->material_department_model->joins = array('CATEGORY_DEPARTMENT');
        $this->db->order_by('order');
        $materials = $this->material_department_model->getMaterialDepartments(array('group_id'=>$department_id))->result_array();
        $result = array();

        foreach($materials as $material){
            $this->db->group_by('material_id');
            $this->db->where('dept_requests.department_id',$department_id);
            $this->db->where('store_confirm_en_date <=',$date);
            $this->db->where('material_id', $material['material_id']);
            $this->db->where('store_status',1);
            $this->db->where('department_status',1);
            $recived_item = $this->dept_request_model->getAllDispatches()->row_array();

            $this->db->where('consumptions.department_id',$department_id);
            $this->db->where('english_date <=',$date);
            $this->db->where('consumptions.material_id', $material['material_id']);
            $consumed_item = $this->consumption_model->getAllConsumptions()->row_array();
            
            if(array_key_exists('all_dispatched', $recived_item) && array_key_exists('item_amount', $consumed_item)){
                $result[$material['material_id']] = $recived_item['all_dispatched'] - $consumed_item['item_amount'];
            }else if(array_key_exists('all_dispatched', $recived_item)){
                $result[$material['material_id']] = $recived_item['all_dispatched'];
            }
        }
        return $result;
    }

    public function getBrands(){
        $this->material_property_model->joins = array('BRAND');
        $department_id = $this->session->userdata('department_id');//session department id
        $this->db->group_by('brand_id');
        $results = $this->material_property_model->getMaterialProperties(array('department_id'=>$department_id))->result_array();
        return $results;
    }

    public function getValue(){

        $department = $this->input->post('department');
        $department = $this->session->userdata('department_id');
        $brand_id = $this->input->post('brand_id');
        $this->db->where('department_id',$department);
        $this->db->where('brand_id',$brand_id);
        if($this->input->post('color_id')){
            $quantity = $this->input->post('quality_id');
            $dimension = $this->input->post('dimension_id');
            $color = $this->input->post('color');
            $this->db->where('quality_id',$quality);
            $this->db->where('dimension_id',$dimension);
            $this->db->where('color_id',$color);
            $this->db->group_by('color_id');
        }
        else if($this->input->post('dimension_id')){
            $quality = $this->input->post('quality_id');
            $dimension = $this->input->post('dimension_id');
            $this->db->where('quality_id',$quality);
            $this->db->where('dimension_id',$dimension);
            $this->db->group_by('color_id');
        }
        else if($this->input->post('quality_id')){
            $quality = $this->input->post('quality_id');
            $this->db->where('quality_id',$quality);
            $this->db->group_by('dimension_id');
        }else{
            $this->db->group_by('quality_id');
        }
        $this->material_property_model->joins = array('BRAND','QUALITY','DIMENSION','COLOR','EMBOSS');
        $result = $this->material_property_model->getMaterialProperties()->result_array();
        // echo '<pre>';print_r($result);
        // echo $this->db->last_query();
        echo json_encode($result);
    }

    public function getdimension(){
        $department = $this->input->post('department');
        $department = $this->session->userdata('department_id');//from session
    }
    //saving consumption
    public function save_consumption(){
        // echo '<pre>';
        $properties = $this->getProperty();
        // print_r($this->input->post());exit();

        $success = FALSE;
        $this->db->order_by('group','desc');
        $this->db->limit(1);
        $last_consumption = $this->consumption_model->getConsumptions()->row_array();
        if(array_key_exists('group',$last_consumption)){
            $data['group'] = $last_consumption['group'] +1;
        }else{
            $data['group'] = 1;
        }
        $data['department_id'] = $this->session->userdata('department_id');
        $data['nepali_date'] = $this->input->post('date');
        $data['english_date'] = $this->input->post('eng_date');
        $data['shift'] = $this->input->post('shift');
        $data['material'] = $this->input->post('material');

        $where = array();
        foreach($properties as $property){
            // print_r($property);
            $where[str_replace(" ","_",strtolower($property['material_property'])).'_id'] = $this->input->post(str_replace(" ","_",strtolower($property['material_property'])).'_id'); 
        }
        
        $where['department_id'] = $data['department_id'];
        $material_property = $this->material_property_model->getMaterialProperties($where)->row_array();
// echo '<pre>';
// echo $this->db->last_query();exit;
//         print_r($material_property);
        $data['material_property_id'] = $material_property['id'];
        $material_ids = $this->input->post('material_ids');
        $consumed_stocks = str_replace(',', '', $this->input->post('consumed_stocks'));
        $current_stocks = str_replace(',', '', $this->input->post('current_stocks'));
        $closing_stocks = str_replace(',', '', $this->input->post('closing_stocks'));
        $i = 0;

        foreach($material_ids as $material_id){
            // echo 'current stock'.$i.' = '.$current_stocks[$i].'<br>';
            if($consumed_stocks[$i] == ''){
            }else if($consumed_stocks[$i] > 0 && $consumed_stocks[$i] != ''){
                $data['material_id'] = $material_id;
                $data['consumption_amount'] = $consumed_stocks[$i];
                $this->consumption_model->insert('CONSUMPTION',$data);
                
                $closing_stock = $closing_stocks[$i];
                $success = $this->stock_model->update('STOCK',array('amount'=>$closing_stock), array('material_id'=>$material_id,'department_id'=>$data['department_id']));
            }
            $i++;
        }
        echo json_encode($success);

    }

    private function getProperty(){
        $department_id = $this->session->userdata('department_id');
        $this->db->order_by('order','asc');
        $result = $this->department_materialproperty_model->getDepartmentMaterialproperties(array('department_id'=>$department_id))->result_array();
        return $result;
    }

    public function getmaterialProperty($property){
        $department_id = $this->session->userdata('department_id');
        $this->db->group_by($property.'_id');
        $this->db->order_by($property.'_name');
        $this->material_property_model->joins = array('QUALITY','BRAND','DIMENSION','COLOR','EMBOSS','COATING');
        $result = $this->material_property_model->getMaterialProperties(array('department_id'=>$department_id))->result_array();
        return $result;
    }
    public function getNextValue(){
        $department = $this->session->userdata('department_id');
        $group_by = $this->input->post('group_by');
        $prop = $this->input->post('properties');
        $limit = $this->input->post('option')+1;
        $this->db->limit($limit);
        $properties  = $this->getProperty();
        $i = 0;
        foreach($properties as $property){
            $this->db->where(str_replace(" ","_",strtolower($property['material_property']).'_id'),$prop[$i]);
            $i++;
        }
        $this->db->group_by($group_by.'_id');
        $this->db->where('department_id',$department);
        $this->material_property_model->joins = array('BRAND','QUALITY','DIMENSION','COLOR','EMBOSS','DESIGNS','COATING');
        $result = $this->material_property_model->getMaterialProperties()->result_array();

        echo json_encode($result);
    }
    //remove consumption
    public function removeConsumption(){
        $group = $this->input->post('group');
        $consumptions = $this->consumption_model->getConsumptions(array('group'=>$group))->result_array();
        $success = $this->consumption_model->delete('CONSUMPTION',array('group'=>$group));
        if($success){
            foreach($consumptions as $consumption){
                $stock = $this->stock_model->getstocks(array('material_id'=>$consumption['material_id'],'department_id'=>$consumption['department_id']))->row_array();
                if(array_key_exists('amount', $stock)){
                    $amount = $stock['amount'] + $consumption['consumption_amount'];
                    $this->stock_model->update('STOCK',array('amount'=>$amount),array('material_id'=>$consumption['material_id'],'department_id'=>$consumption['department_id']));
                }
            }
        }
        echo json_encode($success);
    }

    //get consumption
    public function editForm($group){
        // echo '<pre>';
        $data['group'] = $group;
        $data['consumptions'] = $this->consumption_model->getConsumptions(array('group'=>$group))->result_array();
        $data['consumed_material_property'] = $this->material_property_model->getMaterialProperties(array('id'=>$data['consumptions'][0]['material_property_id']))->row_array();
        $this->material_property_model->joins = array('BRAND','QUALITY','DIMENSION','COLOR','EMBOSS','DESIGNS','COATING');
        $material_property = $this->material_property_model->getMaterialProperties(array('material_properties.id'=>$data['consumptions'][0]['material_property_id']))->result_array();

        $data['properties'] = $this->getProperty();
        $i = 0;
        $a = array();
        foreach($data['properties'] as $prop){
            $init_property = str_replace(" ","_",strtolower($prop['material_property'])); 
            $data['init_property'][] = $init_property;  
            $a[$i] = $data['consumed_material_property'][strtolower(str_replace(' ','_',$prop['material_property'])).'_id'];
            if($i==0){
                $data['init_properties'][$init_property] = $this->getmaterialProperty($init_property);
            }else{
                $data['init_properties'][$init_property] = $this->getNextoptionValue($data['init_property'][$i],$a,$i);
            }
            $i++;
        }

        //consumed material_id
        foreach($data['consumptions'] as $consumed){
            $data['consumed_material'][$consumed['material_id']] = $consumed;
        }
// print_r($data['consumed_material']);exit;
        // Display Page
        $data['header'] = 'Consumption Edit Form';
        $data['view_page'] = "consumption/form";
        $data['module'] = 'consumption';
        $data['rows'] = $this->getConsumptions();
        $data['materials'] = $this->getMaterials();
// print_r($data);exit;
        if(count($data['consumptions'])>0){
            $this->load->view($this->_container, $data);
        }else{
            redirect('consumption');
        }

    }

    //get next option value for edit
    public function getNextoptionValue($group_by,$properties,$option){
        $department = $this->session->userdata('department_id');
        $group_by = $group_by;
        $prop = $properties;
        $limit = $option;
        $this->db->limit($limit);
        $properties  = $this->getProperty();
        $i = 0;
        foreach($properties as $property){
            $this->db->where(str_replace(" ","_",strtolower($property['material_property']).'_id'),$prop[$i]);
            $i++;
        }
        $this->db->group_by($group_by.'_id');
        $this->db->where('department_id',$department);
        $this->material_property_model->joins = array('BRAND','QUALITY','DIMENSION','COLOR','EMBOSS','DESIGNS','COATING');
        $result = $this->material_property_model->getMaterialProperties()->result_array();
        return $result;
    }


    //edit consumption
    public function editConsumption(){
        // echo '<pre>';
        $group = $this->input->post('group');
        $properties = $this->getProperty();
        $success = FALSE;
        // $data['group'] = $group;
        $data['department_id'] = $this->session->userdata('department_id');
        $data['nepali_date'] = $this->input->post('date');
        $data['english_date'] =  $this->input->post('eng_date');
        $data['shift'] =  $this->input->post('shift');
        
        $data['material'] = $this->input->post('material');
        
        $where = array();
        foreach($properties as $property){
            $where[str_replace(" ","_",strtolower($property['material_property'])).'_id'] = $this->input->post(str_replace(" ","_",strtolower($property['material_property'])).'_id'); 
        }
        
        $where['department_id'] = $data['department_id'];
        $material_property = $this->material_property_model->getMaterialProperties($where)->row_array();

        $data['material_property_id'] = $material_property['id'];
        $material_ids = $this->input->post('material_ids');
        $consumed_stocks = str_replace(',', '',$this->input->post('consumed_stocks'));
        $current_stocks = str_replace(',', '',$this->input->post('current_stocks'));
        $closing_stocks = str_replace(',', '',$this->input->post('closing_stocks'));
        $i = 0;
// print_r($material_ids);
        foreach($material_ids as $material_id){
            $last_consumption = $this->consumption_model->count(array('group'=>$group,'material_id'=>$material_id));
            // print_r($last_consumption);
            if($consumed_stocks[$i]>0 || $last_consumption != 0){
                $data['consumption_amount'] = $consumed_stocks[$i];
                $data['material_id'] = $material_id;
                $data['group'] = $group;
                
                $count = $this->consumption_model->count(array('group'=>$group,'material_id'=>$material_id));
                // echo $material_id;
                if($count > 0){
                    $this->consumption_model->update('CONSUMPTION',$data,array('group'=>$group,'material_id'=>$material_id));
                }else{
                    // echo 'not-here';
                    $this->consumption_model->insert('CONSUMPTION',$data);
                }

                $closing_stock = $closing_stocks[$i];
                $success = $this->stock_model->update('STOCK',array('amount'=>$closing_stock), array('material_id'=>$material_id,'department_id'=>$data['department_id']));
            }
            $i++;
        }
        echo json_encode($success);
        
    }
}