<?php

class Department_report extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('department_report','department_report_model');
        $this->load->module_model('department','department_model');
        $this->load->module_model('report','report_model');

        $this->lang->module_load('department_report','department_report');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->db->where('id <>',6);
		$data['departments'] = $this->department_model->getDepartments()->result_array();
		$data['reports'] = $this->report_model->getReports()->result_array();
		// Display Page
		$data['header'] = 'department_report';
		$data['page'] = $this->config->item('template_admin') . "department_report/index";
		$data['module'] = 'department_report';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->department_report_model->joins = array('DEPARTMENTS');
		$report_id = $this->input->post('report_id');
		// if($report_id){
		// 	$this->db->where('report_id',$report_id);
		// }
		// $this->_get_search_param();	
		// $total=$this->department_report_model->count();
		// paging('id');
		if($report_id){
			$this->db->where('report_id',$report_id);
		}
		// $this->_get_search_param();	
		$rows=$this->department_report_model->getDepartmentReports()->result_array();
// echo '<pre>';print_r($rows);
		// echo json_encode(array('total'=>$total,'rows'=>$rows));
		echo json_encode(array('rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['report_id']!='')?$this->db->where('report_id',$params['search']['report_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->department_report_model->getDepartmentReports()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->department_report_model->delete('DEPARTMENT_REPORTS',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		$data['report_id'] = $this->input->post('report_id');
		$department_ids = $this->input->post('department_id');
		$where['report_id'] = $data['report_id'];
		$this->department_report_model->delete('DEPARTMENT_REPORTS',$where);
		// echo '<pre>';print_r($this->input->post());exit;
		$success = 1;
		foreach ($department_ids as $key => $value) {
			$data['department_id'] = $value;
			$i = $this->department_report_model->insert('DEPARTMENT_REPORTS',$data);
			if(!$i){
				$success = $i;
			}
		}

  //       $data=$this->_get_posted_data(); //Retrive Posted Data		

  //       if(!$this->input->post('id'))
  //       {
  //           $success=$this->department_report_model->insert('DEPARTMENT_REPORTS',$data);
  //       }
  //       else
  //       {
  //           $success=$this->department_report_model->update('DEPARTMENT_REPORTS',$data,array('id'=>$data['id']));
  //       }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['department_id'] = $this->input->post('department_id');
$data['report_id'] = $this->input->post('report_id');

        return $data;
   }
   
   	
	    
}