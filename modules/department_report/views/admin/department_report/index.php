<div region="center" border="false">
<div style="padding:20px">

<form id="form-department_report" method="post">
	<div class="easyui-panel" data-options="title:'Department Report' ,collapsible:true, iconCls:'icon-search'" style="padding:15px">
	<select name='report_id' id="report_id" class="form-control margin-bottom">
		<option value="">-- Select Report --</option>
		<?php foreach($reports as $report){?>
			<option value="<?php echo $report['id']?>"><?php echo $report['report']?></option>
		<?php }?>
	</select>

	<!-- <select multiple="multiple" id="my-select" name="my-select[]"> -->
	<select name='department_id[]' id='custom-headers' multiple='multiple'>
			<?php foreach($departments as $key => $department){?>
				<option value="<?php echo $department['id']?>" class="department-option" id="department-<?php echo $department['id']?>?>"><?php echo $department['name']?></option>
			<?php }?>
	</select>
	<button type="button" id="" class="btn btn-primary margin form-group" onClick="save()">Save</button>
	</div>
</form>

</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#department_report-search-form').form('clear');
			$('#department_report-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#department_report-table').datagrid({
				queryParams:{data:$('#department_report-search-form').serialize()}
				});
		});		
		$('#department_report-table').datagrid({
			url:'<?php  echo site_url('department_report/admin/department_report/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
		// for multiple select
		// with search
		$('#custom-headers').multiSelect({
		  selectableHeader: "<div class='custom-header' style='background:#3c8dbc;color:white; text-align: center; font-size: 200%;'>Departments</div><br><input type='text' class='search-input form-control margin-bottom' autocomplete='off' placeholder='Search Department'>",
		  selectionHeader: "<div class='custom-header' style='background:#3c8dbc;color:white; text-align: center; font-size: 200%;'>Selected</div></br><input type='text' class='search-input form-control margin-bottom' autocomplete='off' placeholder='Search Selected'>",
		  afterInit: function(ms){
		    var that = this,
		        $selectableSearch = that.$selectableUl.prev(),
		        $selectionSearch = that.$selectionUl.prev(),
		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		    .on('keydown', function(e){
		      if (e.which === 40){
		        that.$selectableUl.focus();
		        return false;
		      }
		    });

		    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		    .on('keydown', function(e){
		      if (e.which == 40){
		        that.$selectionUl.focus();
		        return false;
		      }
		    });
		  },
		  afterSelect: function(){
		    this.qs1.cache();
		    this.qs2.cache();
		  },
		  afterDeselect: function(){
		    this.qs1.cache();
		    this.qs2.cache();
		  }
		});
		// multiSelect ends
		$('#report_id').change(function(){
			var report_id = $(this).val();
				$('#custom-headers').multiSelect('deselect_all');
				if(report_id != ''){
					$.post('<?php echo site_url("department_report/admin/department_report/json")?>',{report_id:report_id},function(data){
						// for each (var item in data) {
						// 	console.log(item);
						//   $('#department-'+item.id).attr('selected',true);
						// }
						$.each(data.rows,function(index,value){
							// console.log(value.name);
							$('#custom-headers').multiSelect('select', value.department_id);
							// $('#department-'+value.id).attr('selected',true);
		            	});
					},'json');
				}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_department_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removedepartment_report('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_department_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-department_report').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_department_report')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#department_report-table').datagrid('getRows')[index];
		if (row){
			$('#form-department_report').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_department_report')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removedepartment_report(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#department_report-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('department_report/admin/department_report/delete_json')?>', {id:[row.id]}, function(){
					$('#department_report-table').datagrid('deleteRow', index);
					$('#department_report-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#department_report-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('department_report/admin/department_report/delete_json')?>',{id:selected},function(data){
						$('#department_report-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-department_report').form('submit',{
			url: '<?php  echo site_url('department_report/admin/department_report/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-department_report').form('clear');
					$('#report_id').val('');
					$('#dlg').window('close');		// close the dialog
					$('#custom-headers').multiSelect('deselect_all');
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#department_report-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>