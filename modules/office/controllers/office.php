<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Office extends Member_Controller
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->module_model('attendance','attendance_model');
		$this->load->module_model('electric_report','electric_report_model');
		$this->load->module_model('department_report','department_report_model');
		$this->load->module_model('department','department_model');
		$this->load->module_model('diesel','diesel_model');
		$this->load->module_model('purchase','purchase_model');
		$this->load->module_model('calander_shift_report','calander_shift_report_model');
		$this->load->module_model('print_shift_report','print_shift_report_model');
		$this->load->module_model('laminating_shift_report','laminating_shift_report_model');
		$this->load->module_model('inspection','inspection_model');
		$this->load->module_model('rexin_coating_report','rexin_coating_report_model');
		$this->load->module_model('paper_tube','paper_tube_model');
		$this->load->module_model('scrap_segregation','scrap_segregation_model');
		$this->load->module_model('material','material_model');
		$this->load->module_model('tL_type', 'tL_type_model');
		$this->load->module_model('time_loss', 'time_loss_model');
		$this->load->module_model('tL_reason_full', 'tL_reason_full_model');
		$this->load->module_model('consumption', 'consumption_model');
		$this->load->module_model('paper_tube_working', 'paper_tube_working_model');

		$this->load->library('fpdf/fpdf.php');
	}

	private function getTimeWhere($given_date,$i,$index){
		$dates = explode('-', $given_date);
		$where = array();
		if($i == 0){
			if($dates[1] > 3){
				$where[$index .' > "'. $dates[0].'-04-01"'] = NULL;
				$where[$index. ' < "'.($dates[0]+1) .'-03-35"'] = NULL;
			}else{
				$where[$index .' > "'. ($dates[0]-1).'-04-01"'] = NULL;
				$where[$index. ' < "'.($dates[0]) .'-03-35"'] = NULL;
			}
		}else if($i == 1){
			$this->db->like($index,$dates['0'].'-'.$dates['1']);
		}else{
			$where[$index] = $given_date;
		}
		return $where;
	}

	function index($date = NULL)
	{
		if($date == NULL){
			$this->db->order_by('id','desc');
			$last_entry = $this->attendance_model->getAttendances()->row_array();
			// echo $this->db->last_query();
			// print_r($last_entry);
			if(count($last_entry) > 0 ){
				$date = $last_entry['date_np'];
			}else{
				$date = '2073-04-01';
				$data['date'] = $date;
			}
		}else{
			$data['date'] = $date;
		}
		$data['attendences'] = $this->getAttendance($date);
		$data['electric_reports'] = $this->getElectricReport($date);
		$data['generator_reports'] = $this->getGeneratorReport($date);
		$data['diesel_reports'] = $this->getStockReport($date,65);
		$data['husk_reports'] = $this->getStockReport($date,137);
		$data['ct2yard_reports'] = $this->getStockReport($date,67);
		$data['ct2mt_reports'] = $this->getStockReport($date,68);
		$data['production_reports'] = $this->getProductionReport($date);
		$data['io_reports'] = $this->getIoReport($date);
		$data['scrap_reports'] = $this->getScrapReport($date);
		$data['paper_tube_reports'] = $this->getPaperTubeReport($date);
		$data['paper_tube_working_reports'] = $this->getPaperTubeWorkingReports($date);
		$data['timeLossReport'] = $this->getTimeLoss($date);


		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit;
		$data['header'] = 'Dashboard';
		$data['view_page'] = 'home/office/index';

		$this->load->view($this->_container,$data);
	}
	public function getAttendance($date){
		$where['date_np'] = $date;
		$this->attendance_model->joins = $arrayName = array('DEPARTMENTS');
		$this->db->order_by('name');
		$data = $this->attendance_model->getAttendances($where)->result_array();
		return $data;
	}
	public function getElectricReport($date)
	{
		$where['date_np'] = $date;
		$data = $this->electric_report_model->getElectricReports($where)->row_array();
		return $data;
	}
	public function getGeneratorReport($date){
		$this->department_report_model->joins = array('DEPARTMENTS');
		$this->db->order_by('name');
		$departments = $this->department_report_model->getDepartmentReports(array('report_id' => 2))->result_array();
		$where['date_np'] = $date;
		$i = 0;
		foreach ($departments as $key => $value) {
			$where['department_id'] = $value['department_id'];
			// $this->diesel_model->joins = $arrayName = array('DEPARTMENTS');
			$data[$i] = $this->diesel_model->getAllDiesels($where)->row_array();
			$data[$i]['name'] = $value['name'];
			// echo $this->db->last_query().'<br>';
			$i++;
		}
		// exit;
		return $data;
	}
	public function getStockReport($date,$material_id)
	{
		$purchase = $this->purchase_model->getAllPurchases(array('date_np <'=>$date, 'material_id' =>$material_id))->row_array();
		$dispatch = $this->dept_request_model->getAllDispatches(array('store_confirm_np_date <'=>$date, 'material_id' =>$material_id, 'department_status'=>1))->row_array();
		$data['opening_stock'] = $purchase['total_purchase'] - $dispatch['all_dispatched'];
		$today_purchase = $this->purchase_model->getAllPurchases(array('date_np'=>$date, 'material_id' =>$material_id))->row_array();
		$data['purchase'] = $today_purchase['total_purchase'];
		$today_dispatch = $this->dept_request_model->getAllDispatches(array('store_confirm_np_date'=>$date, 'material_id' =>$material_id))->row_array();
		$data['dispatch'] = $today_dispatch['all_dispatched'];
		// echo $purchase['total_purchase'].'<br>'.$dispatch['all_dispatched'];
		// echo $this->db->last_query();
		// print_r($data);
// exit;
		return $data;
	}
	public function getProductionReport($date){
		$dates = explode('-', $date);
		foreach ($dates as $key => $value) {
			$where = $this->getTimeWhere($date,$key,'date-np');
			$total = $this->calander_shift_report_model->getAllReport($where)->row_array();
			$data['Calender']['unit'] = 'kg';
			$data['Calender'][$key] = $total['net_weight'];
			$where = array();
			$where = $this->getTimeWhere($date,$key,'date_np');
			$total = $this->print_shift_report_model->getAllReport($where)->row_array();
			$data['Printing']['unit'] = 'mtr';
			$data['Printing'][$key] = $total['total_output'];
			$where = array();
			$where = $this->getTimeWhere($date,$key,'date-np');
			$total = $this->laminating_shift_report_model->getAllReport($where)->row_array();
			$data['Lamination']['unit'] = 'mtr';
			$data['Lamination'][$key] = $total['total_output'];
			$where = array();
			$where = $this->getTimeWhere($date,$key,'date_np');
			$total = $this->inspection_model->getAllReport($where)->row_array();
			$data['Inspection']['unit'] = 'kg';
			$data['Inspection'][$key] = $total['total'];
			$where = array();
			$where = $this->getTimeWhere($date,$key,'date_np');
			$total = $this->rexin_coating_report_model->getAllReport($where)->row_array();
			$data['Coating']['unit'] = 'mtr';
			$data['Coating'][$key] = $total['total_production'];
			$data['Coating Production']['unit'] = 'Kg';
			$data['Coating Production'][$key] = $total['total_net_wt'];
			$where = array();
			$where = $this->getTimeWhere($date,$key,'date_np');
			$total = $this->paper_tube_model->getAllReport($where)->row_array();
			$data['Paper Tube']['unit'] = 'Pcs';
			$data['Paper Tube'][$key] = $total['total'];
			
		}
		// echo '<pre>';print_r($data);exit();
		return $data;
	}
	public function getIoReport($date){
		$report = $this->calander_shift_report_model->getAllReport(array('date-np'=>$date))->row_array();
		// echo '<pre>';print_r($report);exit;
		$data['Calander']['input'] = $report['mixing_tot'];
		$data['Calander']['output'] = $report['net_weight'];

		$report = $this->laminating_shift_report_model->getAllReport(array('date-np'=>$date))->row_array();
		// echo '<pre>';print_r($report);exit;
		$data['Lamination']['input'] = $report['total_base_ut'];
		$data['Lamination']['output'] = $report['total_output'];

		$report = $this->print_shift_report_model->getAllReport(array('date_np'=>$date))->row_array();
		// echo '<pre>';print_r($report);exit;
		$data['Printing']['input'] = $report['total_input'];
		$data['Printing']['output'] = $report['total_output'];

		$report = $this->rexin_coating_report_model->getAllReport(array('date_np'=>$date))->row_array();
		// echo '<pre>';print_r($report);exit;
		$data['Coating']['input'] = $report['total_gross_wt'];
		$data['Coating']['output'] = $report['total_net_wt'];
		// echo '<pre>';print_r($data);exit;
		return $data;
	}
	public function getScrapReport($date){
		$dates = explode('-', $date);
		$data['Today'] = array();
		$data['Monthly'] = array();
		$data['Yearly'] = array();

		foreach ($dates as $key => $value) {
			$where = $this->getTimeWhere($date,$key,'date_np');
			if($key == 0){
				$index = 'Yearly';
			}elseif ($key == 1) {
				$index = 'Monthly';
			}else{
				$index = 'Today';
			}
			$data[$index] = $this->scrap_segregation_model->getAllReport($where)->row_array();
			$data[$index]['total_hr'] = $data[$index]['total_hr'] + floor($data[$index]['total_min']/60);
			$data[$index]['total_min'] = $data[$index]['total_min']%60;
			$data[$index]['total_running_hr'] = $data[$index]['total_running_hr'] + floor($data[$index]['total_running_min']/60);
			$data[$index]['total_running_min'] = $data[$index]['total_running_min']%60;
		}
		// echo '<pre>';print_r($data);exit;
		return $data;
	}
	public function getPaperTubeReport($date)
	{
		$where['category_id'] = 19;
		$data = $this->material_model->getMaterials($where)->result_array();
		foreach ($data as $key => $value) {
			$data[$key]['opening_stock'] = $this->getStockReport($date,$value['material_id']);
			$production = $this->paper_tube_model->getAllReport(array('paper_tube_id'=>$value['material_id'],'date_np' => $date))->row_array();;
			$data[$key]['production'] = $production['total'];
		}
		// echo '<pre>';print_r($data);exit;
		// return $data;
		return $data;
	}
	public function getTimeLoss($date)
	{
		$this->tloss_department_model->joins = array('DEPARTMENTS');
		$this->db->order_by('name');
		$departments = $this->tloss_department_model->getTlossDepartments()->result_array();
			$last_entry_date = $date;
   			$dates = explode('-',$date);
		
   		// echo $last_entry_date.'<br>';
		foreach ($departments as $key => $value) {
			$department_id = $value['department_id'];
	   		$timeloss_types = $this->tL_type_model->getTlTypes()->result_array();
	   		foreach($dates as $x => $date){
		   		foreach($timeloss_types as $type){
		   			$where = NULL;
			   		$this->time_loss_model->joins = array('TL_REASON_FULL','TL_TYPES','TL_REASONINGS');
			   		if($x == 0){
		   				$index = 'yearly_loss';
		   				$where = $this->getTimeWhere($last_entry_date,$x,'date_np');
		   			}else if($x == 1){
		   				$index = 'monthly_loss';
		   				$this->db->like('date_np',$dates['0'].'-'.$dates['1']); 
		   			}else{
		   				$index = 'daily_loss';
		   				$this->db->like('date_np',$dates['0'].'-'.$dates['1'].'-'.$dates['2']);
		   			}
			   			$where['tl_reason_full.type_id'] = $type['type_id'];
			   			$where['department_id'] = $department_id;
		   			// echo '<pre>';print_r($where);echo '</pre>';
		   			$data[$value['department_id']][$index][$type['loss_type']]['total'] = $this->time_loss_model->getTimeLosseReports($where)->row_array();
		   			// echo '<br>'.$this->db->last_query().'<br>';
		   		}
	   		}
   		// echo $this->db->last_query().'<br>';
	   		if($department_id == 2){
	   			$data[2]['working_days_for_timeloss'] = $this->calander_shift_report_model->getWorkingDays($department_id,$last_entry_date);
	   		}elseif($department_id == 4){
	   			$data[4]['working_days_for_timeloss'] = $this->laminating_shift_report_model->getWorkingDays($last_entry_date);
	   		}else{
		   		$data[$value['department_id']]['working_days_for_timeloss'] = $this->consumption_model->getWorkingDays($department_id,$last_entry_date);
	   		}
	   		$data[$value['department_id']]['name'] = $value['name'];
		}
		// echo '<br>'.count($data).'<br>';
   		// echo '<pre>';
   		// print_r($data);exit;
   		return $data;
	}

	public function excel($date)
	{
		$data = $this->getDataForExport($date);
		$this->export($data,$date);
	}

	/*
     * exporting data for field report
     */
    public function export($reports,$date){
       // echo '<pre>';
       // print_r($reports);exit;
    	$x = 0;
        $objPHPExcel = new PHPExcel();
        	// if($x > 0){
        	// 	$objPHPExcel->createSheet();
        	// }
        $objPHPExcel->setActiveSheetIndex('0');
        $objPHPExcel->getActiveSheet()->setTitle('Sheet1');
        date_default_timezone_set('UTC');
        $j = 1;
        foreach($reports as $key => $data){
        	if($j != 1){
        		$j++;
        	}
	        $i = 'A';
	        if($key != "Time Loss Report"){
		        $objPHPExcel->getActiveSheet()->setCellValue($i.$j, $key);
		        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$j.':J'.$j)->getStyle('A'.$j.':B'.$j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j)
		        ->getFill()
		        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		        ->getStartColor()
		        ->setRGB('cccccc');
		        $j++;
		        foreach($data['title'] as $title){
		            $objPHPExcel->getActiveSheet()->setCellValue($i.$j, $title);
		            $i++;
		        }
		        $j++;
		        foreach($data['data'] as $report){
		        	$i = 'A';
		        	foreach($data['fields'] as $field){
		            	$objPHPExcel->getActiveSheet()->setCellValue($i.$j, $report[$field]);
		        		$i++;
		    		}
	                
	                $j++;
	            }
        	}else if($key == "Time Loss Report"){
    			$objPHPExcel->getActiveSheet()->setCellValue($i.$j, $key);
		        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$j.':J'.$j)->getStyle('A'.$j.':B'.$j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$j)
		        ->getFill()
		        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		        ->getStartColor()
		        ->setRGB('cccccc');
		        $j++;
		        foreach($data['title'] as $title){
		        	if($i == 'A'){
		            	$objPHPExcel->getActiveSheet()->setCellValue($i.$j, $title);
		            	$k = $j+1;
		            	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$j.':A'.$k)->getStyle('A'.$j.':A'.$k)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        	}else{
		        		$objPHPExcel->getActiveSheet()->setCellValue($i.$j, $title);
		        		$l = $i;
		        		$k = $j+1;
		        		$objPHPExcel->getActiveSheet()->setCellValue($i.$k, $data['subtitle'][0]);
		        		$i++;
		        		$objPHPExcel->getActiveSheet()->setCellValue($i.$k, $data['subtitle'][1]);
		        		$i++;
		        		$objPHPExcel->getActiveSheet()->setCellValue($i.$k, $data['subtitle'][2]);

		        		$objPHPExcel->setActiveSheetIndex(0)->mergeCells($l.$j.':'.$i.$j)->getStyle($l.$j.':'.$i.$j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		        	}
		            $i++;
		        	
	                
	                
		        }
		        $j = $j+2;
	            foreach($data['data'] as $report){
	        		$i = 'A';
		        	foreach($data['fields'] as $field){
		        		if($report[$field] == '00:00' && ($field == 'dwh' || $field == 'mwh' || $field == 'ywh')){
			            	$objPHPExcel->getActiveSheet()->setCellValue($i.$j, '24:00');
		        		}else{
			            	$objPHPExcel->getActiveSheet()->setCellValue($i.$j, $report[$field]);
		        		}
		        		$i++;
		    		}
		    		$j++;
            	}
    		}
            $x++;
    	}
            $filename='Office Report('.$date.').xls';
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
            //force user to download the Excel file without writing it to server's HD
            
        $objWriter->save('php://output');
        //- See more at: https://arjunphp.com/how-to-use-phpexcel-with-codeigniter/#sthash.SjpCbSQh.dpuf
        return FALSE;
    }
	public function pdf($date)
	{
	 	$data = $this->getDataForExport($date);	
		// echo '<pre>';
		// print_r($data);exit;
   		$this->exportToPdf($data,$date);	
	}
	public function exportToPdf($data,$date)
	{
// echo '<pre>';print_r($data);exit;
   			$filename = "Office_Report(".$date.").pdf";
			// Send headers
	header("Content-Type: application/pdf");
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header('Content-Disposition: attachment; filename="'.'"');
	header("Content-Transfer-Encoding: binary ");

   			$pdf = new FPDF();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',20);

			$pdf->Cell(80);
		    // Title
		    $pdf->Cell(30,10,'Yeti Polychem Pvt. Ltd.',0,0,'C');
			$pdf->SetFont('Arial','B',12);
			$pdf->Ln();//new line

			$pdf->Cell(80);
			$pdf->Cell(30,10,'Office Report',0,0,'C');
			$pdf->SetFont('Arial','B',8);
			$pdf->Ln();//new line
			
			$pdf->Cell(40,10,'Date : '.$date);
			$pdf->Ln();

			foreach ($data as $i => $datas) {
				if($i != 'Time Loss Report'){
					$pdf->cell(40,10, $i);
					$pdf->Ln();
					// $i = 0;
					foreach($datas['title'] as $index => $value){
						$pdf->Cell(39,6, $value,1);
					}
					$pdf->Ln();
					foreach ($datas['data'] as $key => $value) {
						foreach ($datas['fields'] as $index => $values) {
							$pdf->Cell(39,6, $value[$values],1);
						}
						$pdf->Ln();
					}	
				}else{
					$pdf->cell(40,10, $i);
					$pdf->Ln();
					// $i = 0;
					foreach($datas['title'] as $index => $value){
						if($index == 0){
							$pdf->Cell(20,12, $value,1);
						}else{
							$pdf->Cell(58,6, $value,1);
						}
					}
					$pdf->Ln();
					$pdf->Cell(20,12);
					for ($x=0; $x < 3; $x++) { 
						foreach($datas['subtitle'] as $index => $value){
							if($index == 1){
								$pdf->Cell(18,6, $value,1);
							}else{
								$pdf->Cell(20,6, $value,1);
							}
						}
					}
					$pdf->Ln();
					foreach ($datas['data'] as $key => $value) {
						foreach ($datas['fields'] as $index => $field) {
							if(in_array($index, array(2,5,8))){
								$p = 18;
							}else{
								$p = 20;
							}
							if($value[$field] == '00:00' && ($field == 'dwh' || $field == 'mwh' || $field == 'ywh')){
								$pdf->Cell($p,6, '24:00',1);
							}else{
								$pdf->Cell($p,6, $value[$field],1);
							}
						}
						$pdf->Ln();
					}
				}
				// 	$i++;
			}
			$pdf->Output($filename,'D');
    	} 
    	public function getDataForExport($date)
    	{
    		$key = 0;
    		$data['Attendence Report']['data'] = $this->getAttendance($date);
    		$day_staff = 0;
    		$day_contract = 0;
    		$night_staff = 0;
    		$night_contract = 0;
    		foreach ($data['Attendence Report']['data'] as $key => $value) {
    			$day_staff += $value['day_staff'];
    			$day_contract += $value['day_contract'];
    			$night_staff += $value['night_staff'];
    			$night_contract += $value['night_contract'];
    		}
    		$data['Attendence Report']['data'][$key+1]['name'] = 'Total';
    		$data['Attendence Report']['data'][$key+1]['day_staff'] = number_format($day_staff);
    		$data['Attendence Report']['data'][$key+1]['day_contract'] = number_format($day_contract);
    		$data['Attendence Report']['data'][$key+1]['night_staff'] = number_format($night_staff);
    		$data['Attendence Report']['data'][$key+1]['night_contract'] = number_format($night_contract);
			$data['Attendence Report']['title'] = ['Department','Day Staff','Day Contract','Night Staff','Night Contract'];
			$data['Attendence Report']['fields'] = ['name','day_staff','day_contract','night_staff','night_contract'];
			/************************************************************************************/
			$data['Electric Report']['data'][0] = $this->getElectricReport($date);

			$data['Electric Report']['data'][0]['r1'] = (array_key_exists('r1',$data['Electric Report']['data'][0]))?number_format(($data['Electric Report']['data'][0]['r1']),3):'0.000';
			$data['Electric Report']['data'][0]['r2'] = (array_key_exists('r2',$data['Electric Report']['data'][0]))?number_format(($data['Electric Report']['data'][0]['r2']),3):'0.000';
			$data['Electric Report']['data'][0]['r3'] = (array_key_exists('r3',$data['Electric Report']['data'][0]))?number_format(($data['Electric Report']['data'][0]['r3']),3):'0.000';
			$data['Electric Report']['data'][0]['total'] = (array_key_exists('total',$data['Electric Report']['data'][0]))?number_format(($data['Electric Report']['data'][0]['total']),3):'0.000';
			$data['Electric Report']['title'] = ['R1','R2','R3','Total'];
			$data['Electric Report']['fields'] = ['r1','r2','r3','total'];
			/********************************************************************************/
			$data['Generator Report']['data'] = $this->getGeneratorReport($date);
			$total = 0;
			foreach ($data['Generator Report']['data'] as $key => $value) {
				$running_hour = $value['total_hour'] + floor($value['total_min']/60);
				$running_min = $value['total_min']%60;
				$data['Generator Report']['data'][$key]['running_hour'] = $running_hour . 'hr ' .$running_min. 'min';
				$data['Generator Report']['data'][$key]['unit_per_lt'] = number_format(($value['total_amount'])?$value['total_unit']/$value['total_amount']:0,3);
				$data['Generator Report']['data'][$key]['total_unit'] = number_format($data['Generator Report']['data'][$key]['total_unit'],3);
				$data['Generator Report']['data'][$key]['total_amount'] = number_format($data['Generator Report']['data'][$key]['total_amount'],3);
				$total += $data['Generator Report']['data'][$key]['total_amount'];
			}
			$data['Generator Report']['data'][$key+1]['total_amount'] = number_format($total,3);
			$data['Generator Report']['data'][$key+1]['name'] = 'Total';
			$data['Generator Report']['data'][$key+1]['running_hour'] = '';
			$data['Generator Report']['data'][$key+1]['total_unit'] = '';
			$data['Generator Report']['data'][$key+1]['unit_per_lt'] = '';
			$data['Generator Report']['title'] = ['Generator Set','Running Hour','Units','Diesel Consumed','Unit Per Liter'];
			$data['Generator Report']['fields'] = ['name','running_hour','total_unit','total_amount','unit_per_lt'];
			/*****************************************************************************/
			$data['Diesel Report']['data'][0] = $this->getStockReport($date,65);
			$data['Diesel Report']['data'][0]['total'] = $data['Diesel Report']['data'][0]['opening_stock'] + $data['Diesel Report']['data'][0]['purchase'];
			$data['Diesel Report']['data'][0]['closing_stock'] = number_format($data['Diesel Report']['data'][0]['total'] - $data['Diesel Report']['data'][0]['dispatch'],3);
			$data['Diesel Report']['data'][0]['total'] = number_format($data['Diesel Report']['data'][0]['total'],3);
			$data['Diesel Report']['data'][0]['opening_stock'] = number_format($data['Diesel Report']['data'][0]['opening_stock'],3);
			$data['Diesel Report']['data'][0]['purchase'] = number_format($data['Diesel Report']['data'][0]['purchase'],3);
			$data['Diesel Report']['data'][0]['dispatch'] = number_format($data['Diesel Report']['data'][0]['dispatch'],3);
			$data['Diesel Report']['title'] = ['Opening Stock','Purchase','Total','Issue/Consumed','Closing Stock'];
			$data['Diesel Report']['fields'] = ['opening_stock','purchase','total','dispatch','closing_stock'];
			/*****************************************************************************************/
			$data['Husk Report']['data'][0] = $this->getStockReport($date,137);
			$data['Husk Report']['data'][0]['total'] = $data['Husk Report']['data'][0]['opening_stock'] + $data['Husk Report']['data'][0]['purchase'];
			$data['Husk Report']['data'][0]['closing_stock'] = number_format($data['Husk Report']['data'][0]['total'] - $data['Husk Report']['data'][0]['dispatch'],3);
			$data['Husk Report']['data'][0]['total'] = number_format($data['Husk Report']['data'][0]['total'],3);
			$data['Husk Report']['data'][0]['opening_stock'] = number_format($data['Husk Report']['data'][0]['opening_stock'],3);
			$data['Husk Report']['data'][0]['purchase'] = number_format($data['Husk Report']['data'][0]['purchase'],3);
			$data['Husk Report']['data'][0]['dispatch'] = number_format($data['Husk Report']['data'][0]['dispatch'],3);
			$data['Husk Report']['title'] = ['Opening Stock','Purchase','Total','Issue/Consumed','Closing Stock'];
			$data['Husk Report']['fields'] = ['opening_stock','purchase','total','dispatch','closing_stock'];
			/*****************************************************************************************/
			$data['CT 2 Yard Report']['data'][0] = $this->getStockReport($date,67);
			$data['CT 2 Yard Report']['data'][0]['total'] = $data['CT 2 Yard Report']['data'][0]['opening_stock'] + $data['CT 2 Yard Report']['data'][0]['purchase'];
			$data['CT 2 Yard Report']['data'][0]['closing_stock'] = number_format($data['CT 2 Yard Report']['data'][0]['total'] - $data['CT 2 Yard Report']['data'][0]['dispatch'],3);
			$data['CT 2 Yard Report']['data'][0]['total'] = number_format($data['CT 2 Yard Report']['data'][0]['total'],3);
			$data['CT 2 Yard Report']['data'][0]['opening_stock'] = number_format($data['CT 2 Yard Report']['data'][0]['opening_stock'],3);
			$data['CT 2 Yard Report']['data'][0]['purchase'] = number_format($data['CT 2 Yard Report']['data'][0]['purchase'],3);
			$data['CT 2 Yard Report']['data'][0]['dispatch'] = number_format($data['CT 2 Yard Report']['data'][0]['dispatch'],3);
			$data['CT 2 Yard Report']['title'] = ['Opening Stock','Issue/Consumed','Closing Stock'];
			$data['CT 2 Yard Report']['fields'] = ['opening_stock','dispatch','closing_stock'];
			/*****************************************************************************************/
			$data['CT 2 Meter Report']['data'][0] = $this->getStockReport($date,68);
			$data['CT 2 Meter Report']['data'][0]['total'] = $data['CT 2 Meter Report']['data'][0]['opening_stock'] + $data['CT 2 Meter Report']['data'][0]['purchase'];
			$data['CT 2 Meter Report']['data'][0]['closing_stock'] = number_format($data['CT 2 Meter Report']['data'][0]['total'] - $data['CT 2 Meter Report']['data'][0]['dispatch'],3);
			$data['CT 2 Meter Report']['data'][0]['total'] = number_format($data['CT 2 Meter Report']['data'][0]['total'],3);
			$data['CT 2 Meter Report']['data'][0]['opening_stock'] = number_format($data['CT 2 Meter Report']['data'][0]['opening_stock'],3);
			$data['CT 2 Meter Report']['data'][0]['purchase'] = number_format($data['CT 2 Meter Report']['data'][0]['purchase'],3);
			$data['CT 2 Meter Report']['data'][0]['dispatch'] = number_format($data['CT 2 Meter Report']['data'][0]['dispatch'],3);
			$data['CT 2 Meter Report']['title'] = ['Opening Stock','Issue/Consumed','Closing Stock'];
			$data['CT 2 Meter Report']['fields'] = ['opening_stock','dispatch','closing_stock'];
			/*****************************************************************************************/
			$production_reports = $this->getProductionReport($date);
			$i = 0;
			$total_0 = 0;
			$total_1 = 0;
			$total_2 = 0;
			foreach ($production_reports as $key => $value) {
				$data['Production Report']['data'][$i] = $value;
				$total_0 = $data['Production Report']['data'][$i][0];
				$total_1 = $data['Production Report']['data'][$i][1];
				$total_2 = $data['Production Report']['data'][$i][2];
				$data['Production Report']['data'][$i]['item'] = $key;
				$data['Production Report']['data'][$i][2] = number_format($data['Production Report']['data'][$i][2],3);
				$data['Production Report']['data'][$i][1] = number_format($data['Production Report']['data'][$i][1],3);
				$data['Production Report']['data'][$i][0] = number_format($data['Production Report']['data'][$i][0],3);
				$i++;
			}
			$data['Production Report']['data'][$i+1][0] = number_format($total_0,3);
			$data['Production Report']['data'][$i+1][1] = number_format($total_1,3);
			$data['Production Report']['data'][$i+1][2] = number_format($total_2,3);
			$data['Production Report']['data'][$i+1]['item'] = 'Total';
			$data['Production Report']['data'][$i+1]['unit'] = '';
			$data['Production Report']['title'] = ['Item','Unit','Today','This Month','This Year'];
			$data['Production Report']['fields'] = ['item','unit','2','1','0'];
			/*****************************************************************************/
			$data['IO Report']['data'] = $this->getIoReport($date);
			$input = 0;
			$output =0;
			foreach ($data['IO Report']['data'] as $key => $value) {
				$input += $value['input'];
				$output += $value['output'];
				$data['IO Report']['data'][$key]['section'] = $key;
				$data['IO Report']['data'][$key]['difference'] = number_format($value['input'] - $value['output'],3);
				$data['IO Report']['data'][$key]['loss'] = number_format(($value['input'])?(($value['input'] - $value['output'])*100/$value['input']):0,2);
				$data['IO Report']['data'][$key]['input'] = number_format($data['IO Report']['data'][$key]['input'],3);
				$data['IO Report']['data'][$key]['output'] = number_format($data['IO Report']['data'][$key]['output'],3);
			}
			$data['IO Report']['data'][$key+1]['input'] = number_format($input,3);
			$data['IO Report']['data'][$key+1]['output'] = number_format($output,3);
			$data['IO Report']['data'][$key+1]['difference'] = number_format($input - $output,3);
			$data['IO Report']['data'][$key+1]['section'] = 'Total';
			$data['IO Report']['data'][$key+1]['loss'] = '';

			$data['IO Report']['title'] = ['Section','Input','Output','Difference','Loss %'];
			$data['IO Report']['fields'] = ['section','input','output','difference','loss'];
			/******************************************************************************************/
			$data['Scrap Report']['data'] = $this->getScrapReport($date);
			foreach ($data['Scrap Report']['data'] as $key => $value) {
				$data['Scrap Report']['data'][$key]['key'] = $key;
				$hour = $value['total_hr'] + floor($value['total_min']/60);
				$min = $value['total_min']%60;
				$data['Scrap Report']['data'][$key]['segregation_time'] = $hour.'hr '.$min.'min';
				$hour = $value['total_running_hr'] + floor($value['total_running_min']/60);
				$min = $value['total_running_min']%60;
				$data['Scrap Report']['data'][$key]['running_time'] = $hour.'hr '.$min.'min';
				$data['Scrap Report']['data'][$key]['total'] = number_format($data['Scrap Report']['data'][$key]['total'],3);
			}
			$data['Scrap Report']['title'] = ['','Amount', 'Segregation Duration', 'Running Duration'];
			$data['Scrap Report']['fields'] = ['key','total','segregation_time','running_time'];
			/*****************************************************************************************/
			$data['Paper Tube Report']['data'] = $this->getPaperTubeReport($date);
			$opening = 0;
			$consumed = 0;
			$production = 0;
			foreach ($data['Paper Tube Report']['data'] as $key => $value) {
				$opening += $value['opening_stock']['opening_stock'];
				$consumed += $value['opening_stock']['purchase'];
				$production += $data['Paper Tube Report']['data'][$key]['production'];

				$data['Paper Tube Report']['data'][$key]['opening'] = number_format($value['opening_stock']['opening_stock'],3);
				$data['Paper Tube Report']['data'][$key]['consumed'] = number_format($value['opening_stock']['purchase'],3);
				$data['Paper Tube Report']['data'][$key]['closing'] = number_format($value['opening_stock']['opening_stock']+$value['production']-$value['opening_stock']['purchase'],3);
				$data['Paper Tube Report']['data'][$key]['production'] = number_format($data['Paper Tube Report']['data'][$key]['production'],3);
			}
			$data['Paper Tube Report']['data'][$key+1]['opening'] = number_format($opening,3);
			$data['Paper Tube Report']['data'][$key+1]['consumed'] = number_format($consumed,3);
			$data['Paper Tube Report']['data'][$key+1]['production'] = number_format($production,3);
			$data['Paper Tube Report']['data'][$key+1]['closing'] = number_format($opening-$consumed+$production,3);
			$data['Paper Tube Report']['data'][$key+1]['material_name'] = 'Total';

			$data['Paper Tube Report']['title'] = ['Size','Opening Stock', 'Production', 'Issue to Store', ' Balance'];
			$data['Paper Tube Report']['fields'] = ['material_name','opening','production','consumed','closing'];
			/*********************************************************************************************/
			$working_report = $this->getPaperTubeWorkingReports($date);
			$i = 0;
			// print_r($working_report);
			foreach ($working_report as $key => $value) {
				// print_r($value);
				$data['Paper Tube Working Report']['data'][$i]['index'] = $key;
				$min = (count($value)>0)?(($value['min']<10)?'0'.$value['min']:$value['min']):'00';
				$hour = (count($value)>0)?(($value['hour']<10)?'0'.$value['hour']:$value['hour']):'00';
				$data['Paper Tube Working Report']['data'][$i]['duration'] = $hour.':'.$min;
				$i++;
			}
			$data['Paper Tube Working Report']['title'] = ['','Duration'];
			$data['Paper Tube Working Report']['fields'] = ['index','duration'];
			/*********************************************************************************************/
			$TimeLossReport = $this->getTimeLoss($date);
			// echo '<pre>';print_r($TimeLossReport);exit;
			foreach ($TimeLossReport as $key => $value) {
				$data['Time Loss Report']['data'][$key]['name'] = $value['name'];
				/*
				d=day				m=month				y=year
				bd=breakdown		lh=losshour			wh=workinghour(24*60-bd-lh)
				*/
				$data['Time Loss Report']['data'][$key]['dbd'] = date('H:i', mktime(0,$value['daily_loss']['BreakDown']['total']['total']));
				$data['Time Loss Report']['data'][$key]['dlh'] = date('H:i', mktime(0,$value['daily_loss']['LossHour']['total']['total']));
				$data['Time Loss Report']['data'][$key]['dwh'] = date('H:i', mktime(0,24*60 - $value['daily_loss']['BreakDown']['total']['total'] - $value['daily_loss']['LossHour']['total']['total']));
				if($value['working_days_for_timeloss']['monthly_working_day'] != 0){
					$data['Time Loss Report']['data'][$key]['mbd'] = date('H:i', mktime(0,$value['monthly_loss']['BreakDown']['total']['total']/$value['working_days_for_timeloss']['monthly_working_day']));
					$data['Time Loss Report']['data'][$key]['mlh'] = date('H:i', mktime(0,$value['monthly_loss']['LossHour']['total']['total']/$value['working_days_for_timeloss']['monthly_working_day']));
					$data['Time Loss Report']['data'][$key]['mwh'] = date('H:i', mktime(0,(24*60*$value['working_days_for_timeloss']['monthly_working_day'] - $value['monthly_loss']['BreakDown']['total']['total'] - $value['monthly_loss']['LossHour']['total']['total'])/$value['working_days_for_timeloss']['monthly_working_day']));
				}else{
					$data['Time Loss Report']['data'][$key]['mbd'] = date('H:i', 0);
					$data['Time Loss Report']['data'][$key]['mlh'] = date('H:i', 0);
					$data['Time Loss Report']['data'][$key]['mwh'] = date('H:i', mktime(0,(24*60)));
				}
				$data['Time Loss Report']['data'][$key]['ybd'] = date('H:i', mktime(0,$value['yearly_loss']['BreakDown']['total']['total']/$value['working_days_for_timeloss']['yearly_working_day']));
				$data['Time Loss Report']['data'][$key]['ylh'] = date('H:i', mktime(0,$value['yearly_loss']['LossHour']['total']['total']/$value['working_days_for_timeloss']['yearly_working_day']));
				$data['Time Loss Report']['data'][$key]['ywh'] = date('H:i', mktime(0,(24*60*$value['working_days_for_timeloss']['yearly_working_day'] - $value['yearly_loss']['BreakDown']['total']['total'] - $value['yearly_loss']['LossHour']['total']['total'])/$value['working_days_for_timeloss']['yearly_working_day']));
			}
			$data['Time Loss Report']['title'] = ['Departments','Last Entry', 'This Month', 'This Year'];
			$data['Time Loss Report']['subtitle'] = ['Break Down','Loss Hour', 'Working Hour'];

			$data['Time Loss Report']['fields'] = ['name','dbd','dlh','dwh','mbd','mlh','mwh','ybd','ylh','ywh'];
			return $data;
    	}
    	// for papertube working hour
    	public function getPaperTubeWorkingReports($date)
    	{
    		// echo '<pre>';
    		// print_r($date);
    		$dates = explode('-', $date);
    		$index = array('Yearly', 'Monthly', 'Daily');
    		$data = array();
    		foreach ($dates as $key => $value) {
    			$where = $this->getTimeWhere($date,$key,'date_np');
    			$working = $this->paper_tube_working_model->getAllReport($where)->row_array();

    			$data[$index[$key]]['hour'] = $working['total_hour'] + floor($working['total_min'] / 60);
    			$data[$index[$key]]['min'] = ($working['total_min'] % 60);
    		}
    		// print_r($data);
    		// exit;
    		return $data;
    	}
}