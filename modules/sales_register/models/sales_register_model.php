<?php
class Sales_register_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array(
            'SALES_REGISTER'=>$this->prefix.'sales_register',
            'BRAND_TYPE'=>$this->prefix.'brand_type',
            'PARTY_NAME'=>$this->prefix.'party_name',
            'BRAND'=>$this->prefix.'brand',
            'PARTY'=>$this->prefix.'party');
		$this->_JOINS=array('BRAND_TYPE'=>array('join_type'=>'LEFT','join_field'=>'sales_registers.type=brand_type.id',
                                           'select'=>'brand_type.name','alias'=>'brand_type'),
                            'PARTY_NAME'=>array('join_type'=>'LEFT','join_field'=>'sales_registers.party_id=party_name.id',
                                           'select'=>'party_name.party_name','alias'=>'party_name'),
                            'BRAND'=>array('join_type'=>'LEFT','join_field'=>'sales_registers.type=brand.id',
                                           'select'=>'brand.brand_name','alias'=>'brand'),
                            'PARTY'=>array('join_type'=>'LEFT','join_field'=>'sales_registers.agent_id=party.id',
                                           'select'=>'party.party as agent_name','alias'=>'party'),
                            );        
    }
    
    public function getSalesRegisters($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''), $fields = NULL)
    {
        if($fields == NULL){
            $fields='sales_registers.*';
        }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['SALES_REGISTER']. ' sales_registers');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['SALES_REGISTER'].' sales_registers');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

    function getBagsCount($date = NULL,$type =  NULL){
        $fields='sales_registers.*, SUM(sales_registers.quantity) as total';

        $dates = explode('-',$date);
        $result = array();

        foreach($this->joins as $key):
            $fields=$fields . ','.$this->_JOINS[$key]['select'];
        endforeach;
        
        $this->db->select($fields);
        $this->db->from($this->_TABLES['SALES_REGISTER']. ' sales_registers');

        foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;  

        if($type != NULL){
            $this->db->where('type',$type);
        }

        $this->db->order_by('date_np');
        $this->db->group_by('month_np');
        if($type != NULL){
            $this->db->group_by('brand_id');
        }

        if($dates[1] < 4){
            $this->db->where('(month_np <= 3 AND year_np = ' . ($dates['0']) . ') OR (month_np > 3 AND year_np = '.($dates['0'] - 1).')', NULL);
        }else{
            $this->db->where('(month_np > 3 AND year_np = ' . ($dates['0']) . ') OR (month_np <= 3 AND year_np = '.($dates['0'] + 1).')', NULL);
        }

        return $this->db->get();
    }

    function getVatCount($date = NULL,$type =  NULL){
        $fields='sales_registers.*, SUM(sales_registers.domestic_sales) as total';

        $dates = explode('-',$date);
        $result = array();

        foreach($this->joins as $key):
            $fields=$fields . ','.$this->_JOINS[$key]['select'];
        endforeach;
        
        $this->db->select($fields);
        $this->db->from($this->_TABLES['SALES_REGISTER']. ' sales_registers');

        foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;  

        $this->db->order_by('date_np');
        $this->db->group_by('month_np');

        if($dates[1] < 4){
            $this->db->where('(month_np <= 3 AND year_np = ' . ($dates['0']) . ') OR (month_np > 3 AND year_np = '.($dates['0'] - 1).')', NULL);
        }else{
            $this->db->where('(month_np > 3 AND year_np = ' . ($dates['0']) . ') OR (month_np <= 3 AND year_np = '.($dates['0'] + 1).')', NULL);
        }
        return $this->db->get();
    }

    public function get_sales_payment_array($id, $process){
      $this->db->where('agent_id', $id);
      $this->db->order_by('date_np, id');
      if($process == 'sales'){
        $this->db->where('domestic_sales >', 0);
      }else{
        $this->db->where('payment >', 0);
      }
      $result = $this->db->get('view_sales_payment');

      return $result;
    }
}