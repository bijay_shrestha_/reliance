<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('sales_register_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
	<form action="" method="post" id="sales_register-search-form">
	<table width="100%" cellspacing="1" cellpadding="1">
	<tr>
	<td><label><?php echo lang('date_np')?></label>:</td>
	<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
	<td><label><?php echo lang('agent_id')?></label></td>
	<td><select class="easyui-combobox" name="search[agent_id]">
		<option value=""></option>
		<?php foreach ($agent as $key => $value) {?>
			<option value="<?php echo $value['id']?>"><?php echo $value['party']?></option>
		<?php }?>
	</select></td>
	</tr>
	<tr>
	<td><label><?php echo lang('party_id')?></label>:</td>
	<td>
		<select class="easyui-combobox" name="search[party_id]">
			<option value=""></option>
			<?php foreach ($party as $key => $value) {?>
				<option value="<?php echo $value['id']?>"><?php echo $value['party_name']?></option>
			<?php }?>
		</select>
	</td>
	<td><label><?php echo lang('truck_no')?></label>:</td>
	<td><input type="text" name="search[truck_no]" id="search_truck_no"  class="easyui-validatebox"/></td>
	</tr>
	<tr>
	<td><label><?php echo lang('type')?></label>:</td>
	<td>
		<select class="easyui-combobox" name="search[type]">
			<option value=""></option>
			<?php foreach ($type as $key => $value) {?>
				<option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
			<?php }?>
		</select>
	<!-- <input type="text" name="search[type]" id="search_type"  class="easyui-numberbox"/> -->
	</td>
	
	<td><label><?php echo lang('domestic_sales')?></label>:</td>
	<td><input type="text" name="search[domestic_sales]" id="search_domestic_sales"  class="easyui-validatebox"/></td>
	</tr>
	<tr>
		<td>
			<label><?php echo lang('voucher_no')?></label>
		</td>
		<td>
			<input type="text" name="search[voucher_no]" id="search_voucher_no" class="easyui-validatebox">
		</td>
	</tr>
	
	  <tr>
	    <td colspan="4">
	    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
	    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
	    </td>
	    </tr>
	</table>
	</form>
</div>
<br/>


<table id="sales_register-table" data-options="pagination:true,title:'<?php  echo lang('sales_register')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
<!-- <th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th> -->
<th data-options="field:'date_np',sortable:true" width="60"><?php echo lang('date_np')?></th>
<th data-options="field:'party_name',sortable:true" width="200"><?php echo lang('party_id')?></th>
<th data-options="field:'agent_name',sortable:true" width="200"><?php echo lang('agent_id')?></th>
<th data-options="field:'voucher_no',sortable:true" width="60"><?php echo lang('voucher_no')?></th>
<th data-options="field:'truck_no',sortable:true" width="60"><?php echo lang('truck_no')?></th>
<th data-options="field:'name',sortable:true" width="50"><?php echo lang('type')?></th>
<!-- <th data-options="field:'quantity',sortable:true" width="50"><?php echo lang('quantity')?></th> -->
<!-- <th data-options="field:'rate',sortable:true" width="50"><?php echo lang('rate')?></th> -->
<th data-options="field:'domestic_sales',sortable:true" width="50"><?php echo lang('domestic_sales')?></th>
<!-- <th data-options="field:'payment',sortable:true" width="50"><?php echo lang('payment')?></th> -->
<!-- <th data-options="field:'payment_left',sortable:true" width="50"><?php echo lang('payment_left')?></th> -->
<!-- <th data-options="field:'days_outstanding',sortable:true" width="50"><?php echo lang('days_outstanding')?></th> -->

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_sales_register')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="createOpening()" title="Add Opening">Add Opening</a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_sales_register')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit sales_register form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-sales_register" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('party_id')?>:</label></td>
					  <td width="66%"><input name="party_id" id="party_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		       			<td width="34%" ><label><?php echo lang('voucher_no')?>:</label></td>
					  <td width="66%"><input name="voucher_no" id="voucher_no" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('truck_no')?>:</label></td>
					  <td width="66%"><input name="truck_no" id="truck_no" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('type')?>:</label></td>
					  <td width="66%"><input name="type" id="type" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('quantity')?>:</label></td>
					  <td width="66%"><input name="quantity" id="quantity" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('rate')?>:</label></td>
					  <td width="66%"><input name="rate" id="rate" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('domestic_sales')?>:</label></td>
					  <td width="66%"><input name="domestic_sales" id="domestic_sales" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('payment')?>:</label></td>
					  <td width="66%"><input name="payment" id="payment" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('payment_left')?>:</label></td>
					  <td width="66%"><input name="payment_left" id="payment_left" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('days_outstanding')?>:</label></td>
					  <td width="66%"><input name="days_outstanding" id="days_outstanding" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->

<div id="dlg1" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons1',modal:true">
    <form id="form-sales_register1" method="post" >
    <table>
					  <input type="hidden" name="date_en" id="date_en1" required="true">
		       <tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np1" class="easyui-validatebox" required="true"></td>
					  <script>
                    $('#date_np1').change(function(){
                          $('#date_en1').val(BS2AD($('#date_np1').val()));
                     });
                </script>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('party_id')?>:</label></td>
					  	<td width="66%">
					  		<select name="party_id" id="party_id1" class="easyui-combobox" required="true">
					  			<option value=""></option>
					  			<?php 
					  			foreach ($party as $key => $value) {?>
					  				<option value="<?php echo $value['id']?>"><?php echo $value['party_name']?></option>
					  			<?php }?>
					  		</select>
				  		</td>
		       </tr>
		       <tr>
		              <td width="34%" ><label><?php echo lang('agent_id')?></label></td>
					  <td width="66%">
					  	<select  name="agent_id" id="agent_id1" class="easyui-combobox" required="true">
					  		<option></option>
					  		<?php 
					  			foreach ($agent as $key => $value) {?>
					  				<option value="<?php echo $value['id']?>"><?php echo $value['party']?></option>
					  			<?php }?>
					  	</select>
					  </td>
		       </tr>
		       <tr>
		              <td width="34%" ><label>Opening:</label></td>
					  <td width="66%"><input name="domestic_sales" id="domestic_sales1" class="easyui-validatebox" required="true"></td>
		       </tr>
		       <input type="hidden" name="voucher_no" id="voucher_no1" value="Last year balance"/>
		       <!-- <input type="hidden" name="id" id="id"/> -->
    </table>
    </form>
	<div id="dlg-buttons1">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save1()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg1').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#sales_register-search-form').form('clear');
			$('#sales_register-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#sales_register-table').datagrid({
				queryParams:{data:$('#sales_register-search-form').serialize()}
				});
		});		
		$('#sales_register-table').datagrid({
			url:'<?php  echo site_url('sales_register/admin/sales_register/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});

		// nepali date picker
        $('#date_np').nepaliDatePicker({
			ndpEnglishInput: 'date_en',
		});
		$('#date_np1').nepaliDatePicker({
			ndpEnglishInput: 'date_en1',
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning btn-xs" iconcls="icon-edit"  title="<?php  echo lang('edit_sales_register')?>"><i class="fa fa-pencil"></i> Edit</a>';
		var d = '<a href="#" onclick="removesales_register('+index+')" class="btn btn-danger btn-xs" iconcls="icon-remove"  title="<?php  echo lang('delete_sales_register')?>"><i class="fa fa-trash"> Delete</i></a>';
		return e+ ' ' +d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-sales_register').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_sales_register')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	
	function createOpening(){
		//Create code here
		$('#form-sales_register').form('clear');
		$('#dlg1').window('open').window('setTitle','Add Opening');
	}

	function edit(index)
	{
		var row = $('#sales_register-table').datagrid('getRows')[index];
		if (row){
			$('#form-sales_register').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_sales_register')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removesales_register(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#sales_register-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('sales_register/admin/sales_register/delete_json')?>', {id:[row.id]}, function(){
					$('#sales_register-table').datagrid('deleteRow', index);
					$('#sales_register-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#sales_register-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('sales_register/admin/sales_register/delete_json')?>',{id:selected},function(data){
						$('#sales_register-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-sales_register').form('submit',{
			url: '<?php  echo site_url('sales_register/admin/sales_register/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-sales_register').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#sales_register-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}

	function save1()
	{
		$('#form-sales_register1').form('submit',{
			url: '<?php  echo site_url('sales_register/admin/sales_register/save1')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-sales_register1').form('clear');
					$('#dlg1').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#sales_register-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>