<?php

class Sales_register extends Member_Controller
{
	var $full_month =  array(
            4 => array(),
            5 => array(),
            6 => array(),
            7 => array(),
            8 => array(),
            9 => array(),
            10 => array(),
            11 => array(),
            12 => array(),
            1 => array(),
            2 => array(),
            3 => array(),
            );
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('sales_register','sales_register_model');
        $this->load->module_model('party_name','party_name_model');
        $this->load->module_model('party','party_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->load->module_model('brand','brand_model');
        $this->load->module_model('party_brand','party_brand_model');
        $this->load->module_model('payment','payment_model');
        $this->load->module_model('party_agent','party_agent_model');

        $this->lang->module_load('sales_register','sales_register');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->sales_register_model->joins = array('BRAND_TYPE', 'PARTY_NAME','PARTY','BRAND');
    $this->db->order_by('date_np asc, voucher_no');
		$data['rows'] = $this->sales_register_model->getSalesRegisters()->result_array();
		$data['parties'] = $this->party_name_model->getPartyNames()->result_array();
		$data['types'] = $this->brand_type_model->getBrandTypes()->result_array();
    $data['assigned_parties'] = $this->party_model->getParties()->result_array();
    $data['brands'] = $this->brand_model->getBrands()->result_array();

    $this->payment_model->joins = array('PARTY_NAME');
    $payment = $this->payment_model->getPayments()->result_array();
    
    $total = array();
    foreach ($payment as $key => $value) {
      if(!array_key_exists($value['assigned_party_id'],$total)){
        $total[$value['assigned_party_id']] = 0;
      }
      $total[$value['assigned_party_id']] += $value['payment'];
    }
    $date = date_create(date('Y-m-d'));
    foreach ($data['rows'] as $key => $value) {
      if(array_key_exists('agent_id',$value)){
        if (array_key_exists($value['agent_id'],$total) && $total[$value['agent_id']] > 0) {
          if($value['domestic_sales'] <= $total[$value['agent_id']]){
            $data['rows'][$key]['payment'] = $value['domestic_sales'];
            $data['rows'][$key]['payment_left'] = 0;
            $total[$value['agent_id']] = $total[$value['agent_id']] - $value['domestic_sales'];
          }else{
            $data['rows'][$key]['payment'] = $total[$value['agent_id']];
            $data['rows'][$key]['payment_left'] = $value['domestic_sales'] - $total[$value['agent_id']];
            $total[$value['agent_id']] = 0;
            $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
          }
        }else{
          $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
          $data['rows'][$key]['payment_left'] = $value['domestic_sales'];
        }
      }else{
         $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
         $data['rows'][$key]['payment_left'] = $value['domestic_sales'];
      }
    }
		// Display Page
		$data['header'] = 'Sales';
		$data['view_page'] = "sales/index";
		$data['module'] = 'sales_register';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->sales_register_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->sales_register_model->getSalesRegisters()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['party_id']!='')?$this->db->where('party_id',$params['search']['party_id']):'';
($params['search']['truck_no']!='')?$this->db->like('truck_no',$params['search']['truck_no']):'';
($params['search']['type']!='')?$this->db->where('type',$params['search']['type']):'';
($params['search']['quantity']!='')?$this->db->where('quantity',$params['search']['quantity']):'';
($params['search']['rate']!='')?$this->db->like('rate',$params['search']['rate']):'';
($params['search']['domestic_sales']!='')?$this->db->like('domestic_sales',$params['search']['domestic_sales']):'';
($params['search']['payment']!='')?$this->db->like('payment',$params['search']['payment']):'';
($params['search']['payment_left']!='')?$this->db->like('payment_left',$params['search']['payment_left']):'';
($params['search']['days_outstanding']!='')?$this->db->where('days_outstanding',$params['search']['days_outstanding']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->sales_register_model->getSalesRegisters()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->sales_register_model->delete('SALES_REGISTER',array('id'=>$row));
            endforeach;
		}else{
        $success = $this->sales_register_model->delete('SALES_REGISTER',array('id'=>$id));
        echo json_encode($success);
    }
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->sales_register_model->insert('SALES_REGISTER',$data);
        }
        else
        {
            $success=$this->sales_register_model->update('SALES_REGISTER',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_en'] = $this->input->post('date_en');
$data['date_np'] = $this->input->post('date_np');

$dates = explode('-', $data['date_np']);
$data['month_np'] = $dates[1];
$data['year_np'] = $dates[0];

$data['party_id'] = $this->input->post('party_id');
$data['truck_no'] = $this->input->post('truck_no');
$data['type'] = $this->input->post('type');
$data['quantity'] = $this->input->post('quantity');
$data['rate'] = $this->input->post('rate');
$data['domestic_sales'] = $this->input->post('domestic_sales');
$data['payment'] = $this->input->post('payment');
$data['payment_left'] = $this->input->post('domestic_sales') - $this->input->post('payment');
$data['days_outstanding'] = $this->input->post('days_outstanding');
$data['voucher_no'] = $this->input->post('voucher_no');
$data['brand_id'] = $this->input->post('brand_id');
$data['agent_id'] = $this->input->post('agent_id');
$data['discount_amount'] = $this->input->post('discount_amount');
$data['primary_rate'] = $this->input->post('primary_rate');

        return $data;
   }
   
   	function get_value(){
   		$where['sales_registers.id'] = $this->input->post('id');

   		$this->sales_register_model->joins = array('BRAND_TYPE', 'PARTY_NAME');
   		$data = $this->sales_register_model->getSalesRegisters($where)->row_array();

   		echo json_encode($data);
   	}

   	function report($date = NULL){
   		if($date == NULL){
   			$data['date_en'] = date('Y-m-d');
   		}else{
   			$data['date_en'] = $date;
   		}
        $data['brand_types'] = $this->brand_type_model->getBrandTypes()->result_array();
        $data['months'] = $this->full_month;
        $this->db->order_by('party');
        $data['agents'] = $this->party_model->getParties()->result_array();
        $this->db->order_by('party_name');
        $data['parties'] = $this->party_name_model->getPartyNames()->result_array();
        exit;
   		// Display Page
		$data['header'] = 'Report';
		$data['view_page'] = "sales/report";
		$data['module'] = 'sales_register';
		$this->load->view($this->_container,$data);	
   	}

   	function getBagReport(){
   		$date = $this->input->post('date_np');
   		$data['types'] = $this->brand_type_model->getBrandTypes()->result_array();
   		$this->sales_register_model->joins = array('BRAND_TYPE');

        $this->db->group_by('type');
        $rows = $this->sales_register_model->getBagsCount($date)->result_array();
        $data['rows'] = $this->full_month;

        foreach($data['types'] as $key => $value){
            foreach($rows as $i => $val){
                if($value['id'] == $val['type']){
                    $data['rows'][$val['month_np']][$value['name']] = $val['total'];
                }
            }
        }

   		$this->load->view('sales/monthly_sales_bags',$data);
   	}

   	function getVatReport(){
   		$date = $this->input->post('date_np');
   		$data['types'] = $this->brand_type_model->getBrandTypes()->result_array();
   		$this->sales_register_model->joins = array('BRAND_TYPE');

   		$this->db->group_by('type');
   		$rows = $this->sales_register_model->getVatCount($date)->result_array();

        $data['rows'] = $this->full_month;

   		foreach($data['types'] as $key => $value){
   			foreach($rows as $i => $val){
   				if($value['id'] == $val['type']){
   					$data['rows'][$val['month_np']][$value['name']] = $val['total'];
   				}
   			}
   		}

   		$this->load->view('sales/monthly_vat',$data);
   	}
    /*
    * for brandwise bags report
    */
    public function getBrandBagReport()
    {
        $date_np = $this->input->post('date_np');
        $brand_type = $this->input->post('brand_type');

        $where['type_id'] = $brand_type;
        $data['brands'] = $this->brand_model->getBrands($where)->result_array();

        $rows = $this->sales_register_model->getBagsCount($date_np,$brand_type)->result_array();
        $data['rows'] = $this->full_month;

        foreach($data['brands'] as $key => $value){
            foreach($rows as $i => $val){
                if($value['id'] == $val['brand_id']){
                    $data['rows'][$val['month_np']][$value['brand_name']] = $val['total'];
                }
            }
        }

        $this->load->view('sales/monthly_brand_bag',$data);
    }

    /*for agent monthly bag report*/
    public function month_agent_report(){
      if($this->input->post('month') != 'All'){
        $where['month_np'] = $this->input->post('month');
      }
      $where['agent_id'] = $this->input->post('agent_id');
      $this->sales_register_model->joins = array('PARTY_NAME','BRAND_TYPE','BRAND');
      $data['rows'] = $this->sales_register_model->getSalesRegisters($where)->result_array();
      $this->load->view('sales/report/agent_month',$data);
    }
    /*for agent monthly bag report*/
    public function month_party_report(){
      if($this->input->post('month') != 'All'){
        $where['month_np'] = $this->input->post('month');
      }
      $where['party_id'] = $this->input->post('party_id');
      $this->sales_register_model->joins = array('BRAND_TYPE','BRAND');
      $data['rows'] = $this->sales_register_model->getSalesRegisters($where)->result_array();
      $this->load->view('sales/report/party_month',$data);
    }

    /*
    * for ledger
    */
    function ledger($type = NULL){
        if($type == NULL){
            $this->db->order_by('party');
            $data['options'] = $this->party_model->getParties()->result_array();
            $data['type'] = 'agent';
        }else if($type = 'party'){
            $this->db->order_by('party_name');
            $data['options'] = $this->party_name_model->getPartyNames()->result_array();
            $data['type'] = $type;
        }
      // Display Page
    $data['header'] = 'Ledger';
    $data['view_page'] = "sales/ledger";
    $data['module'] = 'sales_register';
    $this->load->view($this->_container,$data);
    }

    function getLedgerRecord($type = NULL){
      // echo '<pre>';

        if($type == 'agent'){
            $where['agent_id'] = $this->input->post('party_id');
          $this->sales_register_model->joins = array('BRAND_TYPE','PARTY_NAME','BRAND');
          $this->db->order_by('date_np asc, voucher_no');
          $data['rows'] = $this->sales_register_model->getSalesRegisters($where)->result_array();
          $this->payment_model->joins = array('PARTY');
            $where_agent['assigned_party_id'] = $this->input->post('party_id');
          $payment = $this->payment_model->getPayments($where_agent)->result_array();
        }else if($type == 'party'){
            $where['party_id'] = $this->input->post('party_id');
            $this->sales_register_model->joins = array('BRAND_TYPE','PARTY_NAME','BRAND');
            $this->db->order_by('date_np asc, voucher_no');
            $data['rows'] = $this->sales_register_model->getSalesRegisters($where)->result_array();

            $this->payment_model->joins = array('PARTY_NAME');
            $payment = $this->payment_model->getPayments($where)->result_array();
        }
            
            $total = array();
            // $total = 0;
            // foreach ($payment as $key => $value) {
            //     $total += $value['payment'];
            // }
            $total = array();
              foreach ($payment as $key => $value) {
                if(!array_key_exists($value['assigned_party_id'],$total)){
                  $total[$value['assigned_party_id']] = 0;
                }
                $total[$value['assigned_party_id']] += $value['payment'];
              }

            $date = date_create(date('Y-m-d'));
            // foreach ($data['rows'] as $key => $value) {
            //     if ($total > 0) {
            //         if($value['payment_left'] <= $total){
            //             $data['rows'][$key]['payment'] = $value['payment_left'];
            //             $data['rows'][$key]['payment_left'] = 0;
            //             $total = $total - $value['payment_left'];
            //         }else{
            //             $data['rows'][$key]['payment'] = $total;
            //             $data['rows'][$key]['payment_left'] = $value['payment_left'] - $total;
            //             $total = 0;
            //             $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
            //         }
            //     }else{
            //         $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
            //     }
            // }
            foreach ($data['rows'] as $key => $value) {
              if(array_key_exists('agent_id',$value)){
                if (array_key_exists($value['agent_id'],$total) && $total[$value['agent_id']] > 0) {
                  if($value['domestic_sales'] <= $total[$value['agent_id']]){
                    $data['rows'][$key]['payment'] = $value['domestic_sales'];
                    $data['rows'][$key]['payment_left'] = 0;
                    $total[$value['agent_id']] = $total[$value['agent_id']] - $value['domestic_sales'];
                  }else{
                    $data['rows'][$key]['payment'] = $total[$value['agent_id']];
                    $data['rows'][$key]['payment_left'] = $value['domestic_sales'] - $total[$value['agent_id']];
                    $total[$value['agent_id']] = 0;
                    $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
                  }
                }else{
                  $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
                  $data['rows'][$key]['payment_left'] = $value['domestic_sales'];
                }
              }else{
                 $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
                 $data['rows'][$key]['payment_left'] = $value['domestic_sales'];
              }
            }
            $this->load->view('sales/ledger_detail',$data);

    }

    function getAssignedBrand(){
      $where['type_id'] = $where1['brand_type_id'] = $this->input->post('type_id');
      $data['brands'] = $this->brand_model->getBrands($where)->result_array();

      $this->party_brand_model->joins = array('BRAND');
      $where1['party_id'] = $this->input->post('party_id');
      $data['selected_brand'] = $this->party_brand_model->getPartyBrands($where1)->row_array();

      echo json_encode($data);
    }

    /*
    * tally ledger
    */
    public function tally_ledger(){

        $data['options'] = $this->party_model->getParties()->result_array();
        $data['party_name_options'] = $this->party_name_model->getPartyNames()->result_array();

        // Display Page
        $data['header'] = 'Tally Ledger';
        $data['view_page'] = "sales/tally_ledger/tally_ledger";
        $data['module'] = 'sales_register';
        $this->load->view($this->_container,$data);
    }

    public function get_tally_ledger_detail()
    {
        $id = $this->input->post('party_id');
        $party_name_id = $this->input->post('party_name_id');

        $where_payment = 'WHERE ';
        $where_sales = 'WHERE ';
        if(!$party_name_id){
          $where_payment .= 'p.assigned_party_id = '. $id;
          $where_sales .= 'agent_id = ' . $id;
        }else if(!$id){
          $where_payment .= 'party_id = ' . $party_name_id;
          $where_sales .= 'party_id = ' . $party_name_id;
        }else{
          $where_payment .= 'p.assigned_party_id = '. $id . ' AND party_id = ' . $party_name_id;
          $where_sales .= 'agent_id = ' . $id . ' AND party_id = ' . $party_name_id;
        }

        $sql = 
        // 'SELECT p.id, p.date_np, p.date_en, n.party_name, b.bank_name as note, 0 as bill_amount, p.payment, p.party_id FROM tbl_payment p LEFT JOIN tbl_bank b ON b.id = p.bank_id LEFT JOIN tbl_party_name n ON p.party_id = n.id ' . $where_payment
        // .' UNION '
        // .'SELECT s.id, s.date_np, s.date_en, p.party_name, s.voucher_no as note, s.domestic_sales as bill_amount, 0 as payment, s.party_id FROM tbl_sales_register s LEFT JOIN tbl_party_name p ON s.party_id = p.id '. $where_sales . ' ORDER BY date_np';
        
        'SELECT s.id, s.date_np, s.date_en, p.party_name, s.voucher_no as note, s.domestic_sales as bill_amount, 0 as payment, s.party_id,"" AS bank FROM tbl_sales_register s LEFT JOIN tbl_party_name p ON s.party_id = p.id '. $where_sales . ' ORDER BY date_np';
        $bills_array = $this->db->query($sql)->result_array();
        $data['rows'] = $bills_array;
        
        $payment_sql = 'SELECT p.id, p.date_np, p.date_en, n.party_name, b.bank_name as note, 0 as bill_amount, p.payment, p.party_id FROM tbl_payment p LEFT JOIN tbl_bank b ON b.id = p.bank_id LEFT JOIN tbl_party_name n ON p.party_id = n.id ' . $where_payment . ' ORDER BY party_id';
        $payments = $this->db->query($payment_sql)->result_array();

        $count = count($bills_array);
        if($count > 0){
          foreach($data['rows'] as $key => $value){
            if($value['bill_amount'] != 0){
              $bills[$value['party_id']][] = array(
                'bill' => $value['note'],
                'amount' => $value['bill_amount'],
                );
            }
          }
          
            $i = 0;
            $bill_pay = array();
          foreach ($payments as $key => $value) {
            if(!array_key_exists($value['party_id'], $bill_pay)){
              $bill_pay[$value['party_id']] = array();
            }
            if(array_key_exists($value['party_id'],$bills)){
              $j = count($bills[$value['party_id']]);
            }else{
              $j = 0;
            }
            while($value['payment'] > 0 && $j > $i) {
              if(count($bill_pay[$value['party_id']]) == 0){
                $bill_pay[$value['party_id']] = $bills[$value['party_id']][0];
              }

              // if(array_key_exists($i, $bills[$value['party_id']])){
                if($bill_pay[$value['party_id']]['amount'] == 0){
                  $i++;
                  if(array_key_exists($i, $bills[$value['party_id']])){
                    $bill_pay[$value['party_id']] = $bills[$value['party_id']][$i];
                  }else{
                    $bill_pay[$value['party_id']]['amount'] = 0;
                  }
                }
                if($bill_pay[$value['party_id']]['amount'] > $value['payment']){
                  $payment = $value['payment'];
                  $value['payment'] = 0;
                  $bill_pay[$value['party_id']]['amount'] -= $payment;
                }else if($bill_pay[$value['party_id']]['amount'] != 0){
                  $payment = $bill_pay[$value['party_id']]['amount'];
                  $value['payment'] = $value['payment'] - $bill_pay[$value['party_id']]['amount'];
                  $bill_pay[$value['party_id']]['amount'] = 0;
                }else{
                  $payment = $value['payment'];
                  $value['payment'] = $value['payment'] - $bill_pay[$value['party_id']]['amount'];
                  $bill_pay[$value['party_id']]['amount'] = 0;
                }

                $data['rows'][] = array(
                  'date_np' => $value['date_np'],
                  'date_en' => $value['date_en'],
                  'party_name' => $value['party_name'],
                  'note' => $bill_pay[$value['party_id']]['bill'],
                  'bank' => $value['note'],
                  'bill_amount' => 0,
                  'payment' => $payment,
                  );
              // }
            } 
          }
          // echo '<pre>';
          // print_r($data['rows']);
          // echo '</pre>';
          $this->load->view('sales/tally_ledger/tally_ledger_detail',$data);
        }else{
          echo '<div class="class="alert alert-warning bg-yellow"><h4><i class="icon fa fa-warning"></i> Alert!</h4>
                    No data found.
                  </div></div>';
        }
    }
    /*party tally legder*/
    public function party_tally_ledger()
    {
        $this->db->order_by('party');
       $data['options'] = $this->party_model->getParties()->result_array();
        $data['party_name_options'] = $this->party_name_model->getPartyNames()->result_array();

        // Display Page
        $data['header'] = 'Party Tally Ledger';
        $data['view_page'] = "sales/tally_ledger/party_tally_ledger";
        $data['module'] = 'sales_register';
        $this->load->view($this->_container,$data);
    }
    public function get_party_tally_ledger_detail()
    {
        $id = $this->input->post('party_id');
        $party_name_id = $this->input->post('party_name_id');

        $where_payment = 'WHERE ';
        $where_sales = 'WHERE ';
        if(!$party_name_id){
          $where_payment .= 'p.assigned_party_id = '. $id;
          $where_sales .= 'agent_id = ' . $id;
        }else if(!$id){
          $where_payment .= 'party_id = ' . $party_name_id;
          $where_sales .= 'party_id = ' . $party_name_id;
        }else{
          $where_payment .= 'p.assigned_party_id = '. $id . ' AND party_id = ' . $party_name_id;
          $where_sales .= 'agent_id = ' . $id . ' AND party_id = ' . $party_name_id;
        }

        $this->db->order_by('party_name');
        $this->party_agent_model->joins = array('PARTY_NAME');
        $parties = $this->party_agent_model->getPartyAgents(array('agent_id'=>$id))->result_array();
        // $parties = $this->party_name_model->getPartyNames(array('assigned_party_id'=>$id))->result_array();
        $m = 0;
        $data['rows'] = array();
        foreach ($parties as $key => $value) {
          $p = 0;
          $total['payment'] = 0;
          $total['bill_amount'] = 0;

          $this->db->order_by('date_np');
          $this->sales_register_model->joins = array('BRAND','BRAND_TYPE');
          $bill_amount = $this->sales_register_model->getSalesRegisters(array('party_id'=>$value['id']))->result_array();
          $this->db->order_by('date_np');
          $this->payment_model->joins = array('BANK');
          $payment_amount = $this->payment_model->getPayments(array('party_id'=>$value['id']))->result_array();

          $data_value = array();
          $bill_list = 0;
          foreach ($bill_amount as $index => $val) {
            $data_value[] = array(
              'date_np' => $val['date_np'],
              'date_en' => $val['date_en'],
              'party_name' => $value['party_name'],
              'bill_amount' => $val['domestic_sales'],
              'payment' => 0,
              'note' => $val['voucher_no'],
              'bank_name' => '',
              'truck_no' => $val['truck_no'],
              'brand_name' => $val['brand_name'],
              'type' => $val['name'],
              'quantity' => $val['quantity'],
              'rate' => $val['rate'],
              'month' => $val['month_np'],
              );
                $total['bill_amount'] += $val['domestic_sales']; 
          }
          $bill_count = count($bill_amount);
          if($bill_count > 0){
            $domestic_sales = $bill_amount[$bill_list]['domestic_sales'];
          }
          foreach ($payment_amount as $index => $val) {
            $payment_temp = $val['payment'];
            if(array_key_exists($bill_list, $bill_amount)){
              while ($payment_temp > 0) {
                if($payment_temp <= $domestic_sales){
                  $dates = explode('-', $val['date_np']);
                  $data_value[] = array(
                    'date_np' => $val['date_np'],
                    'date_en' => $val['date_en'],
                    'party_name' => $value['party_name'],
                    'bill_amount' => 0,
                    'payment' => $payment_temp,
                    'note' => $bill_amount[$bill_list]['voucher_no'],
                    'bank_name' => $val['bank_name'],
                    'truck_no' => '',
                    'brand_name' => '',
                    'type' => '',
                    'quantity' => '',
                    'rate' => '',
                    'month' => $dates['1'],
                    );
                  $total['payment'] += $payment_temp; 
                  $domestic_sales = $domestic_sales - $payment_temp;
                  $payment_temp = $payment_temp - $payment_temp;
                }
                if($payment_temp > $domestic_sales){
                  $dates = explode('-', $val['date_np']);
                  $data_value[] = array(
                    'date_np' => $val['date_np'],
                    'date_en' => $val['date_en'],
                    'party_name' => $value['party_name'],
                    'bill_amount' => 0,
                    'note' => $bill_amount[$bill_list]['voucher_no'],
                    'payment' => $domestic_sales,
                    'bank_name' => $val['bank_name'],
                    'truck_no' => '',
                    'brand_name' => '',
                    'type' => '',
                    'quantity' => '',
                    'rate' => '',
                    'month' => $dates['1'],
                    );
                  $total['payment'] += $domestic_sales; 
                  $payment_temp = $payment_temp - $domestic_sales;
                  $domestic_sales = $domestic_sales - $domestic_sales;
                }
                if($domestic_sales <= 0){
                  $bill_list++;
                  if(array_key_exists($bill_list, $bill_amount)){
                    $domestic_sales = $bill_amount[$bill_list]['domestic_sales'];
                  }else{
                    $dates = explode('-', $val['date_np']);
                    $data_value[] = array(
                      'date_np' => $val['date_np'],
                      'date_en' => $val['date_en'],
                      'party_name' => $value['party_name'],
                      'bill_amount' => 0,
                      'note' => '',
                      'payment' => $payment_temp,
                      'brand_name' => '',
                      'type' => '',
                      'quantity' => '',
                      'rate' => '',
                      'month' => $dates['1'],
                      'truck_no' => '',
                      );
                    $total['payment'] += $payment_temp; 
                    break;
                  }
                }
              }
            }else{
              $dates = explode('-', $val['date_np']);
              $data_value[] = array(
                'date_np' => $val['date_np'],
                'date_en' => $val['date_en'],
                'party_name' => $value['party_name'],
                'bill_amount' => 0,
                'note' => '',
                'truck_no' => '',
                'payment' => $payment_temp,
                'brand_name'=>'',
                'type' => '',
                'quantity' => '',
                'rate' => '',
                'month' => $dates['1'],
                );
              $total['payment'] += $payment_temp; 
            }

          }
          // echo '<pre>';
          // print_r($data_value);exit;
          
        //   $start = $m;
        //   $data_value = array();

        //   foreach($bill_amount as $index => $val){
        //     $data_value[$m]['party_name'] = $value['party_name'];
        //     $data_value[$m]['date_np'] = $val['date_np']; 
        //     $data_value[$m]['date_en'] = $val['date_en']; 
        //     $data_value[$m]['note'] = $val['voucher_no']; 
        //     $data_value[$m]['bill_amount'] = $val['domestic_sales']; 
        //     $data_value[$m]['payment'] = 0; 
        //     $total['bill_amount'] += $val['domestic_sales']; 

        //     $bill_record[$m] = array(
        //       'amount'=>$val['domestic_sales'],
        //       'bill_no'=>$val['voucher_no']
        //       );
        //     $m++;
        //   }

        //   $k = $start;
        //   $count = $start + count($bill_record);
        //   foreach ($payment_amount as $index => $val) {
        //     $total['payment'] += $val['payment']; 
        //     $payment = $val['payment'];

        //     if($count > $k){
        //       while($payment > 0 && $count > $k){
        //         $data_value[$m]['party_name'] = $value['party_name'] . '/'. $val['bank_name'];
        //         $data_value[$m]['date_np'] = $val['date_np']; 
        //         $data_value[$m]['date_en'] = $val['date_en']; 
        //         $data_value[$m]['bill_amount'] = 0; 

        //         $data_value[$m]['note'] = $data_value[$k]['note'];
        //         if($payment < $bill_record[$k]['amount']){
        //           $data_value[$m]['payment'] = $payment;
        //           $payment = 0;
        //           $bill_record[$k]['amount'] = $bill_record[$k]['amount'] - $payment;
        //         }else{
        //           echo 'k:'.$k.'<br>count:'.$count.'<br>';
        //           $data_value[$m]['payment'] = $bill_record[$k]['amount']; 
        //           $payment = $payment - $bill_record[$k]['amount'];
        //           $k++;
        //         }
        //         $m++;
        //       }
        //     }
        //     if($count <= $k && $payment > 0){
        //       $data_value[$m]['payment'] = $payment;
        //       $data_value[$m]['party_name'] = $value['party_name'] . '/'. $val['bank_name'];
        //       $data_value[$m]['date_np'] = $val['date_np']; 
        //       $data_value[$m]['date_en'] = $val['date_en']; 
        //       $data_value[$m]['note'] = '';
        //       $data_value[$m]['bill_amount'] = 0; 
        //       $m++;
        //     }
        //     // echo $data_value[$m-1]['note'].'<br>';
        //   }
          $final_temp = count($data_value);
          if($final_temp > 0){
            $data['rows'] = array_merge($data['rows'],$this->order($data_value,0,$data));
            // $data['rows'] = array_merge($data['rows'],$this->order($data_value,$start,$data));
          }
          $data_count = count($data['rows']);

          $data['rows'][$data_count]['party_name'] = '<strong>'.$value['party_name'].'</strong>';
          $data['rows'][$data_count]['date_np'] = ''; 
          $data['rows'][$data_count]['date_en'] = ''; 
          $data['rows'][$data_count]['note'] = '<strong>Total</strong>'; 
          $data['rows'][$data_count]['bill_amount'] = $total['bill_amount']; 
          $data['rows'][$data_count]['payment'] = $total['payment']; 
          $data['rows'][$data_count]['truck_no'] = ''; 
          $data['rows'][$data_count]['brand_name'] = ''; 
          $data['rows'][$data_count]['type'] = ''; 
          $data['rows'][$data_count]['quantity'] = ''; 
          $data['rows'][$data_count]['rate'] = ''; 
          $data['rows'][$data_count]['month'] = ''; 
          // $m++;
        }
          $this->load->view('sales/tally_ledger/party_tally_ledger_detail',$data);

    }

    public function order($data_value,$start)
    {
      for($i = $start; $i <= $start+count($data_value) - 1 ; $i++){
        for($j = $i+1; $j <= $start+count($data_value) - 1; $j++){
          $date1 = strtotime($data_value[$i]['date_en']);
          $date2 = strtotime($data_value[$j]['date_en']);
          if($date1 > $date2 && $date1 != $date2){
            $temp = $data_value[$i];
            $data_value[$i] = $data_value[$j];
            $data_value[$j] = $temp;
          }
        }
      }
      return $data_value;
    }
      
}