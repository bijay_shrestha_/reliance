<?php

class Sales_register extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('sales_register','sales_register_model');
        $this->load->module_model('party_name','party_name_model');
        $this->load->module_model('party','party_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->lang->module_load('sales_register','sales_register');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['party'] = $this->party_name_model->getPartyNames()->result_array();
		$data['agent'] = $this->party_model->getParties()->result_array();
		$data['type'] = $this->brand_type_model->getBrandTypes()->result_array();
// print_r($data['agent']);exit;
		// Display Page
		$data['header'] = 'sales_register';
		$data['page'] = $this->config->item('template_admin') . "sales_register/index";
		$data['module'] = 'sales_register';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->sales_register_model->count();
		paging('id');
		$this->_get_search_param();	
		$this->sales_register_model->joins = array('PARTY','PARTY_NAME','BRAND_TYPE');
		$rows=$this->sales_register_model->getSalesRegisters()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['party_id']!='')?$this->db->where('party_id',$params['search']['party_id']):'';
($params['search']['agent_id']!='')?$this->db->where('agent_id',$params['search']['agent_id']):'';
($params['search']['truck_no']!='')?$this->db->like('truck_no',$params['search']['truck_no']):'';
($params['search']['type']!='')?$this->db->where('type',$params['search']['type']):'';
// ($params['search']['quantity']!='')?$this->db->where('quantity',$params['search']['quantity']):'';
// ($params['search']['rate']!='')?$this->db->like('rate',$params['search']['rate']):'';
($params['search']['domestic_sales']!='')?$this->db->like('domestic_sales',$params['search']['domestic_sales']):'';
// ($params['search']['payment']!='')?$this->db->like('payment',$params['search']['payment']):'';
// ($params['search']['payment_left']!='')?$this->db->like('payment_left',$params['search']['payment_left']):'';
// ($params['search']['days_outstanding']!='')?$this->db->where('days_outstanding',$params['search']['days_outstanding']):'';
($params['search']['voucher_no']!='')?$this->db->like('voucher_no',$params['search']['voucher_no']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->sales_register_model->getSalesRegisters()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->sales_register_model->delete('SALES_REGISTER',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->sales_register_model->insert('SALES_REGISTER',$data);
        }
        else
        {
            $success=$this->sales_register_model->update('SALES_REGISTER',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_en'] = $this->input->post('date_en');
$data['date_np'] = $this->input->post('date_np');
$data['party_id'] = $this->input->post('party_id');
$data['truck_no'] = $this->input->post('truck_no');
$data['type'] = $this->input->post('type');
$data['quantity'] = $this->input->post('quantity');
$data['rate'] = $this->input->post('rate');
$data['domestic_sales'] = $this->input->post('domestic_sales');
$data['payment'] = $this->input->post('payment');
$data['payment_left'] = $this->input->post('payment_left');
$data['days_outstanding'] = $this->input->post('days_outstanding');

        return $data;
   }
   
   	public function save1(){
   		$data = $this->input->post();
   		$dates = explode('-', $data['date_np']);
   		$data['month_np'] = $dates['1'];
   		$data['year_np'] = $dates['0'];
// print_r($this->input->post());exit;
   		$success = $this->sales_register_model->insert('SALES_REGISTER',$data);

		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		  echo json_encode(array('msg'=>$msg,'success'=>$success));		
   	}
	    
}