<?php

class Sales_register extends Member_Controller
{
	var $full_month =  array(
            4 => array(),
            5 => array(),
            6 => array(),
            7 => array(),
            8 => array(),
            9 => array(),
            10 => array(),
            11 => array(),
            12 => array(),
            1 => array(),
            2 => array(),
            3 => array(),
            );
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('sales_register','sales_register_model');
        $this->load->module_model('party_name','party_name_model');
        $this->load->module_model('party','party_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->load->module_model('brand','brand_model');
        $this->load->module_model('party_brand','party_brand_model');
        $this->load->module_model('payment','payment_model');
        $this->load->module_model('party_agent','party_agent_model');

        $this->lang->module_load('sales_register','sales_register');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->sales_register_model->joins = array('BRAND_TYPE', 'PARTY_NAME','PARTY','BRAND');
    $this->db->order_by('date_np asc, voucher_no');
		$data['rows'] = $this->sales_register_model->getSalesRegisters()->result_array();
		$data['parties'] = $this->party_name_model->getPartyNames()->result_array();
		$data['types'] = $this->brand_type_model->getBrandTypes()->result_array();
    $this->db->order_by('party');
    $data['assigned_parties'] = $this->party_model->getParties()->result_array();
    $data['brands'] = $this->brand_model->getBrands()->result_array();

    $this->payment_model->joins = array('PARTY_NAME');
    $payment = $this->payment_model->getPayments()->result_array();
    
    $total = array();
    foreach ($payment as $key => $value) {
      if(!array_key_exists($value['assigned_party_id'],$total)){
        $total[$value['assigned_party_id']] = 0;
      }
      $total[$value['assigned_party_id']] += $value['payment'];
    }
    $date = date_create(date('Y-m-d'));
    foreach ($data['rows'] as $key => $value) {
      if(array_key_exists('agent_id',$value)){
        if (array_key_exists($value['agent_id'],$total) && $total[$value['agent_id']] > 0) {
          if($value['domestic_sales'] <= $total[$value['agent_id']]){
            $data['rows'][$key]['payment'] = $value['domestic_sales'];
            $data['rows'][$key]['payment_left'] = 0;
            $total[$value['agent_id']] = $total[$value['agent_id']] - $value['domestic_sales'];
          }else{
            $data['rows'][$key]['payment'] = $total[$value['agent_id']];
            $data['rows'][$key]['payment_left'] = $value['domestic_sales'] - $total[$value['agent_id']];
            $total[$value['agent_id']] = 0;
            $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
          }
        }else{
          $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
          $data['rows'][$key]['payment_left'] = $value['domestic_sales'];
        }
      }else{
         $data['rows'][$key]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
         $data['rows'][$key]['payment_left'] = $value['domestic_sales'];
      }
    }
		// Display Page
		$data['header'] = 'Sales';
		$data['view_page'] = "sales/index";
		$data['module'] = 'sales_register';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->sales_register_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->sales_register_model->getSalesRegisters()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['party_id']!='')?$this->db->where('party_id',$params['search']['party_id']):'';
($params['search']['truck_no']!='')?$this->db->like('truck_no',$params['search']['truck_no']):'';
($params['search']['type']!='')?$this->db->where('type',$params['search']['type']):'';
($params['search']['quantity']!='')?$this->db->where('quantity',$params['search']['quantity']):'';
($params['search']['rate']!='')?$this->db->like('rate',$params['search']['rate']):'';
($params['search']['domestic_sales']!='')?$this->db->like('domestic_sales',$params['search']['domestic_sales']):'';
($params['search']['payment']!='')?$this->db->like('payment',$params['search']['payment']):'';
($params['search']['payment_left']!='')?$this->db->like('payment_left',$params['search']['payment_left']):'';
($params['search']['days_outstanding']!='')?$this->db->where('days_outstanding',$params['search']['days_outstanding']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->sales_register_model->getSalesRegisters()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->sales_register_model->delete('SALES_REGISTER',array('id'=>$row));
            endforeach;
		}else{
        $success = $this->sales_register_model->delete('SALES_REGISTER',array('id'=>$id));
        echo json_encode($success);
    }
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->sales_register_model->insert('SALES_REGISTER',$data);
        }
        else
        {
            $success=$this->sales_register_model->update('SALES_REGISTER',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_en'] = $this->input->post('date_en');
$data['date_np'] = $this->input->post('date_np');

$dates = explode('-', $data['date_np']);
$data['month_np'] = $dates[1];
$data['year_np'] = $dates[0];

$data['party_id'] = $this->input->post('party_id');
$data['truck_no'] = $this->input->post('truck_no');
$data['type'] = $this->input->post('type');
$data['quantity'] = $this->input->post('quantity');
$data['rate'] = $this->input->post('rate');
$data['domestic_sales'] = $this->input->post('domestic_sales');
$data['payment'] = $this->input->post('payment');
$data['payment_left'] = $this->input->post('domestic_sales') - $this->input->post('payment');
$data['days_outstanding'] = $this->input->post('days_outstanding');
$data['voucher_no'] = $this->input->post('voucher_no');
$data['brand_id'] = $this->input->post('brand_id');
$data['agent_id'] = $this->input->post('agent_id');
$data['discount_amount'] = $this->input->post('discount_amount');
$data['primary_rate'] = $this->input->post('primary_rate');

        return $data;
   }
   
   	function get_value(){
   		$where['sales_registers.id'] = $this->input->post('id');

   		$this->sales_register_model->joins = array('BRAND_TYPE', 'PARTY_NAME');
   		$data = $this->sales_register_model->getSalesRegisters($where)->row_array();

   		echo json_encode($data);
   	}

   	function report($date = NULL){
   		if($date == NULL){
   			$data['date_en'] = date('Y-m-d');
   		}else{
   			$data['date_en'] = $date;
   		}
        $data['brand_types'] = $this->brand_type_model->getBrandTypes()->result_array();
        $data['months'] = $this->full_month;
        $this->db->order_by('party');
        $data['agents'] = $this->party_model->getParties()->result_array();
        $this->db->order_by('party_name');
        $data['parties'] = $this->party_name_model->getPartyNames()->result_array();
   		// Display Page
		$data['header'] = 'Report';
		$data['view_page'] = "sales/report";
		$data['module'] = 'sales_register';
		$this->load->view($this->_container,$data);	
   	}

   	function getBagReport(){
   		$date = $this->input->post('date_np');
   		$data['types'] = $this->brand_type_model->getBrandTypes()->result_array();
   		$this->sales_register_model->joins = array('BRAND_TYPE');

        $this->db->group_by('type');
        $rows = $this->sales_register_model->getBagsCount($date)->result_array();
        $data['rows'] = $this->full_month;

        foreach($data['types'] as $key => $value){
            foreach($rows as $i => $val){
                if($value['id'] == $val['type']){
                    $data['rows'][$val['month_np']][$value['name']] = $val['total'];
                }
            }
        }

   		$this->load->view('sales/monthly_sales_bags',$data);
   	}

   	function getVatReport(){
   		$date = $this->input->post('date_np');
   		$data['types'] = $this->brand_type_model->getBrandTypes()->result_array();
   		$this->sales_register_model->joins = array('BRAND_TYPE');

   		$this->db->group_by('type');
   		$rows = $this->sales_register_model->getVatCount($date)->result_array();

        $data['rows'] = $this->full_month;

   		foreach($data['types'] as $key => $value){
   			foreach($rows as $i => $val){
   				if($value['id'] == $val['type']){
   					$data['rows'][$val['month_np']][$value['name']] = $val['total'];
   				}
   			}
   		}

   		$this->load->view('sales/monthly_vat',$data);
   	}
    /*
    * for brandwise bags report
    */
    public function getBrandBagReport()
    {
        $date_np = $this->input->post('date_np');
        $brand_type = $this->input->post('brand_type');

        $where['type_id'] = $brand_type;
        $data['brands'] = $this->brand_model->getBrands($where)->result_array();

        $rows = $this->sales_register_model->getBagsCount($date_np,$brand_type)->result_array();
        $data['rows'] = $this->full_month;

        foreach($data['brands'] as $key => $value){
            foreach($rows as $i => $val){
                if($value['id'] == $val['brand_id']){
                    $data['rows'][$val['month_np']][$value['brand_name']] = $val['total'];
                }
            }
        }

        $this->load->view('sales/monthly_brand_bag',$data);
    }

    /*for agent monthly bag report*/
    public function month_agent_report(){
      if($this->input->post('month') != 'All'){
        $where['month_np'] = $this->input->post('month');
      }
      $where['agent_id'] = $this->input->post('agent_id');
      $this->sales_register_model->joins = array('PARTY_NAME','BRAND_TYPE','BRAND');
      $data['rows'] = $this->sales_register_model->getSalesRegisters($where)->result_array();
      $this->load->view('sales/report/agent_month',$data);
    }
    /*for agent monthly bag report*/
    public function month_party_report(){
      if($this->input->post('month') != 'All'){
        $where['month_np'] = $this->input->post('month');
      }
      $where['party_id'] = $this->input->post('party_id');
      $this->sales_register_model->joins = array('BRAND_TYPE','BRAND');
      $data['rows'] = $this->sales_register_model->getSalesRegisters($where)->result_array();
      $this->load->view('sales/report/party_month',$data);
    }

    /*
    * for ledger
    */
    function ledger($type = NULL){
        if($type == NULL){
            $this->db->order_by('party');
            $data['options'] = $this->party_model->getParties()->result_array();
            $data['type'] = 'agent';
        }else if($type = 'party'){
            $this->db->order_by('party_name');
            $data['options'] = $this->party_name_model->getPartyNames()->result_array();
            $data['type'] = $type;
        }
      // Display Page
    $data['header'] = 'Ledger';
    $data['view_page'] = "sales/ledger";
    $data['module'] = 'sales_register';
    $this->load->view($this->_container,$data);
    }

    function getLedgerRecord($type = NULL){

        if($type == 'agent'){
          $id = $this->input->post('party_id');
          $rows = $this->get_sales_payment_detail($id);
        }else if($type == 'party'){
          $where['party_id'] = $this->input->post('party_id');
          $agent_ids = $this->party_agent_model->getPartyAgents($where)->result_array();
          $rows = array();
          foreach ($agent_ids as $key => $value) {
            $temp = $this->get_sales_payment_detail($value['agent_id'],$where['party_id']);
            $rows = array_merge($rows,$temp);
          }
           
        }
        // echo '<pre>';print_r($rows);
        $date = date_create(date('Y-m-d'));
        $data['rows'] = array();
        foreach ($rows as $key => $value) {
          // echo '<pre>';print_r($value);
          $voucher_no = $value['voucher_no'];
          if(!array_key_exists($voucher_no,$data['rows'])){
            $data['rows'][$voucher_no] = $value;
            $data['rows'][$voucher_no]['payment_left'] = $value['domestic_sales'] - $value['payment'];
            if($data['rows'][$voucher_no]['payment_left'] > 0){
              $data['rows'][$voucher_no]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
            }else{
              $data['rows'][$voucher_no]['days_outstanding'] = 0;
            }
          }else{
            if($value['payment'] == 0){
              $data['rows'][$voucher_no]['date_np'] = $value['date_np'];
              $data['rows'][$voucher_no]['date_en'] = $value['date_en'];
              $data['rows'][$voucher_no]['month_np'] = $value['month_np'];
              $data['rows'][$voucher_no]['year_np'] = $value['year_np'];
            }
            $data['rows'][$voucher_no]['payment'] += $value['payment'];
            $data['rows'][$voucher_no]['domestic_sales'] += $value['domestic_sales'];
            $data['rows'][$voucher_no]['payment_left'] -= $value['payment'];
            // echo $data['rows'][$voucher_no]['payment_left'] != 0;
            if($data['rows'][$voucher_no]['payment_left'] > 0){
              $data['rows'][$voucher_no]['days_outstanding'] = date_diff($date,date_create($value['date_en']))->days;
            }else{
              $data['rows'][$voucher_no]['days_outstanding'] = 0;
            }

          }
        }
        // exit;
            $this->load->view('sales/ledger_detail',$data);

    }

    function getAssignedBrand(){
      $where['type_id'] = $where1['brand_type_id'] = $this->input->post('type_id');
      $data['brands'] = $this->brand_model->getBrands($where)->result_array();

      $this->party_brand_model->joins = array('BRAND');
      $where1['party_id'] = $this->input->post('party_id');
      $data['selected_brand'] = $this->party_brand_model->getPartyBrands($where1)->row_array();

      echo json_encode($data);
    }

    /*
    * tally ledger
    */
    public function tally_ledger(){

        $this->db->order_by('party');
        $data['options'] = $this->party_model->getParties()->result_array();
        $this->db->order_by('party_name');
        $data['party_name_options'] = $this->party_name_model->getPartyNames()->result_array();

        // Display Page
        $data['header'] = 'Tally Ledger';
        $data['view_page'] = "sales/tally_ledger/tally_ledger";
        $data['module'] = 'sales_register';
        $this->load->view($this->_container,$data);
    }

    public function get_tally_ledger_detail()
    {
        $id = $this->input->post('party_id'); // agent id
        $party_name_id = $this->input->post('party_name_id'); // party id

        $data['rows'] = $this->get_sales_payment_detail($id, $party_name_id);

        if(count($data) > 0){
          $this->load->view('sales/tally_ledger/tally_ledger_detail',$data);
        }else{
          echo '<div class="class="alert alert-warning bg-yellow"><h4><i class="icon fa fa-warning"></i> Alert!</h4>
                    No data found.
                  </div></div>';
        }
    }
    /*party tally legder*/
    public function party_tally_ledger()
    {
        $this->db->order_by('party');
       $data['options'] = $this->party_model->getParties()->result_array();
        $data['party_name_options'] = $this->party_name_model->getPartyNames()->result_array();

        // Display Page
        $data['header'] = 'Party Tally Ledger';
        $data['view_page'] = "sales/tally_ledger/party_tally_ledger";
        $data['module'] = 'sales_register';
        $this->load->view($this->_container,$data);
    }
    public function get_party_tally_ledger_detail()
    {
        $id = $this->input->post('party_id');
        $party_name_id = $this->input->post('party_name_id');
        $data['rows'] = array();
        $rows = $this->get_sales_payment_detail($id, $party_name_id);

        if(count($rows) > 0){
          $sort = array();
          foreach($rows as $k=>$v) {
              $sort['party_id'][$k] = $v['party_id'];
              $sort['date_en'][$k] = $v['date_en'];
          }
          if(count($sort)> 0){
            array_multisort($sort['party_id'], SORT_ASC, $sort['date_en'], SORT_ASC,$rows);
          }

          $temp_domestic_sales = 0;
          $temp_payment = 0;
          $party_name = $rows[0]['party'];
          $data['rows'] = array();

          foreach($rows as $key => $value){
            if($value['party'] != $party_name){
              $data['rows'][] = array(
                'party'           => $party_name,
                'domestic_sales'  => $temp_domestic_sales,
                'payment'         => $temp_payment,
                'date_np'         => '',
                'date_en'         => '',
                'voucher_no'      => '<strong>Total</strong>',
                'truck_no'        => '',
                'brand_name'      => '',
                'type'            => '',
                'quantity'        => '',
                'rate'            => '',
                'month_np'        => '',
                'bank_name'       => '',
                'brand_name'      => '',
                'type_name'       => '',
                );
              $temp_domestic_sales = 0;
              $temp_payment = 0;
              $party_name = $value['party'];
            }
            $temp_domestic_sales += $value['domestic_sales'];
            $temp_payment += $value['payment'];
            $data['rows'][] = $value;
          }
          $data['rows'][] = array( //for final total
                'party'           => $party_name,
                'domestic_sales'  => $temp_domestic_sales,
                'payment'         => $temp_payment,
                'date_np'         => '',
                'date_en'         => '',
                'voucher_no'      => '<strong>Total</strong>',
                'truck_no'        => '',
                'brand_name'      => '',
                'type'            => '',
                'quantity'        => '',
                'rate'            => '',
                'month_np'        => '',
                'bank_name'       => '',
                'brand_name'      => '',
                'type_name'       => '',
                );
            }

        $this->load->view('sales/tally_ledger/party_tally_ledger_detail',$data);

    }

    public function order($data_value,$start)
    {
      for($i = $start; $i <= $start+count($data_value) - 1 ; $i++){
        for($j = $i+1; $j <= $start+count($data_value) - 1; $j++){
          $date1 = strtotime($data_value[$i]['date_en']);
          $date2 = strtotime($data_value[$j]['date_en']);
          if($date1 > $date2 && $date1 != $date2){
            $temp = $data_value[$i];
            $data_value[$i] = $data_value[$j];
            $data_value[$j] = $temp;
          }
        }
      }
      return $data_value;
    }

    private function get_sales_payment_detail($agent_id, $party_id = NULL){
      $payment = $this->sales_register_model->get_sales_payment_array($agent_id, 'payment')->result_array();
      $payment_count = count($payment);
      if($payment_count > 0){ // check payment is done or not
        if($payment['0']['party_id'] == 0){ // check if payment is done according to party or agent (payment according to agent only)
          $sales = $this->sales_register_model->get_sales_payment_array($agent_id, 'sales')->result_array();
          $agent_sales = $this->get_sales($sales,'agent_id');
          
          $ledger = $this->get_sales_payment_ledger($agent_sales,$payment,$party_id);
          $data = array_merge($sales,$ledger);

        }else{ // (payment according to party)
          $sales = $this->sales_register_model->get_sales_payment_array($agent_id, 'sales')->result_array();
          if($party_id == ''){
            $required_sales = $sales;
          }else{
            $this->db->where('party_id', $party_id);
            $required_sales = $this->sales_register_model->get_sales_payment_array($agent_id, 'sales')->result_array();
          }
          // sales array according to party
          $party_sales = $this->get_sales($sales,'party_id');
          $ledger = $this->get_sales_payment_ledger($party_sales,$payment,$party_id);
          // print_r($ledger);
          $data = array_merge($required_sales, $ledger);
        }
      }else{
        if($party_id != ''){
            $this->db->where('party_id', $party_id);
        }
        $sales = $this->sales_register_model->get_sales_payment_array($agent_id, 'sales')->result_array();
        $data = $sales;
      }

      // print_r($payment);
      return $data;
    }

    // sales according to index
    private function get_sales($sales, $index)
    {
      $party_sales = array();
      foreach ($sales as $key => $value) {
        $party_id = $value[$index];
        $party_sales[$party_id][] = $value;
      }
      return $party_sales;
    }

    private function get_sales_payment_ledger($sales,$payment,$required_party_id = NULL){
      $temp = array();
      $data = array();

      foreach ($sales as $key => $value) {
        $temp[$key]['amt'] = $value[0]['domestic_sales'];
        $temp[$key]['voucher_no'] = $value[0]['voucher_no'];
        $temp[$key]['index'] = 0;
      }
      foreach ($payment as $key => $value) {
        if($value['party_id'] != 0){
          $party_id = $value['party_id'];
        }else{
          $party_id = $value['agent_id'];
        }
        if(!array_key_exists($party_id, $temp)){
          $temp[$party_id] = array(
            'amt' => 0,
            'voucher_no' => 0,
            'index' => 0
          );
        }
        if($value['payment'] <= $temp[$party_id]['amt']) // if payment is less than equal to sales
        {
          $value['voucher_no'] = $temp[$party_id]['voucher_no'];
          $temp[$party_id]['amt'] -= $value['payment'];
          if($temp[$party_id]['amt'] == 0) // if payment is equal to sales
          {
            $next_index = $temp[$party_id]['index'] + 1; // next index 
            $temp[$party_id]['index'] = $next_index;
            if(array_key_exists($next_index, $sales[$party_id])){
              $temp[$party_id]['amt'] = $sales[$party_id][$next_index]['domestic_sales']; // next sales amount
              $temp[$party_id]['voucher_no'] = $sales[$party_id][$next_index]['voucher_no']; // next voucher_no
            }else{
              $temp[$party_id]['amt'] = 0; // next sales amount
              $temp[$party_id]['voucher_no'] = 0; // next voucher_no
            }
          }
          if($required_party_id == ''){
            $data[] = $value;
          }else{
            if($value['party_id'] == $required_party_id){
              $data[] = $value;
            }
          }
        }
        elseif($temp[$party_id]['voucher_no'] == 0) // if no more sales
        {
          if($required_party_id == ''){
            $data[] = $value;
          }else{
            if($value['party_id'] == $required_party_id){
              $data[] = $value;
            }
          }
        }
        else // if payment is greater than sales
        {
          $i = 0;
          while($value['payment'] > 0){

            if($value['payment'] > $temp[$party_id]['amt']){
              $temp_data = $value;
              $temp_data['voucher_no'] = $temp[$party_id]['voucher_no'];
              $temp_data['payment'] = $temp[$party_id]['amt'];
              // $data[] = $temp_data;

              if($required_party_id == ''){
                $data[] = $temp_data;
              }else{
                if($temp_data['party_id'] == $required_party_id){
                  $data[] = $temp_data;
                }
              }
              $value['payment'] = $value['payment'] - $temp[$party_id]['amt'];
              $next_index = $temp[$party_id]['index'] + 1; // next index 
              $temp[$party_id]['index'] = $next_index;
              if(array_key_exists($next_index, $sales[$party_id])){
                $temp[$party_id]['amt'] = $sales[$party_id][$next_index]['domestic_sales']; // next sales amount
                $temp[$party_id]['voucher_no'] = $sales[$party_id][$next_index]['voucher_no']; // next voucher_no
              }else{
                if($required_party_id == ''){
                  $data[] = $value;
                }else{
                  if($temp_data['party_id'] == $required_party_id){
                    $data[] = $value;
                  }
                }
                $temp[$party_id]['amt'] = 0;
                $temp[$party_id]['voucher_no'] = 0;
                break;
              }
            }
            else{
              $temp_data = $value;
              $temp_data['voucher_no'] = $temp[$party_id]['voucher_no'];
              if($required_party_id == ''){
                $data[] = $temp_data;
              }else{
                if($temp_data['party_id'] == $required_party_id){
                  $data[] = $temp_data;
                }
              }
              $temp[$party_id]['amt'] = $temp[$party_id]['amt'] - $value['payment'];
              $value['payment'] = 0;
            }

          }
        }
      }
      return $data;
    }

    public function test()
    {
      $fruits[0]["fruit"] = "lemons";
      $fruits[1]["fruit"] = "apples";
      $fruits[2]["fruit"] = "grapes";

      usort($fruits, function($a,$b){
        return strcmp($a['fruit'],$b['fruit']);
      });

      while (list($key, $value) = each($fruits)) {
          echo "\$fruits[$key]: " . $value["fruit"] . "\n";
      }
    }
      
}