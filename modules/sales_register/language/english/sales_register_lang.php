<?php


$lang['id'] = 'Id';
$lang['date_en'] = 'Date En';
$lang['date_np'] = 'Date';
$lang['party_id'] = 'Party';
$lang['agent_id'] = 'Agent';
$lang['truck_no'] = 'Truck No';
$lang['type'] = 'Type';
$lang['quantity'] = 'Quantity';
$lang['rate'] = 'Rate';
$lang['domestic_sales'] = 'Domestic Sales';
$lang['payment'] = 'Payment';
$lang['payment_left'] = 'Payment Left';
$lang['days_outstanding'] = 'Days Outstanding';

$lang['create_sales_register']='Create Sales Register';
$lang['edit_sales_register']='Edit Sales Register';
$lang['delete_sales_register']='Delete Sales Register';
$lang['sales_register_search']='Sales Register Search';

$lang['sales_register']='Sales Register';
$lang['voucher_no']='Voucher Number';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

