<?php

class Category extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->module_model('category','category_model');
		$this->lang->module_load('category','category');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index($department = NULL)
	{
		// Display Page
		$data['header'] = 'category';
		$data['page'] = $this->config->item('template_admin') . "category/index";
		$data['module'] = 'category';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->category_model->count();
		paging('category_id');
		$this->_get_search_param();	
		$rows=$this->category_model->getCategories()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['category_name']!='')?$this->db->like('category_name',$params['search']['category_name']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}


	}



	public function combo_json()
	{
		$rows=$this->category_model->getCategories()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->category_model->delete('CATEGORY',array('category_id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('category_id'))
        {
        	$success=$this->category_model->insert('CATEGORY',$data);
        }
        else
        {
        	$success=$this->category_model->update('CATEGORY',$data,array('category_id'=>$data['category_id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$data['category_id'] = $this->input->post('category_id');
    	$data['category_name'] = strtoupper($this->input->post('category_name'));

    	return $data;
    }

    public function combobox_category_id()
    {
    	$this->db->order_by('category_name');
    	$rows = $this->category_model->getCategories(array())->result_array();
    	echo json_encode($rows);
    }



}