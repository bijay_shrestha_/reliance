<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('category_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
			<form action="" method="post" id="category-search-form">
				<table class="table">
					<tr><div class="col-md-2"><label><?php echo lang('category_name')?></label>:</div>
						<div class="col-md-10"><input type="text" name="search[category_name]" id="search_category_name"  class="easyui-validatebox form-control"/></div>
					</tr>
					<tr>
						<td >
							<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
							<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
						</td>
					</tr>
				</table>

			</form>
		</div>
		<br/>
		<!-- <div class="btn-group">
			<a class="btn btn-primary" id="category_btn">Create Category</a>
			<a class="btn btn-default" id="assign_btn">Assign Category</a>
		</div> -->

		<div id="category_table">
			<table id="category-table" data-options="pagination:true,title:'<?php  echo lang('category')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
				<thead>
					<th data-options="field:'checkbox',checkbox:true"></th>
					<!-- <th data-options="field:'category_id',sortable:true" width="30"><?php echo lang('category_id')?></th> -->
					<th data-options="field:'category_name',sortable:true" width="80%"><?php echo lang('category_name')?></th>

					<th field="action" width="12%" formatter="getActions"><?php  echo lang('action')?></th>
				</thead>
			</table>
		</div>

		<div id="assign_table" hidden>
			<table id="assign-table" data-options="pagination:true,title:'<?php  echo "Assign Category"?>',pagesize:'20',toolbar:'#toolbar_assign',rownumbers:true,collapsible:true,fitColumns:true">
				<thead>
					<th data-options="field:'checkbox',checkbox:true"></th>
					<!-- <th data-options="field:'id',sortable:true" width="30"><?php echo "id"?></th> -->
					<th data-options="field:'category_name',sortable:true,width:'auto'"  ><?php echo lang('category_name')?></th>
					<th data-options="field:'department_name',sortable:true,width:'auto'" ><?php echo "Department Name"?></th>

					<th field="action" formatter="getAssign_Actions"><?php  echo lang('action')?></th>
				</thead>
			</table>
		</div>

		<div id="toolbar" style="padding:5px;height:auto">
			<p>
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_category')?>"><?php  echo lang('create')?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected(0)"  title="<?php  echo lang('delete_category')?>"><?php  echo lang('remove_selected')?></a>
			</p>
		</div> 

		<div id="toolbar_assign" style="padding:5px;height:auto">
			<p>
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="assign()" title="<?php  echo "Assign"?>"><?php  echo "Asign"?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected(1)"  title="<?php  echo lang('delete_category')?>"><?php  echo lang('remove_selected')?></a>
			</p>
		</div> 

		<!--for create and edit category form-->
		<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
		data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
		<form id="form-category" method="post" >
			<table>
				<tr>
					<td width="34%" ><label><?php echo lang('category_name')?>:</label></td>
					<td width="66%"><input name="category_name" id="category_name" class="easyui-validatebox" required="true"></td>
				</tr><input type="hidden" name="category_id" id="category_id"/>
			</table>
		</form>
		<div id="dlg-buttons">
			<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save(0)"><?php  echo  lang('general_save')?></a>
			<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
		</div>    
	</div>
	<!--div ends-->

	<!--for create and edit category_department form-->
	<div id="dlg2" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
	data-options="closed:true,collapsible:true,buttons:'#dlg2-buttons',modal:true">
	<form id="form-category_department" method="post" >
		<table class="table">
			<tr>
				<td width="34%" ><label><?php echo "Category Id"?>:</label></td>
				<td width="66%"><input name="category_id" id="category_id_assign" class="easyui-combobox" required="true"></td>
			</tr>
			<tr>
				<td width="34%" ><label><?php echo "Department ID"?>:</label></td>
				<td width="66%"><input name="department_id" id="department_id" class="easyui-combobox" required="true"></td>
			</tr>
			<input type="hidden" name="id" id="id"/>
		</table>
	</form>
	<div id="dlg2-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save(1)"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg2').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->

</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#category-search-form').form('clear');
			$('#category-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#category-table').datagrid({
				queryParams:{data:$('#category-search-form').serialize()}
			});
		});		
		$('#category-table').datagrid({
			url:'<?php  echo site_url('category/admin/category/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
		$('#category_id_assign').combobox({
			url:'<?php echo site_url('category/admin/category/combobox_category_id'); ?>',
			multiple: false,
			valueField: 'category_id',
			textField: 'category_name',
			panelHeight:'auto'

		});
		$('#department_id').combobox({
			url:'<?php echo site_url('department/admin/department/combobox_department_id'); ?>',
			multiple: false,
			valueField: 'id',
			textField: 'name',
			panelHeight:'auto'

		});

		$('#assign_btn').click(function(){
			$(this).removeClass('btn-default').addClass('btn-primary');
			$('#category_btn').addClass('btn-default').removeClass('btn-primary');
			$('#category_table').hide();
			$('#assign_table').show();
			$('#assign-table').datagrid({
				url:'<?php echo site_url('category_department/admin/category_department/json'); ?>',
				height: 'auto',
				width: 'auto'
			});
		});
		$('#category_btn').click(function(){
			$(this).removeClass('btn-default').addClass('btn-primary');
			$('#assign_btn').addClass('btn-default').removeClass('btn-primary');
			$('#assign_table').hide();
			$('#category_table').show();
			$('#category-table').datagrid({
				url:'<?php echo site_url('category/admin/category/json'); ?>',
				height: 'auto',
				width: 'auto'
			});
		});

	});


	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-primary btn-xs margin"   title="<?php  echo lang('edit_category')?>">Edit</a>';
		var d = '<a href="#" onclick="removecategory('+index+')" class="btn btn-danger btn-xs "  title="<?php  echo lang('delete_category')?>">Delete</a>';
		return e+d;		
	}
	function getAssign_Actions(value,row,index)
	{
		var e = '<a href="#" onclick="edit_assigned('+index+')" class="btn btn-primary btn-xs margin"   title="<?php  echo lang('edit_category')?>">Edit</a>';
		var d = '<a href="#" onclick="remove_assigned('+index+')" class="btn btn-danger btn-xs "  title="<?php  echo lang('delete_category')?>">Delete</a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-category').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_category')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}
	function assign(){
		//Create code here
		$('#form-category_department').form('clear');
		$('#dlg2').window('open').window('setTitle','<?php  echo "Assign Category"?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#category-table').datagrid('getRows')[index];
		if (row){
			$('#form-category').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_category')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	function edit_assigned(index)
	{
		var row = $('#assign-table').datagrid('getRows')[index];
		if (row){
			$('#form-category_department').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg2').window('open').window('setTitle','<?php  echo "Edit Assigned"?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	

	function removecategory(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#category-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('category/admin/category/delete_json')?>', {id:[row.category_id]}, function(){
					$('#category-table').datagrid('deleteRow', index);
					$('#category-table').datagrid('reload');
				});

			}
		});
	}

	function remove_assigned(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#assign-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('category_department/admin/category_department/delete_json')?>', {id:[row.id]}, function(){
					$('#assign-table').datagrid('deleteRow', index);
					$('#assign-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected(n)
	{
		if(!n)
		{
			var rows=$('#category-table').datagrid('getSelections');
			if(rows.length>0)
			{
				selected=[];
				for(i=0;i<rows.length;i++)
				{
					selected.push(rows[i].category_id);
				}

				$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
					if(r){				
						$.post('<?php  echo site_url('category/admin/category/delete_json')?>',{id:selected},function(data){
							$('#category-table').datagrid('reload');
						});
					}

				});

			}
			else
			{
				$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
			}
		}
		else{
			var rows=$('#assign-table').datagrid('getSelections');
			if(rows.length>0)
			{
				selected=[];
				for(i=0;i<rows.length;i++)
				{
					selected.push(rows[i].id);
				}

				$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
					if(r){				
						$.post('<?php  echo site_url('category_department/admin/category_department/delete_json')?>',{id:selected},function(data){
							$('#assign-table').datagrid('reload');
						});
					}

				});

			}
			else
			{
				$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
			}
		}
		
	}
	
	function save(n)
	{
		if(!n)
		{
			$('#form-category').form('submit',{
				url: '<?php  echo site_url('category/admin/category/save')?>',
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success)
					{
						$('#form-category').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#category-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close

		});		
		}
		else
		{
			$('#form-category_department').form('submit',{
				url: '<?php  echo site_url('category_department/admin/category_department/save')?>',
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.success)
					{
						$('#form-category_department').form('clear');
						$('#dlg2').window('close');		// close the dialog
						$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
						$('#assign-table').datagrid('reload');	// reload the user data
					} 
					else 
					{
						$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
					} //if close
				}//success close

			});		

		}
		
	}
	
	
</script>