<?php

class Silo_report extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('silo_report','silo_report_model');
        $this->load->module_model('material','material_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->lang->module_load('silo_report','silo_report');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index($date_en = NULL)
	{
		$this->db->order_by('material_name');
		$where_material['dispatch_record_from'] = 1;
		$data['rows'] = $this->material_model->getMaterials($where_material)->result_array();
		$data['brand_type'] = $this->brand_type_model->getBrandTypes()->result_array();

		$date_en = $this->input->post('date_en');
		$data['date_np'] = $this->input->post('date_np');

		$where = array();
		if($date_en != NULL && $date_en != 'NaN-NaN-NaN'){
			$where['date_en'] = $date_en;
		}else{
			$this->db->order_by('date_en desc');
			$last_data = $this->silo_report_model->getSiloReports();
			if($last_data->num_rows > 0){
				$last_date = $last_data->row_array();
				$where['date_en'] = $last_date['date_en'];
			}else{
				$where['date_en'] = date('Y-m-d');
			}
		}
		$this->silo_report_model->joins = array('BRAND_TYPE');
		$consumptions = $this->silo_report_model->getSiloReports($where)->result_array();

		foreach ($data['rows'] as $key => $value) {
			foreach ($data['brand_type'] as $i => $values) {
				$data['rows'][$key][$values['name']] = 0;
			}
			foreach ($consumptions as $index => $val) {
				if($value['material_id'] == $val['material_id']){
					$data['rows'][$key][$val['type']] = $val['amount'];
				}
				$data['date_np'] = $val['date_np'];
			}
		}
		$data['date_en'] = $where['date_en'];

		// Display Page
		$data['header'] = 'Silo Report';
		$data['view_page'] = "silo_report/index";
		$data['module'] = 'silo_report';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->silo_report_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->silo_report_model->getSiloReports()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['type_id']!='')?$this->db->where('type_id',$params['search']['type_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->silo_report_model->getSiloReports()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->silo_report_model->delete('SILO_REPORT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
        $value = $this->_get_posted_data(); //Retrive Posted Data	
		
		// echo '<pre>';print_r($this->input->post());
		$data['date_np'] = $value['date_np'];
		$data['date_en'] = $value['date_en'];
        foreach($value['material_amount'] as $key => $val){
			$data['material_id'] = $key;
			foreach ($val as $i => $input) {
	        	$data['type_id'] = $i;
	        	$data['amount'] = $input;
        		// print_r($data);
        		if($this->silo_report_model->count(array('date_np' => $data['date_np'], 'material_id' => $data['material_id'], 'type_id' => $data['type_id']))){
        			 $success=$this->silo_report_model->update('SILO_REPORT',$data,array('date_np' => $data['date_np'], 'material_id' => $data['material_id'], 'type_id' => $data['type_id']));
        		}else{
        			$success=$this->silo_report_model->insert('SILO_REPORT',$data);
        		}
			}
        }	
// exit;
        // if(!$this->input->post('id'))
        // {
        //     $success=$this->silo_report_model->insert('SILO_REPORT',$data);
        // }
        // else
        // {
        //     $success=$this->silo_report_model->update('SILO_REPORT',$data,array('id'=>$data['id']));
        // }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['material_amount'] = $this->input->post('material_amount');

        return $data;
   }

   	public function get_value()
   	{
   		$where['date_en'] = $this->input->post('date_en');

   		$data = $this->silo_report_model->getSiloReports($where)->result_array();
   		echo json_encode($data);
   	}
   
   	
	    
}