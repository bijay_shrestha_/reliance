<?php

class Silo_report extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('silo_report','silo_report_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->lang->module_load('silo_report','silo_report');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'silo_report';
		$data['page'] = $this->config->item('template_admin') . "silo_report/index";
		$data['module'] = 'silo_report';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->silo_report_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->silo_report_model->getSiloReports()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['type_id']!='')?$this->db->where('type_id',$params['search']['type_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->silo_report_model->getSiloReports()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->silo_report_model->delete('SILO_REPORT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->silo_report_model->insert('SILO_REPORT',$data);
        }
        else
        {
            $success=$this->silo_report_model->update('SILO_REPORT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['material_id'] = $this->input->post('material_id');
$data['type_id'] = $this->input->post('type_id');

        return $data;
   }
   
   // for opening of silo
   public function opening(){
   	$this->db->where('key','silo');
   	$opening = $this->db->get('settings')->result_array();
   	$data['type'] = $this->brand_type_model->getBrandTypes()->result_array();
   	$data['opening'] = array();
   	
   	foreach ($opening as $key => $value) {
   		$index = $value['code'];
   		$data['opening'][$index] = $value;
   	}
   	
   	$data['header'] = 'silo_opening';
	$data['page'] = $this->config->item('template_admin') . "silo_report/opening";
	$data['module'] = 'silo_report';
	$this->load->view($this->_container,$data);		
   }

   	// saving opening
   	public function save_opening()
   	{
   		$form_data = $this->input->post();
   		$this->db->where('key', 'silo');
   		$this->db->delete('settings');
   		foreach ($form_data as $key => $value) {
   			$data = array(
   				'code' 	=> $key,
   				'key' 	=> 'silo',
   				'value'	=> $value
   				);
   			$this->db->insert('settings',$data);
   		}
   		echo json_encode(array('success' => TRUE ));
   	}
   	
	    
}