<?php
class Silo_report_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array(
            'SILO_REPORT'=>$this->prefix.'silo_report',
            'BRAND_TYPE'=>$this->prefix.'brand_type',
            'MATERIALS'=>$this->prefix.'materials');
		$this->_JOINS=array(
            'BRAND_TYPE'=>array('join_type'=>'LEFT','join_field'=>'silo_reports.type_id=brand_type.id',
                                           'select'=>'brand_type.name as type','alias'=>'brand_type'),
            'MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'silo_reports.material_id=material.material_id',
                                           'select'=>'material.material_name as material_name','alias'=>'material'),
                           
                            );        
    }
    
    public function getSiloReports($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''),$fields = NULL)
    {
        if($fields == NULL){
            $fields='silo_reports.*';
        }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['SILO_REPORT']. ' silo_reports');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['SILO_REPORT'].' silo_reports');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}