<div class="box">
    <div class="box-body">
    	<form id="opening">
	    	<?php foreach ($type as $key => $value) {?>
	    		<div class="col-md-2"><?php echo $value['name']?></div>
	    		<div class="col-md-10 form-group">
	    			<input type="number" name="<?php echo $value['name']?>" 
	    				value="<?php echo (array_key_exists($value['name'], $opening))?$opening[$value['name']]['value']:0;?>">
	    			</div>
	    	<?php }?>
    		<button type="button" class="btn btn-primary btn-xs" id="submit">Save</button>
    	</form>
    </div>
</div>

<script type="text/javascript">
	$('#submit').click(function(){
		$.post('<?php echo base_url('silo_report/admin/silo_report/save_opening')?>', $('#opening').serializeArray(),function(data){
			if(data.success){
				alert('Opening Added');
			}else{
				alert('Failed! Please try again');
			}
		},'json')
	})
</script>