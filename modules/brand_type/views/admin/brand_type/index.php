<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('brand_type_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="brand_type-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('name')?></label>:</td>
<td><input type="text" name="search[name]" id="search_name"  class="easyui-validatebox"/></td>
<!-- <td><label><?php echo lang('order')?></label>:</td> -->
<!-- <td><input type="text" name="search[order]" id="search_order"  class="easyui-numberbox"/></td> -->
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="brand_type-table" data-options="pagination:true,title:'<?php  echo lang('brand_type')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
<th data-options="field:'name',sortable:true" width="50"><?php echo lang('name')?></th>
<!-- <th data-options="field:'order',sortable:true" width="50"><?php echo lang('order')?></th> -->

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_brand_type')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_brand_type')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit brand_type form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-brand_type" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('name')?>:</label></td>
					  <td width="66%"><input name="name" id="name" class="easyui-validatebox" required="true"></td>
		       </tr>
		       <!-- <tr>
		              <td width="34%" ><label><?php echo lang('order')?>:</label></td>
					  <td width="66%"><input name="order" id="order" class="easyui-numberbox" ></td>
		       </tr> -->
		       <input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#brand_type-search-form').form('clear');
			$('#brand_type-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#brand_type-table').datagrid({
				queryParams:{data:$('#brand_type-search-form').serialize()}
				});
		});		
		$('#brand_type-table').datagrid({
			url:'<?php  echo site_url('brand_type/admin/brand_type/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<button onclick="edit('+index+')" class="btn btn-warning btn-xs" iconcls="icon-edit"  title="<?php echo lang('edit_brand_type')?>"><?php  echo lang('edit')?></button>';
		var d = '<button href="#" onclick="removebrand_type('+index+')" class="btn btn-danger btn-xs" iconcls="icon-remove"  title="<?php  echo lang('delete_brand_type')?>"><?php  echo lang('delete')?></button>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-brand_type').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_brand_type')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#brand_type-table').datagrid('getRows')[index];
		if (row){
			$('#form-brand_type').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_brand_type')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removebrand_type(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#brand_type-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('brand_type/admin/brand_type/delete_json')?>', {id:[row.id]}, function(){
					$('#brand_type-table').datagrid('deleteRow', index);
					$('#brand_type-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#brand_type-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('brand_type/admin/brand_type/delete_json')?>',{id:selected},function(data){
						$('#brand_type-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-brand_type').form('submit',{
			url: '<?php  echo site_url('brand_type/admin/brand_type/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-brand_type').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#brand_type-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>