<?php


$lang['id'] = 'Id';
$lang['name'] = 'Name';
$lang['order'] = 'Order';

$lang['create_brand_type']='Create Brand Type';
$lang['edit_brand_type']='Edit Brand Type';
$lang['delete_brand_type']='Delete Brand Type';
$lang['brand_type_search']='Brand Type Search';

$lang['brand_type']='Brand Type';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

