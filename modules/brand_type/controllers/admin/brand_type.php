<?php

class Brand_type extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('brand_type','brand_type_model');
        $this->lang->module_load('brand_type','brand_type');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'brand_type';
		$data['page'] = $this->config->item('template_admin') . "brand_type/index";
		$data['module'] = 'brand_type';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->brand_type_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->brand_type_model->getBrandTypes()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
// ($params['search']['order']!='')?$this->db->where('order',$params['search']['order']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->brand_type_model->getBrandTypes()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->brand_type_model->delete('BRAND_TYPE',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->brand_type_model->insert('BRAND_TYPE',$data);
        }
        else
        {
            $success=$this->brand_type_model->update('BRAND_TYPE',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['name'] = $this->input->post('name');
$data['order'] = $this->input->post('order');

        return $data;
   }
   
   	
	    
}