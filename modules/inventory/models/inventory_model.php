<?php
class Inventory_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('INVENTORY'=>$this->prefix.'inventory','MATERIALS'=>$this->prefix.'materials','DEPARTMENTS'=>$this->prefix.'departments','DEALERS'=>$this->prefix.'dealers');
		$this->_JOINS=array('MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'inventory.material_id=departments.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
                            'DEPARTMENTS'=>array('join_type'=>'LEFT','join_field'=>'inventory.department_id=departments.department_id',
                                           'select'=>'departments.*','alias'=>'departments'),
                            'DEALERS'=>array('join_type'=>'LEFT','join_field'=>'inventory.dealer_id=dealers.dealer_id',
                                           'select'=>'dealers.*','alias'=>'dealers'),
                           
                            );        
    }
    
    public function getInventories($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='inventories.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['INVENTORY']. ' inventories');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['INVENTORY'].' inventories');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }

    public function getDateInventory($where = NULL){
        $fields='inventories.*, SUM(inventories.current_stock) as total_stock';
       
        foreach($this->joins as $key):
            $fields=$fields . ','.$this->_JOINS[$key]['select'];
        endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['INVENTORY']. ' inventories');
        
        foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;         
        
        (! is_null($where))?$this->db->where($where):NULL;
        // (! is_null($order_by))?$this->db->order_by($order_by):NULL;

        // if( ! is_null($limit['limit']))
        // {
        //     $this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
        // }
        return $this->db->get();
    }
}