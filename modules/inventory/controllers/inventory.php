<?php

class Inventory extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('inventory','inventory_model');
        $this->load->module_model('material','material_model');
        $this->load->module_model('material_dealer','material_dealer_model');
        $this->load->module_model('dealer','dealer_model');
        $this->load->module_model('department','department_model');
        $this->load->module_model('purchase_request','purchase_request_model');
        $this->load->module_model('purchase','purchase_model');
        $this->load->module_model('dept_request','dept_request_model');
        $this->load->module_model('direct_dispatch_material','direct_dispatch_material_model');
        $this->load->module_model('report_type','report_type_model');
        $this->load->module_model('report_material','report_material_model');
        $this->load->module_model('report_order','report_order_model');
        $this->load->module_model('silo_report','silo_report_model');

        $this->lang->module_load('inventory','inventory');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$this->db->order_by('material_name','asc');
		$data['materials'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
		$this->db->order_by('name','asc');
		$data['departments'] = $this->department_model->getDepartments()->result_array();
		$this->db->order_by('dealer_name','asc');
		$data['dealers'] = $this->dealer_model->getDealers(array('status'=>1))->result_array();
		// $this->material_model->joins = array('INVENTORY');
		$data['stocks'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
    foreach ($data['stocks'] as $key => $value) {
        $total_purchase = $this->purchase_model->getAllPurchases(array('material_id' => $value['material_id']))->row_array();
        $total_dispatch = $this->dept_request_model->getAllDispatches(array('material_id' => $value['material_id'],'department_status'=>1))->row_array();
        $data['stocks'][$key]['current_stock'] = $total_purchase['total_purchase'] - $total_dispatch['all_dispatched'];
    }
		$this->purchase_model->joins = array('MATERIALS','DEALERS','DEPARTMENTS');
		$this->db->order_by('date_np','desc');
    $this->db->order_by('request_id','desc');
		$data['purchases'] = $this->purchase_model->getPurchases()->result_array();

		$data['header'] = 'Inventory';
		$data['view_page'] = "inventory/index";
		$data['module'] = 'inventory';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->inventory_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->inventory_model->getInventories()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['current_stock']!='')?$this->db->where('current_stock',$params['search']['current_stock']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->inventory_model->getInventories()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->inventory_model->delete('INVENTORY',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		// echo '<pre>';print_r($this->input->post());
    // exit;
        $data=$this->_get_posted_data(); //Retrive Posted Data  
        // echo '<pre>';print_r($data);exit;
        if($data['result']){  
          if(!$this->input->post('material_id'))
	        {
	            $success=$this->inventory_model->insert('INVENTORY',$data['inventory']);
	         
	        }
	        else
	        {
	            $success=$this->inventory_model->update('INVENTORY',$data['inventory'],array('material_id'=>$data['inventory']['material_id']));
	            	if($success){
	            		$success = $this->purchase_model->insert('PURCHASE',$data['purchase']);
						if($data['purchase']['request_id'])
						{
		            		$purchase_id = $this->db->insert_id();
		            		$success = $this->purchase_request_model->update('PURCHASE_REQUEST',array('purchase_id'=>$purchase_id),array('id'=>$data['purchase']['request_id']));
		            	}
	            }
	        }
	    }else{
	    	$success = FALSE;
	    }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        // $data['id'] = $this->input->post('id');
        $data['result'] = TRUE;
        if($this->input->post('head_office')){
        	if($this->input->post('material_id') == '' || $this->input->post('added_qty') == '' || $this->input->post('date_np') == '' || $this->input->post('dealer_id') == ''){
        		$data['result'] = FALSE;
        	}
        }
    $current_stock = floatval(str_replace(",","",$this->input->post('current_stock')));
    $added_qty = floatval(str_replace(",","",$this->input->post('added_qty')));
    $data['inventory']['material_id'] = $this->input->post('material_id');
    $data['inventory']['current_stock'] = $current_stock + $added_qty;
    
		$data['purchase']['material_id'] = $this->input->post('material_id');
		$data['purchase']['current_stock'] = $this->input->post('current_stock');
		$data['purchase']['request_id'] = $this->input->post('purchase_request');
		$data['purchase']['good_return'] = $this->input->post('good_return');
		$data['purchase']['department_id'] = $this->input->post('department_id');
		$data['purchase']['headoffice_purchase'] = $this->input->post('head_office');
    if($data['purchase']['headoffice_purchase']){
		  $data['purchase']['dealer_id'] = $this->input->post('dealer_id');
    }else{
      $data['purchase']['dealer_id'] = $this->input->post('post_dealer_id');
    }
		$data['purchase']['headoffice_unitcost'] = $this->input->post('headoffice_unitcost');
		$data['purchase']['date_en'] = $this->input->post('date_en');
		$data['purchase']['date_np'] = $this->input->post('date_np');
		$data['purchase']['current_stock'] = floatval(str_replace(",","",$this->input->post('current_stock')));
		$data['purchase']['added_qty'] = floatval(str_replace(",","",$this->input->post('added_qty')));

        return $data;
   }
   
   	public function getPurchase_request(){
   		$id = $this->input->post('id');
   		// $data['purchase_requests'] = $this->purchase_request_model->getPurchaseRequests(array('material_id' => $id,'approve' => 1, 'purchase_id' => 0))->result_array();
   		$data['current_stock'] = $this->inventory_model->getInventories(array('material_id'=>$id))->row_array();
      $data['current_stock']['current_stock'] = number_format($data['current_stock']['current_stock'],3);
   		$this->material_dealer_model->joins = array('DEALERS');
   		$this->db->order_by('dealer_name','asc');
   		$data['dealers'] = $this->material_dealer_model->getMaterialDealers(array('material_id'=>$id))->result_array();
      $data['material'] = $this->material_model->getMaterials(array('material_id'=>$id))->row_array();
      $purchase = $this->purchase_model->getAllPurchases(array('material_id'=>$id))->row_array();
      $dispatch = $this->dept_request_model->getAllDispatches(array('material_id'=>$id,'department_status' => 1))->row_array();
      $data['current_stock']['current_stock'] = number_format($purchase['total_purchase'] - $dispatch['all_dispatched'],3);
   		echo json_encode($data);
   	}

   	public function getAcceptedQty(){
   		$id = $this->input->post('id');
   		$request = $this->purchase_request_model->getPurchaseRequests(array('id' => $id,'approve' => 1, 'purchase_id' => 0))->row_array();
   		$data['added_qty'] = $request['accepted_quantity'];
   		$data['dealer_id'] = $request['dealer_id'];
   		echo json_encode($data);
   	}

   	public function dispatch(){
   		$data['materials'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
   		$this->dept_request_model->joins = array('MATERIALS','DEPARTMENTS','INVENTORY');
      $this->db->order_by('request_np_date','desc');
      $this->db->order_by('request_id','desc');
   		$data['histories'] = $this->dept_request_model->getDeptRequests(array('store_status' => 0))->result_array();
      $this->db->order_by('request_id','desc');
      $data['confirms'] = $this->dept_request_model->getDeptRequests(array('department_status' => 0, 'store_status' => 1))->result_array();
      //department for direct dispatch
      $this->db->group_by('department_id');
      $this->db->order_by('name','asc');
      $this->direct_dispatch_material_model->joins = array('DEPARTMENTS');
      $data['departments'] = $this->direct_dispatch_material_model->getDirectDispatchMaterials()->result_array();
      
      $where['store_status'] = 1;
      $where['department_status'] = 0;
      $data['pending'] =  $this->dept_request_model->count($where);

   		// echo '<pre>';print_r($data['departments']);exit;

   		$data['header'] = 'Dispatch';
		$data['view_page'] = "inventory/dispatch";
		$data['module'] = 'inventory';
		$this->load->view($this->_container,$data);
   	}

    public function getDispatch(){
      $id = $this->input->post('id');
      $this->dept_request_model->joins = array('MATERIALS','DEPARTMENTS');
      $result = $this->dept_request_model->getDeptRequests(array('request_id' => $id))->row_array();
      $stock = $this->inventory_model->getInventories(array('material_id'=>$result['material_id']))->row_array();
      $result['current_stock'] = $stock['current_stock'] + $result['accepted_quantity'];
      echo json_encode($result);
    }

   	public function departmentRequest(){
   		$material_id = $this->input->post('id');
   		$this->dept_request_model->joins = array('DEPARTMENTS');
   		$data['request'] = $this->dept_request_model->getDeptRequests(array('material_id'=>$material_id,'store_status'=>0))->result_array();
   		$data['current_stock'] = $this->inventory_model->getInventories(array('material_id'=>$material_id))->row_array();
   		// echo $this->db->last_query();
   		echo json_encode($data);
   	}
	
   	public function getRequestedQty(){
   		$request_id = $this->input->post('id');
   		$data = $this->dept_request_model->getDeptRequests(array('request_id'=>$request_id))->row_array();
   		echo json_encode($data);
   	}

   	//saving dispatch
   	public function saveDispatch(){
   		$id = $this->input->post('request_id');
   		// print_r($this->input->post());exit;
   		$check = TRUE;
   		$result['success'] = FALSE;
   		$data['store_confirm_np_date'] = $this->input->post('date_np');
      $data['store_confirm_en_date'] = $this->input->post('en_date');
   		$data['store_status'] = 1;
   		$data['accepted_quantity'] = $this->input->post('accepted_qty');
   		$data['current_stock'] = $this->input->post('current_stock');
   		$current = $this->input->post('current');
   		if($data['store_confirm_np_date'] == '' || $data['accepted_quantity'] == '' || $id == ''){
   			$check = FALSE;
   		}
   		if($check){
   			if($data['accepted_quantity'] == 0){
   				$data['store_status'] = 2;
   			}
   			// if($data['current_stock'] == 0 || $data['current_stock'] == ''){
   				$data['current_stock'] = $current;
   			// }
   			// echo $current;
   			// echo '<pre>';print_r($data);exit;
   			$old_data = $this->dept_request_model->getDeptRequests(array('request_id' => $id))->row_array();
   			$result['success'] = $this->dept_request_model->update('DEPT_REQUEST',$data,array('request_id'=>$id));

   			$new_stock = $current - $data['accepted_quantity'] + $old_data['accepted_quantity'];
   			// var_dump( $current - $data['accepted_quantity'] + $old_data['accepted_quantity']);
   			$this->inventory_model->update('INVENTORY',array('current_stock' => $new_stock),array('material_id'=>$old_data['material_id']));
        // print_r($this->db->last_query());
   			$this->dept_request_model->joins = array('MATERIALS','DEPARTMENTS');
   			$result['history'] = $this->dept_request_model->getDeptRequests(array('store_status <>' => 0,'request_id'=>$id))->row_array();
   		}


   		echo json_encode($result);
   	}

    // edit dispatch
    public function editDispatch(){
      $id = $this->input->post('id');
      $data['accepted_quantity'] = $this->input->post('accepted_qty');
      $data['store_confirm_np_date'] = $this->input->post('date_np');
      $data['store_confirm_en_date'] = $this->input->post('date_en');
      $data['current_stock'] = $this->input->post('current_stock');
      
      $success = $this->dept_request_model->update('DEPT_REQUEST',$data,array('request_id'=>$id));
      if($success){
        $material_id = $this->input->post('material_id');
        $stock['current_stock'] = $data['current_stock'] - $data['accepted_quantity'];
        $this->inventory_model->update('INVENTORY',$stock,array('material_id'=>$material_id));
      }echo json_encode($success);
    }

    // edit direct dispatch
    public function editDirectDispatch(){
      $id = $this->input->post('request_id');
      $data['accepted_quantity'] = $this->input->post('accepted_qty');
      $data['store_confirm_np_date'] = $this->input->post('date_np');
      $data['store_confirm_en_date'] = $this->input->post('date_en');
      $data['current_stock'] = $this->input->post('current_stock');
      // echo '<pre>';
      // print_r($data);exit;
      
      $success = $this->dept_request_model->update('DEPT_REQUEST',$data,array('request_id'=>$id));
      if($success){
        $material_id = $this->input->post('material_id');
        $stock['current_stock'] = $data['current_stock'] - $data['accepted_quantity'];
        $this->inventory_model->update('INVENTORY',$stock,array('material_id'=>$material_id));
      }
      echo json_encode($success);

    }

    //delete dispatch
    public function deleteDispatch(){
      $id = $this->input->post('id');
      $result = $this->dept_request_model->getDeptRequests(array('request_id' => $id))->row_array();
      $data['accepted_quantity'] = 0;
      $data['store_confirm_en_date'] = '';
      $data['store_confirm_np_date'] = '';
      $data['current_stock'] = 0;
      $data['store_status'] = 0;
      $success = $this->dept_request_model->update('DEPT_REQUEST',$data,array('request_id'=>$id));
      if($success){
        $stock = $this->inventory_model->getInventories(array('material_id'=>$result['material_id']))->row_array();
        $current_stock = $stock['current_stock'] + $result['accepted_quantity'];
        $this->inventory_model->update('INVENTORY',array('current_stock'=>$current_stock),array('material_id'=>$result['material_id']));
      }
      echo json_encode($success);
    }
    
    public function deleteDirectDispatch(){
      $id = $this->input->post('id');
      $result = $this->dept_request_model->getDeptRequests(array('request_id' => $id))->row_array();
      
      $success = $this->dept_request_model->delete('DEPT_REQUEST',array('request_id'=>$id));
      if($success){
        $stock = $this->inventory_model->getInventories(array('material_id'=>$result['material_id']))->row_array();
        $current_stock = $stock['current_stock'] + $result['accepted_quantity'];
        $this->inventory_model->update('INVENTORY',array('current_stock'=>$current_stock),array('material_id'=>$result['material_id']));
      }
      echo json_encode($success);
    }

   	//accepted dispatch
   	public function acceptedDispatch(){
   		$this->dept_request_model->joins = array('MATERIALS','DEPARTMENTS');
      $this->db->order_by('store_confirm_en_date','desc');
      $this->db->order_by('request_id','desc');
   		$result['data'] = $this->dept_request_model->getDeptRequests(array('department_status' => 1, 'direct_dispatch' => 0))->result_array();

      $i = 0;
      foreach($result['data'] as $data){
        $result['data'][$i]['current_stock'] = number_format($data['current_stock'],3);
        $result['data'][$i]['request_quantity'] = number_format($data['request_quantity'],3);
        $result['data'][$i]['accepted_quantity'] = number_format($data['accepted_quantity'],3);
        $i++;
      }

   		echo json_encode($result);
   	}
    //direct dispatch
    public function directDispatch(){
      $this->dept_request_model->joins = array('MATERIALS','DEPARTMENTS');
      $this->db->order_by('store_confirm_en_date','desc');
      $this->db->order_by('request_id','desc');
      $result['data'] = $this->dept_request_model->getDeptRequests(array('department_status' => 1, 'direct_dispatch' => 1))->result_array();

      $i = 0;
      foreach($result['data'] as $data){
        $result['data'][$i]['current_stock'] = number_format($data['current_stock'],3);
        $result['data'][$i]['request_quantity'] = number_format($data['request_quantity'],3);
        $result['data'][$i]['accepted_quantity'] = number_format($data['accepted_quantity'],3);
        $result['data'][$i]['action'] = "<td>
                                    <button class='btn btn-warning' onclick='editDirectDispatch(".$data['request_id'].")'><i class='fa fa-pencil'></i></button>
                                    <button class='btn btn-danger' onclick='deleteDirectDispatch(".$data['request_id'].")'><i class='fa fa-trash'></i></button>
                                </td>";
        $i++;
      }

      echo json_encode($result);
    }

   	//rejected dispatch
   	public function rejectedDispatch(){
   		$this->dept_request_model->joins = array('MATERIALS','DEPARTMENTS');
      $this->db->order_by('store_confirm_en_date','desc');
      $this->db->where('department_status' , 2);
      $this->db->or_where('store_status' , 2);
      $this->db->order_by('request_id','desc');
   		$result['data'] = $this->dept_request_model->getDeptRequests()->result_array();

      $i = 0;
      foreach($result['data'] as $data){
        $result['data'][$i]['current_stock'] = number_format($data['current_stock'],3);
        $result['data'][$i]['request_quantity'] = number_format($data['request_quantity'],3);
        $i++;
      }

   		echo json_encode($result);
   	}

    //datewise stock
    public function searchWithDate(){
      $date = $this->input->post('date');
      $inventory = $this->inventory_model->getDateInventory(array('date'=>$date))->result_array();
      echo json_encode($inventory);
    }
    //edit purchase history
    public function editPurchaseHistory(){
      // echo '<pre>';print_r($this->input->post());exit;
      $id = $this->input->post('purchase_id');
      $added_qty = $this->input->post('added_qty');
      $date_np = $this->input->post('date_np');
      $date_en = $this->input->post('date_en');

      $purchase = $this->purchase_model->getPurchases(array('purchase_id'=>$id))->row_array();
      $material_id = $purchase['material_id'];
      $old_qty = $this->inventory_model->getInventories(array('material_id'=>$material_id))->row_array();

      $success = $this->purchase_model->update('PURCHASE',array('added_qty'=>$added_qty,'date_np'=>$date_np,'date_en'=>$date_en),array('purchase_id'=>$id));
// echo $this->db->last_query();

      if($success){
        $old_qty['current_stock'] = floatval($old_qty['current_stock']);
        $purchase['added_qty'] = floatval($purchase['added_qty']);
        $new_qty = floatval($old_qty['current_stock']) - floatval($purchase['added_qty']) + $added_qty;
        $this->inventory_model->update('INVENTORY',array('current_stock'=>$new_qty),array('material_id'=>$material_id));
      }
      echo json_encode($success);
    }

    public function getAllDispatches(){
      $this->db->group_by('material_id');
      $where = array('store_status' => 1, 'department_status' => 1);
      $result = $this->dept_request_model->getAllDispatches($where)->result_array();
      echo '<pre>';
      print_r($result);
    }

    // stock report
    public function report(){
      $data['types'] = $this->report_type_model->getReportTypes()->result_array();
      $data['today_dispatches'] = array();
      $date = NULL;
      if($this->input->post('date')){
        $date = $this->input->post('en-date');
      }else{
        $date = date('Y-m-d');
      }
      
      $this->db->where('date_en <=', $date);
      $this->db->group_by('purchases.material_id');
      $this->purchase_model->joins = array('MATERIALS');
      $purchases = $this->purchase_model->getAllPurchases()->result_array();

      $data['opening_stocks'] = array();
      $data['dispatches'] = array();
      $data['purchase'] = array();
      foreach($purchases as $purchase){
        $material_id = $purchase['material_id'];
        
        $this->db->where('date_en <', $date);
        $this->db->where('purchases.material_id', $material_id);
        $this->db->group_by('purchases.material_id');
        $bought_previous = $this->purchase_model->getAllPurchases()->row_array();
          $this->db->where('store_confirm_en_date <', $date);
          $where = array('material_id' => $material_id,'store_status' => 1, 'department_status' => 1);
          $dispatch = $this->dept_request_model->getAllDispatches($where)->row_array();

        if($purchase['dispatch_record_from'] != 0){
          $this->db->where('date_en <', $date);
          $fields = 'SUM(amount) AS total_consume';
          $where = array('material_id' => $material_id);
          $consume = $this->silo_report_model->getSiloReports($where,NULL,NULL,$fields)->row_array();
        }
          if(count($dispatch)>0){
            if(array_key_exists('total_purchase', $bought_previous)){
              $data['opening_stocks'][$material_id] = $bought_previous['total_purchase'] - $dispatch['all_dispatched'];
            }
          }else{
            if(array_key_exists('total_purchase', $bought_previous)){
              $data['opening_stocks'][$material_id] = $bought_previous['total_purchase'];
            }
          }
        if($purchase['dispatch_record_from'] != 0){
          if(count($consume)>0){
            if(array_key_exists('total_purchase', $bought_previous)){
              $data['opening_stocks'][$material_id] -= $consume['total_consume'];
            }
          }else{
            if(array_key_exists('total_purchase', $bought_previous)){
              $data['opening_stocks'][$material_id] = $bought_previous['total_purchase'];
            }
          }
        }

        $this->db->where('store_confirm_en_date <=',$date);
        $this->db->where('material_id',$material_id);
        $this->db->where('store_status',1);
        $this->db->where('department_status',1);
        $data['dispatches'][$material_id] = $this->dept_request_model->getAllDispatches($where)->row_array();

        $this->db->where('date_en ', $date);
        $this->db->where('purchases.material_id',$material_id);
        $data['purchase'][$material_id] = $this->purchase_model->getPurchases()->result_array();

        //today dispatch
        
          $this->db->where('material_id',$material_id);
          $this->db->where('store_confirm_en_date',$date);
          $this->db->where('store_status',1);
          $this->db->where('department_status',1);
          $data['today_dispatches'][$material_id] = $this->dept_request_model->getDeptRequests()->result_array();
        if($purchase['dispatch_record_from'] != 0){
          $this->db->where('date_en', $date);
          $fields = 'SUM(amount) AS accepted_quantity';
          $where = array('material_id' => $material_id);
            $quantity = $this->silo_report_model->getSiloReports($where,NULL,NULL,$fields)->result_array();
            foreach ($quantity as $q => $qty) {
              $data['today_dispatches'][$material_id][] = $qty;
            }
        }
       
      }
      
      // for report according to type
      $type = $this->input->post('type');
      if($type != ''){
        $data['report'] = $this->report_type_model->getReportTypes(array('rt_id' => $type))->row_array();
        $this->material_model->joins = array('REPORT_MATERIAL','CATEGORY');
        $this->db->where('report_type_id', $type);
      }else{
        $this->material_model->joins = array('CATEGORY');
        $data['report']['type'] = 'Inventory Report';
      }
      $data['materials'] = $this->material_model->getMaterials()->result_array();
      // for today purchase
      $data['today_purchase'] = array();
      $materials = $this->material_model->getMaterials()->result_array();
//for report material
      if($type != ''){
        $i = 0;
        foreach($data['materials'] as $material){
          $order = $this->report_order_model->getReportOrders(array('category_id'=>$material['category_id'],'report_id'=>$type))->row_array();
          // print_r($order);echo '<br>';
          if(array_key_exists('report_order', $order)){
            $data['materials'][$i]['report_order'] = $order['report_order'];
          }
          $i++;
        }
      }


      // echo '<pre>';print_r($data['materials']);exit;
      foreach($materials as $material){
        $material_id = $material['material_id'];
        $this->db->where('purchases.material_id',$material_id);
        $this->db->where('date_en', $date);
        $today_purchases = $this->purchase_model->getAllPurchases()->row_array();
        if($today_purchases['total_purchase']>0){
          $data['today_purchase'][$material['material_id']] = $today_purchases['total_purchase'];    
        }
      }
      // echo $date.'<br>';
      // print_r($data['report']);exit;
      // print_r($data['today_dispatches']);exit;
      
      $data['header'] = 'Inventory';
      $data['view_page'] = "inventory/report";
      $data['module'] = 'inventory';
      $this->load->view($this->_container,$data);
    }

    public function getPurchase(){
      $id = $this->input->post('id');
      $this->purchase_model->joins = array('MATERIALS','DEALERS');
      $purchase = $this->purchase_model->getPurchases(array('purchases.purchase_id'=>$id))->row_array();
      echo json_encode($purchase);
    }

// delete purchase history
    public function deleteHistory(){
      $id = $this->input->post('id');
      $purchase = $this->purchase_model->getPurchases(array('purchase_id'=>$id))->row_array();
      $success = $this->purchase_model->delete('PURCHASE',array('purchase_id'=>$id));
      if($success){
        // $this->purchase_request_model->update('PURCHASE_REQUEST',array('purchase_id'=>0),array('id'=>$purchase['request_id']));
        $stock = $this->inventory_model->getInventories(array('material_id'=>$purchase['material_id']))->row_array();
        $old_qty = $stock['current_stock'] - $purchase['added_qty'];
        $this->inventory_model->update('INVENTORY',array('current_stock'=>$old_qty),array('material_id'=>$purchase['material_id']));
      }
      echo json_encode($success);
    }
//for form
    public function getDirectDispatchForm(){
      $where['request_id'] = $this->input->post('id');
      $data['report'] = $this->dept_request_model->getDeptRequests($where)->row_array();
      $this->direct_dispatch_material_model->joins = array('MATERIALS');
      $data['materials'] = $this->direct_dispatch_material_model->getDirectDispatchMaterials(array('department_id'=>$data['report']['department_id']))->result_array();
      $data['current_stock'] = $this->inventory_model->getInventories(array('material_id' => $data['report']['material_id']))->row_array();
      // echo '<pre>';
      // print_r($data['report']);
      // echo '</pre>';
      $this->db->group_by('department_id');
      $this->direct_dispatch_material_model->joins = array('DEPARTMENTS');
      $data['departments'] = $this->direct_dispatch_material_model->getDirectDispatchMaterials()->result_array();
      $this->load->view('direct_dispatch_material/edit_form',$data);
    }

}