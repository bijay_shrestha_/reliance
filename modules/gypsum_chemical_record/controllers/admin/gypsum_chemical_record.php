<?php

class Gypsum_chemical_record extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('gypsum_chemical_record','gypsum_chemical_record_model');
        $this->lang->module_load('gypsum_chemical_record','gypsum_chemical_record');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'gypsum_chemical_record';
		$data['page'] = $this->config->item('template_admin') . "gypsum_chemical_record/index";
		$data['module'] = 'gypsum_chemical_record';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->gypsum_chemical_record_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->gypsum_chemical_record_model->getGypsumChemicalRecords()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['chemical_id']!='')?$this->db->where('chemical_id',$params['search']['chemical_id']):'';
($params['search']['amount']!='')?$this->db->like('amount',$params['search']['amount']):'';
($params['search']['gypsum_record_id']!='')?$this->db->where('gypsum_record_id',$params['search']['gypsum_record_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->gypsum_chemical_record_model->getGypsumChemicalRecords()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->gypsum_chemical_record_model->delete('GYPSUM_CHEMICAL_RECORD',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->gypsum_chemical_record_model->insert('GYPSUM_CHEMICAL_RECORD',$data);
        }
        else
        {
            $success=$this->gypsum_chemical_record_model->update('GYPSUM_CHEMICAL_RECORD',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['chemical_id'] = $this->input->post('chemical_id');
$data['amount'] = $this->input->post('amount');
$data['gypsum_record_id'] = $this->input->post('gypsum_record_id');

        return $data;
   }
   
   	
	    
}