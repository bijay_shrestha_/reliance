<?php


$lang['id'] = 'Id';
$lang['chemical_id'] = 'Chemical Id';
$lang['amount'] = 'Amount';
$lang['gypsum_record_id'] = 'Gypsum Record Id';

$lang['create_gypsum_chemical_record']='Create Gypsum Chemical Record';
$lang['edit_gypsum_chemical_record']='Edit Gypsum Chemical Record';
$lang['delete_gypsum_chemical_record']='Delete Gypsum Chemical Record';
$lang['gypsum_chemical_record_search']='Gypsum Chemical Record Search';

$lang['gypsum_chemical_record']='Gypsum Chemical Record';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

