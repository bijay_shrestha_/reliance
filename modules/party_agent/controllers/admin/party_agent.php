<?php

class Party_agent extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('party_agent','party_agent_model');
        $this->lang->module_load('party_agent','party_agent');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'party_agent';
		$data['page'] = $this->config->item('template_admin') . "party_agent/index";
		$data['module'] = 'party_agent';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->party_agent_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->party_agent_model->getPartyAgents()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['party_id']!='')?$this->db->where('party_id',$params['search']['party_id']):'';
($params['search']['agent_id']!='')?$this->db->where('agent_id',$params['search']['agent_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->party_agent_model->getPartyAgents()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->party_agent_model->delete('PARTY_AGENT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->party_agent_model->insert('PARTY_AGENT',$data);
        }
        else
        {
            $success=$this->party_agent_model->update('PARTY_AGENT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['party_id'] = $this->input->post('party_id');
$data['agent_id'] = $this->input->post('agent_id');

        return $data;
   }
   
   	
	    
}