<?php


$lang['id'] = 'Id';
$lang['purchase_id'] = 'Purchase Id';
$lang['material_id'] = 'Material Id';
$lang['dealer_id'] = 'Dealer Id';
$lang['requested_quantity'] = 'Requested Quantity';
$lang['accepted_quantity'] = 'Accepted Quantity';
$lang['requested_date_nepali'] = 'Requested Date';
$lang['requested_date_english'] = 'Requested Date English';
$lang['accepted_date_nepali'] = 'Accepted Date';
$lang['accepted_date_english'] = 'Accepted Date English';
$lang['request_cost'] = 'Request Cost';
$lang['accepted_cost'] = 'Accepted Cost';
$lang['approve'] = 'Approve';
$lang['requested_remarks'] = 'Requested Remarks';
$lang['approved_remarks'] = 'Approved Remarks';

$lang['create_purchase_request']='Create Purchase Request';
$lang['edit_purchase_request']='Edit Purchase Request';
$lang['delete_purchase_request']='Delete Purchase Request';
$lang['purchase_request_search']='Purchase Request Search';

$lang['purchase_request']='Purchase Request';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

