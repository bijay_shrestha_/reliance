<?php
class Purchase_request_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('PURCHASE_REQUEST'=>$this->prefix.'purchase_request','MATERIALS'=>$this->prefix.'materials','DEALERS'=>$this->prefix.'dealers');
		$this->_JOINS=array('MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'purchase_requests.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
                            'DEALERS'=>array('join_type'=>'LEFT','join_field'=>'purchase_requests.dealer_id=dealers.dealer_id',
                                           'select'=>'dealers.*','alias'=>'dealers')
                           
                            );        
    }
    
    public function getPurchaseRequests($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='purchase_requests.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['PURCHASE_REQUEST']. ' purchase_requests');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['PURCHASE_REQUEST'].' purchase_requests');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}