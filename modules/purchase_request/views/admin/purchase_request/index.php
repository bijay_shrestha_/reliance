<div region="center" border="false">
	<div style="padding:20px">
		<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('purchase_request_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
			<form action="" method="post" id="purchase_request-search-form">
				<table class="table">
					<tr><!-- <td><label><?php echo lang('purchase_id')?></label>:</td>
						<td><input type="text" name="search[purchase_id]" id="search_purchase_id"  class="easyui-numberbox"/></td> -->
						<td><label>Material</label>:</td>
						<td><input type="text" name="search[material_id]" id="search_material_id"  class="easyui-validbox"/></td>
						<td><label>Dealer</label>:</td>
						<td><input type="text" name="search[dealer_id]" id="search_dealer_id"  class="easyui-validbox"/></td>

					</tr>
					<tr>
						<td><label><?php echo lang('requested_date_nepali')?></label>:</td>
						<td><input type="text" name="date[requested_date_nepali][from]" id="search_requested_date_nepali_from"  class="easyui-datebox"/> ~ <input type="text" name="date[requested_date_nepali][to]" id="search_requested_date_nepali_to"  class="easyui-datebox"/>
						<td><label><?php echo lang('requested_quantity')?></label>:</td>
						<td><input type="text" name="search[requested_quantity]" id="search_requested_quantity"  class="easyui-numberbox"/></td>
					</tr>
					<?php /*
					<tr>
						<td><label><?php echo lang('accepted_quantity')?></label>:</td>
						<td><input type="text" name="search[accepted_quantity]" id="search_accepted_quantity"  class="easyui-numberbox"/></td>
						</td>
					</tr>
					<tr>
						<td><label><?php echo lang('requested_date_english')?></label>:</td>
						<td><input type="text" name="date[requested_date_english][from]" id="search_requested_date_english_from"  class="easyui-datebox" /> ~ <input type="text" name="date[requested_date_english][to]" id="search_requested_date_english_to"  class="easyui-datebox" /></td>
						<td><label><?php echo lang('accepted_date_nepali')?></label>:</td>
						<td><input type="text" name="date[accepted_date_nepali][from]" id="search_accepted_date_nepali_from"  class="easyui-datebox"/> ~ <input type="text" name="date[accepted_date_nepali][to]" id="search_accepted_date_nepali_to"  class="easyui-datebox"/></td>
					</tr>
					<tr>
						<td><label><?php echo lang('accepted_date_english')?></label>:</td>
						<td><input type="text" name="date[accepted_date_english][from]" id="search_accepted_date_english_from"  class="easyui-datebox"/> ~ <input type="text" name="date[accepted_date_english][to]" id="search_accepted_date_english_to"  class="easyui-datebox"/></td>
						<td><label><?php echo lang('request_cost')?></label>:</td>
						<td><input type="text" name="search[request_cost]" id="search_request_cost"  class="easyui-numberbox"/></td>
					</tr>
					<tr>
						<td><label><?php echo lang('accepted_cost')?></label>:</td>
						<td><input type="text" name="search[accepted_cost]" id="search_accepted_cost"  class="easyui-numberbox"/></td>
						<td><label><?php echo lang('approve')?></label>:</td>
						<td><input type="text" name="search[approve]" id="search_approve"  class="easyui-numberbox"/></td>
					</tr>
					<tr>
						<td><label><?php echo lang('requested_remarks')?></label>:</td>
						<td><input type="text" name="search[requested_remarks]" id="search_requested_remarks"  class="easyui-validatebox"/></td>
						<td><label><?php echo lang('approved_remarks')?></label>:</td>
						<td><input type="text" name="search[approved_remarks]" id="search_approved_remarks"  class="easyui-validatebox"/></td>
					</tr>*/?>
					<tr>
					</tr>
					<tr>
						<td colspan="4">
							<a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
							<a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
						</td>
					</tr>
				</table>

			</form>
		</div>
		<br/>
		<div class="btn-group">
			<a class="btn btn-primary" id="pending">Pending Request</a>
			<a class="btn btn-default" id="approved">Approved</a>
			<a class="btn btn-default" id="rejected">Rejected</a>
		</div>
		<div id="pending_request">
			<table id="purchase_request-table" style="width:100%" data-options="pagination:true,title:'Pending <?php  echo lang('purchase_request')?>',pagesize:'20',  rownumbers:true,collapsible:true,fitColumns:true">
				<thead>
					<!-- <th data-options="field:'checkbox',checkbox:true"></th> -->
					<!-- <th data-options="field:'id',sortable:true" width="3%"><?php echo lang('id')?></th> -->
					<!-- <th data-options="field:'purchase_id',sortable:true" width="90"><?php echo lang('purchase_id')?></th> -->
					<th data-options="field:'material_name',sortable:true" width="50"><?php echo "Material Name";?></th>
					<th data-options="field:'dealer_name',sortable:true" width="50"><?php echo 'Dealer Name'?></th>
					<!-- <th data-options="field:'accepted_quantity',sortable:true" width="50"><?php echo lang('accepted_quantity')?></th> -->
					<th data-options="field:'requested_date_nepali',sortable:true" width="50"><?php echo lang('requested_date_nepali')?></th>
					<!-- <th data-options="field:'requested_date_english',sortable:true" width="50"><?php echo lang('requested_date_english')?></th> -->
					<!-- <th data-options="field:'accepted_date_nepali',sortable:true" width="50"><?php echo lang('accepted_date_nepali')?></th> -->
					<!-- <th data-options="field:'accepted_date_english',sortable:true" width="50"><?php echo lang('accepted_date_english')?></th> -->
					<th data-options="field:'requested_quantity',sortable:true" width="50"><?php echo lang('requested_quantity')?></th>
					<th data-options="field:'request_cost',sortable:true" width="50"><?php echo lang('request_cost')?></th>
					<th data-options="sortable:true" field="total" formatter="getTotal"><?php echo "Total"?></th>
					<!-- <th data-options="field:'accepted_cost',sortable:true" width="50"><?php echo lang('accepted_cost')?></th> -->
					<!-- <th data-options="field:'approve',sortable:true" width="50"><?php echo lang('approve')?></th> -->
					<!-- <th data-options="field:'requested_remarks',sortable:true" width="50"><?php echo lang('requested_remarks')?></th> -->
					<!-- <th data-options="field:'approved_remarks',sortable:true" width="50"><?php echo lang('approved_remarks')?></th> -->

					<th field="action" formatter="getActions" width="150"><?php  echo lang('action')?></th>
				</thead>
			</table>
		</div>

		<div id="approved_request" hidden>
			<table id="approved_request-table" data-options="pagination:true,title:'Approved <?php  echo lang('purchase_request')?>',pagesize:'20', rownumbers:true,collapsible:true,fitColumns:true">
				<thead>
					<!-- <th data-options="field:'checkbox',checkbox:true"></th> -->
					<!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
					<!-- <th data-options="field:'purchase_id',sortable:true" width="50"><?php echo lang('purchase_id')?></th> -->
					<th data-options="field:'material_name',sortable:true" width="50"><?php echo "Material Name";?></th>
					<th data-options="field:'dealer_name',sortable:true" width="50"><?php echo "Dealer Name";?></th>
					<th data-options="field:'requested_quantity',sortable:true" width="50"><?php echo lang('requested_quantity')?></th>
					<th data-options="field:'request_cost',sortable:true" width="50"><?php echo lang('request_cost')?></th>
					<th data-options="field:'accepted_quantity',sortable:true" width="50"><?php echo lang('accepted_quantity')?></th>
					<th data-options="field:'accepted_cost',sortable:true" width="50"><?php echo lang('accepted_cost')?></th>
					<th data-options="sortable:true" field="total" formatter="getTotal"><?php echo "Total"?></th>
					<th data-options="field:'requested_date_nepali',sortable:true" width="50"><?php echo lang('requested_date_nepali')?></th>
					<th data-options="field:'requested_date_english',sortable:true" width="50"><?php echo lang('requested_date_english')?></th>
					<th data-options="field:'accepted_date_nepali',sortable:true" width="50"><?php echo lang('accepted_date_nepali')?></th>
					<th data-options="field:'accepted_date_english',sortable:true" width="50"><?php echo lang('accepted_date_english')?></th>
					<!-- <th data-options="field:'approve',sortable:true" width="50"><?php echo lang('approve')?></th> -->
					<th data-options="field:'requested_remarks',sortable:true" width="50"><?php echo lang('requested_remarks')?></th>
					<th data-options="field:'approved_remarks',sortable:true" width="150">Remarks</th>

					<!-- <th field="action" width="100" formatter="getApprovedActions"><?php  echo lang('action')?></th> -->
				</thead>
			</table>
		</div>
		<div id="rejected_request" hidden>
			<table id="rejected_request-table" style="width:100%" data-options="pagination:true,title:'Rejected <?php  echo lang('purchase_request')?>',pagesize:'20',  rownumbers:true,collapsible:true,fitColumns:true">
				<thead>
					<!-- <th data-options="field:'checkbox',checkbox:true"></th> -->
					<!-- <th data-options="field:'id',sortable:true" width="3%"><?php echo lang('id')?></th> -->
					<!-- <th data-options="field:'purchase_id',sortable:true" width="7%"><?php echo lang('purchase_id')?></th> -->
					<th data-options="field:'material_name',sortable:true" width="7%"><?php echo "Material Name"?></th>
					<th data-options="field:'dealer_name',sortable:true" width="7%"><?php echo "Dealer Name"?></th>
					<th data-options="field:'requested_quantity',sortable:true" width="50"><?php echo lang('requested_quantity')?></th>
					<th data-options="field:'request_cost',sortable:true" width="50"><?php echo lang('request_cost')?></th>
					<th data-options="sortable:true" field="total" formatter="getTotal"><?php echo "Total"?></th>
					<!-- <th data-options="field:'accepted_quantity',sortable:true" width="50"><?php echo lang('accepted_quantity')?></th> -->
					<th data-options="field:'requested_date_nepali',sortable:true" width="50"><?php echo lang('requested_date_nepali')?></th>
					<th data-options="field:'requested_date_english',sortable:true" width="50"><?php echo lang('requested_date_english')?></th>
					<!-- <th data-options="field:'accepted_date_nepali',sortable:true" width="50"><?php echo lang('accepted_date_nepali')?></th> -->
					<!-- <th data-options="field:'accepted_date_english',sortable:true" width="50"><?php echo lang('accepted_date_english')?></th> -->
					<!-- <th data-options="field:'accepted_cost',sortable:true" width="50"><?php echo lang('accepted_cost')?></th> -->
					<!-- <th data-options="field:'approve',sortable:true" width="50"><?php echo lang('approve')?></th> -->
					<th data-options="field:'requested_remarks',sortable:true" width="50"><?php echo lang('requested_remarks')?></th>
					<!-- <th data-options="field:'approved_remarks',sortable:true" width="50"><?php echo lang('approved_remarks')?></th> -->

					<!-- <th field="action" formatter="getRejectedActions" width="35%"><?php  echo lang('action')?></th> -->
				</thead>
			</table>
		</div>



		<!-- <div id="toolbar" style="padding:5px;height:auto">
			<p>
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_purchase_request')?>"><?php  echo lang('create')?></a>
				<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_purchase_request')?>"><?php  echo lang('remove_selected')?></a>
			</p>
		</div>  -->

		<!--for create and edit purchase_request form -->
		<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px" data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
		<form id="form-purchase_request" method="post" >
			<table class="table">
				<!-- <tr>
					<td width="34%" ><label><?php echo lang('purchase_id')?>:</label></td>
					<td width="66%"><input name="purchase_id" id="purchase_id" class="easyui-numberbox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('material_id')?>:</label></td>
					<td width="66%"><input name="material_id" id="material_id" class="easyui-numberbox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('dealer_id')?>:</label></td>
					<td width="66%"><input name="dealer_id" id="dealer_id" class="easyui-numberbox" required="true"></td>
				</tr> -->
				<tr>
					<td width="34%" ><label><?php echo lang('requested_quantity')?>:</label></td>
					<td width="66%"><input name="requested_quantity" id="requested_quantity" class="easyui-numberbox" required="true"></td>
				</tr>
				<!-- <tr>
					<td width="34%" ><label><?php echo lang('accepted_quantity')?>:</label></td>
					<td width="66%"><input name="accepted_quantity" id="accepted_quantity" class="easyui-numberbox" required="true"></td>
				</tr> -->
				<!-- <tr>
					<td width="34%" ><label><?php echo lang('requested_date_nepali')?>:</label></td>
					<td width="66%"><input name="requested_date_nepali" id="requested_date_nepali" class="easyui-datetimebox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('requested_date_english')?>:</label></td>
					<td width="66%"><input name="requested_date_english" id="requested_date_english" class="easyui-datetimebox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('accepted_date_nepali')?>:</label></td>
					<td width="66%"><input name="accepted_date_nepali" id="accepted_date_nepali" class="easyui-datetimebox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('accepted_date_english')?>:</label></td>
					<td width="66%"><input name="accepted_date_english" id="accepted_date_english" class="easyui-datetimebox" required="true"></td>
				</tr> -->
				<tr>
					<td width="34%" ><label><?php echo lang('request_cost')?>:</label></td>
					<td width="66%"><input name="request_cost" id="request_cost" class="easyui-numberbox" required="true"></td>
				</tr>
				<!-- <tr>
					<td width="34%" ><label><?php echo lang('accepted_cost')?>:</label></td>
					<td width="66%"><input name="accepted_cost" id="accepted_cost" class="easyui-numberbox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('approve')?>:</label></td>
					<td width="66%"><input name="approve" id="approve" class="easyui-numberbox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('requested_remarks')?>:</label></td>
					<td width="66%"><input name="requested_remarks" id="requested_remarks" class="easyui-validatebox" required="true"></td>
				</tr>
				<tr>
					<td width="34%" ><label><?php echo lang('approved_remarks')?>:</label></td>
					<td width="66%"><input name="approved_remarks" id="approved_remarks" class="easyui-validatebox" required="true"></td>
				</tr> -->
				<input type="hidden" name="id" id="id"/>
			</table>
		</form>
		<div id="dlg-buttons">
			<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
			<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
		</div>    
	</div>
	<!--div ends-->

</div>
</div>

<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#purchase_request-search-form').form('clear');
			$('#purchase_request-table').datagrid({
				queryParams:null
			});

		});

		$('#search').click(function(){
			$('#purchase_request-table').datagrid({
				queryParams:{data:$('#purchase_request-search-form').serialize()}
			});
		});		
		$('#purchase_request-table').datagrid({
			url:'<?php  echo site_url('purchase_request/admin/purchase_request/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
			
		}); /*.datagrid('enableFilter')*/

		//$('#approved').removeClass('btn-default').addClass('btn-primary');

		$('#dealer_id_assign').combobox({
			url:'<?php echo site_url('dealer/admin/dealer/combobox_dealer_id'); ?>',
			multiple: false,
			valueField: 'dealer_id',
			textField: 'dealer_name',
			panelHeight:'300',
			editable: true,
			onChange:function(dealer_id_assign)
			{
				material_dealer_form(dealer_id_assign);
			}

		});
		$('#material_id').combobox({
			url:'<?php echo site_url('material/admin/material/combobox_material_id'); ?>',
			multiple: false,
			valueField: 'material_id',
			textField: 'material_name',
			panelHeight:'300',
			editable: true,
		});


	});
	$('#approved').click(function(){
		$(this).removeClass('btn-default').addClass('btn-primary');
		$('#pending').removeClass('btn-primary').addClass('btn-default');
		$('#rejected').removeClass('btn-primary').addClass('btn-default');
		$('#pending_request').hide();
		$('#rejected_request').hide();
		$('#approved_request').show();
		$('#approved_request-table').datagrid({
			url:'<?php echo site_url('purchase_request/admin/purchase_request/approved_json');?>',
			height:'auto',
			width:'auto',
			

		});
	});

	$('#pending').click(function(){
		$(this).removeClass('btn-default').addClass('btn-primary');
		$('#approved').removeClass('btn-primary').addClass('btn-default');
		$('#rejected').removeClass('btn-primary').addClass('btn-default');
		$('#approved_request').hide();
		$('#rejected_request').hide();
		$('#pending_request').show();
		$('#purchase_request-table').datagrid({
			url:'<?php  echo site_url('purchase_request/admin/purchase_request/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
			
			
		});
	});
	$('#rejected').click(function(){
		$(this).removeClass('btn-default').addClass('btn-primary');
		$('#pending').removeClass('btn-primary').addClass('btn-default');
		$('#approved').removeClass('btn-primary').addClass('btn-default');
		$('#approved_request').hide();
		$('#pending_request').hide();
		$('#rejected_request').show();
		$('#rejected_request-table').datagrid({
			url:'<?php  echo site_url('purchase_request/admin/purchase_request/rejected_json')?>',
			height:'auto',
			width:'auto',
			
			
		});
	});
	function this_log(index)
	{
		console.log(index);
	}
	function getActions(value,row,index)
	{
		var form = '<form id="pending_form-'+index+'"><input type="number" class="form-control" style="color:#000;" name="quantity" placeholder="Approved Qty"/><input type="text"  class="form-control" style="color:#000;" name="remarks" placeholder="Remarks"/><input type="hidden" name="pending_status" id="pending_status"><input type="hidden" name="id" value="'+row.id+'"><input type="hidden" name="requested_quantity" value="'+row.requested_quantity+'"><input type="hidden" name="request_cost" value="'+row.request_cost+'"></form><span class="btn btn-default btn-sm" onclick="approve('+index+')">Approve</span><span class="btn btn-danger btn-sm" onclick="reject('+index+')">Reject</span>';
		// var d = '<a href="#" onclick="removepurchase_request('+index+')" class="btn btn-danger btn-sm" iconcls="icon-remove"  title="<?php  echo lang('delete_purchase_request')?>">Delete</a>';
		return form;		
	}
	function getTotal(value,row,index)
	{
		var t = row.request_cost * row.requested_quantity;
		//var e = '<a href="#" onclick="edit('+index+')" class="btn btn-default btn-sm"  title="<?php  echo lang('edit_material')?>">Edit</a>';
		return t;		
	}


	function approve(index)
	{
		var row = $('#purchase_request-table').datagrid('getRows')[index];
		$('#pending_status').val('0');
		// this_log(row);
		if(row)
		{
			$.post('<?php echo site_url('purchase_request/admin/purchase_request/pending_request_remark'); ?>',$('#pending_form-'+index+'').serialize(),function(data){
				$('#purchase_request-table').datagrid('reload');
			},'json');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}	
	}
	function reject(index)
	{
		$('#pending_status').val('1');
		var row = $('#purchase_request-table').datagrid('getRows')[index];
		this_log(row);
		if(row)
		{
			$.post('<?php echo site_url('purchase_request/admin/purchase_request/pending_request_remark'); ?>',$('#pending_form-'+index+'').serialize(),function(data){
				$('#purchase_request-table').datagrid('reload');
			},'json');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}	
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-purchase_request').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_purchase_request')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		console.log(index);
		$('#purchase_request-search-form').form('clear');
		var row = $('#purchase_request-table').datagrid('getRows')[index];
		if (row){
			$('#form-purchase_request').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_purchase_request')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	

	function removepurchase_request(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#purchase_request-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('purchase_request/admin/purchase_request/delete_json')?>', {id:[row.id]}, function(){
					$('#purchase_request-table').datagrid('deleteRow', index);
					$('#purchase_request-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#purchase_request-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('purchase_request/admin/purchase_request/delete_json')?>',{id:selected},function(data){
						$('#purchase_request-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-purchase_request').form('submit',{
			url: '<?php  echo site_url('purchase_request/admin/purchase_request/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-purchase_request').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#purchase_request-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close

		});		
		
	}
	
	
</script>