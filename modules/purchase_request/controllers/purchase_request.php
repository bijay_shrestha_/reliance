<?php

class Purchase_request extends Member_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->module_model('purchase_request','purchase_request_model');
		$this->load->module_model('material','material_model');
		$this->load->module_model('material_dealer','material_dealer_model');
		$this->load->module_model('stock','stock_model');
		$this->load->module_model('inventory','inventory_model');
		$this->lang->module_load('purchase_request','purchase_request');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		// Display Page
		$this->db->order_by('material_name');
		$data['materials'] = $this->material_model->getMaterials(array('status'=>1))->result_array();
		$this->purchase_request_model->joins = array('MATERIALS','DEALERS');
		$this->db->order_by('id','desc');
		$data['pending_requests'] = $this->purchase_request_model->getPurchaseRequests(array('approve' => 0))->result_array();
		$data['pending_no'] = $this->purchase_request_model->count(array('approve' => 0));
		$this->db->order_by('id','desc');
		$data['approved_requests'] = $this->purchase_request_model->getPurchaseRequests(array('approve' => 1))->result_array();
		$data['approved_no'] = $this->purchase_request_model->count(array('approve' => 1,'purchase_id' => 0));
		$this->db->order_by('id','desc');
		$data['disapproved_requests'] = $this->purchase_request_model->getPurchaseRequests(array('approve' => 2))->result_array();
		$data['disapproved_no'] = $this->purchase_request_model->count(array('approve' => 2));
		$data['header'] = 'Purchase Request';
		$data['view_page'] = "purchase_request/index";
		$data['module'] = 'purchase_request';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->purchase_request_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->purchase_request_model->getPurchaseRequests(array('approve' => 0))->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function approved_json()
	{
		$this->_get_search_param();	
		$total=$this->purchase_request_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->purchase_request_model->getPurchaseRequests(array('approve' => 1))->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	public function rejected_json()
	{
		$this->_get_search_param();	
		$total=$this->purchase_request_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->purchase_request_model->getPurchaseRequests(array('approve' => 2))->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['purchase_id']!='')?$this->db->where('purchase_id',$params['search']['purchase_id']):'';
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
			($params['search']['dealer_id']!='')?$this->db->where('dealer_id',$params['search']['dealer_id']):'';
			($params['search']['requested_quantity']!='')?$this->db->where('requested_quantity',$params['search']['requested_quantity']):'';
			($params['search']['accepted_quantity']!='')?$this->db->where('accepted_quantity',$params['search']['accepted_quantity']):'';
			($params['search']['request_cost']!='')?$this->db->where('request_cost',$params['search']['request_cost']):'';
			($params['search']['accepted_cost']!='')?$this->db->where('accepted_cost',$params['search']['accepted_cost']):'';
			($params['search']['approve']!='')?$this->db->where('approve',$params['search']['approve']):'';
			($params['search']['requested_remarks']!='')?$this->db->like('requested_remarks',$params['search']['requested_remarks']):'';
			($params['search']['approved_remarks']!='')?$this->db->like('approved_remarks',$params['search']['approved_remarks']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}


	}

	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
				"' and '".date('Y-m-d',strtotime($to))."')");
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}	

	public function combo_json()
	{
		$rows=$this->purchase_request_model->getPurchaseRequests()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->purchase_request_model->delete('PURCHASE_REQUEST',array('id'=>$row));
			endforeach;
		}else{
			$data['success'] = $this->purchase_request_model->delete('PURCHASE_REQUEST',array('id'=>$id));
			echo json_encode($data);
		}
	}    

	public function save()
	{
        // echo '<pre>';
        // print_r($this->input->post());
        $datas=$this->_get_posted_data(); //Retrive Posted Data	
        // print_r($datas);exit;
        // $i = 0;
		foreach($datas as $data){
	        if(!$data['id'])
	        {
	        	$success=$this->purchase_request_model->insert('PURCHASE_REQUEST',$data);
	        }
	        else
	        {
	        	$success=$this->purchase_request_model->update('PURCHASE_REQUEST',$data,array('id'=>$data['id']));
	        }
        }
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }
    public function editRequest(){
        $data=$this->_get_edit_posted_data(); //Retrive Posted Data	
    	// echo '<pre>';
    	// print_r($data);exit;
        
    	$success=$this->purchase_request_model->update('PURCHASE_REQUEST',$data,array('id'=>$data['id']));
	    
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$ids = $this->input->post('id');
    	$purchase_ids = $this->input->post('purchase_id');
    	$material_ids = $this->input->post('material_id');
    	$dealer_ids = $this->input->post('dealer_id');
    	$requested_quantities = $this->input->post('requested_quantity');
    	$accepted_quantities = $this->input->post('accepted_quantity');
    	$requested_dates_nepali = $this->input->post('requested_date');
    	$requested_dates_english = $this->input->post('requested_date_english');
    	$request_costs = $this->input->post('request_cost');
    	$accepted_costs = $this->input->post('accepted_cost');
    	$approves = $this->input->post('approve');
    	$requested_remarks = $this->input->post('requested_remarks');
    	$approved_remarks = $this->input->post('approved_remarks');
    	$unit_cost = $this->input->post('cost_per_unit');

    	$i=0;
    	foreach($material_ids as $material_id){
    		$data[$i]['id'] = $ids[$i];
    		$data[$i]['material_id'] = $material_id;
    		$data[$i]['dealer_id'] = $dealer_ids[$i];
    		$data[$i]['requested_quantity'] = $requested_quantities[$i];
    		$data[$i]['requested_date_nepali'] = $requested_dates_nepali[$i];
    		$data[$i]['requested_date_english'] = $requested_dates_english[$i];
    		$data[$i]['request_cost'] = $unit_cost[$i];
    		$data[$i]['requested_remarks'] = $requested_remarks[$i];
    		$data[$i]['unit_cost'] = $unit_cost[$i];
    		$i++;
    	}
    	return $data;
    }

    private function _get_edit_posted_data()
    {
    	$data=array();
    	$id = $this->input->post('request_id');
    	
    		$data['id'] = $this->input->post('request_id');
    		$data['material_id'] = $this->input->post('material_id');
    		$data['dealer_id'] = $this->input->post('dealer_id');
    		$data['requested_quantity'] = $this->input->post('requested_quantity');
    		$data['requested_date_nepali'] = $this->input->post('requested_date');
    		$data['requested_date_english'] = $this->input->post('requested_dates_english');
    		$data['request_cost'] = $this->input->post('request_costs');
    		$data['requested_remarks'] = $this->input->post('remark');
    		$data['unit_cost'] = $this->input->post('cost_per_unit');
    	
    	return $data;
    }

    public function getdata(){
    	$id = $this->input->post('id');
    	$this->material_dealer_model->joins = array('DEALERS');
    	$this->db->order_by('dealer_name','asc');
    	$data['dealers'] = $this->material_dealer_model->getMaterialDealers(array('material_id'=>$id))->result_array();
    	$material = $this->material_model->getMaterials(array('material_id' =>$id))->row_array();
		$data['unit'] = $material['unit'];
		$material = $this->inventory_model->getinventories(array('material_id' =>$id))->row_array();
		if($material){
			$data['stock'] = $material['current_stock'];
		}else{
			$data['stock'] = '0';
		}
		echo json_encode($data);
    }
    public function add_row(){
    	$data['i'] = $this->input->post('index');
    	$this->db->order_by('material_name');
    	$data['materials'] = $this->material_model->getMaterials()->result_array();
    	$this->load->view('purchase_request/add_more',$data);
    }
    public function edit(){
    	// echo '<pre>';print_r($this->input->post());exit;
    	$data['requested_quantity'] = floatval(str_replace(",","",$this->input->post('request_quantity')));
    	$data['unit_cost'] = floatval(str_replace(",","",$this->input->post('unit_cost')));;
    	$id = $this->input->post('id');
    	$success = $this->purchase_request_model->update('PURCHASE_REQUEST',$data,array('id'=>$id));
    	echo $this->db->last_query();
    	echo json_encode(array('success'=>$success));
    	
    }
    //get purchase request for modal
    public function getPurchaseRequest(){
    	$id = $this->input->post('id');
    	$this->purchase_request_model->joins = array('MATERIALS');
    	$result = $this->purchase_request_model->getPurchaseRequests(array('id'=>$id))->row_array();
    	$stock = $this->inventory_model->getinventories(array('material_id'=>$result['material_id']))->row_array();
    	$this->material_dealer_model->joins = array('DEALERS');
    	$dealers = $this->material_dealer_model->getMaterialDealers(array('material_dealers.material_id'=>$result['material_id']))->result_array();
    	
    	$result['dealers'] = "<select name='dealer_id' id='edit-dealer-id' class='form-control'>";
    	foreach($dealers as $dealer){
    		$result['dealers'] .= '<option value="'.$dealer['dealer_id'].'">'. $dealer['dealer_name'].'</option>';
    	}
    	$result['dealers'] .= '</select>';
    	$result['current_stock'] = $stock['current_stock'];
    	echo json_encode($result);
    }
}