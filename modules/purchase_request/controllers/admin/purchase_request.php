<?php

class Purchase_request extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->module_model('purchase_request','purchase_request_model');
		$this->lang->module_load('purchase_request','purchase_request');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		// Display Page
		$data['header'] = 'purchase_request';
		$data['page'] = $this->config->item('template_admin') . "purchase_request/index";
		$data['module'] = 'purchase_request';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->purchase_request_model->joins= array('MATERIALS','DEALERS');
		$this->_get_search_param();	
		$total=$this->purchase_request_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->purchase_request_model->getPurchaseRequests(array('approve' => 0))->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}

	public function approved_json()
	{
		$this->purchase_request_model->joins= array('MATERIALS','DEALERS');
		$this->_get_search_param();	
		$total=$this->purchase_request_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->purchase_request_model->getPurchaseRequests(array('approve' => 1))->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	public function rejected_json()
	{
		$this->purchase_request_model->joins= array('MATERIALS','DEALERS');
		$this->_get_search_param();	
		$total=$this->purchase_request_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->purchase_request_model->getPurchaseRequests(array('approve' => 2))->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			// ($params['search']['purchase_id']!='')?$this->db->where('purchase_id',$params['search']['purchase_id']):'';
			($params['search']['material_id']!='')?$this->db->like('material_name',$params['search']['material_id']):'';
			($params['search']['dealer_id']!='')?$this->db->like('dealer_name',$params['search']['dealer_id']):'';
			($params['search']['requested_quantity']!='')?$this->db->where('requested_quantity',$params['search']['requested_quantity']):'';
			// ($params['search']['accepted_quantity']!='')?$this->db->where('accepted_quantity',$params['search']['accepted_quantity']):'';
			// ($params['search']['request_cost']!='')?$this->db->where('request_cost',$params['search']['request_cost']):'';
			// ($params['search']['accepted_cost']!='')?$this->db->where('accepted_cost',$params['search']['accepted_cost']):'';
			// ($params['search']['approve']!='')?$this->db->where('approve',$params['search']['approve']):'';
			// ($params['search']['requested_remarks']!='')?$this->db->like('requested_remarks',$params['search']['requested_remarks']):'';
			// ($params['search']['approved_remarks']!='')?$this->db->like('approved_remarks',$params['search']['approved_remarks']):'';

		}  
		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}


	}

	
	private function _datewise($field,$from,$to)
	{
		if(!empty($from) && !empty($to))
		{
			$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
				"' and '".date('Y-m-d',strtotime($to))."')");
		}
		else if(!empty($from))
		{
			$this->db->like($field,date('Y-m-d',strtotime($from)));				
		}		
	}	

	public function combo_json()
	{
		$rows=$this->purchase_request_model->getPurchaseRequests()->result_array();
		echo json_encode($rows);    	
	}    

	public function delete_json()
	{
		$id=$this->input->post('id');
		if($id && is_array($id))
		{
			foreach($id as $row):
				$this->purchase_request_model->delete('PURCHASE_REQUEST',array('id'=>$row));
			endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		
        if(!$this->input->post('id'))
        {
        	$success=$this->purchase_request_model->insert('PURCHASE_REQUEST',$data);
        }
        else
        {
        	$success=$this->purchase_request_model->update('PURCHASE_REQUEST',$data,array('id'=>$data['id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }

    private function _get_posted_data()
    {
    	$data=array();
    	$data['id'] = $this->input->post('id');
    	// $data['purchase_id'] = $this->input->post('purchase_id');
    	// $data['material_id'] = $this->input->post('material_id');
    	// $data['dealer_id'] = $this->input->post('dealer_id');
    	$data['requested_quantity'] = $this->input->post('requested_quantity');
    	// $data['accepted_quantity'] = $this->input->post('accepted_quantity');
    	// $data['requested_date_nepali'] = $this->input->post('requested_date_nepali');
    	// $data['requested_date_english'] = $this->input->post('requested_date_english');
    	// $data['accepted_date_nepali'] = $this->input->post('accepted_date_nepali');
    	// $data['accepted_date_english'] = $this->input->post('accepted_date_english');
    	$data['request_cost'] = $this->input->post('request_cost');
    	// $data['accepted_cost'] = $this->input->post('accepted_cost');
    	// $data['approve'] = $this->input->post('approve');
    	// $data['requested_remarks'] = $this->input->post('requested_remarks');
    	// $data['approved_remarks'] = $this->input->post('approved_remarks');

    	return $data;
    }
    public function pending_request_remark()
    {
    	// echo'<pre>';print_r($this->input->post());exit;
    	$data = $this->input->post();
    	$success = '';
    	if(!$data['pending_status']) /*Accepted*/
    	{
    		$setVal = array(
    			'accepted_quantity' =>	$data['quantity'],
    			'accepted_cost'		=>	($data['request_cost'] / $data['requested_quantity']) * $data['quantity'],
    			'approved_remarks'	=>	$data['remarks'],
    			'approve'			=>	1,
    			'accepted_date_english'=>	date('Y-m-d'),
    			'accepted_date_nepali'=>	date('Y-m-d'),
    			);
    		$success = $this->db->update('tbl_purchase_request',$setVal,array('id'=>$data['id']));
    	}
    	else  /*Rejected*/
    	{
    		$setVal = array(
    			'approved_remarks' => $data['remarks'],
    			'approve'			=> 2,
    			'accepted_date_english'=>	date('Y-m-d'),
    			'accepted_date_nepali'=>	date('Y-m-d')
    			);
    		$success = $this->db->update('tbl_purchase_request',$setVal,array('id'=>$data['id']));
    	}
    	echo json_encode($success);
    }



}

/*
	[quantity] => 
    [remarks] => 
    [pending_status] => 1
*/