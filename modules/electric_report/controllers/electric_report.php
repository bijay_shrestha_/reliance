<?php

class Electric_report extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('electric_report','electric_report_model');
        $this->lang->module_load('electric_report','electric_report');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['rows'] = $this->electric_report_model->getElectricReports()->result_array();
		// Display Page
		$data['header'] = 'Electric Report';
		$data['view_page'] = "electric_report/index";
		$data['module'] = 'electric_report';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->electric_report_model->count();
		paging('');
		$this->_get_search_param();	
		$rows=$this->electric_report_model->getElectricReports()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['id']!='')?$this->db->where('id',$params['search']['id']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['shift']!='')?$this->db->like('shift',$params['search']['shift']):'';
($params['search']['power_cut']!='')?$this->db->where('power_cut',$params['search']['power_cut']):'';
($params['search']['breakdown']!='')?$this->db->where('breakdown',$params['search']['breakdown']):'';
($params['search']['cement_mill_operation_nea']!='')?$this->db->where('cement_mill_operation_nea',$params['search']['cement_mill_operation_nea']):'';
($params['search']['cement_mill_operation_dg']!='')?$this->db->where('cement_mill_operation_dg',$params['search']['cement_mill_operation_dg']):'';
($params['search']['total']!='')?$this->db->where('total',$params['search']['total']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->electric_report_model->getElectricReports()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->electric_report_model->delete('ELECTRIC_REPORT',array('id'=>$row));
            endforeach;
		}else{
			$success = $this->electric_report_model->delete('ELECTRIC_REPORT',array('id'=>$id));
		}
		echo json_encode($success);
	}    

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data		
		// echo '<pre>';print_r($data);exit;

        if(!$this->input->post('id'))
        {
            $success=$this->electric_report_model->insert('ELECTRIC_REPORT',$data);
        }
        else
        {
            $success=$this->electric_report_model->update('ELECTRIC_REPORT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['shift'] = $this->input->post('shift');
$data['power_cut'] = $this->input->post('power_cut_hr') * 60 +  $this->input->post('power_cut_min');
$data['breakdown'] = $this->input->post('breakdown_hr') * 60 + $this->input->post('breakdown_min');
$data['cement_mill_operation_nea'] = $this->input->post('cement_mill_operation_nea_hr') * 60 + $this->input->post('cement_mill_operation_nea_min');
$data['cement_mill_operation_dg'] = $this->input->post('cement_mill_operation_dg_hr') * 60 + $this->input->post('cement_mill_operation_dg_min');
$data['only_lighiting_nea'] = $this->input->post('only_lighiting_nea_hr') * 60 + $this->input->post('only_lighiting_nea_min');
$data['reason'] = $this->input->post('reason');
$data['total'] = $this->input->post('total');

        return $data;
   }
   
   	public function get_value()
   	{
   		$where['id'] = $this->input->post('id');
   		$data = $this->electric_report_model->getElectricReports($where)->row_array();
   		echo json_encode($data);
   	}
	    
}