<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('purchase_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="purchase-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('request_id')?></label>:</td>
<td><input type="text" name="search[request_id]" id="search_request_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('material_id')?></label>:</td>
<td><input type="text" name="search[material_id]" id="search_material_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('good_return')?></label>:</td>
<td><input type="radio" name="search[good_return]" id="search_good_return1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[good_return]" id="search_good_return0" value="0"/><?php echo lang('general_no')?></td>
<td><label><?php echo lang('department_id')?></label>:</td>
<td><input type="text" name="search[department_id]" id="search_department_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('headoffice_purchase')?></label>:</td>
<td><input type="radio" name="search[headoffice_purchase]" id="search_headoffice_purchase1" value="1"/><?php echo lang('general_yes')?>
									<input type="radio" name="search[headoffice_purchase]" id="search_headoffice_purchase0" value="0"/><?php echo lang('general_no')?></td>
<td><label><?php echo lang('dealer_id')?></label>:</td>
<td><input type="text" name="search[dealer_id]" id="search_dealer_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('headoffice_unitcost')?></label>:</td>
<td><input type="text" name="search[headoffice_unitcost]" id="search_headoffice_unitcost"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('date_en')?></label>:</td>
<td><input type="text" name="date[date_en][from]" id="search_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_en][to]" id="search_date_en_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('date-np')?></label>:</td>
<td><input type="text" name="search[date-np]" id="search_date-np"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('current_stock')?></label>:</td>
<td><input type="text" name="search[current_stock]" id="search_current_stock"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('added_qty')?></label>:</td>
<td><input type="text" name="search[added_qty]" id="search_added_qty"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="purchase-table" data-options="pagination:true,title:'<?php  echo lang('purchase')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'purchase_id',sortable:true" width="30"><?php echo lang('purchase_id')?></th>
<th data-options="field:'request_id',sortable:true" width="50"><?php echo lang('request_id')?></th>
<th data-options="field:'material_id',sortable:true" width="50"><?php echo lang('material_id')?></th>
<th data-options="field:'good_return',sortable:true" width="50"><?php echo lang('good_return')?></th>
<th data-options="field:'department_id',sortable:true" width="50"><?php echo lang('department_id')?></th>
<th data-options="field:'headoffice_purchase',sortable:true" width="50"><?php echo lang('headoffice_purchase')?></th>
<th data-options="field:'dealer_id',sortable:true" width="50"><?php echo lang('dealer_id')?></th>
<th data-options="field:'headoffice_unitcost',sortable:true" width="50"><?php echo lang('headoffice_unitcost')?></th>
<th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th>
<th data-options="field:'date-np',sortable:true" width="50"><?php echo lang('date-np')?></th>
<th data-options="field:'current_stock',sortable:true" width="50"><?php echo lang('current_stock')?></th>
<th data-options="field:'added_qty',sortable:true" width="50"><?php echo lang('added_qty')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_purchase')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_purchase')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit purchase form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-purchase" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('request_id')?>:</label></td>
					  <td width="66%"><input name="request_id" id="request_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('material_id')?>:</label></td>
					  <td width="66%"><input name="material_id" id="material_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('good_return')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="good_return" id="good_return1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="good_return" id="good_return0" /><?php echo lang("general_no")?></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('department_id')?>:</label></td>
					  <td width="66%"><input name="department_id" id="department_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('headoffice_purchase')?>:</label></td>
					  <td width="66%"><input type="radio" value="1" name="headoffice_purchase" id="headoffice_purchase1" /><?php echo lang("general_yes")?> <input type="radio" value="0" name="headoffice_purchase" id="headoffice_purchase0" /><?php echo lang("general_no")?></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('dealer_id')?>:</label></td>
					  <td width="66%"><input name="dealer_id" id="dealer_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('headoffice_unitcost')?>:</label></td>
					  <td width="66%"><input name="headoffice_unitcost" id="headoffice_unitcost" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date-np')?>:</label></td>
					  <td width="66%"><input name="date-np" id="date-np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('current_stock')?>:</label></td>
					  <td width="66%"><input name="current_stock" id="current_stock" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('added_qty')?>:</label></td>
					  <td width="66%"><input name="added_qty" id="added_qty" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="purchase_id" id="purchase_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#purchase-search-form').form('clear');
			$('#purchase-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#purchase-table').datagrid({
				queryParams:{data:$('#purchase-search-form').serialize()}
				});
		});		
		$('#purchase-table').datagrid({
			url:'<?php  echo site_url('purchase/admin/purchase/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_purchase')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removepurchase('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_purchase')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-purchase').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_purchase')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#purchase-table').datagrid('getRows')[index];
		if (row){
			$('#form-purchase').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_purchase')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removepurchase(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#purchase-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('purchase/admin/purchase/delete_json')?>', {id:[row.purchase_id]}, function(){
					$('#purchase-table').datagrid('deleteRow', index);
					$('#purchase-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#purchase-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].purchase_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('purchase/admin/purchase/delete_json')?>',{id:selected},function(data){
						$('#purchase-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-purchase').form('submit',{
			url: '<?php  echo site_url('purchase/admin/purchase/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-purchase').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#purchase-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>