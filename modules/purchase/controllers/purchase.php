<?php

class Purchase extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('purchase','purchase_model');
        $this->lang->module_load('purchase','purchase');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'purchase';
		$data['view_page'] ="purchase/index";
		$data['module'] = 'purchase';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->purchase_model->count();
		paging('purchase_id');
		$this->_get_search_param();	
		$rows=$this->purchase_model->getPurchases()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['request_id']!='')?$this->db->where('request_id',$params['search']['request_id']):'';
($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
(isset($params['search']['good_return']))?$this->db->where('good_return',$params['search']['good_return']):'';
($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
(isset($params['search']['headoffice_purchase']))?$this->db->where('headoffice_purchase',$params['search']['headoffice_purchase']):'';
($params['search']['dealer_id']!='')?$this->db->where('dealer_id',$params['search']['dealer_id']):'';
($params['search']['headoffice_unitcost']!='')?$this->db->where('headoffice_unitcost',$params['search']['headoffice_unitcost']):'';
($params['search']['date-np']!='')?$this->db->like('date-np',$params['search']['date-np']):'';
($params['search']['current_stock']!='')?$this->db->where('current_stock',$params['search']['current_stock']):'';
($params['search']['added_qty']!='')?$this->db->where('added_qty',$params['search']['added_qty']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->purchase_model->getPurchases()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->purchase_model->delete('PURCHASE',array('purchase_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('purchase_id'))
        {
            $success=$this->purchase_model->insert('PURCHASE',$data);
        }
        else
        {
            $success=$this->purchase_model->update('PURCHASE',$data,array('purchase_id'=>$data['purchase_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['purchase_id'] = $this->input->post('purchase_id');
$data['request_id'] = $this->input->post('request_id');
$data['material_id'] = $this->input->post('material_id');
$data['good_return'] = $this->input->post('good_return');
$data['department_id'] = $this->input->post('department_id');
$data['headoffice_purchase'] = $this->input->post('headoffice_purchase');
$data['dealer_id'] = $this->input->post('dealer_id');
$data['headoffice_unitcost'] = $this->input->post('headoffice_unitcost');
$data['date_en'] = $this->input->post('date_en');
$data['date-np'] = $this->input->post('date-np');
$data['current_stock'] = $this->input->post('current_stock');
$data['added_qty'] = $this->input->post('added_qty');

        return $data;
   }
   
   public function getAllPurchase(){
   	$this->db->group_by('material_id');
   	$result = $this->purchase_model->getAllPurchases()->result_array();
   	echo '<pre>';
   	print_r($result);
   }
   	
	    
}