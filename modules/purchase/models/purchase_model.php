<?php
class Purchase_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('PURCHASE'=>$this->prefix.'purchase','MATERIALS'=>$this->prefix.'materials','DEPARTMENTS'=>$this->prefix.'departments','DEALERS'=>$this->prefix.'dealers','PURCHASE_REQUEST'=>$this->prefix.'purchase_request','REPORT_MATERIAL'=>$this->prefix.'report_material');
		$this->_JOINS=array('KEY'=>array('join_type'=>'LEFT','join_field'=>'join1.id=join2.id',
                                           'select'=>'field_names','alias'=>'alias_name'),
                            'MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'purchases.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
                            'DEPARTMENTS'=>array('join_type'=>'LEFT','join_field'=>'purchases.department_id=departments.id',
                                           'select'=>'departments.*','alias'=>'departments'),
                            'DEALERS'=>array('join_type'=>'LEFT','join_field'=>'purchases.dealer_id=dealers.dealer_id',
                                           'select'=>'dealers.*','alias'=>'dealers'),
                            'PURCHASE_REQUEST'=>array('join_type'=>'LEFT','join_field'=>'purchases.request_id=purchase_request.id',
                                           'select'=>'purchase_request.*','alias'=>'purchase_request'),
                            'REPORT_MATERIAL'=>array('join_type'=>'LEFT','join_field'=>'purchases.material_id=report_material.material_id',
                                           'select'=>'report_material.*','alias'=>'report_material'),
                           
                            );        
    }
    
    public function getPurchases($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''),$fields = NULL)
    {
        if($fields == NULL){
            $fields='purchases.*,purchases.purchase_id pid';
        }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['PURCHASE']. ' purchases');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['PURCHASE'].' purchases');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
    // get all purchase
    public function getAllPurchases($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='purchases.*,SUM(added_qty) as total_purchase';
       
        foreach($this->joins as $key):
            $fields=$fields . ','.$this->_JOINS[$key]['select'];
        endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['PURCHASE']. ' purchases');
        
        foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;         
        
        (! is_null($where))?$this->db->where($where):NULL;
        (! is_null($order_by))?$this->db->order_by($order_by):NULL;

        if( ! is_null($limit['limit']))
        {
            $this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
        }
        return $this->db->get();        
    }
}