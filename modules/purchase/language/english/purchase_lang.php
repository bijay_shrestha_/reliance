<?php


$lang['purchase_id'] = 'Purchase Id';
$lang['request_id'] = 'Request Id';
$lang['material_id'] = 'Material Id';
$lang['good_return'] = 'Good Return';
$lang['department_id'] = 'Department Id';
$lang['headoffice_purchase'] = 'Headoffice Purchase';
$lang['dealer_id'] = 'Dealer Id';
$lang['headoffice_unitcost'] = 'Headoffice Unitcost';
$lang['date_en'] = 'Date En';
$lang['date-np'] = 'Date-np';
$lang['current_stock'] = 'Current Stock';
$lang['added_qty'] = 'Added Qty';

$lang['create_purchase']='Create Purchase';
$lang['edit_purchase']='Edit Purchase';
$lang['delete_purchase']='Delete Purchase';
$lang['purchase_search']='Purchase Search';

$lang['purchase']='Purchase';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

