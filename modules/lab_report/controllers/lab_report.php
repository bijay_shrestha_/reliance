<?php

class Lab_report extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('lab_report','lab_report_model');
        $this->load->module_model('mst_lab_requirement','mst_lab_requirement_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->load->module_model('brand','brand_model');
        $this->lang->module_load('lab_report','lab_report');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index($no_of_days = NULL)
	{
		$where_days = array();
		if($no_of_days != NULL){
			$days_before =  $no_of_days-1;
			$required_date = strtotime('-' . ($days_before) .' days');
			$where_days['sample_date_en <='] = date('Y-m-d',$required_date);
			$where_days['strength28'] = NULL;
		}
		$this->lab_report_model->joins = array('BRAND');
		$data['rows']=$this->lab_report_model->getLabReports($where_days)->result_array();
		$where['type'] = 1;
		$data['testing_parameter'] = $this->mst_lab_requirement_model->getMstLabRequirements($where)->result_array();
		$where['type'] = 2;
		$data['physical_parameter'] = $this->mst_lab_requirement_model->getMstLabRequirements($where)->result_array();
		$data['brand_types'] = $this->brand_type_model->getBrandTypes()->result_array();
		$data['brands'] = $this->brand_model->getBrands()->result_array();

		// for 3 days
		$required_date = strtotime('-2 days');
		$where_days['sample_date_en <='] = date('Y-m-d',$required_date);
		$where_days['strength3'] = NULL;
		$data['report_for_update']['3'] = $this->lab_report_model->count($where_days);

		// for 7 days
		$required_date = strtotime('-6 days');
		$where_days['sample_date_en <='] = date('Y-m-d',$required_date);
		$where_days['strength7'] = NULL;
		$data['report_for_update']['7'] = $this->lab_report_model->count($where_days);

		// for 28 days
		$required_date = strtotime('-27 days');
		$where_days['sample_date_en <='] = date('Y-m-d',$required_date);
		$where_days['strength28'] = NULL;
		$data['report_for_update']['28'] = $this->lab_report_model->count($where_days);

		// Display Page
		$data['header'] = 'Lab Report';
		$data['view_page'] = "lab_report/index";
		$data['module'] = 'lab_report';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->lab_report_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->lab_report_model->getLabReports()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['sample_date_np']!='')?$this->db->like('sample_date_np',$params['search']['sample_date_np']):'';
($params['search']['loss_of_ignition']!='')?$this->db->where('loss_of_ignition',$params['search']['loss_of_ignition']):'';
($params['search']['magenisum_oxide']!='')?$this->db->where('magenisum_oxide',$params['search']['magenisum_oxide']):'';
($params['search']['insoluble_residue']!='')?$this->db->where('insoluble_residue',$params['search']['insoluble_residue']):'';
($params['search']['sulphur_trioxide']!='')?$this->db->where('sulphur_trioxide',$params['search']['sulphur_trioxide']):'';
($params['search']['chloride']!='')?$this->db->where('chloride',$params['search']['chloride']):'';
($params['search']['sulphide_sulphur']!='')?$this->db->where('sulphide_sulphur',$params['search']['sulphide_sulphur']):'';
($params['search']['fineness']!='')?$this->db->where('fineness',$params['search']['fineness']):'';
($params['search']['setting_time_initial']!='')?$this->db->where('setting_time_initial',$params['search']['setting_time_initial']):'';
($params['search']['setting_time_final']!='')?$this->db->where('setting_time_final',$params['search']['setting_time_final']):'';
($params['search']['expansion_lc_method']!='')?$this->db->where('expansion_lc_method',$params['search']['expansion_lc_method']):'';
($params['search']['expansion_ac_method']!='')?$this->db->where('expansion_ac_method',$params['search']['expansion_ac_method']):'';
($params['search']['strength1']!='')?$this->db->where('strength1',$params['search']['strength1']):'';
($params['search']['strength3']!='')?$this->db->where('strength3',$params['search']['strength3']):'';
($params['search']['strength7']!='')?$this->db->where('strength7',$params['search']['strength7']):'';
($params['search']['strength28']!='')?$this->db->where('strength28',$params['search']['strength28']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->lab_report_model->getLabReports()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->lab_report_model->delete('LAB_REPORT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->lab_report_model->insert('LAB_REPORT',$data);
        }
        else
        {
            $success=$this->lab_report_model->update('LAB_REPORT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['sample_date_np'] = $this->input->post('sample_date_np');
$data['sample_date_en'] = $this->input->post('sample_date_en');
$data['loss_of_ignition'] = $this->input->post('loss_of_ignition');
$data['magenisum_oxide'] = $this->input->post('magenisum_oxide');
$data['insoluble_residue'] = $this->input->post('insoluble_residue');
$data['sulphur_trioxide'] = $this->input->post('sulphur_trioxide');
$data['chloride'] = $this->input->post('chloride');
$data['sulphide_sulphur'] = $this->input->post('sulphide_sulphur');
$data['fineness'] = $this->input->post('fineness');
$data['setting_time_initial'] = $this->input->post('setting_time_initial');
$data['setting_time_final'] = $this->input->post('setting_time_final');
$data['expansion_lc_method'] = $this->input->post('expansion_lc_method');
$data['expansion_ac_method'] = $this->input->post('expansion_ac_method');
$data['strength1'] = $this->input->post('strength1');
if($this->input->post('strength3')){
	$data['strength3'] = $this->input->post('strength3');
}
if($this->input->post('strength7')){
$data['strength7'] = $this->input->post('strength7');
}
if($this->input->post('strength28')){
$data['strength28'] = $this->input->post('strength28');
}
	
$data['brand_id'] = $this->input->post('brand_id');

        return $data;
   }

   public function get_value(){
   		$where['id'] = $this->input->post('id');
   		$data = $this->lab_report_model->getLabReports($where)->row_array();
   		echo json_encode($data);
   }
   
   	
	    
}