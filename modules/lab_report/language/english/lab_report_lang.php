<?php


$lang['id'] = 'Id';
$lang['date_np'] = 'Date Np';
$lang['date_en'] = 'Date En';
$lang['sample_date_np'] = 'Sample Date Np';
$lang['sample_date_en'] = 'Sample Date En';
$lang['loss_of_ignition'] = 'Loss Of Ignition';
$lang['magenisum_oxide'] = 'Magenisum Oxide';
$lang['insoluble_residue'] = 'Insoluble Residue';
$lang['sulphur_trioxide'] = 'Sulphur Trioxide';
$lang['chloride'] = 'Chloride';
$lang['sulphide_sulphur'] = 'Sulphide Sulphur';
$lang['fineness'] = 'Fineness';
$lang['setting_time_initial'] = 'Setting Time Initial';
$lang['setting_time_final'] = 'Setting Time Final';
$lang['expansion_lc_method'] = 'Expansion Lc Method';
$lang['expansion_ac_method'] = 'Expansion Ac Method';
$lang['strength1'] = 'Strength1';
$lang['strength3'] = 'Strength3';
$lang['strength7'] = 'Strength7';
$lang['strength28'] = 'Strength28';

$lang['create_lab_report']='Create Lab Report';
$lang['edit_lab_report']='Edit Lab Report';
$lang['delete_lab_report']='Delete Lab Report';
$lang['lab_report_search']='Lab Report Search';

$lang['lab_report']='Lab Report';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

