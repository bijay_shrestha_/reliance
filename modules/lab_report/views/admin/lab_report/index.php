<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('lab_report_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="lab_report-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('date_np')?></label>:</td>
<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('date_en')?></label>:</td>
<td><input type="text" name="date[date_en][from]" id="search_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_en][to]" id="search_date_en_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('sample_date_np')?></label>:</td>
<td><input type="text" name="search[sample_date_np]" id="search_sample_date_np"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('sample_date_en')?></label>:</td>
<td><input type="text" name="date[sample_date_en][from]" id="search_sample_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[sample_date_en][to]" id="search_sample_date_en_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('loss_of_ignition')?></label>:</td>
<td><input type="text" name="search[loss_of_ignition]" id="search_loss_of_ignition"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('magenisum_oxide')?></label>:</td>
<td><input type="text" name="search[magenisum_oxide]" id="search_magenisum_oxide"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('insoluble_residue')?></label>:</td>
<td><input type="text" name="search[insoluble_residue]" id="search_insoluble_residue"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('sulphur_trioxide')?></label>:</td>
<td><input type="text" name="search[sulphur_trioxide]" id="search_sulphur_trioxide"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('chloride')?></label>:</td>
<td><input type="text" name="search[chloride]" id="search_chloride"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('sulphide_sulphur')?></label>:</td>
<td><input type="text" name="search[sulphide_sulphur]" id="search_sulphide_sulphur"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('fineness')?></label>:</td>
<td><input type="text" name="search[fineness]" id="search_fineness"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('setting_time_initial')?></label>:</td>
<td><input type="text" name="search[setting_time_initial]" id="search_setting_time_initial"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('setting_time_final')?></label>:</td>
<td><input type="text" name="search[setting_time_final]" id="search_setting_time_final"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('expansion_lc_method')?></label>:</td>
<td><input type="text" name="search[expansion_lc_method]" id="search_expansion_lc_method"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('expansion_ac_method')?></label>:</td>
<td><input type="text" name="search[expansion_ac_method]" id="search_expansion_ac_method"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('strength1')?></label>:</td>
<td><input type="text" name="search[strength1]" id="search_strength1"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('strength3')?></label>:</td>
<td><input type="text" name="search[strength3]" id="search_strength3"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('strength7')?></label>:</td>
<td><input type="text" name="search[strength7]" id="search_strength7"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('strength28')?></label>:</td>
<td><input type="text" name="search[strength28]" id="search_strength28"  class="easyui-numberbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="lab_report-table" data-options="pagination:true,title:'<?php  echo lang('lab_report')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'date_np',sortable:true" width="50"><?php echo lang('date_np')?></th>
<th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th>
<th data-options="field:'sample_date_np',sortable:true" width="50"><?php echo lang('sample_date_np')?></th>
<th data-options="field:'sample_date_en',sortable:true" width="50"><?php echo lang('sample_date_en')?></th>
<th data-options="field:'loss_of_ignition',sortable:true" width="50"><?php echo lang('loss_of_ignition')?></th>
<th data-options="field:'magenisum_oxide',sortable:true" width="50"><?php echo lang('magenisum_oxide')?></th>
<th data-options="field:'insoluble_residue',sortable:true" width="50"><?php echo lang('insoluble_residue')?></th>
<th data-options="field:'sulphur_trioxide',sortable:true" width="50"><?php echo lang('sulphur_trioxide')?></th>
<th data-options="field:'chloride',sortable:true" width="50"><?php echo lang('chloride')?></th>
<th data-options="field:'sulphide_sulphur',sortable:true" width="50"><?php echo lang('sulphide_sulphur')?></th>
<th data-options="field:'fineness',sortable:true" width="50"><?php echo lang('fineness')?></th>
<th data-options="field:'setting_time_initial',sortable:true" width="50"><?php echo lang('setting_time_initial')?></th>
<th data-options="field:'setting_time_final',sortable:true" width="50"><?php echo lang('setting_time_final')?></th>
<th data-options="field:'expansion_lc_method',sortable:true" width="50"><?php echo lang('expansion_lc_method')?></th>
<th data-options="field:'expansion_ac_method',sortable:true" width="50"><?php echo lang('expansion_ac_method')?></th>
<th data-options="field:'strength1',sortable:true" width="50"><?php echo lang('strength1')?></th>
<th data-options="field:'strength3',sortable:true" width="50"><?php echo lang('strength3')?></th>
<th data-options="field:'strength7',sortable:true" width="50"><?php echo lang('strength7')?></th>
<th data-options="field:'strength28',sortable:true" width="50"><?php echo lang('strength28')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_lab_report')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_lab_report')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit lab_report form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-lab_report" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datetimebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('sample_date_np')?>:</label></td>
					  <td width="66%"><input name="sample_date_np" id="sample_date_np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('sample_date_en')?>:</label></td>
					  <td width="66%"><input name="sample_date_en" id="sample_date_en" class="easyui-datetimebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('loss_of_ignition')?>:</label></td>
					  <td width="66%"><input name="loss_of_ignition" id="loss_of_ignition" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('magenisum_oxide')?>:</label></td>
					  <td width="66%"><input name="magenisum_oxide" id="magenisum_oxide" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('insoluble_residue')?>:</label></td>
					  <td width="66%"><input name="insoluble_residue" id="insoluble_residue" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('sulphur_trioxide')?>:</label></td>
					  <td width="66%"><input name="sulphur_trioxide" id="sulphur_trioxide" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('chloride')?>:</label></td>
					  <td width="66%"><input name="chloride" id="chloride" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('sulphide_sulphur')?>:</label></td>
					  <td width="66%"><input name="sulphide_sulphur" id="sulphide_sulphur" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('fineness')?>:</label></td>
					  <td width="66%"><input name="fineness" id="fineness" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('setting_time_initial')?>:</label></td>
					  <td width="66%"><input name="setting_time_initial" id="setting_time_initial" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('setting_time_final')?>:</label></td>
					  <td width="66%"><input name="setting_time_final" id="setting_time_final" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('expansion_lc_method')?>:</label></td>
					  <td width="66%"><input name="expansion_lc_method" id="expansion_lc_method" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('expansion_ac_method')?>:</label></td>
					  <td width="66%"><input name="expansion_ac_method" id="expansion_ac_method" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('strength1')?>:</label></td>
					  <td width="66%"><input name="strength1" id="strength1" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('strength3')?>:</label></td>
					  <td width="66%"><input name="strength3" id="strength3" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('strength7')?>:</label></td>
					  <td width="66%"><input name="strength7" id="strength7" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('strength28')?>:</label></td>
					  <td width="66%"><input name="strength28" id="strength28" class="easyui-numberbox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#lab_report-search-form').form('clear');
			$('#lab_report-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#lab_report-table').datagrid({
				queryParams:{data:$('#lab_report-search-form').serialize()}
				});
		});		
		$('#lab_report-table').datagrid({
			url:'<?php  echo site_url('lab_report/admin/lab_report/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_lab_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removelab_report('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_lab_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-lab_report').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_lab_report')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#lab_report-table').datagrid('getRows')[index];
		if (row){
			$('#form-lab_report').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_lab_report')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removelab_report(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#lab_report-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('lab_report/admin/lab_report/delete_json')?>', {id:[row.id]}, function(){
					$('#lab_report-table').datagrid('deleteRow', index);
					$('#lab_report-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#lab_report-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('lab_report/admin/lab_report/delete_json')?>',{id:selected},function(data){
						$('#lab_report-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-lab_report').form('submit',{
			url: '<?php  echo site_url('lab_report/admin/lab_report/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-lab_report').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#lab_report-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>