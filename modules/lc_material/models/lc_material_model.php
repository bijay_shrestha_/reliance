<?php
class Lc_material_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('LC_MATERIAL'=>$this->prefix.'lc_material','MATERIALS'=>$this->prefix.'materials','LC'=>$this->prefix.'lc');
		$this->_JOINS=array('MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'lc_materials.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
                            'LC'=>array('join_type'=>'LEFT','join_field'=>'lc_materials.lc_id=lc.id',
                                           'select'=>'lc.*','alias'=>'lc'),
                           
                            );        
    }
    
    public function getLcMaterials($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='lc_materials.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['LC_MATERIAL']. ' lc_materials');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['LC_MATERIAL'].' lc_materials');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}