<?php

class Lc_material extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('lc_material','lc_material_model');
        $this->lang->module_load('lc_material','lc_material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'lc_material';
		$data['page'] = $this->config->item('template_admin') . "lc_material/index";
		$data['module'] = 'lc_material';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->lc_material_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->lc_material_model->getLcMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['quantity']!='')?$this->db->where('quantity',$params['search']['quantity']):'';
($params['search']['price']!='')?$this->db->where('price',$params['search']['price']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->lc_material_model->getLcMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->lc_material_model->delete('LC_MATERIAL',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->lc_material_model->insert('LC_MATERIAL',$data);
        }
        else
        {
            $success=$this->lc_material_model->update('LC_MATERIAL',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['material_id'] = $this->input->post('material_id');
$data['quantity'] = $this->input->post('quantity');
$data['price'] = $this->input->post('price');

        return $data;
   }
   
   	
	    
}