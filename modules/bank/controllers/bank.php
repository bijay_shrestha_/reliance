<?php

class Bank extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('bank','bank_model');
        $this->lang->module_load('bank','bank');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['rows'] = $this->bank_model->getBanks()->result_array();
		// Display Page
		$data['header'] = 'Bank';
		$data['view_page'] = "bank/index";
		$data['module'] = 'bank';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->bank_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->bank_model->getBanks()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['bank_name']!='')?$this->db->like('bank_name',$params['search']['bank_name']):'';
($params['search']['phone']!='')?$this->db->like('phone',$params['search']['phone']):'';
($params['search']['email']!='')?$this->db->like('email',$params['search']['email']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->bank_model->getBanks()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->bank_model->delete('BANK',array('id'=>$row));
            endforeach;
		}else{
			$this->bank_model->delete('BANK',array('id'=>$id));
		}
		echo json_encode(array('success'=>'success'));
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->bank_model->insert('BANK',$data);
        }
        else
        {
            $success=$this->bank_model->update('BANK',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['bank_name'] = $this->input->post('bank_name');
$data['phone'] = $this->input->post('phone');
$data['email'] = $this->input->post('email');

        return $data;
   }

   public function get_value(){
   		$where['id'] = $this->input->post('id');
   		$data = $this->bank_model->getBanks($where)->row_array();
   		echo json_encode($data);
   }
   
   	
	    
}