<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('bank_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="bank-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('bank_name')?></label>:</td>
<td><input type="text" name="search[bank_name]" id="search_bank_name"  class="easyui-textbox"/></td>
<td><label><?php echo lang('phone')?></label>:</td>
<td><input type="text" name="search[phone]" id="search_phone"  class="easyui-textbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('email')?></label>:</td>
<td><input type="text" name="search[email]" id="search_email"  class="easyui-textbox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="bank-table" data-options="pagination:true,title:'<?php  echo lang('bank')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
<th data-options="field:'bank_name',sortable:true" width="50"><?php echo lang('bank_name')?></th>
<th data-options="field:'phone',sortable:true" width="50"><?php echo lang('phone')?></th>
<th data-options="field:'email',sortable:true" width="50"><?php echo lang('email')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_bank')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_bank')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit bank form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-bank" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('bank_name')?>:</label></td>
					  <td width="66%"><input name="bank_name" id="bank_name" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('phone')?>:</label></td>
					  <td width="66%"><input name="phone" id="phone" class="easyui-validatebox"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('email')?>:</label></td>
					  <td width="66%"><input name="email" id="email" class="easyui-validatebox"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#bank-search-form').form('clear');
			$('#bank-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#bank-table').datagrid({
				queryParams:{data:$('#bank-search-form').serialize()}
				});
		});		
		$('#bank-table').datagrid({
			url:'<?php  echo site_url('bank/admin/bank/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning" iconcls="icon-edit"  title="<?php  echo lang('edit_bank')?>"><span class="l-btn-left">Edit</span></a>';
		var d = '<a href="#" onclick="removebank('+index+')" class="btn btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_bank')?>"><span class="l-btn-left">Delete</span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-bank').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_bank')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#bank-table').datagrid('getRows')[index];
		if (row){
			$('#form-bank').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_bank')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removebank(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#bank-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('bank/admin/bank/delete_json')?>', {id:[row.id]}, function(){
					$('#bank-table').datagrid('deleteRow', index);
					$('#bank-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#bank-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('bank/admin/bank/delete_json')?>',{id:selected},function(data){
						$('#bank-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-bank').form('submit',{
			url: '<?php  echo site_url('bank/admin/bank/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-bank').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#bank-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>