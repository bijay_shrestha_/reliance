<?php

class Party extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('party','party_model');
        $this->lang->module_load('party','party');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['rows'] = $this->party_model->getParties()->result_array();
		// Display Page
		$data['header'] = 'Agent';
		$data['view_page'] = "party/index";
		$data['module'] = 'party';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->party_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->party_model->getParties()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['party']!='')?$this->db->like('party',$params['search']['party']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->party_model->getParties()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->party_model->delete('PARTY',array('id'=>$row));
            endforeach;
		}else{
			$data['success'] = $this->party_model->delete('PARTY',array('id'=>$id));
			echo json_encode($data);
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->party_model->insert('PARTY',$data);
        }
        else
        {
            $success=$this->party_model->update('PARTY',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['party'] = $this->input->post('party');

        return $data;
   }
   
   	function get_value(){
    	$where['id'] = $this->input->post('id');
    	$row = $this->party_model->getParties($where)->row_array();
    	echo json_encode($row);
   	}	
	    
}