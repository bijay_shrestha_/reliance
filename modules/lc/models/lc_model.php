<?php
class Lc_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('LC'=>$this->prefix.'lc','DEALERS'=>$this->prefix.'dealers','BANK'=>$this->prefix.'bank','LC_TRACK'=>$this->prefix.'lc_track','COST_SHEET'=>$this->prefix.'cost_sheet');
		$this->_JOINS=array('DEALERS'=>array('join_type'=>'LEFT','join_field'=>'lcs.dealer_id=dealers.dealer_id',
                                           'select'=>'dealers.*','alias'=>'dealers'),
                            'BANK'=>array('join_type'=>'LEFT','join_field'=>'lcs.bank_id=bank.id',
                                           'select'=>'bank.bank_name,bank.phone,bank.email,bank.id as bank_id','alias'=>'bank'),
                            'LC_TRACK'=>array('join_type'=>'LEFT','join_field'=>'lcs.id=lc_track.lc_id',
                                           'select'=>'lc_track.eta_kolkata,lc_track.reached_kolkata,lc_track.reached_nepal,lc_track.border,lc_track.reached_factory,lc_track.date_en,lc_track.date_np,lc_track.current_status','alias'=>'lc_track'),
                            'COST_SHEET'=>array('join_type'=>'LEFT','join_field'=>'lcs.id=cost_sheet.lc_id',
                                           'select'=>'cost_sheet.lc_id,cost_sheet.pp_no,cost_sheet.currency_exchange_rate,cost_sheet.duty_exchange_rate,cost_sheet.transport_charge,cost_sheet.insurance,cost_sheet.custom_fee,cost_sheet.agency_fee,cost_sheet.dry_port_fee,cost_sheet.addition_fee,cost_sheet.bank_charge,cost_sheet.csf,cost_sheet.freight,cost_sheet.fee_fine,cost_sheet.local_transport,cost_sheet.id as cost_id,cost_sheet.date_np as cost_date_np,,cost_sheet.date_en as cost_date_en','alias'=>'cost_sheet'),
                            );        
    }
    
    public function getLcs($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='lcs.*,lcs.id as lcid';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['LC']. ' lcs');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['LC'].' lcs');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}