<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('lc_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="lc-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('id')?></label>:</td>
<td><input type="text" name="search[id]" id="search_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('pi_number')?></label>:</td>
<td><input type="text" name="search[pi_number]" id="search_pi_number"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('dealer_id')?></label>:</td>
<td><input type="text" name="search[dealer_id]" id="search_dealer_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('dd_lc_no')?></label>:</td>
<td><input type="text" name="search[dd_lc_no]" id="search_dd_lc_no"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('bank_id')?></label>:</td>
<td><input type="text" name="search[bank_id]" id="search_bank_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('additional_charge')?></label>:</td>
<td><input type="text" name="search[additional_charge]" id="search_additional_charge"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('total_lc_value')?></label>:</td>
<td><input type="text" name="search[total_lc_value]" id="search_total_lc_value"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('date_en')?></label>:</td>
<td><input type="text" name="date[date_en][from]" id="search_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_en][to]" id="search_date_en_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('date_np')?></label>:</td>
<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="lc-table" data-options="pagination:true,title:'<?php  echo lang('lc')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="50"><?php echo lang('id')?></th>
<th data-options="field:'pi_number',sortable:true" width="50"><?php echo lang('pi_number')?></th>
<th data-options="field:'dealer_id',sortable:true" width="50"><?php echo lang('dealer_id')?></th>
<th data-options="field:'dd_lc_no',sortable:true" width="50"><?php echo lang('dd_lc_no')?></th>
<th data-options="field:'bank_id',sortable:true" width="50"><?php echo lang('bank_id')?></th>
<th data-options="field:'additional_charge',sortable:true" width="50"><?php echo lang('additional_charge')?></th>
<th data-options="field:'total_lc_value',sortable:true" width="50"><?php echo lang('total_lc_value')?></th>
<th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th>
<th data-options="field:'date_np',sortable:true" width="50"><?php echo lang('date_np')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_lc')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_lc')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit lc form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-lc" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('id')?>:</label></td>
					  <td width="66%"><input name="id" id="id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('pi_number')?>:</label></td>
					  <td width="66%"><input name="pi_number" id="pi_number" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('dealer_id')?>:</label></td>
					  <td width="66%"><input name="dealer_id" id="dealer_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('dd_lc_no')?>:</label></td>
					  <td width="66%"><input name="dd_lc_no" id="dd_lc_no" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('bank_id')?>:</label></td>
					  <td width="66%"><input name="bank_id" id="bank_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('additional_charge')?>:</label></td>
					  <td width="66%"><input name="additional_charge" id="additional_charge" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('total_lc_value')?>:</label></td>
					  <td width="66%"><input name="total_lc_value" id="total_lc_value" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np" class="easyui-validatebox" required="true"></td>
		       </tr><input type="hidden" name="" id=""/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#lc-search-form').form('clear');
			$('#lc-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#lc-table').datagrid({
				queryParams:{data:$('#lc-search-form').serialize()}
				});
		});		
		$('#lc-table').datagrid({
			url:'<?php  echo site_url('lc/admin/lc/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_lc')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removelc('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_lc')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-lc').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_lc')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#lc-table').datagrid('getRows')[index];
		if (row){
			$('#form-lc').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_lc')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removelc(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#lc-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('lc/admin/lc/delete_json')?>', {id:[row.]}, function(){
					$('#lc-table').datagrid('deleteRow', index);
					$('#lc-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#lc-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('lc/admin/lc/delete_json')?>',{id:selected},function(data){
						$('#lc-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-lc').form('submit',{
			url: '<?php  echo site_url('lc/admin/lc/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-lc').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#lc-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>