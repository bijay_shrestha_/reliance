<?php

class Lc extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('lc','lc_model');
        $this->lang->module_load('lc','lc');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'lc';
		$data['page'] = $this->config->item('template_admin') . "lc/index";
		$data['module'] = 'lc';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->lc_model->count();
		paging('');
		$this->_get_search_param();	
		$rows=$this->lc_model->getLcs()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['id']!='')?$this->db->where('id',$params['search']['id']):'';
($params['search']['pi_number']!='')?$this->db->like('pi_number',$params['search']['pi_number']):'';
($params['search']['dealer_id']!='')?$this->db->where('dealer_id',$params['search']['dealer_id']):'';
($params['search']['dd_lc_no']!='')?$this->db->like('dd_lc_no',$params['search']['dd_lc_no']):'';
($params['search']['bank_id']!='')?$this->db->where('bank_id',$params['search']['bank_id']):'';
($params['search']['additional_charge']!='')?$this->db->like('additional_charge',$params['search']['additional_charge']):'';
($params['search']['total_lc_value']!='')?$this->db->like('total_lc_value',$params['search']['total_lc_value']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->lc_model->getLcs()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->lc_model->delete('LC',array(''=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post(''))
        {
            $success=$this->lc_model->insert('LC',$data);
        }
        else
        {
            $success=$this->lc_model->update('LC',$data,array(''=>$data['']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['pi_number'] = $this->input->post('pi_number');
$data['dealer_id'] = $this->input->post('dealer_id');
$data['dd_lc_no'] = $this->input->post('dd_lc_no');
$data['bank_id'] = $this->input->post('bank_id');
$data['additional_charge'] = $this->input->post('additional_charge');
$data['total_lc_value'] = $this->input->post('total_lc_value');
$data['date_en'] = $this->input->post('date_en');
$data['date_np'] = $this->input->post('date_np');

        return $data;
   }
   
   	
	    
}