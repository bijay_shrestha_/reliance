<?php

class Lc extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('lc','lc_model');
        $this->load->module_model('dealer','dealer_model');
        $this->load->module_model('material_dealer','material_dealer_model');
        $this->load->module_model('lc_material','lc_material_model');
        $this->load->module_model('lc_track','lc_track_model');
        $this->load->module_model('bank','bank_model');
        $this->load->module_model('border','border_model');

        $this->lang->module_load('lc','lc');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->db->order_by('border');
		$data['borders'] = $this->border_model->getBorders()->result_array();
		$this->lc_model->joins = array('DEALERS','BANK');
		$data['rows'] = $this->lc_model->getLcs()->result_array();
		$this->db->order_by('dealer_name','asc');
		$data['dealers'] = $this->dealer_model->getDealers()->result_array();
		$this->db->order_by('bank_name');
		$data['banks'] = $this->bank_model->getBanks()->result_array();
		foreach ($data['rows'] as $key => $value) {
			$data['materials'][$value['id']] = $this->lc_material_model->getLcMaterials(array('lc_id'=>$value['id']))->result_array();
		}
		// echo '<pre>';
		// print_r($data['materials']);exit;
		// Display Page
		$data['header'] = 'Letter of Credit';
		$data['view_page'] = "lc/index";
		$data['module'] = 'lc';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->lc_model->count();
		paging('');
		$this->_get_search_param();	
		$rows=$this->lc_model->getLcs()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['id']!='')?$this->db->where('id',$params['search']['id']):'';
($params['search']['pi_number']!='')?$this->db->like('pi_number',$params['search']['pi_number']):'';
($params['search']['dealer_id']!='')?$this->db->where('dealer_id',$params['search']['dealer_id']):'';
($params['search']['dd_lc_no']!='')?$this->db->like('dd_lc_no',$params['search']['dd_lc_no']):'';
($params['search']['bank_id']!='')?$this->db->where('bank_id',$params['search']['bank_id']):'';
($params['search']['additional_charge']!='')?$this->db->like('additional_charge',$params['search']['additional_charge']):'';
($params['search']['total_lc_value']!='')?$this->db->like('total_lc_value',$params['search']['total_lc_value']):'';
($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->lc_model->getLcs()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->lc_model->delete('LC',array('id'=>$row));
            endforeach;
		}else{
			$success = $this->lc_model->delete('LC',array('id'=>$id));
		}
		echo json_encode($success);
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        // echo '<pre>';
        // print_r($this->input->post());
        // print_r($data);
        // exit;
        if(!$this->input->post('id'))
        {
            $success=$this->lc_model->insert('LC',$data['data']);
            if($success){
            	$lc_id = $this->db->insert_id();
	            foreach ($data['material']['material_id'] as $key => $value) {
	            	$this->lc_material_model->insert('LC_MATERIAL',array('material_id'=>$value, 'lc_id'=>$lc_id, 'quantity'=>$data['material']['quantity'][$key],'price'=>$data['material']['price'][$key]));
	            }
            }
        }
        else
        {
            $success=$this->lc_model->update('LC',$data['data'],array('id'=>$data['data']['id']));
            $this->lc_material_model->delete('LC_MATERIAL',array('lc_id'=>$data['data']['id']));
            foreach ($data['material']['material_id'] as $key => $value) {
            	$this->lc_material_model->insert('LC_MATERIAL',array('material_id'=>$value, 'lc_id'=>$data['data']['id'], 'quantity'=>$data['material']['quantity'][$key],'price'=>$data['material']['price'][$key]));
            }
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['data']['id'] = $this->input->post('id');
$data['data']['pi_number'] = $this->input->post('pi_number');
$data['data']['dealer_id'] = $this->input->post('dealer_id');
$data['data']['dd_lc_no'] = trim($this->input->post('dd_lc_no'));
$data['data']['bank_id'] = $this->input->post('bank_id');
$data['data']['additional_charge'] = $this->input->post('additional_charge');
$data['data']['total_lc_value'] = $this->input->post('total_lc_value');
$data['data']['total_lc_quantity'] = $this->input->post('total_lc_quantity');
$data['data']['date_en'] = $this->input->post('date_en');
$data['data']['date_np'] = $this->input->post('date_np');
$data['data']['currency'] = $this->input->post('currency');
$data['data']['border'] = $this->input->post('border');

$data['material']['material_id'] = $this->input->post('material_id');
$data['material']['quantity'] = $this->input->post('quantity');
$data['material']['price'] = $this->input->post('price');

        return $data;
   }
   
   	public function getMaterialInput()
   	{
   		$where['dealer_id'] = $this->input->post('id');
   		$data['i'] = $this->input->post('i');
   		$this->material_dealer_model->joins = array('MATERIALS');
   		$this->db->order_by('material_name','asc');
   		$data['materails'] = $this->material_dealer_model->getMaterialDealers($where)->result_array();
   		$data['id'] = $where['dealer_id'];
   		$this->load->view('lc/material_input',$data);
   	}
   	public function getValue()
   	{
   		$id = $this->input->post('id');
   		$data['lc'] = $this->lc_model->getLcs(array('id'=>$id))->row_array();
   		$this->material_dealer_model->joins = array('MATERIALS');
   		$data['material_options'] = $this->material_dealer_model->getMaterialDealers(array('dealer_id'=>$data['lc']['dealer_id']))->result_array();
   		$data['materials'] = $this->lc_material_model->getLcMaterials(array('lc_id'=>$id))->result_array();
   		echo json_encode($data);
   	}
   	public function closeLc()
   	{
   		$id = $this->input->post('id');
   		$success = $this->lc_model->update('LC',array('closed'=>1),array('id'=>$id));
   		echo json_encode(array('success'=>$success));
   	}
   	public function openLC()
   	{
   		$id = $this->input->post('id');
   		$success = $this->lc_model->update('LC',array('closed'=>0),array('id'=>$id));
   		echo json_encode(array('success'=>$success));
   	}
   	public function getLcDetail()
   	{
   		$lc = $this->input->post('lc');
   		$lc = $this->lc_model->getLcs(array('dd_lc_no'=>$lc))->row_array();
   		// echo '<pre>';print_r($lc) ;print_r($lc);
   		$this->lc_material_model->joins = array('MATERIALS');
   		$data['material'] = $this->lc_material_model->getLcMaterials(array('lc_id'=>$lc['id']))->result_array();
   		// echo json_encode($data);
   		$this->load->view('lc/detail',$data);
   	}
   	public function storeReport()
   	{
   		// echo '<pre>';
   		$id = $this->input->post('id');
   		$this->lc_material_model->joins = array('LC');
   		$data['lc'] = $this->lc_material_model->getLcMaterials(array('material_id' => $id, 'closed' => 0))->row_array();
   		if(array_key_exists('lc_id', $data['lc'])){
   			$data['lc_track'] = $this->lc_track_model->getLcTracks(array('lc_id'=>$data['lc']['lc_id']))->row_array();
   			// print_r($data);
   			$this->load->view('home/store_lc',$data);
   		}else{
   			echo 'LC record not found';
   		}
   	}
   	public function saveTrack(){
   		print_r($this->input->post());
   	}
	    
}