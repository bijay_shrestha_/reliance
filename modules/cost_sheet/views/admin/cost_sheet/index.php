<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('cost_sheet_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="cost_sheet-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('lc_id')?></label>:</td>
<td><input type="text" name="search[lc_id]" id="search_lc_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('pp_no')?></label>:</td>
<td><input type="text" name="search[pp_no]" id="search_pp_no"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('currency_exchange_rate')?></label>:</td>
<td><input type="text" name="search[currency_exchange_rate]" id="search_currency_exchange_rate"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('duty_exchange_rate')?></label>:</td>
<td><input type="text" name="search[duty_exchange_rate]" id="search_duty_exchange_rate"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('transport_charge')?></label>:</td>
<td><input type="text" name="search[transport_charge]" id="search_transport_charge"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('insurance')?></label>:</td>
<td><input type="text" name="search[insurance]" id="search_insurance"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('custom_fee')?></label>:</td>
<td><input type="text" name="search[custom_fee]" id="search_custom_fee"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('agency_fee')?></label>:</td>
<td><input type="text" name="search[agency_fee]" id="search_agency_fee"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('dry_port_fee')?></label>:</td>
<td><input type="text" name="search[dry_port_fee]" id="search_dry_port_fee"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('addition_fee')?></label>:</td>
<td><input type="text" name="search[addition_fee]" id="search_addition_fee"  class="easyui-validatebox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="cost_sheet-table" data-options="pagination:true,title:'<?php  echo lang('cost_sheet')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'lc_id',sortable:true" width="50"><?php echo lang('lc_id')?></th>
<th data-options="field:'pp_no',sortable:true" width="50"><?php echo lang('pp_no')?></th>
<th data-options="field:'currency_exchange_rate',sortable:true" width="50"><?php echo lang('currency_exchange_rate')?></th>
<th data-options="field:'duty_exchange_rate',sortable:true" width="50"><?php echo lang('duty_exchange_rate')?></th>
<th data-options="field:'transport_charge',sortable:true" width="50"><?php echo lang('transport_charge')?></th>
<th data-options="field:'insurance',sortable:true" width="50"><?php echo lang('insurance')?></th>
<th data-options="field:'custom_fee',sortable:true" width="50"><?php echo lang('custom_fee')?></th>
<th data-options="field:'agency_fee',sortable:true" width="50"><?php echo lang('agency_fee')?></th>
<th data-options="field:'dry_port_fee',sortable:true" width="50"><?php echo lang('dry_port_fee')?></th>
<th data-options="field:'addition_fee',sortable:true" width="50"><?php echo lang('addition_fee')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_cost_sheet')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_cost_sheet')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit cost_sheet form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-cost_sheet" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('lc_id')?>:</label></td>
					  <td width="66%"><input name="lc_id" id="lc_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('pp_no')?>:</label></td>
					  <td width="66%"><input name="pp_no" id="pp_no" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('currency_exchange_rate')?>:</label></td>
					  <td width="66%"><input name="currency_exchange_rate" id="currency_exchange_rate" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('duty_exchange_rate')?>:</label></td>
					  <td width="66%"><input name="duty_exchange_rate" id="duty_exchange_rate" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('transport_charge')?>:</label></td>
					  <td width="66%"><input name="transport_charge" id="transport_charge" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('insurance')?>:</label></td>
					  <td width="66%"><input name="insurance" id="insurance" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('custom_fee')?>:</label></td>
					  <td width="66%"><input name="custom_fee" id="custom_fee" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('agency_fee')?>:</label></td>
					  <td width="66%"><input name="agency_fee" id="agency_fee" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('dry_port_fee')?>:</label></td>
					  <td width="66%"><input name="dry_port_fee" id="dry_port_fee" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('addition_fee')?>:</label></td>
					  <td width="66%"><input name="addition_fee" id="addition_fee" class="easyui-validatebox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#cost_sheet-search-form').form('clear');
			$('#cost_sheet-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#cost_sheet-table').datagrid({
				queryParams:{data:$('#cost_sheet-search-form').serialize()}
				});
		});		
		$('#cost_sheet-table').datagrid({
			url:'<?php  echo site_url('cost_sheet/admin/cost_sheet/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_cost_sheet')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removecost_sheet('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_cost_sheet')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-cost_sheet').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_cost_sheet')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#cost_sheet-table').datagrid('getRows')[index];
		if (row){
			$('#form-cost_sheet').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_cost_sheet')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removecost_sheet(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#cost_sheet-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('cost_sheet/admin/cost_sheet/delete_json')?>', {id:[row.id]}, function(){
					$('#cost_sheet-table').datagrid('deleteRow', index);
					$('#cost_sheet-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#cost_sheet-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('cost_sheet/admin/cost_sheet/delete_json')?>',{id:selected},function(data){
						$('#cost_sheet-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-cost_sheet').form('submit',{
			url: '<?php  echo site_url('cost_sheet/admin/cost_sheet/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-cost_sheet').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#cost_sheet-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>