<?php


$lang['id'] = 'Id';
$lang['lc_id'] = 'Lc Id';
$lang['pp_no'] = 'Pp No';
$lang['currency_exchange_rate'] = 'Currency Exchange Rate';
$lang['duty_exchange_rate'] = 'Duty Exchange Rate';
$lang['transport_charge'] = 'Transport Charge';
$lang['insurance'] = 'Insurance';
$lang['custom_fee'] = 'Custom Fee';
$lang['agency_fee'] = 'Agency Fee';
$lang['dry_port_fee'] = 'Dry Port Fee';
$lang['addition_fee'] = 'Addition Fee';

$lang['create_cost_sheet']='Create Cost Sheet';
$lang['edit_cost_sheet']='Edit Cost Sheet';
$lang['delete_cost_sheet']='Delete Cost Sheet';
$lang['cost_sheet_search']='Cost Sheet Search';

$lang['cost_sheet']='Cost Sheet';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

