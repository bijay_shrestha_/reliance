<?php

class Cost_sheet extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('cost_sheet','cost_sheet_model');
        $this->load->module_model('cost_material','cost_material_model');
        $this->load->module_model('lc','lc_model');
        $this->load->module_model('lc_material','lc_material_model');
        $this->lang->module_load('cost_sheet','cost_sheet');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->lc_model->joins = array('DEALERS','BANK');
		$data['rows'] = $this->lc_model->getLcs()->result_array();
		$data['lcs'] = $this->lc_model->getLcs(array('closed'=>1))->result_array();
		foreach ($data['rows'] as $key => $value) {
			$where['lc_id'] = $value['id'];
			$data['rows'][$key]['costsheets'] = $this->cost_sheet_model->getCostSheets($where)->result_array();
		}
		// echo '<pre>';print_r($data);exit;
		// Display Page
		$data['header'] = 'Cost Sheet';
		$data['view_page'] = "cost_sheet/index";
		$data['module'] = 'cost_sheet';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->cost_sheet_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->cost_sheet_model->getCostSheets()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['lc_id']!='')?$this->db->where('lc_id',$params['search']['lc_id']):'';
($params['search']['pp_no']!='')?$this->db->like('pp_no',$params['search']['pp_no']):'';
($params['search']['currency_exchange_rate']!='')?$this->db->like('currency_exchange_rate',$params['search']['currency_exchange_rate']):'';
($params['search']['duty_exchange_rate']!='')?$this->db->like('duty_exchange_rate',$params['search']['duty_exchange_rate']):'';
($params['search']['transport_charge']!='')?$this->db->like('transport_charge',$params['search']['transport_charge']):'';
($params['search']['insurance']!='')?$this->db->like('insurance',$params['search']['insurance']):'';
($params['search']['custom_fee']!='')?$this->db->like('custom_fee',$params['search']['custom_fee']):'';
($params['search']['agency_fee']!='')?$this->db->like('agency_fee',$params['search']['agency_fee']):'';
($params['search']['dry_port_fee']!='')?$this->db->like('dry_port_fee',$params['search']['dry_port_fee']):'';
($params['search']['addition_fee']!='')?$this->db->like('addition_fee',$params['search']['addition_fee']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->cost_sheet_model->getCostSheets()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->cost_sheet_model->delete('COST_SHEET',array('id'=>$row));
            endforeach;
		}else{
			$success = $this->cost_sheet_model->delete('COST_SHEET',array('id'=>$id));
			if($success){
				$this->cost_material_model->delete('COST_MATERIAL',array('cost_id'=>$id));
			}
		}
		// echo $this->db->last_query();
		echo json_encode(array('success'=>$success));
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		
// echo '<pre>';
// print_r($this->input->post());
// print_r($data);
// exit;
        if(!$this->input->post('id'))
        {
            $success=$this->cost_sheet_model->insert('COST_SHEET',$data);
            $id = $this->db->insert_id();
        }
        else
        {
            $success=$this->cost_sheet_model->update('COST_SHEET',$data,array('id'=>$data['id']));
            $id = $this->input->post('id');
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message');
			$materials = $this->input->post('material_id'); 
			$quantity = $this->input->post('quantity'); 
			$price = $this->input->post('price'); 
			$duty = $this->input->post('duty_amount'); 
			$excise = $this->input->post('excise'); 
			$material['cost_id'] = $id;
			$this->cost_material_model->delete('COST_MATERIAL',array('cost_id'=>$id));
			foreach ($materials as $key => $value) {
				$material['material_id'] = $value;
				$material['quantity'] = $quantity[$key];
				$material['duty'] = $duty[$key];
				$material['excise'] = $excise[$key];
				$material['price'] = $price[$key];
				$this->cost_material_model->insert('COST_MATERIAL',$material);
			}
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['lc_id'] = $this->input->post('lc_id');
$data['pp_no'] = $this->input->post('pp_no');
$data['currency_exchange_rate'] = $this->input->post('currency_exchange_rate');
$data['duty_exchange_rate'] = $this->input->post('duty_exchange_rate');
$data['transport_charge'] = $this->input->post('transport_charge');
$data['insurance'] = $this->input->post('insurance');
$data['custom_fee'] = $this->input->post('custom_fee');
$data['agency_fee'] = $this->input->post('agency_fee');
$data['dry_port_fee'] = $this->input->post('dry_port_fee');
$data['addition_fee'] = $this->input->post('addition_fee');
$data['bank_charge'] = $this->input->post('bank_charge');
$data['csf'] = $this->input->post('csf');
$data['freight'] = $this->input->post('freight');
$data['fee_fine'] = $this->input->post('fee_fine');
$data['local_transport'] = $this->input->post('local_transport');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');

        return $data;
   }
   public function getValue()
   {
   		$id = $this->input->post('id');
   		$index = $this->input->post('index');
   		$this->lc_model->joins = array('COST_SHEET');
   		$data['lc'] = $this->lc_model->getLcs(array($index=>$id))->row_array();
   		$lc_id = $data['lc']['lcid'];
   		if($index == 'pp_no'){
   			$data['lc_cost'] = $this->cost_sheet_model->getCostSheets(array('pp_no'=>$id))->row_array();
   			$material_detail = $this->cost_material_model->getCostMaterials(array('cost_id'=>$data['lc_cost']['id']))->result_array();
   			foreach ($material_detail as $key => $value) {
   				$data['material_detail'][$value['material_id']] = $value;
   			}
   		}
   		// echo $this->db->last_query();exit;
   		$this->lc_material_model->joins = array('MATERIALS');
   		$data['materials'] = $this->lc_material_model->getLcMaterials(array('lc_id'=>$lc_id))->result_array();
   		echo json_encode($data);
   }
   	public function getAllValue()
   	{
   		$data['id'] = $id = $this->input->post('id');
   		// echo $id;
   		$this->lc_model->joins = array('COST_SHEET');
   		$data['rows'] = $this->lc_model->getLcs(array('dd_lc_no'=>$id))->result_array();
   		// echo $this->db->last_query().'<br>';
   		// print_r($data);exit;
   		$count = $this->cost_sheet_model->count(array('lc_id'=>$data['rows'][0]['id']));
   		if($count){
	   		// echo $this->db->last_query();
	   		foreach ($data['rows'] as $key => $value) {
   				$total_cost = 0;
   				$total_quantity = 0;
   				$total_duty_amount = 0;
   				$total_excise = 0;
	   			$this->cost_material_model->joins = array('MATERIALS');
	   			$data['material'][$key] = $this->cost_material_model->getCostMaterials(array('cost_id'=>$value['cost_id']))->result_array();
	   			foreach($data['material'][$key] as $material){
	   				$total = $material['quantity'] * $material['price'] * $value['currency_exchange_rate'];
	   				$total_cost += $total;
	   				// echo $total;
	   				$total_quantity += $material['quantity'];
	   				$total_duty_amount += $material['duty'];
	   				$total_excise += $material['excise'];
	   			}
	   			$data['rows'][$key]['total_duty'] = $total_duty_amount;
	   			$data['rows'][$key]['total_excise'] = $total_excise;
	   			// $data['rows'][$key]['total_charge'] = $total_cost + $value['transport_charge'] + $value['insurance'] + $value['custom_fee'] +$value['agency_fee'] +$value['dry_port_fee'] + $value['addition_fee'] + $value['bank_charge'] + $value['csf'] + $value['fee_fine'] + $value['local_transport'];
	   			$data['rows'][$key]['bill_amt'] = $total_cost;
	   			$data['rows'][$key]['total_charge'] = $value['bank_charge'] + $value['insurance'] + $value['transport_charge'] + $value['local_transport'] + $value['custom_fee'] +$value['agency_fee'] + $value['fee_fine'] +$value['dry_port_fee'] + $value['csf'] + $value['addition_fee'];
	   			// echo $data['rows'][$key]['total_charge'];
	   			$data['rows'][$key]['quantity'] = $total_quantity;
	   		}
	   		// echo "<pre>";
	   		// print_r($data);
	   		// exit;
	   		$this->load->view('cost_sheet/detail',$data);
   		}else{
   			echo 'No record';
   		}
   	}
   	public function excel()
   	{
   		// echo '<pre>';
   		$data['data'] = $this->getExcelReport();
   		$data['title'] = array('Date','PP No','Dealer','Material','LC No','Quantity','Bill Amount','Currency','Rate','NPR Cost','Duty','Excise','Bank Charge','Insurance','Upto Custom','Dry Port','Freight','Local Freight','Agent Commession','Other Charge','CSF','Landed Cost','Average cost',);
   		$data['fields'] = array('date_np','pp_no','dealer_name','material_name', 'lc_no', 'quantity', 'bill_amount', 'currency', 'rate', 'NPR','duty','excise','bank_charge', 'insurance', 'custom_fee', 'dry_port_fee', 'transport_charge', 'local_transport', 'agency_fee', 'addition_fee','csf','landed_cost', 'avg');
   		$this->export($data);
		// print_r($data);
   	}
   	private function getExcelReport()
   	{
   		$data = array();
   		$rows = array();
   		$this->lc_model->joins = array('DEALERS','BANK');
		$lcs = $this->lc_model->getLcs()->result_array();
		
		// print_r($lcs);
		$this->cost_material_model->joins = array('MATERIALS');
		$m = 0;
		foreach ($lcs as $key => $value) {
			$cost_sheet = $this->cost_sheet_model->getCostSheets(array('lc_id'=>$value['id']))->result_array();
			// print_r($cost_sheet);
			foreach ($cost_sheet as $i => $val) {
				$rows[$m] = $this->cost_material_model->getCostMaterials(array('cost_id'=>$val['id']))->result_array();
				// echo '<pre>';print_r($rows[$m]);exit;
				$qty = 0;
				$cost = 0;
				$charge = 0;
				$charge += $val['bank_charge'];
				$charge += $val['insurance'];
				$charge += $val['custom_fee'];
				$charge += $val['dry_port_fee'];
				$charge += $val['transport_charge'];
				$charge += $val['local_transport'];
				$charge += $val['agency_fee'];
				$charge += $val['addition_fee'];
				$charge += $val['csf'];
				foreach ($rows[$m] as $index => $v) {
					$qty += $v['quantity'];
					$cost += $v['quantity'] * $v['price'];
				}
				foreach ($rows[$m] as $index => $v) {
					$rows[$m][$index]['dealer_name'] = $value['dealer_name'];
					$rows[$m][$index]['pp_no'] = $val['pp_no'];
					$rows[$m][$index]['date_np'] = $val['date_np'];
					$rows[$m][$index]['lc_no'] = $value['dd_lc_no'];
					$rows[$m][$index]['currency'] = $value['currency'];
					$rows[$m][$index]['rate'] = number_format($val['currency_exchange_rate'],3);
					$landed_cost = ($charge / $cost) * $v['quantity'] * $v['price'] + $v['duty'] + $v['excise'] + $v['quantity'] * $v['price'] * $val['currency_exchange_rate'];
					$rows[$m][$index]['landed_cost'] = number_format($landed_cost,3);
					// echo $rows[$m][$index]['landed_cost'];exit;
					$rows[$m][$index]['bank_charge'] = number_format($val['bank_charge'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['insurance'] = number_format($val['insurance'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['custom_fee'] = number_format($val['custom_fee'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['dry_port_fee'] = number_format($val['dry_port_fee'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['transport_charge'] = number_format($val['transport_charge'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['local_transport'] = number_format($val['local_transport'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['agency_fee'] = number_format($val['agency_fee'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['addition_fee'] = number_format($val['addition_fee'] / $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['bill_amount'] = number_format($v['quantity'] * $v['price'],3);
					$rows[$m][$index]['NPR'] = number_format($v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					$rows[$m][$index]['avg'] = number_format($landed_cost/$v['quantity'],3);
					$rows[$m][$index]['csf'] = number_format($val['csf']/ $cost * $v['quantity'] * $v['price'] * $val['currency_exchange_rate'],3);
					// echo $rows[$m][$index]['avg'].'<br>'.$rows[$m][$index]['landed_cost'];exit;
				}
				$m++;
			}
		}
		// print_r($rows);
		foreach ($rows as $key => $value) {
			foreach ($value as $index => $val) {
				$data[] = $val;
			}
		}
		return $data;
   	}
	/*
     * exporting data for field report
     */
   	private function export($data)
   	{
   		$x = 0;
   		// echo '<pre>';print_r($data);exit;
        $objPHPExcel = new PHPExcel();
        	// if($x > 0){
        	// 	$objPHPExcel->createSheet();
        	// }
        $objPHPExcel->setActiveSheetIndex('0');
        $objPHPExcel->getActiveSheet()->setTitle('Sheet1');
        date_default_timezone_set('UTC');
        $j = 1;
        // foreach($data as $key => $value){
        	// if($j != 1){
        	// 	$j++;
        	// }
	        $i = 'A';
		        
		        foreach($data['title'] as $title){
		            $objPHPExcel->getActiveSheet()->setCellValue($i.$j, $title);
		            $i++;
		        }
		        $j++;
		        foreach($data['data'] as $report){
		        	$i = 'A';
		        	foreach($data['fields'] as $field){
		            	$objPHPExcel->getActiveSheet()->setCellValue($i.$j, $report[$field]);
		        		$i++;
		    		}
	                
	                $j++;
	            }
        	
            // $x++;
    	// }
            $filename='Landed_Cost_Sheet_Report.xls';
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
            //force user to download the Excel file without writing it to server's HD
            
        $objWriter->save('php://output');
        //- See more at: https://arjunphp.com/how-to-use-phpexcel-with-codeigniter/#sthash.SjpCbSQh.dpuf
        return FALSE;
   	}
   	
	    
}