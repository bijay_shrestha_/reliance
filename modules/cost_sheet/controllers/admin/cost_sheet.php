<?php

class Cost_sheet extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('cost_sheet','cost_sheet_model');
        $this->lang->module_load('cost_sheet','cost_sheet');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'cost_sheet';
		$data['page'] = $this->config->item('template_admin') . "cost_sheet/index";
		$data['module'] = 'cost_sheet';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->cost_sheet_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->cost_sheet_model->getCostSheets()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['lc_id']!='')?$this->db->where('lc_id',$params['search']['lc_id']):'';
($params['search']['pp_no']!='')?$this->db->like('pp_no',$params['search']['pp_no']):'';
($params['search']['currency_exchange_rate']!='')?$this->db->like('currency_exchange_rate',$params['search']['currency_exchange_rate']):'';
($params['search']['duty_exchange_rate']!='')?$this->db->like('duty_exchange_rate',$params['search']['duty_exchange_rate']):'';
($params['search']['transport_charge']!='')?$this->db->like('transport_charge',$params['search']['transport_charge']):'';
($params['search']['insurance']!='')?$this->db->like('insurance',$params['search']['insurance']):'';
($params['search']['custom_fee']!='')?$this->db->like('custom_fee',$params['search']['custom_fee']):'';
($params['search']['agency_fee']!='')?$this->db->like('agency_fee',$params['search']['agency_fee']):'';
($params['search']['dry_port_fee']!='')?$this->db->like('dry_port_fee',$params['search']['dry_port_fee']):'';
($params['search']['addition_fee']!='')?$this->db->like('addition_fee',$params['search']['addition_fee']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->cost_sheet_model->getCostSheets()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->cost_sheet_model->delete('COST_SHEET',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->cost_sheet_model->insert('COST_SHEET',$data);
        }
        else
        {
            $success=$this->cost_sheet_model->update('COST_SHEET',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['lc_id'] = $this->input->post('lc_id');
$data['pp_no'] = $this->input->post('pp_no');
$data['currency_exchange_rate'] = $this->input->post('currency_exchange_rate');
$data['duty_exchange_rate'] = $this->input->post('duty_exchange_rate');
$data['transport_charge'] = $this->input->post('transport_charge');
$data['insurance'] = $this->input->post('insurance');
$data['custom_fee'] = $this->input->post('custom_fee');
$data['agency_fee'] = $this->input->post('agency_fee');
$data['dry_port_fee'] = $this->input->post('dry_port_fee');
$data['addition_fee'] = $this->input->post('addition_fee');

        return $data;
   }
   
   	
	    
}