<?php
class Cost_sheet_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('COST_SHEET'=>$this->prefix.'cost_sheet','LC'=>$this->prefix.'lc');
		$this->_JOINS=array('LC'=>array('join_type'=>'LEFT','join_field'=>'cost_sheets.lc_id=lc.id',
                                           'select'=>'lc.pi_number,lc.dealer_id,lc.currency,lc.dd_lc_no,lc.bank_id,lc.additional_charge,lc.total_lc_value,lc.closed,lc.total_lc_quantity,lc.border','alias'=>'lc'),
                           
                            );        
    }
    
    public function getCostSheets($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='cost_sheets.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['COST_SHEET']. ' cost_sheets');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['COST_SHEET'].' cost_sheets');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}