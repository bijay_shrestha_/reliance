<?php
class Material_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('MATERIALS'=>$this->prefix.'materials','CATEGORY'=>$this->prefix.'category','INVENTORY'=>$this->prefix.'inventory','MATERIAL_DEPARTMENTS'=>$this->prefix.'material_departments','CATEGORY_DEPARTMENT'=>$this->prefix.' category_department', 'REPORT_MATERIAL'=>$this->prefix.'report_material',
          'REPORT_ORDER'=>$this->prefix.'report_order');
		$this->_JOINS=array('CATEGORY'=>array('join_type'=>'LEFT','join_field'=>'materials.category_id=category.category_id',
                                           'select'=>'category.category_name','alias'=>'category'),
                           'INVENTORY'=>array('join_type'=>'LEFT','join_field'=>'materials.material_id=inventory.material_id',
                                           'select'=>'inventory.*','alias'=>'inventory'),
                           'MATERIAL_DEPARTMENTS'=>array('join_type'=>'LEFT','join_field'=>'materials.material_id=material_departments.material_id',
                                           'select'=>'material_departments.*','alias'=>'material_departments'),
                           'CATEGORY_DEPARTMENT'=>array('join_type'=>'RIGHT','join_field'=>'category_department.department_id=material_departments.group_id',
                                           'select'=>'category_department.*,category_department.order as category_order','alias'=>'category_department'),
                           'REPORT_MATERIAL'=>array('join_type'=>'LEFT','join_field'=>'materials.material_id=report_material.material_id', 'select'=>'report_material.*','alias'=>'report_material'),
                           'REPORT_ORDER'=>array('join_type'=>'LEFT','join_field'=>'materials.category_id=report_order.category_id', 'select'=>'report_order.*','alias'=>'report_order'),

                            );        
    }
    
    public function getMaterials($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''),$fields = NULL)
    {
      if($fields == NULL){
       $fields='materials.*';
      }
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['MATERIALS']. ' materials');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['MATERIALS'].' materials');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}