<?php

class Material extends Member_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->module_model('material', 'material_model');
        $this->load->module_model('category', 'category_model');
        $this->load->module_model('inventory', 'inventory_model');
        $this->load->module_model('unit', 'unit_model');

        $this->lang->module_load('material', 'material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }

    public function index() {
        // Display Page
        $this->material_model->joins = array('CATEGORY');
        $this->db->order_by('material_name');
        $data['materials'] = $this->material_model->getMaterials()->result_array();

        $data['units'] = $this->unit_model->getUnits()->result_array();

        $data['categories'] = $this->category_model->getCategories()->result_array();
        $data['header'] = 'Material';
        $data['view_page'] = "material/index";
        $data['module'] = 'material';
        $this->load->view($this->_container, $data);
    }

    public function getMaterial(){
        $id = $this->input->post('material_id');
        $data = $this->material_model->getmaterials(array('material_id'=>$id))->row_array();
        echo json_encode($data);
    }
    
    public function json() {
        $this->_get_search_param();
        $total = $this->material_model->count();
        paging('material_id');
        $this->_get_search_param();
        $rows = $this->material_model->getMaterials()->result_array();
        echo json_encode(array('total' => $total, 'rows' => $rows));
    }

    public function _get_search_param() {
        // Search Param Goes Here
        parse_str($this->input->post('data'), $params);
        if (!empty($params['search'])) {
            ($params['search']['material_name'] != '') ? $this->db->like('material_name', $params['search']['material_name']) : '';
            ($params['search']['category_id'] != '') ? $this->db->where('category_id', $params['search']['category_id']) : '';
            ($params['search']['status'] != '') ? $this->db->where('status', $params['search']['status']) : '';
        }


        if (!empty($params['date'])) {
            foreach ($params['date'] as $key => $value) {
                $this->_datewise($key, $value['from'], $value['to']);
            }
        }
    }

    public function combo_json() {
        $rows = $this->material_model->getMaterials()->result_array();
        echo json_encode($rows);
    }

    public function delete_json() {
        $id = $this->input->post('id');
        if ($id && is_array($id)) {
            foreach ($id as $row):
                $this->material_model->delete('MATERIALS', array('material_id' => $row));
            endforeach;
        }
    }

    public function save() {
         $this->load->library('form_validation');
        $this->form_validation->set_rules('material_name', 'Material', 'trim|required|is_unique[tbl_materials.material_name]');
            $data = $this->_get_posted_data(); //Retrive Posted Data        
    //        print_r($data);exit;
            $success = FALSE;
            if(!($data['material_name'] == "" || $data['category_id'] == "" || $data['unit'] == "")){
                if (!$this->input->post('material_id')) {
                    if ($this->form_validation->run() != FALSE){
                        $success = $this->material_model->insert('MATERIALS', $data);
                        if($success){
                            $material_id = $this->db->insert_id();
                            $this->inventory_model->insert('INVENTORY',array('material_id'=>$material_id));
                        }

                    }else{
                        $success = 3;
                    }
                } else {
                    $success = $this->material_model->update('MATERIALS', $data, array('material_id' => $data['material_id']));
                }
            }
            if ($success) {
                $success = TRUE;
                $msg = lang('success_message');
            }else if($success == 3){
                $success = FALSE;
                $msg = 'Duplicate Entry';
            } else {
                $success = FALSE;
                $msg = lang('failure_message');
            }

        echo json_encode(array('msg' => $msg, 'success' => $success));
    }

    private function _get_posted_data() {
        $data = array();
        
        if($this->input->post('material_id')){
            $data['material_id'] = $this->input->post('material_id');
        }

        $data['material_name'] = $this->input->post('material_name');
        $data['category_id'] = $this->input->post('category_id');
        $data['status'] = $this->input->post('status');
        $data['unit'] = $this->input->post('unit');
        $data['dispatch_record_from'] = $this->input->post('dispatch_record_from');

        return $data;
    }

}
