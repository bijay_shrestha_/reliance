<?php

class Material extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('material','material_model');
        $this->load->module_model('inventory','inventory_model');
        $this->lang->module_load('material','material');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'material';
		$data['page'] = $this->config->item('template_admin') . "material/index";
		$data['module'] = 'material';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{

        $this->material_model->joins = array('CATEGORY');
		$this->_get_search_param();	
		$total=$this->material_model->count();
		paging('material_id');
		$this->_get_search_param();	
		$rows=$this->material_model->getMaterials()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_name']!='')?$this->db->like('material_name',$params['search']['material_name']):'';
// ($params['search']['category_id']!='')?$this->db->where('materials.category_id',$params['search']['category_id']):'';
($params['search']['status']!='')?$this->db->where('status',$params['search']['status']):'';
($params['search']['category_name']!='')?$this->db->like('category.category_name',$params['search']['category_name']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->material_model->getMaterials()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->material_model->delete('MATERIALS',array('material_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('material_id'))
        {
            $success=$this->material_model->insert('MATERIALS',$data);
            if($success){
            	$id = $this->db->insert_id();
            	$this->inventory_model->insert('INVENTORY',array('material_id'=>$id));
            }
        }
        else
        {
            $success=$this->material_model->update('MATERIALS',$data,array('material_id'=>$data['material_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['material_id'] = $this->input->post('material_id');
		$data['material_name'] = $this->input->post('material_name');
		$data['category_id'] = $this->input->post('category_id');
		$data['status'] = 1; // $this->input->post('status');
		$data['unit'] = $this->input->post('unit');
		$data['dispatch_record_from'] = $this->input->post('dispatch_record_from');

        return $data;
   }
   
   public function combobox_material_id()
   {
   	$this->db->order_by('material_name');
   	$rows= $this->material_model->getMaterials()->result_array();
   	echo json_encode($rows);
   }
   public function combobox_material_id_by_category($category_id = NULL){
   	$this->db->where('category_id',$category_id);
   	$this->db->order_by('material_name');
   	$rows= $this->material_model->getMaterials()->result_array();
   	echo json_encode($rows);
   }
   	
	    
}