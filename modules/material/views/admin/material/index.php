<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('material_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="material-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('material_name')?></label>:</td>
<td><input type="text" name="search[material_name]" id="search_material_name"  class="easyui-validatebox"/></td>
<td><label>Category</label>:</td>
<td><input type="text" name="search[category_name]" id="search_category_id"  class="easyui-validatebox"/></td>
</tr>
<tr>

  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>
<input type="hidden" name="search[status]"/>

</form>
</div>
<br/>
<table id="material-table" data-options="pagination:true,title:'<?php  echo lang('material')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <!-- <th data-options="field:'material_id',sortable:true" width="30"><?php echo lang('material_id')?></th> -->
<th data-options="field:'material_name',sortable:true" width="50"><?php echo lang('material_name')?></th>
<th data-options="field:'category_name',sortable:true" width="50">Category</th>
<th data-options="field:'unit',sortable:true" width="50">Unit</th>
<th data-options="field:'dispatch_record_from',sortable:true,formatter:formatRecordFrom" width="60" align="center">Dispatch Record From</th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_material')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_material')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit material form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-material" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('material_name')?>:</label></td>
					  <td width="66%"><input name="material_name" id="material_name" class="easyui-validatebox" required="true"></td>
		       </tr>
		       <br><tr>
		              <td width="34%" ><label>Category:</label></td>
					  <td width="66%"><input name="category_id" id="category_id" class="easyui-combobox" required="true"></td>
		       </tr>
		       <br>
		       <tr>
		              <td width="34%" ><label>Unit:</label></td>
					  <td width="66%"><input name="unit" id="unit" class="easyui-combobox" required="true"></td>
		       </tr>
		       <tr>
		       		<td width="34%" ><label>Dispatch Record From</label></td>
				  	<td width="66%">
				  		<select id="dispatch_record_from" class="easyui-combobox" name="dispatch_record_from" style="width:200px;">
						    <option value="0">Dispatch</option>
						    <option value="1">Consumption</option>
						</select>
					</td>
		       </tr>
		       <input type="hidden" name="material_id" id="material_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#material-search-form').form('clear');
			$('#material-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#material-table').datagrid({
				queryParams:{data:$('#material-search-form').serialize()}
				});
		});		
		$('#material-table').datagrid({
			url:'<?php  echo site_url('material/admin/material/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
		$('#category_id').combobox({
			url:'<?php echo site_url('category/admin/category/combobox_category_id'); ?>',
			multiple: false,
			valueField: 'category_id',
			textField: 'category_name',
			panelHeight:'200',
			editable:false,

		});
		$('#unit').combobox({
			url:'<?php echo site_url('unit/admin/unit/combo_json'); ?>',
			multiple: false,
			valueField: 'unit',
			textField: 'unit',
			panelHeight:'200',
			editable:false,

		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning" iconcls="icon-edit"  title="<?php  echo lang('edit_material')?>"><span class="l-btn-left">Edit</span></a>';
		var d = '<a href="#" onclick="removematerial('+index+')" class="btn btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_material')?>"><span class="l-btn-left">Delete</span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function formatRecordFrom(value) {
		if(value == 1){
			return 'Consumption';
		}else{
			return 'Dispatch'
		}
	}
        
        
	function create(){
		//Create code here
		$('#form-material').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_material')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#material-table').datagrid('getRows')[index];
		if (row){
			$('#form-material').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_material')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removematerial(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#material-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('material/admin/material/delete_json')?>', {id:[row.material_id]}, function(){
					$('#material-table').datagrid('deleteRow', index);
					$('#material-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#material-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].material_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('material/admin/material/delete_json')?>',{id:selected},function(data){
						$('#material-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-material').form('submit',{
			url: '<?php  echo site_url('material/admin/material/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-material').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#material-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>