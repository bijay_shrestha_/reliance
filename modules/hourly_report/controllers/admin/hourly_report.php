<?php

class Hourly_report extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('hourly_report','hourly_report_model');
        $this->lang->module_load('hourly_report','hourly_report');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'hourly_report';
		$data['page'] = $this->config->item('template_admin') . "hourly_report/index";
		$data['module'] = 'hourly_report';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->hourly_report_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->hourly_report_model->getHourlyReports()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['date_np']!='')?$this->db->like('date_np',$params['search']['date_np']):'';
($params['search']['time']!='')?$this->db->like('time',$params['search']['time']):'';
($params['search']['residue']!='')?$this->db->like('residue',$params['search']['residue']):'';
($params['search']['sp_surface']!='')?$this->db->like('sp_surface',$params['search']['sp_surface']):'';
($params['search']['cao']!='')?$this->db->like('cao',$params['search']['cao']):'';
($params['search']['ist']!='')?$this->db->like('ist',$params['search']['ist']):'';
($params['search']['fst']!='')?$this->db->like('fst',$params['search']['fst']):'';
($params['search']['ir']!='')?$this->db->like('ir',$params['search']['ir']):'';
($params['search']['day_1']!='')?$this->db->like('day_1',$params['search']['day_1']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->hourly_report_model->getHourlyReports()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->hourly_report_model->delete('HOURLY_REPORT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->hourly_report_model->insert('HOURLY_REPORT',$data);
        }
        else
        {
            $success=$this->hourly_report_model->update('HOURLY_REPORT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['duration'] = $this->input->post('duration');
$data['residue'] = $this->input->post('residue');
$data['sp_surface'] = $this->input->post('sp_surface');
$data['cao'] = $this->input->post('cao');
$data['ist'] = $this->input->post('ist');
$data['fst'] = $this->input->post('fst');
$data['ir'] = $this->input->post('ir');
$data['day_1'] = $this->input->post('day_1');
$data['explanation'] = $this->input->post('explanation');

        return $data;
   }
   
   	
	    
}