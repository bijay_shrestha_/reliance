<?php


$lang['id'] = 'Id';
$lang['date_np'] = 'Date Np';
$lang['date_en'] = 'Date En';
$lang['time'] = 'Time';
$lang['duration'] = 'Duration';
$lang['residue'] = 'Residue';
$lang['sp_surface'] = 'Sp Surface';
$lang['cao'] = 'Cao';
$lang['ist'] = 'Ist';
$lang['fst'] = 'Fst';
$lang['ir'] = 'Ir';
$lang['day_1'] = 'Day 1';
$lang['explanation'] = 'Explanation';

$lang['create_hourly_report']='Create Hourly Report';
$lang['edit_hourly_report']='Edit Hourly Report';
$lang['delete_hourly_report']='Delete Hourly Report';
$lang['hourly_report_search']='Hourly Report Search';

$lang['hourly_report']='Hourly Report';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

