<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('hourly_report_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="hourly_report-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('date_np')?></label>:</td>
<td><input type="text" name="search[date_np]" id="search_date_np"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('date_en')?></label>:</td>
<td><input type="text" name="date[date_en][from]" id="search_date_en_from"  class="easyui-datebox"/> ~ <input type="text" name="date[date_en][to]" id="search_date_en_to"  class="easyui-datebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('time')?></label>:</td>
<td><input type="text" name="search[time]" id="search_time"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('duration')?></label>:</td>
<td><input type="text" name="search[duration]" id="search_duration"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('residue')?></label>:</td>
<td><input type="text" name="search[residue]" id="search_residue"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('sp_surface')?></label>:</td>
<td><input type="text" name="search[sp_surface]" id="search_sp_surface"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('cao')?></label>:</td>
<td><input type="text" name="search[cao]" id="search_cao"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('ist')?></label>:</td>
<td><input type="text" name="search[ist]" id="search_ist"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('fst')?></label>:</td>
<td><input type="text" name="search[fst]" id="search_fst"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('ir')?></label>:</td>
<td><input type="text" name="search[ir]" id="search_ir"  class="easyui-validatebox"/></td>
</tr>
<tr>
<td><label><?php echo lang('day_1')?></label>:</td>
<td><input type="text" name="search[day_1]" id="search_day_1"  class="easyui-validatebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="hourly_report-table" data-options="pagination:true,title:'<?php  echo lang('hourly_report')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'date_np',sortable:true" width="50"><?php echo lang('date_np')?></th>
<th data-options="field:'date_en',sortable:true" width="50"><?php echo lang('date_en')?></th>
<th data-options="field:'time',sortable:true" width="50"><?php echo lang('time')?></th>
<th data-options="field:'duration',sortable:true" width="50"><?php echo lang('duration')?></th>
<th data-options="field:'residue',sortable:true" width="50"><?php echo lang('residue')?></th>
<th data-options="field:'sp_surface',sortable:true" width="50"><?php echo lang('sp_surface')?></th>
<th data-options="field:'cao',sortable:true" width="50"><?php echo lang('cao')?></th>
<th data-options="field:'ist',sortable:true" width="50"><?php echo lang('ist')?></th>
<th data-options="field:'fst',sortable:true" width="50"><?php echo lang('fst')?></th>
<th data-options="field:'ir',sortable:true" width="50"><?php echo lang('ir')?></th>
<th data-options="field:'day_1',sortable:true" width="50"><?php echo lang('day_1')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_hourly_report')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_hourly_report')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit hourly_report form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-hourly_report" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('date_np')?>:</label></td>
					  <td width="66%"><input name="date_np" id="date_np" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('date_en')?>:</label></td>
					  <td width="66%"><input name="date_en" id="date_en" class="easyui-datebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('time')?>:</label></td>
					  <td width="66%"><input name="time" id="time" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('duration')?>:</label></td>
					  <td width="66%"><input name="duration" id="duration" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('residue')?>:</label></td>
					  <td width="66%"><input name="residue" id="residue" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('sp_surface')?>:</label></td>
					  <td width="66%"><input name="sp_surface" id="sp_surface" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('cao')?>:</label></td>
					  <td width="66%"><input name="cao" id="cao" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('ist')?>:</label></td>
					  <td width="66%"><input name="ist" id="ist" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('fst')?>:</label></td>
					  <td width="66%"><input name="fst" id="fst" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('ir')?>:</label></td>
					  <td width="66%"><input name="ir" id="ir" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('day_1')?>:</label></td>
					  <td width="66%"><input name="day_1" id="day_1" class="easyui-validatebox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('explanation')?>:</label></td>
					  <td width="66%"><textarea name="explanation" id="explanation" class="easyui-validatebox" required="true" style="width:300px;height:100px"></textarea></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#hourly_report-search-form').form('clear');
			$('#hourly_report-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#hourly_report-table').datagrid({
				queryParams:{data:$('#hourly_report-search-form').serialize()}
				});
		});		
		$('#hourly_report-table').datagrid({
			url:'<?php  echo site_url('hourly_report/admin/hourly_report/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_hourly_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removehourly_report('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_hourly_report')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-hourly_report').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_hourly_report')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#hourly_report-table').datagrid('getRows')[index];
		if (row){
			$('#form-hourly_report').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_hourly_report')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removehourly_report(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#hourly_report-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('hourly_report/admin/hourly_report/delete_json')?>', {id:[row.id]}, function(){
					$('#hourly_report-table').datagrid('deleteRow', index);
					$('#hourly_report-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#hourly_report-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('hourly_report/admin/hourly_report/delete_json')?>',{id:selected},function(data){
						$('#hourly_report-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-hourly_report').form('submit',{
			url: '<?php  echo site_url('hourly_report/admin/hourly_report/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-hourly_report').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#hourly_report-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>