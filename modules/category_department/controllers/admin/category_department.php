<?php

class Category_department extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('category_department','category_department_model');
        $this->lang->module_load('category_department','category_department');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index($department = NULL)
	{
		$data['department'] = $department;
		// Display Page
		$data['header'] = 'category_department';
		$data['page'] = $this->config->item('template_admin') . "category_department/index";
		$data['module'] = 'category_department';
		$this->load->view($this->_container,$data);		
	}

	public function json($department = NULL)
	{
		$where = array();
		if($department != NULL){
			$where['department_id'] = $department;
		}
		$this->category_department_model->joins=array('CATEGORIES','DEPARTMENTS');
		$this->_get_search_param();	
		$total=$this->category_department_model->count($where);


		paging('id');
		$this->_get_search_param();	
		$rows=$this->category_department_model->getCategoryDepartments($where)->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['category_id']!='')?$this->db->where('category_id',$params['search']['category_id']):'';
($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->category_department_model->getCategoryDepartments()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->category_department_model->delete('CATEGORY_DEPARTMENT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data	

        if(!$this->input->post('id'))
        {
        	$this->db->order_by('order','desc');
        	$where['department_id'] = $this->input->post('department_id');
        	$order = $this->category_department_model->getCategoryDepartments($where)->row_array();
        	$data['order'] = $order['order']+1;
            $success=$this->category_department_model->insert('CATEGORY_DEPARTMENT',$data);
        }
        else
        {
            $success=$this->category_department_model->update('CATEGORY_DEPARTMENT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['category_id'] = $this->input->post('category_id');
$data['department_id'] = $this->input->post('department_id');

        return $data;
   }
   
       
}