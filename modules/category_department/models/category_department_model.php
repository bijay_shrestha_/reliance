<?php
class Category_department_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('CATEGORY_DEPARTMENT'=>$this->prefix.'category_department','CATEGORIES'=>$this->prefix.'category','DEPARTMENTS'=>$this->prefix.'departments');
		$this->_JOINS=array('CATEGORIES'=>array('join_type'=>'LEFT','join_field'=>'category_departments.category_id=categories.category_id',
                                           'select'=>'categories.category_name','alias'=>'categories'),
                            'DEPARTMENTS'=>array('join_type'=>'LEFT','join_field'=>'category_departments.department_id=departments.id',
                                           'select'=>'departments.name as department_name','alias'=>'departments')
                           
                            );        
    }
    
    public function getCategoryDepartments($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='category_departments.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['CATEGORY_DEPARTMENT']. ' category_departments');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['CATEGORY_DEPARTMENT'].' category_departments');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}