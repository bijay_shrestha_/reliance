<div region="center" border="false">
<div style="padding:20px">
<?php /*<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('category_department_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="category_department-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('category_id')?></label>:</td>
<td><input type="text" name="search[category_id]" id="search_category_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('department_id')?></label>:</td>
<td><input type="text" name="search[department_id]" id="search_department_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>*/?>
<!-- <br/> -->
<table id="category_department-table" data-options="pagination:true,title:'<?php  echo lang('category_department')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <?php /*<th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'category_id',sortable:true" width="50"><?php echo lang('category_id')?></th>
<th data-options="field:'department_id',sortable:true" width="50"><?php echo lang('department_id')?></th>*/?>

    <th data-options="field:'category_name',sortable:true" width="100"><?php echo lang('id')?></th>
    <th field="action" width="30" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_category_department')?>"><?php  echo 'Add'?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_category_department')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit category_department form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-category_department" method="post" >
    <table>
		<tr>
		              <?php /*<td width="34%" ><label><?php echo lang('category_id')?>:</label></td>
					  <td width="66%">?><input name="category_id" id="category_id" required="true" hidden><?php </td>*/?>
		       </tr><tr>
		              <?php /*<td width="34%" ><label><?php echo lang('department_id')?>:</label></td>
					  <td width="66%">*/?><?php /*</td>*/?>
					  <input name="department_id" id="department_id" value='1' required="true" type='hidden'>
					  <td width="34%" ><label>Category Name:</label></td>
					  <td width="66%"><input name="category_id" id="category_id_assign" class="easyui-combobox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#category_department-search-form').form('clear');
			$('#category_department-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#category_department-table').datagrid({
				queryParams:{data:$('#category_department-search-form').serialize()}
				});
		});		
		$('#category_department-table').datagrid({
			url:'<?php  echo site_url('category_department/admin/category_department/json/'.$department)?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
		$('#category_id_assign').combobox({
			url:'<?php echo site_url('category/admin/category/combobox_category_id'); ?>',
			multiple: false,
			valueField: 'category_id',
			textField: 'category_name',
			panelHeight:'100',
			editable: false,

		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning" iconcls="icon-edit"  title="<?php  echo lang('edit_category_department')?>"><span class="l-btn-left">Edit</span></a>';
		var d = '<a href="#" onclick="removecategory_department('+index+')" class="btn btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_category_department')?>"><span class="l-btn-left">Delete</span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-category_department').form('clear');
		$('#department_id').val('<?php echo $department?>');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_category_department')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#category_department-table').datagrid('getRows')[index];
		if (row){
			$('#form-category_department').form('load',row);
			$('#department_id').val('<?php echo $department?>');
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_category_department')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removecategory_department(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#category_department-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('category_department/admin/category_department/delete_json')?>', {id:[row.id]}, function(){
					$('#category_department-table').datagrid('deleteRow', index);
					$('#category_department-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#category_department-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('category_department/admin/category_department/delete_json')?>',{id:selected},function(data){
						$('#category_department-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-category_department').form('submit',{
			url: '<?php  echo site_url('category_department/admin/category_department/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-category_department').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#category_department-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>