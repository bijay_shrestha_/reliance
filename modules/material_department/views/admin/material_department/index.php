<div region="center" border="false">
<div style="padding:20px">
<?php /*<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('material_department_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="material_department-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('material_id')?></label>:</td>
<td><input type="text" name="search[material_id]" id="search_material_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('department_id')?></label>:</td>
<td><input type="text" name="search[department_id]" id="search_department_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('units')?></label>:</td>
<td><input type="text" name="search[units]" id="search_units"  class="easyui-validatebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>*/?>
<table id="material_department-table" data-options="pagination:true,title:'<?php  echo lang('material_department')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <?php /*<th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'materialid',sortable:true" width="50"><?php echo lang('material_id')?></th>
<th data-options="field:'department_id',sortable:true" width="50"><?php echo lang('department_id')?></th>
<th data-options="field:'units',sortable:true" width="50"><?php echo lang('units')?></th>*/?>
<th data-options="field:'material_name',sortable:true" width="100"><?php echo lang('id')?></th>

    <th field="action" width="30" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_material_department')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_material_department')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit material_department form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-material_department" method="post" >
    <table>
				<tr>
			  			<td width="34%" ><label>Category Name:</label></td>
					  <td width="66%"><input name="category_id" id="category_id_assign" class="easyui-combobox" required="true">
					  </td>
			  	</tr><tr>
		              <td width="34%" ><label>Material Name:</label></td>
					  <td width="66%"><input name="material_id" id="material_id_assign" class="easyui-combobox" required="true">
					  </td>
		       	</tr><tr>
					  <input name="department_id" id="department_id" type="hidden">
		       </tr><tr>
		       </tr><input type="hidden" name="md_id" id="md_id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#material_department-search-form').form('clear');
			$('#material_department-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#material_department-table').datagrid({
				queryParams:{data:$('#material_department-search-form').serialize()}
				});
		});		
		$('#material_department-table').datagrid({
			url:'<?php  echo site_url('material_department/admin/material_department/json/'.$department)?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
		
		$('#category_id_assign').combobox({
			url:'<?php echo site_url('category/admin/category/combobox_category_id'); ?>',
			multiple: false,
			valueField: 'category_id',
			textField: 'category_name',
			panelHeight:'200',
			editable: false,
			onChange:function(category_id)
			{
				getMaterials(category_id);
			}

		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning" iconcls="icon-edit"  title="<?php  echo lang('edit_material_department')?>">Edit</a>';
		var d = '<a href="#" onclick="removematerial_department('+index+')" class="btn btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_material_department')?>"><span class="l-btn-left">Delete</a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-material_department').form('clear');
		$('#department_id').val('<?php echo $department?>');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_material_department')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#material_department-table').datagrid('getRows')[index];
		if (row){
			$('#form-material_department').form('load',row);
			$('#department_id').val('<?php echo $department?>');
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_material_department')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removematerial_department(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#material_department-table').datagrid('getRows')[index];
				// alert(row.md_id);
				$.post('<?php  echo site_url('material_department/admin/material_department/delete_json')?>', {id:[row.md_id]}, function(){
					$('#material_department-table').datagrid('deleteRow', index);
					$('#material_department-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#material_department-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('material_department/admin/material_department/delete_json')?>',{id:selected},function(data){
						$('#material_department-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-material_department').form('submit',{
			url: '<?php  echo site_url('material_department/admin/material_department/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-material_department').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#material_department-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	function getMaterials(category_id){
		$('#material_id_assign').combobox({
			url:'<?php echo site_url('material/admin/material/combobox_material_id_by_category'); ?>/'+category_id,
			multiple: false,
			valueField: 'material_id',
			textField: 'material_name',
			panelHeight:'100',
			editable: false,

		});
	}
	
	
</script>