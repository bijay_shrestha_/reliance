<?php
class Material_department_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('MATERIAL_DEPARTMENTS'=>$this->prefix.'material_departments','MATERIALS'=>$this->prefix.'materials','CATEGORY_DEPARTMENT'=>$this->prefix.'category_department','STOCK'=>$this->prefix.'stocks');
		$this->_JOINS=array('MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'material_departments.material_id=materials.material_id',
                                           'select'=>'materials.*','alias'=>'materials'),
        'CATEGORY_DEPARTMENT'=>array('join_type'=>'LEFT','join_field'=>'material_departments.cd_id=category_department.id',
                                           'select'=>'category_department.*','alias'=>'category_department'),
                            'STOCK'=>array('join_type'=>'LEFT','join_field'=>'material_departments.stock_id=stocks.stock_id',
                                           'select'=>'stocks.*,stocks.material_id as stock_material_id','alias'=>'stocks'),
                           
                            );        
    }
    
    public function getMaterialDepartments($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='material_departments.*,material_departments.material_id as materialid';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['MATERIAL_DEPARTMENTS']. ' material_departments');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['MATERIAL_DEPARTMENTS'].' material_departments');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}