<?php

class Material_department extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('material_department','material_department_model');
        $this->load->module_model('category_department','category_department_model');
        
        $this->lang->module_load('material_department','material_department');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index($department = NULL)
	{
		$data['department'] = $department;
		// Display Page
		$data['header'] = 'material_department';
		$data['page'] = $this->config->item('template_admin') . "material_department/index";
		$data['module'] = 'material_department';
		$this->load->view($this->_container,$data);		
	}

	public function json($department)
	{
		$this->_get_search_param();	
		$where['group_id'] = $department;
		$total=$this->material_department_model->count($where);
		paging('order');
		$this->_get_search_param();	
		$this->material_department_model->joins=array('MATERIALS','CATEGORY_DEPARTMENT','STOCK');
		$rows=$this->material_department_model->getMaterialDepartments($where)->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['material_id']!='')?$this->db->where('material_id',$params['search']['material_id']):'';
($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['units']!='')?$this->db->like('units',$params['search']['units']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->material_department_model->getMaterialDepartments()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->material_department_model->delete('MATERIAL_DEPARTMENTS',array('md_id'=>$row));
            endforeach;
		}else{
				$this->material_department_model->delete('MATERIAL_DEPARTMENTS',array('md_id'=>$id));
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data	
        if(!$this->input->post('md_id'))
        {
            $success=$this->material_department_model->insert('MATERIAL_DEPARTMENTS',$data);
        }
        else
        {
            $success=$this->material_department_model->update('MATERIAL_DEPARTMENTS',$data,array('md_id'=>$data['md_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['md_id'] = $this->input->post('md_id');
$data['material_id'] = $this->input->post('material_id');
$data['category_id'] = $this->input->post('category_id');
$data['group_id'] = $this->input->post('department_id');
$category_department = $this->category_department_model->getCategoryDepartments(array('category_id'=>$data['category_id'],'department_id'=>$data['group_id']))->row_array();
	$data['cd_id'] = $category_department['id'];

        return $data;
   }
   
   	
	    
}