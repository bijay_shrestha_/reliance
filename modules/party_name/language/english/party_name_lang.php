<?php


$lang['id'] = 'Id';
$lang['party_name'] = 'Party';
$lang['assigned_party_id'] = 'Assigned Party Id';

$lang['create_party_name']='Create Party';
$lang['edit_party_name']='Edit Party';
$lang['delete_party_name']='Delete Party';
$lang['party_name_search']='Party Search';

$lang['party_name']='Party';
$lang['action']='Actions';
$lang['saved_successful']='Saved Successful';
$lang['delete_successful']='Delete Successful';

$lang['assign_brand'] = 'Assign Brand';
$lang['view_assign'] = 'View Brand';
$lang['party_brand'] = 'Party Brand';

$lang['brand_name'] = 'Brand';
$lang['brand_type'] = 'Type';

$lang['agent'] = 'Agent';
