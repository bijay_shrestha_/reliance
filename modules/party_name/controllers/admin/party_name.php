<?php

class Party_name extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('party_name','party_name_model');
        $this->load->module_model('brand_type','brand_type_model');
        $this->load->module_model('party','party_model');
        $this->load->module_model('party_agent','party_agent_model');

        $this->lang->module_load('party_name','party_name');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['party'] = $this->party_model->getParties()->result_array();

		$data['brand_types'] = $this->brand_type_model->getBrandTypes()->result_array();
		// Display Page
		$data['header'] = 'party';
		$data['page'] = $this->config->item('template_admin') . "party_name/index";
		$data['module'] = 'party_name';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$this->party_name_model->joins = array('PARTY_AGENT','PARTY');
		$this->db->group_by('party_name');
		$this->db->where('deleted <>', 1);
		$total=$this->party_name_model->count();
		paging('id');
		$this->_get_search_param();	
		$this->party_name_model->joins = array('PARTY_AGENT','PARTY');
		$this->db->group_by('party_name');
		$this->db->where('deleted <>', 1);
		$rows=$this->party_name_model->getPartyNames()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['party_name']!='')?$this->db->like('party_name',$params['search']['party_name']):'';
// ($params['search']['assigned_party_id']!='')?$this->db->where('assigned_party_id',$params['search']['assigned_party_id']):'';
($params['search']['party']!='')?$this->db->where('party like','%'.$params['search']['party'].'%'):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->party_name_model->getPartyNames()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				// $this->party_name_model->delete('PARTY_NAME',array('id'=>$row));
				$success = $this->party_name_model->update('PARTY_NAME',array('deleted'=>1),array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
        
        $data=$this->_get_posted_data(); //Retrive Posted Data		
        $agents = $this->input->post('assigned_party_id');

        if(!$this->input->post('id'))
        {
            $success=$this->party_name_model->insert('PARTY_NAME',$data);
            $data['id'] = $this->db->insert_id();        }
        else
        {
            $success=$this->party_name_model->update('PARTY_NAME',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 

			$this->party_agent_model->delete('PARTY_AGENT',array('party_id' => $data['id']));
            foreach ($agents as $key => $value) {
            	$party_agent['party_id'] = $data['id'];
            	$party_agent['agent_id'] = $value;
            	$this->party_agent_model->insert('PARTY_AGENT',$party_agent);
            }
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['party_name'] = $this->input->post('party_name');
// $data['assigned_party_id'] = $this->input->post('assigned_party_id');

        return $data;
   }
   
   	function get_value(){
   		$where['id'] = $this->input->post('id');

   		$data = $this->party_name_model->getPartyNames($where)->row_array();

   		$where_party_agents['party_id'] = $where['id'];
   		$data['agent_ids'] = $this->party_agent_model->getPartyAgents($where_party_agents)->result_array();

   		echo json_encode($data);
   	}
	    
}