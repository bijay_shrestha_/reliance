<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('party_name_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="party_name-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('party_name')?></label>:</td>
<td><input type="text" name="search[party_name]" id="search_party_name"  class="easyui-validatebox"/></td>
<td><label><?php echo lang('agent')?></label>:</td>
<td><input type="text" name="search[party]" id="party"  class="easyui-validatebox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="party_name-table" data-options="pagination:true,title:'<?php  echo lang('party_name')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <!-- <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th> -->
<th data-options="field:'party_name',sortable:true" width="50"><?php echo lang('party_name')?></th>
<th data-options="field:'party',sortable:true" width="50"><?php echo lang('agent')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_party_name')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_party_name')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

 <!--for assign brnd to party form-->
<div id="assign_dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-brand-party" method="post" >
	    <select name='brand_type_id' id="brand_type_id" class="form-control margin-bottom">
			<option value="">-- Select Brand Type --</option>
			<?php foreach($brand_types as $brand_type){?>
				<option value="<?php echo $brand_type['id']?>"><?php echo $brand_type['name']?></option>
			<?php }?>
		</select>
		<input type="hidden" name="party_id" id="party_id">

		<select name='brand_id' id='brand' class="form-control margin-bottom">
			<option value="">-- Select Brand --</option>
		</select>
		<button type="button" id="" class="btn btn-primary margin form-group" onClick="save_brand()">Save</button> 
	</form>
</div>
<!--div ends-->

<!--for view party brand form-->
<div id="dlg_brand" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
        <table>
    		<thead>
    			<th data-options="field:'brand_name',sortable:true" width="50"><?php echo lang('brand_name')?></th>
    			<th data-options="field:'brand_type',sortable:true" width="50"><?php echo lang('brand_type')?></th>
    		</thead>
    		<tbody id="party_brand_detail">
    		</tbody>
		</table>
    
</div>
<!--div ends-->

<!--for create and edit party_name form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-party_name" method="post" >
    <table>
		<!-- <tr>
          	<td width="34%" ><label><?php echo lang('party_name')?>:</label></td>
		  	<td width="66%"><input name="party_name" id="party_name" class="easyui-validatebox" required="true"></td>
	   	</tr><tr>
          	<td width="34%" ><label><?php echo lang('assigned_party_id')?>:</label></td>
		  	<td width="66%"><input name="assigned_party_id" id="assigned_party_id" class="easyui-combobox" required="true"></td>
	   	</tr><input type="hidden" name="id" id="id"/> -->
        <div class="form-group">
            <label for="party" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" name="party_name" class="form-control" id="party_name" placeholder="party Name">
                <div id="error-party_name" hidden style="color: red" class="error-div">Name is required</div>
            </div>
        </div>
        <div class="form-group">
            <label for="party" class="col-sm-4 control-label">Agents</label>
            <div class="col-sm-8">
                <select name="assigned_party_id[]" class="form-control" id="assigned_party_id" placeholder="party"  multiple='multiple'>
                    <option value="" disabled>--Select party--</option>
                    <?php foreach ($party as $key => $value) {?>
                        <option value="<?php echo $value['id']?>" id="option-<?php echo $value['id']?>"><?php echo $value['party']?></option>
                    <?php }?>
                </select>
                <div id="error-assigned_party_id" hidden style="color: red" class="error-div">Party is required</div>
            </div>
        </div>

        <input type="hidden" name="id" id="id" value="">


    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
</div>
  
</div>

<!-- multiselect -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css.map">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dist.css">

<!-- for multiselector -->
<script src="<?php echo base_url()?>assets/js/jquery.multi-select.js" type="text/javascript"></script>
<!-- quick search -->
<script src="<?php echo base_url()?>assets/js/jquery.quicksearch.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#party_name-search-form').form('clear');
			$('#party_name-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#party_name-table').datagrid({
				queryParams:{data:$('#party_name-search-form').serialize()}
				});
		});		
		$('#party_name-table').datagrid({
			url:'<?php  echo site_url('party_name/admin/party_name/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});

		// $('#assigned_party_id').combobox({
		// 	url:'<?php echo site_url('party/admin/party/combo_json'); ?>',
		// 	multiple: false,
		// 	valueField: 'id',
		// 	textField: 'party',
		// 	panelHeight:'200',
		// 	editable:false,

		// });

		// $('#brand_type_id').change(function(){
		// 	var brand_type_id = $(this).val();
		// 	var party_id = $('#party_id').val();
		// 		$('#custom-headers').multiSelect('deselect_all');
		// 		if(brand_type_id != ''){
		// 			$.post('<?php echo site_url("party_brand/admin/party_brand/json")?>',{party_id:party_id, brand_type_id:brand_type_id},function(data){
		// 				$.each(data.rows,function(index,value){
		// 					// console.log(value.name);
		// 					$('#custom-headers').multiSelect('select', value.department_id);
		//             	});
		// 			},'json');
		// 		}
		// });

		// for multiple select
	    // with search
	    $('#assigned_party_id').multiSelect({
	      selectableHeader: "<div class='custom-header' style='background:#3c8dbc;color:white; text-align: center; font-size: 200%;'>Agents</div><br><input type='text' class='search-input form-control margin-bottom' autocomplete='off' placeholder='Search Department'>",
	      selectionHeader: "<div class='custom-header' style='background:#3c8dbc;color:white; text-align: center; font-size: 200%;'>Selected</div></br><input type='text' class='search-input form-control margin-bottom' autocomplete='off' placeholder='Search Selected'>",
	      afterInit: function(ms){
	        var that = this,
	            $selectableSearch = that.$selectableUl.prev(),
	            $selectionSearch = that.$selectionUl.prev(),
	            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
	            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

	        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
	        .on('keydown', function(e){
	          if (e.which === 40){
	            that.$selectableUl.focus();
	            return false;
	          }
	        });

	        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
	        .on('keydown', function(e){
	          if (e.which == 40){
	            that.$selectionUl.focus();
	            return false;
	          }
	        });
	      },
	      afterSelect: function(){
	        this.qs1.cache();
	        this.qs2.cache();
	      },
	      afterDeselect: function(){
	        this.qs1.cache();
	        this.qs2.cache();
	      }
	    });
	    // multiSelect ends
	});
	
	function getActions(value,row,index)
	{
		var a = '<button class="btn btn-primary btn-xs" onclick="assign_brand('+index+')"><?php echo lang("assign_brand")?></button>';
		var v = '<button class="btn btn-primary btn-xs" onclick="view_assign('+index+')"><?php echo lang("view_assign")?></button>';
		var e = '<button onclick="edit('+index+')" class="btn btn-warning btn-xs" title="<?php  echo lang('edit_party_name')?>"><?php  echo lang('edit_party_name')?></button>';
		var d = '<button onclick="removeparty_name('+index+')" class="btn btn-danger btn-xs" iconcls="icon-remove"  title="<?php  echo lang('delete_party_name')?>"><?php  echo lang('delete_party_name')?></button>';
		return v+' '+a+' '+e+' '+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-party_name').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_party_name')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#party_name-table').datagrid('getRows')[index];
		if (row){
			$('#assigned_party_id').multiSelect('deselect_all');
			$.post('<?php echo site_url("party_name/admin/party_name/get_value")?>',{id:row.id},function(data){
				$.each(data.agent_ids, function(index, value){
	                // $('#option-' + value.agent_id).attr('selected','selected');
	                $('#assigned_party_id').multiSelect('select', value.agent_id);
	            })
			},'json');

			$('#form-party_name').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_party_name')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}

	function assign_brand(index)
	{
		var row = $('#party_name-table').datagrid('getRows')[index];
		if (row){
			$('#party_id').val(row.id);
			$('#assign_dlg').window('open').window('setTitle','<?php  echo lang('assign_brand')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removeparty_name(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#party_name-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('party_name/admin/party_name/delete_json')?>', {id:[row.id]}, function(){
					$('#party_name-table').datagrid('deleteRow', index);
					$('#party_name-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#party_name-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('party_name/admin/party_name/delete_json')?>',{id:selected},function(data){
						$('#party_name-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-party_name').form('submit',{
			url: '<?php  echo site_url('party_name/admin/party_name/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-party_name').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#party_name-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}

	$('#brand_type_id').change(function(){
		var type_id = $(this).val();
		$('#brand').empty().append($('<option>',{value:'',text : '-- Select Brand --'}));
		$.post("<?php echo site_url('brand/admin/brand/get_brand')?>",{type_id:type_id},function(data){
			$.each(data.brands, function(i,v){
				$('#brand').append($('<option>', { 
			        value: v.id,
			        text : v.brand_name 
			    }));
			});
		},'json');
	});
	
	function save_brand(){
		$.post("<?php echo site_url('party_brand/admin/party_brand/save')?>",$('#form-brand-party').serializeArray(),function(data){
			if(data.success){
				alert('Data Saved');
				$('#assign_dlg').window('close');
			}else{
				alert('Error occured. Please Try Again');
			}
		},'json');
	}

	function view_assign(index){
		var row = $('#party_name-table').datagrid('getRows')[index];
		$("#party_brand_detail").html('');
		$.post("<?php echo site_url('party_brand/admin/party_brand/get_party_brand_json')?>",{party_id:row.id},function(data){
			$.each(data, function(i,v){
				$("#party_brand_detail").append('<tr><td>'+ v.brand_name + '</td><td>'+ v.type_name +'</td></tr>');
			});

			$('#dlg_brand').window('open').window('setTitle','<?php  echo lang('party_brand')?>');
		},'json')
	}

	
</script>