<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('material_property_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="material_property-search-form">
<table width="100%" cellspacing="1" cellpadding="1">
<tr>
<td><label>Brand</label>:</td>
<td><input type="text" name="search[brand_id]" id="search_brand_id"  class="easyui-validbox"/></td>
<td><label>Design</label>:</td>
<td><input type="text" name="search[design_id]" id="search_design_id"  class="easyui-validbox"/></td>
</tr>
<tr>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>
<input type="hidden" name="search[department_id]" id="search_department_id" value="<?php echo $department?>"/>
<input type="hidden" name="search[quality_id]" id="search_quality_id"/>
<input type="hidden" name="search[dimension_id]" id="search_dimension_id"/>
<input type="hidden" name="search[color_id]" id="search_color_id"/>
<input type="hidden" name="search[emboss_id]" id="search_emboss_id"/>
</form>
</div>
<br/>
<table id="material_property-table" data-options="pagination:true,title:'<?php  echo lang('material_property')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <?php /*<th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'department_id',sortable:true" width="50"><?php echo lang('department_id')?></th>*/?>
<th data-options="field:'brand_name',sortable:true" width="50">Brand</th>
<th data-options="field:'design_name',sortable:true" width="50">Design</th>
<?php /*<th data-options="field:'dimension_id',sortable:true" width="50"><?php echo lang('dimension_id')?></th>
<th data-options="field:'color_id',sortable:true" width="50"><?php echo lang('color_id')?></th>
<th data-options="field:'emboss_id',sortable:true" width="50"><?php echo lang('emboss_id')?></th>*/?>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_material_property')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_material_property')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit material_property form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-material_property" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label>Brand:</label></td>
					  <td width="66%"><input name="brand_id" id="brand_id" class="easyui-combobox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label>Design:</label></td>
					  <td width="66%"><input name="design_id" id="design_id" class="easyui-combobox" required="true"></td>
		       </tr><input type="hidden" name="mp_id" id="id"/>
		       <input type="hidden" name="department_id" id="department_id" >
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#material_property-search-form').form('clear');
			$('#material_property-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#material_property-table').datagrid({
				queryParams:{data:$('#material_property-search-form').serialize()}
				});
		});		
		$('#material_property-table').datagrid({
			url:'<?php  echo site_url('material_property/admin/material_property/json/'.$department)?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});

		$('#brand_id').combobox({
			url:'<?php echo site_url('brand/admin/brand/combobox_brand_id'); ?>',
			multiple: false,
			valueField: 'id',
			textField: 'brand_name',
			panelHeight:'200',
			editable:false,

		});
		
		$('#design_id').combobox({
			url:'<?php echo site_url('design/admin/design/combobox_design_id'); ?>',
			multiple: false,
			valueField: 'id',
			textField: 'design_name',
			panelHeight:'200',
			editable:false,

		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="btn btn-warning" iconcls="icon-edit"  title="<?php  echo lang('edit_material_property')?>">Edit</a>';
		var d = '<a href="#" onclick="removematerial_property('+index+')" class="btn btn-danger" iconcls="icon-remove"  title="<?php  echo lang('delete_material_property')?>">Delete</a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-material_property').form('clear');
		$('#department_id').val('<?php echo $department?>')
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_material_property')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#material_property-table').datagrid('getRows')[index];
		if (row){
			$('#form-material_property').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_material_property')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removematerial_property(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#material_property-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('material_property/admin/material_property/delete_json')?>', {id:[row.mp_id]}, function(){
					$('#material_property-table').datagrid('deleteRow', index);
					$('#material_property-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#material_property-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].mp_id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('material_property/admin/material_property/delete_json')?>',{id:selected},function(data){
						$('#material_property-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-material_property').form('submit',{
			url: '<?php  echo site_url('material_property/admin/material_property/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-material_property').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#material_property-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>