<?php


$lang['id'] = 'Id';
$lang['department_id'] = 'Department Id';
$lang['brand_id'] = 'Brand Id';
$lang['quality_id'] = 'Quality Id';
$lang['dimension_id'] = 'Dimension Id';
$lang['color_id'] = 'Color Id';
$lang['emboss_id'] = 'Emboss Id';

$lang['create_material_property']='Create Material Property';
$lang['edit_material_property']='Edit Material Property';
$lang['delete_material_property']='Delete Material Property';
$lang['material_property_search']='Material Property Search';

$lang['material_property']='Material Property';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

