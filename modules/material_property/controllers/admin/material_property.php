<?php

class Material_property extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('material_property','material_property_model');
        $this->lang->module_load('material_property','material_property');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index($department = NULL)
	{
		$data['department'] = $department;
		// Display Page
		$data['header'] = 'material_color';
		if($department == 1){
			$data['page'] = $this->config->item('template_admin') . "material_property/index";//for mixing
		}else if($department == 3){
			$data['page'] = $this->config->item('template_admin') . "material_property/printing";
		}else{
			$data['page'] = $this->config->item('template_admin') . "material_property/lamination";
		}
		$data['module'] = 'material_property';
		$this->load->view($this->_container,$data);		
	}

	public function json($department = NULL)
	{
		$where['department_id'] = $department;
		$this->material_property_model->joins = array('BRAND','QUALITY','DIMENSION','COLOR','EMBOSS','DESIGNS','COATING');
		$this->_get_search_param();	
		$total=$this->material_property_model->count($where);
		paging('material_properties.id');
		$this->_get_search_param();	
		$rows=$this->material_property_model->getMaterialProperties($where)->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['department_id']!='')?$this->db->where('department_id',$params['search']['department_id']):'';
($params['search']['brand_id']!='')?$this->db->like('brand_name',$params['search']['brand_id']):'';
($params['search']['quality_id']!='')?$this->db->like('quality_name',$params['search']['quality_id']):'';
($params['search']['dimension_id']!='')?$this->db->like('dimension_name',$params['search']['dimension_id']):'';
($params['search']['color_id']!='')?$this->db->like('color_name',$params['search']['color_id']):'';
($params['search']['emboss_id']!='')?$this->db->like('emboss_name',$params['search']['emboss_id']):'';
($params['search']['design_id']!='')?$this->db->like('design_name',$params['search']['design_id']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->material_property_model->getMaterialProperties()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->material_property_model->delete('MATERIAL_PROPERTY',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('mp_id'))
        {
            $success=$this->material_property_model->insert('MATERIAL_PROPERTY',$data);
        }
        else
        {
            $success=$this->material_property_model->update('MATERIAL_PROPERTY',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('mp_id');
$data['department_id'] = $this->input->post('department_id');
$data['brand_id'] = $this->input->post('brand_id');
$data['quality_id'] = $this->input->post('quality_id');
$data['dimension_id'] = $this->input->post('dimension_id');
$data['color_id'] = $this->input->post('color_id');
$data['emboss_id'] = $this->input->post('emboss_id');
$data['design_id'] = $this->input->post('design_id');


        return $data;
   }
   
   	
	    
}