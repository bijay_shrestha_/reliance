<?php
class Material_property_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('MATERIAL_PROPERTY'=>$this->prefix.'material_property','MATERIALS'=>$this->prefix.'materials','BRAND'=>$this->prefix.'brand','QUALITY'=>$this->prefix.'quality','DIMENSION'=>$this->prefix.'dimension','COLOR'=>$this->prefix.'color','EMBOSS'=>$this->prefix.'emboss','DESIGNS'=>$this->prefix.'designs','COATING'=>$this->prefix.'coating');
		$this->_JOINS=array('BRAND'=>array('join_type'=>'LEFT','join_field'=>'material_properties.brand_id=brand.id',
                                           'select'=>'brand.*','alias'=>'brand'),
                            'QUALITY'=>array('join_type'=>'LEFT','join_field'=>'material_properties.quality_id=quality.id',
                                           'select'=>'quality.*','alias'=>'quality'),
                            'DIMENSION'=>array('join_type'=>'LEFT','join_field'=>'material_properties.dimension_id=dimension.id',
                                           'select'=>'dimension.*','alias'=>'dimension'),
                            'COLOR'=>array('join_type'=>'LEFT','join_field'=>'material_properties.color_id=color.id',
                                           'select'=>'color.*','alias'=>'color'),
                            'EMBOSS'=>array('join_type'=>'LEFT','join_field'=>'material_properties.emboss_id=emboss.id',
                                           'select'=>'emboss.*','alias'=>'emboss'),
                            'MATERIALS'=>array('join_type'=>'LEFT','join_field'=>'material_properties.materials_id=materials.id',
                                           'select'=>'materials.*','alias'=>'materials'),
                            'DESIGNS'=>array('join_type'=>'LEFT','join_field'=>'material_properties.design_id=designs.id',
                                           'select'=>'designs.*','alias'=>'designs'),
                            'COATING'=>array('join_type'=>'LEFT','join_field'=>'material_properties.coating_id=coating.id',
                                           'select'=>'coating.*','alias'=>'coating'),
                           
                           
                            );        
    }
    
    public function getMaterialProperties($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='material_properties.*,material_properties.id as mp_id';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['MATERIAL_PROPERTY']. ' material_properties');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['MATERIAL_PROPERTY'].' material_properties');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}