<?php

class Plant extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('plant','plant_model');
        $this->lang->module_load('plant','plant');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'Plant';
		$data['page'] = "plant/index";
		$data['module'] = 'plant';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->plant_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->plant_model->getPlants()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->plant_model->getPlants()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->plant_model->delete('PLANT',array('id'=>$row));
            endforeach;
		}else{
			$this->plant_model->delete('PLANT',array('id'=>$id));
		}
		echo json_encode(array('success=>true'));
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->plant_model->insert('PLANT',$data);
        }
        else
        {
            $success=$this->plant_model->update('PLANT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['name'] = $this->input->post('name');

        return $data;
   }
   
   	
	    
}