<?php
class Payment_model extends MY_Model
{
	var $joins=array();
    public function __construct()
    {
    	parent::__construct();
        $this->prefix='tbl_';
        $this->_TABLES=array('PAYMENT'=>$this->prefix.'payment','BANK'=>$this->prefix.'bank','PARTY_NAME'=>$this->prefix.'party_name','PARTY'=>$this->prefix.'party');
		$this->_JOINS=array('BANK'=>array('join_type'=>'LEFT','join_field'=>'payments.bank_id=bank.id',
                                           'select'=>'bank.bank_name','alias'=>'bank'),
                            'PARTY_NAME'=>array('join_type'=>'LEFT','join_field'=>'payments.party_id=party_name.id',
                                           'select'=>'party_name.party_name','alias'=>'party_name'),
                            'PARTY'=>array('join_type'=>'LEFT','join_field'=>'payments.assigned_party_id=party.id',
                                           'select'=>'party.party','alias'=>'party'),
                           
                            );        
    }
    
    public function getPayments($where=NULL,$order_by=NULL,$limit=array('limit'=>NULL,'offset'=>''))
    {
       $fields='payments.*';
       
		foreach($this->joins as $key):
			$fields=$fields . ','.$this->_JOINS[$key]['select'];
		endforeach;
                
        $this->db->select($fields);
        $this->db->from($this->_TABLES['PAYMENT']. ' payments');
		
		foreach($this->joins as $key):
                    $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
		endforeach;	        
        
		(! is_null($where))?$this->db->where($where):NULL;
		(! is_null($order_by))?$this->db->order_by($order_by):NULL;

		if( ! is_null($limit['limit']))
		{
			$this->db->limit($limit['limit'],( isset($limit['offset'])?$limit['offset']:''));
		}
		return $this->db->get();	    
    }
    
    public function count($where=NULL)
    {
		
        $this->db->from($this->_TABLES['PAYMENT'].' payments');
        
        foreach($this->joins as $key):
        $this->db->join($this->_TABLES[$key]. ' ' .$this->_JOINS[$key]['alias'],$this->_JOINS[$key]['join_field'],$this->_JOINS[$key]['join_type']);
        endforeach;        
       
       (! is_null($where))?$this->db->where($where):NULL;
		
        return $this->db->count_all_results();
    }
}