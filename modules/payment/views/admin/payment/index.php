<div region="center" border="false">
<div style="padding:20px">
<div id="search-panel" class="easyui-panel" data-options="title:'<?php  echo lang('payment_search')?>',collapsible:true,iconCls:'icon-search'" style="padding:5px">
<form action="" method="post" id="payment-search-form">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
<tr><td><label><?php echo lang('party_id')?></label>:</td>
<td><input type="text" name="search[party_id]" id="search_party_id"  class="easyui-numberbox"/></td>
<td><label><?php echo lang('bank_id')?></label>:</td>
<td><input type="text" name="search[bank_id]" id="search_bank_id"  class="easyui-numberbox"/></td>
</tr>
<tr>
<td><label><?php echo lang('payment')?></label>:</td>
<td><input type="text" name="search[payment]" id="search_payment"  class="easyui-validatebox"/></td>
</tr>
  <tr>
    <td colspan="4">
    <a href="javascript:void(0)" class="easyui-linkbutton" id="search" data-options="iconCls:'icon-search'"><?php  echo lang('search')?></a>  
    <a href="javascript:void(0)" class="easyui-linkbutton" id="clear" data-options="iconCls:'icon-clear'"><?php  echo lang('clear')?></a>
    </td>
    </tr>
</table>

</form>
</div>
<br/>
<table id="payment-table" data-options="pagination:true,title:'<?php  echo lang('payment')?>',pagesize:'20', rownumbers:true,toolbar:'#toolbar',collapsible:true,fitColumns:true">
    <thead>
    <th data-options="field:'checkbox',checkbox:true"></th>
    <th data-options="field:'id',sortable:true" width="30"><?php echo lang('id')?></th>
<th data-options="field:'party_id',sortable:true" width="50"><?php echo lang('party_id')?></th>
<th data-options="field:'bank_id',sortable:true" width="50"><?php echo lang('bank_id')?></th>
<th data-options="field:'payment',sortable:true" width="50"><?php echo lang('payment')?></th>

    <th field="action" width="100" formatter="getActions"><?php  echo lang('action')?></th>
    </thead>
</table>

<div id="toolbar" style="padding:5px;height:auto">
    <p>
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="false" onclick="create()" title="<?php  echo lang('create_payment')?>"><?php  echo lang('create')?></a>
    <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="false" onclick="removeSelected()"  title="<?php  echo lang('delete_payment')?>"><?php  echo lang('remove_selected')?></a>
    </p>

</div> 

<!--for create and edit payment form-->
<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
        data-options="closed:true,collapsible:true,buttons:'#dlg-buttons',modal:true">
    <form id="form-payment" method="post" >
    <table>
		<tr>
		              <td width="34%" ><label><?php echo lang('party_id')?>:</label></td>
					  <td width="66%"><input name="party_id" id="party_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('bank_id')?>:</label></td>
					  <td width="66%"><input name="bank_id" id="bank_id" class="easyui-numberbox" required="true"></td>
		       </tr><tr>
		              <td width="34%" ><label><?php echo lang('payment')?>:</label></td>
					  <td width="66%"><input name="payment" id="payment" class="easyui-validatebox" required="true"></td>
		       </tr><input type="hidden" name="id" id="id"/>
    </table>
    </form>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onClick="save()"><?php  echo  lang('general_save')?></a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onClick="javascript:$('#dlg').window('close')"><?php  echo  lang('general_cancel')?></a>
	</div>    
</div>
<!--div ends-->
   
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('#clear').click(function(){
			$('#payment-search-form').form('clear');
			$('#payment-table').datagrid({
				queryParams:null
				});

		});

		$('#search').click(function(){
			$('#payment-table').datagrid({
				queryParams:{data:$('#payment-search-form').serialize()}
				});
		});		
		$('#payment-table').datagrid({
			url:'<?php  echo site_url('payment/admin/payment/json')?>',
			height:'auto',
			width:'auto',
			onDblClickRow:function(index,row)
			{
				edit(index);
			}
		});
	});
	
	function getActions(value,row,index)
	{
		var e = '<a href="#" onclick="edit('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-edit"  title="<?php  echo lang('edit_payment')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-edit"></span></span></a>';
		var d = '<a href="#" onclick="removepayment('+index+')" class="easyui-linkbutton l-btn" iconcls="icon-remove"  title="<?php  echo lang('delete_payment')?>"><span class="l-btn-left"><span style="padding-left: 20px;" class="l-btn-text icon-cancel"></span></span></a>';
		return e+d;		
	}
	
	function formatStatus(value)
	{
		if(value==1)
		{
			return 'Yes';
		}
		return 'No';
	}

	function create(){
		//Create code here
		$('#form-payment').form('clear');
		$('#dlg').window('open').window('setTitle','<?php  echo lang('create_payment')?>');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = $('#payment-table').datagrid('getRows')[index];
		if (row){
			$('#form-payment').form('load',row);
			//uploadReady(); //Uncomment This function if ajax uploading
			$('#dlg').window('open').window('setTitle','<?php  echo lang('edit_payment')?>');
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');				
		}		
	}
	
		
	function removepayment(index)
	{
		$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#payment-table').datagrid('getRows')[index];
				$.post('<?php  echo site_url('payment/admin/payment/delete_json')?>', {id:[row.id]}, function(){
					$('#payment-table').datagrid('deleteRow', index);
					$('#payment-table').datagrid('reload');
				});

			}
		});
	}
	
	function removeSelected()
	{
		var rows=$('#payment-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}
			
			$.messager.confirm('Confirm','<?php  echo lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?php  echo site_url('payment/admin/payment/delete_json')?>',{id:selected},function(data){
						$('#payment-table').datagrid('reload');
					});
				}
				
			});
			
		}
		else
		{
			$.messager.alert('Error','<?php  echo lang('edit_selection_error')?>');	
		}
		
	}
	
	function save()
	{
		$('#form-payment').form('submit',{
			url: '<?php  echo site_url('payment/admin/payment/save')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#form-payment').form('clear');
					$('#dlg').window('close');		// close the dialog
					$.messager.show({title: '<?php  echo lang('success')?>',msg: result.msg});
					$('#payment-table').datagrid('reload');	// reload the user data
				} 
				else 
				{
					$.messager.show({title: '<?php  echo lang('error')?>',msg: result.msg});
				} //if close
			}//success close
		
		});		
		
	}
	
	
</script>