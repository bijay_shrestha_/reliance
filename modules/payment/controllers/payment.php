<?php

class Payment extends Member_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('payment','payment_model');
        $this->load->module_model('party_name','party_name_model');
        $this->load->module_model('party','party_model');
        $this->load->module_model('bank','bank_model');
        $this->load->module_model('party_agent','party_agent_model');

        $this->lang->module_load('payment','payment');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$this->payment_model->joins = array('PARTY_NAME', 'BANK','PARTY');
		$data['rows'] = $this->payment_model->getPayments()->result_array();
		$data['parties'] = $this->party_name_model->getPartyNames()->result_array();
		$data['banks'] = $this->bank_model->getbanks()->result_array();

		$data['assigned_parties'] = $this->party_model->getParties()->result_array();
		// Display Page
		$data['header'] = 'Payment';
		$data['view_page'] = "sales/payment";
		$data['module'] = 'payment';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->payment_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->payment_model->getPayments()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['party_id']!='')?$this->db->where('party_id',$params['search']['party_id']):'';
($params['search']['bank_id']!='')?$this->db->where('bank_id',$params['search']['bank_id']):'';
($params['search']['payment']!='')?$this->db->like('payment',$params['search']['payment']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->payment_model->getPayments()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->payment_model->delete('PAYMENT',array('id'=>$row));
            endforeach;
		}else{
			$success = $this->payment_model->delete('PAYMENT',array('id'=>$id));
			echo json_encode($success);
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->payment_model->insert('PAYMENT',$data);
        }
        else
        {
            $success=$this->payment_model->update('PAYMENT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['bank_id'] = $this->input->post('bank_id');
$data['payment'] = $this->input->post('payment');
$data['date_np'] = $this->input->post('date_np');
$data['date_en'] = $this->input->post('date_en');
$data['assigned_party_id'] = $this->input->post('assigned_party_id');
$data['payment_from'] = $this->input->post('payment_from');
if($data['payment_from'] == 'Kathmandu'){
	$data['party_id'] = 0;
}

        return $data;
   }
   
   	public function get_value(){
   		$where['payments.id'] = $this->input->post('id');

   		$this->payment_model->joins = array('PARTY_NAME', 'BANK');
		$data = $this->payment_model->getPayments($where)->row_array();

		echo json_encode($data);
   	}	

   	function getParty_name(){
   		$where['agent_id'] = $this->input->post('id');

   		$this->party_agent_model->joins = array('PARTY_NAME');
   		$parties = $this->party_agent_model->getPartyAgents($where)->result_array();

   		echo json_encode($parties);
   	}
	    
}