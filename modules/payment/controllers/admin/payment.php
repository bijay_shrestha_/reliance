<?php

class Payment extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('payment','payment_model');
        $this->lang->module_load('payment','payment');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'payment';
		$data['page'] = $this->config->item('template_admin') . "payment/index";
		$data['module'] = 'payment';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->_get_search_param();	
		$total=$this->payment_model->count();
		paging('id');
		$this->_get_search_param();	
		$rows=$this->payment_model->getPayments()->result_array();
		echo json_encode(array('total'=>$total,'rows'=>$rows));
	}
	
	public function _get_search_param()
	{
		// Search Param Goes Here
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['party_id']!='')?$this->db->where('party_id',$params['search']['party_id']):'';
($params['search']['bank_id']!='')?$this->db->where('bank_id',$params['search']['bank_id']):'';
($params['search']['payment']!='')?$this->db->like('payment',$params['search']['payment']):'';

		}  

		
		if(!empty($params['date']))
		{
			foreach($params['date'] as $key=>$value){
				$this->_datewise($key,$value['from'],$value['to']);	
			}
		}
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->payment_model->getPayments()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->payment_model->delete('PAYMENT',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->payment_model->insert('PAYMENT',$data);
        }
        else
        {
            $success=$this->payment_model->update('PAYMENT',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['party_id'] = $this->input->post('party_id');
$data['bank_id'] = $this->input->post('bank_id');
$data['payment'] = $this->input->post('payment');

        return $data;
   }
   
   	
	    
}