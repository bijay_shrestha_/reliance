<!-- Logo -->
        <a href="<?php echo site_url('/admin'); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>R</b>M</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Reliance</b>MIS</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                    <span class="hidden-xs">RelianceMIS</span> <i class="fa fa-chevron-circle-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                      <img src="<?php echo base_url('uploads/logo.png')?>" class="img-circle" alt="User Image">
                    <p>
                      Reliacne MIS
                      <small>Admin</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                      <!-- <div class="pull-left">
                          <a href='<?php // echo site_url("account/admin/account")?>' class='btn btn-default btn-flat'>Profile</a>
                      </div> -->
                    <div class="pull-right">
                      <a href="javascript:void(0)" class="btn btn-default btn-flat" onclick="logout()"><?php echo lang('logout_menu')?></a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <?php /*<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>*/?>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <!--<div class="pull-left image">
              <!--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            <div class="pull-left info">
              <p>Admin</p>
            </div>-->
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
      <li>
        <a href="<?php echo site_url('admin')?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <!-- <li><a href="<?php echo site_url('home/index/store'); ?>"><i class="fa fa-files-o"></i> View Report </a></li> -->
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Users</span>
           <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('auth/admin/members')?>" ><?php echo  lang('members_menu')?></a></li>
          <li>
            <a href="<?php echo site_url('department/admin/department')?>">
              <span>Departments</span>
            </a>
            <li>
            <a href="<?php echo site_url('user_department/admin/user_department')?>">
              <span>Departments-User</span>
            </a>
          </li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Material Properties</span><i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a  href="<?php echo site_url('brand_type/admin/brand_type/index')?>"><i class="fa fa-files-o"></i> <span>Brand Type</span></a></li>
          <li><a  href="<?php echo site_url('brand/admin/brand/index')?>"><i class="fa fa-files-o"></i> <span>Brand</span></a></li>
          <li><a  href="<?php echo site_url('unit/admin/unit/index')?>"><i class="fa fa-files-o"></i> <span>Units</span></a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Store</span><i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('category/admin/category'); ?>">Category</a></li>
          <li><a  href="<?php echo site_url('material/admin/material')?>">Materials</a></li>
          <li><a href="<?php echo base_url('dealer/admin/dealer'); ?>">Dealer</a></li>
        </ul>
      </li>
      <li>
          <a href="<?php echo site_url('plant_operation/admin/plant_operation')?>"><i class="fa fa-clock-o"></i><span>Plant Working Hour</span></a>
      </li>
      <li>
          <a href="<?php echo site_url('sales_register/admin/sales_register')?>"><i class="fa fa-sign-out"></i><span>Sales Records</span></a>
      </li>
      <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Mixing|Calendar</span><i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('category_department/admin/category_department/index/1'); ?>">Category</a></li>
          <li><a  href="<?php echo site_url('material_department/admin/material_department/index/1')?>">Materials</a></li>
          <li><a  href="<?php echo site_url('material_property/admin/material_property/index/1')?>">Material Color</a></li>
          <li><a  href="<?php echo site_url('target/admin/target/index/2')?>">Target</a></li>
        </ul>
      </li> -->
      
      <!-- losses -->
      <?php /*<li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Losses</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <!-- timeloss -->
          <li>
            <a href="#"><i class="fa fa-circle-o"></i> Time Loss <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('tl_type/admin/tl_type'); ?>"><i class="fa fa-circle-o"></i> Type </a></li>
              <li><a href="<?php echo base_url('tl_reasoning/admin/tl_reasoning'); ?>"><i class="fa fa-circle-o"></i> Reason </a></li>
              <li><a href="<?php echo base_url('tl_reason_full/admin/tl_reason_full'); ?>"><i class="fa fa-circle-o"></i> Reason Full </a></li>
            </ul>
          </li>
          <!-- end timeloss -->
          <!-- timeloss -->
          <li>
            <a href="#"><i class="fa fa-circle-o"></i> Print Loss <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('printloss_type/admin/printloss_type'); ?>"><i class="fa fa-circle-o"></i> Type </a></li>
              <li><a href="<?php echo base_url('printloss_reason/admin/printloss_reason'); ?>"><i class="fa fa-circle-o"></i> Reason </a></li>
              <li><a href="<?php echo base_url('printloss_full/admin/printloss_full'); ?>"><i class="fa fa-circle-o"></i> Reason Full </a></li>
            </ul>
          </li>
          <!-- end timeloss -->
        </ul>
      </li>*/?>
<!-- end losses -->
<!-- reports -->
    <li>
        <a href="<?php echo site_url('department_report/admin/department_report')?>">
            <i class="fa fa-files-o"></i><span>Report</span>
        </a>
    </li>
<!-- end reports -->
<!-- reports -->
    <li>
        <a href="<?php echo site_url('bank/admin/bank')?>">
            <i class="fa fa-files-o"></i><span>Bank</span>
        </a>
    </li>
    <li>
      <a href="<?php echo base_url('plant/admin/plant'); ?>">
        <i class="fa fa-bank"></i><span>Plants</span>
      </a>
    </li>
      <li><a href="<?php echo base_url('electric_meter/admin/electric_meter'); ?>"><i class="fa fa-plug"></i><span>Electric Meter</span></a></li>
<!-- end reports -->
<!-- reports -->
    <li>
        <a href="<?php echo site_url('border/admin/border')?>">
            <i class="fa fa-files-o"></i><span>Dry Port</span>
        </a>
    </li>
<!-- end reports -->
<!-- silo opening -->
      <li>
        <a href="<?php echo site_url('silo_report/admin/silo_report/opening')?>">
            <i class="fa fa-industry"></i><span>Silo Opening</span>
        </a>
      </li>
      <!-- end silo opening -->

<!-- sales -->
      <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Sales</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('brand_type/admin/brand_type'); ?>"><i class="fa fa-circle-o"></i> Brand Type </a></li>
              <li><a href="<?php echo base_url('brand/admin/brand'); ?>"><i class="fa fa-circle-o"></i> Brand </a></li>
              <li><a href="<?php echo base_url('party/admin/party'); ?>"><i class="fa fa-circle-o"></i> Agent </a></li>
              <li><a href="<?php echo base_url('party_name/admin/party_name'); ?>"><i class="fa fa-circle-o"></i> Party </a></li>
            </ul>
        </ul>
      </li>
<!-- end sales -->

<!-- Report Records -->
      <li class="treeview">
        <a href="#">
          <i class="fa fa-th-list" aria-hidden="true"></i> <span>Lab Records</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('company/admin/company'); ?>"><i class="fa fa-circle-o"></i> Company </a></li>
              <li><a href="<?php echo base_url('supplier/admin/supplier'); ?>"><i class="fa fa-circle-o"></i> Supplier </a></li>
              <li><a href="<?php echo base_url('master_chemical/admin/master_chemical'); ?>"><i class="fa fa-circle-o"></i> Chemical </a></li>
              <li><a href="<?php echo base_url('report_list/admin/report_list'); ?>"><i class="fa fa-circle-o"></i> Record List </a></li>
              <li><a href="<?php echo base_url('report_chemical_usage/admin/report_chemical_usage'); ?>"><i class="fa fa-circle-o"></i> Chemical Usage In Report </a></li>
            </ul>
        </ul>
      </li>
<!-- end Report Records -->
      

      <?php /*
      <li><a href="<?php print site_url('auth/admin/access_control')?>"><?php echo lang('access_control_menu')?></a></li>
      <li><a  href="<?php echo site_url('admin/settings')?>"  plain="false" iconCls="icon-tools"><?php echo lang('settings_menu')?></a></li>
      <li><a  href="<?php echo site_url('tools/admin/generators')?>"><?php echo lang('generator_menu')?></a></li>*/?>



    </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->

<script language="javascript">
  function logout(){
    $.messager.defaults={ok:"OK",cancel:"<?php echo lang('general_cancel')?>",width: 300};
    $.messager.confirm('<?php echo lang('confirm')?>', '<?php echo lang('logout_confirm')?>', function(r){
    if (r){
     location.href = '<?php echo site_url('auth/logout')?>';
    }
   });
  }
 </script>

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <section class="content-header">
            <h1>
                    <?php echo ucwords(str_replace('_', ' ', $header))?>
            </h1>
            <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </ol>
            </section>
<!-- Main content -->
        <section class="content">


        