<!DOCTYPE html>
<html lang="en">
<head>
	<?php print $this->bep_site->get_metatags();?>
	<title><?php print $header.' | '.$this->preference->item('site_name');?></title>
	<?php print $this->bep_site->get_variables();?>
	
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- custom -->
    <script src="<?php echo base_url()?>assets/js/custom.js"></script>

    <!-- easyui -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/themes/bootstrap/easyui.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/themes/icon.css">
    <!-- multiselect -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css.map">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dist.css">


    <?php  print $this->bep_assets->get_header_assets();?>
    <?php print $this->bep_site->get_js_blocks();?>

    <!-- Nepali DatePicker -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/nepali.datepicker.v2.min.css">
    
    <!--<script>
    $(document).ready(function()
    {
    $('body').show();
    });
</script>    -->
<style type="text/css">
    .window{
        /*width: 100% !important;*/
        /*left: 0 !important;*/
        /*top: -10px !important;*/
    }

    .image{
        height: 50%;
        overflow-y: scroll;
        overflow-x: hidden;
    }

    .full-form{
        height: 50%;
        overflow-y: scroll;
        padding-top: 5px;
    }

    .panel-header{
        /*display: none;*/
    }

 /*   .window-shadow, .window-mask{
        height: 0 !important;
        display: none !important;
    }*/

    .dialog-button{
        padding: 0 2px;
    }

    #dlg{
        /*padding: 10px 0px !important;*/
        /*width: 100% !important; */
        /*padding-right: 0;*/
        /*margin-right: 0;*/
        /*margin-left: 0;*/
    }

    .full-form .row{
        margin: 0;
    }
</style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <?php print $this->load->view($this->config->item('template_admin') . 'menu');?>