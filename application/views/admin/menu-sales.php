<!-- Logo -->
        <a href="<?php echo site_url('/admin'); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>R</b>M</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Reliance</b>MIS</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                    <span class="hidden-xs">RelianceMIS</span> <i class="fa fa-chevron-circle-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                      <img src="<?php echo base_url('uploads/logo.png')?>" class="img-circle" alt="User Image">
                    <p>
                      Reliacne MIS
                      <small>Admin</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                      <!-- <div class="pull-left">
                          <a href='<?php // echo site_url("account/admin/account")?>' class='btn btn-default btn-flat'>Profile</a>
                      </div> -->
                    <div class="pull-right">
                      <a href="javascript:void(0)" class="btn btn-default btn-flat" onclick="logout()"><?php echo lang('logout_menu')?></a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <?php /*<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>*/?>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <!--<div class="pull-left image">
              <!--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            <div class="pull-left info">
              <p>Admin</p>
            </div>-->
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
      <li>
        <a href="<?php echo site_url('admin')?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <!-- <li><a href="<?php echo site_url('home/index/store'); ?>"><i class="fa fa-files-o"></i> View Report </a></li> -->
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Users</span>
           <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('auth/admin/members')?>" ><?php echo  lang('members_menu')?></a></li>
          <li>
            <a href="<?php echo site_url('department/admin/department')?>">
              <span>Departments</span>
            </a>
            <li>
            <a href="<?php echo site_url('user_department/admin/user_department')?>">
              <span>Departments-User</span>
            </a>
          </li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Material Properties</span><i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a  href="<?php echo site_url('brand_type/admin/brand_type/index')?>"><i class="fa fa-files-o"></i> <span>Brand Type</span></a></li>
          <li><a  href="<?php echo site_url('brand/admin/brand/index')?>"><i class="fa fa-files-o"></i> <span>Brand</span></a></li>
          <li><a  href="<?php echo site_url('unit/admin/unit/index')?>"><i class="fa fa-files-o"></i> <span>Units</span></a></li>
        </ul>
      </li>
      <li>
          <a href="<?php echo site_url('sales_register/admin/sales_register')?>"><i class="fa fa-sign-out"></i><span>Sales Records</span></a>
      </li>
      
    <li>
        <a href="<?php echo site_url('bank/admin/bank')?>">
            <i class="fa fa-files-o"></i><span>Bank</span>
        </a>
    </li>
    
      <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Sales</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('brand_type/admin/brand_type'); ?>"><i class="fa fa-circle-o"></i> Brand Type </a></li>
              <li><a href="<?php echo base_url('brand/admin/brand'); ?>"><i class="fa fa-circle-o"></i> Brand </a></li>
              <li><a href="<?php echo base_url('party/admin/party'); ?>"><i class="fa fa-circle-o"></i> Agent </a></li>
              <li><a href="<?php echo base_url('party_name/admin/party_name'); ?>"><i class="fa fa-circle-o"></i> Party </a></li>
            </ul>
        </ul>
      </li>
<!-- end sales -->
      

      <?php /*
      <li><a href="<?php print site_url('auth/admin/access_control')?>"><?php echo lang('access_control_menu')?></a></li>
      <li><a  href="<?php echo site_url('admin/settings')?>"  plain="false" iconCls="icon-tools"><?php echo lang('settings_menu')?></a></li>
      <li><a  href="<?php echo site_url('tools/admin/generators')?>"><?php echo lang('generator_menu')?></a></li>*/?>

    </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->

<script language="javascript">
  function logout(){
    $.messager.defaults={ok:"OK",cancel:"<?php echo lang('general_cancel')?>",width: 300};
    $.messager.confirm('<?php echo lang('confirm')?>', '<?php echo lang('logout_confirm')?>', function(r){
    if (r){
     location.href = '<?php echo site_url('auth/logout')?>';
    }
   });
  }
 </script>

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <section class="content-header">
            <h1>
                    <?php echo ucwords(str_replace('_', ' ', $header))?>
            </h1>
            <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </ol>
            </section>
<!-- Main content -->
        <section class="content">


        