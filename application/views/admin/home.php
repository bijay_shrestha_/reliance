<?php /*
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Statistics</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <tr>
                      <th><?php print $this->lang->line('dashboard_statistics_name') ?></th>
                      <th><?php print $this->lang->line('dashboard_statistics_value') ?></th>
                    </tr>
                    <tr>
                       <td><?php print $this->lang->line('dashboard_statistics_total_members') ?></td> 
                       <td><?php print $total_members ?></td>
                    </tr>
                    <tr>
                        <td><?php print $this->lang->line('dashboard_statistics_total_unactivated_members') ?></td>
                        <td><?php print $total_unactivated_members ?></td>
                    </tr>
                    <tr>
                        <td><?php print $this->lang->line('dashboard_statistics_user_registration') ?></td>
                        <td><?php print $user_registration ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php print $this->lang->line('dashboard_statistics_ci_version') ?></td>
                        <td><?php print CI_VERSION ?></td>
                    </tr>
                    <tr>
                        <td><?php print $this->lang->line('dashboard_statistics_bep_version') ?></td>
                        <td><?php print BEP_VERSION ?></td>
                    </tr>
                    <tr>
                        <td><?php print $this->lang->line('dashboard_statistics_php_version') ?></td>
                        <td><?php print phpversion();?></td>
                    </tr>
                </table>
            </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col-md-12 -->
          </div><!-- /.row -->*/?>
        </section><!-- /.content -->
</div><!-- /.content-wrapper -->