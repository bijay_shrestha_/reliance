<?php
class Member_Controller extends Public_Controller
{
	var $user_id=NULL;
	var $new_mails=0;
	var $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadhra','Ashoj','Kartik','Mangshir','Poush','Magh','Falgun','Chaitra');
	var $check_permission;
	var $month_options = array(
		'4' => 'Shrawan',
		'5' => 'Bhadhra',
		'6' => 'Ashoj',
		'7' => 'Kartik',
		'8' => 'Mangshir',
		'9' => 'Poush',
		'10' => 'Magh',
		'11' => 'Falgun',
		'12' => 'Chaitra',
		'1' => 'Baishak',
		'2' => 'Jesth',
		'3' => 'Ashad');
	var $record_list;
	public function __construct()
	{
		parent::__construct();
		if(!is_user())
		{
			redirect(site_url('auth/login'));
		}
		$this->user_id=$this->session->userdata('id');
		$this->load->module_model('permission','permission_model');
/*		$this->load->module_model('message','message_model');
		$this->message_model->joins=array('MESSAGE_RECIPIENTS');
		$this->new_mails=$this->message_model->countMessages(array('recipient'=>$this->user_id,'is_read'=>0));
*/		$this->record_list = $this->get_record_list();
		$this->google_tracking=FALSE;		
	}

	public function get_record_list()
	{
		$record_list = $this->db->get('tbl_report_lists')->result_array();
		return $record_list;
	}


	
}
