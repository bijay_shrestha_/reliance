<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo theme_url()?>assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="<?php echo theme_url()?>assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo theme_url()?>assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo theme_url()?>assets/ico/favicon.png">
  <title><?php print $header.' | '.$this->preference->item('site_name')?></title>
        
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/AdminLTE.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/buttons.dataTables.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/skins/_all-skins.min.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo theme_url()?>plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo theme_url()?>plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo theme_url()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Nepali Datepicker -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/nepalidatepicker/nepalidatepicker.css" />
    <!-- custom css -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/style.css" />

    <!-- multiselect -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css.map">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dist.css">

    <!-- <link rel="stylesheet" type="text/css" href="<?php echo theme_url()?>nepali.datepicker.v2.1.min.css" /> -->
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo theme_url()?>plugins/jQuery/jQuery-2.1.4.min.js"></script>

  </head>

  <body class="hold-transition skin-yellow sidebar-mini" style="padding:0px">
      <div class="wrapper">

      <?php 
      if(is_user()){
        ?>

          <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo site_url()?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>R</b>MIS</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Reliance</b>MIS</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
                  <span class="hidden-xs"><?php echo $this->session->userdata('username');?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
                    <p>
                      <?php echo $this->session->userdata('username');?>
                      <small><?php echo $this->session->userdata('department')?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo site_url('auth/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li <?php if($header == 'Dashboard'){?>class="active"<?php }?>>
              <a href="<?php echo site_url()?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            <?php if($this->session->userdata('department_id') == '7'){?>
            <li>
                <!--<a href="<?php echo site_url('report')?>">Report</a>-->
            </li>
            <li>
                <a href="<?php echo site_url('dealer')?>"><i class="fa fa-user"></i><span>Raw Material Dealer</span></a>
            </li>
            <li>
                <a href="<?php echo site_url('material')?>"><i class="fa fa-archive"></i><span>Material</span></a>
            </li>
            <li>
                <a href="<?php echo site_url('inventory')?>"><i class="fa fa-gift"></i><span>Inventory</span></a>
            </li>
            <!--<li>
                <a href="<?php echo site_url('inventory/report')?>">Inventory Report</a>
            </li>-->
            <!-- report-->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Inventory Report</span>
                
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url('inventory/report')?>"><i class="fa fa-circle-o"></i> Report</a></li>
                <li><a href="<?php echo site_url('report_type')?>"><i class="fa fa-circle-o"></i> Report Type</a></li>
              </ul>
            </li>
            <!-- dispatch-->
            <li class="treeview">
              
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url('inventory/dispatch')?>"><i class="fa fa-circle-o"></i> Dispatch</a></li>
                <li><a href="<?php echo site_url('direct_dispatch_material')?>"><i class="fa fa-circle-o"></i> Assign Dispatch</a></li>
              </ul>
            </li>
            <!-- dispatch-->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-sign-out"></i>
                <span>Dispatch <!--<span class="label label-danger"><?php echo $this->get_dispatch_for_header?></span>--></span>
                
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url('inventory/dispatch')?>"><i class="fa fa-circle-o"></i> Dispatch</a></li>
                <li><a href="<?php echo site_url('direct_dispatch_material')?>"><i class="fa fa-circle-o"></i> Assign Dispatch</a></li>
              </ul>
            </li>
            <!--time loss-->
            <?php /*<li>
                <a href="<?php echo site_url('time_loss/report')?>">Timeloss</a>
            </li>*/?>
            <?php }

            elseif($this->session->userdata('group_id') == '1') { ?>
              <?php /*<li class="treeview">
                <a href="#"><i class="fa fa-files-o"></i><span>Departments</span></a>
                <ul class="treeview-menu">*/?>
                <?php /*</ul>
              </li>*/?>
              <?php if($this->session->userdata('department_id') == 1 || $this->session->userdata('department_id') == 3 || $this->session->userdata('department_id') == 5){?>
                  <?php if($this->session->userdata('department_id') == 3){?>
                      <li><a href="<?php echo site_url('print_shift_report')?>"><i class="fa fa-circle-o"></i>Print Shift Report</a></li>
                  <?php }?>
                  <!-- <li><a href="<?php echo site_url('department')?>"><i class="fa fa-circle-o"></i>Demand Request</a></li> -->
                  <?php if($this->session->userdata('department_id') == 5){?>
                    <li><a href="<?php echo site_url('rexin_coating_report')?>"><i class="fa fa-circle-o"></i>Coating Production Report</a></li>
                  <?php }?>
                <li><a href="<?php echo site_url('consumption');?>"><i class="fa fa-times" aria-hidden="true"></i><span>Consumption</span></a></li>
              <li><a href="<?php echo site_url('stock');?>"><i class="fa fa-times" aria-hidden="true"></i><span>Stock Report</span></a></li>
              <?php }?>
              <?php if($this->session->userdata('department_id') == 2 ){?>
                <li><a href="<?php echo site_url('calander_shift_report/daily');?>"><i class="fa fa-times" aria-hidden="true"></i><span>Daily Production Shift Report</span></a></li>
                <li><a href="<?php echo site_url('calander_shift_report');?>"><i class="fa fa-times" aria-hidden="true"></i><span>Production Shift Report</span></a></li>
              <?php }
              if($this->session->userdata('department_id') == 4){?>
                <li><a href="<?php echo site_url('laminating_shift_report');?>"><i class="fa fa-times" aria-hidden="true"></i><span>Production Shift Report</span></a></li>
              <?php }?>
              <?php /*if(in_array($this->session->userdata('department_id'),$department_with_scrap)){?>
                  <li><a href="<?php echo site_url('scrap_record');?>"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Scrap</span></a></li>
              <?php }*/?>
            <?php }
            if($this->session->userdata('department_id') == 20){?>
                <li><a href="<?php echo site_url('attendance');?>"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Attendance</span></a></li>
                <li><a href="<?php echo site_url('diesel');?>"><i class="fa fa-tint" aria-hidden="true"></i><span>Diesel</span></a></li>
                <li><a href="<?php echo site_url('scrap_segregation');?>"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Scrap Segregation</span></a></li>
                <li><a href="<?php echo site_url('scrap_report');?>"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Scrap Report</span></a></li>
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Paper Tube</span>
                    
                    <i class='fa fa-angle-left pull-right'></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('paper_tube');?>"><i class="fa fa-circle-o" aria-hidden="true"></i><span>Production</span></a></li>
                    <li><a href="<?php echo site_url('paper_tube_working')?>"><i class="fa fa-circle-o"></i> Working Hours</a></li>
                  </ul>
                </li>
                <li><a href="<?php echo site_url('inspection');?>"><i class="fa fa-search" aria-hidden="true"></i><span>Inspection</span></a></li>
                <li><a href="<?php echo site_url('report_status');?>"><i class="fa fa-files-o" aria-hidden="true"></i><span>Reports</span></a></li>
            <?php }
            if($this->session->userdata('department_id') == 21){?>
                <li><a href="<?php echo site_url('lc');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>LC</span></a></li>
                <li><a href="<?php echo site_url('lc_track');?>"><i class="fa fa-calendar" aria-hidden="true"></i><span>Track LC</span></a></li>
                <li><a href="<?php echo site_url('cost_sheet');?>"><i class="fa fa-money" aria-hidden="true"></i><span>Cost Sheet</span></a></li>
                <li><a href="<?php echo site_url('lc_track/closed');?>"><i class="fa fa-lock" aria-hidden="true"></i><span>Closed LC</span></a></li>
            <?php 
            }
            ?>
            <?php if($this->session->userdata('group_id') == 2 || $this->session->userdata('department_id') == 31){?>
                <?php if($this->session->userdata('group_id') == 2){?><li><a href="<?php echo site_url('admin');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Admin Section</span></a></li><?php }?>
                <li><a href="<?php echo site_url('home/index/store');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Store Report</span></a></li>
                <li><a href="<?php echo site_url('home/index/mixing');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Mixing Report</span></a></li>
                <li><a href="<?php echo site_url('home/index/calendar');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Calendar Report</span></a></li>
                <li><a href="<?php echo site_url('home/index/lamination');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Lamination Report</span></a></li>
                <li><a href="<?php echo site_url('home/index/printing');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Printing Report</span></a></li>
                <li><a href="<?php echo site_url('home/index/rexin');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Rexin Report</span></a></li>
                <li><a href="<?php echo site_url('home/index/office');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>Office Report</span></a></li>
                <li><a href="<?php echo site_url('home/index/ktm_office');?>"><i class="fa fa-file-o" aria-hidden="true"></i><span>LC Report</span></a></li>
            <?php }?>

          <?php // if($this->check_permission('Department Request')){?>
          <!-- <li><a href="<?php echo site_url('department')?>"><i class="fa fa-circle-o"></i>Demand Request</a></li> -->
          <?php // }?>
                <li><a href="<?php echo base_url('sales_register'); ?>"><i class="fa fa-sign-out"></i> <span>Sales</span> </a></li>
                <li><a href="<?php echo base_url('payment'); ?>"><i class="fa fa-money"></i> <span>Payment</span> </a></li>
                <li><a href="<?php echo base_url('sales_register/report'); ?>"><i class="fa fa-area-chart"></i> <span>Report</span> </a></li>
                <li><a href="<?php echo base_url('daily_production_sale'); ?>"><i class="fa fa-industry"></i> <span>Daily Production Sales</span> </a></li>
                <li><a href="<?php echo site_url('electric_report');?>"><i class="fa fa-plug" aria-hidden="true"></i><span>Electric Report</span></a></li>
                <li><a href="<?php echo site_url('electric_meter_consumption');?>"><i class="fa fa-plug" aria-hidden="true"></i><span>Electric Meter Consumption</span></a></li>
                <li><a href="<?php echo site_url('plant_operation');?>"><i class="fa fa-bank" aria-hidden="true"></i><span>Plant_operation</span></a></li>
                <li><a href="<?php echo site_url('hourly_report');?>"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Hourly Report</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text-o"></i> <span>Ledger</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('sales_register/ledger'); ?>"><i class="fa fa-circle-o"></i> Agent Ledger </a></li>
                <li><a href="<?php echo base_url('sales_register/ledger/party'); ?>"><i class="fa fa-circle-o"></i> Party Ledger </a></li>
                <li><a href="<?php echo base_url('sales_register/tally_ledger'); ?>"><i class="fa fa-circle-o"></i> Tally Ledger </a></li>
                <li><a href="<?php echo base_url('sales_register/party_tally_ledger'); ?>"><i class="fa fa-circle-o"></i> Party Tally Ledger </a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i> <span>Sales Master Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <!-- <li><a href="<?php echo base_url('brand_type'); ?>"><i class="fa fa-circle-o"></i> Brand Type </a></li> -->
                <li><a href="<?php echo base_url('brand'); ?>"><i class="fa fa-circle-o"></i> Brand </a></li>
                <li><a href="<?php echo base_url('party'); ?>"><i class="fa fa-circle-o"></i> Agent </a></li>
                <li><a href="<?php echo base_url('party_name'); ?>"><i class="fa fa-circle-o"></i> Party </a></li>
                <li><a href="<?php echo base_url('bank'); ?>"><i class="fa fa-circle-o"></i> Bank </a></li>
              </ul>
            </li>
            <?php if($this->session->userdata('department_id') == 33){?>
            <li><a href="<?php echo site_url('lab_report')?>"><i class="fa fa-flask"></i>Lab Report</a></li>
            <?php }?>
            <li><a href="<?php echo site_url('silo_report')?>"><i class="fa fa-industry"></i>Silo</a></li>
          </ul>



        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $header?>
          </h1>
            
      <?php }?>