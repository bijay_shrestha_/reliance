<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button class="btn btn-primary" id="add-btn">Add</button>
                    <table id="brand_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time Slot</th>
                                <th>Residue</th>
                                <th>SP Surface M2/Kg</th>
                                <th>W.C.MI</th>
                                <th>CaO</th>
                                <th>IST</th>
                                <th>FST</th>
                                <th>IR</th>
                                <th>1 Day</th>
                                <th>Explanation</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $value) {?>
                            <tr>
                                <td><?php echo $value['date_np']?></td>
                                <td><?php echo $value['time']?></td>
                                <td><?php echo number_format($value['residue'],2)?></td>
                                <td><?php echo number_format($value['sp_surface'],2)?></td>
                                <td><?php echo number_format($value['wcmi'],2)?></td>
                                <td><?php echo number_format($value['cao'],2)?></td>
                                <td><?php echo number_format($value['ist'],2)?></td>
                                <td><?php echo number_format($value['fst'],2)?></td>
                                <td><?php echo number_format($value['ir'],2)?></td>
                                <td><?php echo number_format($value['day_1'],2)?></td>
                                <td><?php echo $value['explanation']?></td>
                                <td>
                                    <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger" onclick="delete_data(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="hourly-report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="Date">
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                                <div id="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Time</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='time' id="time">
                                    <option value="">-- select time --</option>
                                <?php for($i = 0; $i < 24; $i++) {?>
                                <?php 
                                    $k = ($i < 10)?"0$i":"$i";
                                    $j = ($i < 9)?"0".($i+1):$i+1
                                ?>
                                    <option value="<?php echo $k.'-'.$j?>"><?php echo $k.'-'.$j?></option>
                                <?php }?>
                                </select>
                                <div id="error_time" hidden style="color: red" class="error-div">Time is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Residue</label>
                            <div class="col-sm-8">
                                <input type="number" name="residue" id="residue" class="form-control" placeholder="Residue">
                                <div id="error_brand_id" hidden style="color: red" class="error-div">Residue is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">SP Surface M2/Kg</label>
                            <div class="col-sm-8">
                                <input type="number" name="sp_surface" id="sp_surface" class="form-control" placeholder="SP Surface M2/Kg">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">W.C.MI</label>
                            <div class="col-sm-8">
                                <input type="number" name="wcmi" id="wcmi" class="form-control" placeholder="W.C.MI">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">CaO</label>
                            <div class="col-sm-8">
                                <input type="number" name="cao" id="cao" class="form-control" placeholder="CaO">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">IST</label>
                            <div class="col-sm-8">
                                <input type="number" name="ist" id="ist" class="form-control" placeholder="IST">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">FST</label>
                            <div class="col-sm-8">
                                <input type="number" name="fst" id="fst" class="form-control" placeholder="FST">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">IR</label>
                            <div class="col-sm-8">
                                <input type="number" name="ir" id="ir" class="form-control" placeholder="IR">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">1 Day</label>
                            <div class="col-sm-8">
                                <input type="number" name="day_1" id="day_1" class="form-control" placeholder="1 Day">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Explanation</label>
                            <div class="col-sm-8">
                                <textarea name="explanation" id="explanation" class="form-control" placeholder="Explanation"></textarea>
                            </div>
                        </div>

                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });

        // nepali date picker
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('input').val('');
        $('.error-div').hide();
        $('option').attr('selected', false);
        $('#myModalLabel').html('Add Hourly Report');
        $('#hourly-report-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var date_np = $('#date_np').val();
        var time = $('#time').val();
        var i = 0;

        if(date_np == ''){
            i = 1;
            $('#error_date_np').show();
        }else{
            $('#error_date_np').hide();
        }

        if(time == ''){
            i = 1;
            $('#error_time').show();
        }else{
            $('#error_time').hide();
        }
        if(i == 0){
            $.post('<?php echo site_url("hourly_report/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.error-div').hide();
        $.post('<?php echo site_url("hourly_report/get_value")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });
            $('#myModalLabel').html('Edit Hourly Report');
            $('#hourly-report-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_data(id){
        $.post('<?php echo site_url("hourly_report/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>