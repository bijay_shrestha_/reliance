<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
	<!-- for total report -->
	<div class='row'>
		<?php /*<div class='col-md-12'>
			<a href="<?php echo site_url("calander_shift_report/pdf?date=".$date)?>" class="dt-button buttons-excel buttons-html5" id="pdf-btn">PDF</a>
		</div>*/?>
		<div class='col-md-4'>
			<div class="box">
			    <div class="box-body">
			    	<table id="example" class="table table-bordered table-striped">
			            <thead>
			                <tr>
			                    <th colspan="2">Material Detail</th>
			                </tr>
			            </thead>
			            <tbody>
		                    <tr>
		                        <td>Raw Materials</td>
		                        <td><?php echo $total_raw_material?></td>
		                    </tr>
		                    <tr>
		                        <td>Bought Scrap</td>
		                        <td><?php echo $total_bought_scrap?></td>
		                    </tr>
		                    <tr>
		                        <td>Factory Scrap</td>
		                        <td><?php echo $total_factory_scrap?></td>
		                    </tr>
		                    <tr>
		                        <td>Total</td>
		                        <td><?php echo $total_raw_material+$total_bought_scrap +$total_factory_scrap?></td>
		                    </tr>
			            </tbody>
			        </table>
		        </div>
		    </div>
	    </div>
	    <div class='col-md-4'>
			<div class="box">
			    <div class="box-body">
			    	<table id="example" class="table table-bordered table-striped">
			            <thead>
			                <tr>
			                    <th colspan="2">Scrap Detail</th>
			                </tr>
			            </thead>
			            <tbody>
		                    <tr>
		                        <td>Reusable</td>
		                        <td><?php echo $total_reusable_scrap?></td>
		                    </tr>
		                    <tr>
		                        <td>Lamps and Plates</td>
		                        <td><?php echo $total_lumps_plates_scrap?></td>
		                    </tr>
		                    <tr>
		                        <td>Total</td>
		                        <td><?php echo $total_reusable_scrap+$total_lumps_plates_scrap?></td>
		                    </tr>
			            </tbody>
			        </table>
		        </div>
		    </div>
	    </div>
	    <div class='col-md-4'>
			<div class="box">
			    <div class="box-body">
			    	<a href='<?php echo site_url()?>calander_shift_report/add'><?php echo $new_item?> New item is added by Mixing Section</a>
			    </div>
		    </div>
	    </div>
    </div>
    <!-- end total report -->
    <!-- Small boxes (Stat box) -->
    <div class="box">
	    <div class="box-body">
	    <?php if($page == 'list'){?>
	    	<a href='<?php echo site_url("calander_shift_report/add")?>' class='btn btn-primary margin'>Add</a>
    	<?php }?>
    	<br>
    	<form>
    		<input type="text" name='date' id="date" value="<?php echo $this->input->get('date')?>" placeholder="Select Date">
    		<button class='btn btn-primary'><i class="fa fa-search"></i></button>
    		<button type='button' id="pdf-btn" class="pull-right margin dt-button buttons-excel buttons-html5">PDF</button>
    	</form>
                <br>
            <!-- for date -->
	    	<table id="example2" class="table table-bordered table-striped">
	            <thead>
	                <tr>
	                    <th>Date</th>
	                    <th>Shift</th>
	                    <th>Quality</th>
	                    <th>Brand</th>
	                    <th>color</th>
	                    <th>dimension</th>
	                    <th>Mixing Total</th>
	                    <th></th>
	                    <th>Length</th>
	                    <th>
	                    <th>NTWT</th>
	                    <th>
	                    <th>Action</th>
	                </tr>
	            </thead>
	            <tfoot>
		            <tr>
		                <th colspan="6" style="text-align:right">Total:</th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
	            <tbody>
	                <?php foreach ($consumptions as $consumption) { ?>
	                    <tr>
	                        <td><?php echo $consumption['nepali_date'] ?></td>
	                        <td><?php echo $consumption['shift'] ?></td>
	                        <td><?php echo $consumption['brand']['brand_name']?></td>
	                        <td><?php echo $consumption['quality']['quality_name'] ?></td>
	                        <td><?php echo $consumption['color']['color_name']?></td>
	                        <td><?php echo $consumption['dimension']['dimension_name']?></td>
	                        <td><?php echo number_format($consumption['item_amount'],3)?></td>
	                        <td><?php echo $consumption['item_amount']?></td>
                        	<td><?php echo number_format($consumption['length'],3)?></td>
                        	<td><?php echo $consumption['length']?></td>
                        	<td><?php echo number_format($consumption['net_wt'],3)?></td>
                        	<td><?php echo $consumption['net_wt']?></td>
	                        <td>
	                        	<?php if($page != 'list'){?>
	                        		<button type="button" class="btn btn-warning add_report" id="consumption-<?php echo $consumption['group'] ?>" onclick="reportForm(<?php echo $consumption['group'] ?>)" value="<?php echo $consumption['group'] ?>"> Add</button>
	                        	<?php }else{?>
	                        		<button type="button" class="btn btn-warning add_report" id="consumption-<?php echo $consumption['group'] ?>" onclick="reportForm(<?php echo $consumption['group'] ?>)" value="<?php echo $consumption['group'] ?>"> Edit</button>
	                        	<?php }?>
                        	</td>
	                    </tr>
	                <?php } ?>
	            </tbody>
	        </table>
        </div>
    </div>
</section>

<!-- modal to add shift report -->
<div class="modal fade" id="report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="report-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Date*</label>
                            <div class="col-sm-8">
                                <input type="text" name="date-np" class="form-control" id="date-np" placeholder="Date" disabled>
                                <input type="hidden" name="date-en" class="form-control" id="date-en">
                                <span id="error-dealer-name" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shift" class="col-sm-4 control-label">Shift</label>
                            <div class="col-sm-8">
                                <input type="text" name="shift" class="form-control" id="shift" placeholder="Shift" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quality" class="col-sm-4 control-label">Quality*</label>
                            <div class="col-sm-8">
                                <input type="text" name="quality" class="form-control" id="quality" placeholder="Quality" disabled>
                                <span id="error-contact" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="color" class="col-sm-4 control-label">Color*</label>
                            <div class="col-sm-8">
                                <input type="text" name="color" class="form-control" id="color" placeholder="Color" disabled>
                                <span id="error-mobile" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dimension" class="col-sm-4 control-label">Dimension</label>
                            <div class="col-sm-8">
                                <input type="text" name="dimension" class="form-control" id="dimension" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="length" class="col-sm-4 control-label">Length</label>
                            <div class="col-sm-8">
                                <input type="number" name="length" class="form-control" id="length" placeholder="Length">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nt_wt" class="col-sm-4 control-label">NTWT</label>
                            <div class="col-sm-8">
                                <input type="number" name="net_wt" class="form-control" id="nt-wt" placeholder="NTWT" onchange='checkValue()'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mixing_total" class="col-sm-4 control-label">Mixing Total</label>
                            <div class="col-sm-8">
                                <input type="text" name="mixing_total" class="form-control" id="mixing-total" placeholder="Mixing Total" disabled>
                            </div>
                        </div>

                        <input type="hidden" name="group" id="group" value="">
                        <input type="hidden" name="material_properyt_id" id="material-properyt-id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$('#example2').DataTable({
		"order":[[0, "desc"]],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;

	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };

                // Total over all pages
                total = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                total_length = api
                    .column( 9 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                total_ntwt = api
                    .column( 11 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Total over this page
                pageTotal = api
                    .column( 7, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                pageTotal_length = api
                    .column( 9, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                pageTotal_ntwt = api
                    .column( 11, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 6 ).footer() ).html(
                    pageTotal.toFixed(3) + ' (' + total.toFixed(3) + ')'
                );
                $( api.column( 8 ).footer() ).html(
                    pageTotal_length.toFixed(3) + ' (' + total_length.toFixed(3) + ')'
                );
                $( api.column( 10 ).footer() ).html(
                    pageTotal_ntwt.toFixed(3) + ' (' + total_ntwt.toFixed(3) + ')'
                );
		},
        "columnDefs": [
            {
                "targets": [ 7,9,11 ],
                "visible": false,
                "searchable": false
            }
        ],
	});
});
</script>
<script type="text/javascript">
	function reportForm(group){
		<?php if($page != 'list'){?>
			$.post('<?php echo site_url("calander_shift_report/getconsumption")?>',{group:group},function(data){
				$('#date-np').val(data.nepali_date);
				$('#date-en').val(data.english_date);
				$('#shift').val(data.shift);
				$('#quality').val(data.quality.quality_name);
				$('#color').val(data.color.color_name);
				$('#dimension').val(data.dimension.dimension_name);
				$('#mixing-total').val(data.consumption_amount);
				$('#group').val(data.group);
				$('#material-properyt-id').val(data.material_property_id);
				$('#report-dialog').modal();

			},'json')
		<?php }else{?>
			$.post('<?php echo site_url("calander_shift_report/getlistconsumption")?>',{group:group},function(data){
				$('#date-np').val(data.nepali_date);
				$('#date-en').val(data.english_date);
				$('#shift').val(data.shift);
				$('#quality').val(data.quality.quality_name);
				$('#color').val(data.color.color_name);
				$('#dimension').val(data.dimension.dimension_name);
				$('#mixing-total').val(data.consumption_amount);
				$('#group').val(data.group);
				$('#length').val(data.length);
				$('#nt-wt').val(data.net_wt);
				$('#material-properyt-id').val(data.material_property_id);
				$('#report-dialog').modal();

			},'json')
		<?php }?>
	}
</script>
<!-- saving report -->
<script type="text/javascript">
	$('#form-submit').click(function(){
		if($('#length').val() == ''){
			alert('Length is required.');
		}else if( $('#length').val() == ''){
			alert('Net weight is required');
		}else{
			$.post('<?php echo site_url("calander_shift_report/save")?>',$('#report-form').serializeArray(),function(data){
				if(data.success){
					<?php if($page == 'list'){
						echo 'alert("Report edited");';
					}else{
						echo "alert('Report added');";
					 }?>
					location.reload();
				}else{
					alert('Error occur');
				}
			},'json');
		}
	});
</script>
<script type="text/javascript">
	function checkValue(){
		mixing_total = parseFloat($('#mixing-total').val().replace(",", ""));
		nt_wt = parseFloat($('#nt-wt').val());
		if(mixing_total<nt_wt){
			alert('Net weight is greater than mixing total');
			$('#nt-wt').val('');
		}
	}
</script>
<script type="text/javascript">
	$(function () {
        $('#date').nepaliDatePicker();
    });
</script>
<script type="text/javascript">
    $('#pdf-btn').click(function(){
        var date = $('#date').val();
        window.location.href = "<?php echo site_url("home/pdf")?>"+'/2/'+date;
    })
</script>