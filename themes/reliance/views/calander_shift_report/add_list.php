<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box">
	    <div class="box-body">
	    <form id='all-report-submit'>
	    	<table id="example2" class="table table-bordered table-striped">
	            <thead>
	                <tr>
	                    <th>Date</th>
	                    <th>Shift</th>
	                    <th>Quality</th>
	                    <th>Color</th>
	                    <th>Dimension</th>
	                    <th>Mixing Total</th>
	                    <th>Length</th>
	                    <th>NTWT</th>
	                    <!-- <th>Action</th> -->
	                </tr>
	            </thead>
	            <tbody>
	            	<?php $i="0"?>
	                <?php foreach ($consumptions as $consumption) { ?>
	                    <tr>
	                        <td><?php echo $consumption['nepali_date'] ?></td>
	                        <td><?php echo $consumption['shift'] ?></td>
	                        <td><?php echo $consumption['quality']['quality_name'] ?></td>
	                        <td><?php echo $consumption['color']['color_name']?></td>
	                        <td><?php echo $consumption['dimension']['dimension_name']?></td>
	                        <td><?php echo number_format($consumption['item_amount'],3)?>
	                        	<input type="hidden" id="mixing-total-<?php echo $i?>" name="mixing_total[<?php echo $i?>]" value="<?php echo $consumption['item_amount']?>">
	                        	<input type="hidden" name="group[<?php echo $i?>]" value="<?php echo $consumption['group']?>">
	                        	<input type="hidden" name="date-np[<?php echo $i?>]" value="<?php echo $consumption['nepali_date']?>">
	                        	<input type="hidden" name="date-en[<?php echo $i?>]" value="<?php echo $consumption['english_date']?>">
	                        	<input type="hidden" name="shift[<?php echo $i?>]" value="<?php echo $consumption['shift']?>">
	                        	<input type="hidden" name="material_properyt_id[<?php echo $i?>]" value="<?php echo $consumption['material_property_id']?>">
	                        	
	                        </td>
	                        <td><input type="number" name='length[<?php echo $i?>]'>
	                        <?php //echo $consumption['length']?>
	                        </td>
	                        <td><input type="number" onkeyup="checkValue(<?php echo $i?>)" onblur="checkValue(<?php echo $i?>)" name='net_wt[<?php echo $i?>]' id="nt-wt-<?php echo $i?>">

	                        <?php //echo $consumption['net_wt']?>
	                        </td>
	                        <!--<td>
	                        	<?php /*if($page != 'list'){?>
	                        		<button type="button" class="btn btn-warning add_report" id="consumption-<?php echo $consumption['group'] ?>" onclick="reportForm(<?php echo $consumption['group'] ?>)" value="<?php echo $consumption['group'] ?>"> Add</button>
	                        	<?php }else{?>
	                        		<button type="button" class="btn btn-warning add_report" id="consumption-<?php echo $consumption['group'] ?>" onclick="reportForm(<?php echo $consumption['group'] ?>)" value="<?php echo $consumption['group'] ?>"> Edit</button>
	                        	<?php }*/?>
                        	</td>-->
	                    </tr>
	                <?php 
	                $i++;
	                } ?>
	            </tbody>
	        </table>
	        <button class='btn btn-primary' id="submit" type="button">Submit</button>
	        </form>
        </div>
    </div>
</section>

<!-- modal to add shift report -->
<div class="modal fade" id="report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="report-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Date*</label>
                            <div class="col-sm-8">
                                <input type="text" name="date-np" class="form-control" id="date-np" placeholder="Date" disabled>
                                <input type="hidden" name="date-en" class="form-control" id="date-en">
                                <span id="error-dealer-name" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shift" class="col-sm-4 control-label">Shift</label>
                            <div class="col-sm-8">
                                <input type="text" name="shift" class="form-control" id="shift" placeholder="Shift" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quality" class="col-sm-4 control-label">Quality*</label>
                            <div class="col-sm-8">
                                <input type="text" name="quality" class="form-control" id="quality" placeholder="Quality" disabled>
                                <span id="error-contact" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="color" class="col-sm-4 control-label">Color*</label>
                            <div class="col-sm-8">
                                <input type="text" name="color" class="form-control" id="color" placeholder="Color" disabled>
                                <span id="error-mobile" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dimension" class="col-sm-4 control-label">Dimension</label>
                            <div class="col-sm-8">
                                <input type="text" name="dimension" class="form-control" id="dimension" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="length" class="col-sm-4 control-label">Length</label>
                            <div class="col-sm-8">
                                <input type="number" name="length" class="form-control" id="length" placeholder="Length">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nt_wt" class="col-sm-4 control-label">NTWT</label>
                            <div class="col-sm-8">
                                <input type="number" name="net_wt" class="form-control" id="nt-wt" placeholder="NTWT">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mixing_total" class="col-sm-4 control-label">Mixing Total</label>
                            <div class="col-sm-8">
                                <input type="text" name="mixing_total" class="form-control" id="mixing-total" placeholder="Mixing Total" disabled>
                            </div>
                        </div>

                        <input type="hidden" name="group" id="group" value="">
                        <input type="hidden" name="material_properyt_id" id="material-properyt-id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$('#example2').DataTable({
		"order":[[0, "desc"]],
		paging: false
	});
});
</script>
<script type="text/javascript">
	function reportForm(group){
		<?php if($page != 'list'){?>
			$.post('<?php echo site_url("calander_shift_report/getconsumption")?>',{group:group},function(data){
				$('#date-np').val(data.nepali_date);
				$('#date-en').val(data.english_date);
				$('#shift').val(data.shift);
				$('#quality').val(data.quality.quality_name);
				$('#color').val(data.color.color_name);
				$('#dimension').val(data.dimension.dimension_name);
				$('#mixing-total').val(data.consumption_amount);
				$('#group').val(data.group);
				$('#material-properyt-id').val(data.material_property_id);
				$('#report-dialog').modal();

			},'json')
		<?php }else{?>
			$.post('<?php echo site_url("calander_shift_report/getlistconsumption")?>',{group:group},function(data){
				$('#date-np').val(data.nepali_date);
				$('#date-en').val(data.english_date);
				$('#shift').val(data.shift);
				$('#quality').val(data.quality.quality_name);
				$('#color').val(data.color.color_name);
				$('#dimension').val(data.dimension.dimension_name);
				$('#mixing-total').val(data.consumption_amount);
				$('#group').val(data.group);
				$('#length').val(data.length);
				$('#nt-wt').val(data.net_wt);
				$('#material-properyt-id').val(data.material_property_id);
				$('#report-dialog').modal();

			},'json')
		<?php }?>
	}
</script>
<!-- saving report -->
<script type="text/javascript">
	$('#form-submit').click(function(){
		if($('#length').val() == ''){
			alert('Length is required.');
		}else if( $('#length').val() == ''){
			alert('Net weight is required');
		}else{
			$.post('<?php echo site_url("calander_shift_report/save")?>',$('#report-form').serializeArray(),function(data){
				if(data.success){
					<?php if($page == 'list'){
						echo 'alert("Report edited");';
					}else{
						echo "alert('Report added');";
					 }?>
					location.reload();
				}else{
					alert('Error occur');
				}
			},'json');
		}
	});
</script>
<!-- saving all report -->
<script type="text/javascript">
	$('#submit').click(function(){
		// if($('#length').val() == ''){
		// 	alert('Length is required.');
		// }else if( $('#length').val() == ''){
		// 	alert('Net weight is required');
		// }else{
			$.post('<?php echo site_url("calander_shift_report/saveAll")?>',$('#all-report-submit').serializeArray(),function(data){
				if(data.success){
					<?php if($page == 'list'){
						echo 'alert("Report edited");';
					}else{
						echo "alert('Report added');";
					 }?>
					location.reload();
				}else{
					alert('Error occur');
				}
			},'json');
		// }
	});
</script>
<script type="text/javascript">
	function checkValue(i){
		// alert('here');
		mixing_total = parseFloat($('#mixing-total-'+i).val().replace(",", ""));
		nt_wt = parseFloat($('#nt-wt-'+i).val())
		if(mixing_total < nt_wt){
			alert('Net weight is greater than mixing total');
			$('#nt-wt-'+i).val('');
		}
	}
</script>