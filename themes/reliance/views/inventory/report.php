<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">

<form action='<?php echo site_url("inventory/report")?>' method='post'>
	<div class='row'>
        <div class='col-md-6'>
            <label>Date :</label><input type='text' name='date' id='date' class="form-control" value="<?php echo $this->input->post('date')?>">
        </div>
        <div class='col-md-6'>
    	   <label>Report :</label><input type='hidden' name='en-date' id='en-date' value="<?php echo $this->input->post('en-date')?>" plceholder="Date">
    <select name='type' id='type' name='type'  class="form-control">
        <option value="">Show All</option>
        <?php 
            foreach($types as $type){?>
                <option value="<?php echo $type['rt_id']?>" <?php echo ($this->input->post('type') == $type['rt_id'])?'selected':''?>>
                    <?php echo $type['type'];?>
                </option>
        <?php }
        ?>
    </select>
        </div>
    </div>
	<button class='btn btn-primary margin'>Submit</button>
</form>

 <table id="example3" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Category</th>
            <th></th>
          <th>Material Name</th>
          <th>Opening</th>
          <th>Purchase</th>
          <th>Dispatch</th>
          <th>Closing</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materials as $material){?>
        <tr>
            <td><?php echo $material['category_name']?></td>
            <td><?php echo (array_key_exists('report_order', $material)?$material['report_order']:'10000000')?></td>
            <td><?php echo $material['material_name']?></td>
            <td><?php echo number_format(((array_key_exists($material['material_id'], $opening_stocks))?$opening_stocks[$material['material_id']]:'0'),3)?></td>
            <td><?php 
            $total = 0;
            if(array_key_exists($material['material_id'], $today_purchase)){
            	// foreach($purchase[$material['material_id']] as $buy){
            		$total += $today_purchase[$material['material_id']];
            	// }
            	echo number_format($total,3);
            }else{
            	echo '0';
            }?></td>
            <?php /*<td><?php echo (array_key_exists($material['material_id'], $dispatches))?(($dispatches[$material['material_id']]['all_dispatched']!='')?$dispatches[$material['material_id']]['all_dispatched']:'0'):'0'?></td>*/?>
            <td><?php 
            $total_dispatch = 0;
            	if(array_key_exists($material['material_id'], $today_dispatches)){
            		foreach($today_dispatches[$material['material_id']] as $out_going){
            			$total_dispatch += $out_going['accepted_quantity'];
            		}
            		echo number_format($total_dispatch,3);
        		}else{
        			echo '0';
        		}?>
    		</td>
            <td>
            	<?php
            	$result = 0;
            	if(array_key_exists($material['material_id'], $opening_stocks)){
            		$result += $opening_stocks[$material['material_id']];
            	}
            	if(array_key_exists($material['material_id'], $today_dispatches)){
            		$result -= $total_dispatch;
            	}
            	$result += $total;
            	echo number_format($result,3);
            	?>
            </td>
        </tr>
        <?php }?>
    </tbody>
</table>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

        <input type='hidden' value="<?php echo date('Y-m-d')?>" id="nepaliDate9">
<script type="text/javascript">
    $(function(){
        <?php if($this->input->post('date')){?>
            var date = '<?php echo $this->input->post('date')?>';
            console.log(date);
        <?php }else{?>
            $('#nepaliDate9').val(AD2BS($('#nepaliDate9').val()));
            var date = $('#nepaliDate9').val();
        <?php }?>

    	table = $('#example3').DataTable({
            "order": [[ 1, 'asc' ]],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0,2,3,4,5,6 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [0,2,3,4,5,6]
                    },
                    customize: function ( doc ) {
                        doc.content.splice( 0, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            image: '<?php echo $this->pdf_logo?>',/*64 bit encoded image in public controller*/
                        })
                        doc.content.splice( 2, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Date : '+date
                        })
                        doc.content.splice( 3, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Report Type : <?php echo $report["type"]?>'
                        })
                    }
                },
            ],
            "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                }],
        });
    });
</script>
<script type="text/javascript">
	$(function () {
		$('#date').nepaliDatePicker({
            ndpEnglishInput: 'en-date'
        });
	});
</script>