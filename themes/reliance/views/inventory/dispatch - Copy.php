<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php /*<li ><a href="#tab_1" data-toggle="tab">Dispatch</a></li>*/?>
                <li class="active"><a href="#tab_2" data-toggle="tab">Pending</a></li>
                <li><a href="#tab_3" data-toggle="tab">Accepted</a></li>
                <li><a href="#tab_4" data-toggle="tab">Rejected</a></li>
            </ul>
            <div class="tab-content">
                <?php /*<div class="tab-pane" id="tab_1">
                <form id="dispatch-form" class="form-horizontal">
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Material*</label>
                        <div class="col-sm-8">
                            <select name="material_id" id="material" class="form-control">
                                <option value="">-- select Material --</option>
                                <?php foreach($materials as $material){?>
                                <option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
                                <?php }?>
                            </select>
                            <span class="error_message" id="error-material" style="color: red" hidden="true">Material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Request From</label>
                        <div class="col-sm-8">
                            <select name="request_id" id='purchase_request' class="form-control">
                                
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Request is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="requested_amount" class="col-sm-4 control-label">Requested Amount</label>
                        <div class="col-sm-8">
                            <input type="text" readonly id="requested-amount" name="requested_amount" class="form-control">
                            <!-- <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="current_stock" class="col-sm-4 control-label">Current Stock</label>
                        <div class="col-sm-8">
                            <input type="text" readonly id="current_stock" name="current_stock" class="form-control">
                            <!-- <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label">Nepali Date</label>
                        <div class="col-sm-8">
                            <input type="text" id="nepali-date" name="date_np" class="form-control">
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Date is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Added_stock" class="col-sm-4 control-label">Accepted Quantity</label>
                        <div class="col-sm-8">
                            <input type="text" id="accepted-qty" min=0 name="accepted_qty" id="added_qty" class="form-control">
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Accepted quantity is required</span>
                        </div>
                    </div>
                    <button type='button' class="btn btn-primary" id="submit">Save</button>
                </form>
                </div>*/?>
                <div class="tab-pane active" id="tab_2">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Material Name</th>
                                <th>Depart Name</th>
                                <th>Requested Date</th>
                                <th>Accepted Date</th>
                                <th>Current Available Stock</th>
                                <th>Requested Stock</th>
                                <th>Accepted Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // $count = 0;
                            foreach ($histories as $history){?>
                            <tr>
                                <td>
                                <?php echo $history['material_name']?>
                                </td>
                                    <td><input type='hidden' id="requested_id-<?php echo $history['request_id']?>" value='<?php echo $history['request_id']?>'>
                                    <?php echo $history['name']?>
                                </td>
                                <td>
                                    <?php echo $history['request_np_date']?></td>
                                <td>
                                    <input type='text' class="date-approve" id="confirm-date-<?php echo $history['request_id']?>" data-request_id="<?php echo $history['request_id']?>" value='<?php echo $history['store_confirm_np_date']?>'>
                                    <?php //echo $history['store_confirm_np_date']?>
                                </td>
                                <td>
                                    <input type='hidden' id="current-stock-<?php echo $history['request_id']?>" value='<?php echo $history['current_stock']?>'>
                                    <?php echo number_format($history['current_stock'])?>
                                </td>
                                <td>
                                    <input type='hidden' id="requested_stock-<?php echo $history['request_id']?>" value='<?php echo $history['request_quantity']?>'>
                                    <?php echo number_format($history['request_quantity'])?>
                                </td>
                                <td>
                                    <input type='text' id="accept-stock-<?php echo $history['request_id']?>" onkeyup="checkQty(<?php echo $history['request_id']?>)" value='<?php echo $history['accepted_quantity']?>'>
                                </td>
                                <td><button class="btn btn-primary" onclick=editPending(<?php echo $history['request_id']?>)>Confirm</button></td>
                            </tr>
                            <?php //$count++; 
                            }?>
                        </tbody>
                      </table>
                </div>
                <div class="tab-pane" id="tab_3">
                     <table id="example" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Material Name</th>
                                <th>Depart Name</th>
                                <th>Requested Date</th>
                                <th>Accepted Date</th>
                                <th>Current Available Stock</th>
                                <th>Requested Stock</th>
                                <th>Supplied Stock</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tab-pane" id="tab_4">
                     <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Material Name</th>
                                <th>Depart Name</th>
                                <th>Requested Date</th>
                                <th>Accepted Date</th>
                                <th>Current Available Stock</th>
                                <th>Requested Stock</th>
                                <th>Supplied Stock</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
        var table = $('#example2').DataTable({
            "order" : [[0, 'desc']]
        });

        var table1 = $('#example').DataTable( {
            "ajax": "<?php echo site_url()?>inventory/acceptedDispatch",
            "columns": [
                { "data": "material_name" },
                { "data": "name" },
                { "data": "request_np_date" },
                { "data": "store_confirm_np_date" },
                { "data": "current_stock" },
                { "data": "request_quantity" },
                { "data": "accepted_quantity" }
            ],
            "order" : [[0, 'desc']]
        } );
        var table1 = $('#example1').DataTable( {
            "ajax": "<?php echo site_url()?>inventory/rejectedDispatch",
            "columns": [
                { "data": "material_name" },
                { "data": "name" },
                { "data": "request_np_date" },
                { "data": "store_confirm_np_date" },
                { "data": "current_stock" },
                { "data": "request_quantity" },
                { "data": "accepted_quantity" }
            ],
            "order" : [[0, 'desc']]
        } );

    // var history_count = '<?php //echo $count?>' ;
    $(function () {
        // for(i=0; i<history_count; i++){
        //     console.log('i > '+i);
        //     $('#confirm-date-'+i).nepaliDatePicker();    
        // }
        $('.confirm-date').nepaliDatePicker();
    });
</script>

<script type="text/javascript">
    $("#material").change(function(){
        $("#purchase_request").empty();
        $("#requested-amount").val('');
        var id = $(this).val();
        $.post('<?php echo site_url("inventory/departmentRequest")?>',{id:id},function(data){
            $("#purchase_request").append
                ($('<option>',{
                    value:'',
                    text:'-- select request --'
                }));
            $.each(data.request,function(index,request){
                $("#purchase_request").append
                ($('<option>',{
                    value:request.request_id,
                    text:'Request from '+request.name
                }));
            });
             $('#added_qty').val('');
            $('#current_stock').val(data.current_stock.current_stock)
        },'json')
    });
</script>
<script type="text/javascript">
    $("#purchase_request").change(function(){
        var id = $(this).val();
        var material_id = $('#material_id').val();
        $.post('<?php echo site_url("inventory/getRequestedQty")?>',{id:id,material_id:material_id},function(data){
            $('#requested-amount').val(data.request_quantity);
            
        },'json')
    });
</script>
<script type="text/javascript">
    $("#accepted-qty").keyup(function(){
        $request_amount = Number($("#requested-amount").val());
        accepted_qty = Number($(this).val());
        console.log(accepted_qty+"<="+$request_amount);
        if($(this).val() < 0 || accepted_qty > $request_amount){
            alert('Accepted quantity is not valid');
            $('#submit').prop('disabled',true);
        }else{
            $('#submit').prop('disabled',false);
        }
    })
</script>
<?php /*saving dispatch*/?>
<script type="text/javascript">
    $("#submit").click(function(){
        $.post('<?php echo site_url("inventory/saveDispatch")?>',$("#dispatch-form").serializeArray(),function(data){
            if(data.success){
                // myDataRow =;
                // table.row.add( [data.history.material_name,
                //                 data.history.name,
                //                 data.history.request_np_date,
                //                 data.history.store_confirm_np_date,
                //                 data.history.current_stock,
                //                 data.history.request_quantity,
                //                 data.history.accepted_quantity,
                //                 "<button class='btn btn-primary'>Confirm</button>"]).draw( false );

    table.row.add( [data.history.material_name,
                                data.history.name,
                                data.history.request_np_date,
                                data.history.store_confirm_np_date,
                                data.history.current_stock,
                                data.history.request_quantity,
                                data.history.accepted_quantity,
                                "<button class='btn btn-primary'>Confirm</button>"]).draw( false );

                alert('Material Dispatched');
                $('#material_id').val('');
                $('#purchase_request').empty();
                $("#purchase_request option[value="+$('#purchase_request').val()+"]").remove();
                $('#requested-amount').val('');
                $('#current_stock').val('');
                $('#nepali-date').val('');
                $('#accepted-qty').val('');
                $('.error_message').hide();
            }else{
                $('.error_message').show();
            }
        },'json')
    });
</script>
<script type="text/javascript">
    function checkQty(id){
        request_qty = parseInt($('#requested_stock-'+id).val());
        accepte_qty = parseInt($('#accept-stock-'+id).val());
        if(request_qty < accepte_qty){
            alert('Invalid Input');
            $('#accept-stock-'+id).val("");
        }
    }
</script>
<script type="text/javascript">
    function editPending(id){
        //request_id = $('#requested_id-'+id).val();
        confirm_date = $('#confirm-date-'+id).val();
        alert('history_id >> '+$('#confirm-date-'+id).data('request_id'));

        // current_stock = $('#current-stock-'+id).val();
        // accept_qty = $('#accept-stock-'+id).val();
        // $.post('<?php echo site_url("inventory/saveDispatch")?>',{request_id:request_id, date_np:confirm_date, accepted_qty:accept_qty, current_stock:current_stock},function(data){
        //         if(data.success){
        //             alert('Item dispatched');
        //         }
        // },'json')
    }
</script>