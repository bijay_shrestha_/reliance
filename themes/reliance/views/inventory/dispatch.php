<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php /*<li ><a href="#tab_1" data-toggle="tab">Dispatch</a></li>*/?>
                <!-- <li class="active"><a href="#tab_2" data-toggle="tab">Material Dispatch <span class="label label-danger"><?php echo $this->get_dispatch_for_header?></span></a></li> -->
                <li class="active"><a href="#tab_6" data-toggle="tab">Dispatch</a></li>
                <!-- <li><a href="#tab_5" data-toggle="tab">Pending <span class="label label-warning"><?php echo $pending?></span></a></li> -->
                <!-- <li><a href="#tab_3" data-toggle="tab">Accepted</a></li> -->
                <!-- <li><a href="#tab_4" data-toggle="tab">Rejected</a></li> -->
                <li><a href="#tab_7" data-toggle="tab">Dispatch Record</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                <form id="dispatch-form" class="form-horizontal">
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Material*</label>
                        <div class="col-sm-8">
                            <select name="material_id" id="material" class="form-control">
                                <option value="">-- select Material --</option>
                                <?php foreach($materials as $material){?>
                                <option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
                                <?php }?>
                            </select>
                            <span class="error_message" id="error-material" style="color: red" hidden="true">Material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Request From</label>
                        <div class="col-sm-8">
                            <select name="request_id" id='purchase_request' class="form-control">
                                
                            </select>
                            <span class="error_message" id="error-request" style="color: red" hidden="true">Request is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="requested_amount" class="col-sm-4 control-label">Requested Amount</label>
                        <div class="col-sm-8">
                            <input type="text" readonly id="requested-amount" name="requested_amount" class="form-control">
                            <!-- <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="current_stock" class="col-sm-4 control-label">Current Stock</label>
                        <div class="col-sm-8">
                            <input type="text" readonly id="current_stock" name="current_stock" class="form-control">
                            <!-- <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label">Nepali Date</label>
                        <div class="col-sm-8">
                            <input type="text" id="nepali-date" name="date_np" class="form-control">
                            <span class="error_message" id="error-date" style="color: red" hidden="true">Date is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Added_stock" class="col-sm-4 control-label">Accepted Quantity</label>
                        <div class="col-sm-8">
                            <input type="text" id="accepted-qty" min=0 name="accepted_qty" id="added_qty" class="form-control">
                            <span class="error_message" id="error-qty" style="color: red" hidden="true">Accepted quantity is required</span>
                        </div>
                    </div>
                    <button type='button' class="btn btn-primary" id="submit">Save</button>
                </form>
                </div>
                <!--##########################################################################-->
                <!-- for direct dispatch -->
                <div class="tab-pane active" id="tab_6">
                <?php $i = 0?>
                <form id='direct-dispatch-form'>
                <table id="dir-dispatch-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Department</th>
                                <th>Material Name</th>
                                <th>Dispatchaed Date</th>
                                <th>Current Available Stock</th>
                                <th>Dispatchaed Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select id="dir-department-<?php echo $i?>" name="department_id[]" class='form-control' style="min-width:200px" onchange="directDispatchMaterial(<?php echo $i?>)">
                                        <option valure="">-- select option --</option>
                                        <?php foreach($departments as $department){?>
                                            <option value="<?php echo $department['id']?>"><?php echo $department['name']?></option>
                                        <?php }?>
                                    </select>
                                </td>
                                <td>
                                    <select id="dir-material-<?php echo $i?>" name='material_id[]' class='form-control'  style="min-width:200px" onchange="getMaterialStock(<?php echo $i?>)"></select>
                                </td>
                                <td>
                                    <input type='text' name="date_np[]" class="date form-control" id="dir-confirm-date-<?php echo $i?>" onchange=changeDate(<?php echo $i?>)>
                                    <input type='hidden' name="date_en[]" class="en-date" id="dir-confirm-en-date-<?php echo $i?>" >
                                    <script>
                                    $(function () {
                                            $('#dir-confirm-date-<?php echo $i?>').nepaliDatePicker({
                                                ndpEnglishInput: 'dir-confirm-en-date-<?php echo $i?>'
                                            });
                                        });
                                        $("#dir-confirm-date-<?php echo $i?>").change(function(){
                                            $('#dir-confirm-en-date-<?php echo $i?>').val(BS2AD($('#dir-confirm-date-<?php echo $i?>').val()));
                                        });
                                    </script>
                                </td>
                                <td>
                                    <input type='hidden' name="current_stock[]" id="dir-current-stock-hidden-<?php echo $i?>">
                                    <input type='text' id="dir-current-stock-<?php echo $i?>" disabled class='form-control'>
                                </td>
                                <td>
                                    <input type='number' name="added_qty[]" id="dir-accept-stock-<?php echo $i?>" onchange="dirCheckQty(<?php echo $i?>)" onkeyup="dirCheckQty(<?php echo $i?>)" onblur="dirCheckQty(<?php echo $i?>)" class='form-control' min=0>
                                </td>
                                <td><button type='button' class='btn btn-danger removebutton'><i class='fa fa-trash'></i></button></td>
                                
                            </tr>
                        </tbody>
                      </table>
                      <button type='button' class="btn btn-primary" onclick='addRow(<?php echo $i?>)'>Add</button>
                      <span id='direct-dispatch-button' ><button type='button' class="btn btn-primary" onclick='saveDirectDispatch(<?php echo $i?>)'>Confirm</button></span>
                  </form>
                </div>
                <!--##########################################################################-->
                <div class="tab-pane" id="tab_2">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Depart Name</th>
                                <th>Material Name</th>
                                <th>Requested Date</th>
                                <th>Accepted Date</th>
                                <th>Current Available Stock</th>
                                <th>Requested Stock</th>
                                <th>Accepted Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($histories as $history){?>
                            <tr>
                                <td>
                                <?php echo $history['request_id']?>
                                </td>
                                <td>
                                <?php echo $history['material_name']?>
                                </td>
                                    <td><input type='hidden' id="requested_id-<?php echo $history['request_id']?>" value='<?php echo $history['request_id']?>'>
                                    <?php echo $history['name']?>
                                </td>
                                <td>
                                    <?php echo $history['request_np_date']?></td>
                                <td>
                                    <input type='text' class="date" id="confirm-date-<?php echo $history['request_id']?>" onchange=changeDate(<?php echo  $history['request_id']?>) value='<?php echo $history['store_confirm_np_date']?>'>
                                    <input type='hidden' class="en-date" id="confirm-en-date-<?php echo $history['request_id']?>" value="<?php echo $history['store_confirm_en_date']?>">
                                    <script>
                                    $(function () {
                                            $('#confirm-date-<?php echo $history['request_id']?>').nepaliDatePicker({
                                                ndpEnglishInput: 'confirm-en-date-<?php echo $history['request_id']?>'
                                            });
                                        });
                                        $("#confirm-date-<?php echo $history['request_id']?>").change(function(){
                                            $('#confirm-en-date-<?php echo $history['request_id']?>').val(BS2AD($('#confirm-date-<?php echo $history['request_id']?>').val()));
                                        });
                                    </script>
                                </td>
                                <td>
                                    <input type='hidden' id="current-stock-<?php echo $history['request_id']?>" value='<?php echo $history['current_stock']?>'>
                                    <input type='hidden' id="current-<?php echo $history['request_id']?>" value='<?php echo $history['current']?>'>
                                    <?php echo ($history['current_stock'] != 0 && $history['current_stock'] != '')?number_format($history['current_stock'],3):number_format($history['current'],3)?>
                                </td>
                                <td>
                                    <input type='hidden' id="requested_stock-<?php echo $history['request_id']?>" value='<?php echo $history['request_quantity']?>'>
                                    <?php echo number_format($history['request_quantity'],3)?>
                                </td>
                                <td>
                                    <input type='number' id="accept-stock-<?php echo $history['request_id']?>" onchange="checkQty(<?php echo $history['request_id']?>)" value='<?php echo $history['accepted_quantity']?>'>
                                </td>
                                <td><button class="btn btn-primary" onclick=editPending(<?php echo $history['request_id']?>)>Confirm</button></td>
                            </tr>
                            <?php }?>
                        </tbody>
                      </table>
                </div>
                <div class="tab-pane" id="tab_3">
                     <table id="example" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Material Name</th>
                                <th>Department Name</th>
                                <th>Requested Date</th>
                                <th>Accepted Date</th>
                                <th>Current Available Stock</th>
                                <th>Requested Stock</th>
                                <th>Supplied Stock</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tab-pane" id="tab_4">
                     <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Material Name</th>
                                <th>Department Name</th>
                                <th>Requested Date</th>
                                <th>Accepted Date</th>
                                <th>Current Available Stock</th>
                                <th>Requested Stock</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tab-pane" id="tab_7">
                     <table id="direct-dispatch-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Material Name</th>
                                <th>Depart Name</th>
                                <th>Dispatched Date</th>
                                <th>Current Available Stock</th>
                                <th>Supplied Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tab-pane" id="tab_5">
                    <table id="confirm-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Material Name</th>
                                <th>Depart Name</th>
                                <th>Requested Date</th>
                                <th>Accepted Date</th>
                                <th>Current Available Stock</th>
                                <th>Requested Stock</th>
                                <th>Accepted Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($confirms as $history){?>
                            <tr>
                                <td>
                                <?php echo $history['request_id']?>
                                </td>
                                <td>
                                <?php echo $history['material_name']?>
                                </td>
                                    <td>
                                    <?php echo $history['name']?>
                                </td>
                                <td>
                                    <?php echo $history['request_np_date']?></td>
                                <td>
                                    <?php echo $history['store_confirm_np_date']?>
                                    <?php //echo $history['store_confirm_np_date']?>
                                </td>
                                <td>
                                    <?php echo ($history['current_stock'] != 0 && $history['current_stock'] != '')?number_format($history['current_stock'],3):number_format($history['current'],3)?>
                                </td>
                                <td>
                                    <?php echo number_format($history['request_quantity'],3)?>
                                </td>
                                <td>
                                    <?php echo number_format($history['accepted_quantity'],3)?>
                                </td>
                                <td>
                                    <button class='btn btn-warning' onclick='edit(<?php echo $history['request_id']?>)'><i class='fa fa-pencil'></i></button>
                                    <button class='btn btn-danger' onclick='deletedispatch(<?php echo $history['request_id']?>)'><i class='fa fa-trash'></i></button>
                                </td>
                                
                            </tr>
                            <?php }?>
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- edit modal -->
<div class="modal fade" id="edit-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="edit-dispatch-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Material</label>
                        <div class="col-sm-8">
                            <input type='text' name='material_name' id='edit-material-name' class='form-control' disabled>
                            <input type='hidden' name='material_id' id='edit-material-id' class='form-control' >
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Department</label>
                        <div class="col-sm-8">
                            <input type="text" name="department_name" id="edit-department-name" disabled class='form-control'>
                            <span class="error_message" id="error-request" style="color: red" hidden="true">Purchase request is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Current Stock</label>
                        <div class="col-sm-8">
                            <input type="text" name="current_stock" id="edit-current-stock" readonly="" class='form-control'>
                            <span class="error_message" id="error-request" style="color: red" hidden="true">Purchase request is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Requested Quantity</label>
                        <div class="col-sm-8">
                            <input type="text" name="request_qty" id="edit-request-quantity" readonly="" class='form-control'>
                            <span class="error_message" id="error-request" style="color: red" hidden="true">Purchase request is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Accepted Quantity</label>
                        <div class="col-sm-8">
                            <input type="number" name="accepted_qty" id="edit-accepted-quantity" class='form-control'>
                            <span class="error_message" id="error-request" style="color: red" hidden="true">Purchase request is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label">Nepali Date</label>
                        <div class="col-sm-8">
                            <input type="text" id="edit-accepted-date-np" name="date_np" class="form-control">
                            <input type="hidden" name="date_en" id="edit-accepted-date-en" class="form-control">
                            <span class="error_message" id="error-date" style="color: red" hidden="true">Date is required</span>
                        </div>
                    </div>


                    <input type="hidden" name='id' id='edit-id'>
<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                        
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-edit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- edit modal end -->
<!-- edit direct dispatch modal -->
<div class="modal fade" id="edit-direct-dispatch-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="edit-direct-header"></h4>
            </div>
            <form class="form-horizontal" id="edit-direct-dispatch-form" action="#">
            <div id='edit-direct-form'>
            
            </div>
                

                <div class="modal-footer">
                    <button type="button" id="dir-form-edit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- edit direct dispatch modal end -->


<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    var i = 0;
        var table = $('#example2,#confirm-table').DataTable({
            // "order" : [[0, 'desc']]
            "aaSorting": [ [3,'desc'], [0,'desc']],
            "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ]
        });

        var table1 = $('#example').DataTable( {
            "ajax": "<?php echo site_url()?>inventory/acceptedDispatch",
            "columns": [
                { "data": "request_id"},
                { "data": "material_name" },
                { "data": "name" },
                { "data": "request_np_date" },
                { "data": "store_confirm_np_date" },
                { "data": "current_stock" },
                { "data": "request_quantity" },
                { "data": "accepted_quantity" }
            ],
            "aaSorting": [ [3,'desc'], [0,'desc']],
            "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }]

        } );
        var table1 = $('#example1').DataTable( {
            "ajax": "<?php echo site_url()?>inventory/rejectedDispatch",
            "columns": [
                { "data": "request_id"},
                { "data": "material_name" },
                { "data": "name" },
                { "data": "request_np_date" },
                { "data": "store_confirm_np_date" },
                { "data": "current_stock" },
                { "data": "request_quantity" },
            ],
            "aaSorting": [ [3,'desc'], [0,'desc']],
            "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }]
        } );

        var table2 = $('#direct-dispatch-table').DataTable( {
            "ajax": "<?php echo site_url()?>inventory/directDispatch",
            "columns": [
                { "data": "request_id"},
                { "data": "material_name" },
                { "data": "name" },
                { "data": "store_confirm_np_date" },
                { "data": "current_stock" },
                { "data": "accepted_quantity" },
                { "data": "action"},
            ],
            "aaSorting": [ [3,'desc'], [0,'desc']],
             "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }]
        } );

    $(function () {
        $('#nepali-date').nepaliDatePicker();
        $('#edit-accepted-date-np').nepaliDatePicker({
            ndpEnglishInput: 'edit-accepted-date-en'
        })
    });
</script>

<script type="text/javascript">
    $("#material").change(function(){
        $("#purchase_request").empty();
        $("#requested-amount").val('');
        var id = $(this).val();
        $.post('<?php echo site_url("inventory/departmentRequest")?>',{id:id},function(data){
            $("#purchase_request").append
                ($('<option>',{
                    value:'',
                    text:'-- select request --'
                }));
            $.each(data.request,function(index,request){
                $("#purchase_request").append
                ($('<option>',{
                    value:request.request_id,
                    text:'Request from '+request.name
                }));
            });
             $('#added_qty').val('');
            $('#current_stock').val(data.current_stock.current_stock)
        },'json')
    });
</script>
<script type="text/javascript">
    $("#purchase_request").change(function(){
        var id = $(this).val();
        var material_id = $('#material_id').val();
        $.post('<?php echo site_url("inventory/getRequestedQty")?>',{id:id,material_id:material_id},function(data){
            $('#requested-amount').val(data.request_quantity);
            
        },'json')
    });
</script>
<script type="text/javascript">
    $("#accepted-qty").change(function(){
        $request_amount = Number($("#requested-amount").val());
        accepted_qty = Number($(this).val());
        console.log(accepted_qty+"<="+$request_amount);
        if($(this).val() < 0 || accepted_qty > $request_amount){
            alert('Accepted quantity is not valid');
            $('#submit').prop('disabled',true);
        }else{
            $('#submit').prop('disabled',false);
        }
    })
</script>
<?php /*saving dispatch*/?>
<script type="text/javascript">
    $("#submit").click(function(){
        $.post('<?php echo site_url("inventory/saveDispatch")?>',$("#dispatch-form").serializeArray(),function(data){
            if(data.success){
                // myDataRow =;
                // table.row.add( [data.history.material_name,
                //                 data.history.name,
                //                 data.history.request_np_date,
                //                 data.history.store_confirm_np_date,
                //                 data.history.current_stock,
                //                 data.history.request_quantity,
                //                 data.history.accepted_quantity,
                //                 "<button class='btn btn-primary'>Confirm</button>"]).draw( false );

table.row.add( [data.history.material_name,
                                data.history.name,
                                data.history.request_np_date,
                                data.history.store_confirm_np_date,
                                data.history.current_stock,
                                data.history.request_quantity,
                                data.history.accepted_quantity,
                                "<button class='btn btn-primary'>Confirm</button>"]).draw( false );

                alert('Material Dispatched');
                $('#material_id').val('');
                $('#purchase_request').empty();
                $("#purchase_request option[value="+$('#purchase_request').val()+"]").remove();
                $('#requested-amount').val('');
                $('#current_stock').val('');
                $('#nepali-date').val('');
                $('#accepted-qty').val('');
                $('.error_message').hide();
            }else{
                //$('.error_message').show();
                if($('#material_id').val('')){
                    $('#error_material').show();
                }else{
                    $('#error_material').hide();
                }
            }
        },'json')
    });
</script>
<script type="text/javascript">
    function checkQty(id){
        request_qty = parseFloat($('#requested_stock-'+id).val().replace(',',''));
        accepte_qty = parseFloat($('#accept-stock-'+id).val().replace(',',''));
        current_stock = parseFloat($('#current-'+id).val().replace(',',''));
        console.log(request_qty);
        console.log(accepte_qty);
        console.log(current_stock);
        
            if(request_qty < accepte_qty){
                alert('Accepted quantity is greater than requested quantity');
                $('#accept-stock-'+id).val("");
            }
            else if(accepte_qty >current_stock){
                alert('Accepted quantity is greater than stock');
                $('#accept-stock-'+id).val("");
            }
        
    }
</script>
<script type="text/javascript">
    function editPending(id){
        var r = confirm('Do you want to dispatch item?')
        if(r){
            request_id = $('#requested_id-'+id).val();
            confirm_date = $('#confirm-date-'+id).val();
            confirm_en_date = $('#confirm-en-date-'+id).val();
            current_stock = $('#current-stock-'+id).val();
            current = $('#current-'+id).val();
            accept_qty = $('#accept-stock-'+id).val().replace(',','');
            $.post('<?php echo site_url("inventory/saveDispatch")?>',{request_id:request_id, date_np:confirm_date, accepted_qty:accept_qty, current_stock:current_stock,current:current,en_date:confirm_en_date},function(data){
                    if(data.success){
                        alert('Item dispatched');
                        location.reload();
                    }
                    if(!data.success){
                        alert('Please enter the date and stock amount');
                    }
            },'json')
        }
    }
</script>


<script type="text/javascript">
    function edit(id){
        $.post('<?php echo site_url("inventory/getDispatch")?>',{id:id},function(data){
            $('#edit-material-name').val(data.material_name);
            $('#edit-material-id').val(data.material_id);
            $('#edit-current-stock').val(data.current_stock);
            $('#edit-department-name').val(data.name);
            $('#edit-request-quantity').val(data.request_quantity);
            $('#edit-accepted-quantity').val(data.accepted_quantity);
            $('#edit-accepted-date-np').val(data.store_confirm_np_date);
            $('#edit-accepted-date-en').val(data.store_confirm_en_date);
            $('#edit-id').val(data.request_id);
            $('#edit-dialog').modal();
        },'json');
    }
</script>
<!-- direct dispatch form -->
<script type="text/javascript">
    function editDirectDispatch(id){
        $.post('<?php echo site_url("inventory/getDirectDispatchForm")?>',{id:id},function(data){
            $('#edit-direct-header').html('Edit');
            $('#edit-direct-form').empty();
            $('#edit-direct-form').html(data);
            $('#edit-direct-dispatch-dialog').modal();
        },'html');
    }
</script>

<script type="text/javascript">
    $('#form-edit').click(function(){
        if($('#edit-accepted-quantity').val()> parseFloat($('#edit-request-quantity').val())){
            alert('accepted quantity is greater than requested quantity');
            $('#edit-accepted-quantity').val('');
        }else if($('#edit-accepted-quantity').val() > parseFloat($('#edit-current-stock').val())){
            alert('accepted quantity is greater than current stock');
            $('#edit-accepted-quantity').val('');
        }else{
            if(confirm('Do you want to edit dispatch?')){
                $.post('<?php echo site_url("inventory/editDispatch")?>',$("#edit-dispatch-form").serializeArray(),function(data){
                    if(data){
                        alert('Dispatch edited');
                        location.reload();
                    }else{
                        alert('Try again');
                    }
                },'json');
            }
        }
    })
</script>
<!-- for direct dispatch edit -->
<script type="text/javascript">
    $('#dir-form-edit').click(function(){
            if(confirm('Do you want to edit dispatch?')){
                $.post('<?php echo site_url("inventory/editDirectDispatch")?>',$("#edit-direct-dispatch-form").serializeArray(),function(data){
                    if(data){
                        alert('Dispatch edited');
                        location.reload();
                    }else{
                        alert('Try again');
                    }
                },'json');
            }
    })
</script>
<script type="text/javascript">
    function deletedispatch(id){
        if(confirm('Do you want to delete dispatch?')){
            $.post('<?php echo site_url('inventory/deleteDispatch')?>',{id:id},function(data){
                if(data){
                    alert('Dispatch removed');
                    location.reload();
                }else{
                    alert('Please try again');
                }
            },'json');
        }
    }
</script>
<script type="text/javascript">
    function deleteDirectDispatch(id){
        if(confirm('Do you want to delete dispatch?')){
            $.post('<?php echo site_url('inventory/deleteDirectDispatch')?>',{id:id},function(data){
                if(data){
                    alert('Dispatch removed');
                    location.reload();
                }else{
                    alert('Please try again');
                }
            },'json');
        }
    }
</script>
<script type="text/javascript">
    function directDispatchMaterial(i){
        id = $('#dir-department-'+i).val();
        $.post('<?php echo site_url("direct_dispatch_material/getMaterial")?>',{id:id},function(data){
            $("#dir-material-"+i).empty();
            $("#dir-material-"+i).append
                    ($('<option>',{
                        value:'',
                        text: '-- Select opton -- '
                    }));
            $.each(data,function(index,value){
                console.log(value);
                $("#dir-material-"+i).append
                    ($('<option>',{
                        value:value['material_id'],
                        text: value['material_name']
                    }));
            });

        },'json');
    }
</script>
<script type="text/javascript">
    function getMaterialStock(i){
        material_id = $('#dir-material-'+i).val();
        // alert(i);
        $.post('<?php echo site_url('direct_dispatch_material/get_stock')?>', {'material_id':material_id},function(return_stock){
                if(return_stock.current_stock){
                    // $('#stock-'+i).val(return_stock.amount);
                    $('#dir-current-stock-'+i).val(return_stock.current_stock);
                    $('#dir-current-stock-hidden-'+i).val(return_stock.current_stock);
                }else{
                    $('#dir-current-stock-'+i).val(0);
                }
            },"json");
    }
</script>
<script type="text/javascript">
    function saveDirectDispatch(i){
        // if($('#dir-accept-stock-'+i).val() != '' && $('#dir-current-stock-'+i).val() != '' && $('#dir-confirm-en-date-'+i).val() != ''){
            $.post('<?php echo site_url('direct_dispatch_material/savedd')?>', $('#direct-dispatch-form').serializeArray(),function(success){
                    if(success.success){
                        // $('#stock-'+i).val(return_stock.amount);
                        alert('Item dispatched')
                        location.reload();
                    }else{
                        alert('Error occured,please try again');
                    }
                },"json");
        // }else{
        //     alert('All field is required');
        // }
    }
</script>
<script type="text/javascript">
    function dirCheckQty(i){
        added_qty = parseFloat($('#dir-accept-stock-'+i).val());
        current_stock = parseFloat($('#dir-current-stock-'+i).val().replace(',',''));
        // if(current_stock != )
        if(current_stock < added_qty){
            alert('Current stock is less than dispatch quantity')
            $('#dir-accept-stock-'+i).val('');
        }
    }
</script>
<script type="text/javascript">
    function addRow(){
        $.post('<?php echo site_url("direct_dispatch_material/addRow")?>',{i:i},function(data){
            $('#dir-dispatch-table').append(data);
        },'html');
        i++;
        $('#direct-dispatch-button').empty();
        $('#direct-dispatch-button').html("<button type='button' class='btn btn-primary' onclick='saveDirectDispatch("+i+")'>Confirm</button>");
        <?php $i++?>
    }
</script>
<script type="text/javascript">
    $(document).on('click', 'button.removebutton', function () { // <-- changes
     $(this).closest('tr').remove();
     return false;
 });
</script>