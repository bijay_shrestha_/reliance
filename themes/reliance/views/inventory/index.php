<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Add Stock</a></li>
                <li><a href="#tab_2" data-toggle="tab">Stock</a></li>
                <li><a href="#tab_3" data-toggle="tab">Purchase History</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                <form id="stock-form" class="form-horizontal">
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Material</label>
                        <div class="col-sm-8">
                            <select name="material_id" id="material" class="form-control">
                                <option value="">-- select Material --</option>
                                <?php foreach($materials as $material){?>
                                <option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
                                <?php }?>
                            </select>
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label for="head_office_purchase" class="col-sm-4 control-label"> Head-office Purchase</label>
                        <div class="col-sm-8"><input id="enable-dealer" value="1" name="head_office" type="checkbox"></div>
                    </div> -->
                    <div class="form-group">
                        <label for="dealer_id" class="col-sm-4 control-label">Dealer</label>
                        <div class="col-sm-8">
                            <select name="dealer_id" id="dealer-id" class="form-control" >
                                <option value="">-- select dealer --</option>
                                <?php foreach($dealers as $dealer){?>
                                <option value="<?php echo $dealer['dealer_id']?>"><?php echo $dealer['dealer_name']?></option>
                                <?php }?>
                            </select>
                            <input type='hidden' name="post_dealer_id" id='post-dealer-id'>
                            <span class="error_message" id="error-dealer" style="color: red" hidden="true">Dealer is required if head office purchase</span>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Purchase Request Number</label>
                        <div class="col-sm-8">
                            <select name="purchase_request" id='purchase_request' class="form-control">
                                
                            </select>
                            <span class="error_message" id="error-request" style="color: red" hidden="true">Purchase request is required</span>
                        </div>
                    </div> -->
                    <?php /*<div class="form-group">
                        <label for="return_item" class="col-sm-4 control-label">Return Item</label>
                        <div class="col-sm-8"><input id="enable-department" name="return_item" type="checkbox"></div>
                    </div>
                    <div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label"> Item Return From Department</label>
                        <div class="col-sm-8">
                            <select name="department_id" id="department-id" class="form-control" disabled>
                                <option value="">-- select department --</option>
                                <?php foreach($departments as $department){?>
                                <option value="<?php echo $department['id']?>"><?php echo $department['name']?></option>
                                <?php }?>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Department is required if item return</span>
                        </div>
                    </div>*/?>
                    <?php /*<div class="form-group">
                        <label for="dealer_id" class="col-sm-4 control-label">Head Office Per-Unit Cost</label>
                        <div class="col-sm-8">
                            <input type='number' class="form-control" id="head-oddice-unit-cost" disabled name="headoffice_unitcost" id="per-unit-cost">
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>*/?>
                    <div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label">Nepali Date</label>
                        <div class="col-sm-8">
                            <input type="text" id="nepaliDate" name="date_np" class="form-control">
                            <input type="hidden" name="date_en" id="date-en" class="form-control">
                            <script>
                                $('#nepaliDate').change(function(){
                                      $('#date-en').val(BS2AD($('#nepaliDate').val()));
                                });
                            </script>

                            <span class="error_message" id="error-date" style="color: red" hidden="true">Date is required</span>
                        </div>
                    </div>

                    <?php /*<div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label">English Date</label>
                        <div class="col-sm-8">
                            <input type="text" name="date_en" class="form-control">
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>*/?>
                    <div class="form-group">
                        <label for="current_stock" class="col-sm-4 control-label">Current Stock</label>
                        <div class="col-sm-8">
                            <input type="text" readonly id="current_stock" name="current_stock" class="form-control">
                            <!-- <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Added_stock" class="col-sm-4 control-label">Added Quantity</label>
                        <div class="col-sm-8">
                            <input type="text" id="added-qty" name="added_qty" class="form-control">
                            <span class="error_message" id="error-qty" style="color: red" hidden="true">Added quantity is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="unit" class="col-sm-4 control-label">Unit</label>
                        <div class="col-sm-8">
                            <input type="text" id="unit" class="form-control" disabled>
                        </div>
                    </div>
                    <button type='button' class="btn btn-primary" id="submit">Save</button>
                </form>
                </div>
                <div class="tab-pane" id="tab_2">
                <!-- <input type="text" id="date" class="margin" name="date">
                <input type="hidden" id="eng-search-date" class="margin" name="date">
                <button onclick="date_search()">search</button> -->
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>Material Name</th>
                          <th>Current Stock</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($stocks as $stock){?>
                            <tr>
                                <td><?php echo $stock['material_name']?></td>
                                <td><?php echo number_format($stock['current_stock'],3)?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                      </table>
                </div>
                <div class="tab-pane" id="tab_3">
                    <table id="example3" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                              <th>Date</th>
                              <th>Material Name</th>
                              <th>Dealer Name</th>
                              <!-- <th>Purchase Request ID</th> -->
                              <!-- <th>Department</th> -->
                              <!-- <th>Head Office Purchase</th> -->
                              <th>Added Quantity</th>
                              <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($purchases as $purchase){?>
                            <tr>
                                <td><?php echo $purchase['purchase_id']?></td>
                                <td><?php echo $purchase['date_np']?></td>
                                <td><?php echo $purchase['material_name']?></td>
                                <td><?php echo $purchase['dealer_name']?></td>
                                <!-- <td><?php echo $purchase['request_id']?></td> -->
                                <?php /*<td><?php echo $purchase['name']?></td>*/?>
                                <!-- <td><?php echo ($purchase['headoffice_purchase'])?'Yes':'No'?></td> -->
                                <td><?php echo number_format($purchase['added_qty'],3)?></td>
                                <td>
                                    <button type="button" class="btn btn-warning" onclick="edit(<?php echo $purchase['purchase_id']?>)"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class='btn btn-danger' onclick='deleteHistory(<?php echo $purchase['purchase_id']?>)'><i class='fa fa-trash'></i></button>
                                </td>
                            </tr>
                            <?php }?>
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--edit modal-->
<div class="modal fade" id="edit-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="edit-stock-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Material</label>
                        <div class="col-sm-8">
                            <input type='text' name='material_name' id='edit-material-name' class='form-control' disabled>
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label for="head_office_purchase" class="col-sm-4 control-label"> Head-office Purchase</label>
                        <div class="col-sm-8"><input id="edit-enable-dealer" value="1" name="head_office" type="checkbox" disabled></div>
                    </div> -->
                    <div class="form-group">
                        <label for="dealer_id" class="col-sm-4 control-label">Dealer</label>
                        <div class="col-sm-8">
                            <?php /*<select name="dealer_id" id="edit-dealer-id" class="form-control" disabled>
                                <option value="">-- select dealer --</option>
                                <?php foreach($dealers as $dealer){?>
                                <option value="<?php echo $dealer['dealer_id']?>"><?php echo $dealer['dealer_name']?></option>
                                <?php }?>
                            </select>*/?>
                            <input type="text" name="dealer_id" id='edit-dealer-id' class="form-control" disabled>
                            <input type='hidden' name="post_dealer_id" id='post-dealer-id'>
                            <span class="error_message" id="error-dealer" style="color: red" hidden="true">Dealer is required if head office purchase</span>
                        </div>
                    </div>
                    <?php /*<div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Purchase Request Number</label>
                        <div class="col-sm-8">
                            <input type="text" name="purchase_request" id="edit-purchase_request" class='form-control'>
                            <span class="error_message" id="error-request" style="color: red" hidden="true">Purchase request is required</span>
                        </div>
                    </div>*/?>                    
                    <div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label">Nepali Date</label>
                        <div class="col-sm-8">
                            <input type="text" id="edit-date-np" name="date_np" class="form-control">
                            <input type="hidden" name="date_en" id="edit-date-en" class="form-control">
                            <span class="error_message" id="error-date" style="color: red" hidden="true">Date is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="current_stock" class="col-sm-4 control-label">Current Stock</label>
                        <div class="col-sm-8">
                            <input type="text" readonly id="edit-current-stock" name="current_stock" class="form-control">
                            <!-- <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Added_stock" class="col-sm-4 control-label">Added Quantity</label>
                        <div class="col-sm-8">
                            <input type="number" id="edit-added-qty" name="added_qty" class="form-control">
                            <span class="error_message" id="error-qty" style="color: red" hidden="true">Added quantity is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="unit" class="col-sm-4 control-label">Unit</label>
                        <div class="col-sm-8">
                            <input type="text" id="edit-unit" class="form-control" disabled>
                        </div>
                    </div>



<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                        <input type="hidden" name="accepted_qty" id="edit-accepted-qty">
                        <input type="hidden" id="edit_purchase_id" name="purchase_id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-edit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
    $("#dealer-id").change(function(){
        $("#post-dealer-id").val($(this).val());
    });
</script>
<script>
    $(function () {
        table = $('#example2').DataTable();
        $('#example3').DataTable({
            "aaSorting": [ [0,'desc'], [1,'desc'] ],
            "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 1,2,3,5,6 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [1,2,3,5,6]
                    },
                    customize: function ( doc ) {
                        doc.content.splice( 0, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            image: '<?php echo $this->pdf_logo?>',/*64 bit encoded image in public controller*/
                        })
                        doc.content.splice( 2, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Date : '+date
                        })
                        doc.content.splice( 3, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Report Type : Purchase History'
                        })
                    }
                },
            ],
            // "order" : [[0, "desc"]]
        });
        $('#nepaliDate').nepaliDatePicker({
            ndpEnglishInput: 'date-en'
        });
        $('#date').nepaliDatePicker({
            ndpEnglishInput: 'eng-search-date'
        });
        $('#edit-date-np').nepaliDatePicker({
            ndpEnglishInput: 'edit-date-en'
        });
    
    });
</script>
<script type="text/javascript">
    function date_search(){
        var date = $("#eng-search-date").val();
        $.post('<?php echo site_url("inventory/searchWithDate")?>',{date:date},function(data){
            alert('here');
        },'json');
        table.draw();
    }
</script>

<script type="text/javascript">
    $("#material").change(function(){
        $("#purchase_request").empty();
        $("#dealer-id").empty();
        $("#post-dealer-id").empty();
        var id = $(this).val();
        $.post('<?php echo site_url("inventory/getPurchase_request")?>',{id:id},function(data){
            $("#dealer-id").append('<option value="">-- Select dealer --</option>');
            $.each(data.dealers,function(index,dealer){
                $("#dealer-id").append('<option value="'+dealer.dealer_id+'" id="dealer-'+dealer.dealer_id+'">'+dealer.dealer_name+'</option>');
                // ($('<option>',{
                //     value:dealer.dealer_id,
                //     text:dealer.dealer_name
                // }));
            });
            $('#unit').val(data.material.unit);
             $('#added-qty').val('');
            $('#current_stock').val(data.current_stock.current_stock)
        },'json')
    });
</script>

<script type="text/javascript">
    $("#purchase_request").change(function(){
        var id = $(this).val();
        $.post('<?php echo site_url("inventory/getAcceptedQty")?>',{id:id},function(data){
            $('#added-qty').val(data.added_qty);
            $('#dealer-'+data.dealer_id).prop('selected',true);
            // console.log(data.dealer_id);
            $('#post-dealer-id').val(data.dealer_id);
        },'json');
    })
</script>

<script type="text/javascript">
    $('#submit').click(function(){
        $.post('<?php echo site_url('inventory/save');?>',$("#stock-form").serializeArray(),
            function(data){
                if(data.success){
                    alert('Inventory added successfully')
                    $('.error_message').hide();
                    location.reload();
                }
                else{
                    // $('.error_message').show();
                    if($('#material').val() == ''){
                        $('#error-material').show();
                    }else{
                        $('#error-material').hide();
                    }
                    if($('#nepaliDate').val() == ''){
                        $('#error-date').show();
                    }else{
                        $('#error-date').hide();
                    }
                    if($('#added-qty').val() == ''){
                        $('#error-qty').show();
                    }else{
                        $('#error-qty').hide();
                    }
                }
            },'json');
    })
</script>
<script type="text/javascript">
    $('#enable-department').click(function(){
        $('#department-id').prop('disabled',!$(this).prop('checked'));
        $('#purchase_request').prop('disabled',$(this).prop('checked'));
        $('#dealer-id').prop('disabled',true);
        // $('#enable-dealer').prop('checked',false);
        $('#head-oddice-unit-cost').prop('disabled',true);
        $('#added-qty').prop('readonly',!$(this).prop('checked'));
    });
</script>
<script type="text/javascript">
    // $('#enable-dealer').click(function(){
    //     $('#dealer-id').prop('disabled',!$(this).prop('checked'));
    //     $('#purchase_request').prop('disabled',$(this).prop('checked'));
    //     $('#department-id').prop('disabled',true);
    //     $('#enable-department').prop('checked',false);
    //     $('#head-oddice-unit-cost').prop('disabled',!$(this).prop('checked'));
    //     $('#added-qty').prop('readonly',!$(this).prop('checked'));
    // });
</script>
<script type="text/javascript">
    function edit(id){
        $.post('<?php echo site_url('inventory/getPurchase')?>',{id:id},function(data){
            $('#myModalLabel').text('Edit Purchase History');
            // $('#edit-material-'+data.material_id).prop('selected',true);
            $('#edit-material-name').val(data.material_name);
            $('#edit_purchase_id').val(data.pid);
            // if(data.headoffice_purchase == 1){
                // $('#edit-enable-dealer').prop('checked',true);
                console.log(data.dealer_name);
                $('#edit-dealer-id').val(data.dealer_name);
                $('#edit-purchase_request').prop('disabled',true);
            // }else{
                // $('#edit-purchase_request').val('accepted quantity '+data.accepted_quantity+' at rate '+data.accepted_cost);
                // $('#edit-purchase_request').prop('disabled',true);
            // }
            $('#edit-accepted-qty').val(data.accepted_quantity)
            $('#edit-date-np').val(data.date_np);
            $('#edit-date-en').val(data.date_en);
            $('#edit-current-stock').val(data.current_stock);
            $('#edit-added-qty').val(data.added_qty);
            $('#edit-unit').val(data.unit);
            $('#edit-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    $('#edit-added-qty').keyup(function(){
        var requested_qty = $('#edit-accepted-qty');
        var added_qty = $(this).val();
        if(requested_qty != ''){
            if(added_qty > requested_qty){
                alert('Added quantity is greater than accepted qty');
            }
        }
    });
</script>
<script type="text/javascript">
    $('#form-edit').click(function(){
        if(confirm('Do you want to edit purchase history?')){
            $.post('<?php echo site_url("inventory/editPurchaseHistory")?>',$('#edit-stock-form').serializeArray(),function(data){
                if(data){
                    location.reload();
                }else{
                    alert('Please try again')
                }
            },'json');
        }
    })
</script>
<script type="text/javascript">
    function deleteHistory(id){
        if(confirm('Do you want to delete purchase history?')){
            $.post("<?php echo site_url('inventory/deleteHistory')?>",{id:id},function(data){
                if(data){
                    alert('Purchase history deleted');
                    location.reload();
                }else{
                    alert('History not deleted');
                }
            },'json');
        }
    }
</script>