<!-- DataTables -->
<style type="text/css">
    td.details-control {
        background: url('<?php echo theme_url()?>plugins/datatables/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?php echo theme_url()?>plugins/datatables/images/details_close.png') no-repeat center center;
    }
</style>
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
        <!-- lc table -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">LC list Table</h3>
                </div><!-- /.box-header -->
                <button id="add-lc" class="btn btn-primary margin">Add</button>
                <div class="box-body">
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="sorting_disabled"></th>
                                <th>Date</th>
                                <th>PI #</th>
                                <th>Dealer Name</th>
                                <th>Bank Name</th>
                                <th>DD/LC #</th>
                                <th>Total Quantity</th>
                                <th>Total LC Value</th>
                                <!-- <th>Status</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($rows as $row) { ?>
                                <tr>
                                    <td class="details-control"></td>
                                    <td><?php echo $row['date_en']?></td>
                                    <td><?php echo $row['pi_number'] ?></td>
                                    <td><?php echo $row['dealer_name'] ?></td>
                                    <td><?php echo $row['bank_name'] ?></td>
                                    <td><?php echo $row['dd_lc_no'] ?></td>
                                    <td>
                                        <?php 
                                            $total_qty = 0;
                                            foreach ($materials[$row['id']] as $key => $value) {
                                                $total_qty += $value['quantity'];
                                            }
                                            echo number_format($total_qty,3);
                                        ?>
                                    </td>
                                    <td><?php echo number_format($row['total_lc_value'],3) ?></td>
                                    <td>
                                        <button type="button" class="btn btn-warning edit-lc" id="lc-<?php echo $row['id']?>" value="<?php echo $row['id']?>">Edit</button>
                                        <button type="button" class="btn btn-danger delete-lc" id="delete-lc-<?php echo $row['id']?>" value="<?php echo $row['id']?>">Delete</button>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        <!-- dealer table end -->
                    
</section>

<div class="modal fade" id="lc-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-3">
                                <input type="text" name="date_en" class="form-control" id="date_en" placeholder="English Date">
                                <span class="error_message" id="error-date" style="color: red" hidden="true">Date is required</span>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Nepali Date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pi_number" class="col-sm-4 control-label">PI#</label>
                            <div class="col-sm-8">
                                <input type="text" name="pi_number" class="form-control" id="pi_number" placeholder="PI Number">
                                <span class="error_message" id="error-pi_number" style="color: red" hidden="true">PI number is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dealer" class="col-sm-4 control-label">Dealer</label>
                            <div class="col-sm-8">
                                <select id='dealer_id' class="form-control" name="dealer_id">
                                    <option value="">-- select dealer --</option>
                                    <?php foreach ($dealers as $key => $value) {?>
                                        <option value="<?php echo $value['dealer_id']?>"><?php echo $value['dealer_name']?></option>
                                    <?php }?>
                                </select>
                                <span class="error_message" id="error-dealer_id" style="color: red" hidden="true">Please select the dealer</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class='col-md-1'></div>
                            <div class='col-md-3' style="text-align: center;"><label>Material</label></div>
                            <div class='col-md-2' style="text-align: center;"><label>Quantity</label></div>
                            <div class='col-md-2' style="text-align: center;"><label>Per Unit Price</label></div>
                            <div class='col-md-3' style="text-align: center;"><label>Total Price</label></div>
                            <div class='col-md-1' style="min-height: 30px"></div>
                            <div id="material-input"></div>
                        </div>
                        <div class="form-group">
                            <label for="currency" class="col-sm-4 control-label">Currency</label>
                            <div class="col-sm-8">
                                <select id='currency' class="form-control" name="currency">
                                    <option value="">-- select currency --</option>
                                    <option value="INR">INR</option>
                                    <option value="NPR">NPR</option>
                                    <option value="USD">USD</option>
                                </select>
                                <span class="error_message" id="error-currency" style="color: red" hidden="true">Please select the currency</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank" class="col-sm-4 control-label">Bank</label>
                            <div class="col-sm-8">
                                <select id='bank_id' class="form-control" name="bank_id">
                                    <option value="">-- select bank --</option>
                                    <?php foreach ($banks as $key => $value) {?>
                                        <option value="<?php echo $value['id']?>";"><?php echo $value['bank_name']?></option>
                                    <?php }?>
                                </select>
                                <span class="error_message" id="error-bank_id" style="color: red" hidden="true">Please select the bank</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dd_lc_no" class="col-sm-4 control-label">DD/LC Number</label>
                            <div class="col-sm-8">
                                <input type="text" name="dd_lc_no" class="form-control" id="dd_lc_no" placeholder="DD/LC Number">
                                <span class="error_message" id="error-dd_lc_no" style="color: red" hidden="true">DD/LC number is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="additional_charge" class="col-sm-4 control-label">Additional Charge</label>
                            <div class="col-sm-8">
                                <input type="number" name="additional_charge" class="form-control" id="additional_charge" onchange='totalValue()' onkeyup='totalValue()' placeholder="Additional Charge">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total_lc_quantity" class="col-sm-4 control-label">Total LC Quantity</label>
                            <div class="col-sm-8">
                                <input type="number" readonly name="total_lc_quantity" class="form-control" id="total_lc_quantity" placeholder="Total LC Quantity">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Total LC Quantity is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total_lc_value" class="col-sm-4 control-label">Total LC Value</label>
                            <div class="col-sm-8">
                                <input type="number" readonly name="total_lc_value" class="form-control" id="total_lc_value" placeholder="Total LC Value">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Total LC Value is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank" class="col-sm-4 control-label">Custom Entry Point</label>
                            <div class="col-sm-8">
                                <select id='border' class="form-control" name="border">
                                    <option value="">-- select custom --</option>
                                    <?php foreach ($borders as $key => $value) {?>
                                        <option value="<?php echo $value['id']?>";"><?php echo $value['border']?></option>
                                    <?php }?>
                                </select>
                                <span class="error_message" id="error-border" style="color: red" hidden="true">Please select the border</span>
                            </div>
                        </div>

                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="save" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function(){
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
        $('#date_np').change(function(){
            $('#date_en').val(BS2AD($('#date_np').val()));
        });
        $('#date_en').datetimepicker({
            format: 'YYYY-MM-DD'
        }).on('dp.change', function (e) {
                $('#date_np').val(AD2BS($('#date_en').val()));
        });
        var table = $('#table').DataTable({
            "order" : [ 1, 'desc']
        });
        // table.order( [ 2, 'asc' ] )
         // Add event listener for opening and closing details
        $('#table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
     
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                var i = row.data();
                console.log(i[5]);

                $.post('<?php echo site_url('lc/getLcDetail')?>',{lc:i[5]},function(data){
                    // row.child( formatRow(data) ).show();
                    row.child( data ).show();
                    tr.addClass('shown');
                },'html');
            }
        } );
    })
</script>
<script type="text/javascript">
    /* Formatting function for row details - modify as you need */
    function formatRow ( d ) {
        // `d` is the original data object for the row
        // console.log(d.material);
            row = '<table cellpadding="5" class="table table-bordered table-striped dataTable no-footer" cellspacing="0" border="0">'+
                '<tr>'+
                    '<td>Material</td>'+
                    '<td>Quantity</td>'+
                    '<td>Price</td>'+
                    '<td>Total</td>'+
                '</tr>';
            $.each( d.material, function( key, value ) {
                row +='<tr>'+
                        '<td>'+value.material_name+'</td>'+
                        '<td>'+(parseFloat((value.quantity)).toFixed(3)).toLocaleString()+'</td>'+
                        '<td>'+(parseFloat(value.price)).toLocaleString()+'</td>'+
                        '<td>'+(parseFloat((value.price * value.quantity).toFixed(3))).toLocaleString()+'</td>'+
                    '</tr>';
                
            });
            row += '</table>';
        return row;
    }
</script>

<script>
    $('#add-lc').click(function () {
        $('#form').trigger('reset');
        $('#material-input').html('');
        $('#myModalLabel').text('LC Form');
        $(".error_message").hide();
        $('#material_name').val('');
        $('#lc-dialog').modal();
    });
</script>
<script type="text/javascript">
    $('#dealer_id').change(function(){
        var id = $(this).val();
        // alert(id);
        $('#material-input').empty();
        $('#material-input').empty();
        $.post('<?php echo site_url("lc/getMaterialInput")?>',{id:id,i:0},function(data){
            $('#material-input').append(data);
        },'html');
    });
</script>
<script type="text/javascript">
    function add(id,i){
        $.post('<?php echo site_url("lc/getMaterialInput")?>',{id:id,i:i},function(data){
            $('#btn-'+(--i)).hide();
            $('.error_message').hide();
            $('#btn-delete-'+(i)).show();
            $('#material-input').append(data);
        },'html');
    }
</script>
<script type="text/javascript">
    function removeRow(i){
        // alert(i);
        $('#row-'+i).remove();
        totalValue();
        totalQty();
    }
</script>
<script type="text/javascript">
    $('#save').click(function(){
        var date_en = $('#date_en').val();
        var pi_number = $('#pi_number').val();
        var dealer_id = $('#dealer_id').val();
        var currency = $('#currency').val();
        var bank_id = $('#bank_id').val();
        var dd_lc_no = $('#dd_lc_no').val();
        var border = $('#border').val();
        var i = 1;
        if(date_en == ''){
            i = 0
            $('#error-date').show();
        }else{
            $('#error-date').hide();
        }
        if(pi_number == ''){
            i = 0
            $('#error-pi_number').show();
        }else{
            $('#error-pi_number').hide();
        }
        if(dealer_id == ''){
            i = 0
            $('#error-dealer_id').show();
        }else{
            $('#error-dealer_id').hide();
        }
        if(currency == ''){
            i = 0
            $('#error-currency').show();
        }else{
            $('#error-currency').hide();
        }
        if(bank_id == ''){
            i = 0
            $('#error-bank_id').show();
        }else{
            $('#error-bank_id').hide();
        }
        if(dd_lc_no == ''){
            i = 0
            $('#error-dd_lc_no').show();
        }else{
            $('#error-dd_lc_no').hide();
        }
        if(border == ''){
            i = 0
            $('#error-border').show();
        }else{
            $('#error-border').hide();
        }
        if(i == 1){
            $.post('<?php echo site_url('lc/save')?>',$("#form").serializeArray(),function(data){
                if(data.success){
                    location.reload();
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    $('.edit-lc').click(function(){
        $(".error_message").hide();
        $('#myModalLabel').text('LC Form');
        var id = $(this).val();
        var i = 0;
        // alert(id);
        $.post('<?php echo site_url('lc/getValue')?>',{id:id},function(data){
            $.each(data['lc'],function(index,value){
                // console.log(index +'-'+ value);
                $('#'+index).val(value);
            });
            $('#material-input').empty();
            $.each(data['materials'],function(index,value){
                // console.log(index +'-'+ value);
                var x = '';
                $.each(data['material_options'],function(key,material_option){
                    if(material_option['material_id'] == value['material_id']){
                        x += '<option value="'+material_option['material_id']+'" selected>'+ material_option['material_name']+'</option>';
                    }else{
                        x += '<option value="'+material_option['material_id']+'">'+ material_option['material_name']+'</option>';
                    }
                });
                $('#material-input').append(
                    '<div class="form-group" id="row-'+i+'">'+
                        '<div class="col-md-1"></div>'+
                        '<div class="col-md-3">'+
                            '<select id="material_id" class="form-control" name="material_id['+i+']">'+
                                '<option value="">-- select material --</option>'+
                                x+
                                <?php /* '<?php foreach ($materails as $key => $value) {?>'+
                                    // '<option value="<?php echo $value['material_id']?>"><?php echo $value['material_name']?></option>'+
                                 '<?php }?>'+*/?>
                            '</select>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<input type="number" class="form-control quantity" name="quantity['+i+']" onkeyup="totalQty('+i+')" onchange="totalQty('+i+')" value="'+value.quantity+'" id="qty-'+i+'">'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<input type="number" class="form-control unitprice" name="price['+i+']" onkeyup="total_cost('+i+')" onchange="total_cost('+i+')" value="'+value.price+'" id="unitcost-'+i+'">'+
                        '</div>'+
                        '<div class="col-md-3">'+
                            '<input type="number" class="form-control price" name="total_price['+i+']" onkeyup="unitprice('+i+')" onchange="unitprice('+i+')" value="'+value.price * value.quantity+'" id="totalcost-'+i+'">'+
                        '</div>'+
                        '<div class="col-md-1">'+
                            '<button type="button" id="btn-delete-'+ i +'" class="btn btn-danger" hidden="hidden" onclick="removeRow('+ (i)+')"><i class="fa fa-times"></i></button>'+
                        '</div>'+
                    '</div>'
                    );
                i++;
                //$('#'+index).val(value);
            });
            $('#material-input').append('<button type="button" id="btn-'+(i-1)+'" class="btn pull-right margin" onclick="add('+data.lc["dealer_id"]+','+i+')"><i class="fa fa-plus"></i></button>')
            $('#lc-dialog').modal();
        },'json');
    });
</script>
<script type="text/javascript">
    $('.delete-lc').click(function(){
        var id = $(this).val();
        if(confirm('Are you sure to delete this LC?')){
            $.post('<?php echo site_url("lc/delete_json")?>',{id:id},function(data){
                if(data){
                    location.reload();
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function totalValue() {
        var total = parseFloat($('#additional_charge').val()?$('#additional_charge').val():0);
        // var qty = 0;
        // console.log(total);
        $('.price').each(function(){
            var qty = $(this).val();
            // console.log(qty);
            total += parseFloat((qty)?qty:0);
            console.log($(this).val());

        });
        // alert(total);
        $('#total_lc_value').val(total);
    }
</script>
<script type="text/javascript">
    function total_cost(i){
        var qty = parseFloat($('#qty-'+i).val());
        var unitcost = parseFloat($('#unitcost-'+i).val());
        // alert(unitcost * qty);
        $('#totalcost-'+i).val(qty * unitcost);
        totalValue();
    }
</script>
<script type="text/javascript">
    function unitprice(i){
        var qty = parseFloat($('#qty-'+i).val());
        var total = parseFloat($('#totalcost-'+i).val());
        // alert(unitcost * qty);
        $('#unitcost-'+i).val( parseFloat(total/qty));
        totalValue();
    }
</script><script type="text/javascript">
    function total_cost(i){
        var qty = $('#qty-'+i).val();
        var unitcost = $('#unitcost-'+i).val();
        // alert(unitcost * qty);
        console.log(qty * unitcost);
        console.log(unitcost);
        console.log(qty);
        $('#totalcost-'+i).val(qty * unitcost);
        totalValue();
    }
</script>
<script type="text/javascript">
    function totalQty(i) {
        var total = 0;
        total_cost(i);
        $('.quantity').each(function(){
            var qty = $(this).val();
            total += parseFloat((qty)?qty:0);
            // console.log($(this).val());
        });
        $('#total_lc_quantity').val(total);
        totalValue();
        // alert($('#total_lc_quantity').val());
    }
</script>