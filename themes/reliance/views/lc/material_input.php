<div class="form-group" id='row-<?php echo $i?>'>
	<div class="col-md-1"></div>
	<div class="col-md-3">
	    <select id='material_id' class="form-control" name="material_id[<?php echo $i?>]">
	        <option value="">-- select material --</option>
	        <?php foreach ($materails as $key => $value) {?>
	            <option value="<?php echo $value['material_id']?>"><?php echo $value['material_name']?></option>
	        <?php }?>
	    </select>
	</div>
	<div class="col-md-2">
	    <input type="number" class="form-control quantity" name="quantity[<?php echo $i?>]" onchange="totalQty(<?php echo $i?>)" id="qty-<?php echo $i?>" onkeyup='totalQty(<?php echo $i?>)'>
	</div>
	<div class="col-md-2">
		<input type="number" class="form-control unitprice" name="price[<?php echo $i?>]" onchange='total_cost(<?php echo $i?>)' onkeyup='total_cost(<?php echo $i?>)' id="unitcost-<?php echo $i?>" >
	</div>
	<div class="col-md-3">
		<input type="number" class="form-control price" name="total_price[<?php echo $i?>]" id="totalcost-<?php echo $i?>" onkeyup='unitprice(<?php echo $i?>)' onchange='unitprice(<?php echo $i?>)'>
	</div>
	<div class="col-md-1">
		<button type="button" id="btn-delete-<?php echo $i?>" class="btn btn-danger" hidden='hidden' onclick="removeRow(<?php echo $i?>)"><i class="fa fa-times"></i></button>
	</div>
</div>
		<button type="button" id="btn-<?php echo $i?>" class="btn pull-right margin" onclick="add(<?php echo $id?>,<?php echo ++$i?>)"><i class="fa fa-plus"></i></button>