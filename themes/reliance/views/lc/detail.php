<table cellpadding="5" class="table table-bordered table-striped dataTable no-footer" cellspacing="0" border="0">
    <tr>
        <td>Material</td>
        <td>Quantity</td>
        <td>Price</td>
        <td>Total</td>
    </tr>
    <?php foreach( $material as $key => $value) {?>
        <tr>
            <td><?php echo $value['material_name']?></td>
            <td><?php echo number_format($value['quantity'],3)?></td>
            <td><?php echo number_format($value['price'],3)?></td>
            <td><?php echo number_format($value['price'] * $value['quantity'],3)?></td>
        </tr>
    <?php }?>
</table>