<div id='detail_<?php echo $track["id"]?>'>
<hr>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6"><label>ETA Kolkata</label></div>
            <div class="col-md-6"><label><?php echo $track['eta_kolkata']?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Reached Kolkata</label></div>
            <div class="col-md-6"><label><?php echo ($track['reached_kolkata'])?'Yes':'No'?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Reached Border</label></div>
            <div class="col-md-6"><label><?php echo ($track['reached_nepal'])?'Yes':'No'?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Reached Factory</label></div>
            <div class="col-md-6"><label><?php echo ($track['reached_factory'])?'Yes':'No'?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Date</label></div>
            <div class="col-md-6"><label><?php echo $track['date_en']?></label></div>
            <!-- <div class="col-md-3"><label><?php //echo $track['date_np']?></label></div> -->
        </div>
        <div class="row">
            <div class="col-md-6"><label>Current Status</label></div>
            <div class="col-md-6"><label><?php echo $track['current_status']?></label></div>
        </div>
    </div>
    <div class="col-md-6">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Material Name</label>
                                </div>
                                <div class="col-md-8">
                                    <label>Quantity</label>
                                </div>
                            </div>
                            <?php foreach ($materials as $key => $value) {?>
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label><?php echo $value['material_name']?></label>
                                    </div>
                                    <div class="col-md-8">
                                        <?php echo number_format($value['quantity'],3)?>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-warning" onclick="edit(<?php echo $track['id']?>)"><i class="fa fa-pencil" title="Edit"></i></button>
                            <button class="btn btn-danger" title="Delete" onclick="remove_track(<?php echo $track['id']?>)"><i class="fa fa-trash"></i></button>
                        </div>
</div>
</div>