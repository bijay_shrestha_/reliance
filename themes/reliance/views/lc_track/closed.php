<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
        <!-- lc table -->
            <div class="box">
            	<div class="box-body">
            		<div class="row">
            			<div class="col-md-1"><label>LC No.:</label></div>
            			<div class="col-md-3">
	            			<select id='lc' class="form-control">
	            				<option value="">--select LC No.--</option>
	            				<?php foreach($lcs as $key => $value){?>
	            					<option value="<?php echo $value['id']?>"><?php echo $value['dd_lc_no']?></option>
            					<?php }?>
		            		</select>
	            		</div>
            		</div>
            		<br>
            		<div class='row'>
            			<div class="col-md-6">
	            			<!-- <table id='material-table' class="table table-bordered table-striped">
	            				<tr>
	            					<th>Material</th>
	            					<th>Quantity</th>
	        					</tr>
	        					<tbody id='material-body'>
	        						
	        					</tbody>
	            			</table> -->
            			</div>
            			<div class='col-md-12' id='button'></div>
            		</div>
            	</div><!-- /.box-body -->
            </div><!-- /.box -->
        <!-- dealer table end -->
        <div class="box">
        	<div class="box-body">
            		<table id="lc_list" class="table table-bordered table-striped">
            			<thead>
            				<tr>
	            				<th>LC no.</th>
	            				<!-- <th>PI no.</th> -->
	            				<th>Materials</th>
	            				<!-- <th>Dealer</th> -->
	            				<th>ETA Kolkata</th>
	            				<!-- <th>Reached Kolkata</th>
	            				<th>Reached Nepal</th>
	            				<th>Reached Factory</th> -->
	            				<th>Reached Factory Date</th>
	            				<!-- <th>Current Status</th> -->
	            				<th>Duration (Days)</th>
	            				<th></th>
            				</tr>
            			</thead>
            			<tbody>
        					<?php foreach ($lc_details as $key => $value) {?>
            				<tr>
        						<td><?php echo $value['dd_lc_no']?></td>
        						<!-- <td><?php //echo $value['pi_number']?></td> -->
        						<td><?php
        						foreach ($value['materials'] as $i => $material) {
        								echo '<span style="background-color:#ccc; padding:2px;border-radius: 50px;">'.$material['material_name'].',</span><br>';
        						}
        						?></td>
        						<!-- <td><?php echo $value['dealer_name']?></td> -->
        						<td><?php echo $value['eta_kolkata']?></td>
        						<!-- <td><?php echo ($value['reached_kolkata'])?'yes':'No'?></td>
        						<td><?php echo ($value['reached_nepal'])?'yes':'No'?></td>
        						<td><?php echo ($value['reached_factory'])?'yes':'No'?></td> -->
        						<td><?php echo $value['date_en']?></td>
        						<!-- <td><?php echo $value['current_status']?></td> -->
        						<td><?php 
        							if($value['eta_kolkata'] != '0000-00-00'){
	        							if($value['date_en'] != '0000-00-00'){
			        						$date1=date_create($value['eta_kolkata']);
											$date2=date_create($value['date_en']);
											$diff=date_diff($date1,$date2);
											print_r($diff->days);
	        							}else{
	        								echo 'Factory Not Reached';
	        							}
        							}else{
        								echo 'Local LC or Indian LC';
        							}?>
								</td>
        						<td><button class="btn btn-primary detail-lc" value="<?php echo $value['id']?>">View Detail</button></td>
            				</tr>
        					<?php }?>
            			</tbody>
            		</table>
            	</div><!-- /.box-body -->
        </div>
                    
</section>

<div class="modal fade" id="lc-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="track-form" action="#">
                <div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="row form-group">
								<div class="col-md-4">
									<label>ETA Kolkata</label>
								</div>
								<div class="col-md-8">
									<input type="text" id="eta_kolkata" name="eta_kol" class="form-control">
								</div>
							</div>
							<div class="row form-group" id='div_reached_kolkata' hidden>
								<div class="col-md-4">
									<label>Reached Kolkata</label>
								</div>
								<div class="col-md-8">
									<input type="radio" name="reached_kol" checked value="0" onclick='
									$("#div_reached_nepal").hide();
									$("#div_reached_factory").hide();
									$("#date-factory").hide();
									$("#border_no").prop("checked",true);
									$("#reached_factory_no").prop("checked",true);
									$("#date_en").val("");$("#date_np").val("")
									' id="kolkata_no"> No 
									<input type="radio" name="reached_kol" id="reached_kolkata" value="1" onclick='$("#div_reached_nepal").show()'> Yes
								</div>
							</div>
							<div class="row form-group" id='div_reached_nepal' hidden>
								<div class="col-md-4">
									<label>Reached Border</label>
								</div>
								<div class="col-md-8">
									<input type="radio" name="reached_nepal" checked value="0" onclick='$("#div_reached_factory").hide();$("#date-factory").hide();$("#reached_factory_no").prop("checked",true);$("#date_en").val("");$("#date_np").val("")' id="border_no"> No 
									<input type="radio" name="reached_nepal" value="1" id="reached_nepal" onclick='$("#div_reached_factory").show()'> Yes
								</div>
							</div>
							<div class="row form-group" id='div_reached_factory' hidden>
								<div class="col-md-4">
									<label>Reached Factory</label>
								</div>
								<div class="col-md-8">
									<input type="radio" name="reached_factory" checked value="0" id="reached_factory_no" onclick="$('#date_en').val('');$('#date_np').val('')"> No 
									<input type="radio" name="reached_factory" id="reached_factory" value="1"> Yes
								</div>
							</div>
							<div class="row form-group">
								<div id="date-factory">
									<div class="col-md-4">
										<label>Date</label>
									</div>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="English Date" name="date_en" id="date_en">
										<input type="text" class="form-control" placeholder="Nepali Date" name="date_np" id="date_np">
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-4">
									<label>Current Status</label>
								</div>
								<div class="col-md-8">
									<textarea name="current_status" class="form-control" id="current_status" style="height:100px"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row form-group">
								<div class="col-md-4">
									<label>Material Name</label>
								</div>
								<div class="col-md-8">
									<label>Arrived Quantity</label>
								</div>
							</div>
							<div id="material_list"></div>
							<!-- <?php foreach ($materials as $key => $value) {?>
								<div class="row form-group">
									<div class="col-md-4">
										<label><?php echo $value['material_name']?></label>
									</div>
									<div class="col-md-8">
										<input type="number" name="quantity[]" id="material_qty_<?php echo $value['material_id']?>" class="form-control" value="<?php echo $value['quantity']?>">
										<input type="hidden" name="material_name[]" value="<?php echo $value['material_id']?>">
									</div>
								</div>
							<?php }?> -->
						</div>
					</div>
                </div>
                <!-- <input type="hidden" name="lc_id" id="lc_id" value="<?php echo $lc['id']?>"> -->
                <input type="hidden" name="lc_id" id="lc_id">
                <input type="hidden" name="id" id="id">
                <div class="modal-footer">
                    <button type="button" id="save" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
	$('.detail-lc').click(function(){
		var id = $(this).val();
		$('#lc').val(id);
		$.post('<?php echo site_url('lc_track/getClosedValue')?>',{id:id},function(data){
			// $('#material-body').html('');
			// $.each(data['materials'],function(key,material){
			// 	$('#material-body').append('<tr><td>'+material['material_name']+'</td><td>'+material['quantity']+'</td></tr>')
			// });
			// $('#button').html('<button id="form-button" class="btn btn-primary" value="'+data['lc']["id"]+'">Add Info</button>')
			$('#button').html(data);
		},'html');
	});
</script>
<script type="text/javascript">
	$('#lc').change(function(){
		var id = $(this).val();
		$.post('<?php echo site_url('lc_track/getClosedValue')?>',{id:id},function(data){
			// $('#material-body').html('');
			// $.each(data['materials'],function(key,material){
			// 	$('#material-body').append('<tr><td>'+material['material_name']+'</td><td>'+material['quantity']+'</td></tr>')
			// });
			// $('#button').html('<button id="form-button" class="btn btn-primary" value="'+data['lc']["id"]+'">Add Info</button>')
			$('#button').html(data);
		},'html');
	});
</script>
<script type="text/javascript">
	$(function(){
		$('#lc_list').dataTable({
			"order": [[ 0, 'desc' ]],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
                    // {
                    //     text: 'PDF',
                    //     action: function ( e, dt, node, config ) {
                    //         // alert( 'Button activated' );
                    //         var date = $('#date').val();
                    //         // var date = $('#nepaliDate92').val();
                    //         window.location.href = "<?php echo site_url("home/pdf")?>"+'/1/'+date;
                    //         // <button type='button' id="pdf-btn" class="dt-button buttons-excel buttons-html5">PDF</button>
                    //     }
                    // },
            ],
		});
	});
</script>

<script type="text/javascript">
	$('#save').click(function(){
		$('#save').prop('disabled',true);
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('lc_track/save')?>",
			data: $('#track-form').serializeArray(),
			dataType: "json",
			success: function (data) {
				console.log(data);
				if(data.success){
					// location.reload()
					var type = data.type;
					var id = data.id;
					$.post('<?php echo site_url('lc_track/addDetail')?>', {id:data.id}, function(data1){
						if(type == 1){
							$('#detail').append(data1);
						}else{
							$('#detail_'+id).html(data1);
						}

						 $('#lc-dialog').modal('hide');
					},'html');
				}else{
					alert('Error occured!');
					$('#save').prop('disabled',false);
				}
			}
		});
		// $.post('<?php echo site_url('lc_track/saveall')?>', values, function(data){
		// 	if(data.success){
		// 		// location.reload()
		// 		var type = data.type;
		// 		var id = data.id;
		// 		// $.post('<?php echo site_url('lc_track/addDetail')?>', {id:data.id}, function(data1){
		// 		// 	if(type == 1){
		// 		// 		$('#detail').append(data1);
		// 		// 	}else{
		// 		// 		$('#detail_'+id).html(data1);
		// 		// 	}

		// 			 $('#lc-dialog').modal('hide');
		// 		// },'html');
		// 	}else{
		// 		alert('Error occured!');
		// 	}
		// },'json');
		// $.post(<?php echo site_url()?>);
	});
</script>