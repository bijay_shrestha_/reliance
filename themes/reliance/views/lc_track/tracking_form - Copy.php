<div class="col-md-6">
	<table id="table" class="table table-bordered table-striped">
		<tr>
			<th>Material</th>
			<th>Quantity</th>
		</tr>
		<?php foreach ($materials as $key => $value) {?>
			<tr>
				<td><?php echo $value['material_name']?></td>
				<td><?php echo $value['quantity']?></td>
			</tr>
		<?php }?>
	</table>
</div>
<div class="col-md-12">
	<form id="track-form">
		<div class="row form-group">
			<div class="col-md-2">
				<label>Partial Shipment</label>
			</div>
			<div class="col-md-2">
				<input type="radio" name="partial" checked value="0" id="partial-no"> No 
				<input type="radio" name="partial" value="1" id="partial-yes"> Yes
			</div>
		</div>
		<div id="form-add">
		<?php foreach ($tracks as $key => $value) {?>
			<div class="col-md-12">
				<hr style="background-color: blue">
				<div class="col-md-6">
					<div class="row form-group">
						<div class="col-md-4">
							<label>ETA Kolkata</label>
						</div>
						<div class="col-md-8">
							<input type="text" name="eta_kolkata[]" value="<?php echo $value['eta_kolkata']?>" class="form-control">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<label>Reached Kolkata</label>
						</div>
						<div class="col-md-8">
							<input type="radio" name="reached_kolkata[]" checked value="0"> No 
							<input type="radio" name="reached_kolkata[]" <?php if($value['reached_kolkata']){echo 'checked';}?> value="1"> Yes
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<label>Reached Border</label>
						</div>
						<div class="col-md-8">
							<input type="radio" name="reached_nepal[]" checked value="0"> No 
							<input type="radio" name="reached_nepal[]" <?php if($value['reached_nepal']){echo 'checked';}?> value="1"> Yes
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<label>Nepal Border</label>
						</div>
						<div class="col-md-8">
							<select name='border[]' class="form-control">
								<option value="">--select border--</option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<label>Reached Factory</label>
						</div>
						<div class="col-md-8">
							<input type="radio" name="reached_factory[]" checked value="0" id="reached_factory_no"> No 
							<input type="radio" name="reached_factory[]" <?php if($value['reached_factory']){echo 'checked';}?> value="1" id="reached_factory"> Yes
						</div>
					</div>
					<div class="row form-group">
						<div id="date-factory" <?php if($value['reached_factory'] == 0){echo 'hidden';}?>>
							<div class="col-md-4">
								<label>Date</label>
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="English Date" value="<?php echo $value['date_en']?>" name="date_en[]" id="date_en">
								<input type="text" class="form-control" placeholder="Nepali Date" value="<?php echo $value['date_np']?>" name="date_np[]" id="date_np">
							</div>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<label>Current Status</label>
						</div>
						<div class="col-md-8">
							<textarea name="current_status[]" class="form-control" style="height:100px"><?php echo $value['current_status']?></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row form-group">
						<div class="col-md-4">
							<label>Material Name</label>
						</div>
						<div class="col-md-8">
							<label>Arrived Quantity</label>
						</div>
					</div>
					<?php foreach ($materials as $keys => $values) {?>
						<div class="row form-group">
							<div class="col-md-4">
								<label><?php echo $values['material_name']?></label>
							</div>
							<div class="col-md-8">
								<input type="number" name="quantity[0][]" class="form-control" value="<?php echo $value['material'][$keys]['quantity']?>">
								<input type="hidden" name="material_name[0][]" value="<?php echo $values['material_id']?>">
							</div>
						</div>
					<?php }?>
				</div>
			</div>
			<br>
			<br>
			<hr>
		<?php }?>
		</div>
		<div class="col-md-12">

			<div id="add-button" hidden>
				<button type='button' id='add' class="btn" value="1"><i class="fa fa-plus"></i></button>
			</div>
			<div class="row form-group">
				<div class="col-md-2">
					<label>Close LC</label>
				</div>
				<input type="hidden" name="lc_id" value='<?php echo $lc['id']?>'>
				<div class="col-md-4">
					<input type="checkbox" name="closed" value='1'>
				</div>
			</div>
			<div id="btn-save" class="btn btn-primary">Update</div>
		</div>
		</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
    $('#date_en').datetimepicker({
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            $('#date_np').val(AD2BS($('#date_en').val()));
    });
</script>
<script type="text/javascript">
	$('#reached_factory').click(function(){
		$('#date-factory').show();
	});
</script>
<script type="text/javascript">
	$('#reached_factory_no').click(function(){
		$('#date-factory').hide();
	});
</script>
<script type="text/javascript">
	$('#partial-yes').click(function(){
		$('#add-button').show();
	})
</script>
<script type="text/javascript">
	$('#partial-no').click(function(){
		$('#add-button').hide();
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
		var i = $(this).val()
		$.post('<?php echo site_url("lc_track/addForm")?>',{id:<?php echo $lc['id']?>,i:i},function(data){
			$('#form-add').append(data);
			$('#add').val(++i);
		},'html');
	});
</script>
<script type="text/javascript">
	$('#btn-save').click(function(){
		// location.reload();
		$.post('<?php echo site_url('lc_track/save')?>', $('#track-form').serializeArray(), function(data){
			// alert('here')
			console.log(data.success);
			if(data.success){
				alert('here');
				location.reload()
			}else{
				alert('Error occured!');
			}
		},'json');
	})
</script>