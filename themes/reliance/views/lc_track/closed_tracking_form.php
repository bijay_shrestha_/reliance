<div class="col-md-12">
	<div id="btn-save" class="btn btn-primary margin" value='<?php echo $lc['id']?>' onclick="openLC()">Open Lc</div>
</div>		
<div class="col-md-6">
	<table id="table" class="table table-bordered table-striped">
		<tr>
			<th>Material</th>
			<th>Quantity</th>
		</tr>
		<?php foreach ($materials as $key => $value) {?>
			<tr>
				<td><?php echo $value['material_name']?></td>
				<td><?php echo number_format($value['quantity'],3)?></td>
			</tr>
		<?php }?>
	</table>
</div>
<div class="col-md-12" id="detail">
	<?php foreach ($tracks as $key => $value) {?>
		<div id='detail_<?php echo $value["id"]?>'>
		<hr>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6"><label>ETA Kolkata</label></div>
            <div class="col-md-6"><label><?php echo $value['eta_kolkata']?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Reached Kolkata</label></div>
            <div class="col-md-6"><label><?php echo ($value['reached_kolkata'])?'Yes':'No'?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Reached Border</label></div>
            <div class="col-md-6"><label><?php echo ($value['reached_nepal'])?'Yes':'No'?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Reached Factory</label></div>
            <div class="col-md-6"><label><?php echo ($value['reached_factory'])?'Yes':'No'?></label></div>
        </div>
        <div class="row">
            <div class="col-md-6"><label>Date</label></div>
            <div class="col-md-6"><label><?php echo $value['date_en']?></label></div>
            <!-- <div class="col-md-3"><label><?php //echo $track['date_np']?></label></div> -->
        </div>
        <div class="row">
            <div class="col-md-6"><label>Current Status</label></div>
            <div class="col-md-6"><label><?php echo $value['current_status']?></label></div>
        </div>
    </div>
    <div class="col-md-6">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Material Name</label>
                                </div>
                                <div class="col-md-8">
                                    <label>Arrived Quantity</label>
                                </div>
                            </div>
                            <?php foreach ($materials as $key1 => $values1) {?>
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <label><?php echo $values1['material_name']?></label>
                                    </div>
                                    <div class="col-md-8">
                                        <?php echo $value['material'][$values1['material_id']]?>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-warning" title="Edit" onclick="edit(<?php echo $value['id']?>)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-danger" title="Delete" onclick="remove_track(<?php echo $value['id']?>)"><i class="fa fa-trash"></i></button>
                        </div>
</div>
</div>
	<?php }?>

</div>
<div class="col-md-12">
<hr>
	<!-- <form id="track-form"> -->
			<button type='button' id='add_detail' title="Add tracking detail" class="btn margin" value="1"><i class="fa fa-plus"></i></button>
		
	<!-- </form> -->
</div>


<script type="text/javascript">
	$('#material_list').html('<?php foreach($materials as $key => $value){?>'+
		'<div class="row form-group">'+
			'<div class="col-md-4">'+
				'<label><?php echo $value['material_name']?></label>'+
			'</div>'+
			'<div class="col-md-8">'+
				'<input type="number" name="quantity[]" id="material_qty_<?php echo $value['material_id']?>"'+ 'class="form-control" value="<?php echo $value['quantity']?>">'+
				'<input type="hidden" name="material_name[]" value="<?php echo $value['material_id']?>">'+
			'</div>'+
		'</div>'+
		'<?php }?>');
	$('#lc_id').val(<?php echo $lc['id']?>);
</script>
<script type="text/javascript">
	$('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
    $('#date_en').datetimepicker({
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            $('#date_np').val(AD2BS($('#date_en').val()));
    });
    $('#eta_kolkata').datetimepicker({
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
    	$('#div_reached_kolkata').show();
    //         $('#eta_kolkata_np').val(AD2BS($('#eta_kolkata').val()));
    });
</script>
<script type="text/javascript">
	$('#reached_factory').click(function(){
		$('#date-factory').show();
	});
</script>
<script type="text/javascript">
	$('#reached_factory_no').click(function(){
		$('#date-factory').hide();
	});
</script>
<script type="text/javascript">
	$('#partial-yes').click(function(){
		$('#add-button').show();
	})
</script>
<script type="text/javascript">
	$('#partial-no').click(function(){
		$('#add-button').hide();
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
		var i = $(this).val();
		$('#track-form').reset();
		$.post('<?php echo site_url("lc_track/addForm")?>',{id:<?php echo $lc['id']?>,i:i},function(data){
			$('#form-add').append(data);
			$('#add').val(++i);
		},'html');
	});
</script>
<script type="text/javascript">
	$('#add_detail').click(function(){
		$('#save').prop('disabled',false);
		$('#track-form').trigger("reset");
		$('#id').val('');
		$('#div_reached_kolkata').hide();
		$('#div_reached_nepal').hide();
		$('#div_reached_factory').hide();
		$('#date-factory').hide();
		$('#date-factory').hide();
		$('#myModalLabel').html('Track Form');
		$('#lc-dialog').modal();
	})
</script>
<script type="text/javascript">
	
</script>

<script type="text/javascript">
	function edit(id){
		$('#save').prop('disabled',false);
		$('#track-form').trigger("reset");
		$('#div_reached_kolkata').hide();
		$('#div_reached_nepal').hide();
		$('#div_reached_factory').hide();
		$('#date-factory').hide();
		$('#date-factory').hide();
		// alert(id);
		$.post('<?php echo site_url('lc_track/getData')?>', {id:id}, function(data){
			$('#eta_kolkata').val(data.track.eta_kolkata);
			$('#div_reached_kolkata').show();
			if(data.track.reached_kolkata == 1){
				$('#reached_kolkata').prop('checked',true);
				$('#div_reached_nepal').show();
				if(data.track.reached_nepal == 1){
					$('#reached_nepal').prop('checked',true);
					$('#div_reached_factory').show();
					if(data.track.reached_factory == 1){
						$('#reached_factory').prop('checked',true);
						$('#date-factory').show();
						$('#date_en').val(data.track.date_en);
						$('#date_np').val(data.track.date_np);
					}
				}
			}
			$.each( data.materials, function( key, value ) {
			  	// alert( key + ": " + value.material_id );
			  	$('#material_qty_'+value.material_id).val(value.quantity);
			});
			$('#current_status').text(data.track.current_status);
			$('#id').val(data.track.id);
			$('#myModalLabel').html('Track Form');
			$('#lc-dialog').modal();
		},'json');
	}
</script>
<script type="text/javascript">
	function remove_track(id){
		if(confirm('Do you want to remove record')){
			$.post('<?php echo site_url('lc_track/delete_json')?>',{id:id},function(data){
				console.log(data);
				if(data.success){
					$('#detail_'+id).remove();
				}
			},'json');
		}
	}
</script>
<script type="text/javascript">
	function openLC(){
		var id = <?php echo $lc['id']?>;
		// alert(id);
		$.post('<?php echo site_url('lc/openLC')?>',{id:id},function(data){
			if(data.success){
				location.reload();
			}
		},'json');
	}
</script>
