<div class="col-md-12">
<hr style="background-color: blue">
	<div class="col-md-6">
				<div class="row form-group">
					<div class="col-md-4">
						<label>ETA Kolkata</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="eta_kolkata[<?php echo $i?>]" class="form-control">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-4">
						<label>Reached Kolkata</label>
					</div>
					<div class="col-md-8">
						<input type="radio" name="reached_kolkatakata[<?php echo $i?>]" checked value="0"> No 
						<input type="radio" name="reached_kolkatakata[<?php echo $i?>]" value="1"> Yes
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-4">
						<label>Reached Border</label>
					</div>
					<div class="col-md-8">
						<input type="radio" name="reached_nepal[<?php echo $i?>]" checked value="0"> No 
						<input type="radio" name="reached_nepal[<?php echo $i?>]" value="1"> Yes
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-4">
						<label>Nepal Border</label>
					</div>
					<div class="col-md-8">
						<select name='border[<?php echo $i?>]' class="form-control">
							<option>--select border--</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-4">
						<label>Reached Factory</label>
					</div>
					<div class="col-md-8">
						<input type="radio" name="reached_factory[<?php echo $i?>]" checked value="0" id="reached_factory_no-<?php echo $i?>"> No 
						<input type="radio" name="reached_factory[<?php echo $i?>]" value="1" id="reached_factory-<?php echo $i?>"> Yes
					</div>
				</div>
				<div class="row form-group">
					<div id="date-factory-<?php echo $i?>" hidden>
						<div class="col-md-4">
							<label>Date</label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" placeholder="English Date" name="date_en[<?php echo $i?>]" id="date_en_<?php echo $i?>">
							<input type="text" class="form-control" placeholder="Nepali Date" name="date_np[<?php echo $i?>]" id="date_np_<?php echo $i?>">
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-4">
						<label>Current Status</label>
					</div>
					<div class="col-md-8">
						<textarea name="current_status[<?php echo $i?>]" class="form-control" style="height:100px"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<?php foreach ($materials as $key => $value) {?>
					<div class="row form-group">
						<div class="col-md-4">
							<label><?php echo $value['material_name']?></label>
						</div>
						<div class="col-md-8">
							<input type="number" name="quantity[<?php echo $i?>][]" class="form-control">
							<input type="hidden" name="material_name[<?php echo $i?>][]" value="<?php echo $value['material_id']?>">
						</div>
					</div>
				<?php }?>
			</div>
</div>
<script type="text/javascript">
	$('#date_np_<?php echo $i?>').nepaliDatePicker({
            ndpEnglishInput: 'date_en_<?php echo $i?>'
        });
	$('#date_en_<?php echo $i?>').datetimepicker({
    	format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
            $('#date_np_<?php echo $i?>').val(AD2BS($('#date_en_<?php echo $i?>').val()));
    });
</script>
<script type="text/javascript">
	$('#reached_factory-<?php echo $i?>').click(function(){
		$('#date-factory-<?php echo $i?>').show();
	});
</script>
<script type="text/javascript">
	$('#reached_factory_no-<?php echo $i?>').click(function(){
		$('#date-factory-<?php echo $i?>').hide();
	});
</script>