<div class="box-body">
    <table id="agent_month_list" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Voucher No.</th>
                <th>Party</th>
                <th>Brand Type</th>
                <th>Brand</th>
                <th>Bags</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $key => $value) {?>
            <tr>
                <td><?php echo $value['voucher_no']?></td>
                <td><?php echo $value['party_name']?></td>
                <td><?php echo $value['name']?></td>
                <td><?php echo $value['brand_name']?></td>
                <td><?php echo number_format($value['quantity'])?></td>
                <td><?php echo number_format($value['quantity'] * $value['rate']) ?></td>
            </tr>
        <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>

        </tfoot>
    </table>
</div>


<script type="text/javascript">
        $('#agent_month_list').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
         
                    // Total over this page
                    pageTotal = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 4 ).footer() ).html(
                        pageTotal.toLocaleString('en-US', {
                            minimumFractionDigits: 0
                        })
                    );

                    // Total over this page
                    pageTotal = api
                        .column( 5, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 5 ).footer() ).html(
                        pageTotal.toLocaleString('en-US', {
                            minimumFractionDigits: 0
                        })
                    );

                }
        });
</script>