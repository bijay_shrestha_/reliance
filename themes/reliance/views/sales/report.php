<input type="hidden" name="date_en" value="<?php echo $date_en?>" id="date_en">
<input type="hidden" name="date_np""date_en" id="date_np">
<!-- Main content -->
<section class="content">
    <div class="row">
    <!-- for sales bag -->
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Monthly Sales Bag</h3>
                </div><!-- /.box-header -->
                <div class="box-body" id="monthly_sales_bag">
                </div>
            </div>
        </div>
        <!-- for sales vat -->
        <div class="col-xs-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Monthly Pre Vat OPC and PSC Cement</h3>
                </div><!-- /.box-header -->
                <div class="box-body" id="monthly_vat">
                </div>
            </div>
        </div>
    </div>
    <!-- for sales bag brand -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Brand Wise Monthly Sales Bag</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div>
                        <select class="form-control" id="select-brand-type">
                            <option value="">-- Select type --</option>
                            <?php foreach ($brand_types as $key => $value) {?>
                                <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                            <?php }?>
                        </select>
                    </div>
                    <span id="monthly_sales_bag_brand"></span>
                </div>
            </div>
        </div>
    </div>
    <!-- for agent sales bag -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Agent/Monthly Wise Monthly Sales Bag</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-6">
                        <select class="form-control" id="select-monthagent_month">
                            <option value="">-- Select Month --</option>
                            <option value="All">All</option>
                            <?php foreach ($this->month_options as $key => $value) {?>
                                <option value="<?php echo $key?>"><?php echo $value?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select class="form-control" id="select-monthagent_agent">
                            <option value="">-- Select Agent --</option>
                            <?php foreach ($agents as $key => $value) {?>
                                <option value="<?php echo $value['id']?>"><?php echo $value['party']?></option>
                            <?php }?>
                        </select>
                    </div>
                    <span id="monthly_agent_sales_bag"></span>
                </div>
            </div>
        </div>
    </div>
    <!-- for party sales bag -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Party/Monthly Wise Monthly Sales Bag</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-6">
                        <select class="form-control" id="select-monthparty_month">
                            <option value="">-- Select Month --</option>
                            <option value="All">All</option>
                            <?php foreach ($this->month_options as $key => $value) {?>
                                <option value="<?php echo $key?>"><?php echo $value?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select class="form-control" id="select-monthparty_agent">
                            <option value="">-- Select Party --</option>
                            <?php foreach ($parties as $key => $value) {?>
                                <option value="<?php echo $value['id']?>"><?php echo $value['party_name']?></option>
                            <?php }?>
                        </select>
                    </div>
                    <span id="monthly_party_sales_bag"></span>
                </div>
            </div>
        </div>
    </div>
</section>



<script type="text/javascript">
    $(function(){
        $('#date_np').val(AD2BS($('#date_en').val()));

        var date_np = $('#date_np').val();

        $.post('<?php echo site_url('sales_register/getBagReport')?>',{date_np:date_np},function(data){
            $('#monthly_sales_bag').html(data);
        },'html');

        $.post('<?php echo site_url('sales_register/getVatReport')?>',{date_np:date_np},function(data){
            $('#monthly_vat').html(data);
        },'html');

    });
</script>
<script type="text/javascript">
    $('#select-brand-type').change(function(){
        var date_np = $('#date_np').val();
        var brand_type = $(this).val();
        $.post('<?php echo site_url("sales_register/getBrandBagReport")?>',{date_np:date_np, brand_type:brand_type},function(data){
            $('#monthly_sales_bag_brand').html(data);
        },'html');
    });
</script>
<script type="text/javascript">
    $('#select-monthagent_month, #select-monthagent_agent').change(function(){
        var month = $('#select-monthagent_month').val();
        var agent_id = $('#select-monthagent_agent').val();
        if(month != "" && agent_id != ""){
            $.post("<?php echo site_url('sales_register/month_agent_report')?>",{month:month,agent_id:agent_id},function(data){
                $('#monthly_agent_sales_bag').html(data);
            },'html');
        }
    });
</script>
<script type="text/javascript">
    $('#select-monthparty_month, #select-monthparty_agent').change(function(){
        var month = $('#select-monthparty_month').val();
        var party_id = $('#select-monthparty_agent').val();
        if(month != "" && party_id != ""){
            $.post("<?php echo site_url('sales_register/month_party_report')?>",{month:month,party_id:party_id},function(data){
                $('#monthly_party_sales_bag').html(data);
            },'html');
        }
    });
</script>
