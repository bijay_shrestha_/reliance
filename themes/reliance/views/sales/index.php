<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <button class="btn btn-primary" id="add-btn">Add</button>
                    <table id="sale_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Voucher No.</th>
                                <th>Nepali Date</th>
                                <th>English Date</th>
                                <th>Agent Name</th>
                                <th>Party Name</th>
                                <th>Truck Number</th>
                                <th>Type</th>
                                <th>Rate</th>
                                <th>Quantity</th>
                                <th>Domestic Sales</th>
                                <th>Payment</th>
                                <th>Payment Left</th>
                                <th>Days Outstanding</th>
                                <th>Brand Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $value) {?>
                            <tr>
                                <td><?php echo $value['voucher_no']?></td>
                                <td><?php echo $value['date_np']?></td>
                                <td><?php echo $value['date_en']?></td>
                                <td><?php echo $value['agent_name']?></td>
                                <td><?php echo $value['party_name']?></td>
                                <td><?php echo $value['truck_no']?></td>
                                <td><?php echo $value['name']?></td>
                                <td><?php echo number_format($value['rate'],2)?></td>
                                <td><?php echo number_format($value['quantity'])?></td>
                                <td><?php echo number_format($value['domestic_sales'],2)?></td>
                                <td><?php echo number_format($value['payment'],2)?></td>
                                <td><?php echo number_format($value['payment_left'],2)?></td>
                                <td><?php echo number_format($value['days_outstanding'])?></td>
                                <td><?php echo $value['brand_name']?></td>
                                <td>
                                    <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)" title="Edit"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger" onclick="delete_sale(<?php echo ($value['id'])?>)" title='Delete'><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        <tfoot>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add sale -->
<div class="modal fade" id="sale-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="sale" class="col-sm-4 control-label">Date(Nepali)</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date(Nepali)">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="sale Name">
                                <div id="error-date_np" hidden style="color: red" class="error-div">Date is required</div>
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="voucher_no" class="col-sm-4 control-label">Voucher Number</label>
                            <div class="col-sm-8">
                                <input type="text" name="voucher_no" class="form-control" id="voucher_no" placeholder="Voucher Number">
                                <div id="error-voucher_no" hidden style="color: red" class="error-div">Voucher Number is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="payment_type" class="col-sm-4 control-label">Agent</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='agent_id' id="agent_id" onchange="getParty_name()">
                                    <option value=''>-- select Agent --</option>
                                <?php foreach ($assigned_parties as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['party']?></option>
                                <?php }?>
                                </select>
                                <div id="error-agent_id" hidden style="color: red" class="error-div">Agent is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="party" class="col-sm-4 control-label">Party</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='party_id' id="party_id">
                                    <option value=''>-- select party --</option>
                                <?php foreach ($parties as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['party_name']?></option>
                                <?php }?>
                                </select>
                                <div id="error-party_id" hidden style="color: red" class="error-div">Party is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="truck_no" class="col-sm-4 control-label">Truck Number</label>
                            <div class="col-sm-8">
                                <input type="text" name="truck_no" class="form-control" id="truck_no" placeholder="Truck Number">
                                <div id="error-truck_no" hidden style="color: red" class="error-div">Truck Number is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="types" class="col-sm-4 control-label">Sale Type</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='type' id="type">
                                    <option value=''>-- select brand type --</option>
                                <?php foreach ($types as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                                <?php }?>
                                </select>
                                <div id="error-type" hidden style="color: red" class="error-div">Type is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brands" class="col-sm-4 control-label">Brand</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='brand_id' id="brand_id">
                                    <option value=''>-- select brand --</option>
                                <?php foreach ($brands as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['brand_name']?></option>
                                <?php }?>
                                </select>
                                <div id="error-type" hidden style="color: red" class="error-div">Brand is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="col-sm-4 control-label">Quantity</label>
                            <div class="col-sm-8">
                                <input type="number" name="quantity" class="form-control" id="quantity" placeholder="Quantity" onchange="calculate_total()" onkeyup="calculate_total()">
                                <div id="error-quantity" hidden style="color: red" class="error-div">Quantity is required</div>
                            </div>
                        </div>
                        <!-- primary rate -->
                        <div class="form-group">
                            <label for="rate" class="col-sm-4 control-label">Rate</label>
                            <div class="col-sm-8">
                                <input type="number" name="primary_rate" class="form-control" id="primary_rate" placeholder="Original Rate" onchange="calculate_total()" onkeyup="calculate_total()">
                                <div id="error-rate" hidden style="color: red" class="error-div">Rate is required</div>
                            </div>
                        </div>
                        <!-- discount -->
                        <div class="form-group">
                            <label for="rate" class="col-sm-4 control-label">Discount</label>
                            <div class="col-sm-8">
                                <input type="number" name="discount_amount" class="form-control" id="discount_amount" placeholder="Discount" onchange="calculate_total()" onkeyup="calculate_total()" value="20">
                                <!-- <div id="error-rate" hidden style="color: red" class="error-div">Rate is required</div> -->
                            </div>
                        </div>
                        <!-- discount rate -->
                        <div class="form-group">
                            <label for="rate" class="col-sm-4 control-label">Discounted Rate</label>
                            <div class="col-sm-8">
                                <input type="number" name="rate" class="form-control" id="rate" placeholder="Discounted Rate" readonly>
                                <!-- <div id="error-rate" hidden style="color: red" class="error-div">Rate is required</div> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="domestic_sales" class="col-sm-4 control-label">Domestic Sales</label>
                            <div class="col-sm-8">
                                <input type="number" name="domestic_sales" class="form-control" id="domestic_sales" placeholder="Domestic Sales" readonly>
                            </div>
                        </div>

                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
        $('#sale_list').DataTable({
            "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            "columnDefs": [
                {
                    "targets": [ 13 ],
                    "visible": false,
                    "searchable": false
                },
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
         
                    // Total over all pages
                    total = api
                        .column( 10 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 10 ).footer() ).html(
                        total.toLocaleString('en-US', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                        })
                    );

                    // Total over all pages
                    total = api
                        .column( 9 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 9 ).footer() ).html(
                        total.toLocaleString('en-US', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                        })
                    );

                    // Total over all pages
                    total = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 8 ).footer() ).html(
                        total.toLocaleString('en-US', {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                        })
                    );
                },
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('.error-div').hide();
        $('input').val('');
        $('#discount_amount').val('20');
        $('option').attr('selected', false);
        $('#sale-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var f = 0;
        if($('#date_np').val() == ''){
            $('#error-date_np').show();
            f = 1;
        }else{
            $('#error-date_np').hide();
        }

        if($('#agent_id').val() == ''){
            $('#error-agent_id').show();
        }else{
            $('#error-agent_id').hide();
        }

        if($('#party_id').val() == ''){
            $('#error-party_id').show();
        }else{
            $('#error-party_id').hide();
        }

        if($('#truck_no').val() == ''){
            $('#error-truck_no').show();
        }else{
            $('#error-truck_no').hide();
        }

        if($('#type').val() == ''){
            $('#error-type').show();
        }else{
            $('#error-type').hide();
        }

        if($('#quantity').val() == ''){
            $('#error-quantity').show();
        }else{
            $('#error-quantity').hide();
        }

        if($('#rate').val() == ''){
            $('#error-rate').show();
            f = 1;
        }else{
            $('#error-rate').hide();
        }

        if($('#voucher_no').val() == ''){
            $('#error-voucher_no').show();
            f = 1;
        }else{
            $('#error-voucher_no').hide();
        }

        if(f == 0){
            $.post('<?php echo site_url("sales_register/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.error-div').hide();
        $.post('<?php echo site_url("sales_register/get_value")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });
            $('#sale-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_sale(id){
        $.post('<?php echo site_url("sales_register/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>
<script type="text/javascript">
    function calculate_total(){
        var primary_rate = parseFloat($('#primary_rate').val());
        var discount = (parseFloat($('#discount_amount').val()))?parseFloat($('#discount_amount').val()):0;
        var quantity = parseFloat($('#quantity').val());
        var rate = primary_rate - discount;

        // alert(rate);
        // alert(quantity)
        var total = rate * quantity;
        var domestic_sales = (total + (9 * quantity)) * 1.13;

        $('#rate').val(rate);
        $('#domestic_sales').val(domestic_sales.toFixed(2));
        // $('#payment_left').val(total);
    }
</script>
<script type="text/javascript">
    function getParty_name(){
        var id = $('#agent_id').val();
        $.post('<?php echo site_url("payment/getParty_name")?>',{id:id},function(data){
            $('#party_id').empty();
            $('#party_id').append($('<option>', { 
                    value: '',
                    text : '-- select party --' 
                }));
            $.each(data, function( index, value ) {
                $('#party_id').append($('<option>', { 
                    value: value.party_id,
                    text : value.party_name 
                }));
            });
        },'json');
    }
</script>
<script type="text/javascript">
    $('#type, #party_id, #assigned_party_id').change(function(){
        // alert();
        type_id = $('#type').val();
        party_id = $('#party_id').val();
        assigned_party_id = $('#assigned_party_id').val();
        $.post('<?php echo site_url('sales_register/getAssignedBrand')?>',{type_id:type_id, party_id:party_id, assigned_party_id:assigned_party_id},function(data){
            $('#brand_id').empty();
            $.each(data.brands, function(key, value) {
                $('#brand_id').append($("<option/>", {
                    value: value.id,
                    text: value.brand_name,
                    selected: (value.id == data.selected_brand.brand_id)?true:false
                }));
            });
        },'json');
    })
</script>