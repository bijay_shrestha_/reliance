<div class="box-body">
    <table id="vat_list" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Month</th>
                <?php foreach ($types as $key => $value){?>
                    <th><?php echo $value['name']?></th>
                <?php }?>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $key => $value) {?>
            <tr>
                <td><?php echo $this->months[$key-1]?></td>
                <?php 
                $total = 0;
                foreach ($types as $index => $val) {?>
                    <td>
                    <?php if(array_key_exists($val['name'], $value)){
                        echo number_format($value[$val['name']],2);
                    }else{
                        echo 0;
                    }?>
                    </td>
                    <?php if(array_key_exists($val['name'], $value)){
                        $total += $value[$val['name']];
                    }
                }?>
                <td><?php echo number_format($total)?></td>
            </tr>
        <?php }?>

        <?php /*"foreach ($this->months as $key => $value) {?>
        <?php 
        $total = 0;
        $month_index = ($key < 9)?($key + 3):($key - 9);
        ?>
            <tr>
                <td><?php echo $this->months[$month_index]?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php }*/?>
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>

        </tfoot>
    </table>
</div>


<script type="text/javascript">
        $('#vat_list').DataTable({
            "paging":   false,
            "ordering": false,
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
         
                    // Total over this page
                    pageTotal = api
                        .column( 1, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 1 ).footer() ).html(
                        pageTotal.toLocaleString('en-US', {
                            minimumFractionDigits: 2
                        })
                    );

                    // Total over this page
                    pageTotal = api
                        .column( 2, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 2 ).footer() ).html(
                        pageTotal.toLocaleString('en-US', {
                            minimumFractionDigits: 2
                        })
                    );

                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 3 ).footer() ).html(
                        pageTotal.toLocaleString('en-US', {
                            minimumFractionDigits: 2
                        })
                    );

                }
        });
</script>