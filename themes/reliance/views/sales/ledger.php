<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo ucwords($type)?> Ledger</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <select class="form-control" id="party_id" name="party_id">
                                <option value="">-- Select party --</option>
                                <?php foreach ($options as $key => $value) {?>
                                    <?php if($type == 'party'){?>
                                        <option value="<?php echo $value['id']?>"><?php echo $value['party_name']?></option>
                                    <?php }else if($type = 'agent'){?>
                                        <option value="<?php echo $value['id']?>"><?php echo $value['party']?></option>
                                    <?php }?>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-12" id="ledger_detail">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });
    });
</script>
<script type="text/javascript">
    $('#party_id, #month').change(function(){
        var party_id = $('#party_id').val();
        var month = $('#month').val();

        if(party_id != ''){
            $.post('<?php echo site_url('sales_register/getLedgerRecord/'.$type)?>',{party_id:party_id, month:month},function(data){
                $('#ledger_detail').html(data);
            },'html');
        }
    });
</script>