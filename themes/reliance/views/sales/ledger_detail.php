<div class="row">
    <div class="col-md-6">
        <select id="month" class="form-control">
            <option value="">All Month</option>
            <?php 
                foreach ($this->month_options as $key => $value) {
            ?>
                    <option value="<?php echo $value?>"><?php echo $value?></option>
            <?php 
                }
            ?>
        </select>
    </div>
</div>
<div class="box-body table-responsive col-md-12">
    <table id="ledger_detail_list" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Voucher Number</th>
                <th>Date</th>
                <th>Month</th>
                <th>Party Name</th>
                <th>Truck No.</th>
                <th>Brand</th>
                <th>Type</th>
                <th>Rate</th>
                <th>Quantity</th>
                <th>Domestic Sales</th>
                <th>Payment</th>
                <th>Payment Left</th>
                <th>Outstanding Day</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $key => $value) {?>
            <tr>
                <td><?php echo $value['voucher_no']?></td>
                <td><?php echo $value['date_np']?></td>
                <td><?php echo $this->months[$value['month_np']-1]?></td>
                <td><?php echo $value['party']?></td>
                <td><?php echo $value['truck_no']?></td>
                <td><?php echo $value['brand_name']?></td>
                <td><?php echo $value['type_name']?></td>
                <td><?php echo number_format($value['rate'],2)?></td>
                <td><?php echo number_format($value['quantity'])?></td>
                <td><?php echo number_format($value['domestic_sales'],2)?></td>
                <td><?php echo number_format($value['payment'],2)?></td>
                <td><?php echo number_format($value['payment_left'],2)?></td>
                <td><?php echo number_format($value['days_outstanding'])?></td>
            </tr>
        <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <th style="text-align:right">Total:</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>

<script type="text/javascript">
    var table = $('#ledger_detail_list').DataTable({
        // "order":[[0, "desc"]],
        "pageLength": 50,
        "order": [[ 1, 'asc' ]],
        dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],

        "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;
        // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 9 ).footer() ).html(
                // total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                total.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                })
            );

            // Total over all pages
            total = api
                .column( 10 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 10 ).footer() ).html(
                total.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                })
            );

            // Total over all pages
            // total = api
            //     .column( 7 )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );
 
            // // Update footer
            // $( api.column( 7 ).footer() ).html(
            //     total.toLocaleString('en-US', {
            //         minimumFractionDigits: 2
            //     })
            // );

            // Total over all pages
            total = api
                .column( 11 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 11 ).footer() ).html(
                total.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                })
            );
        }
    });

    $('#month').change( function() {
        table.draw();
    } );

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var month = $('#month').val();
            var table_month = data[2] || ""; // use data for the age column

            if(month != ''){
                if ( month == table_month )
                {
                    return true;
                }
                return false;
            }else{
                return true;
            }
        
        }
    );
</script>