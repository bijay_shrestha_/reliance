<!-- <pre>
    <?php print_r($rows);?>
</pre> -->

<div class="row">
    <div class="col-md-6">
        <select id="month" class="form-control">
            <option value="">All Month</option>
            <?php 
                foreach ($this->month_options as $key => $value) {
            ?>
                    <option value="<?php echo $value?>"><?php echo $value?></option>
            <?php 
                }
            ?>
        </select>
    </div>
</div>
<div class="box-body table-responsive">
    <table id="ledger_detail_list" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>Date(Nepali)</th>
                <th>Date(English)</th>
                <th>Month</th>
                <th>Party Name</th>
                <th>Bank</th>
                <th>Note(Voucher No.)</th>
                <th>Truck No.</th>
                <th>Brand</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Bill Amount</th>
                <th>Payment Amount</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $key => $value) {?>
            <?php if($value['payment'] == 0){?>
                <tr>
                    <td><?php echo $value['date_np']?></td>
                    <td><?php echo $value['date_en']?></td>
                    <td><?php echo (array_key_exists((int)$value['month_np'], $this->month_options))?$this->month_options[(int)$value['month_np']]:''?></td>
                    <td><?php echo $value['party']?></td>
                    <td></td> 
                    <td><?php echo $value['voucher_no']?></td>
                    <td><?php echo $value['truck_no']?></td>
                    <td><?php echo $value['brand_name']?></td>
                    <td><?php echo $value['type_name']?></td>
                    <td><?php echo $value['quantity']?></td>
                    <td><?php echo $value['rate']?></td>
                    <td><?php echo number_format($value['domestic_sales'],2)?></td>
                    <td><?php echo number_format($value['payment'],2)?></td>
                </tr>
            <?php }else{?>
                <tr>
                    <td><?php echo $value['date_np']?></td>
                    <td><?php echo $value['date_en']?></td>
                    <td><?php echo (array_key_exists((int)$value['month_np'], $this->month_options))?$this->month_options[(int)$value['month_np']]:''?></td>
                    <td><?php echo $value['party']?></td> 
                    <td><?php echo (array_key_exists('bank_name', $value))?$value['bank_name']:''?></td>
                    <td><?php echo $value['voucher_no']?></td>
                    <td><?php echo (array_key_exists('truck_no', $value))?$value['truck_no']:''?></td>
                    <td><?php echo $value['brand_name']?></td>
                    <td><?php echo $value['type_name']?></td>
                    <td><?php echo $value['quantity']?></td>
                    <td><?php echo $value['rate']?></td>
                    <td><?php echo number_format($value['domestic_sales'],2)?></td>
                    <td><?php echo number_format($value['payment'],2)?></td>
                </tr>
            <?php }?>
        <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <th style="text-align:right">Total:</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>
<div class="row form-group" style="margin-top: 50px">
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-flag-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"><label>Bill Amount</label></span>
              <span class="info-box-number" id="bill_amount">--</span>
            </div><!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"><label>Payment</label></span>
              <span class="info-box-number" id="payment">--</span>
            </div><!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-flag-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"><label>Balance</label></span>
              <span class="info-box-number" id="balance">--</span>
            </div><!-- /.info-box-content -->
        </div>
    </div>
</div>
<script type="text/javascript">
    var balance = 0;

    var table = $('#ledger_detail_list').DataTable({
        // "order":[[0, "desc"]],
        "pageLength": 50,
        "ordering": false,
        dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],

        "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;
        // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 11 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 11 ).footer() ).html(
                (total/2).toLocaleString('en-US', {
                    minimumFractionDigits: 2
                })
            );
            $('#bill_amount').html((total/2).toLocaleString('en-US', {
                    minimumFractionDigits: 2
                }));

            balance = 0;
            balance += total;

            // Total over all pages
            total = api
                .column( 12 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 12 ).footer() ).html(
                (total/2).toLocaleString('en-US', {
                    minimumFractionDigits: 2
                })
            );
            $('#payment').html((total/2).toLocaleString('en-US', {
                    minimumFractionDigits: 2
                }));

            balance -= total;
            balance = balance/2;

            $('#balance').html('');
            $('#balance').html(balance.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                }));
        },

        // row grouping
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last = null;
 
            api.column(3, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="13"><strong>'+group+'</strong></td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    });

    $('#month').change( function() {
        table.draw();
    } );

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var month = $('#month').val();
            var table_month = data[2] || ""; // use data for the age column

            if(month != ''){
                if ( month == table_month )
                {
                    return true;
                }
                return false;
            }else{
                return true;
            }
        
        }
    );
</script>