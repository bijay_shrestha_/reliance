<div class="box-body table-responsive">
    <table id="ledger_detail_list" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Date(Nepali)</th>
                <th>Date(English)</th>
                <th>Party Name</th>
                <th>Bank</th>
                <th>Note(Voucher No.)</th>
                <th>Bill Amount</th>
                <th>Payment Amount</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $key => $value) {?>
            <?php if($value['payment'] == 0){?>
                <tr>
                    <td><?php echo $value['date_np']?></td>
                    <td><?php echo $value['date_en']?></td>
                    <td><?php echo $value['party']?></td> 
                    <td><?php echo $value['bank_name']?></td>
                    <td><?php echo $value['voucher_no']?></td>
                    <td><?php echo number_format($value['domestic_sales'],2)?></td>
                    <td><?php echo number_format($value['payment'],2)?></td>
                </tr>
            <?php }else{?>
                <tr>
                    <td><?php echo $value['date_np']?></td>
                    <td><?php echo $value['date_en']?></td>
                    <td><?php echo $value['party']?></td> 
                    <td><?php echo $value['bank_name']?></td>
                    <td><?php echo $value['voucher_no']?></td>
                    <td><?php echo number_format($value['domestic_sales'],2)?></td>
                    <td><?php echo number_format($value['payment'],2)?></td>
                </tr>
            <?php }?>
        <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <th style="text-align:right">Total:</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-flag-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"><label>Bill Amount</label></span>
              <span class="info-box-number" id="bill_amount">--</span>
            </div><!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"><label>Payment</label></span>
              <span class="info-box-number" id="payment">--</span>
            </div><!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-flag-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"><label>Balance</label></span>
              <span class="info-box-number" id="balance">--</span>
            </div><!-- /.info-box-content -->
        </div>
    </div>
</div>

<script type="text/javascript">
    var balance = 0;

    $('#ledger_detail_list').DataTable({
        // "order":[[0, "desc"]],
        "pageLength": 50,
        "order": [[ 1, 'asc' ]],
        dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],

        "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;
        // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                total.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                })
            );
            $('#bill_amount').html(total.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                }));

            balance = 0;
            balance += total;

            // Total over all pages
            total = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 6 ).footer() ).html(
                total.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                })
            );
            $('#payment').html(total.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                }));

            balance -= total;

            $('#balance').html('');
            $('#balance').html(balance.toLocaleString('en-US', {
                    minimumFractionDigits: 2
                }));
        }
    });
</script>