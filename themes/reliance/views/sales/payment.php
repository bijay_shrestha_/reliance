<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <button class="btn btn-primary" id="add-btn">Add</button>
                    <table id="payment_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Party</th>
                                <th>Agent</th>
                                <th>Bank</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $value) {?>
                            <tr>
                                <td><?php echo $value['date_np']?></td>
                                <td><?php echo $value['party_name']?></td>
                                <td><?php echo $value['party']?></td>
                                <td><?php echo $value['bank_name']?></td>
                                <td><?php echo number_format($value['payment'],2)?></td>
                                <td>
                                    <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)" title="Edit"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger" onclick="delete_payment(<?php echo ($value['id'])?>)" title="Delete"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add payment -->
<div class="modal fade" id="payment-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="sale" class="col-sm-4 control-label">Date(Nepali)</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date(Nepali)">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="sale Name">
                                <div id="error-date_np" hidden style="color: red" class="error-div">Date is required</div>
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="payment_type" class="col-sm-4 control-label">Agent</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='assigned_party_id' id="assigned_party_id" onchange="getParty_name()">
                                    <option value="">-- select agent</option>
                                <?php foreach ($assigned_parties as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['party']?></option>
                                <?php }?>
                                </select>
                                <div id="error-agent_id" hidden style="color: red" class="error-div">Agent is required</div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="payment_type" class="col-sm-4 control-label">Payment From</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='payment_from' id="payment_from" onchange="getParty_name()">
                                    <option value="">-- select place --</option>
                                // <?php foreach ($parties as $key => $value) {?>
                                    <option>Kathmandu</option>
                                    <option>Birjung</option>
                                <!-- <?php }?> -->
                                </select>
                                <div id="error-payment_from" hidden style="color: red" class="error-div">Payment from is required</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="payment_type" class="col-sm-4 control-label">Party</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='party_id' id="party_id">
                                    <option value="">-- select party --</option>
                                <?php foreach ($parties as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['party_name']?></option>
                                <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="payment_type" class="col-sm-4 control-label">Bank</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='bank_id' id="bank_id">
                                    <option value="">-- select bank --</option>
                                <?php foreach ($banks as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['bank_name']?></option>
                                <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="payment" class="col-sm-4 control-label">Payment Amount</label>
                            <div class="col-sm-8">
                                <input type="number" name="payment" class="form-control" id="payment" placeholder="payment Amount">
                                <div id="error-payment" hidden style="color: red" class="error-div">Payment required</div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
        $('#payment_list').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $(".error-div").hide();
        $('input').val('');
        $('option').attr('selected', false);
        $('#payment-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var f = 0;
        if($('#date_np').val() == ''){
            f = 1;
            $('#error-date_np').show();
        }else{
            $('#error-date_np').hide();
        }
        if($('#payment').val() == ''){
            f = 1;
            $('#error-payment').show();
        }else{
            $('#error-payment').hide();
        }
        if($('#assigned_party_id').val() == ''){
            f = 1;
            $('#error-agent_id').show();
        }else{
            $('#error-agent_id').hide();
        }
        if($('#assigned_payment_from').val() == ''){
            f = 1;
            $('#error-payment_from').show();
        }else{
            $('#error-payment_from').hide();
        }

        if(f == 0){
            $.post('<?php echo site_url("payment/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
         $(".error-div").hide();
        $.post('<?php echo site_url("payment/get_value")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });
            $('#payment-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_payment(id){
        $.post('<?php echo site_url("payment/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>
<script type="text/javascript">
    function getParty_name(){
        var id = $('#assigned_party_id').val();
        $('#party_id').empty();
        $('#party_id').append($('<option>', { 
                value: "",
                text : '-- select party --' 
            }));
        if($('#payment_from').val() != 'Kathmandu'){
            $.post('<?php echo site_url("payment/getParty_name")?>',{id:id},function(data){
                $.each(data, function( index, value ) {
                    $('#party_id').append($('<option>', { 
                        value: value.id,
                        text : value.party_name 
                    }));
                });
            },'json');
        }
    }
</script>