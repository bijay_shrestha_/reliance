<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<button class='btn btn-primary margin' id='add'>Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="inspection-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date</th>
				                    <th>Inspection Total</th>
                                    <th>Action</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo number_format($row['inspection_total'],3)?></td>
                                        <td><button class='btn btn-warning' onclick='edit(<?php echo $row['id']?>)'>Edit</button> <button class='btn btn-danger' onclick='delete_record(<?php echo $row['id']?>)'>Delete</button></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of diesel consumption-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="inspection-form" method="post">
      	<div class="modal-body">
                        
            <table class="table table-striped" cellpadding="3px" >
                <tr>
                    <td><label>Date</label></td>
                    <td id='date-div'>
                        <input type="text" id="date-np" name="date_np" class='form-control'>
                        <span id="error-date" hidden>Date is required</span>
                    </td>
                    <input type="hidden" id="date-en" name="date_en" class='form-control'>
                </tr>
                <tr>
                    <td><label>Scrap</label></td>
                    <td id="inspection_total_div">
                        <input type="number" id="inspection_total" name="inspection_total" class='form-control amount-div'>
                        <span id="error-inspection_total" hidden>Inspection amount is required</span>
                    </td>
                </tr>

                    <?php /*<input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>">*/?>
                
                    <input type="hidden" id="id" name="id"/>
                </table>
        
        </div>
    
        <div class="modal-footer">
            <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#date-np').nepaliDatePicker({
			// ndpEnglishInput: 'date_en',
			// onChange: function(){
			// 	var date = $('#date-np').val();
			// 	var date_en = $('#date-ep').val();
			// 	$.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
			// 		$('#display-div').html(data);
			// 	},'html');
			// }
		});
		$('#inspection-table').dataTable({
            order: [[ 0, 'desc' ]],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
        $('#error-date, #error-inspection_total').hide();
        $('#date-div, #inspection_total_div').removeClass('has-error')
		$('#myModalLabel').text('Inspection Record Form');
                $('#date-np').val('');
                $('#date-en').val('');
                $('#inspection_total').val('');
                $('#id').val('');
                
		$('#form-dialog').modal();
	});
</script>
<script type="text/javascript">
	$('#save').click(function(){
        var date= $('#date-np').val();
        var inspection_total = $('#inspection_total').val();
        var i = 0;
        if(date == ''){
            $('#date-div').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date-div').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(inspection_total == ''){
            $('#inspection_total_div').addClass('has-error');
            $('#error-inspection_total').show();
        }else{
            $('#inspection_total_div').removeClass('has-error');
            $('#error-inspection_total').hide();
            i++;
        }
        if(i == 2){
    		$.post("<?php echo site_url('inspection/save')?>",$("#inspection-form").serializeArray(),function($data){
                if($data.success){
                    alert('Data saved');
                    location.reload();
                }
            },'json');
        }
	});
</script>
<script type="text/javascript">
    $('#scrap, #segregation_waste, #burnt_scrap, #final_chipps, #sieved_dust').keyup(function(){
        var scrap = $('#scrap').val();
        var segregation_waste = $('#segregation_waste').val();
        var burnt_scrap = $('#burnt_scrap').val();
        var final_chipps = $('#final_chipps').val();
        var sieved_dust = $('#sieved_dust').val();

        $('#foaming_scrap').val(scrap-segregation_waste-burnt_scrap-final_chipps-sieved_dust);
    });
</script>
<script type="text/javascript">
    function edit(id){
        $('#myModalLabel').text('Inspection Record Form');
        $('#error-date, #error-inspection_total').hide();
        $('#date-div, #inspection_total_div').removeClass('has-error')
        $.post('<?php echo site_url('inspection/getData')?>',{id:id},function(data){
            $('#date-np').val(data.date_np);
            $('#date-en').val(data.date_en);
            $('#inspection_total').val(data.inspection_total);
            $('#id').val(data.id);
                
        },'json');
        $('#form-dialog').modal();
    }
</script>
<script type="text/javascript">
    function delete_record(id){
        if(confirm('Do you want to delete this record?')){
            $.post('<?php echo site_url('inspection/delete_json')?>',{id:id},function(data){
                alert(data.msg);
                if(data.success){
                    location.reload();
                }
            },'json');
        }
    }
</script>