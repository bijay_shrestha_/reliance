<!-- <pre>
<?php 
// print_r($materials)
?>
	
</pre> -->
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">

<!-- <input type="text" value='<?php echo date('Y-m-d')?>' id="nepaliDate92"> -->
    
<!-- for date -->



<style type="text/css">
	#example2 tr,#example2 thead,#example2 tbody { display: block; float: left; }
	#example2 th, #example2 td { display: block; }
	#example2 td { min-height: 20px ;border-bottom:2px solid #f4f4f4; border-top: 0px};
	/*#example2 th { min-width: 500px };*/
</style>

	<section class="content">
		<div class="row">
			<div class="nav-tabs-custom">
	            <ul class="nav nav-tabs">
	                <li class="active"><a href="#tab_1" data-toggle="tab">Consumption</a></li>
	                <li><a href="#tab_2" data-toggle="tab">Add Consumption</a></li>
	            </ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<div class="box">
							<div class="box-body">
									<input type="text" id="date" class="margin" name="date"><button onclick="date_search()">search</button>
                                    
                                        <!-- <a href="<?php echo site_url("calander_shift_report/pdf")?>" class="dt-button buttons-excel buttons-html5 margin" id="pdf-btn">PDF</a> -->
									<table id="example2" class="table table-bordered table-striped">
										<thead>
										<tr>
											<th style="margin-top:2px"> Acton</th>
											<th style="min-width:400px">Date</th>
											<th >shift</th>
											<?php if($this->session->userdata('department_id') == 3){?>
						                    <th>Material</th>
						                    <?php }?>
											<?php foreach($properties as $property){?>
												<th><?php echo $property['material_property']?></th>
											<?php }?>
											<?php foreach($materials as $material){?>
												<th ><?php echo $material['material_name']?></th>
											<?php }?>
											<th>Total</th>
										</tr>
										</thead>
		                                    <tbody>
										<?php foreach($rows as $key=>$group){?>
											<?php foreach($group as $group=>$row){?>
												<?php foreach($row['shifts'] as $shift=>$quality){?>
													<?php $total = 0;?>
													<tr>
														<td>
															<a class='btn btn-warning btn-xs' href='<?php echo site_url('consumption/editForm/'.$group)?>'><i class='fa fa-pencil'></i></a>
															<button class='btn btn-danger btn-xs' onclick='removeConsumption(<?php echo $group?>)'><i class='fa fa-trash'></i></button>
														</td>
														<td><?php echo $key;?></td>
														<td><?php echo $shift;?></td>
														<?php if($this->session->userdata('department_id') == 3){?>
									                    	<td><?php echo $quality['mat']?></td>
									                    <?php }?>
														<?php foreach($properties as $property){?>
															<td><?php echo $quality[$property['material_property']]?></td>
														<?php }?>

														<?php foreach($materials as $material){?>
															<td><?php if(key_exists($material['materialid'],$quality['materials'])){
																echo number_format($quality['materials'][$material['materialid']],3);
																$total += $quality['materials'][$material['materialid']];
															}else{
																echo number_format(0,3);
															}?></td>
														<?php }?>
														<td><?php echo number_format($total,3)?></td>
													</tr>
												<?php }?>
											<?php }?>
										<?php }?>
										</tbody>
									</table>
							</div><!-- box body -->
						</div>
					</div>
					<div class="tab-pane" id="tab_2">
						<form id="stock-form" class="form-horizontal">
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Date*</label>
                        <div class="col-sm-8">
                                <input type="text" id="consumption-date" class=" form-control" name="date">
                                <input type="hidden" id="eng-date" name="eng_date">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">Select</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Shift*</label>
                        <div class="col-sm-8">
                            <select name="shift" id='shift' class="form-control">
                            	<option value="">-- select option --</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>
                    <!--#####################################################################################################-->
                    <?php if($this->session->userdata('department_id') == 3){?>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Material*</label>
                        <div class="col-sm-8">
                            <input type='text' name='material' class="form-control">
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>
                    <?php }?>
                    <!--*****************************************************************************************************-->
                    <?php 
                    $i = 0;
                    $count = count($properties);
                    foreach($properties as $property){?>
                    	<?php //echo str_replace(" ","_",strtolower($property['material_property']))?>
                    	<?php if($i == 0){?>
                    	<div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label"><?php echo $property['material_property']?>*</label>
                        <div class="col-sm-8">
                            <select 
                            name="<?php echo str_replace(" ","_",strtolower($property['material_property']))?>_id" 
                            id='<?php echo str_replace(" ","_",strtolower($property['material_property']))?>-option' class="form-control  option-<?php echo $i?>" 
                            onchange="getNextOptions('<?php echo str_replace(" ","_",strtolower($property['material_property']))?>','<?php echo str_replace(" ","_",strtolower($properties[$i+1]['material_property']))?>',<?php echo $i?>)"
                            >
                                <option value=""></option>
                                <?php foreach($init_properties as $init_properties){?>
                                	<option value="<?php echo $init_properties[$init_property.'_id']?>"><?php echo $init_properties[$init_property.'_name']?></option>
                            	<?php }?>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                        <?php }else if($i < $count-1){?>
                        	<div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label"><?php echo $property['material_property']?>*</label>
                        <div class="col-sm-8">
                            <select name="<?php echo str_replace(" ","_",strtolower($property['material_property']))?>_id" id='<?php echo str_replace(" ","_",strtolower($property['material_property']))?>-option' onchange="getNextOptions('<?php echo str_replace(" ","_",strtolower($property['material_property']))?>','<?php echo str_replace(" ","_",strtolower($properties[$i+1]['material_property']))?>',<?php echo $i?>)" class="form-control option-<?php echo $i?>">
                                <option value=""></option>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                        <?php }else{?>
                        	<div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label"><?php echo $property['material_property']?>*</label>
                        <div class="col-sm-8">
                            <select name="<?php echo str_replace(" ","_",strtolower($property['material_property']))?>_id" id='<?php echo str_replace(" ","_",strtolower($property['material_property']))?>-option' class="form-control  option-<?php echo $i?>">
                                <option value=""></option>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                        <?php }?>
                    </div>
                    <?php 
                    $i++;
                    }?>
                    <!--*****************************************************************************************************-->
                    <?php /*<div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Brand*</label>
                        <div class="col-sm-8">
                            <select name="brand_id" id='brand-option' class="form-control">
                                <option value=""></option>
                                <?php foreach($brands as $brand){?>
                                	<option value="<?php echo $brand['brand_id']?>"><?php echo $brand['brand_name']?></option>
                            	<?php }?>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Quality*</label>
                        <div class="col-sm-8">
                            <select name="quality_id" id='quality-option' class="form-control">
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Dimension*</label>
                        <div class="col-sm-8">
                            <select name="dimension_id" id='dimension-option' class="form-control">
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Color*</label>
                        <div class="col-sm-8">
                            <select name="color_id" id='color-option' class="form-control">
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>*/?>
                    <!--*********************************************************************************************************-->


					<div class="row">
						<div class="col-md-5"  align="right">
							<label for="category" control-label">Material</label>
						</div>
						<div class="col-md-2">
							<label for="category" control-label">Opining Stock</label>
						</div>
						<div class="col-md-2">
							<label for="category" control-label">Consumed</label>
						</div>
						<div class="col-md-2">
							<label for="category" control-label">Closing</label>
						</div>
					</div>
					<?php foreach($materials as $material){?>
						<div class="row form-group">
						<div class="col-md-5"  align="right">
							<?php echo $material['material_name']?>
							<?php if($material['category_id'] != 4){?>
							<input type="hidden" name="material_ids[]" value="<?php echo $material['material_id']?>">
							<?php }else{?>
								<input type="hidden" name="material_ids[]" value="<?php echo $material['materialid']?>">
							<?php }?>
						</div>
						<div class="col-md-2">
							<input type="text" readonly class='form-control' disabled name="current_stock[]" value="<?php echo number_format((($material['amount'] == NULL)?'0':$material['amount']),3)?>">
							<input type="hidden" readonly class='form-control' id="opening-stock-<?php echo $material['material_id']?>" name="current_stocks[]" value="<?php echo number_format((($material['amount'] == NULL)?'0':$material['amount']),3)?>">
						</div>
						<div class="col-md-2">
							<?php if($material['material_id']){?>
								<?php if($material['category_id'] == 4){?>
									<input type="text" class='form-control' name="consumed_stocks[]">
								<?php }else{?>
									<input type="text" class='form-control' id="used-stock-<?php echo $material['material_id']?>" onkeyup="stockCal(<?php echo $material['material_id']?>)" name="consumed_stocks[]">
								<?php }?>
							<?php }else if($material['category_id'] == 4){?>
								<input type="text" class='form-control' name="consumed_stocks[]">
							<?php }else{?>
								<input type="text" class='form-control used-stock' name="consumed_stocks[]">
							<?php }?>
						</div>
						<div class="col-md-2">
							<input type="text" readonly class='form-control' id="closing-stock-<?php echo $material['material_id']?>" name="closing_stock[]" disabled>
							<input type="hidden" readonly class='form-control' id="closing-stock-hidden-<?php echo $material['material_id']?>" name="closing_stocks[]" value="<?php echo number_format((($material['amount'] == NULL)?'0':$material['amount']),3)?>">
						</div>
					</div>
					<?php }?>
                    
                    <button type='button' class="btn btn-primary" id="submit">Save</button>
                </form>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min =  $('#date').val();
        var date = data[1]  || ''; // use data for the age column
        if ( min == "" ||
              min == date ) 
        {
            return true;
        }
        return false;
    }
);
</script>

<script>
    $(function () {
        table = $('#example2').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
                    {
                        text: 'PDF',
                        action: function ( e, dt, node, config ) {
                            // alert( 'Button activated' );
                            var date = $('#date').val();
                            // var date = $('#nepaliDate92').val();
                            window.location.href = "<?php echo site_url("home/pdf")?>"+'/1/'+date;
                            // <button type='button' id="pdf-btn" class="dt-button buttons-excel buttons-html5">PDF</button>
                        }
                    },
            ],

        	"order":[[0, "desc"]],
        	"pageLength": 4
        });
        $('#date').nepaliDatePicker();
        $('#consumption-date').nepaliDatePicker({
            ndpEnglishInput: 'eng-date'
        });
    });
    function date_search(){
// alert('here');
    // $('#date').val().change( function() {
        table.draw();
    // });
	}
</script>
<script type="text/javascript">
	/*$('#brand-option').change(function(){
		var brand_id = $(this).val();
		$("#quality-option").empty();
		$.post('<?php echo site_url("consumption/getValue")?>',{brand_id:brand_id},function(data){
			$("#quality-option").append
                ($('<option>',{
                    value:"",
                    text: '--select quality--'
                }));
			$.each(data,function(index,quality){
			$("#quality-option").append
                ($('<option>',{
                    value:quality.quality_id,
                    text: quality.quality_name
                }));
            });
		},'json')
	});*/
</script>
<script type="text/javascript">
	// $('#quality-option').change(function(){
	// 	var brand_id = $('#brand-option').val();
	// 	var quality_id = $(this).val();
	// 	$("#dimension-option").empty();
	// 	$.post('<?php echo site_url("consumption/getValue")?>',{brand_id:brand_id,quality_id:quality_id},function(data){
	// 		$("#dimension-option").append
 //                ($('<option>',{
 //                    value:"",
 //                    text: '--select dimension--'
 //                }));
	// 		$.each(data,function(index,dimension){
	// 		$("#dimension-option").append
 //                ($('<option>',{
 //                    value:dimension.dimension_id,
 //                    text: dimension.dimension_name
 //                }));
 //            });
	// 	},'json')
	// })
</script>
<script type="text/javascript">
	// $('#dimension-option').change(function(){
	// 	var brand_id = $('#brand-option').val();
	// 	var quality_id = $('#quality-option').val();
	// 	var dimension_id = $(this).val();
	// 	$("#color-option").empty();
	// 	$.post('<?php echo site_url("consumption/getValue")?>',{brand_id:brand_id,quality_id:quality_id,dimension_id:dimension_id},function(data){
	// 		$("#color-option").append
 //                ($('<option>',{
 //                    value:"",
 //                    text: '--select color--'
 //                }));
	// 		$.each(data,function(index,color){
	// 		$("#color-option").append
 //                ($('<option>',{
 //                    value:color.color_id,
 //                    text: color.color_name
 //                }));
 //            });
	// 	},'json')
	// })
</script>
<script type="text/javascript">
	function stockCal(i){
		var opening_stock = $("#opening-stock-"+i).val().replace(',','');
		var used_stock = $("#used-stock-"+i).val().replace(',','');
		if(used_stock != ''){
			if(parseFloat(opening_stock) >= parseFloat(used_stock)){
				var closing_stock = opening_stock - used_stock;
				closing_stock = closing_stock.toFixed(3);
				$('#closing-stock-'+i).val(closing_stock);
				$('#closing-stock-hidden-'+i).val(closing_stock);
			}else{
				alert('opening stock is less or invalid input');
				$("#used-stock-"+i).val('');
				$("#closing-stock-"+i).val('');
				$("#closing-stock-hidden-"+i).val('');
			}
		}else{
			$("#used-stock-"+i).val('');
				$("#closing-stock-"+i).val('');
				$("#closing-stock-hidden-"+i).val('');
		}
	}
</script>

<script type="text/javascript">
	$('#submit').click(function(){
		// console.log($("#stock-form").serializeArray());
		var ready = true;
		var properties = <?php echo json_encode($properties) ?>;
		$.each(properties,function(index,value){
			property = value.material_property.toLowerCase();
			property =property.replace(" ",'_');
			if($('#'+property+'-option').val() == ''){
				ready = false;
			}
		});
			console.log(ready);
		if(!ready){
			alert('Please fill the required information');
			$('.error').show();
		}else{
			$.post('<?php echo site_url("consumption/save_consumption")?>',$("#stock-form").serializeArray(),function(data){
				if(data){
					location.reload();
				}
			},'json');
		}
		
	});
</script>
<script type="text/javascript">
	function getNextOptions(option1,option2,i){
		var property = {};
		console.log($('.option-0').val());
		for(var j=0;j<=i;j++){
			property[j] = $('.option-'+j).val();
			// property['name'] = $('<?php //echo str_replace(" ","_",strtolower($propertyies[j]['material_property']))?>');
		}
		$("#"+option2+'-option').empty();
		$.post('<?php echo site_url("consumption/getNextValue")?>',{'properties':property,'option':i,'group_by':option2},function(data){
			$("#"+option2+'-option').append
                ($('<option>',{
                    value:"",
                    text: '--select quality--'
                }));
			$.each(data,function(index,value){
				console.log(option2+'-option');
			$("#"+option2+'-option').append
                ($('<option>',{
                    value:value[option2+'_id'],
                    text: value[option2+'_name']
                }));
            });
		},'json')
	}
</script>
<script type="text/javascript">
	// function removeInput(){
		$('.used-stock').keyup(function(){
			if($(this).val() != 0){
				alert('opening stock is less or invalid input');
				$(this).val('');
			}
		})
	// }
</script>
<script type="text/javascript">
	function removeConsumption(group){
		if(confirm('Do you want to remove this record?')){
			$.post('<?php echo site_url("consumption/removeConsumption")?>',{group:group},function(data){
				if(data){
					alert('Consumption deleted');
					location.reload();
				}else{
					alert('please try again');
				}
			},'json');
		}
	}
</script>
