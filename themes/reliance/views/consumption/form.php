<!-- <pre><?print_r($materials)?></pre> -->
<section class="content">
		<div class="row">
			<div class="box box-default">
			<div class="box-body">
				<form id="consumption-form" action='#' class="form-horizontal">
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Date*</label>
                        <div class="col-sm-8">
                                <input type="text" id="consumption-date" value='<?php echo $consumptions[0]['nepali_date']?>' class="form-control" name="date">
                                <input type="hidden" id="eng-date" value='<?php echo $consumptions[0]['english_date']?>' name="eng_date">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">Select</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Shift*</label>
                        <div class="col-sm-8">
                            <select name="shift" id='shift' class="form-control">
                            	<option value="">-- select option --</option>
                                <option value="A" <?php echo ($consumptions['0']['shift'] == 'A')?'selected':''?>>A</option>
                                <option value="B" <?php echo ($consumptions['0']['shift'] == 'B')?'selected':''?>>B</option>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>
                    <!--#####################################################################################################-->
                    <?php if($this->session->userdata('department_id') == 3){?>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Material*</label>
                        <div class="col-sm-8">
                            <input type='text' name='material' class="form-control" value='<?php echo $consumptions['0']['material']?>'>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                    </div>
                    <?php }?>
                    <!--*****************************************************************************************************-->
                    <?php 
                    $i = 0;
                    $count = count($properties);
                    foreach($properties as $property){?>
                    	<?php /*<pre>
                    		<?php //print_r($init_properties[str_replace(" ","_",strtolower($property["material_property"]))]);?>
                    	</pre>*/?>
                    	<?php //echo str_replace(" ","_",strtolower($property['material_property']))?>
                    	<?php if($i < $count-1){?>
                    	<div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label"><?php echo $property['material_property']?>*</label>
                        <div class="col-sm-8">
                        
                            <select 
                            name="<?php echo str_replace(" ","_",strtolower($property['material_property']))?>_id" 
                            id='<?php echo str_replace(" ","_",strtolower($property['material_property']))?>-option' class="form-control  option-<?php echo $i?>" 
                            onchange="getNextOptions('<?php echo str_replace(" ","_",strtolower($property['material_property']))?>','<?php echo str_replace(" ","_",strtolower($properties[$i+1]['material_property']))?>',<?php echo $i?>)"
                            >
                                	<option value=""></option>
                                <?php foreach($init_properties[str_replace(" ","_",strtolower($property["material_property"]))] as $init_property){?>
                                	<option 
                                	value="<?php echo $init_property[str_replace(" ","_",strtolower($property["material_property"])).'_id'];?>"
                                	<?php echo ($init_property[str_replace(" ","_",strtolower($property["material_property"])).'_id'] == $consumed_material_property[str_replace(" ","_",strtolower($property["material_property"])).'_id'])?'selected':''?>
                                	>
	                                	<?php echo $init_property[str_replace(" ","_",strtolower($property["material_property"])).'_name']?>
                                	</option>
                            	<?php }?>
                            </select>

                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                        <?php }else{?>
                        	<div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label"><?php echo $property['material_property']?>*</label>
                        <div class="col-sm-8">
                            <select name="<?php echo str_replace(" ","_",strtolower($property['material_property']))?>_id" id='<?php echo str_replace(" ","_",strtolower($property['material_property']))?>-option' class="form-control  option-<?php echo $i?>">
                                <option value=""></option>
                                <?php foreach($init_properties[str_replace(" ","_",strtolower($property["material_property"]))] as $init_property){?>
                                	<option 
                                	value="<?php echo $init_property[str_replace(" ","_",strtolower($property["material_property"])).'_id'];?>"
                                	<?php echo ($init_property[str_replace(" ","_",strtolower($property["material_property"])).'_id'] == $consumed_material_property[str_replace(" ","_",strtolower($property["material_property"])).'_id'])?'selected':''?>
                                	>
	                                	<?php echo $init_property[str_replace(" ","_",strtolower($property["material_property"])).'_name']?>
                                	</option>
                            	<?php }?>
                            </select>
                            <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                        </div>
                        <?php }?>
                    </div>
                    <?php 
                    $i++;
                    }?>
                    <!--*****************************************************************************************************-->
                    


					<div class="row">
						<div class="col-md-5">
							<label for="category" control-label">Material</label>
						</div>
						<div class="col-md-2">
							<label for="category" control-label">Opining Stock</label>
						</div>
						<div class="col-md-2">
							<label for="category" control-label">Consumed</label>
						</div>
						<div class="col-md-2">
							<label for="category" control-label">Closing</label>
						</div>
					</div>
					<?php foreach($materials as $material){?>
						<div class="row form-group">
						<div class="col-md-5" align="right">
							<?php echo $material['material_name']?>
							<?php if($material['category_id'] != 4){?>
								<input type="hidden" name="material_ids[]" value="<?php echo $material['material_id']?>">
							<?php }else{?>
								<input type="hidden" name="material_ids[]" value="<?php echo $material['materialid']?>">
							<?php }?>
						</div>
						<div class="col-md-2">
							
							<input type="text" readonly class='form-control' disabled name="current_stock[]" 
							value="<?php
							$stock = ($material['amount'] == NULL)?'0':$material['amount'];
							if(!array_key_exists($material['material_id'], $consumed_material)){
								echo number_format(($stock),3);
							}else{
								echo number_format($stock+$consumed_material[$material['material_id']]['consumption_amount'],3);
							}
							?>">
							
							<input type="hidden" readonly class='form-control' id="opening-stock-<?php echo $material['material_id']?>" name="current_stocks[]" 
							value="<?php 
							if(!array_key_exists($material['material_id'], $consumed_material)){
								echo $stock;
							}else{
								echo $stock+$consumed_material[$material['material_id']]['consumption_amount'];
							}
							?>">

						</div>
						<div class="col-md-2">
							<?php if($material['category_id'] != 4){?>
							<?php if($material['material_id']){?>
								<input type="text" 
									class='form-control' 
									id="used-stock-<?php echo $material['material_id']?>" 
									onkeyup="stockCal(<?php echo $material['material_id']?>)" 
									name="consumed_stocks[]" 
									value="<?php echo (array_key_exists($material['materialid'], $consumed_material))?number_format($consumed_material[$material['materialid']]['consumption_amount'],3):'0.000';?>">
							<?php }else{?>
								<input type="text" class='form-control used-stock' name="consumed_stocks[]">
							<?php }?>
							<?php }else{?>
								<input type="text" 
									class='form-control' name="consumed_stocks[]" value="<?php echo (array_key_exists($material['materialid'], $consumed_material))?number_format($consumed_material[$material['materialid']]['consumption_amount'],3):'0.000';?>">
							<?php }?>
						</div>
						<div class="col-md-2">
							<input type="text" readonly class='form-control' id="closing-stock-<?php echo $material['material_id']?>" name="closing_stock[]" disabled
								value="<?php 
									if(array_key_exists($material['material_id'], $consumed_material)){
										echo number_format(($stock),3);
									}else{
										echo '0.000';
									}
								?>"
							>
							<input type="hidden" readonly class='form-control' id="closing-stock-hidden-<?php echo $material['material_id']?>" name="closing_stocks[]" 
									value="<?php 
										if(array_key_exists($material['material_id'], $consumed_material)){
											echo $stock;
										}else{
											echo $material['amount'];
										}
									?>">
						</div>
					</div>
					<?php }?>
					<input type='hidden' name='group' value="<?php echo $consumptions[0]['group']?>">
                    
                    <button type='button' class="btn btn-primary" id="submit">Save</button>
                </form>
            </div>
            </div>
		</div>
</section>

<script type="text/javascript">
	function getNextOptions(option1,option2,i){
		var property = {};
		console.log($('.option-0').val());
		for(var j=0;j<=i;j++){
			property[j] = $('.option-'+j).val();
			// property['name'] = $('<?php //echo str_replace(" ","_",strtolower($propertyies[j]['material_property']))?>');
		}
		$("#"+option2+'-option').empty();
		$.post('<?php echo site_url("consumption/getNextValue")?>',{'properties':property,'option':i,'group_by':option2},function(data){
			$("#"+option2+'-option').append
                ($('<option>',{
                    value:"",
                    text: '--select quality--'
                }));
			$.each(data,function(index,value){
				console.log(option2+'-option');
			$("#"+option2+'-option').append
                ($('<option>',{
                    value:value[option2+'_id'],
                    text: value[option2+'_name']
                }));
            });
		},'json')
	}
</script>
<script type="text/javascript">
	$(function () {
        table = $('#example2').DataTable({
        	"order":[[0, "desc"]],
        	"pageLength": 4
        });
        $('#date').nepaliDatePicker();
        $('#consumption-date').nepaliDatePicker({
            ndpEnglishInput: 'eng-date'
        });
    });
</script>
<script type="text/javascript">
	function stockCal(i){
		var opening_stock = $("#opening-stock-"+i).val().replace(',','');
		var used_stock = $("#used-stock-"+i).val().replace(',','');
		if(used_stock != ''){
			if(parseFloat(opening_stock) >= parseFloat(used_stock)){
				var closing_stock = opening_stock - used_stock;
				closing_stock = closing_stock.toFixed(3);
				$('#closing-stock-'+i).val(closing_stock);
				$('#closing-stock-hidden-'+i).val(closing_stock);
			}else{
				alert('opening stock is less or invalid input');
				$("#used-stock-"+i).val('');
				$("#closing-stock-"+i).val('');
				$("#closing-stock-hidden-"+i).val('');
			}
		}else{
			$("#used-stock-"+i).val('');
				$("#closing-stock-"+i).val('');
				$("#closing-stock-hidden-"+i).val('');
		}
	}
</script>
<script type="text/javascript">
	// function removeInput(){
		$('.used-stock').keyup(function(){
			if($(this).val() != 0){
				alert('opening stock is less or invalid input');
				$(this).val('');
			}
		})
	// }
</script>
<script type="text/javascript">
	$('#submit').click(function(){
		// console.log($("#stock-form").serializeArray());
		var ready = true;
		var properties = <?php echo json_encode($properties) ?>;
		$.each(properties,function(index,value){
			property = value.material_property.toLowerCase();
			property =property.replace(" ",'_');
			if($('#'+property+'-option').val() == ''){
				ready = false;
			}
		});
			console.log(ready);
		if(!ready){
			alert('Please fill the required information');
			$('.error').show();
		}else{
			if(confirm('Do you want to edit data?')){
				$.post('<?php echo site_url("consumption/editConsumption")?>',$("#consumption-form").serializeArray(),function(data){
					if(data){
						alert('Data edited');
						window.location.href = '<?php echo site_url("consumption")?>';
					}
				},'json');

			}
		}
		
	});
</script>