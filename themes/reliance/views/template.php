<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button class="btn btn-primary" id="add-btn">Add</button>
                    <table id="brand_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Brand Name</th>
                                <th>Brand Type</th>
                                <th>Production</th>
                                <th>Sales</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $value) {?>
                            <tr>
                                <td><?php echo $value['date_np']?></td>
                                <td><?php echo $value['brand_name']?></td>
                                <td><?php echo $value['brand_type']?></td>
                                <td><?php echo $value['production']?></td>
                                <td><?php echo $value['sales']?></td>
                                <td>
                                    <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger" onclick="delete_data(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="daily-production-sales-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="Date">
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                                <div id="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Brand Type</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='type_id' id="type_id">
                                    <option value="">-- select brand type --</option>
                                <?php foreach ($brand_types as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                                <?php }?>
                                </select>
                                <div id="error_type_id" hidden style="color: red" class="error-div">Brand type is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Brand</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='brand_id' id="brand_id">
                                    <option value="">-- select brand --</option>
                                <?php foreach ($brands as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['brand_name']?></option>
                                <?php }?>
                                </select>
                                <div id="error_brand_id" hidden style="color: red" class="error-div">Brand name is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Production (Bags)</label>
                            <div class="col-sm-8">
                                <input type="number" name="production" id="production" class="form-control" placeholder="production">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Sales (Bags)</label>
                            <div class="col-sm-8">
                                <input type="number" name="sales" id="sales" class="form-control" placeholder="Sales">
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });

        // nepali date picker
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('input').val('');
        $('.error-div').hide();
        $('option').attr('selected', false);
        $('#myModalLabel').html('Add Daily Production Sales');
        $('#daily-production-sales-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var i = 0;
        var date_np = $('#date_np').val();
        var type_id = $('#type_id').val();
        var brand_id = $('#brand_id').val();

        if(date_np == ''){
            i = 1;
            $('#error_date_np').show();
        }else{
            $('#error_date_np').hide();
        }

        if(type_id == ''){
            i = 1;
            $('#error_type_id').show();
        }else{
            $('#error_type_id').hide();
        }
        if(brand_id == ''){
            i = 1;
            $('#error_brand_id').show();
        }else{
            $('#error_brand_id').hide();
        }
        if(i == 0){
            $.post('<?php echo site_url("daily_production_sale/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.error-div').hide();
        $.post('<?php echo site_url("daily_production_sale/get_value")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });
            $('#myModalLabel').html('Edit Daily Production Sales');
            $('#daily-production-sales-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_data(id){
        $.post('<?php echo site_url("daily_production_sale/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>