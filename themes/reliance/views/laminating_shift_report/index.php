<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url()?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box">
        <div class="box-body">
        <button class="btn btn-primary margin" onclick='reportForm()'>Add</button>

                <button type='button' id="pdf-btn" class="pull-right margin dt-button buttons-excel buttons-html5">PDF</button>
            <input type="text" value='<?php //echo date('Y-m-d')?>' class='pull-right margin' id="nepaliDate92">
                <br>
            <!-- for date -->

            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Shift</th>
                        <th>Brand</th>
                        <th>Color</th>
                        <th>Base UT</th>
                        <th>Basr MT</th>
                        <th>Base OT</th>
                        <th>Print Film</th>
                        <th>CT</th>
                        <th>Dull CT</th>
                        <th>output</th>
                        <th>Action</th>
                    </tr>               
                </thead>
                <tbody>
                    <?php foreach($reports as $report){?>
                        <tr>
                            <td><?php echo $report['date-np']?></td>
                            <td><?php echo $report['shift'] ?></td>
                            <td><?php echo $report['brand_name']?></td>
                            <td><?php echo $report['color'] ?></td>
                            <td><?php echo number_format($report['base_ut'],3) ?></td>
                            <td><?php echo number_format($report['base_mt'],3)?></td>
                            <td><?php echo number_format($report['base_ot'],3)?></td>
                            <td><?php echo number_format($report['print_film'],3)?></td>
                            <td><?php echo number_format($report['ct'],3)?></td>
                            <td><?php echo number_format($report['dull_ct'],3)?></td>
                            <td><?php echo number_format($report['output'],3)?></td>
                            <td>
                                <button type="button" class="btn btn-warning add_report" id="report-<?php echo $report['lsr_id'] ?>" onclick="reportForm(<?php echo $report['lsr_id'] ?>)" value="<?php echo $report['lsr_id'] ?>">Edit</button>
                                <button type="button" class="btn btn-danger" id="report-<?php echo $report['lsr_id'] ?>" onclick="deleteReport(<?php echo $report['lsr_id'] ?>)" value="<?php echo $report['lsr_id'] ?>">Delete</button>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- modal to add shift report -->
<div class="modal fade" id="report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="report-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Date*</label>
                            <div class="col-sm-8">
                                <input type="text" name="date-np" class="form-control" id="date-np" placeholder="Date" >
                                <input type="hidden" name="date-en" class="form-control" id="date-en">
                                <span id="error-dealer-name" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shift" class="col-sm-4 control-label">Shift</label>
                            <div class="col-sm-8">
                                <select name="shift" class="form-control" id="shift" placeholder="Shift" >
                                    <option value=''>-- select option --</option>
                                    <option id="shift-A" value='A'>A</option>
                                    <option id="shift-B" value='B'>B</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        	<label for="brand" class="col-sm-4 control-label">Brand</label>
                            <div class="col-sm-8">
                                <select name="brand" class="form-control" id="brand" >
                                	<option value=''>-- select option</option>
                                	<?php foreach($brands as $brand){?>
                                		<option  value='<?php echo $brand['brand_id']?>' id='brand-<?php echo $brand["brand_id"]?>'><?php echo $brand['brand_name']?></option>
                            		<?php  }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        	<label for="color" class="col-sm-4 control-label">Color</label>
                            <div class="col-sm-8">
                                <select name="color" class="form-control" id="color" >
                                	<option>-- select option</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="base_ut" class="col-sm-4 control-label">Base UT</label>
                            <div class="col-sm-8">
                                <input type="number" name="base_ut" class="form-control" id="base_ut" placeholder="Base UT" >
                                <span id="error-contact" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="base_mt" class="col-sm-4 control-label">Base MT</label>
                            <div class="col-sm-8">
                                <input type="number" name="base_mt" class="form-control" id="base_mt" placeholder="Base MT" >
                                <span id="error-mobile" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="base_ot" class="col-sm-4 control-label">Base OT</label>
                            <div class="col-sm-8">
                                <input type="number" name="base_ot" class="form-control" id="base_ot" placeholder="Base OT" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="print_film" class="col-sm-4 control-label">Print Film</label>
                            <div class="col-sm-8">
                                <input type="number" name="print_film" class="form-control" id="print_film" placeholder="Print Film">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ct" class="col-sm-4 control-label">CT</label>
                            <div class="col-sm-8">
                                <input type="number" name="ct" class="form-control" id="ct" placeholder="CT">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dull_ct" class="col-sm-4 control-label">Dull CT</label>
                            <div class="col-sm-8">
                                <input type="number" name="dull_ct" class="form-control" id="dull_ct" placeholder="Dull CT">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="output" class="col-sm-4 control-label">Output</label>
                            <div class="col-sm-8">
                                <input type="number" name="output" class="form-control" id="output" placeholder="Output">
                            </div>
                        </div>

                        <input type="hidden" name="lsr_id" id="lsr-id" value="">
                        <input type="hidden" name="material_properyt_id" id="material-properyt-id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$('#example2').DataTable({
		"order":[[0, "desc"]],
        dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
	});
});
</script>
<script type="text/javascript">
	function reportForm(id = null){
		if(id == null){
				$('#myModalLabel').html('Add');
				$('#date-np').val('');
				$('#date-en').val('');
				$('#shift').val('');
                $('#brand').val('');
                $('#color').empty();
                $('#base_ut').val('');
                $('#base_mt').val('');
                $('#base_ot').val('');
				$('#print_film').val('');
				$('#ct').val('');
				$('#dull_ct').val('');
				$('#output').val('');
                $('#lsr-id').val('');
				$('#material-properyt-id').val('');
                $('#base_ut').removeAttr("disabled");
                $('#base_mt').removeAttr("disabled");
                $('#base_ot').removeAttr("disabled");
                $('#print_film').removeAttr("disabled");
                $('#ct').removeAttr("disabled");
                $('#dull_ct').removeAttr("disabled");

				$('#report-dialog').modal();

		}else{
			$.post('<?php echo site_url("laminating_shift_report/getReport")?>',{id:id},function(data){
                $('#myModalLabel').html('Edit');
                $('#date-np').val(data.date_np);
                $('#date-en').val(data.date_en);
                $('#shift').val(data.shift);
                $('#brand-'+data.brand).prop('selected', true);
                $('#color').empty();
                $.each(data.color_options.color,function(index,value){
                $("#color").append
                    ($('<option>',{
                        value:value['color'],
                        text: value['color']
                    }));
                });
                $('#color').val(data.color);
                $.each(data.color_options.material_active,function(index,value){
                    if(value == 1){
                        $('#'+index).removeAttr("disabled");
                    }else{
                        $('#'+index).prop('disabled','true');
                    }
                });
                $('#base_ut').val(data.base_ut);
                $('#base_mt').val(data.base_mt);
                $('#base_ot').val(data.base_ot);
                $('#print_film').val(data.print_film);
                $('#ct').val(data.ct);
                $('#dull_ct').val(data.dull_ct);
                $('#output').val(data.output);
                $('#material-properyt-id').val('');
                $('#lsr-id').val(data.lsr_id);
				$('#report-dialog').modal();

			},'json')
		}
	}
</script>
<!-- saving report -->
<script type="text/javascript">
	$('#form-submit').click(function(){
		id = $('#brand').val();
        output = parseFloat($('#output').val());
console.log(id);
        if(id != 'undefined'){
            $.post('<?php echo site_url("laminating_shift_report/getbrandmaterial")?>',{id:id},function(data){
	               ready = 1;
                
                $.each(data,function(index,value){
                    if(value == 1){
                        material_value = parseFloat($('#'+index).val());
                        if(index != 'lbm_id' && index != 'brand_id'){
                            console.log(index);
                            if(material_value < output){

                                ready = 0;
                            }
                        }
                    }
                    // 
                });
            		if(ready == 0){
            		  alert('Output shoud be less than input values');
            		}else{
            			$.post('<?php echo site_url("laminating_shift_report/save")?>',$('#report-form').serializeArray(),function(data){
                            if(data.success){
                                alert('Data added');
                                location.reload();
                            }else{
                                alert('Error occured, please try again.');
                            }
            							},'json');
            		}
            },'json');
        }else{
            alert('All input field are required')
        }
	});
</script>
<script type="text/javascript">
	$('#brand').change(function(){
		brand_id = $(this).val();
        $("#color").empty();
		$.post('<?php echo site_url("laminating_shift_report/getColor")?>',{id:brand_id},function(data){
			             $("#color").append
                ($('<option>',{
                    text: '-- Select option--'
                }));
            $.each(data.color,function(index,value){
			  	$("#color").append
                ($('<option>',{
                    value:value['color'],
                    text: value['color']
                }));
            });
            $.each(data.material_active,function(index,value){
                if(value == 1){
                    $('#'+index).removeAttr("disabled");
                }else{
                    $('#'+index).prop('disabled','true');
                }
            });
		},'json');
	});
</script>
<script type="text/javascript">
    $(function(){
        $('#date-np').nepaliDatePicker({
                ndpEnglishInput: 'date-en'
        });
    });
</script>
<script type="text/javascript">
    function deleteReport(id){
        if(confirm('Do you want to delete this data?')){
            $.post('<?php echo site_url("laminating_shift_report/delete_json")?>',{id:id},function(data){
                    if(data){
                        alert('Data deleted');
                        location.reload();
                    }else{
                        alert('Error occured, please try again.');
                    }
            },'json');
        }
    }
</script>
<!-- for pdf -->
<script type="text/javascript">
$(function(){
    $('#nepaliDate92').nepaliDatePicker();
    // $('#nepaliDate92').val(AD2BS($('#nepaliDate92').val()));
    // var date = $('#nepaliDate92').val();
});
    $('#pdf-btn').click(function(){
        var date = $('#date').val();
        var date = $('#nepaliDate92').val();
        window.location.href = "<?php echo site_url("home/pdf")?>"+'/2/'+date;
    })
</script>