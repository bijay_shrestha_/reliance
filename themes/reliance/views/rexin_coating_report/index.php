<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box">
	    <div class="box-body">
            <div class="pull-right margin"><?php $this->load->view('home/pdf_button')?></div>
	    	<button class='btn btn-primary margin' onclick='reportForm()'>Add</button>
	    	<table id="example2" class="table table-bordered table-striped table-responsive">
	            <thead>
	                <tr>
	                    <th>Date</th>
	                    <th>Shift</th>
	                    <th>Brand</th>
	                    <th>Color</th>
	                    <th>Production</th>
	                    <th>Top Coat</th>
	                    <th>Foam Coat</th>
	                    <th>Adhesive Coat</th>
	                    <th>Fabric Weight</th>
	                    <th>Gross Wt</th>
	                    <th>Net wt</th>
	                    <th>Action</th>
	                </tr>
	            </thead>
	            <tbody>
	                <?php foreach ($reports as $report) { ?>
	                    <tr>
                        <?php //echo '<pre>';print_r($report['material']);echo '</pre>'?>
	                    <?php 
                            if(count($report['material'])>0){
                                $gross_wt = $report['top_coat'] + $report['foam_coat'] +$report['adhesive_coat'] + ($report['production']* $report['material']['weight']);
                            }else{
                                $gross_wt = $report['top_coat'] + $report['foam_coat'] +$report['adhesive_coat'];
                            }?>
	                        <td><?php echo $report['date_np'] ?></td>
	                        <td><?php echo $report['shift'] ?></td>
	                        <td><?php if(count($report['material'])>0){echo $report['material']['brand_name'];}else{echo '--';} ?></td>
	                        <td><?php if(count($report['material'])>0){echo $report['material']['color_name'];}else{echo '--';}?></td>
	                        <td><?php echo number_format($report['production'],3)?></td>
	                        <td><?php echo number_format($report['top_coat'],3)?></td>
	                        	<td><?php echo number_format($report['foam_coat'],3)?></td>
	                        	<td><?php echo number_format($report['adhesive_coat'],3)?></td>
	                        <td><?php echo number_format($report['fabric_wt'] * $report['production'],3)?></td>
	                        <td><?php echo number_format($gross_wt,3)?></td>
	                        <td><?php echo number_format($report['net_wt'],3)?></td>
	                        <td>
	                        		<button type="button" class="btn btn-warning add_report" id="report-<?php echo $report['report_id'] ?>" onclick="reportForm(<?php echo $report['report_id'] ?>)" value="<?php echo $report['report_id'] ?>"> Edit</button>
	                        		<button type="button" class="btn btn-danger add_report" id="report-<?php echo $report['report_id'] ?>" onclick="deleteReport(<?php echo $report['report_id'] ?>)" value="<?php echo $report['report_id'] ?>"> Delete</button>
                        	</td>
	                    </tr>
	                <?php } ?>
	            </tbody>
	        </table>
        </div>
    </div>
</section>

<!-- modal to add shift report -->
<div class="modal fade" id="report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="report-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date-np" placeholder="Date" >
                                <input type="hidden" name="date_en" class="form-control" id="date-en">
                                <span id="error-dealer-name" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shift" class="col-sm-4 control-label">Shift</label>
                            <div class="col-sm-8">
                                <select name="shift" class="form-control" id="shift" >
                                	<option value="">-- Select option --</option>
                                	<option value="A">A</option>
                                	<option value='B'>B</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quality" class="col-sm-4 control-label">Brand*</label>
                            <div class="col-sm-8">
                                <select name="brand_id" class="form-control" id="brand" onchange='getValue("color","brand",0)'>
                                	<option value="">-- select option --</option>
                                	<?php foreach($brands as $brand){?>
                                		<option value="<?php echo $brand['brand_id']?>"><?php echo $brand['brand_name']?></option>
                            		<?php }?>
                                </select>
                                <span id="error-contact" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="color" class="col-sm-4 control-label">Color</label>
                            <div class="col-sm-8">
                                <select name="color_id" class="form-control" id="color" onchange='getValue("emboss","color",1)'></select>
                                <span id="error-mobile" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="emboss" class="col-sm-4 control-label">Emboss</label>
                            <div class="col-sm-8">
                                <select name="emboss_id" class="form-control" id="emboss" onchange='getValue("rpaper","emboss",2)'></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="r_paper" class="col-sm-4 control-label">R. Paper</label>
                            <div class="col-sm-8">
                                <select name="rpaper_id" class="form-control" id="rpaper" onchange='getValue("fabric","rpaper",3)'></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fabric" class="col-sm-4 control-label">Fabric</label>
                            <div class="col-sm-8">
                                <select name="fabric_id" class="form-control" id="fabric" onchange='getValue("thickness","fabric",4)'></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="thickness" class="col-sm-4 control-label">Thickness</label>
                            <div class="col-sm-8">
                                <select name="thickness_id" class="form-control" id="thickness" ></select>
                            </div>
                        </div>
                        <!--##############################################################################-->
                        <div class="form-group">
                            <label for="width" class="col-sm-4 control-label">Width (mm)</label>
                            <div class="col-sm-8">
                                <input type='number' name="width" class="form-control" id="width" onkeyup='checkValue("width")' onblur='checkValue("width")'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="production" class="col-sm-4 control-label">Production (m)</label>
                            <div class="col-sm-8">
                                <input type='number' name="production" class="form-control" id="production" onkeyup='checkValue("production")' onblur='checkValue("production")'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="top_coat" class="col-sm-4 control-label">Top Coat</label>
                            <div class="col-sm-8">
                                <input type='number' name="top_coat" class="form-control" id="top-coat" onkeyup='checkValue("top-coat")' onblur='checkValue("top-coat")'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="foam_coat" class="col-sm-4 control-label">Foam Coat</label>
                            <div class="col-sm-8">
                                <input type='number' name="foam_coat" class="form-control" id="foam-coat" onkeyup='checkValue("foam-coat")' onblur='checkValue("foam-coat")'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="adhesive_coat" class="col-sm-4 control-label">Adhesive Coat</label>
                            <div class="col-sm-8">
                                <input type='number' name="adhesive_coat" class="form-control" id="adhesive-coat" onkeyup='checkValue("adhesive-coat")' onblur='checkValue("adhesive-coat")'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fabric_wt" class="col-sm-4 control-label">Fabric Weight</label>
                            <div class="col-sm-8">
                        		<input type='text' class="form-control" id="fabric-wt1" disabled>
                             	<input type='hidden' name="fabric_wt" class="form-control" id="fabric-wt" readonly>
                            </div>
                        </div>
                       <div class="form-group">
                            <label for="gross_wt" class="col-sm-4 control-label">Gross weight</label>
                            <div class="col-sm-8">
                        		<input type='text' class="form-control" id="gross-wt" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="net_wt" class="col-sm-4 control-label">Net Weight</label>
                            <div class="col-sm-8">
                                <input type='number' name="net_wt" class="form-control" id="net-wt" onkeyup='checkValue("net-wt")' onblur='checkValue("net-wt")'>
                        <input type="hidden" name="report_id" id="report-id" value="">
                            </div>
                        </div>
                        <!--#####################################################################################-->

                        <input type="hidden" name="material_properyt_id" id="material-properyt-id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
            <input type="hidden" id='process'>
        </div>
    </div>
</div>
<script type="text/javascript">
var process = 'add';
$(function(){
	$('#example2').DataTable({
		"order":[[0, "desc"]],
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
            },
        ],
	});
});
</script>
<script type="text/javascript">
    $(function(){
        $('#date-np').nepaliDatePicker({
                ndpEnglishInput: 'date-en'
        });
    });
</script>
<script type="text/javascript">
	function reportForm(id = null){
		if(id){
			$.post('<?php echo site_url("rexin_coating_report/getreport")?>',{id:id},function(data){
				$('#myModalLabel').html('Edit');
				$('#date-np').val(data.report.date_np);
				$('#date-en').val(data.report.date_en);
				$('#shift').val(data.report.shift);
				$('#brand').val(data.report.material.brand_id);
				////////////////////////////////////////////////////////////////////
				$('#color').empty();
				$.each(data.colors,function(index,value){
                $("#color").append
                    ($('<option>',{
                        value:value['color_id'],
                        text: value['color_name']
                    }));
                });
				$('#color').val(data.report.material.color_id);
//////////////////////////////////////////////////////////////
				$('#emboss').empty();
				$.each(data.emboss,function(index,value){
                $("#emboss").append
                    ($('<option>',{
                        value:value['emboss_id'],
                        text: value['emboss_name']
                    }));
                });
				$('#emboss').val(data.report.material.emboss_id);
/////////////////////////////////////////////////////////////////////////
				$('#rpaper').empty();
				$.each(data.rpaper,function(index,value){
                $("#rpaper").append
                    ($('<option>',{
                        value:value['rpaper_id'],
                        text: value['rp_name']
                    }));
                });
				$('#rpaper').val(data.report.material.rpaper_id);
///////////////////////////////////////////////////////////////////////
				$('#fabric').empty();
				$.each(data.fabric,function(index,value){
                $("#fabric").append
                    ($('<option>',{
                        value:value['fabric_id'],
                        text: value['fabric_name']
                    }));
                });
				$('#fabric').val(data.report.material.fabric_id);
				/////////////////////////////////////////////////////////////////////////////
				$('#thickness').empty();
				$.each(data.thickness,function(index,value){
                $("#thickness").append
                    ($('<option>',{
                        value:value['thickness_id'],
                        text: value['thickness_name']
                    }));
                });
				$('#thickness').val(data.report.material.thickness_id);
				$('#width').val(data.report.width);
				$('#production').val(data.report.production);
				$('#fabric-wt').val(data.report.fabric_wt);
				$('#fabric-wt1').val(data.report.fabric_wt);
				$('#top-coat').val(data.report.top_coat);
				$('#foam-coat').val(data.report.foam_coat);
				$('#adhesive-coat').val(data.report.adhesive_coat);
				$('#net-wt').val(data.report.net_wt);
				$('#report-id').val(data.report.report_id);
				$('#process').val('edit');
				$('#report-dialog').modal();

			},'json')
		}else{
			// $.post('<?php echo site_url("calander_shift_report/getlistconsumption")?>',{group:group},function(data){
				$('#myModalLabel').html('Add');
				$('#date-np').val('');
				$('#date-en').val('');
				$('#shift').val('');
				$('#brand').val('');
				$('#color').val('');
				$('#emboss').val('');
				$('#rpaper').val('');
				$('#fabric').val('');
				$('#thickness').val('');
				$('#width').val('');
				$('#production').val('');
				$('#fabric-wt').val('');
				$('#fabric-wt1').val('');
				$('#top-coat').val('');
				$('#foam-coat').val('');
				$('#adhesive-coat').val('');
				$('#net-wt').val('');
				$('#process').val('add');
				$('#report-dialog').modal();

			// },'json')
		}
	}
</script>
<!-- saving report -->
<script type="text/javascript">
	$('#form-submit').click(function(){
		if($('#net-wt').val() == ''){
			alert('All fields are required.');
		}else{
			$.post('<?php echo site_url("rexin_coating_report/save")?>',$('#report-form').serializeArray(),function(data){
				if(data.success){
					if ($('#process').val() == 'add'){
						alert('Report added');
					}else{
						alert('Report edited');
					}
					location.reload();
				}else{
					alert('Error occur');
				}
			},'json');
		}
	});
</script>
<script type="text/javascript">
	function checkValue(type){
		product_total = parseFloat($('#production').val().replace(",", "")) * parseFloat($('#fabric-wt').val().replace(",", ""));
		product_total += parseFloat($('#top-coat').val().replace(",", ""))
		product_total += parseFloat($('#foam-coat').val().replace(",", ""))
		product_total += parseFloat($('#adhesive-coat').val().replace(",", ""))
		net_wt = parseFloat($('#net-wt').val());
		fabric_wt = $('#fabric-wt').val();
		fabric = $('#fabric').val();
		value = $('#'+type).val();
		$('#gross-wt').val(product_total);
		if(value != ''){
			if(fabric != '' && fabric_wt != ''){
				if(product_total<net_wt){
					alert('Net weight is greater than mixing total');
					$('#net-wt').val('');
				}
			}else{
				alert('Please complete above detail first');
			}
		}
	}
</script>
<script type="text/javascript">
	function getValue(type,option,i){
		var options =[];
		var ids = []
		if(i == 0){
			options[0] = option;
			ids[0] = $('#'+option).val();
		}else if(i == 1){
			options[0] = 'brand';
			ids[0] = $('#brand').val();
			options[1] = option;
			ids[1] = $('#'+option).val();
		}else if(i == 2){
			options[0] = 'brand';
			ids[0] = $('#brand').val();
			options[1] = 'color';
			ids[1] = $('#color').val();
			options[2] = option;
			ids[2] = $('#'+option).val();
		}else if(i == 3){
			options[0] = 'brand';
			ids[0] = $('#brand').val();
			options[1] = 'color';
			ids[1] = $('#color').val();
			options[2] = 'emboss';
			ids[2] = $('#emboss').val();
			options[3] = option;
			ids[3] = $('#'+option).val();
		}
		else if(i == 4){
			options[0] = 'brand';
			ids[0] = $('#brand').val();
			options[1] = 'color';
			ids[1] = $('#color').val();
			options[2] = 'emboss';
			ids[2] = $('#emboss').val();
			options[3] = 'rpaper';
			ids[3] = $('#rpaper').val();
			options[4] = option;
			ids[4] = $('#'+option).val();
		}
		else if(i == 5){
			options[0] = 'brand';
			ids[0] = $('#brand').val();
			options[1] = 'color';
			ids[1] = $('#color').val();
			options[2] = 'emboss';
			ids[2] = $('#emboss').val();
			options[3] = 'rpaper';
			ids[3] = $('#rpaper').val();
			options[4] = 'fabric';
			ids[4] = $('#fabric').val();
			options[5] = option;
			ids[5] = $('#'+option).val();
		}
		$("#"+type).empty();
		// alert(option);
		$.post('<?php echo site_url("rexin_coating_report/getValue")?>',{type:type,options:options,ids:ids},function(data){
			$("#"+type).append
                    ($('<option>',{
                        value:'',
                        text: '-- select option --'
                    }));
			$.each(data,function(index,value){
                if(type == 'rpaper'){
                	$("#"+type).append
	                    ($('<option>',{
	                        value:value[type+'_id'],
	                        text: value['rp_name']
                    }));
                }else{
	                $("#"+type).append
	                    ($('<option>',{
	                        value:value[type+'_id'],
	                        text: value[type+'_name']
	                    }));
	                }
					if(option == 'fabric'){
						$('#fabric-wt').val(value.weight);
						$('#fabric-wt1').val(value.weight);
					}
                });
		},'json');
	}
</script>
<script type="text/javascript">
	function deleteReport(id){
		$.post('<?php echo site_url("rexin_coating_report/delete_json")?>',{id:id},function(data){
			if(data){
					alert('Report deleted');
				location.reload();
			}else{
				alert('Error occur');
			}
		},'json');
	}
</script>