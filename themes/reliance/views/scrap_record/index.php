<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">List</a></li>
                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <button id="add-material" class="btn btn-primary margin">Add</button>
                    <!-- dealer table -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Scrap Record</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($scrap_records as $scrap) { ?>
                                            <tr>
                                                <td><?php echo $scrap['date_np'] ?></td>
                                                <td><?php echo $scrap['type_name'] ?></td>
                                                <td><?php echo $scrap['amount'] ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-warning edit_scrap" id="edit-<?php echo $scrap['sr_id']?>" value="<?php echo $scrap['sr_id']?>"> Edit</button>
                                                    <button type="button" class="btn btn-danger delete_scrap" id="delete-<?php echo $scrap['sr_id']?>" value="<?php echo $scrap['sr_id']?>"> Delete</button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    <!-- dealer table end -->
                    
                        
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
    </div>
</section>

<div class="modal fade" id="member-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="dealer-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Date*</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date-np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date-en">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tyoe" class="col-sm-4 control-label">Type*</label>
                            <div class="col-sm-8">
                                <select name="type_id" id='type-id'class="form-control">
                                    <option value="">-- select Type --</option>
                                    <?php foreach($department_scraps as $scrap){?>
                                    <option value="<?php echo $scrap['type_id']?>"><?php echo $scrap['type_name']?></option>
                                    <?php }?>
                                </select>
                                <span class="error_message" id="error-category" style="color: red" hidden="true">Category is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="amount" class="col-sm-4 control-label">Amount*</label>
                            <div class="col-sm-8">
                                <input type="number" name="amount" class="form-control" id="amount" placeholder="Amount" min="0">
                                <span class="error_message" id="error-unit" style="color: red" hidden="true">Please select the unit</span>
                            </div>
                        </div>
                        <input type="hidden" name="sr_id" id="sr-id" value="">

                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $('#date-np').nepaliDatePicker({
            ndpEnglishInput: 'date-en'
        });
        $('#example2').DataTable({
            "order":[[0, "desc"]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
        });
    });
</script>
<script>
    $("#form-submit").click(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('scrap_record/save') ?>",
            data: $("#dealer-form").serializeArray(),
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
//                    $('#error-dealer-name').html(data.msg.dealer_name);
//                    $('#error-contact').html(data.msg.contact);
//                    $('#error-mobile').html(data.msg.mobile);

                        if($('#material_name').val() == ''){
                            $("#error-name").show();
                        }else{
                            $("#error-name").hide();
                        }
                        if($('#category-id').val() == ''){
                            $("#error-category").show();
                        }else{
                            $("#error-category").hide();
                        }
                        if($('#units').val() == ''){
                            $("#error-unit").show();
                        }else{
                            $("#error-unit").hide();
                        }
                        //$(".error_message").show();
                } else {
                    location.reload();
                }
            },
        });
    });
</script>
<script>
    $('#add-material').click(function () {
        $('#myModalLabel').text('Add Material');
        $(".error_message").hide();
        $('#material_name').val('');
        $('#member-dialog').modal();
    });
</script>
<script>
    $(".edit_scrap").click(function(){
        id = $(this).val();
        $.post('<?php echo site_url()?>scrap_record/getScrap',{sr_id:id},function(data){
            $('#myModalLabel').text('Edit Material');
            $(".error_message").hide();
            $('#date-np').val(data.date_np);
            $('#date-en').val(data.date_en);
            $('#type-id').val(data.type_id);
            $('#amount').val(data.amount);
            $('#sr-id').val(data.sr_id);
            $('#member-dialog').modal();
            },'json');
    });
</script>
<script>
    $(".delete_scrap").click(function(){
       if(confirm('Do you want to delete this record?')){
           id = $(this).val();
           $.post('<?php echo site_url()?>scrap_record/delete_json',{id:id},function(data){
               if(data){
                   alert('Data deleted');
                   location.reload();
               }else{
                   alert('Error occured');
               }
           });
       } 
    });
</script>