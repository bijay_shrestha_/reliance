<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <div class="col-md-4">
                        <button class="btn btn-primary" id="add-btn">Add</button>
                    </div>
                    <div class="col-md-4">
                        <ul class="nav nav-stacked">
                            <li><a href="<?php echo site_url('lab_report/index/3')?>">Report to update of 3 Day <span class="pull-right badge bg-blue"><?php echo $report_for_update['3']?></span></a></li>
                            <li><a href="<?php echo site_url('lab_report/index/7')?>">Report to update of 7 Day <span class="pull-right badge bg-blue"><?php echo $report_for_update['7']?></span></a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="nav nav-stacked">
                            <li><a href="<?php echo site_url('lab_report/index/28')?>">Report to update of 28 Day <span class="pull-right badge bg-blue"><?php echo $report_for_update['28']?></span></a></li>
                            <li><a href="<?php echo site_url('lab_report')?>">All Report</a></li>
                        </ul>
                    </div>
                    <table id="lab_report_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sampled Date</th>
                                <th>Brand Name</th>
                                <th>Loss of Ignition</th>
                                <th>Magensium Oxide (MgO)</th>
                                <th>Insolube Residue</th>
                                <th>Sulphur Trioxide (SO3)</th>
                                <th>Chloried</th>
                                <th>Sulphide Suplher (S)</th>
                                <th>Fineness (M2/Kg)</th>
                                <th>Setting time Initial (Minimum)</th>
                                <th>Setting Time Final (Maximum)</th>
                                <th>Expansion - Le-Chaterlier Method (MM) (Max)</th>
                                <th>Expansion - Auto Clave Method (%) (Max)</th>
                                <th>Strength 1 Day</th>
                                <th>Strength 3 Day</th>
                                <th>Strength 7 Day</th>
                                <th>Strength 28 Day</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $value) {?>
                            <tr>
                                <td><?php echo $value['sample_date_np']?></td>
                                <td><?php echo $value['brand_name']?></td>
                                <td><?php echo number_format($value['loss_of_ignition'],2)?></td>
                                <td><?php echo number_format($value['magenisum_oxide'],2)?></td>
                                <td><?php echo number_format($value['insoluble_residue'],2)?></td>
                                <td><?php echo number_format($value['sulphur_trioxide'],2)?></td>
                                <td><?php echo number_format($value['chloride'],2)?></td>
                                <td><?php echo number_format($value['sulphide_sulphur'],2)?></td>
                                <td><?php echo number_format($value['fineness'],2)?></td>
                                <td><?php echo number_format($value['setting_time_initial'],2)?></td>
                                <td><?php echo number_format($value['setting_time_final'],2)?></td>
                                <td><?php echo number_format($value['expansion_lc_method'],2)?></td>
                                <td><?php echo number_format($value['expansion_ac_method'],2)?></td>
                                <td><?php echo number_format($value['strength1'],2)?></td>
                                <td><?php echo number_format($value['strength3'],2)?></td>
                                <td><?php echo number_format($value['strength7'],2)?></td>
                                <td><?php echo number_format($value['strength28'],2)?></td>
                                <td>
                                    <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger" onclick="delete_brand(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add report -->
<div class="modal fade" id="report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-4">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date(YYYY-MM-DD)">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="Date(YYYY-MM-DD)">
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                                <div id="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                            </div>
                            <label for="collection_date" class="col-sm-2 control-label">Sample Collection Date</label>
                            <div class="col-sm-4">
                                <input type="text" name="sample_date_np" class="form-control" id="sample_date_np" placeholder="Sample Date(YYYY-MM-DD)">
                                <input type="hidden" name="sample_date_en" class="form-control" id="sample_date_en" placeholder="Date(YYYY-MM-DD)">
                                <script>
                                    $('#sample_date_np').change(function(){
                                        $('#sample_date_en').val(BS2AD($('#sample_date_np').val()));

                                        var date1 = new Date($('#sample_date_en').val());
                                        var date2 = new Date();
                                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                        if(diffDays >= 3) {
                                            $('#div_strength3').show();
                                        }else{
                                            $('#div_strength3').hide();
                                        }
                                        if(diffDays >= 7){
                                            $('#div_strength7').show();
                                        }else{
                                            $('#div_strength7').hide();
                                        }
                                        if(diffDays >= 28){
                                            $('#div_strength28').show();
                                        }else{
                                            $('#div_strength28').hide();
                                        }
                                     });
                                </script>
                                <div id="error_sample_date_np" hidden style="color: red" class="error-div">Sample Collection Date is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <!-- <label for="brand_type" class="col-sm-2 control-label">Type</label>
                            <div class="col-sm-4">
                                <select class="form-control" name='type_id' id="type_id">
                                    <option value="">-- select brand type --</option>
                                <?php foreach ($brand_types as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                                <?php }?>
                                </select>
                            </div> -->
                            <label for="brand" class="col-sm-2 control-label">Brand</label>
                            <div class="col-sm-4">
                                <select class="form-control" name='brand_id' id="brand_id">
                                    <option value="">-- select brand --</option>
                                <?php foreach ($brands as $key => $value) {?>
                                    <option value="<?php echo $value['id']?>"><?php echo $value['brand_name']?></option>
                                <?php }?>
                                </select>
                                <div id="error_brand_id" hidden style="color: red" class="error-div">Brand Name is required</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <strong>Testing Parameter</strong>
                        </div>
                        <div class="col-md-4">
                            <strong>Test Result</strong>
                        </div>
                        <div class="col-md-4">
                            <strong>Requirement (NS 384)</strong>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="loss_of_ignition"><?php echo $testing_parameter[0]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="loss_of_ignition" id="loss_of_ignition" class="form-control" placeholder="<?php echo $testing_parameter[0]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $testing_parameter[0]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="magenisum_oxide"><?php echo $testing_parameter[1]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="magenisum_oxide" id="magenisum_oxide" class="form-control" placeholder="<?php echo $testing_parameter[1]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $testing_parameter[1]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="insoluble_residue"><?php echo $testing_parameter[2]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="insoluble_residue" id="insoluble_residue" class="form-control" placeholder="<?php echo $testing_parameter[2]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $testing_parameter[2]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="sulphur_trioxide"><?php echo $testing_parameter[3]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="sulphur_trioxide" id="sulphur_trioxide" class="form-control" placeholder="<?php echo $testing_parameter[3]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $testing_parameter[3]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="chloride"><?php echo $testing_parameter[4]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="chloride" id="chloride" class="form-control" placeholder="<?php echo $testing_parameter[4]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $testing_parameter[4]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="sulphide_sulphur"><?php echo $testing_parameter[5]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="sulphide_sulphur" id="sulphide_sulphur" class="form-control" placeholder="<?php echo $testing_parameter[5]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $testing_parameter[5]['value']?>
                            </div>
                        </div>

                        <hr>
                        <div class="col-md-4">
                            <strong>Physical Testing</strong>
                        </div>
                        <div class="col-md-4">
                            <strong>Test Result</strong>
                        </div>
                        <div class="col-md-4">
                            <strong>Requirement (NS 384)</strong>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="fineness"><?php echo $physical_parameter[0]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="fineness" id="fineness"class="form-control" placeholder="<?php echo $physical_parameter[0]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[0]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="setting_time_initial"><?php echo $physical_parameter[1]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="setting_time_initial" id="setting_time_initial"class="form-control" placeholder="<?php echo $physical_parameter[1]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[1]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="setting_time_final"><?php echo $physical_parameter[2]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="setting_time_final" id="setting_time_final"class="form-control" placeholder="<?php echo $physical_parameter[2]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[2]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="expansion_lc_method"><?php echo $physical_parameter[3]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="expansion_lc_method" id="expansion_lc_method"class="form-control" placeholder="<?php echo $physical_parameter[3]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[3]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="expansion_ac_method"><?php echo $physical_parameter[4]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="expansion_ac_method" id="expansion_ac_method"class="form-control" placeholder="<?php echo $physical_parameter[4]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[4]['value']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="strength1"><?php echo $physical_parameter[5]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="strength1" id="strength1"class="form-control" placeholder="<?php echo $physical_parameter[5]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[5]['value']?>
                            </div>
                        </div>
                        <div class="form-group" id="div_strength3" hidden>
                            <div class="col-sm-4">
                                <label for="strength3"><?php echo $physical_parameter[6]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="strength3" id="strength3"class="form-control" placeholder="<?php echo $physical_parameter[6]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[6]['value']?>
                            </div>
                        </div>
                        <div class="form-group" id="div_strength7" hidden>
                            <div class="col-sm-4">
                                <label for="strength7"><?php echo $physical_parameter[7]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="strength7" id="strength7"class="form-control" placeholder="<?php echo $physical_parameter[7]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[7]['value']?>
                            </div>
                        </div>
                        <div class="form-group" id="div_strength28" hidden>
                            <div class="col-sm-4">
                                <label for="strength28"><?php echo $physical_parameter[8]['parameter']?> </label>
                            </div>
                            <div class="col-md-4">
                                <input type="number" name="strength28" id="strength28"class="form-control" placeholder="<?php echo $physical_parameter[8]['parameter']?>">
                            </div>
                            <div class="col-md-4">
                                <?php echo $physical_parameter[8]['value']?>
                            </div>
                        </div>

                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#lab_report_list').DataTable({
            "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            "columnDefs": [
            <?php for ($i = 2; $i < 8; $i++){?>
                {
                    "targets": [ <?php echo $i ?> ],
                    "visible": false,
                    "searchable": false
                },
            <?php }?>
            ]
        });

        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });

        $('#sample_date_np').nepaliDatePicker({
            ndpEnglishInput: 'sample_date_en',
            onChange: function(){
                var date1 = new Date($('#sample_date_en').val());
                var date2 = new Date();
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if(diffDays >= 3) {
                    $('#div_strength3').show();
                }else{
                    $('#div_strength3').hide();
                }
                if(diffDays >= 7){
                    $('#div_strength7').show();
                }else{
                    $('#div_strength7').hide();
                }
                if(diffDays >= 28){
                    $('#div_strength28').show();
                }else{
                    $('#div_strength28').hide();
                }
            }
        });
        
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('input').val('');
        $('option').attr('selected', false);
        $('.modal-title').html('Lab Report');
        
        $('#div_strength3').hide();
        $('#div_strength7').hide();
        $('#div_strength28').hide();
        $('.error-div').hide();

        $('#report-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var i = 0;
        var date_np = $('#date_np').val();
        var sample_date_np = $('#sample_date_np').val();
        var brand_id = $('#brand_id').val();

        if(date_np == ''){
            i = 1;
            $('#error_date_np').show();
        }else{
            $('#error_date_np').hide();
        }

        if(sample_date_np == ''){
            i = 1;
            $('#error_sample_date_np').show();
        }else{
            $('#error_sample_date_np').hide();
        }
        if(brand_id == ''){
            i = 1;
            $('#error_brand_id').show();
        }else{
            $('#error_brand_id').hide();
        }

        if(i == 0){
            $.post('<?php echo site_url("lab_report/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.modal-title').html('Lab Report');
        $.post('<?php echo site_url("lab_report/get_value")?>',{id:id},function(data){
            $('#div_strength3').hide();
            $('#div_strength7').hide();
            $('#div_strength28').hide();
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });

            var date1 = new Date($('#sample_date_en').val());
            var date2 = new Date();
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if(diffDays >= 3) {
                $('#div_strength3').show();
            }
            if(diffDays >= 7){
                $('#div_strength7').show();
            }
            if(diffDays >= 28){
                $('#div_strength28').show();
            }
            $('.error-div').hide();

            $('#report-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_brand(id){
        $.post('<?php echo site_url("lab_report/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>