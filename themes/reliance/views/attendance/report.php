<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">

<?php if(count($rows) > 0){?>
	<button id="edit" class="btn btn-primary margin">Edit</button>
	<button id="delete" class="btn btn-danger margin">Delete</button>
<?php }?>
<table id="example" class="table table-bordered table-striped">
	            <thead>
	                <tr>
	                	<th>Date</th>
	                    <th>Department</th>
	                    <th>Day Staff</th>
	                    <th>Day Contract</th>
	                    <th>Night Staff</th>
	                    <th>Night Contract</th>
	                </tr>
	            </thead>
	            <tbody>
	                <?php foreach ($rows as $row) { ?>
	                    <tr>
	                        <td><?php echo $row['date_np']?></td>
	                        <td><?php echo $row['name'] ?></td>
	                        <td><?php echo ($row['day_staff'])?$row['day_staff']:'0' ?></td>
	                        <td><?php echo ($row['day_contract'])?$row['day_contract']:'0' ?></td>
	                        <td><?php echo ($row['night_staff'])?$row['night_staff']:'0'?></td>
	                        <td><?php echo ($row['night_contract'])?$row['night_contract']:'0'?></td>
	                    </tr>
	                <?php } ?>
	            </tbody>
	        </table>

	        <script type="text/javascript">
	        	$('#example').dataTable({
					"order": [[0,'desc'],[1,'asc']],
					dom: 'lBfrtip',
		            buttons: [
		                {
		                    extend: 'excelHtml5',
		                },
		            ],
				});
	        </script>
	        <script type="text/javascript">
	        	$('#edit').click(function(){
	        		var date_np = $('#date-np').val();
	        		var date_en = $('#date-en').val();
	        		$.post('<?php echo site_url("attendance/json")?>',{date:date_np,date_en:date_en,edit:1},function(data){
	        			$('#myModalLabel').text('Attendance Form');
				                $(".modal-body").html(data);
						$('#form-dialog').modal();
	        		},'html');
	        	});
	        </script>
	        <script type="text/javascript">
	        	$('#delete').click(function(){
	        		var date_np = $('#date-np').val();
	        		var date_en = $('#date-en').val();
	        		var r = confirm('Do you want to delete the records?');
	        		if(r){
	        			$.post('<?php echo site_url("attendance/delete_json")?>',{date:date_np,date_en:date_en,edit:1},function(data){
	        				console.log(data)
	        				if(data.success){
	        					alert('Record Deleted');
	        					location.reload();
	        				}else{
	        					alert('Error occur. please try again.')
	        				}
	        			},'json');
	        		}
	        	});
	        </script>