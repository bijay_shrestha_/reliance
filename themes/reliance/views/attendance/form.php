<?php /*echo '<pre>';
     print_r($rows);
echo '</pre>'; */?>
<br>
<div id='display-content'>
<form class="form-horizontal" id="report-form" action="#">
    <div class="form-group">
        <div class='col-md-2'>
            <label>Date</label>
        </div>
        <div class='col-md-4' id="date-div">
            <input type="text" <?php if($edit == !1){?>name="date_np"<?php }?> id="date_np_form" value="<?php echo $date_np?>" class="form-control" placeholder="Date" <?php if($edit == 1){echo 'readonly';}?>>
            <?php if($edit == 1){?>
            <input type="hidden" name="date_np" id="date_np_form1" value="<?php echo $date_np?>" class="form-control" placeholder="Date" >
            <?php }?>
            <input type="hidden" name="date_en" id="date_en" value="<?php echo $date_en?>" class="form-control">
            <span id="error-date" class="error-span" hidden>Date is required</span>
        </div>
    </div>
    <div class="form-group">
        <label for="date" class="col-sm-2"></label>
        <label for="date" class="col-sm-2">Day Staff</label>
        <label for="date" class="col-sm-3">Day Contract</label>
        <label for="date" class="col-sm-2">Night Staff</label>
        <label for="date" class="col-sm-3">Night Contract</label>
    </div>
    <?php 
    $i = 0;
    if(count($rows) > 0){
        foreach($departments as $department){?>
            <div class="form-group">
                <label for="department" class="col-sm-2"><?php echo $department['name']?></label>
                <input type="hidden" name="department_id[]" class="form-control" value="<?php echo $department['department_id']?>">
                <div class="col-sm-2">
                    <input type="number" name="day_staff[]" class="form-control" placeholder="Day Staff" value=<?php echo (array_key_exists($department['department_id'], $rows))?$rows[$department['department_id']]['day_staff']:''?>>
                </div>
                <div class="col-sm-3">
                    <input type="text" name="day_contract[]" class="form-control" id="day-contract" placeholder="Day Contract" value=<?php echo (array_key_exists($department['department_id'], $rows))?$rows[$department['department_id']]['day_contract']:''?>>
                </div>
                <div class="col-sm-2">
                    <input type="number" name="night_staff[]" class="form-control" placeholder="Night Staff" value=<?php echo (array_key_exists($department['department_id'], $rows))?$rows[$department['department_id']]['night_staff']:''?>>
                </div>
                <div class="col-sm-3">
                    <input type="text" name="night_contract[]" class="form-control" id="night-contract" placeholder="Night Contract" value=<?php echo (array_key_exists($department['department_id'], $rows))?$rows[$department['department_id']]['night_contract']:''?>>
                </div>
                <!-- <input type="hidden" name="id[]" id="id" value=""> -->
            </div>
        <?php 
            $i++;
        }
    }else{
        foreach($departments as $department){?>
            <div class="form-group">
                <label for="department" class="col-sm-2"><?php echo $department['name']?></label>
                <input type="hidden" name="department_id[]" class="form-control" value="<?php echo $department['department_id']?>">
                <div class="col-sm-2">
                    <input type="number" name="day_staff[]" class="form-control" placeholder="Day Staff">
                </div>
                <div class="col-sm-3">
                    <input type="text" name="day_contract[]" class="form-control" id="day-contract" placeholder="Day Contract">
                </div>
                <div class="col-sm-2">
                    <input type="number" name="night_staff[]" class="form-control" placeholder="Night Staff">
                </div>
                <div class="col-sm-3">
                    <input type="text" name="night_contract[]" class="form-control" id="night-contract" placeholder="Night Contract">
                </div>
                <!-- <input type="hidden" name="id[]" id="id" value=""> -->
            </div>
        <?php 
            $i++;
        }
    }?>
                        




    

    <div class="modal-footer">
        <button type="button" id="form-submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
</form>

<script type="text/javascript">
    $('#form-submit').click(function(){
        var i = 0;
        var date = $('#date_np_form').val();
        if(date == ''){
            $('#date-div').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date-div').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(i == 1){
            $.post('<?php echo site_url("attendance/save")?>',$("#report-form").serializeArray(),function(){
                var date = $('#date-np').val();
                    var date_en = $('#date-ep').val();
                    $.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
                        $('#display-div').html(data);
                    },'html');
                    location.reload();
            },'json');
        }
    })
</script>
<script type="text/javascript">
    $("#date_np_form").nepaliDatePicker();
</script>
</div>