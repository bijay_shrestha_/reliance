<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<input type="text" name='date_np' id="date-np" class="form-control margin">
	    			<input type="hidden" name='date_en' id="date-en" class="form-control margin">
	    		</div>
	    		<div class="col-md-3">
	    			<a href="" class='btn btn-primary margin'>View All</a>
	    			<button class='btn btn-primary margin' id="add">Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="attendance-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date</th>
				                    <th>Department</th>
				                    <th>Day Staff</th>
				                    <th>Day Contract</th>
				                    <th>Night Staff</th>
				                    <th>Night Contract</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo $row['name'] ?></td>
				                        <td><?php echo ($row['day_staff'])?$row['day_staff']:'0' ?></td>
				                        <td><?php echo ($row['day_contract'])?$row['day_contract']:'0' ?></td>
				                        <td><?php echo ($row['night_staff'])?$row['night_staff']:'0'?></td>
				                        <td><?php echo ($row['night_contract'])?$row['night_contract']:'0'?></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of attendance form-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width: 80%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="diesel-form" method="post">
      	<div class="modal-body">
                        
            
        
        </div>
    
        <div class="modal-footer">
            <!-- <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#date-np').nepaliDatePicker({
			// ndpEnglishInput: 'date_en',
			onChange: function(){
				$('#date-en').val(BS2AD($('#date-np').val()));
				var date = $('#date-np').val();
				var date_en = $('#date-ep').val();
				$.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
					$('#display-div').html(data);
				},'html');
			}
		});
		$('#attendance-table').dataTable({
			"order": [0,'desc'],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
		$.post('<?php echo site_url("attendance/json")?>',{add:1},function(data){
			$('#myModalLabel').text('Attendance Form');
	                $(".modal-body").html(data);
			$('#form-dialog').modal();
		},'html');
	});
</script>