<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Material</a></li>
                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <button id="add-material" class="btn btn-primary margin">Add</button>
                    <!-- dealer table -->
                    <?php if (count($materials) > 0) { ?>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Material Table</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Material Name</th>
                                            <th>Category</th>
                                            <th>Units</th>
                                            <th>Dispatch Record From</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($materials as $material) { ?>
                                            <tr>
                                                <td><?php echo $material['material_name'] ?></td>
                                                <td><?php echo $material['category_name'] ?></td>
                                                <td><?php echo $material['unit'] ?></td>
                                                <td><?php echo ($material['dispatch_record_from'] == 0)?'Dispatch':'Consumption' ?></td>
                                                <td><button type="button" class="btn btn-warning edit_material" id="material-<?php echo $material['material_id']?>" value="<?php echo $material['material_id']?>"> Edit</button></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    <?php } else { ?>
                        <div class="callout callout-warning">
                            <h4>No Material</h4>
                        </div>
                    <?php } ?>
                    <!-- dealer table end -->
                    
                        
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
    </div>
</section>

<div class="modal fade" id="member-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="dealer-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="material_name" class="col-sm-4 control-label">Name*</label>
                            <div class="col-sm-8">
                                <input type="text" name="material_name" class="form-control" id="material_name" placeholder="Material Name">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                                <span class="error_message" id="error-duplicate" style="color: red" hidden="true">Duplicate Entry</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="category" class="col-sm-4 control-label">Category*</label>
                            <div class="col-sm-8">
                                <select name="category_id" id='category-id'class="form-control">
                                    <option value="">-- select category --</option>
                                    <?php foreach($categories as $category){?>
                                    <option value="<?php echo $category['category_id']?>" id="category-option-<?php echo $category['category_id']?>"><?php echo $category['category_name']?></option>
                                    <?php }?>
                                </select>
                                <span class="error_message" id="error-category" style="color: red" hidden="true">Category is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="units" class="col-sm-4 control-label">Unit*</label>
                            <div class="col-sm-8">
                                <select id='units' class="form-control" name="unit">
                                    <option value="">-- select unit --</option>
                                    <?php foreach($units as $unit){?>
                                    <option id="unit-option-<?php echo $unit['unit']?>"><?php echo $unit['unit']?></option>
                                    <?php }?>
                                </select>
                                <span class="error_message" id="error-unit" style="color: red" hidden="true">Please select the unit</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="dispatch_record_from" class="col-sm-4 control-label">Dispatch Record From</label>
                            <div class="col-sm-8">
                                <select id='dispatch_record_from' class="form-control" name="dispatch_record_from">
                                    <option value="0" id=dispatch_record_0>Dispatch</option>
                                    <option value="1" id=dispatch_record_1>Consumption</option>
                                </select>
                                <!-- <span class="error_message" id="error-unit" style="color: red" hidden="true">Please select the unit</span> -->
                            </div>
                        </div>

                        <input type="hidden" name="material_id" id="material_id" value="">
                        <input type="hidden" name="status" id="status" value="1">

<!--                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Status:</label> &nbsp;&nbsp;
                                    </div>
                                    <div class="col-md-8">
                                        <input type="radio" id="active" name="status" id="status1" value="1"> Active&nbsp;&nbsp;
                                        <input type="radio" id="inactive" name="status" id="status2" value="0"> Inactive
                                    </div>
                                </div>
                            </td>
                        </tr>-->

                        <?php /* <input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>"> */ ?>

                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $('#example2').DataTable();
    });
</script>
<script>
    $("#form-submit").click(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('material/save') ?>",
            data: $("#dealer-form").serializeArray(),
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
//                    $('#error-dealer-name').html(data.msg.dealer_name);
//                    $('#error-contact').html(data.msg.contact);
//                    $('#error-mobile').html(data.msg.mobile);
                        
                        if(data.msg == 'Duplicate Entry'){
                            alert(data.msg);
                            $('#error-duplicate').show();
                        }else{
                            $("#error-duplicate").hide();
                        }
                        if($('#material_name').val() == ''){
                            $("#error-name").show();
                        }else{
                            $("#error-name").hide();
                        }
                        if($('#category-id').val() == ''){
                            $("#error-category").show();
                        }else{
                            $("#error-category").hide();
                        }
                        if($('#units').val() == ''){
                            $("#error-unit").show();
                        }else{
                            $("#error-unit").hide();
                        }
                        //$(".error_message").show();
                } else {
                    location.reload();
                }
            },
        });
    });
</script>
<script>
    $('#add-material').click(function () {
        $('#myModalLabel').text('Add Material');
        $(".error_message").hide();
        $('#material_name').val('');
        $('#category-id').val('');
        $('#units').val('');
        $('#dispatch_record_0').prop('selected',true);
        $('#member-dialog').modal();
    });
</script>
<script>
    $(".edit_material").click(function(){
        id = $(this).val();
        $.post('<?php echo site_url()?>material/getMaterial',{material_id:id},function(data){
            $('#myModalLabel').text('Edit Material');
            $(".error_message").hide();
            $('#material_name').val(data.material_name);
            $('#material_id').val(data.material_id);
            $('#category-option-'+data.category_id).prop('selected',true);
            $('#unit-option-'+data.unit).prop('selected',true);
            $('#dispatch_record_'+data.dispatch_record_from).prop('selected',true);
            $('#member-dialog').modal();
            },'json');
    });
</script>