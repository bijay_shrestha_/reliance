<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
  .error{
    color:red;
  }
</style>

<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
      <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Add Time Loss</a></li>
          </ul>
          <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
          <section class="content">
          <form id='form'>
            <!--start date-->
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="start_date" class="control-label">Date</label>
                        <input type="text" name="date" class="form-control" id="start-date" value="<?php echo $tl['date_np']?>" placeholder="Start Date">
                        <input type="hidden" name="en_date" class="form-control" id="eng-start-date" value="<?php echo $tl['date_en']?>" placeholder="Start Date">
              <div id="error-start" class="error" hidden>Start date and time is required</div>
                </div>
              </div>
          </div>
          <div class="row">
                <div class="form-group">
                    <div class='col-md-12'>
                    <label for="start_date" class="control-label">Start Time</label>
                    </div>
                      <div class='col-md-2'>
                        <select name='start_hr' onchange='totalTime()' class="form-control select2" id='start-hr'>
                          <!-- <option value="">hr</option> -->
                          <?php for($i = 0;$i < 24; $i++){?>
                            <option <?php if($i == $tl['start_hr'])echo 'selected'?> value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">hr</div>
                      <div class='col-md-2'>
                        <select name='start_min' onchange='totalTime()' class="form-control select2" id='start-min'>
                          <!-- <option value="">min</option> -->
                          <?php for($i = 0;$i < 60; $i++){?>
                            <option <?php if($i == $tl['start_min'])echo 'selected'?> value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">min</div>
              </div>
          </div>

          <!-- end date -->
          <div class="row">
              
                    <div class='col-md-12'>
                    <label for="end_date" class="control-label">End Time</label>
                    </div>
                      <div class='col-md-2'>
                        <select name='end_hr' onchange='totalTime()'  class="form-control select2" id='end-hr' >
                          <!-- <option value="">hr</option> -->
                          <?php for($i = 0;$i < 24; $i++){?>
                            <option <?php if($i == $tl['end_min'])echo 'selected'?> value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">hr</div>
                      <div class='col-md-2'>
                        <select name='end_min' onchange='totalTime()'  class="form-control select2" id='end-min' ">
                          <!-- <option value="">min</option> -->
                          <?php for($i = 0;$i < 60; $i++){?>
                            <option <?php if($i == $tl['end_min'])echo 'selected'?> value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">min</div>
              </div>
          </div>
          <div class='row'>
            <div class='col-md-6'>
              <label for="end_date" class="control-label">Total Time:</label>
              <input type='text' id='total-time' disabled class='form-control' value="<?php echo floor($tl['duration']/60).'hr '. ($tl['duration']%60).'min'?>">
            </div>
          </div>

          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Shift</label>
                  <select name="shift" class="form-control select2" id="shift" style="width: 100%;">
                    <option selected="selected" value=''>-- Select Shift --</option>
                    <option value='A' <?php echo ($tl['shift'] == 'A')?'selected':''?>>A</option>
                    <option value='B' <?php echo ($tl['shift'] == 'B')?'selected':''?>>B</option>
                  </select>
                <div id="error-type" class="error" hidden>Type is required</div>
                </div>
              </div>
          </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Loss Type</label>
                  <select name="type_id" class="form-control select2" id="type" style="width: 100%;">
                    <option selected="selected" value=''>-- Select Type --</option>
                    <?php foreach($types as $type){?>
                      <option <?php echo ($tl['type_id'] == $type['type_id'])?'selected':''?> value='<?php echo $type['type_id']?>'><?php echo $type['loss_type']?></option>
                    <?php }?>
                  </select>
                <div id="error-type" class="error" hidden>Type is required</div>
                </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Reason</label>
                  <select name="reason_id" class="form-control select2" id="reason" style="width: 100%;">
                    <option selected="selected" value=''>-- Select Reason --</option>
                    <?php foreach($reasons as $reason){?>
                      <option <?php echo ($tl['reason_id'] == $reason['reason_id'])?'selected':''?> value='<?php echo $reason['reason_id']?>'><?php echo $reason['reason']?></option>
                    <?php }?>
                  </select>
                <div id="error-reason" class="error" hidden>Reason is required</div>
                </div>
              </div>
          </div>
<input type="hidden" name='tl_id' value="<?php echo $tl['tl_id']?>">
          <div class="row">
            <div class="col-md-6">
              <button id="button" class="btn btn-primary" type='button'>Save</button>
              <a href="<?php echo site_url('time_loss/getDepartmentTimeLoss')?>" class="btn btn-danger" type='button'>Back</a>
            </div>
          </div>
          </form>
          </section>
       

<script type="text/javascript">
$('#type').change(function(){
  var type = $(this).val();
  $.post("<?php echo site_url('time_loss/getReason')?>",{type:type},function(data){
    // console.log(data.reasons);
    $("#reason").empty();
      $("#reason").append(
        $('<option>',{
          value:"",
          text:"-- Select option --"
        }));
      $.each(data.reasons,function(index,value){
        $("#reason").append
        // console.error(value);
        ($('<option>',{
          value:value.reason_id,
          text:value.reason
        }));
      });
  },'json')
})
</script>
<script type="text/javascript">
    $(function () {
        $('#start-date').nepaliDatePicker({
          ndpEnglishInput: 'eng-start-date'
        });
        $('#end-date').nepaliDatePicker({
          ndpEnglishInput: 'eng-end-date'
        });
    });
</script>
<script type="text/javascript">
  $("#button").click(function(){
    var ready = 1;
    if($('#type').val() == ''){
      ready = 0;
      $('#error-type').show();
    }else{
      $('#error-type').hide();
    }
    if($('#reason').val() == ''){
      ready = 0;
      $('#error-reason').show();
    }else{
      $('#error-reason').hide();
    }
    if($('#start-date').val() == '' || $('#start-hr').val() == '' || $('#start-min').val() == ''){
      ready = 0;
      $('#error-start').show();
    }else{
      $('#error-start').hide();
    }
    if( $('#end-hr').val() == '' || $('#end-min').val() == ''){
      ready = 0;
      $('#error-start').show();
    }else{
      $('#error-start').hide();
    }
    if(ready == 0){
      return false;
    }
      
          $.post('<?php echo site_url("time_loss/saveTimeLoss")?>',$("#form").serializeArray(),function(data){
            if(data){
              alert('Timeloss edited');
              location.reload();
            }else{
              alert('Please try again');
            }
          },'json');
    
  });
</script>
<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
  table = $('.table').DataTable();
</script>
<script type="text/javascript">
  function totalTime(){
    total = 0;
    start_hr = parseInt($('#start-hr').val());
    start_min = parseInt($('#start-min').val());
    end_hr = parseInt($('#end-hr').val());
    end_min = parseInt($('#end-min').val());
    if(isNaN(start_hr) || isNaN(start_min) || isNaN(end_hr) || isNaN(end_min)){
      total_val = '00 hr 00 min'
      $('#total-time').val(total_val);
    }else{
      if(start_hr > end_hr){
        total = (24*60)-(start_hr*60 + start_min)+(end_hr*60+end_min);
        total_hr = Math.floor(total / 60);
        total_min = total % 60;
        total_val = total_hr +' hr '+ total_min+' min';
      $('#total-time').val(total_val);
      }
      else{
        total = (end_hr*60+end_min)-(start_hr*60 + start_min);
        total_hr = Math.floor(total / 60);
        total_min = total % 60;
        total_val = total_hr +' hr '+ total_min+' min';
      $('#total-time').val(total_val);
      }
    }
  }
</script>
