<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">

<form action="<?php echo ($this->session->userdata('group_id')==3)?site_url('time_loss/report'):site_url('time_loss/getDepartmentTimeLoss')?>">
	<input type='text' name='date' id="date" placeholder="Select date"><button class="btn btn-primary margin">View Report</button>
</form>

<?php foreach($types as $type){?>
	<?php $total = 0;?>
	<table id="example3" class="table table-bordered table-striped">
	    <thead>
	        <tr>
	          <th>Type</th>
	          <th>Reason</th>
	          <th>Department</th>
	          <th>Start Time</th>
	          <th>End Time</th>
	          <th>Duration</th>
	          <th>percentage</th>
	          <?php /*if($this->session->userdata('department_id') != 7){?><th>Action</th><?php }*/?>
	        </tr>
	    </thead>
	    <?php if(array_key_exists($type['type_id'], $time_losses)){?>
	    <tbody>
	    <?php foreach($time_losses[$type['type_id']] as $tl){?>
	    	<tr>
	    		<td><?php echo $tl['loss_type']?></td>
	    		<td><?php echo $tl['reason']?></td>
	    		<td><?php echo $tl['department']?></td>
	    		<td><?php echo (($tl['start_hr']<10)?'0':'').$tl['start_hr'].':'.(($tl['start_min']<10)?'0':'').$tl['start_min']?></td>
	    		<td><?php echo (($tl['end_hr']<10)?'0':'').$tl['end_hr'].':'.(($tl['end_min']<10)?'0':'').$tl['end_min']?></td>
	    		<td><?php echo floor ( $tl['duration'] / 60 )?>hr <?php echo $tl['duration']%60?>min</td>
	    		<td><?php echo round($tl['tl_percent'], 2);?></td>
	    		<?php if($this->session->userdata('department_id') != 7){?>
	    		<?php /*<td>
	    			<button class='btn btn-warning' onclick='editForm(<?php echo $tl["tl_id"]?>)'><i class='fa fa-pencil'></i></button>
	    			<button class='btn btn-danger' onclick='delete(<?php echo $tl["tl_id"]?>)'><i class='fa fa-trash'></i></button>
	    		</td>*/?>
	    		<?php }?>
	    		<?php $total += $tl['duration']?>
    		</tr>
	    <?php }?>
	    </tbody>
	    <tr>
	    	<td></td>
	    	<td>Total</td>
	    	<td></td>
	    	<td></td>
	    	<td></td>
	    	<td><?php echo floor($total/60)?>hr <?php echo $total%60?>min</td>
	    	<td></td>
	    	<?php if($this->session->userdata('department_id') != 7){?>
	    	<td></td>
	    	<?php }?>
    	</tr>
    	<?php }else{?>
    		<tbody>
	    	</tbody>
    	<?php }?>
	</table>
	<hr>
<?php }?>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	table = $('.table').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
        });
</script>
<script type="text/javascript">
$(function(){
	 $('#date').nepaliDatePicker();
})
</script>
<script type="text/javascript">
	// for edit
	function editForm(id){
		$.post('<?php echo site_url("time_loss/getTimeLoss")?>',{id:id},function(data){7
			$('#myModalLabel').text('Edit Timeloss');
			$('#tl_id').val(id);
			$('#date').val(data.date_np);
			$('#en_date').val(data.date_en)
			$('#time-loss-model').modal();
		},'json')
	}
</script>


<!-- edit modal -->
<div class="modal fade" id="time-loss-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class='row'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="dealer-form" action="#">
                <div class="modal-body">
                    <!--start date-->
          <div class="box">
          <table class="table table-striped" cellpadding="3px" >
          <td>
          <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="start_date" class="control-label">Date</label>
                        <input type="text" name="date" class="form-control" id="start-date" placeholder="Start Date">
                        <input type="hidden" name="en_date" class="form-control" id="eng-start-date" placeholder="Start Date">
              <div id="error-start" class="error" hidden>Start date and time is required</div>
                </div>
              </div>
          </div>
          <div class="row">
                <div class="form-group">
                    <div class='col-md-12'>
                    <label for="start_date" class="control-label">Start Time</label>
                    </div>
                      <div class='col-md-2'>
                        <select name='start_hr' onchange='totalTime()' class="form-control select2" id='start-hr'>
                          <!-- <option value="">hr</option> -->
                          <?php for($i = 0;$i < 24; $i++){?>
                            <option value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">hr</div>
                      <div class='col-md-2'>
                        <select name='start_min' onchange='totalTime()' class="form-control select2" id='start-min'>
                          <!-- <option value="">min</option> -->
                          <?php for($i = 0;$i < 60; $i++){?>
                            <option value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">min</div>
              </div>
          </div>

          <!-- end date -->
          <div class="row">
              
                    <div class='col-md-12'>
                    <label for="end_date" class="control-label">End Time</label>
                    </div>
                      <div class='col-md-2'>
                        <select name='end_hr' onchange='totalTime()'  class="form-control select2" id='end-hr' >
                          <!-- <option value="">hr</option> -->
                          <?php for($i = 0;$i < 24; $i++){?>
                            <option value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">hr</div>
                      <div class='col-md-2'>
                        <select name='end_min' onchange='totalTime()'  class="form-control select2" id='end-min' ">
                          <!-- <option value="">min</option> -->
                          <?php for($i = 0;$i < 60; $i++){?>
                            <option value="<?php echo $i?>"><?php if($i<10){echo 0;}echo $i?></option>
                          <?php }?>
                        </select>
                      </div><div class="col-md-1">min</div>
              </div>
          <div class='row'>
            <div class='col-md-6'>
              <label for="end_date" class="control-label">Total Time:</label>
              <input type='text' id='total-time' value="00 hr 00 min" disabled class='form-control'>
            </div>
          </div>

          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Shift</label>
                  <select name="shift" class="form-control select2" id="shift" style="width: 100%;">
                    <option selected="selected" value=''>-- Select Shift --</option>
                    <option value='A'>A</option>
                    <option value='B'>B</option>
                  </select>
                <div id="error-type" class="error" hidden>Type is required</div>
                </div>
              </div>
          </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Loss Type</label>
                  <select name="type_id" class="form-control select2" id="type" style="width: 100%;">
                    <option selected="selected" value=''>-- Select Type --</option>
                    <?php foreach($types as $type){?>
                      <option value='<?php echo $type['type_id']?>'><?php echo $type['loss_type']?></option>
                    <?php }?>
                  </select>
                <div id="error-type" class="error" hidden>Type is required</div>
                </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Reason</label>
                  <select name="reason_id" class="form-control select2" id="reason" style="width: 100%;">
                    <option selected="selected" value=''>-- Select Reason --</option>
                  </select>
                <div id="error-reason" class="error" hidden>Reason is required</div>
                </div>
              </div>
          </div>


                        <input type="hidden" name="dealer_id" id="dealer_id" value="">
                        <input type="hidden" name="status" id="status" value="1">

<!--                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Status:</label> &nbsp;&nbsp;
                                    </div>
                                    <div class="col-md-8">
                                        <input type="radio" id="active" name="status" id="status1" value="1"> Active&nbsp;&nbsp;
                                        <input type="radio" id="inactive" name="status" id="status2" value="0"> Inactive
                                    </div>
                                </div>
                            </td>
                        </tr>-->

                        <?php /* <input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>"> */ ?>

                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>
                </td>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
