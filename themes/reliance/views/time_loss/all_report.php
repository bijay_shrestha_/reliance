<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<?php $total = 0;?>

<form action="<?php echo ($this->session->userdata('group_id')==3)?site_url('time_loss/report'):site_url('time_loss/getDepartmentTimeLoss')?>">
	<input type='text' name='date' id="date" placeholder="Select date" value="<?php echo $this->input->get('date')?>"><button class="btn btn-primary margin">View Report</button>
</form>
<a href="<?php echo site_url('time_loss')?>" type="button" class="btn btn-primary pull-right margin">Add</a>
<br>

<table id="example3" class="table table-bordered table-striped">
	    <thead>
	        <tr>
	          <th>Date</th>
	          <th>Shift</th>
	          <th>Department</th>
	          <th>Type</th>
	          <th>Reason</th>
	          <th>Start Time</th>
	          <th>End Time</th>
	          <th>Loss Time</th>
	          <th></th>
	          <th>Action</th>
	        </tr>
	    </thead>
	    <tfoot>
            <tr>
                <th colspan="7" style="text-align:right">Total:</th>
                <th></th>
            </tr>
        </tfoot>
	    
	    <tbody>
	    <?php foreach($time_losses as $tl){?>
	    	<tr>
	    		<td><?php echo $tl['date_np']?></td>
	    		<td><?php echo $tl['shift']?></td>
	    		<td><?php echo $department['name']?></td>
	    		<td><?php echo $tl['loss_type']?></td>
	    		<td><?php echo $tl['reason']?></td>
	    		<td><?php echo (($tl['start_hr']<10)?'0':'').$tl['start_hr'].':'.(($tl['start_min']<10)?'0':'').$tl['start_min']?></td>
	    		<td><?php echo (($tl['end_hr']<10)?'0':'').$tl['end_hr'].':'.(($tl['end_min']<10)?'0':'').$tl['end_min']?></td>
	    		<td><?php echo floor ( $tl['duration'] / 60 )?>hr <?php echo $tl['duration']%60?>min</td>
	    		<!-- <td><?php //echo round($tl['tl_percent'], 2);?></td> -->
	    		<?php if($this->session->userdata('department_id') != 7){?>
	    			<td><?php echo $tl['duration']?></td>
	    		<td>
	    			<a href='<?php echo site_url("time_loss/editForm/".$tl["tl_id"])?>' class='btn btn-warning'><i class='fa fa-pencil'></i></a>
	    			<a href='<?php echo site_url("time_loss/delete/".$tl["tl_id"])?><?php echo ($this->input->get('date'))? '?date='.$this->input->get('date'):'';?>'  class='btn btn-danger'><i class='fa fa-trash'></i></a>
	    		</td>
	    		<?php }?>
	    		<?php $total += $tl['duration']?>
    		</tr>
	    <?php }?>
	    </tbody>
	    	<!-- <tr>
		    	<td></td>
		    	<td>Total</td>
		    	<td></td>
		    	<td></td>
		    	<td></td>
		    	<td></td>
		    	<td></td>
		    	<td><?php //echo floor($total/60)?>hr <?php //echo $total%60?>min</td>
		    	<td></td>
    		</tr> -->
	</table>

	<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
$(function(){
	table = $('#example3').DataTable({
            dom: 'lBfrtip',
				"order":[[0, "desc"]],
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 8, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    page_total_hr = Math.floor(pageTotal/60);
                    page_total_min = pageTotal % 60;

                    total_hr = Math.floor(total/60);
                    total_min = total % 60;

                    // Update footer
                    $( api.column( 7 ).footer() ).html(
                        page_total_hr + 'hr ' + page_total_min + 'min<br> ' +' ( '+ total_hr + 'hr ' + total_min + 'min ' +' total)'
                    );
                },
		"columnDefs": [
                    {
                        "targets": [ 8 ],
                        "visible": false,
                        "searchable": false
                    }
                ],

	});
});
</script>
<script type="text/javascript">
$(function(){
	 $('#date').nepaliDatePicker();
})
</script>