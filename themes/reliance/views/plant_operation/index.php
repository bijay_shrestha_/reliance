<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-6 form-group">
                        <button class="btn btn-primary" id="add-btn">Add</button>
                    </div>
                    <form method="post"action="<?php echo site_url('plant_operation')?>">
                        <div class="col-md-4">
                            <input type="text" name="date_np" class="form-control" id="date_np_search" placeholder="Date">
                            <input type="hidden" name="date_en" class="form-control" id="date_en_search" placeholder="Date">
                            <script>
                                $('#date_np').change(function(){
                                      $('#date_en').val(BS2AD($('#date_np').val()));
                                 });
                            </script>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary" id="add-btn">View Detail</button>
                        </div>
                    </form>
                    <!-- <div class="col-md-6">
                        <input type="text" name="date_np" class="form-control" id="date_np_search" placeholder="Date">
                        <input type="hidden" name="date_en" class="form-control" id="date_en_search" placeholder="Date">
                        <script>
                            $('#date_np').change(function(){
                                  $('#date_en').val(BS2AD($('#date_np').val()));
                             });
                        </script>
                    </div> -->
                    <div class="col-xs-12">
                        <table id="brand_list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Plant</th>
                                    <th>Working Hour</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key => $value) {?>
                                <tr>
                                    <td><?php echo $value['date_np']?></td>
                                    <td><?php echo $value['name']?></td>
                                    <td><?php echo number_format($value['working_hour'],3)?></td>
                                    <td>
                                        <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger" onclick="delete_data(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="daily-production-sales-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="Date">
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                                <div id="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                            </div>
                        </div>
                        <?php foreach ($plants as $key => $value) {?>
                            <div class="form-group hour_input">
                                <label for="brand_type" class="col-sm-4 control-label"><?php echo $value['name']?></label>
                                <div class="col-sm-8">
                                    <input type="number" name="working_hour[<?php echo $value['id']?>]" class="form-control hr_input" id="work_hr_<?php echo $value['id']?>">
                                    <input type="hidden" name="plant_id[]" class="plant_id_data" value="<?php echo $value['id']?>">
                                    <div id="error_work_<?php echo $value['id']?>" hidden style="color: red" class="error-div">Must be between 0 to 24</div>
                                </div>
                            </div>
                        <?php }?>

                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });

        // nepali date picker
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
        $('#date_np_search').nepaliDatePicker({
            ndpEnglishInput: 'date_en_search'
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('.hr_input').val('');
        $('date_np').val('');
        $('date_en').val('');
        $('.error-div').hide();
        $('option').attr('selected', false);
        $('#myModalLabel').html('Add Plant Running Hours');
        $('#daily-production-sales-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var date_np = $('#date_np').val();
        var type_id = $('#type_id').val();
        var brand_id = $('#brand_id').val();
        var i = 0;

        if(date_np == ''){
            i = 1;
            $('#error_date_np').show();
        }else{
            $('#error_date_np').hide();
        }

        // $( ".hour_input" ).each(function() {
        //     var hr = $( this ).find('input.hr_input').val();
        //     var error_class = $(this).find('.plant_id_data').val()
        //     console.log(error_class);
        //     if(hr > 24){
        //         i = 1;
        //         $('#error_work_'+error_class).show();
        //     }else{
        //         $('#error_work_'+error_class).hide();
        //     }
        // });

        if(i == 0){
            $.post('<?php echo site_url("plant_operation/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.error-div').hide();
        var hour = 0;
        $.post('<?php echo site_url("plant_operation/get_value")?>',{id:id},function(data){
                $('#date_np').val(data.date.date_np);
                $('#date_en').val(data.date.date_en);
            $.each(data.data, function( index, value ) {
                hour = parseFloat(value.working_hour).toFixed(2);
                decimal_hour = hour;
                $('#work_hr_'+value.plant_id).val(hour);
            });
            $('#myModalLabel').html('Edit Plant Running Hours');
            $('#daily-production-sales-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_data(id){
        $.post('<?php echo site_url("plant_operation/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>