
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Department</label>
                        <div class="col-sm-8">
                            <select name="department_name" id="edit-dir-department" class='form-control' disabled onchange="editdirectDispatchMaterial()">
                                <option value=''>-- delect option --</option>
                                <?php foreach ($departments as $department) {?>
                                    <option value="<?php echo $department['department_id']?>" <?php echo ($department['department_id'] == $report['department_id'])?'selected':''?>>
                                    <?php echo $department['name']?>
                                    </option> 
                                <?php }?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Material</label>
                        <div class="col-sm-8">
                            <select name='material_name' id='edit-dir-material' class='form-control' disabled>
                                <option value="">-- select option --</option>
                                <?php foreach($materials as $material){?>
                                    <option value="<?php echo $material['material_id']?>" <?php echo ($material['material_id'] == $report['material_id'])?'selected':''?>>
                                    <?php echo $material['material_name']?>
                                    </option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="material_name" class="col-sm-4 control-label">Nepali Date</label>
                        <div class="col-sm-8">
                            <input type="text" id="edit-dispatch-date-np" name="date_np" class="form-control" value="<?php echo $report['store_confirm_np_date']?>">
                            <input type="hidden" name="date_en" id="edit-dispatch-date-en" class="form-control" value="<?php echo $report['store_confirm_en_date']?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Current Stock</label>
                        <div class="col-sm-8">
                            <input type="text" name="current_stock" id="dir-edit-current-stock" readonly="" class='form-control' value="<?php echo number_format(($report['accepted_quantity']+$current_stock['current_stock']),3)?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="purchase_request_id" class="col-sm-4 control-label">Accepted Quantity</label>
                        <div class="col-sm-8">
                            <input type="number" name="accepted_qty" id="dir-edit-accepted-quantity" class='form-control' value="<?php echo $report['accepted_quantity']?>" onkeyup='checkvalue()'>
                        </div>
                    </div>


                    <input type="hidden" name='request_id' id='dir-edit-id' value="<?php echo $report['request_id']?>">
<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                        
                    </table>
                    </div>

<script type="text/javascript">
    $('#edit-dispatch-date-np').nepaliDatePicker({
        ndpEnglishInput: 'edit-dispatch-date-en'
    })
    $("#edit-dispatch-date-np").change(function(){
        $('#edit-dispatch-date-en').val(BS2AD($('#edit-dispatch-date-np').val()));
    });
    
</script>
<script type="text/javascript">
    function editdirectDispatchMaterial(){
        id = $('#edit-dir-department').val();
        $.post('<?php echo site_url("direct_dispatch_material/getMaterial")?>',{id:id},function(data){
            $("#edit-dir-material").empty();
            $("#edit-dir-material").append
                    ($('<option>',{
                        value:'',
                        text: '-- Select opton -- '
                    }));
            $.each(data,function(index,value){
                console.log(value);
                $("#edit-dir-material").append
                    ($('<option>',{
                        value:value['material_id'],
                        text: value['material_name']
                    }));
            });

        },'json');
    }
</script>
<script type="text/javascript">
    $('#edit-dir-material').change(function(){
        id = $(this).val();
        $.post('<?php echo site_url("direct_dispatch_material/getStock")?>',{id:id},function(data){
            $('#dir-edit-current-stock').val(data['current_stock']);

        },'json');
    });
</script>
<script type="text/javascript">
    function checkvalue(){
        current_stock = parseFloat($('#dir-edit-current-stock').val().replace(',',''));
        added_qty = parseFloat($('#dir-edit-accepted-quantity').val().replace(',',''));
        if(current_stock < added_qty){
            alert('Dispatch quantity is greater than current stock');
            $('#dir-edit-accepted-quantity').val('');
        }

    }
</script>
