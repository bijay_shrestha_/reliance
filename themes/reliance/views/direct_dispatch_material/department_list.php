<div class="box-body">
    <table id="department_list" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>Departments</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($departments as $key => $value) {?>
            <tr>
                <td>
                    <?php echo $value['name'];?>
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
        $('#department_list').DataTable();
</script>