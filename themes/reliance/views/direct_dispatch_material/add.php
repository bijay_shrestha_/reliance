<tr>
    <td>
        <select id="dir-department-<?php echo $i?>" name="department_id[]" class='form-control' style="min-width:200px" onchange="directDispatchMaterial(<?php echo $i?>)">
            <option valure="">-- select option --</option>
            <?php foreach($departments as $department){?>
                <option value="<?php echo $department['id']?>"><?php echo $department['name']?></option>
            <?php }?>
        </select>
    </td>
    <td>
        <select id="dir-material-<?php echo $i?>" name='material_id[]' class='form-control'  style="min-width:200px" onchange="getMaterialStock(<?php echo $i?>)"></select>
    </td>
    <td>
        <input type='text' name="date_np[]" class="date form-control" id="dir-confirm-date-<?php echo $i?>" onchange=changeDate(<?php echo $i?>)>
        <input type='hidden' name="date_en[]" class="en-date" id="dir-confirm-en-date-<?php echo $i?>" >
        
    </td>
    <td>
        <input type='hidden' name="current_stock[]" id="dir-current-stock-hidden-<?php echo $i?>">
        <input type='text' id="dir-current-stock-<?php echo $i?>" disabled class='form-control'>
    </td>
    <td>
        <input type='number' name="added_qty[]" id="dir-accept-stock-<?php echo $i?>" onchange="dirCheckQty(<?php echo $i?>)" onkeyup="dirCheckQty(<?php echo $i?>)" onblur="dirCheckQty(<?php echo $i?>)" class='form-control' min=0>
    </td>
    <td><button type='button' class='btn btn-danger removebutton'><i class='fa fa-trash'></i></button></td>
    
</tr>
 <!-- Nepali Datepicker  -->
      <script src="<?php echo theme_url()?>assets/js/nepalidatepicker/nepalidatepicker.js"></script>
      <script>
                $('#dir-confirm-date-<?php echo $i?>').nepaliDatePicker({
                    ndpEnglishInput: 'dir-confirm-en-date-<?php echo $i?>'
                });
            $("#dir-confirm-date-<?php echo $i?>").change(function(){
                $('#dir-confirm-en-date-<?php echo $i?>').val(BS2AD($('#dir-confirm-date-<?php echo $i?>').val()));
            });
        </script>
<script type="text/javascript">
    $(document).on('click', 'button.removebutton', function () { // <-- changes
     $(this).closest('tr').remove();
     return false;
 });
</script>