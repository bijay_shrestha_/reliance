<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <!-- <li class="active"><a href="#tab_1" data-toggle="tab">Dealer</a></li> -->
                <li class="active"><a href="#tab_2" data-toggle="tab">Add Item To Department</a></li>
                <li><a href="#tab_3" data-toggle="tab">Direct Dispatch Material-Department</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_2">
                    <form id="material-department-form">
                        <div class="row">
                            <div class="col-md-10">
                                <select id="material-for-department" name="department_id" class="form-control">
                                    <option value="" id="select-option">-- select department --</option>
                                    <?php foreach ($departments as $department) { ?>
                                        <option value="<?php echo $department['id'] ?>"><?php echo $department['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-1 margin">
                                <!--<button type="button" id="material-dealer-btn" class="btn btn-primary">Show</button>-->
                                <button type="button" id="material-department-save" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        <div class="row" id="material-list">
                            <?php 
                            $i = 0;
                            foreach ($materials as $material) { ?>
                                <div class = "col-md-3 col-sm-6 col-xs-6" >
                                    <input type = "checkbox" class="check_material" name = "material[]" value="<?php echo $material['material_id']?>" id="material-<?php echo $material['material_id']?>">
                                    <?php echo $material['material_name'] ?>
                                </div>
                            <?php 
                            $i++;} ?>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    <div class='row'>
                        <div class='col-md-10'>
                            <select id='material_id_for_dealer' class='form-control'>
                            <option value=''>-- select --</option>
                            <?php foreach($materials as $material){?>
                                <option value='<?php echo $material["material_id"]?>'><?php echo $material['material_name']?></option>
                            <?php }?>
                            </select>
                        </div>
                        <div class='col-md-12'>
                            <div id='dealer_material_table'>
                        </div>
                    </div>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
    </div>
</section>

<div class="modal fade" id="member-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="dealer-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="dealer_name" class="col-sm-4 control-label">Name*</label>
                            <div class="col-sm-8">
                                <input type="text" name="dealer_name" class="form-control" id="dealer_name" placeholder="Dealer Name">
                                <span id="error-dealer-name" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dealer_address" class="col-sm-4 control-label">Address</label>
                            <div class="col-sm-8">
                                <input type="text" name="dealer_address" class="form-control" id="dealer_address" placeholder="Dealer Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dealer_landline" class="col-sm-4 control-label">Contact*</label>
                            <div class="col-sm-8">
                                <input type="text" name="contact" class="form-control" id="dealer_contact" placeholder="Contact">
                                <span id="error-contact" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dealer_mobile" class="col-sm-4 control-label">Mobile*</label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" class="form-control" id="dealer_mobile" placeholder="Mobile">
                                <span id="error-mobile" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="created_date_np" class="col-sm-4 control-label">Created Date(Nepali)</label>
                            <div class="col-sm-8">
                                <input type="text" name="created_date_np" class="form-control" id="created_date_np" placeholder="Created Date(Nepali)">
                            </div>
                        </div>
                        <?php /*<div class="form-group">
                            <label for="craeted_date_en" class="col-sm-4 control-label">Created Date(English)</label>
                            <div class="col-sm-8">
                                <input type="text" name="created_date_en" class="form-control" id="created_date_en" placeholder="Created Date(English)">
                            </div>
                        </div>*/?>
                        <input type="hidden" name="dealer_id" id="dealer_id" value="">
                        <input type="hidden" name="status" id="status" value="1">

<!--                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Status:</label> &nbsp;&nbsp;
                                    </div>
                                    <div class="col-md-8">
                                        <input type="radio" id="active" name="status" id="status1" value="1"> Active&nbsp;&nbsp;
                                        <input type="radio" id="inactive" name="status" id="status2" value="0"> Inactive
                                    </div>
                                </div>
                            </td>
                        </tr>-->

                        <?php /* <input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>"> */ ?>

                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#material-for-department").change(function () {
        var id = $(this).val();
        $('.check_material').removeAttr('checked');
        $.post('<?php echo site_url("direct_dispatch_material/getformmaterial") ?>', {id: id}, function (data) {
            $.each(data,function(index,value){
                $("#material-"+value.material_id).prop('checked',true).attr('checked', 'checked');
            });

        },'json');
    })
</script>
<script>
    $("#material-department-save").click(function(){
        $.post('<?php echo site_url()?>direct_dispatch_material/savemd',$("#material-department-form").serializeArray(),function(data){
            alert(data.msg);
            $('#select-option').prop('selected', true);
            $('.check_material').prop('checked',false);
        },'json')
    });
</script>
<script type="text/javascript">
    $('#material_id_for_dealer').change(function(){
        var id = $(this).val();
        $('#dealer_material_table').empty();
        $.post('<?php echo site_url("direct_dispatch_material/getDepartmentsToMaterial") ?>', {id: id}, function (data) {
            $('#dealer_material_table').html(data);
        },'html');
    });
</script>