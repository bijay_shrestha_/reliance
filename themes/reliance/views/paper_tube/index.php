<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<button class='btn btn-primary margin' id='add'>Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="diesel-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date</th>
				                    <th>Shift</th>
				                    <th>Paper Tube</th>
				                    <th>Production</th>
                                    <th>Action</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo $row['shift']?></td>
				                        <td><?php echo $row['material_name'] ?></td>
				                        <td><?php echo number_format($row['production'],3)?></td>
                                        <td><button class='btn btn-warning' onclick='edit(<?php echo $row['id']?>)'>Edit</button> <button class='btn btn-danger' onclick='delete_record(<?php echo $row['id']?>)'>Delete</button></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of diesel consumption-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="diesel-form" method="post" novalidate>
      	<div class="modal-body">
                        
            <table class="table table-striped" cellpadding="3px" >
                <tr>
                    <td><label>Date</label></td>
                    <td id="date_td"><input type="text" id="date-np" name="date_np" class='form-control'><span id="error-date" class="error-span" hidden>Date is required</span></td>
                    <td><input type="hidden" id="date-en" name="date_en" class='form-control'></td>
                </tr>
                <tr>
                    <td><label>Shift</label></td>
                    <td id="shift-td"><select name="shift" id='shift' class="form-control">
                        <option value="">--select shift--</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                    </select><span id="error-shift" class="error-span" hidden>Shift is required</span></td>
                </tr>
                <tr>
                    <td><label>Paper Tube</label></td>
                    <td id="papper-td"><select name="paper_tube_id" id='paper_tube_id' class="form-control">
                        <option value="">--select paper tube--</option>
                        <?php foreach ($paper_tubs as $paper_tub) {?>
                            <option value="<?php echo $paper_tub['material_id']?>"><?php echo $paper_tub['material_name']?></option>
                        <?php }?>
                    </select><span id="error-papper" class="error-span" hidden>Paper tube is required</span></td>
                </tr>
                <tr>
                    <td><label>Production</label></td>
                    <td id="production-td"><input type="text" id="production" name="production" class='form-control'><span id="error-production" class="error-span" hidden>Production is required</span></td>
                </tr>
                
                    <input type="hidden" id="id" name="id"/>
                </table>
        
        </div>
    
        <div class="modal-footer">
            <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(function(){
		$('#date-np').nepaliDatePicker({
			// ndpEnglishInput: 'date_en',
			// onChange: function(){
			// 	var date = $('#date-np').val();
			// 	var date_en = $('#date-ep').val();
			// 	$.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
			// 		$('#display-div').html(data);
			// 	},'html');
			// }
		});
		$('#diesel-table').dataTable({
            order: [[ 0, 'desc' ]],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
        $('#diesel-form').attrvalidate();
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
        $('#error-date, #error-shift, #error-papper, #error-production').hide();
        $('#date_td, #shift-td, #papper-td, #production-td').removeClass('has-error');
		$('#myModalLabel').text('Paper Tube Form');
                $('#date-np').val('');
                $('#date-en').val('');
                $('#shift').val('');
                $('#paper_tube_id').val('');
                $('#production').val('');
                // $('#min').val('');
                $('#id').val('');
                
		$('#form-dialog').modal();
	});
</script>
<script type="text/javascript">
	$('#save').click(function(){
        var date_np= $('#date-np').val();
        var shift = $('#shift').val();
        var paper_tube_id = $('#paper_tube_id').val();
        var production = $('#production').val();
        var i = 0;
        if(date_np == ''){
            $('#date_td').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date_td').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(shift == ''){
            $('#shift-td').addClass('has-error');
            $('#error-shift').show();
        }else{
            $('#shift-td').removeClass('has-error');
            $('#error-shift').hide();
            i++;
        }
        if(paper_tube_id == ''){
            $('#papper-td').addClass('has-error');
            $('#error-papper').show();
        }else{
            $('#papper-td').removeClass('has-error');
            $('#error-papper').hide();
            i++;
        }
        if(production == ''){
            $('#production-td').addClass('has-error');
            $('#error-production').show();
        }else{
            $('#production-td').removeClass('has-error');
            $('#error-production').hide();
            i++;
        }
        if(i == 4){
    		$.post("<?php echo site_url('paper_tube/save')?>",$("#diesel-form").serializeArray(),function($data){
                if($data.success){
                    alert('Data saved');
                    location.reload();
                }
            },'json');
            
        }
	});
</script>
<script type="text/javascript">
    function edit(id){
        $('#myModalLabel').text('Paper Tube Form');
        $('#error-date, #error-shift, #error-papper, #error-production').hide();
        $('#date_td, #shift-td, #papper-td, #production-td').removeClass('has-error');
        $.post('<?php echo site_url('paper_tube/getData')?>',{id:id},function(data){
            $('#date-np').val(data.date_np);
            $('#date-en').val(data.date_en);
            $('#paper_tube_id').val(data.paper_tube_id);
            $('#shift').val(data.shift);
            $('#production').val(data.production);
            // $('#min').val();
            $('#id').val(id);
                
        },'json');
        $('#form-dialog').modal();
    }
</script>
<script type="text/javascript">
    function delete_record(id){
        $.post('<?php echo site_url('paper_tube/delete_json')?>',{id:id},function(data){
            alert(data.msg);
            if(data.success){
                location.reload();
            }
        },'json');
    }
</script>

