<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<button class='btn btn-primary margin' id='add'>Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="diesel-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date(Nepali)</th>
				                    <th>Date(English)</th>
				                    <th>Duration</th>
                                    <th>Action</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo $row['date_en']?></td>
				                        <td><?php echo (($row['hour']<10)?0:'').$row['hour'].':'.(($row['min']<10)?0:'').$row['min'] ?></td>
                                        <td><button class='btn btn-warning' onclick='edit(<?php echo $row['ptw_id']?>)'>Edit</button> <button class='btn btn-danger' onclick='delete_record(<?php echo $row['ptw_id']?>)'>Delete</button></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of diesel consumption-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="diesel-form" method="post" novalidate>
      	<div class="modal-body">
                        
            <table class="table table-striped" cellpadding="3px" >
                <tr>
                    <td><label>Date</label></td>
                    <td id="date_td"><input type="text" id="date_np" name="date_np" class='form-control' placeholder="Date"><span id="error-date" class="error-span" hidden>Date is required</span></td>
                    <td><input type="hidden" id="date_en" name="date_en" class='form-control'></td>
                </tr>
                <tr>
                    <td><label>Woring Hour</label></td>
                    <td id="shift-td"><select name="hour" id='hour' class="form-control">
                        <option value="">--select hour--</option>
                        <?php for($i = 0; $i < 24; $i++){?><option value="<?php echo $i?>"><?php echo $i?></option><?php }?>
                    </select><span id="error-shift" class="error-span" hidden>Shift is required</span></td>
                </tr>
                <tr>
                    <td><label>Minute</label></td>
                    <td id="papper-td"><select name="min" id='min' class="form-control">
                        <option value="">--select minute--</option>
                        <?php for($i = 0; $i < 60; $i++){?><option value="<?php echo $i?>"><?php echo $i?></option><?php }?>
                    </select><span id="error-papper" class="error-span" hidden>min is required</span></td>
                </tr>
                
                    <input type="hidden" id="ptw_id" name="ptw_id"/>
                </table>
        
        </div>
    
        <div class="modal-footer">
            <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(function(){
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
        $('#date_en').datetimepicker({
            format: 'YYYY-MM-DD'
        }).on('dp.change', function (e) {
                $('#date_np').val(AD2BS($('#date_en').val()));
        });
		$('#diesel-table').dataTable({
            order: [[ 0, 'desc' ]],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
        $('#diesel-form').attrvalidate();
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
        $('#error-date, #error-shift, #error-papper, #error-production').hide();
        $('#date_td, #shift-td, #papper-td, #production-td').removeClass('has-error');
		$('#myModalLabel').text('Paper Tube Form');
                $('#date_np').val('');
                $('#date_en').val('');
                $('#hour').val('');
                $('#min').val('');
                // $('#min').val('');
                $('#ptw_sid').val('');
                
		$('#form-dialog').modal();
	});
</script>
<script type="text/javascript">
	$('#save').click(function(){
        var date_np= $('#date_np').val();
        var hour = $('#hour').val();
        var min = $('#min').val();
        var i = 0;
        if(date_np == ''){
            $('#date_td').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date_td').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(hour == ''){
            $('#shift-td').addClass('has-error');
            $('#error-shift').show();
        }else{
            $('#shift-td').removeClass('has-error');
            $('#error-shift').hide();
            i++;
        }
        if(i == 2){
    		$.post("<?php echo site_url('paper_tube_working/save')?>",$("#diesel-form").serializeArray(),function($data){
                if($data.success){
                    alert('Data saved');
                    location.reload();
                }
            },'json');
            
        }
	});
</script>
<script type="text/javascript">
    function edit(id){
        $('#myModalLabel').text('Paper Tube Form');
        $('#error-date, #error-shift, #error-papper, #error-production').hide();
        $('#date_td, #shift-td, #papper-td, #production-td').removeClass('has-error');
        $.post('<?php echo site_url('paper_tube_working/getData')?>',{id:id},function(data){
            $('#date_np').val(data.date_np);
            $('#date_en').val(data.date_en);
            $('#hour').val(data.hour);
            $('#min').val(data.min);
            $('#ptw_id').val(data.ptw_id);
            // $('#min').val();
            $('#id').val(id);
                
        },'json');
        $('#form-dialog').modal();
    }
</script>
<script type="text/javascript">
    function delete_record(id){
        $.post('<?php echo site_url('paper_tube_working/delete_json')?>',{id:id},function(data){
            alert(data.msg);
            if(data.success){
                location.reload();
            }
        },'json');
    }
</script>

