    <style type="text/css">
      .form-signin {
        max-width: 400px;
        padding: 19px 29px 29px;
        margin: 10.5% auto 12%;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
<?php print form_open('auth/login',array('class'=>'form-signin'))?>
        <div class="login-logo">
            <!-- <img src="<?php echo base_url('uploads/logo.png')?>"> -->
            Reliance MIS
        </div>
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" name="login_field" id="login_field" value="<?php print $this->validation->login_field?>" class="form-control" placeholder="<?php print $login_field?>" onblur='getDepsrtment()'>
        <input type="password" id="password" name="password" class="form-control" placeholder="<?php print $this->lang->line('userlib_password')?>">
        <select name="department" class="form-control" id='department'>
          <?php /*foreach($groups as $group){?>
              <option value="<?php echo $group['id']?>"><?php echo $group['name']?></option>
          <?php }*/?>
        </select>
        <label class="checkbox">
	         <input type="checkbox" id="remember" name="remember" value="yes"/> <?php print $this->lang->line('userlib_remember_me')?>?
        </label>
            <?php
            // Only display captcha if needed
            if($this->preference->item('use_login_captcha')):?>
            
                <label for="recaptcha_response_field"><?php print $this->lang->line('userlib_captcha')?>:</label>
                <?php print $captcha?>
            
            <?php endif;?>        
        <button class="btn btn-large btn-primary" type="submit" name="submit" value="submit"><?php print $this->lang->line('userlib_login')?></button> 
            		<a href="<?php print site_url('auth/forgotten_password') ?>" class="btn btn-large btn-danger">
            			<?php print $this->bep_assets->icon('arrow_refresh') ?>
            			<?php print $this->lang->line('userlib_forgotten_password')?>
            		</a>

            		<?php if($this->preference->item('allow_user_registration')):?>
            		<?php /*?><a href="<?php print site_url('auth/register') ?>"  class="btn btn-large btn-danger">
            			<?php print $this->bep_assets->icon('user') ?>
            			<?php print $this->lang->line('userlib_register')?>
            		</a><?php */?>
            		<?php endif;?>        
<?php print form_close()?>

<script type="text/javascript">
function getDepsrtment(){
    name = $('#login_field').val();
    $.post("<?php echo site_url('auth/getDepartment')?>",{name:name},function(data){
        $("#department").empty();
        $.each(data,function(index,value){
                
            $("#department").append
                ($('<option>',{
                    value:value['id'],
                    text: value['name']
                }));
            });
    },'json');
}
</script>
<script type="text/javascript">
  $(function(){
    $.post("<?php echo site_url('auth/getDepartment')?>",{name:name},function(data){
        $("#department").empty();
        $.each(data,function(index,value){
                
            $("#department").append
                ($('<option>',{
                    value:value['id'],
                    text: value['name']
                }));
            });
    },'json');
  });
</script>

