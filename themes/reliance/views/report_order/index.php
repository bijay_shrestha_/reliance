<div id='order-table'>
<table class="table table-striped">
  <tr>
    <th>Category</th>
    <th>Up</th>
    <th>Down</th>
  </tr>
  <?php foreach($categories as $category){?>
  
  <tr>
      <td><?php echo $category['category_name']?></td>
      <td>
        <button type='button' class='btn btn-primary' onclick='up(<?php echo $category['category_id']?>,<?php echo $category['report_id']?>)' >Up</button>
      </td>
      <td><button type='button' class='btn btn-primary' onclick='down(<?php echo $category['category_id']?>,<?php echo $category['report_id']?>)' >Down</button></td>
    </tr>
    <?php }?>
</table>
</div>
<script type="text/javascript">
    function up(id,report_id){
      $('#order-table').empty();
      $.post('<?php echo site_url("report_order/up")?>',{id:id,report_id:report_id},function(data){
        $('#order-table').html(data);
      },'html');
    }
</script>
<script type="text/javascript">
    function down(id,report_id){
      $('#order-table').empty();
      $.post('<?php echo site_url("report_order/down")?>',{id:id,report_id:report_id},function(data){
        $('#order-table').html(data);
      },'html');
    }
</script>
                