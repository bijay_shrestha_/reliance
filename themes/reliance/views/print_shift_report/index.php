<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Shift Report</a></li>
                <li><a href="#tab_2" data-toggle="tab">Loss Report</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="pull-right margin"><?php $this->load->view('home/pdf_button')?></div>
                    <button id="add-report" class="btn btn-primary margin">Add</button>
                    <!-- dealer table -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Report Table</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Shift</th>
                                            <th>Dimension</th>
                                            <th>Color</th>
                                            <th>Design</th>
                                            <th>Input</th>
                                            <th>Output</th>
                                            <th>Unprinted</th>
                                            <th>Printed</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($reports as $report) { ?>
                                            <tr>
                                                <td><?php echo $report['date_np'] ?></td>
                                                <td><?php echo $report['shift'] ?></td>
                                                <td><?php echo $report['dimension_name'] ?></td>
                                                <td><?php echo $report['color_name'] ?></td>
                                                <td><?php echo $report['design_name'] ?></td>
                                                <td><?php echo number_format($report['input'],3) ?></td>
                                                <td><?php echo number_format($report['output'],3) ?></td>
                                                <td><?php echo number_format($report['unprinted_scrap'],3) ?></td>
                                                <td><?php echo number_format($report['printed_scrap'],3) ?></td>
                                                <td><button type="button" class="btn btn-warning edit_report" id="material-<?php echo $report['psr_id']?>" value="<?php echo $report['psr_id']?>"> Edit</button>
                                                <button type="button" class="btn btn-danger delete_report" id="delete-<?php echo $report['psr_id']?>" value="<?php echo $report['psr_id']?>"> Delete</button></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    
                    <!-- dealer table end -->
                    
                        
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <!-- dealer table -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Loss Report Table</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Shift</th>
                                            <th>Dimension</th>
                                            <th>Color</th>
                                            <th>Design</th>
                                            <th>Type</th>
                                            <th>Reason</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($loss_reports as $report) { ?>
                                            <tr>
                                                <td><?php echo $report['date_np'] ?></td>
                                                <td><?php echo $report['shift'] ?></td>
                                                <td><?php echo $report['dimension_name'] ?></td>
                                                <td><?php echo $report['color_name'] ?></td>
                                                <td><?php echo $report['design_name'] ?></td>
                                                <td><?php echo ($report['type'] == 1)?'Printed':'Unprinted' ?></td>
                                                <td><?php echo $report['reason'] ?></td>
                                                <td><?php echo number_format($report['amount'],3) ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    
                    <!-- dealer table end -->
                    
                        
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
    </div>
</section>

<div class="modal fade" id="report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="report-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date_np" class="col-sm-4 control-label">Date*</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date-np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date-en">
                            </div>
                            <div id='error-date' style='color:red' hidden>Date is required</div>
                        </div>
                        <div class="form-group">
                            <label for="shift" class="col-sm-4 control-label">Shift*</label>
                            <div class="col-sm-8">
                                <select name="shift" id='shift' class="form-control">
                                    <option value="">-- select option --</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                </select>
                            </div>
                            <div id='error-date' style='color:red' hidden>Shift is required</div>
                        </div>
                        <div class="form-group">
                            <label for="Dimension" class="col-sm-4 control-label">Dimension*</label>
                            <div class="col-sm-8">
                                <select id='dimension' class="form-control" name="dimension_id" onchange="getColor()">
                                    <option value="">-- select option --</option>
                                    <?php foreach($dimensions as $dimension){?>}
                                        <option value="<?php echo $dimension['dimension_id']?>"><?php echo $dimension['dimension_name']?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div id='error-date' style='color:red' hidden>Date is required</div>
                        </div>
                        <div class="form-group">
                            <label for="color" class="col-sm-4 control-label">Color*</label>
                            <div class="col-sm-8">
                                <select id='color' class="form-control" name="color_id" onchange="getDesign()">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="design" class="col-sm-4 control-label">Design*</label>
                            <div class="col-sm-8">
                                <select id='design' class="form-control" name="design_id"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input" class="col-sm-4 control-label">Input Amount*</label>
                            <div class="col-sm-8">
                                <input type="number" min=0 id='input' class="form-control" name="input" placeholder="Input Amount" onkeyup="checkOutput()">
                            </div>
                        </div>
                        <div class="form-group" id='output-div'>
                            <label for="inputError" class="col-sm-4 control-label">Output Amount*</label>
                            <div class="col-sm-8">
                                <input type='number' min=0 id='output' class="form-control" name="output" placeholder="Output Amount" onkeyup="checkOutput()">
                            </div>
                        </div>
                        <div class="form-group" id='unprinted-scrap-div'>
                            <label for="unprinted_scrap" class="col-sm-4 control-label">Unprinted Scrap</label>
                            <div class="col-sm-8">
                                <input type='number' min=0 id='unprinted-scrap' class="form-control" name="unprinted_scrap" placeholder="Unprinted Scrap" onkeyup="checkOutput()">

                            </div>
                        </div>
                        <hr>
                        <div id='add-unprinted'></div>
                        <div class="form-group">
                            <label for="unprinted_scrap" class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <button type='button' class='btn btn-primary' id='add-unprinted-reason'>Add Reason</button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" id='printed-scrap-div'>
                            <label for="unprinted_scrap" class="col-sm-4 control-label">Printed Scrap</label>
                            <div class="col-sm-8">
                                <input type='number' min=0 id='printed-scrap' class="form-control" name="printed_scrap" placeholder="Printed Scrap" onkeyup="checkOutput()">
                            </div>
                        </div>
                        <hr>
                            <div id='add-printed'></div>
                        <div class="form-group">
                            <label for="unprinted_scrap" class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <button type='button' class='btn btn-primary' id='add-printed-reason'>Add Reason</button>
                            </div>
                        </div>
                            <hr>
                        <input type="hidden" name="material_id" id="material_id" value="">
                        <input type="hidden" name="status" id="status" value="1">


                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- edit modal -->
<div class="modal fade" id="edit-report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myEditModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="edit-report-form" action="#">
                <div class="modal-body" id='edit-modal-body'>
                </div>

                <div class="modal-footer">
                    <button type="button" id="edit-form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $('#example2,#example1').DataTable({
            "order":[[0, "desc"]],
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
        });
    });
</script>
<script>
    $("#form-submit").click(function () {
        var ready = 1;
        if(!$('#date-np').val() || !$('#shift').val() || !$('#design').val() || !$('#input').val() || !$('#output').val()){
            ready = 0;
        }
        var total = 0;
        var i=0;
        if(ready == 1){
            $( ".unprinted-amount" ).each(function( index ) {
                total += parseFloat($(this).val());
            });
        }
        if(parseFloat($('#unprinted-scrap').val())>0 && ready == 1){
            if(total <= 0){
                alert('Reason and amount is needed');
                ready = 0;
                return false;
            }else if(parseFloat($('#unprinted-scrap').val()) != total){
                alert('Reason amount doesnot match with unprinted scrap amount');
                ready = 0;
                return false;
            }
        }
        total = 0;
        i=0;
        if(ready == 1){
            $( ".printed_amount" ).each(function( index ) {
                total += parseFloat($(this).val());
            });
        }
        if(parseFloat($('#printed-scrap').val())>0 && ready == 1){
            if(total <= 0){
                alert('Reason and amount is needed');
                ready = 0;
                return false;
            }else if(parseFloat($('#printed-scrap').val()) != total){
                alert('Reason amount doesnot match with printed scrap amount');
                ready = 0;
                return false;
            }
        }
        if(ready){
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('print_shift_report/save') ?>",
                data: $("#report-form").serializeArray(),
                dataType: 'json',
                success: function (data) {
                    if (!data.success) {
                        alert('Error occured');
                    } else {
                        alert('Report saved')
                        location.reload();
                    }
                },
            });
        }else{
            alert('All * fields are required');
            return false;
        }
    });
</script>
<script>
    $('#add-report').click(function () {
        $('#output-div').removeClass('has-error');
        $('#unprinted-scrap-div').removeClass('has-error');
        $('#printed-scrap-div').removeClass('has-error');
        $('#form-submit').prop('disabled',false);
        $('#date-np').val('');
        $('#date-en').val('');
        $('#shift').val('');
        $('#dimension').val('');
        $('#color option').remove();
        $('#design option').remove();
        $('#input').val('');
        $('#output').val('');
        $('#unprinted-scrap').val('');
        $('#printed-scrap').val('');
        $('#add-printed').empty();
        $('#add-unprinted').empty();
        $('#myModalLabel').text('Add Material');
        $(".error_message").hide();
        $('#material_name').val('');
        $('#report-dialog').modal();
    });
</script>
<script>
    $(".edit_report").click(function(){
        id = $(this).val();
        $.post('<?php echo site_url()?>print_shift_report/getReport',{id:id},function(data){
            $('#myEditModalLabel').text('Edit Material');
            $('#edit-modal-body').html(data);
            $('#edit-report-dialog').modal();
            },'html');
    });
</script>
<script type="text/javascript">
    var i=0;
    var j=0;

    $('#add-unprinted-reason').click(function(){
        $.post('<?php echo site_url('print_shift_report/addUnPrinted')?>',{i:i},function(data){
            $('#add-unprinted').append(data);
            i++;
        },'html');
    });
</script>
<script type="text/javascript">
    $('#add-printed-reason').click(function(){
        $.post('<?php echo site_url('print_shift_report/addPrinted')?>',{j:j},function(data){
            $('#add-printed').append(data);
        },'html');
        j++;
    });
</script>
<script type="text/javascript">
function getColor(){
    dimension_id = $('#dimension').val();
    $.post('<?php echo site_url("print_shift_report/getColor")?>',{dimension_id:dimension_id},function(data){
            $('#color').empty();
            $("#color").append
                ($('<option>',{
                    value:"",
                    text: '--select quality--'
                }));
            $.each(data,function(index,value){
                
            $("#color").append
                ($('<option>',{
                    value:value['color_id'],
                    text: value['color_name']
                }));
            });
    },'json');
    
}
</script>
<script type="text/javascript">
function getDesign(){
    dimension_id = $('#dimension').val();
    color_id = $('#color').val();
    $.post('<?php echo site_url('print_shift_report/getDesign')?>',{dimension_id:dimension_id,color_id:color_id},function(data){
            $('#design').empty();
            $("#design").append
                ($('<option>',{
                    value:"",
                    text: '--select quality--'
                }));
            $.each(data,function(index,value){
                
            $("#design").append
                ($('<option>',{
                    value:value['design_id'],
                    text: value['design_name']
                }));
            });
    },'json');
    
}
</script>
<script type="text/javascript">
$(function(){
    $('#date-np').nepaliDatePicker({
            ndpEnglishInput: 'date-en'
        });
});
</script>
<!-- edit form submit -->
<script>
    $("#edit-form-submit").click(function () {

        var ready = 1;
        if($('#edit-date-np').val() == '' || $('#edit-shift').val() == '' || $('#edit-design').val() == '' || $('#edit-input').val() == '' || $('#edit-output').val() == ''){
            ready = 0;
        }
//----------------------------------
        var total = 0;
        var i=0;
        if(ready == 1){
            $( ".unprinted-amount" ).each(function( index ) {
                total += parseFloat($(this).val());
            });
        }
        if(parseFloat($('#edit-unprinted-scrap').val())>0 && ready == 1){
            if(total <= 0){
                alert('Reason and amount is needed');
                ready = 0;
                return false;
            }else if(parseFloat($('#edit-unprinted-scrap').val()) != total){
                alert('Reason amount doesnot match with unprinted scrap amount');
                ready = 0;
                return false;
            }
        }
        total = 0;
        i=0;
        if(ready == 1){
            $( ".printed_amount" ).each(function( index ) {
                total += parseFloat($(this).val());
            });
        }
        if(parseFloat($('#edit-printed-scrap').val())>0 && ready == 1){
            if(total <= 0){
                alert('Reason and amount is needed');
                ready = 0;
                return false;
            }else if(parseFloat($('#edit-printed-scrap').val()) != total){
                alert('Reason amount doesnot match with printed scrap amount');
                ready = 0;
                return false;
            }
        }
//------------------------------------------------------
        if(ready){ 
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('print_shift_report/save') ?>",
            data: $("#edit-report-form").serializeArray(),
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
                    alert('Error ocured');
                } else {
                    alert('Report saved')
                    location.reload();
                }
            },
        });
        }else{
            alert('All * fields are required');
        }
    });
</script>
<script type="text/javascript">
    function checkOutput(){
        output = parseFloat($('#output').val()) || 0;
        input = parseFloat($('#input').val()) || 0;
        unprinted_scrap = parseFloat($('#unprinted-scrap').val()) || 0;
        printed_scrap = parseFloat($('#printed-scrap').val()) || 0;
        if((output + unprinted_scrap + printed_scrap) != input){
            // alert('Output is greater than input');
            if(output != 0){
                $('#output-div').addClass('has-error');
            }
            if(unprinted_scrap != 0){
                $('#unprinted-scrap-div').addClass('has-error');
            }
            if(printed_scrap != 0){
                $('#printed-scrap-div').addClass('has-error');
            }
            $('#form-submit').prop('disabled',true);
        }else{
                    $('#output-div').removeClass('has-error');
                    $('#unprinted-scrap-div').removeClass('has-error');
                    $('#printed-scrap-div').removeClass('has-error');
                    $('#form-submit').prop('disabled',false);
        }
    }
</script>
<script type="text/javascript">
    $('.delete_report').click(function(){
        id = $(this).val();
        if(confirm('Do you want to delete this report?')){
            $.post('<?php echo site_url('print_shift_report/delete_json')?>',{id:id},function(data){
                if(data){
                    alert('Record deleted');
                    location.reload();
                }else{
                    alert('Error occured');
                }
            },'json');
        }
    });
</script>