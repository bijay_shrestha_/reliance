<div class='form-group printed-reason-div' id='add-printed-div-<?php echo $i?>'>
		<div class='col-md-2'>Reason</div>
		<div class='col-md-4'>
                    <select name="printed_reason[<?php echo $i?>]" id="printed-reason-<?php echo $i?>" class='form-control'>
				<option value="">-- select option --</option>
				<?php foreach($reasons as $reason){?>
					<option value='<?php echo $reason["reason_id"]?>'><?php echo $reason["reason"]?></option>
				<?php }?>
			</select>
		</div>
		<div class='col-md-2'>Quantity</div>
		<div class='col-md-3'>
			<input type="number" name="printed_quantity[<?php echo $i?>]" class='form-control printed_amount' onkeyup='checkPrinted(<?php echo $i?>)' id='printed_quantity-<?php echo $i?>'>
		</div>
		<div class='col-md-1'>
			<button type='button' class='btn btn-danger' id='delete-printed-row-<?php echo $i?>'><i class='fa fa-trash'></i></button>
		</div>
</div>

<script type="text/javascript">
    $('#delete-printed-row-<?php echo $i?>').click(function(){
        var i = <?php echo $i?>;
        $('#add-printed-div-'+i).remove();
    });
</script>
<script type="text/javascript">
	function checkPrinted(i){
		reason = $('#printed-reason-'+i).val();
		value_printed = parseFloat($('#printed_quantity-'+i).val()) || 0;
		if(reason == '' && value_printed != 0){
			alert('Reason is needed');
			$('.printed-reason-div').addClass('has-error');
			$('#form-submit').prop('disabled',true);
			return false;
		}{
			$('.printed-reason-div').removeClass('has-error');
			$('#form-submit').prop('disabled',false);
		}
	    var value = 0;
		// value = $('#unprinted_quantity-'+i).val();
		printed_scrap = $('#printed-scrap').val() || 0;
		$( ".printed_amount" ).each(function( index ) {
			if($.isNumeric( parseFloat($(this).val())) ){
				value += parseFloat($(this).val());
			}
		});
		if(value > printed_scrap){
			$('.printed-reason-div').addClass('has-error');
			$('#form-submit').prop('disabled',true);
		}else{
			$('.printed-reason-div').removeClass('has-error');
			$('#form-submit').prop('disabled',false);
		}
	}
</script>