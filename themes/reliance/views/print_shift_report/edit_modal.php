<table class="table table-striped table-responsive" cellpadding="3px" >
                        <div class="form-group">
                            <label for="date_np" class="col-sm-4 control-label">Date*</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="edit-date-np" placeholder="Date" value="<?php echo $date_np?>">
                                <input type="hidden" name="date_en" class="form-control" id="edit-date-en" value="<?php echo $date_en?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shift" class="col-sm-4 control-label">Shift*</label>
                            <div class="col-sm-8">
                                <select name="shift" id='shift'class="form-control">
                                    <option value="">-- select option --</option>
                                    <option value="A" <?php echo ($shift == 'A')?'selected':''?>>A</option>
                                    <option value="B" <?php echo ($shift == 'B')?'selected':''?>>B</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Dimension" class="col-sm-4 control-label">Dimension*</label>
                            <div class="col-sm-8">
                                <select id='dimension' class="form-control for-dimension" name="dimension_id" onchange="getEditColor()">
                                    <option value="">-- select option --</option>
                                    <?php foreach($dimensions as $dimension){?>}
                                        <option value="<?php echo $dimension['dimension_id']?>" <?php echo ($dimension_id == $dimension['dimension_id'])?'selected':''?>><?php echo $dimension['dimension_name']?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="color" class="col-sm-4 control-label">Color*</label>
                            <div class="col-sm-8">
                                <select id='color' class="form-control for-color" name="color_id" onchange="getEditDesign()">
                                    <option value="">-- select option --</option>
                                    <?php foreach($color as $color_list){?>}
                                        <option value="<?php echo $color_list['color_id']?>" <?php echo ($color_id == $color_list['color_id'])?'selected':''?>><?php echo $color_list['color_name']?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="design" class="col-sm-4 control-label">Design*</label>
                            <div class="col-sm-8">
                                <select id='design' class="form-control for-design" name="design_id">
                                    <option value="">-- select option --</option>
                                    <?php foreach($design as $design_list){?>}
                                        <option value="<?php echo $design_list['design_id']?>" <?php echo ($design_id == $design_list['design_id'])?'selected':''?>><?php echo $design_list['design_name']?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input" class="col-sm-4 control-label">Input Amount*</label>
                            <div class="col-sm-8">
                                <input type="number" min=0 id='edit-input' class="form-control" name="input" placeholder="Input Amount" value="<?php echo $input?>" onkeyup='checkOutput()'>
                            </div>
                        </div>
                        <div class="form-group" id="edit-output-div">
                            <label for="output" class="col-sm-4 control-label">Output Amount*</label>
                            <div class="col-sm-8">
                                <input type='number' min=0 id='edit-output' class="form-control" name="output" placeholder="Output Amount" value="<?php echo $output?>" onkeyup='checkOutput()'>
                            </div>
                        </div>
                        <div class="form-group" id="edit-unprinted-scrap-div">
                            <label for="unprinted_scrap" class="col-sm-4 control-label">Unprinted Scrap</label>
                            <div class="col-sm-8">
                                <input type='number' min=0 id='edit-unprinted-scrap' class="form-control" name="unprinted_scrap" placeholder="Unprinted Scrap" value="<?php echo $unprinted_scrap?>" onkeyup='checkOutput()'>

                            </div>
                        </div>
                        <hr>
                        <div id='edit-add-unprinted'>
                            <?php
                                $i = 0;
                                foreach($unprinted_scrap_list as $list){?>
                                    <div class='form-group unprinted-reason-div' id='unprinted-div-<?php echo $i?>'>
                                        <div class='col-md-2'>Reason</div>
                                        <div class='col-md-4'>
                                            <select name="unprinted_reason[<?php echo $i?>]" class='form-control'>
                                                <option value="">-- select option --</option>
                                                <?php foreach($printed_reaon_list['reasons'] as $reason){?>
                                                    <option <?php echo ($reason["reason_id"] == $list['reason_id'])?'selected':''?> value='<?php echo $reason["reason_id"]?>'><?php echo $reason["reason"]?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class='col-md-2'>Quantity</div>
                                        <div class='col-md-3'>
                                            <input type="number" name="unprinted_quantity[<?php echo $i?>]" class='form-control unprinted-amount' value='<?php echo $list['amount']?>'' onkeyup='checkEditUnprinted()'>
                                        </div>
                                        <div class='col-md-1'>
                                            <button class='btn btn-danger delete-unprinted-row' value='<?php echo $i?>'><i class='fa fa-trash'></i></button>
                                        </div>
                                </div>
                                <?php $i++;} 
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="unprinted_scrap" class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <button type='button' class='btn btn-primary' id='edit-add-unprinted-reason'>Add Reason</button>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" id="edit-printed-scrap-div">
                            <label for="unprinted_scrap" class="col-sm-4 control-label">Printed Scrap</label>
                            <div class="col-sm-8">
                                <input type='number' min=0 id='edit-printed-scrap' class="form-control" name="printed_scrap" placeholder="Printed Scrap" value="<?php echo $printed_scrap?>" onkeyup='checkOutput()'>
                            </div>
                        </div>
                        <hr>
                            <div id='edit-add-printed'>
                            <?php
                                $j = 0;
                                foreach($printed_scrap_list as $list){?>
                                <div class='form-group printed-reason-div' id='printed-div-<?php echo $j?>'>
                                    <div class='col-md-2'>Reason</div>
                                    <div class='col-md-4'>
                                        <select name="printed_reason[<?php echo $j?>]" class='form-control'>
                                            <option value="">-- select option --</option>
                                            <?php foreach($printed_reaon_list['reasons'] as $reason){?>
                                                <option <?php echo ($reason["reason_id"] == $list['reason_id'])?'selected':''?> value='<?php echo $reason["reason_id"]?>'><?php echo $reason["reason"]?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class='col-md-2'>Quantity</div>
                                    <div class='col-md-3'>
                                        <input type="number" name="printed_quantity[<?php echo $j?>]" class='form-control printed_amount' value='<?php echo $list['amount']?>'' onkeyup='checkEditPrinted()'>
                                    </div>
                                    <div class='col-md-1'>
                                        <button class='btn btn-danger delete-printed-row' value='<?php echo $j?>'><i class='fa fa-trash'></i></button>
                                    </div>
                                </div>
                                <?php 
                                $j++;
                                } 
                            ?>
                            </div>
                        <div class="form-group">
                            <label for="unprinted_scrap" class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <button type='button' class='btn btn-primary' id='edit-add-printed-reason'>Add Reason</button>
                            </div>
                        </div>
                            <hr>
                        <input type="hidden" name="psr_id" id="psr-id" value="<?php echo $psr_id?>">


                        <input type="hidden" id="id" name="id"/>
                    </table>

<script type="text/javascript">
    var i=<?php echo $i?>;
    var j=<?php echo $j?>;

    $('#edit-add-unprinted-reason').click(function(){
        $.post('<?php echo site_url('print_shift_report/editUnPrinted')?>',{i:i},function(data){
            $('#edit-add-unprinted').append(data);
            i++;
        },'html');
    });
</script>
<script type="text/javascript">
    $('#edit-add-printed-reason').click(function(){
        $.post('<?php echo site_url('print_shift_report/editPrinted')?>',{j:j},function(data){
            $('#edit-add-printed').append(data);
        },'html');
        j++;
    });
</script>
<script type="text/javascript">
    $('.delete-unprinted-row').click(function(){
        var i = $(this).val();
        $('#unprinted-div-'+i).remove();
    });
</script>
<script type="text/javascript">
    $('.delete-printed-row').click(function(){
        var i = $(this).val();
        $('#printed-div-'+i).remove();
    });
</script>
<script type="text/javascript">
    function checkEditPrinted(){
        var value = 0;
        // value = $('#unprinted_quantity-'+i).val();
        printed_scrap = $('#edit-printed-scrap').val();
        $( ".printed_amount" ).each(function( index ) {
            if($.isNumeric( parseFloat($(this).val())) ){
                value += parseFloat($(this).val());
            }
        });
        if(value > printed_scrap){
            $('.printed-reason-div').addClass('has-error');
            $('#edit-form-submit').prop('disabled',true);
        }else{
            $('.printed-reason-div').removeClass('has-error');
            $('#edit-form-submit').prop('disabled',false);
        }
    }
</script>
<script type="text/javascript">
    function checkEditUnprinted(){
        var value = 0;
        // value = $('#unprinted_quantity-'+i).val();
        unprinted_scrap = $('#edit-unprinted-scrap').val();
        $( ".unprinted-amount" ).each(function( index ) {
            if($.isNumeric( parseFloat($(this).val()) )){
                value += parseFloat($(this).val());
            }
        });
        if(value > unprinted_scrap){
            $('.unprinted-reason-div').addClass('has-error');
            $('#edit-form-submit').prop('disabled',true);
        }else{
            $('.unprinted-reason-div').removeClass('has-error');
            $('#edit-form-submit').prop('disabled',false);
        }
    }
</script>
<!-- Nepali Datepicker  -->
      <!--<script src="<?php echo theme_url()?>assets/js/nepalidatepicker/nepalidatepicker.js"></script>-->
<script type="text/javascript">
    $(function(){
        $('#edit-date-np').nepaliDatePicker({
            ndpEnglishInput: 'edit-date-en'
        });
    });
</script>
<script type="text/javascript">
    function checkOutput(){
        output = parseFloat($('#edit-output').val()) || 0;
        input = parseFloat($('#edit-input').val()) || 0;
        unprinted_scrap = parseFloat($('#edit-unprinted-scrap').val()) || 0;
        printed_scrap = parseFloat($('#edit-printed-scrap').val()) || 0;
        if((output + unprinted_scrap + printed_scrap) > input){
            // alert('Output is greater than input');
            if(output != 0){
                $('#edit-output-div').addClass('has-error');
            }
            if(unprinted_scrap != 0){
                $('#edit-unprinted-scrap-div').addClass('has-error');
            }
            if(printed_scrap != 0){
                $('#edit-printed-scrap-div').addClass('has-error');
            }
            $('#edit-form-submit').prop('disabled',true);
        }else{
            $('#edit-output-div').removeClass('has-error');
            $('#edit-unprinted-scrap-div').removeClass('has-error');
            $('#edit-printed-scrap-div').removeClass('has-error');
            $('#edit-form-submit').prop('disabled',false);
        }
    }
</script>
<script type="text/javascript">
function getEditColor(){
    dimension_id = $('.for-dimension').val();
    $.post('<?php echo site_url("print_shift_report/getColor")?>',{dimension_id:dimension_id},function(data){
            $('.for-color').empty();
            $(".for-color").append
                ($('<option>',{
                    value:"",
                    text: '--select quality--'
                }));
            $.each(data,function(index,value){
                
            $(".for-color").append
                ($('<option>',{
                    value:value['color_id'],
                    text: value['color_name']
                }));
            });
    },'json');
    
}
</script>
<script type="text/javascript">
function getEditDesign(){
    dimension_id = $('.for-dimension').val();
    color_id = $('.for-color').val();
    $.post('<?php echo site_url('print_shift_report/getDesign')?>',{dimension_id:dimension_id,color_id:color_id},function(data){
            $('.for-design').empty();
            $(".for-design").append
                ($('<option>',{
                    value:"",
                    text: '--select quality--'
                }));
            $.each(data,function(index,value){
                
            $(".for-design").append
                ($('<option>',{
                    value:value['design_id'],
                    text: value['design_name']
                }));
            });
    },'json');
    
}
</script>