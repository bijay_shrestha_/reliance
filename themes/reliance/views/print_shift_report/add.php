<div class='form-group unprinted-reason-div' id='add-unprinted-div-<?php echo $i?>'>
		<div class='col-md-2'>Reason</div>
		<div class='col-md-4'>
			<select name="unprinted_reason[<?php echo $i?>]" class='form-control' id='unprinted-reason-<?php echo $i?>'>
				<option value="">-- select option --</option>
				<?php foreach($reasons as $reason){?>
					<option value='<?php echo $reason["reason_id"]?>'><?php echo $reason["reason"]?></option>
				<?php }?>
			</select>
		</div>
		<div class='col-md-2'>Quantity</div>
		<div class='col-md-3'>
			<input type="number" name="unprinted_quantity[<?php echo $i?>]" class='form-control unprinted-amount' id='unprinted_quantity-<?php echo $i?>' onkeyup='checkUnprinted(<?php echo $i?>)'>
		</div>
		<div class='col-md-1'>
			<button type='button' class='btn btn-danger' id='delete-unprinted-row-<?php echo $i?>'><i class='fa fa-trash'></i></button>
		</div>
</div>

<script type="text/javascript">
    $('#delete-unprinted-row-<?php echo $i?>').click(function(){
        var i = <?php echo $i?>;
        $('#add-unprinted-div-'+i).remove();
    });
</script>
<script type="text/javascript">
	function checkUnprinted(i){
		reason = $('#unprinted-reason-'+i).val();
		value_unprinted = parseFloat($('#unprinted_quantity-'+i).val()) || 0;
		if(reason == '' && value_unprinted != 0){
			alert('Reason is needed');
			$('.unprinted-reason-div').addClass('has-error');
			$('#form-submit').prop('disabled',true);
			return false;
		}{
			$('.unprinted-reason-div').removeClass('has-error');
			$('#form-submit').prop('disabled',false);
		}
	    var value = 0;
		// value = $('#unprinted_quantity-'+i).val();
		unprinted_scrap = $('#unprinted-scrap').val();
		$( ".unprinted-amount" ).each(function( index ) {
			if($.isNumeric( parseFloat($(this).val()) )){
				value += parseFloat($(this).val());
			}
		});
		if(value > unprinted_scrap){
			$('.unprinted-reason-div').addClass('has-error');
			$('#form-submit').prop('disabled',true);
		}else{
			$('.unprinted-reason-div').removeClass('has-error');
			$('#form-submit').prop('disabled',false);
		}
	}
</script>