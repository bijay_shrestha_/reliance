<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo theme_url()?>assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="<?php echo theme_url()?>assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo theme_url()?>assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo theme_url()?>assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo theme_url()?>assets/ico/favicon.png">
  <title><?php print $header.' | '.$this->preference->item('site_name')?></title>
        
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/AdminLTE.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/buttons.dataTables.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/skins/_all-skins.min.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo theme_url()?>plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo theme_url()?>plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo theme_url()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Nepali Datepicker -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/nepalidatepicker/nepalidatepicker.css" />
    <!-- custom css -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/css/style.css" />

    <!-- multiselect -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dev.css.map">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/multi-select.dist.css">

    <!-- <link rel="stylesheet" type="text/css" href="<?php echo theme_url()?>nepali.datepicker.v2.1.min.css" /> -->
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo theme_url()?>plugins/jQuery/jQuery-2.1.4.min.js"></script>

  </head>

  <body class="hold-transition skin-yellow sidebar-mini" style="padding:0px">
      <div class="wrapper">

      <?php 
      if(is_user()){
        ?>

          <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo site_url()?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>R</b>MIS</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Reliance</b>MIS</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
                  <span class="hidden-xs"><?php echo $this->session->userdata('username');?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
                    <p>
                      <?php echo $this->session->userdata('username');?>
                      <small><?php echo $this->session->userdata('department')?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo site_url('auth/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li <?php if($header == 'Dashboard'){?>class="active"<?php }?>>
              <a href="<?php echo site_url()?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i> <span>Master Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <!-- <li><a href="<?php echo base_url('brand_type'); ?>"><i class="fa fa-circle-o"></i> Brand Type </a></li> -->
                <?php if($this->session->userdata('department_id') == '7'){?>
                <li>
                    <a href="<?php echo site_url('dealer')?>"><i class="fa fa-user"></i><span>Raw Material Dealer</span></a>
                </li>
                <li>
                    <a href="<?php echo site_url('material')?>"><i class="fa fa-archive"></i><span>Material</span></a>
                </li>
                <?php }?>
                <li><a href="<?php echo base_url('brand'); ?>"><i class="fa fa-circle-o"></i> Brand </a></li>
                <?php if($this->session->userdata('department_id') == '32'){?>
                <li><a href="<?php echo base_url('party'); ?>"><i class="fa fa-circle-o"></i> Agent </a></li>
                <li><a href="<?php echo base_url('party_name'); ?>"><i class="fa fa-circle-o"></i> Party </a></li>
                <li><a href="<?php echo base_url('bank'); ?>"><i class="fa fa-circle-o"></i> Bank </a></li>
                <?php }?>
              </ul>
            </li>
            <?php if($this->session->userdata('department_id') == '7'){?>
            <li>
                <!--<a href="<?php echo site_url('report')?>">Report</a>-->
            </li>
            <li>
                <a href="<?php echo site_url('inventory')?>"><i class="fa fa-gift"></i><span>Inventory</span></a>
            </li>
            <!--<li>
                <a href="<?php echo site_url('inventory/report')?>">Inventory Report</a>
            </li>-->
            <!-- report-->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Inventory Report</span>
                
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url('inventory/report')?>"><i class="fa fa-circle-o"></i> Report</a></li>
                <li><a href="<?php echo site_url('report_type')?>"><i class="fa fa-circle-o"></i> Report Type</a></li>
              </ul>
            </li>
            <!-- dispatch-->
            <li class="treeview">
              
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url('inventory/dispatch')?>"><i class="fa fa-circle-o"></i> Dispatch</a></li>
                <li><a href="<?php echo site_url('direct_dispatch_material')?>"><i class="fa fa-circle-o"></i> Assign Dispatch</a></li>
              </ul>
            </li>
            <!-- dispatch-->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-sign-out"></i>
                <span>Dispatch <!--<span class="label label-danger"><?php echo $this->get_dispatch_for_header?></span>--></span>
                
                <i class='fa fa-angle-left pull-right'></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url('inventory/dispatch')?>"><i class="fa fa-circle-o"></i> Dispatch</a></li>
                <li><a href="<?php echo site_url('direct_dispatch_material')?>"><i class="fa fa-circle-o"></i> Assign Dispatch</a></li>
              </ul>
            </li>
            <?php } ?>

          <?php // if($this->check_permission('Department Request')){?>
          <!-- <li><a href="<?php echo site_url('department')?>"><i class="fa fa-circle-o"></i>Demand Request</a></li> -->
          <?php // }?>
          <?php if($this->session->userdata('department_id') == 7){?>
                <li><a href="<?php echo site_url('silo_report')?>"><i class="fa fa-industry"></i>Silo</a></li>
                <li><a href="<?php echo base_url('daily_production_sale'); ?>"><i class="fa fa-industry"></i> <span>Daily Production Sales</span> </a></li>
                <li><a href="<?php echo site_url('electric_report');?>"><i class="fa fa-plug" aria-hidden="true"></i><span>Electric Report</span></a></li>
                <li><a href="<?php echo site_url('electric_meter_consumption');?>"><i class="fa fa-plug" aria-hidden="true"></i><span>Electric Meter Consumption</span></a></li>
                <li><a href="<?php echo site_url('plant_operation');?>"><i class="fa fa-bank" aria-hidden="true"></i><span>Plant_operation</span></a></li>
                <li><a href="<?php echo site_url('hourly_report');?>"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Hourly Report</span></a></li>
          <?php }?>
          <?php if($this->session->userdata('department_id') == 32){?>
                <li><a href="<?php echo base_url('sales_register'); ?>"><i class="fa fa-sign-out"></i> <span>Sales</span> </a></li>
                <li><a href="<?php echo base_url('payment'); ?>"><i class="fa fa-money"></i> <span>Payment</span> </a></li>
                <li><a href="<?php echo base_url('sales_register/report'); ?>"><i class="fa fa-area-chart"></i> <span>Report</span> </a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text-o"></i> <span>Ledger</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('sales_register/ledger'); ?>"><i class="fa fa-circle-o"></i> Agent Ledger </a></li>
                <li><a href="<?php echo base_url('sales_register/ledger/party'); ?>"><i class="fa fa-circle-o"></i> Party Ledger </a></li>
                <li><a href="<?php echo base_url('sales_register/tally_ledger'); ?>"><i class="fa fa-circle-o"></i> Tally Ledger </a></li>
                <li><a href="<?php echo base_url('sales_register/party_tally_ledger'); ?>"><i class="fa fa-circle-o"></i> Party Tally Ledger </a></li>
              </ul>
            </li>
          <?php }?>

            
            <?php if($this->session->userdata('department_id') == 33){?>
            <li><a href="<?php echo site_url('lab_report')?>"><i class="fa fa-flask"></i>Lab Report</a></li>
            <?php }?>
          </ul>



        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $header?>
          </h1>
            
      <?php }?>