<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<button class='btn btn-primary margin' id='add'>Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="diesel-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date</th>
				                    <th>Shift</th>
				                    <th>Production</th>
				                    <th>Working Hour</th>
                                    <th>Working Minutes</th>
                                    <th>Segregation Hour</th>
                                    <th>Segregation Minutes</th>
				                    <th>Action</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo $row['shift']?></td>
				                        <td><?php echo number_format($row['production'],3) ?></td>
                                        <td><?php echo $row['hour']?></td>
				                        <td><?php echo $row['min']?></td>
                                        <td><?php echo $row['segregation_hr']?></td>
				                        <td><?php echo $row['segregation_min']?></td>
                                        <td><button class='btn btn-warning' onclick='edit(<?php echo $row['id']?>)'>Edit</button> <button class='btn btn-danger' onclick='delete_record(<?php echo $row['id']?>)'>Delete</button></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of diesel consumption-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="diesel-form" method="post">
      	<div class="modal-body">
                        
            <table class="table table-striped" cellpadding="3px" >
                <tr>
                    <td><label>Date</label></td>
                    <td id='date-div'>
                        <input type="text" id="date-np" name="date_np" class='form-control'>
                        <span id='error-date' hidden>Date is required</span>
                    </td>
                    <td><input type="hidden" id="date-en" name="date_en" class='form-control'></td>
                </tr>
                <tr>
                    <td><label>Shift</label></td>
                    <td id="shift-div">
                        <select name="shift" id='shift' class="form-control">
                            <option value="">--select shift--</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                        </select>
                        <span id='error-shift' hidden>Shift is required</span>
                    </td>
                </tr>
                <tr>
                    <td><label>Production</label></td>
                    <td id="production-div">
                        <input type="number" id="production" name="production" class='form-control' >
                        <span id='error-production' hidden>Production is required</span>
                    </td>
                </tr>
                <tr>
                    <td><label>Working Hour</label></td>
                    <td id="hour-div">
                        <select name="hour" style="max-height: 200px" class="form-control" id="hour">
                            <option value="">--select hour--</option>
                            <?php for($i = 0; $i < 24; $i++){?>}
                            <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php }?>
                        </select>
                        <!-- <input type="number" id="hour" name="hour" class='form-control'> -->
                        <span id='error-hour' hidden>Hour is required</span>
                    </td>
                </tr>
                <tr>
                    <td><label>Working Minutes</label></td>
                    <td id="hour-div">
                        <select name="min" style="max-height: 200px" class="form-control" id="min">
                            <option value="">--select minute--</option>
                            <?php for($i = 0; $i < 60; $i++){?>}
                            <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php }?>
                        </select>
                        <!-- <input type="number" id="hour" name="hour" class='form-control'> -->
                        <span id='error-hour' hidden>Minute is required</span>
                    </td>
                </tr>
                <tr>
                    <td><label>Segregation Hour</label></td>
                    <td id="sh-div">
                        <select name="segregation_hr" style="max-height: 200px" class="form-control" id="segregation_hr">
                            <option value="">--select segregation hour--</option>
                            <?php for($i = 0; $i < 24; $i++){?>}
                            <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php }?>
                        </select>
                        <!-- <input type="number" id="segregation_hr" name="segregation_hr" min="0" max="59" class='form-control'> -->
                        <span id='error-sh' hidden>Segregation hour is required</span>
                    </td>
                </tr>
                <tr>
                    <td><label>Segregation Minutes</label></td>
                    <td id="sh-div">
                        <select name="segregation_min" style="max-height: 200px" class="form-control" id="segregation_min">
                            <option value="">--select segregation minutes--</option>
                            <?php for($i = 0; $i < 60; $i++){?>}
                            <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php }?>
                        </select>
                        <!-- <input type="number" id="segregation_hr" name="segregation_hr" min="0" max="59" class='form-control'> -->
                        <span id='error-sh' hidden>Segregation minutes is required</span>
                    </td>
                </tr>
                 
                    <?php /*<input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>">*/?>
                
                    <input type="hidden" id="id" name="id"/>
                </table>
        
        </div>
    
        <div class="modal-footer">
            <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#date-np').nepaliDatePicker({
			// ndpEnglishInput: 'date_en',
			// onChange: function(){
			// 	var date = $('#date-np').val();
			// 	var date_en = $('#date-ep').val();
			// 	$.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
			// 		$('#display-div').html(data);
			// 	},'html');
			// }
		});
		$('#diesel-table').dataTable({
            order: [[ 0, 'desc' ]],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
        $('#error-date, #error-shift, #error-production, #error-hour, #error-sh').hide();
        $('#date-div, #shift-div, #production-div, #sh-div, #hour-div').removeClass('has-error');
		$('#myModalLabel').text('Scrap Segregation Form');
                $('#date-np').val('');
                $('#date-en').val('');
                $('#shift').val('');
                $('#production').val('');
                $('#hour').val('');
                $('#min').val('');
                $('#segregation_hr').val('');
                $('#segregation_min').val('');
                $('#id').val();
                
		$('#form-dialog').modal();
	});
</script>
<script type="text/javascript">
	$('#save').click(function(){
        var date = $('#date-np').val();
        var shift = $('#shift').val();
        var production = $('#production').val();
        var hour = $('#hour').val();
        var shr = $('#segregation_hr').val();
        var i = 0;
        if(date == ''){
            $('#date-div').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date-div').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(shift == ''){
            $('#shift-div').addClass('has-error');
            $('#error-shift').show();
        }else{
            $('#shift-div').removeClass('has-error');
            $('#error-shift').hide();
            i++;
        }
        if(production == ''){
            $('#production-div').addClass('has-error');
            $('#error-production').show();
        }else{
            $('#production-div').removeClass('has-error');
            $('#error-production').hide();
            i++;
        }
        if(hour == ''){
            $('#hour-div').addClass('has-error');
            $('#error-hour').show();
        }else{
            $('#hour-div').removeClass('has-error');
            $('#error-hour').hide();
            i++;
        }
        if(shr == ''){
                $('#error-sh').show();
                $('#sh-div').addClass('has-error');
                // $('error-sh').show();
            }else{
                $('#error-sh').hide();
                $('#sh-div').removeClass('has-error');
                // $('error-sh').hide();
                i++;
            }
        // alert(i);
        if(i == 5){
    		$.post("<?php echo site_url('scrap_segregation/save')?>",$("#diesel-form").serializeArray(),function($data){
                if($data.success){
                    alert('Data saved');
                    location.reload();
                }
            },'json');
        }
	});
</script>
<script type="text/javascript">
    function edit(id){
        $('#myModalLabel').text('Scrap Segregation Form');
        $('#error-date, #error-shift, #error-production, #error-hour, #error-sh').hide();
        $('#date-div, #shift-div, #production-div, #sh-div, #hour-div').removeClass('has-error');
        $.post('<?php echo site_url('scrap_segregation/getData')?>',{id:id},function(data){
            $('#date-np').val(data.date_np);
            $('#date-en').val(data.date_en);
            $('#shift').val(data.shift);
            $('#production').val(data.production);
            $('#hour').val(data.hour);
            $('#min').val(data.min);
            $('#segregation_hr').val(data.segregation_hr);
            $('#segregation_min').val(data.segregation_min);
            $('#id').val(data.id);
                
        },'json');
        $('#form-dialog').modal();
    }
</script>
<script type="text/javascript">
    function delete_record(id){
        $.post('<?php echo site_url('scrap_segregation/delete_json')?>',{id:id},function(data){
            alert(data.msg);
            if(data.success){
                location.reload();
            }
        },'json');
    }
</script>

