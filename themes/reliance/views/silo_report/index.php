<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-6 form-group">
                        <button class="btn btn-primary" id="add-btn">Add</button>
                        <button class="btn btn-warning" onclick="form_edit('<?php echo $date_en?>')">EDIT</button>
                    </div>
                    
                    <form action="<?php echo site_url('silo_report')?>" method="post">
                        <div class="col-md-5 form-group">
                            <input type="text" name="date_np" class="form-control" id="search_by_date_np" placeholder="Date" value="<?php echo $this->input->post('date_np')?>">
                            <script>
                                $('#search_by_date_np').change(function(){
                                      $('#search_by_date_en').val(BS2AD($('#search_by_date_np').val()));
                                 });
                            </script>
                            <input type="hidden" name="date_en" id="search_by_date_en" value="<?php echo $this->input->post('date_en')?>">
                        </div>
                        <div class="col-md-1 form-group">
                            <button class="btn btn-primary" id="search">Search</button>
                        </div>
                    </form>

                    <div class="col-sm-9"></div>
                    <div class="col-xs-3 callout callout-success">
                        Date: <?php echo $date_np?>
                    </div>
                    <div class="col-sm-12">
                        <table id="brand_list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Date(Nepali)</th>
                                    <th>Date(English)</th>
                                    <th>Raw Material</th>
                                    <?php foreach ($brand_type as $key => $value) {?>
                                        <th><?php echo $value['name']?></th>
                                    <?php }?>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key => $value) {?>
                                <tr>
                                    <td><?php echo $date_np?></td>
                                    <td><?php echo $date_en?></td>
                                    <td><?php echo $value['material_name']?></td>
                                    <?php foreach ($brand_type as $i => $val) {
                                        echo '<td>'.number_format($value[$val['name']],3).'</td>';
                                    }?>
                                </tr>
                            <?php }?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Total:</th>
                                    <?php foreach ($brand_type as $key => $value) {?>
                                        <th></th>
                                    <?php }?>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="daily-production-sales-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-3 control-label">Date</label>
                            <div class="col-sm-9">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="Date">
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                                <div id="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                            </div>
                        </div>
                        <?php foreach ($rows as $key => $value) {?>
                            <div class="form-group">
                                <label for="rows" class="col-sm-3 control-label"><?php echo $value['material_name']?></label>
                                <?php foreach ($brand_type as $i => $val) {?>
                                    <div class="col-sm-3">
                                        <input type="number" class='form-control' name="material_amount[<?php echo $value['material_id']?>][<?php echo $val['id']?>]" id="material_amount_<?php echo $value['material_id']?>_<?php echo $val['id']?>" placeholder="<?php echo $val['name']?>">
                                    </div>
                                <?php }?>
                            </div>
                        <?php }?>

                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": false
                }
            ],

            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                var i = 3;
                <?php foreach ($brand_type as $key => $value) {?>
                    // Total over all pages
                    total = api
                        .column( i )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( i ).footer() ).html(
                        // total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        total.toLocaleString('en-US', {
                            minimumFractionDigits: 3
                        })
                    );
                    i++;
                <?php }?>

                    
            }
        });

        // nepali date picker
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });

        $('#search_by_date_np').nepaliDatePicker({
            ndpEnglishInput: 'search_by_date_en'
        });

    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('input').val('');
        $('.error-div').hide();
        $('option').attr('selected', false);
        $('#myModalLabel').html('Add Daily Production Sales');
        $('#daily-production-sales-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var i = 0;
        var date_np = $('#date_np').val();
        var material_id = $('#material_id').val();
        var brand_id = $('#brand_id').val();

        if(date_np == ''){
            i = 1;
            $('#error_date_np').show();
        }else{
            $('#error_date_np').hide();
        }

        if(i == 0){
            $.post('<?php echo site_url("silo_report/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(date_en){
        $('.error-div').hide();
        $.post('<?php echo site_url("silo_report/get_value")?>',{date_en:date_en},function(data){
            $.each(data, function( index, value ) {
                console.log(value);
                $('#date_np').val(value.date_np);
                $('#date_en').val(value.date_en);
                $('#material_amount_'+value['material_id']+'_'+value['type_id']).val(value.amount);
            });
            $('#myModalLabel').html('Edit Daily Production Sales');
            $('#daily-production-sales-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_data(id){
        $.post('<?php echo site_url("daily_production_sale/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>