<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-6 form-group">
                        <button class="btn btn-primary" id="add-btn">Add</button>
                    </div>
                    <form method="post"action="<?php echo site_url('electric_meter_consumption')?>">
                        <div class="col-md-4">
                            <input type="text" name="date_np" class="form-control" id="date_np_search" placeholder="Date" value="<?php echo $this->input->post('date_np')?>">
                            <input type="hidden" name="date_en" class="form-control" id="date_en_search" placeholder="Date">
                            <script>
                                $('#date_np').change(function(){
                                      $('#date_en').val(BS2AD($('#date_np').val()));
                                 });
                            </script>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary" id="add-btn">View Detail</button>
                        </div>
                    </form>
                        
                    <div class="col-xs-12">
                        <table id="brand_list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Meter</th>
                                    <th>Consumption</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $key => $value) {?>
                                <tr>
                                    <td><?php echo $value['date_np']?></td>
                                    <td><?php echo $value['meter']?></td>
                                    <td><?php echo number_format($value['consumption'],2);?></td>
                                    <td>
                                        <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger" onclick="delete_data(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="daily-production-sales-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="Date">
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                                <div id="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                            </div>
                        </div>
                        <?php foreach ($meters as $key => $value) {?>
                            <div class="form-group hour_input">
                                <label for="brand_type" class="col-sm-4 control-label"><?php echo $value['meter']?></label>
                                <div class="col-sm-8">
                                    <input type="number" name="consumption[<?php echo $value['id']?>]" class="form-control consumption_input" id="consumption_<?php echo $value['id']?>">
                                    <input type="hidden" name="meter_id[]" class="meter_id" value="<?php echo $value['id']?>">
                                    <div id="error_work_<?php echo $value['id']?>" hidden style="color: red" class="error-div">Must be between 0 to 24</div>
                                </div>
                            </div>
                        <?php }?>

                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });

        // nepali date picker
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
        $('#date_np_search').nepaliDatePicker({
            ndpEnglishInput: 'date_en_search'
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('.consumption_input').val('');
        $('date_np').val('');
        $('date_en').val('');
        $('.error-div').hide();
        $('option').attr('selected', false);
        $('#myModalLabel').html('Add Electircal Meter Report');
        $('#daily-production-sales-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var date_np = $('#date_np').val();
        var i = 0;

        if(date_np == ''){
            i = 1;
            $('#error_date_np').show();
        }else{
            $('#error_date_np').hide();
        }

        if(i == 0){
            $.post('<?php echo site_url("electric_meter_consumption/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.error-div').hide();
        $.post('<?php echo site_url("electric_meter_consumption/get_value")?>',{id:id},function(data){
            $('#date_np').val(data.date_np)
            $('#date_en').val(data.date_en)
            $.each(data, function( index, value ) {
                $('#consumption_'+value.meter_id).val(value.consumption);
                console.log('#consumption_'+value.meter_id +' = '+ value.consumption)
            });
            $('#myModalLabel').html('Edit Electircal Meter Report');
            $('#daily-production-sales-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_data(id){
        $.post('<?php echo site_url("electric_meter_consumption/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>