<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<button class='btn btn-primary margin' id='add'>Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="diesel-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date</th>
				                    <th>Scrap</th>
				                    <th>Segregated Wasted</th>
				                    <th>Burnt Scrap</th>
                                    <th>Sieved Dust</th>
                                    <th>Final Chipps</th>
				                    <th>Foaming Scrap</th>
                                    <th>Action</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo number_format($row['scrap'],3)?></td>
				                        <td><?php echo number_format($row['segregation_waste'],3) ?></td>
                                        <td><?php echo number_format($row['burnt_scrap'],3)?></td>
				                        <td><?php echo number_format($row['sieved_dust'],3)?></td>
                                        <td><?php echo number_format($row['final_chipps'],3)?></td>
				                        <td><?php echo number_format($row['scrap']-$row['segregation_waste']-$row['burnt_scrap']-$row['final_chipps'],3)?></td>
                                        <td><button class='btn btn-warning' onclick='edit(<?php echo $row['id']?>)'>Edit</button> <button class='btn btn-danger' onclick='delete_record(<?php echo $row['id']?>)'>Delete</button></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of diesel consumption-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="diesel-form" method="post">
      	<div class="modal-body">
                        
            <table class="table table-striped" cellpadding="3px" >
                <tr>
                    <td><label>Date</label></td>
                    <td id='date-div'>
                        <input type="text" id="date-np" name="date_np" class='form-control'>
                        <span id="error-date" hidden>Date is required</span>
                    </td>
                    <input type="hidden" id="date-en" name="date_en" class='form-control'>
                </tr>
                <tr>
                    <td><label>Scrap</label></td>
                    <td class="amount-div" id="scrap_div">
                        <input type="number" id="scrap" name="scrap" class='form-control amount-div'>
                        <span id="error-scrap" hidden>Scrap amount is required</span>
                    </td>
                </tr>
                <tr>
                    <td><label>Segregated Waste</label></td>
                    <td class="amount-div"><input type="number" id="segregation_waste" name="segregation_waste" class='form-control amount-div'></td>
                </tr>
                <tr>
                    <td><label>Burnt Scrap</label></td>
                    <td class="amount-div"><input type="number" id="burnt_scrap" name="burnt_scrap" min="0" max="59" class='form-control amount-div'></td>
                </tr>
                <tr>
                    <td><label>Sieved Dust</label></td>
                    <td class="amount-div"><input type="number" id="sieved_dust" name="sieved_dust" min="0" max="59" class='form-control amount-div'></td>
                </tr>
                <tr>
                    <td><label>Final Chipps</label></td>
                    <td class="amount-div"><input type="number" id="final_chipps" name="final_chipps" min="0" max="59" class='form-control amount-div'></td>
                </tr>
                <tr>
                    <td><label>Foaming Scrap</label></td>
                    <td class="amount-div"><input type="number" id="foaming_scrap" name="foaming_scrap" min="0" max="59" class='form-control amount-div' readonly></td>
                </tr>

                    <?php /*<input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>">*/?>
                
                    <input type="hidden" id="id" name="id"/>
                </table>
        
        </div>
    
        <div class="modal-footer">
            <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#date-np').nepaliDatePicker({
			// ndpEnglishInput: 'date_en',
			// onChange: function(){
			// 	var date = $('#date-np').val();
			// 	var date_en = $('#date-ep').val();
			// 	$.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
			// 		$('#display-div').html(data);
			// 	},'html');
			// }
		});
		$('#diesel-table').dataTable({
            order: [[ 0, 'desc' ]],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
        $('#error-date, #error-scrap, #error-amount').hide();
        $('#date-div, #scrap_div, #amount-div').removeClass('has-error')
		$('#myModalLabel').text('Scrap Record Form');
                $('#date-np').val('');
                $('#date-en').val('');
                $('#scrap').val('');
                $('#segregation_waste').val('');
                $('#burnt_scrap').val('');
                $('#sieved_dust').val('');
                $('#final_chipps').val('');
                $('#foaming_scrap').val('');
                $('#id').val('');
                
		$('#form-dialog').modal();
	});
</script>
<script type="text/javascript">
	$('#save').click(function(){
        var date= $('#date-np').val();
        var scrap_text = $('#scrap').val();
        var scrap=  parseFloat($('#scrap').val());
        var segregation=  parseFloat($('#segregation_waste').val());
        var burnt=  parseFloat($('#burnt_scrap').val());
        var sieved=  parseFloat($('#sieved_dust').val());
        var final=  parseFloat($('#final_chipps').val());
        var foaming=  parseFloat($('#foaming_scrap').val());
        var i = 0;
        if(date == ''){
            $('#date-div').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date-div').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(scrap_text == ''){
            $('#scrap_div').addClass('has-error');
            $('#error-scrap').show();
        }else{
            $('#scrap_div').removeClass('has-error');
            $('#error-scrap').hide();
            i++;
            if(scrap <= segregation + burnt +sieved +final +foaming && foaming < 0){
                $('.amount-div').addClass('has-error');
                $('#error-amount').show();
            }else{
                $('.amount-div').removeClass('has-error');
                $('#error-amount').hide();
                i++;
            }
        }
        if(i == 3){
    		$.post("<?php echo site_url('scrap_report/save')?>",$("#diesel-form").serializeArray(),function($data){
                if($data.success){
                    alert('Data saved');
                    location.reload();
                }
            },'json');
        }
	});
</script>
<script type="text/javascript">
    $('#scrap, #segregation_waste, #burnt_scrap, #final_chipps, #sieved_dust').keyup(function(){
        var scrap = $('#scrap').val();
        var segregation_waste = $('#segregation_waste').val();
        var burnt_scrap = $('#burnt_scrap').val();
        var final_chipps = $('#final_chipps').val();
        var sieved_dust = $('#sieved_dust').val();

        $('#foaming_scrap').val(scrap-segregation_waste-burnt_scrap-final_chipps-sieved_dust);
    });
</script>
<script type="text/javascript">
    function edit(id){
        $('#myModalLabel').text('Scrap Record Form');
        $('#error-date, #error-scrap, #error-amount').hide();
        $('#date-div, #scrap-div, #amount-div').removeClass('has-error')
        $.post('<?php echo site_url('scrap_report/getData')?>',{id:id},function(data){
            $('#date-np').val(data.date_np);
            $('#date-en').val(data.date_en);
            $('#scrap').val(data.scrap);
            $('#segregation_waste').val(data.segregation_waste);
            $('#burnt_scrap').val(data.burnt_scrap);
            $('#sieved_dust').val(data.sieved_dust);
            $('#final_chipps').val(data.final_chipps);
            $('#foaming_scrap').val(data.foaming_scrap);
            $('#id').val(data.id);
                
        },'json');
        $('#form-dialog').modal();
    }
</script>
<script type="text/javascript">
    function delete_record(id){
        $.post('<?php echo site_url('scrap_report/delete_json')?>',{id:id},function(data){
            alert(data.msg);
            if(data.success){
                location.reload();
            }
        },'json');
    }
</script>
