<style>
    #table-form td{
        min-width: 150px;
    }
</style>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<?php $i = 0?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Purchase Request Form</a></li>
                <li><a href="#tab_2" data-toggle="tab">Pending Request <small class="label pull-right bg-yellow"><?php echo $pending_no?></small></a></li>
                <li><a href="#tab_3" data-toggle="tab">Approved Request <small class="label pull-right bg-green"><?php echo $approved_no?></small></a></li>
                <li><a href="#tab_4" data-toggle="tab">Disapproved Request <small class="label pull-right bg-red"><?php echo $disapproved_no?></small></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    
                    <form id='form'>
                    <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div class="box-body table-responsive no-padding">
                              <table class="table table-hover" id="table-form">
                                <tr>
                                  <th>Material Name</th>
                                  <th>Dealer Name</th>
                                  <th>Requested date</th>
                                  <th>Unit Type</th>
                                  <th>Current Stock</th>
                                  <th>Requested Quantity</th>
                                  <th>Cost Per Unit</th>
                                  <th>Total Cost</th>
                                  <th>Remarks</th>
                                </tr>
                                <tr>
                                    <td>
                                        <select name='material_id[]' id='materail_id-<?php echo $i?>' onchange='detail(<?php echo $i?>)' class="form-control">
                                            <option value="">-- material --</option>
                                            <?php foreach ($materials as $material){?>
                                                <option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="dealer_id[]" id="dealer-<?php echo $i?>" class="form-control">
                                            <option value="">-- dealer--</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="requested_date[]" id='date-<?php echo $i?>' class="form-control date">
                                    </td>
                                    <td>
                                        <input type="text" name="unit[]" id="unit-<?php echo $i?>" disabled="true" class="form-control">
                                    </td>
                                    <td>
                                        <input type="number" name="current_stock[]" id="stock-<?php echo $i?>" disabled="true" class="form-control">
                                    </td>
                                    <td>
                                        <input type="number" name="requested_quantity[]" id="qty-<?php echo $i?>" onkeyup="total_cost(<?php echo $i?>)" class="form-control">
                                    </td>
                                    <td>
                                        <input type="number" name="cost_per_unit[]" id="unit_cost-<?php echo $i?>" onkeyup="total_cost(<?php echo $i?>)" class="form-control">
                                    </td>
                                    <td>
                                        <input type="number" name="total_cost[]" id="total-<?php echo $i?>" disabled="true" class="form-control">
                                        <input type="hidden" name="request_cost[]" id="request_cost<?php echo $i?>">
                                    </td>
                                    <td>
                                        <input type="text" name="remark[]" class="form-control">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger delete_row" id="delete_row-<?php echo $i?>" onclick="delete_row(<?php echo $i?>)"><i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                
                              </table>
                            </div><!-- /.box-body -->
                          </div><!-- /.box -->
                        </div>
                      </div>
                    <div>
                        <button type="button" id="add-row" class="btn btn-primary margin">Add</button>
                        <button type='button' id='submit-form' class='btn btn-primary margin'>Send</button>
                    </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab_2">
                <div class="box-body table-responsive no-padding">
                    <table id="table-pending" class="table table-bordered table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Date</th>
                                            <th>Material</th>
                                            <th>Dealer</th>
                                            <th>Requested Quantity</th>
                                            <th>Unit Cost</th>
                                            <th>Total Cost</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($pending_requests as $request) { ?>
                                            <tr>
                                            <form id='request-form-<?php echo $request['id']?>'>
                                                <td><?php echo $request['id']?></td>
                                                <td><?php echo $request['requested_date_nepali'] ?></td>
                                                <td><?php echo $request['material_name'] ?></td>
                                                <td><?php echo $request['dealer_name'] ?></td>
                                                <td><?php echo number_format($request['requested_quantity'],3) ?></td>
                                                <td><?php echo number_format($request['request_cost'],3) ?></td>
                                                <td><?php echo number_format($request['request_cost']*$request['requested_quantity'],3) ?></td>
                                                <td>
                                                    <input type='hidden' name="id" value='<?php echo $request['id']?>'>
                                                    <button type="button" class="btn btn-warning" onclick="pendingForm(<?php echo $request['id'] ?>)" id="pending-edit-<?php echo $request['id'] ?>" value="<?php echo $request['id'] ?>"> <i class="fa fa-pencil"></i></button>
                                                    <button type="button" class="btn btn-danger" onclick="delete_pending_row(<?php echo $request['id'] ?>)" id="pending-<?php echo $request['id'] ?>" value="<?php echo $request['id'] ?>"> <i class="fa fa-trash"></i></button>
                                                </td>
                                            </form>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </div>
                </div>
                <div class="tab-pane" id="tab_3">
                <div class="box-body table-responsive no-padding">
                    <table id="table-approved" class="table table-bordered table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <td><?php echo $request['id']?></td>
                                            <th>Requested Date</th>
                                            <th>Approved Date</th>
                                            <th>Material</th>
                                            <th>Dealer</th>
                                            <th>Requested Quantity</th>
                                            <th>Requested Unit Cost</th>
                                            <th>Requested Cost</th>
                                            <th>Approved Quantity</th>
                                            <!-- <th>Approved Unit Cost</th>
                                            <th>Approved Total Cost</th> -->
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($approved_requests as $request) { ?>
                                            <tr>
                                                <td></td>
                                                <td><?php echo $request['requested_date_nepali'] ?></td>
                                                <td><?php echo $request['accepted_date_nepali'] ?></td>
                                                <td><?php echo $request['material_name'] ?></td>
                                                <td><?php echo $request['dealer_name'] ?></td>
                                                <td><?php echo number_format($request['requested_quantity'],3) ?></td>
                                                <td><?php echo number_format($request['request_cost'],3) ?></td>
                                                <td><?php echo number_format($request['request_cost']*$request['requested_quantity'],3) ?></td>
                                                <td><?php echo number_format($request['accepted_quantity'],3) ?></td>
                                                <?php /*<td><?php echo number_format($request['accepted_cost'],3) ?></td>
                                                <td><?php echo number_format($request['accepted_cost'],3) ?></td>*/?>
                                                <td><?php echo $request['approved_remarks']?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </div>
                </div>
                <div class="tab-pane" id="tab_4">
                <div class="box-body table-responsive no-padding">
                    <table id="table-disapproved" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <td><?php echo $request['id']?></td>
                                <th>Requested Date</th>
                                <th>Approved Date</th>
                                <th>Material</th>
                                <th>Dealer</th>
                                <th>Requested Quantity</th>
                                <th>Requested Unit Cost</th>
                                <th>Requested Cost</th>
                                <th>Remarks</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($disapproved_requests as $request) { ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $request['requested_date_nepali'] ?></td>
                                    <td><?php echo $request['accepted_date_nepali'] ?></td>
                                    <td><?php echo $request['material_name'] ?></td>
                                    <td><?php echo $request['dealer_name'] ?></td>
                                    <td><?php echo number_format($request['requested_quantity'],3 )?></td>
                                    <td><?php echo number_format($request['request_cost'],3 )?></td>
                                    <td><?php echo number_format($request['request_cost']*$request['requested_quantity'],3 )?></td>
                                    <td><?php echo $request['approved_remarks']?></td>
                                    <td style="min-width:200px">
                                        <button class="btn btn-warning margin"><i class="fa fa-mail-reply" ></i></button>
                                        <button class="btn btn-danger margin"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
        var table = $('#table-pending').DataTable({
            "aaSorting": [ [1,'desc'], [0,'desc']],
            "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }]
        });
</script>

<script type="text/javascript">
    function detail(i){
        $("#dealer-"+i).empty();
        var id = $('#materail_id-'+i).val();
        $.post('<?php echo site_url()?>purchase_request/getdata',{id:id},function(data){
            $.each(data.dealers,function(index,value){
                $("#dealer-"+i).append
                ($('<option>',{
                    value:value.dealer_id,
                    text:value.dealer_name
                }));
            });
            $("#unit-"+i).val(data.unit);
            if(data.stock){
                $("#stock-"+i).val(data.stock);
            }
        },'json');
    };
</script>
<script type="text/javascript">
    function total_cost(index){
        var unit_cost = $('#unit_cost-'+index).val();
        var qty = $('#qty-'+index).val();
        var total = unit_cost * qty;
        $("#total-"+index).val(total);
        $("#request_cost"+index).val(total);
    }
</script>
<script type="text/javascript">
    function delete_row(i) {
            var r = confirm('Are sure to remove this row ?');
            if (r == true) {
                $('#delete_row-'+i).closest('tr').remove();
                alert('Row removed');
            } else {
                return false;
            }
    };
</script>
<script type="text/javascript">
    function delete_pending_row(i) {
            var r = confirm('Are sure to remove this row ?');
            if (r == true) {
                // $('#delete_pending_row-'+i).closest('tr').remove();
                $.post("<?php echo site_url('purchase_request/delete_json')?>",{id:i},function(data){
                    if(data.success){
                        table
                            .row( $('#pending-'+i).parents('tr') )
                            .remove()
                            .draw();
                        alert('Request removed');
                        location.reload();
                    }else{
                        alert('Request not removed');
                    }
                },'json');
            } else {
                return false;
            }
                
    };
</script>
<script type="text/javascript">
var i = 0;
    $('#add-row').click(function(){
        material_id = $('#material_id'+i).val();
        dealer_id = $('#dealer_id'+i).val();
        date = $('#date-'+i).val();
        request_qty = $('#qty-'+i).val();
        request_amount = $('#unit-cost-'+i).val();

        if(material_id != '' && dealer_id != '' && date != '' && request_qty != '' && request_amount != ''){
            $.post('<?php echo site_url('purchase_request/add_row')?>',{index:++i},function(data){
                $('#table-form').append(data);
            },'html');
        }else{
            alert('Please enter all required fields');
        }
    });
</script>
<script type="text/javascript">
var title_row = "<tr><th>Material Name</th><th>Dealer Name</th><th>Requested date</th>"+
              "<th>Unit Type</th><th>Current Stock</th><th>Requested Quantity</th><th>Cost Per Unit</th>"+
              "<th>Total Cost</th><th>Remarks</th></tr>";
    $('#submit-form').click(function(){

        material_id = $('#material_id'+i).val();
        dealer_id = $('#dealer_id'+i).val();
        date = $('#date-'+i).val();
        request_qty = $('#qty-'+i).val();
        request_amount = $('#unit-cost-'+i).val();


        
        if(material_id != '' && dealer_id != '' && date != '' && request_qty != '' && request_amount != ''){
            
            $.post('<?php echo site_url('purchase_request/save')?>',$("#form").serializeArray(),function(data){
                if(data.success){
                    alert('Request send sucessfully');
                    $('#table-form').empty();
                    $.post('<?php echo site_url('purchase_request/add_row')?>',{index:++i},function(data){
                        $('#table-form').append(title_row);                                  
                        $('#table-form').append(data);
                        location.reload();
                    },'html');
                }else{
                    alert('Sending failed');
                }
            },'json');
        }else{
            alert('Please enter all field');
        }
    });
</script>
<script>
    $(function () {
        $('#table-approved,#table-disapproved').DataTable({
            "aaSorting": [ [1,'desc'], [0,'desc']],
            "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }]
        });
        $('#date-<?php echo $i?>').nepaliDatePicker();
    });
</script>

<!--edit modal-->
<div class="modal fade" id="edit-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="edit-stock-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Material</label>
                        <div class="col-sm-8">
                            <select name='material_id' id='edit-materail-id' onchange='editDetail()' class="form-control">
                                <option value="">-- material --</option>
                                <?php foreach ($materials as $material){?>
                                    <option value="<?php echo $material['material_id']?>" id='edit-materail-id-<?php echo $material['material_id']?>'>
                                        <?php echo $material['material_name']?>
                                    </option>
                                <?php }?>
                            </select>
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Dealer</label>
                        <div class="col-sm-8" id="edit-dealer">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Request Date</label>
                        <div class="col-sm-8">
                            <input type="text" name="requested_date" id='edit-date' class="form-control date">
                            <input type="hidden" name="requested_date_en" id='edit-date-en' class="form-control date">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Unit</label>
                        <div class="col-sm-8">
                            <input type="text" name="unit" id="edit-unit" disabled="true" class="form-control">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Current Stock</label>
                        <div class="col-sm-8">
                            <input type="number" name="current_stock" id="edit-stock" disabled="true" class="form-control">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Requested Quantity</label>
                        <div class="col-sm-8">
                            <input type="number" name="requested_quantity" id="edit-qty" onkeyup="total_cost(<?php echo $i?>)" class="form-control">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Cost Per Unit</label>
                        <div class="col-sm-8">
                            <input type="number" name="cost_per_unit" id="edit-unit_cost" onkeyup="total_cost(<?php echo $i?>)" class="form-control">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Total Cost</label>
                        <div class="col-sm-8">
                            <input type="number" name="total_cost" id="edit-total" disabled="true" class="form-control">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-4 control-label">Remarks</label>
                        <div class="col-sm-8">
                            <input type="text" name="remark" id='edit-remark' class="form-control">
                            <span class="error_message" id="error-material" style="color: red" hidden="true">material is required</span>
                        </div>
                    </div>


                    
<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

                        <input type="hidden" name="request_id" id="edit-request-id">
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-edit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end of edit modal -->

<script type="text/javascript">
    $(function(){
        $('#edit-date').nepaliDatePicker({
            ndpEnglishInput: 'edit-date-en'
        });
    })
</script>

<script type="text/javascript">
    function pendingForm(id){
        $.post('<?php echo site_url("purchase_request/getPurchaseRequest")?>',{id:id},function(data){

            $('#myModalLabel').text('Edit Purchase Request');
            // populating modal
            $('#edit-materail-id-'+data.material_id).prop('selected',true);
            console.log(data.dealers);
            $('#edit-dealer').empty();
            $('#edit-dealer').append(data.dealers);
            $('#edit-date').val(data.requested_date_nepali);
            $('#edit-date-en').val(data.requested_date_english);
            $('#edit-unit').val(data.unit);
            $('#edit-stock').val(data.current_stock);
            $('#edit-qty').val(data.requested_quantity);
            $('#edit-unit_cost').val(data.unit_cost);
            $('#edit-request-id').val(data.id);
            var total = parseFloat(data.requested_quantity) * parseFloat(data.unit_cost);
            
            $('#edit-total').val(total);
            $('#edit-remark').val(data.remark);
           
            $('#edit-dialog').modal();

        },'json');
    }
</script>
<script type="text/javascript">
    $('#form-edit').click(function(){

        material_id = $('#edit-material-id').val();
        dealer_id = $('#edit-dealer-id').val();
        date = $('#edit-date').val();
        request_qty = $('#edit-qty').val();
        request_amount = $('#edit-unit_cost').val();

        
        if(material_id != '' && dealer_id != '' && date != '' && request_qty != '' && request_amount != ''){
            
            $.post('<?php echo site_url('purchase_request/editRequest')?>',$("#edit-stock-form").serializeArray(),function(data){
                if(data.success){
                    alert('Request send sucessfully');
                    location.reload();
                }else{
                    alert('Sending failed');
                }
            },'json');
        }else{
            alert('Please enter all field');
        }
    });
</script>
<script type="text/javascript">
    function edit_pending_row(id){
        r = confirm('Do you want to edit request?'+id);
        if(r){
            $.post('<?php echo site_url("purchase_request/edit")?>',$("#request-form-"+id).serializeArray(),function(data){
                if(data.success){
                    alert('Request updated')
                }else{
                    alert('Please try again')
                }
            },'json');
        }
    }
</script>
<script type="text/javascript">
    function calformtotal(id){
        var qty = $('#form-qty-'+id).val().replace(',','');
        var cost = $('#form-cost-'+id).val().replace(',','');
        var total = qty * cost;
        $('#form-total-'+id).val(total);
    }
</script>
<script type="text/javascript">
    function editDetail(){
        $("#edit-dealer-id").empty();
        id = $('#edit-materail-id').val();
        console.log(id);
        $.post('<?php echo site_url()?>purchase_request/getdata',{id:id},function(data){
            $.each(data.dealers,function(index,value){
                $("#edit-dealer-id").append
                ($('<option>',{
                    value:value.dealer_id,
                    text:value.dealer_name
                }));
            });
            $("#edit-unit_cost").val(data.unit);
            if(data.stock){
                $("#edit-stock").val(data.stock);
            }
        },'json');
    }

</script>

