<tr>
    <td>
        <select name='material_id[]' id='materail_id-<?php echo $i?>' onchange='detail(<?php echo $i?>)' class="form-control">
            <option value="">-- material --</option>
            <?php foreach ($materials as $material){?>
                <option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
            <?php }?>
        </select>
    </td>
    <td>
        <select name="dealer_id[]" id="dealer-<?php echo $i?>" class="form-control">
            <option value="">-- dealer--</option>
        </select>
    </td>
    <td>
        <input type="text" name="requested_date[]" class="form-control date" id="nep-date-<?php echo $i?>">
    </td>
    <td>
        <input type="text" name="unit[]" id="unit-<?php echo $i?>" disabled="true" class="form-control">
    </td>
    <td>
        <input type="mber" name="current_stock[]" id="stock-<?php echo $i?>" disabled="true" class="form-control">
    </td>
    <td>
        <input type="number" name="requested_quantity[]" id="qty-<?php echo $i?>" onkeyup='total_cost(<?php echo $i?>)' class="form-control">
    </td>
    <td>
        <input type="number" name="cost_per_unit[]" id="unit_cost-<?php echo $i?>" onkeyup='total_cost(<?php echo $i?>)' class="form-control">
    </td>
    <td>
        <input type="number" name="total_cost[]" id="total-<?php echo $i?>" disabled="true" class="form-control">
        <input type="hidden" name="request_cost[]" id="request_cost<?php echo $i?>">
    </td>
    <td>
        <input type="text" name="remark[]" class="form-control">
    </td>
    <td>
        <button type="button" class="btn btn-danger delete_row" id="delete_row-<?php echo $i?>" onclick='delete_row(<?php echo $i?>)'><i class="fa fa-trash"></i>
        </button>
    </td>
</tr>

<script type="text/javascript">
    function total_cost(index){
        var unit_cost = $('#unit_cost-'+index).val();
        var qty = $('#qty-'+index).val();
        var total = unit_cost * qty;
        $("#total-"+index).val(total);
        $("#request_cost"+index).val(total);
    }
</script>

<script type="text/javascript">
    function delete_row(i) {
        var r = confirm('Are sure to remove this row ?');
        if (r == true) {
            $('#delete_row-'+i).closest('tr').remove();
            alert('Row removed');
        } else {
            return false;
        }
    };
</script>
<script type="text/javascript">
        $('#nep-date-<?php echo $i?>').nepaliDatePicker();//use id
</script>
