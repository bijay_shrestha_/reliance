<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<form action='<?php echo site_url("stock")?>' method='post'>
	<input type='text' name='date' id='date' value="<?php echo $this->input->post('date')?>">
	<input type='text' name='en-date' id='en-date' value="<?php echo $this->input->post('en-date')?>">
	<button class='btn btn-primary margin'>Submit</button>
</form>
 <table id="example3" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>cd_id</th>
            <th>Material Name</th>
            <th>Opening</th>
            <th>Recived</th>
            <th>Consumed</th>
            <th>Closing</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materials as $material){?>
        <tr>
            <td><?php echo $material['cd_id']?></td>
            <td><?php echo $material['material_name']?></td>
            <td><?php if($opening_stocks != NULL){echo number_format(((array_key_exists($material['material_id'], $opening_stocks))?$opening_stocks[$material['material_id']]:'0'),3);}else{echo '0.000';}?></td>
            <td><?php 
            $total = 0;
            if(array_key_exists($material['material_id'], $recived_items)){
            	// foreach($recived_items[$material['material_id']] as $buy){
            		$total = $recived_items[$material['material_id']];
            	// }
            	echo number_format($total,3);
            }else{
            	echo '0';
            }?></td>
            <?php /*<td><?php echo (array_key_exists($material['material_id'], $dispatches))?(($dispatches[$material['material_id']]['all_dispatched']!='')?$dispatches[$material['material_id']]['all_dispatched']:'0'):'0'?></td>*/?>
            <td><?php 
            $total_consumed = 0;
            	if(array_key_exists($material['material_id'], $consumptions)){
            		// foreach($today_dispatches[$material['material_id']] as $out_going){
            			$total_consumed = $consumptions[$material['material_id']];
            		// }
            		echo number_format($total_consumed,3);
        		}else{
        			echo '0';
        		}?>
    		</td>
            <td>
            <?php 
            	$closing_stock = 0;
                if($opening_stocks != NULL){
                	if(array_key_exists($material['material_id'], $opening_stocks)){
                		$closing_stock += $opening_stocks[$material['material_id']];
                	}
                }
            	if(array_key_exists($material['material_id'], $consumptions)){
            		$closing_stock -= $consumptions[$material['material_id']];
            	}
            	$closing_stock += $total;
            	echo number_format($closing_stock,3);
        	?>
            </td>
        </tr>
        <?php }?>
    </tbody>
</table>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
	table = $('#example3').DataTable({
        "order": [[ 0, "asc" ]],
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
            },
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        "lengthMenu": [[100, 150, 200, -1], [100, 150, 200, "All"]]
    });
</script>
<script type="text/javascript">
	$(function () {
		$('#date').nepaliDatePicker({
            ndpEnglishInput: 'en-date'
        });
	});
</script>