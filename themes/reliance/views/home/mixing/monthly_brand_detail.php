<table id="brand-report-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Material</th>
            <th>Amount</th>
            <th>Percentage</th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach($raw_material['materials'] as $index => $material){
        ?>
        <tr>
            <td><?php echo $materials[$index]['material_name']?></td>
            <td><?php echo number_format($material['consumption']['item_amount'],3) ?></td>
            <td><?php echo number_format((($raw_material['total']!=0)?(($material['consumption']['item_amount']*100)/$raw_material['total']):0),2). '%'?></td>
        </tr>
    <?php 
    }
    ?>
    <tr>
        <th>Total Rawmaterial</th>
        <th><?php echo number_format($raw_material['total'],3)?></th>
        <th><?php echo number_format((($total != 0)?($raw_material['total']/$total)*100:0),3).'%'?></th>

    </tr>
    <?php if ($this->session->userdata('department_id') != 5){?>
        <tr style='height:20px'><td></td><td></td><td></td></tr>
        <tr>
            <th>Total Bought Scrap</th>
            <th><?php echo number_format($bought_scrap['total'],3)?></th>
            <th><?php echo number_format((($total != 0)?($bought_scrap['total']/$total)*100:0),3).'%'?></th>

        </tr>
        <tr style='height:20px'><td></td><td></td><td></td></tr>
        <?php
        foreach($factory_scrap['materials'] as $index => $material){
            ?>
            <tr>
                <td><?php echo $materials[$index]['material_name']?></td>
                <td><?php echo number_format($material['consumption']['item_amount'],3) ?></td>
                <td><?php echo number_format((($factory_scrap['total']!=0)?(($material['consumption']['item_amount']*100)/$factory_scrap['total']):0),2). '%'?></td>
            </tr>
        <?php 
        }
        ?>
        <tr>
            <th>Total Factory Scrap</th>
            <th><?php echo number_format($factory_scrap['total'],3)?></th>
            <th><?php echo number_format((($total != 0)?($factory_scrap['total']/$total)*100:0),3).'%'?></th>

        </tr>
    <?php }?>
        
    <tr style='height:20px'><td></td><td></td><td></td></tr>
    <tr>
        <th>Total</th>
        <th><?php echo number_format($total,3)?></th>
        <th><?php echo number_format((($total != 0)?($total/$total)*100:0),3).'%'?></th>

    </tr>
    </tbody>
</table>

        <script src="<?php echo theme_url()?>assets/js/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
var date = '<?php echo $date?>';
    $('#brand-report-table').dataTable({
            "paging":   false,
            "ordering": false,
            dom: 'Bfrti',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
                {
                    extend: 'pdfHtml5',
                    
                    customize: function ( doc ) {
                        doc.content.splice( 0, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            image: '<?php echo $this->pdf_logo?>',/*64 bit encoded image in public controller*/
                        })
                        doc.content.splice( 2, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Date : '+date
                        })
                        doc.content.splice( 3, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Report Type : Monthly Brand Report (Mixing Department)'
                        })
 
                    }
                },
            ],
        });
</script>