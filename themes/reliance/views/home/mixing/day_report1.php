<!-- DataTables -->
<?php 
print_r($last_entry_date);
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
    <div class='row'>
        <div class='col-md-6'>
<div class="box">
    <div class="box-header">
        Working Day Report
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <th>No. of Days</th>
                        <td><?php echo $working_days['monthly_working_day'] ?></td>
                        <td><?php echo $working_days['yearly_working_day'] ?></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
<div class='col-md-6'>
<div class="box">
    <div class="box-header">
        Consumption Report
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Daily (<?php echo $last_entry_date[0].'-'.$last_entry_date[1].'-'.$last_entry_date[2]?>)</th>
                    <th>Monthly (<?php echo $months[number_format($last_entry_date[1])].'-'.$last_entry_date[0]?>)</th>
                    <th>Yearly (<?php echo $last_entry_date[0]?>)</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <th>Raw Material</th>
                        <td><?php echo number_format($daily['raw_material'],3)?></td>
                        <td><?php echo number_format($monthly['raw_material'],3)?></td>
                        <td><?php echo number_format($yearly['raw_material'],3)?></td>
                    </tr>
                    <tr>
                        <th>Bought Scrap</th>
                        <td><?php echo number_format($daily['bought_scrap'],3)?></td>
                        <td><?php echo number_format($monthly['bought_scrap'],3)?></td>
                        <td><?php echo number_format($yearly['bought_scrap'],3)?></td>
                    </tr>
                    <tr>
                        <th>Factory Scrap</th>
                        <td><?php echo number_format($daily['factory_scrap'],3)?></td>
                        <td><?php echo number_format($monthly['factory_scrap'],3)?></td>
                        <td><?php echo number_format($yearly['factory_scrap'],3)?></td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <th><?php echo number_format($daily['raw_material'] + $daily['bought_scrap'] + $daily['factory_scrap'],3)?></th>
                        <th><?php echo number_format($monthly['raw_material'] + $monthly['bought_scrap'] + $monthly['factory_scrap'],3)?></th>
                        <th><?php echo number_format($yearly['raw_material'] + $yearly['bought_scrap'] + $yearly['factory_scrap'],3)?></th>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>