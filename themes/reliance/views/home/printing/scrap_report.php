<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$dates[0];
    }
?>
<link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <?php foreach($print_types as $key => $type){?>
    	<?php // print_r($type)?>
    	<div class="box">
		    <div class="box-header">
		    	<h4><?php echo $type['type']?> Loss Reason</h4>
		    </div>
		    <div class="box-body">
		        <table id="daily-dispatch-table" class="table table-bordered table-striped">
		            <thead>
		                <tr>
		                    <th rowspan='2'>Reason</th>
		                    <th colspan='2'>Last Entry Date(<?php echo $date?>)</th>
		                    <th colspan='2'>This Month(<?php echo $months[number_format($dates[1]-1)]?>)</th>
		                    <th colspan='2'>This Year(<?php echo $current_year?>)</th>
		                </tr>
		                <tr>
		                    <th>Total</th>
		                    <th>%</th>
		                    <th>Total</th>
		                    <th>%</th>
		                    <th>Total</th>
		                    <th>%</th>
		                </tr>
		            </thead>
		            <tbody>
		            <?php foreach($type['reasons'] as $res){?>
		                <tr>
		                    <th><?php echo $res['reason'] ?></th>
		                    <td><?php echo number_format(($res['daily']>0)?$res['daily']:0,3) ?></td>
		                    <td><?php echo number_format(($total[$key]['daily']>0)?($res['daily']/$total[$key]['daily'])*100:0,2) ?>%</td>
		                    <td><?php echo number_format(($res['monthly']>0)?$res['monthly']:0,3) ?></td>
		                	<td><?php echo number_format(($total[$key]['monthly']>0)?($res['monthly']/$total[$key]['monthly'])*100:0,2) ?>%</td>
		                    <td><?php echo number_format(($res['yearly']>0)?$res['yearly']:0,3) ?></td>
		                    <td><?php echo number_format(($total[$key]['yearly']>0)?($res['yearly']/$total[$key]['yearly'])*100:0,2) ?>%</td>
		                </tr>
		            <?php }?>
		            <tr>
		                <th>total</th>
	                    <th><?php echo number_format(($total[$key]['daily']>0)?$total[$key]['daily']:0,3) ?></th>
	                    <th><?php echo number_format(($total[$key]['daily']>0)?($total[$key]['daily']/$total[$key]['daily'])*100:0,2) ?>%</th>
	                    <th><?php echo number_format(($total[$key]['monthly']>0)?$total[$key]['monthly']:0,3) ?></th>
	                	<th><?php echo number_format(($total[$key]['monthly']>0)?($total[$key]['monthly']/$total[$key]['monthly'])*100:0,2) ?>%</th>
	                    <th><?php echo number_format(($total[$key]['yearly']>0)?$total[$key]['yearly']:0,3) ?></th>
	                    <th><?php echo number_format(($total[$key]['yearly']>0)?($total[$key]['yearly']/$total[$key]['yearly'])*100:0,2) ?>%</th>
	                </tr>
		            </tbody>
		        </table>
		    </div>
	    </div>
    <?php }?>