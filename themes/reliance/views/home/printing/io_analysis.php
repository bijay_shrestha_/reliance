<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
<?php       
$daily_total=0;
$monthly_total=0;
$yearly_total=0;
foreach($reasons as $index => $res){
    $daily_total += $daily_loss[$index]['total']['total'];
    $monthly_total += $monthly_loss[$index]['total']['total'];
    $yearly_total += $yearly_loss[$index]['total']['total'];
}
?>
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
<div class="box">
    <div class="box-header">
        Input Output Analysis Report
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Last Entry(<?php echo $date?>)</th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Input</th>
                    <td><?php echo number_format($total['daily']['total_input'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_input'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_input'],3) ?></td>
                </tr>
                <tr><td></td><td></td><td></td></tr>
                <tr>
                    <th>Output</th>
                    <td><?php echo number_format($total['daily']['total_output'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_output'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_output'],3) ?></td>
                </tr>
                <tr>
                    <th>Unprinted Scrap</th>
                    <td><?php echo number_format($total['daily']['total_unprinted_scrap'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_unprinted_scrap'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_unprinted_scrap'],3) ?></td>
                </tr>
                <tr>
                    <th>Printed Scrap</th>
                    <td><?php echo number_format($total['daily']['total_printed_scrap'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_printed_scrap'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_printed_scrap'],3) ?></td>
                </tr>
                <tr>
                    <th>Total</th>
                    <td><?php echo number_format($total['daily']['total_output']+$total['daily']['total_unprinted_scrap']+$total['daily']['total_printed_scrap'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_output']+$total['monthly']['total_unprinted_scrap']+$total['monthly']['total_printed_scrap'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_output']+$total['yearly']['total_unprinted_scrap']+$total['yearly']['total_printed_scrap'],3) ?></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Output Input Ratio</th>
                    <td><?php echo number_format($total['daily']['total_output']/$total['daily']['total_input'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_output']/$total['monthly']['total_input'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_output']/$total['yearly']['total_input'],3) ?></td>
                </tr>
                <tr>
                    <th>Number of Color Made</th>
                    <td><?php echo $total['daily']['no_of_color'] ?></td>
                    <td><?php echo $total['monthly']['no_of_color'] ?></td>
                    <td><?php echo $total['yearly']['no_of_color'] ?></td>
                </tr>
                <tr>
                    <th>Per Hour Output</th>
                    <td><?php echo number_format($total['daily']['total_output']/24,3) ?></td>
                    <td><?php echo (@$working_days_for_timeloss['monthly_working_day'])?number_format($total['monthly']['total_output']/(24*$working_days_for_timeloss['monthly_working_day']),3):'0.000' ?></td>
	                <td><?php echo (@$working_days_for_timeloss['yearly_working_day'])?number_format($total['yearly']['total_output']/(24*$working_days_for_timeloss['yearly_working_day']),3):'0.000' ?></td>
	            </tr>
	            <tr>
	            	<?php 
	            		$daily_val = (24*60 - ($daily_total))/60;
	            		$monthly_val = (((@$yearly_working_day)?((24*60*$working_days_for_timeloss['monthly_working_day'])):(24*60)) - ($monthly_total))/60;
	            		$yearly_val = (24*60*$working_days_for_timeloss['yearly_working_day'] - ($yearly_total))/60;
	                ?>
	                <th>Per Work Hour Output</th>
	                <td><?php echo number_format($total['daily']['total_output']/$daily_val,3) ?></td>
	                <td><?php echo number_format($total['monthly']['total_output']/($monthly_val),3) ?></td>
	                <td><?php echo number_format($total['yearly']['total_output']/($yearly_val),3) ?></td>
	            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
    
</div>
</div>
</div>