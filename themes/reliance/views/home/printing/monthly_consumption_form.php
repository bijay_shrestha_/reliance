<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
<br>
<?php // echo $date?>
<div class='row'>
    <div class='col-md-12'>
        <div class="box">
        <div class="box-header">
            <h4>Monthly Consumption Report</h4>
        </div>
        <div class="box-body">
            <select option name="month" id="month1" class='form-control'>
                <option value="">-- select month--</option>
                <option value="04">Shrawan</option>
                <option value="05">Bhadra</option>
                <option value="06">Ashoj</option>
                <option value="07">Kartik</option>
                <option value="08">Mangsir</option>
                <option value="09">Poush</option>
                <option value="10">Magha</option>
                <option value="11">Falgun</option>
                <option value="12">Chaitra</option>
                <option value="01">Baishak</option>
                <option value="02">Jesth</option>
                <option value="03">Ashad</option>
                <option value="0">All</option>
            </select>
            <br>
        <div id='month_report1'></div>
    </div>
</div>
<script type="text/javascript">
    $('#month1').change(function(){
        var month = $(this).val();
        var date = '<?php echo $date?>';
        var department_id = <?php echo $department_id?>;
        $.post('<?php echo site_url("home/monthly_consumption_report/".$department_id)?>',{month:month,date:date,department_id:department_id},function(data){
            $('#month_report1').empty();
            $('#month_report1').append(data);
        },'html');
    });
</script>