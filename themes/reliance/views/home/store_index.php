<!-- <pre>
	<?php // print_r($materials);?>
</pre> -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Average Consumption Table</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="daily-average-consumption-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th rowspan="2">Raw Material</th>
                            <th colspan="5">Daily Average Consumption</th>
                            <th rowspan="2">Current Stock</th>
                            <th rowspan="2">Lasting Days</th>
                        </tr>
                        <tr>
                        	<th>Mixing</th>
                        	<th>Printing</th>
                        	<th>Laminating</th>
                        	<th>Rexin</th>
                        	<th>Total</th>
                            <th></th>
                    	</tr>
                    </thead>
                    <tbody>
                        <?php foreach ($materials as $material) { ?>
                            <tr>
                                <td><?php echo $material['material_name'] ?></td>
                                <td><?php echo number_format($material['mixing'],3) ?></td>
                                <td><?php echo number_format($material['printing'],3) ?></td>
                                <td><?php echo number_format($material['lamination'],3) ?></td>
                                <td><?php echo number_format($material['rexin'],3) ?></td>
                                <td><?php echo number_format($material['total'],3) ?></td>
                                <td><?php echo number_format($material['stock'],3) ?></td>
                                <td><?php echo ($material['total']!=0)?round($material['stock']/$material['total']):0 ?></td>
                                <td>
                                <?php if(in_array($material['material_id'],$lc_list)){?>
                                    <button class="btn btn-primary btn-xs" onclick="lcReport(<?php echo $material['material_id']?>)"><i class="fa fa-eye"></i></button>
                                <?php }?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <!-- table for purchase and dispatch records-->
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Purchase/Dispatch Table</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    	<form id='form-purchase-dispatch'>
                    		<div class='row'>
                    			<div class='col-md-5 form-group'>
        		            		<label>
        		            			Material Name
        		            		</label>
        	            		</div>
        	            		<div class="col-md-7 form-group">
        		            		<select name="material_id" class="form-control" id="material-id">
        		        				<option value="">-- Select Option</option>
        		            			<?php foreach($materials as $material){?>
        		            				<option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
        		        				<?php }?>
        		            		</select>
        	            		</div>
                                <div class='col-md-2 form-group'>
                                    <label>
                                        Date
                                    </label>
                                </div>
                                <div class="col-md-5 form-group">
                                    <input type="text" name='start_date' id='start-date' class="form-control" placeholder="Start Date">
                                </div>
                                <div class="col-md-5 form-group">
                                    <input type="text" name='end_date' id='end-date' class="form-control" placeholder="End Date">
                                </div>
                                <div class="col-md-6 form-group">
                                    <input type="radio" name='options' id="purchase" value='purchase' checked>
                                    <label>
                                        Purchase 
                                    </label>
                                    <input type="radio" name='options' id="dispatch" value='dispatch'>
                                    <label>
                                        Dispatch
                                    </label>
                                </div><br>
        	            		<div class="col-md-7"><button type="button" id="purchase-dispatch-btn" class='btn btn-primary'>View Report</button></div>
                    		</div>
                    		<div id="table-purchase-dispatch">
                    		</div>
                    	</form>
                        <div id="purchase-dispatch-table">

                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                
            </div>

            <!-- for all prchase and dispatch -->
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Purchase/Dispatch Table</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <form id='form-all-purchase-dispatch'>
                            <div class='row'>
                                <div class='col-md-2 form-group'>
                                    <label>
                                        Date : 
                                    </label>
                                </div>
                                <div class="col-md-5 form-group">
                                    <input type="text" name='start_date' id='start-date1' class="form-control" placeholder="Start Date">
                                </div>
                                <div class="col-md-5 form-group">
                                    <input type="text" name='end_date' id='end-date1' class="form-control" placeholder="End Date">
                                </div>
                                <br>
                                <div class="col-md-7"><button type="button" id="all-purchase-dispatch-btn" class='btn btn-primary'>View Report</button></div>
                            </div>
                            <div id="table-all-purchase-dispatch">
                            </div>
                        </form>
                        <div id="purchase-dispatch-table">

                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="lc-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" id="report-body">
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<input type='hidden' value="<?php echo date('Y-m-d')?>" id="nepaliDate9">

<script type="text/javascript">
    $(function(){
        $('#nepaliDate9').val(AD2BS($('#nepaliDate9').val()));
        var date = $('#nepaliDate9').val();

		$('#daily-average-consumption-table').dataTable({
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    // customize: function(xlsx) {
                    //     var sheet = xlsx.xl.worksheets['sheet1.xml'];
         
                    //     // Loop over the cells in column `H`
                    //     $('row c[r^="H"]', sheet).each( function () {
                    //         // Get the value and strip the non numeric characters
                    //         if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 <= 15 ) {
                    //             $(this).attr( 's', '35' );
                    //         }
                    //     });
                    // }
                },
                {
                    extend: 'pdfHtml5',
                    
                    customize: function ( doc ) {
                        doc.content.splice( 0, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            image: '<?php echo $this->pdf_logo?>',/*64 bit encoded image in public controller*/
                        })
                        doc.content.splice( 2, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Date : '+date
                        })
                        doc.content.splice( 3, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Report Type : Average Consumption Report'
                        })
                    }
                },
            ],
            // for color
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    if ( aData[7] <= 15 && aData['5'] != 0)
                    {
                        $('td', nRow).css('background-color', '#FF82AB');
                    }
                }
		});
        $('#start-date,#start-date1').nepaliDatePicker();
        $('#end-date,#end-date1').nepaliDatePicker();
	});
</script>
<script type="text/javascript">
    $('#purchase-dispatch-btn').click(function()
        {
            // alert();
            $('#table-purchase-dispatch').empty();
            $.post('<?php echo site_url("home/getDispatchPurchase")?>',$('#form-purchase-dispatch').serializeArray(),function(data){
                $('#table-purchase-dispatch').append(data);
            },'html');
        }
    )
</script>
<script type="text/javascript">
	$('#all-purchase-dispatch-btn').click(function()
		{
			// alert();
			$('#table-all-purchase-dispatch').empty();
			$.post('<?php echo site_url("home/getAllDispatchPurchase")?>',$('#form-all-purchase-dispatch').serializeArray(),function(data){
				$('#table-all-purchase-dispatch').append(data);
			},'html');
		}
	)
</script>
<script type="text/javascript">
    // $('#all').change(function(){
    //         $('#material-id').prop("disabled",true);
    // });
    // $('#purchase,#dispatch').change(function(){
    //         $('#material-id').prop("disabled",false);
    // });
</script>
<script type="text/javascript">
    function lcReport(id) {
        $.post('<?php echo site_url("lc/storeReport")?>',{'id':id},function(data){
            $('#myModalLabel').html('Track Report');
            $('#report-body').html(data);
            $('#lc-dialog').modal()
        },'html');
    }
</script>