<!-- DataTables -->
<?php 
// print_r($last_entry_date);
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
    <div class='row'>
        <div class='col-md-6'>
<div class="box">
    <div class="box-header">
        Working Day Report
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <th>No. of Days</th>
                        <td><?php echo $working_days['monthly_working_day'] ?></td>
                        <td><?php echo $working_days['yearly_working_day'] ?></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
</div>

</div>