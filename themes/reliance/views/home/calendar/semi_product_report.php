<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
<div class="box">
    <div class="box-header">
        Semi Finished Goods
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Dimension</th>
                    <th>Type</th>
                    <th>Brand</th>
                    <th>Last Entry(<?php echo $date?>)</th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($material_properties as $material_property){?>
                <tr>
                    <th><?php echo $material_property['dimension_name'] ?></th>
                    <td><?php echo $material_property['quality_name'] ?></td>
                    <td><?php echo $material_property['brand_name'] ?></td>
                    <td><?php echo number_format($material_property['daily'],3) ?></td>
                    <td><?php echo number_format($material_property['monthly'],3) ?></td>
                    <td><?php echo number_format($material_property['yearly'],3) ?></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</div>