<!-- for date -->
<input type="hidden" value='<?php echo date('Y-m-d')?>' id="nepaliDate92">

<!-- for report status -->
<?php if($this->session->userdata('department_id') != 21 && $this->session->userdata('department_id') != 31){ ?>
<div class="box">
	<div class="box-body">
		<div class="row form-group">
			<div class="col-md-1"><label>Date</label></div>
			<div class="col-md-2"><input type="text" class="form-control" id="date-np"></div>
			<div class="col-md-1"><input type="hidden" id="date-en"></div>
			<div class="col-md-1"><label>Report Status</label></div>
			<div class="col-md-1"><input type="checkbox" id="report_status"> Complete</div>
			<div class="col-md-1"><button id="btn-status" class="btn btn-primary"  onclick="get_report_status()">View Status</button></div>
		</div>
	</div>
</div>
<?php }?>

<div id='report'></div>
<script type="text/javascript">
$(function(){
	$('#nepaliDate92').val(AD2BS($('#nepaliDate92').val()));
    var date = $('#nepaliDate92').val();

    $.post('<?php echo site_url("home/getCalendarReport")?>',{date:date},function(data){
    	$('#report').empty();
    	$('#report').append(data);
    },'html');
	$('#date-np').nepaliDatePicker({
        ndpEnglishInput: 'date-en'
    });
})
</script>

<script type="text/javascript">
	$('#report_status').click(function(){
		var status = $(this).is(':checked');
		var date_np = $('#date-np').val();
		var date_en = $('#date-en').val();
		if(date_np){
			$.post("<?php echo site_url()?>report_status/save",{status:status,date_np:date_np,date_en:date_en},function(data){
				if(data.success){
					alert('Report status is changed')
				}else{
					alert('Please try again')
				}
			},'json')
		}else{
			alert('Select Date');
			$(this).prop('checked',false);
		}
	});
</script>

<script type="text/javascript">
	function get_report_status(){
		$('#report_status').prop('checked',false);
		var date_np = $('#date-np').val();
		if(date_np){
			$.post("<?php echo site_url()?>report_status/get_status",{date_np:date_np},function(data){
				if(data.rows > 0){
					$('#report_status').prop('checked',(data.value[0]['status'])?true:false);
				}
			},'json');
		}else{
			alert('Select Date');
			$('#report_status').prop('checked',false);
		}
	}
</script>