<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
<div class="box">
    <div class="box-header">
        Input Output Analysis Report
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Last Entry(<?php echo $date?>)</th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Length</th>
                    <td><?php echo number_format($daily['total_length'],3) ?></td>
                    <td><?php echo number_format($monthly['total_length'],3) ?></td>
                    <td><?php echo number_format($yearly['total_length'],3) ?></td>
                </tr>
                <tr><td></td><td></td><td></td></tr>
                <tr>
                    <th>Mixing Total</th>
                    <td><?php echo number_format($daily['mixing_tot'],3) ?></td>
                    <td><?php echo number_format($monthly['mixing_tot'],3) ?></td>
                    <td><?php echo number_format($yearly['mixing_tot'],3) ?></td>
                </tr>
                <tr>
                    <th>Net Weight</th>
                    <td><?php echo number_format($daily['net_weight'],3) ?></td>
                    <td><?php echo number_format($monthly['net_weight'],3) ?></td>
                    <td><?php echo number_format($yearly['net_weight'],3) ?></td>
                </tr>
                <tr>
                    <th>Resuable</th>
                    <td><?php echo number_format($daily['Resuable']['scrap_record'],3) ?></td>
                    <td><?php echo number_format($monthly['Resuable']['scrap_record'],3) ?></td>
                    <td><?php echo number_format($yearly['Resuable']['scrap_record'],3) ?></td>
                </tr>
                <tr>
                    <th>Lamps and Plate</th>
                    <td><?php echo number_format($daily['Lamps  and Plate']['scrap_record'],3) ?></td>
                    <td><?php echo number_format($monthly['Lamps  and Plate']['scrap_record'],3) ?></td>
                    <td><?php echo number_format($yearly['Lamps  and Plate']['scrap_record'],3) ?></td>
                </tr>
                <tr>
                    <th>Unaccounted Loss</th>
                    <td><?php echo number_format($daily['mixing_tot']-$daily['net_weight']-$daily['Resuable']['scrap_record']-$daily['Lamps  and Plate']['scrap_record'],3) ?></td>
                    <td><?php echo number_format($monthly['mixing_tot']-$monthly['net_weight']-$monthly['Resuable']['scrap_record']-$monthly['Lamps  and Plate']['scrap_record'],3) ?></td>
                    <td><?php echo number_format($yearly['mixing_tot']-$yearly['net_weight']-$yearly['Resuable']['scrap_record']-$yearly['Lamps  and Plate']['scrap_record'],3) ?></td>
                </tr>
                <tr>
                    <th>Unaccounted Loss %</th>
                    <td><?php echo number_format(($daily['mixing_tot']-$daily['net_weight']-$daily['Resuable']['scrap_record']-$daily['Lamps  and Plate']['scrap_record'])*100/$daily['mixing_tot'],4) ?>%</td>
                    <td><?php echo number_format(($monthly['mixing_tot']-$monthly['net_weight']-$monthly['Resuable']['scrap_record']-$monthly['Lamps  and Plate']['scrap_record'])*100/$monthly['mixing_tot'],4) ?>%</td>
                    <td><?php echo number_format(($yearly['mixing_tot']-$yearly['net_weight']-$yearly['Resuable']['scrap_record']-$yearly['Lamps  and Plate']['scrap_record'])*100/$yearly['mixing_tot'],4) ?>%</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
    
</div>
</div>
</div>