<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
<?php       
$daily_total=0;
$monthly_total=0;
$yearly_total=0;
foreach($reasons as $index => $res){
    $daily_total += $daily_loss[$index]['total']['total'];
    $monthly_total += $monthly_loss[$index]['total']['total'];
    $yearly_total += $yearly_loss[$index]['total']['total'];
}
$daily_val = (24*60 - ($daily_total))/60;
$monthly_val = (24*60*$working_days_for_timeloss['monthly_working_day'] - ($monthly_total))/60;
$yearly_val = (24*60*$working_days_for_timeloss['yearly_working_day'] - ($yearly_total))/60;
?>
<link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
<br>
<div class="box">
    <div class="box-header">
        Production Shift Summary
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Last Entry(<?php echo $date?>)</th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Base UT</th>
                    <td><?php echo number_format($daily_report['total_base_ut'],3) ?></td>
                    <td><?php echo number_format($monthly_report['total_base_ut'],3) ?></td>
                    <td><?php echo number_format($yearly_report['total_base_ut'],3) ?></td>
                </tr>
                <tr>
                    <th>Base MT</th>
                    <td><?php echo number_format($daily_report['total_base_mt'],3) ?></td>
                    <td><?php echo number_format($monthly_report['total_base_mt'],3) ?></td>
                    <td><?php echo number_format($yearly_report['total_base_mt'],3) ?></td>
                </tr>
                <tr>
                    <th>Base OT</th>
                    <td><?php echo number_format($daily_report['total_base_ot'],3) ?></td>
                    <td><?php echo number_format($monthly_report['total_base_ot'],3) ?></td>
                    <td><?php echo number_format($yearly_report['total_base_ot'],3) ?></td>
                </tr>
                <tr>
                    <th>Print Film</th>
                    <td><?php echo number_format($daily_report['total_print_film'],3) ?></td>
                    <td><?php echo number_format($monthly_report['total_print_film'],3) ?></td>
                    <td><?php echo number_format($yearly_report['total_print_film'],3) ?></td>
                </tr>
                <tr>
                    <th>CT</th>
                    <td><?php echo number_format($daily_report['total_ct'],3) ?></td>
                    <td><?php echo number_format($monthly_report['total_ct'],3) ?></td>
                    <td><?php echo number_format($yearly_report['total_ct'],3) ?></td>
                </tr>
                <tr>
                    <th>Dull CT</th>
                    <td><?php echo number_format($daily_report['total_dull_ct'],3) ?></td>
                    <td><?php echo number_format($monthly_report['total_dull_ct'],3) ?></td>
                    <td><?php echo number_format($yearly_report['total_dull_ct'],3) ?></td>
                </tr>
                <tr>
                    <th>Output</th>
                    <td><?php echo number_format($daily_report['total_output'],3) ?></td>
                    <td><?php echo number_format($monthly_report['total_output'],3) ?></td>
                    <td><?php echo number_format($yearly_report['total_output'],3) ?></td>
                </tr>
                <tr>
                    <th>Per Hour Output</th>
                    <td><?php echo number_format($daily_report['total_output']/24,3) ?></td>
                    <td><?php echo number_format($monthly_report['total_output']/(24*$working_days_for_timeloss['monthly_working_day']),3) ?></td>
                    <td><?php echo number_format($yearly_report['total_output']/(24*$working_days_for_timeloss['yearly_working_day']),3) ?></td>
                </tr>
                <tr>
                    <th>Per Working Hour Output</th>
                    <td><?php echo number_format($daily_report['total_output']/($daily_val),3) ?></td>
                    <td><?php echo number_format($monthly_report['total_output']/($monthly_val),3) ?></td>
                    <td><?php echo number_format($yearly_report['total_output']/$yearly_val,3) ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="box">
    <div class="box-header">
        Scrap %
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Last Entry(<?php echo $date?>)</th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Base UT</th>
                    <td><?php echo number_format(($daily_report['total_base_ut']>0)?($total['daily_total']['base_ut']/$daily_report['total_base_ut'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($monthly_report['total_base_ut']>0)?($total['monthly_total']['base_ut']/$monthly_report['total_base_ut'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($yearly_report['total_base_ut']>0)?($total['yearly_total']['base_ut']/$yearly_report['total_base_ut'])*100:0,2) ?>%</td>
                </tr>
                <tr>
                    <th>Base MT</th>
                    <td><?php echo number_format(($daily_report['total_base_mt']>0)?($total['daily_total']['base_mt']/$daily_report['total_base_mt'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($monthly_report['total_base_mt']>0)?($total['monthly_total']['base_mt']/$monthly_report['total_base_mt'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($yearly_report['total_base_mt']>0)?($total['yearly_total']['base_mt']/$yearly_report['total_base_mt'])*100:0,2) ?>%</td>
                </tr>
                <tr>
                    <th>Base OT</th>
                    <td><?php echo number_format(($daily_report['total_base_ot']>0)?($total['daily_total']['base_ot']/$daily_report['total_base_ot'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($monthly_report['total_base_ot']>0)?($total['monthly_total']['base_ot']/$monthly_report['total_base_ot'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($yearly_report['total_base_ot']>0)?($total['yearly_total']['base_ot']/$yearly_report['total_base_ot'])*100:0,2) ?>%</td>
                </tr>
                <tr>
                    <th>Print Film</th>
                    <td><?php echo number_format(($daily_report['total_print_film']>0)?($total['daily_total']['print_film']/$daily_report['total_print_film'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($monthly_report['total_print_film']>0)?($total['monthly_total']['print_film']/$monthly_report['total_print_film'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($yearly_report['total_print_film']>0)?($total['yearly_total']['print_film']/$yearly_report['total_print_film'])*100:0,2) ?>%</td>
                </tr>
                <tr>
                    <th>CT</th>
                    <td><?php echo number_format(($daily_report['total_ct']>0)?($total['daily_total']['ct']/$daily_report['total_ct'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($monthly_report['total_ct']>0)?($total['monthly_total']['ct']/$monthly_report['total_ct'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($yearly_report['total_ct']>0)?($total['yearly_total']['ct']/$yearly_report['total_ct'])*100:0,2) ?>%</td>
                </tr>
                <tr>
                    <th>Dull CT</th>
                    <td><?php echo number_format(($daily_report['total_dull_ct']>0)?($total['daily_total']['dull_ct']/$daily_report['total_dull_ct'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($monthly_report['total_dull_ct']>0)?($total['monthly_total']['dull_ct']/$monthly_report['total_dull_ct'])*100:0,2) ?>%</td>
                    <td><?php echo number_format(($yearly_report['total_dull_ct']>0)?($total['yearly_total']['dull_ct']/$yearly_report['total_dull_ct'])*100:0,2) ?>%</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>