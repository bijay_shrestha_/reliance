<link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
<table id="daily-dispatch-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Brand</th>
            <th>Output</th>
            <th>Target</th>
            <th>Difference</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($targets as $target){?>
            <tr>
                <td><?php echo $target['brand_name'] ?></td>
                <?php 
                // $total_net_weight = 0;
                // $total_length = 0;
                // foreach($reports[$target['id']] as $report){
                //     $total_net_weight += $report['net_weight'];
                //     $total_length += $report['total_length'];
                // }
                // $output = ($total_length != 0)?($total_net_weight/$total_length):0;
                ?>
                <td><?php echo number_format($target['output'],3) ?></td>
                <td><?php echo number_format($target['target'],3) ?></td>
                <?php $difference = $target['output'] - $target['target'];?>
                <td style="background :<?php echo ($difference < 0 && $target['output'] != 0)?'#f2dede':'#dff0d8'?>" ><?php echo ($target['output'] != 0)?number_format($difference,3):'N/A' ?></td>
            </tr>
        <?php }?>
    </tbody>
</table>