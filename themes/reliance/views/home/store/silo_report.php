<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4>Silo Report</h4>
            </div>
            <div class="box-body">
                <table id="silo_report" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Opening Quantity</th>
                            <th>Production</th>
                            <th>Packaged</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rows as $key => $value) {?>
                        <tr>
                            <td><?php echo $value['type_name']?></td>
                            <td><?php echo number_format($value['opening'], 2)?></td>
                            <td><?php echo number_format($value['production'], 2)?></td>
                            <td><?php echo number_format($value['sales'], 2)?></td>
                            <td><?php echo number_format($value['balance'], 2)?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
        $('#silo_report').DataTable({
            // "order":[[0, "desc"]],
            paging:false,
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            
        });

        function numberWithCommas(n) {
            var parts=n.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }
</script>