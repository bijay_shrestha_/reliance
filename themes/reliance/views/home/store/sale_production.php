<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->

<!-- for brand type -->
<!-- production -->
<div class="row">
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <h4>Type Wise Production Report</h4>
        </div>
        <div class="box-body">
            <table id="brand_type_list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Daily(<?php echo $date?>)</th>
                        <th>Monthly(<?php echo $month?>)</th>
                        <th>Yearly(<?php echo $year?>)</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($brand_type as $key => $value) {?>
                    <tr>
                        <td><?php echo $value['brand_type']?></td>
                        <td><?php echo number_format($value['total_production'], 2)?></td>
                        <td><?php echo number_format($value['monthly_total_production'], 2)?></td>
                        <td><?php echo number_format($value['yearly_total_production'], 2)?></td>
                    </tr>
                <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<!-- sales -->
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <h4>Type Wise Sales Report</h4>
        </div>
        <div class="box-body">
            <table id="brand_type_list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Daily(<?php echo $date?>)</th>
                        <th>Monthly(<?php echo $month?>)</th>
                        <th>Yearly(<?php echo $year?>)</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($brand_type as $key => $value) {?>
                    <tr>
                        <td><?php echo $value['brand_type']?></td>
                        <td><?php echo number_format($value['total_sales'], 2)?></td>
                        <td><?php echo number_format($value['monthly_total_sales'], 2)?></td>
                        <td><?php echo number_format($value['yearly_total_sales'], 2)?></td>
                    </tr>
                <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- brand -->
<!-- production -->
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <h4>Brand Wise Production Report</h4>
        </div>
        <div class="box-body">
            <table id="brand_list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Daily(<?php echo $date?>)</th>
                        <th>Monthly(<?php echo $month?>)</th>
                        <th>Yearly(<?php echo $year?>)</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($brand as $key => $value) {?>
                    <tr>
                        <td><?php echo $value['brand_name']?></td>
                        <td><?php echo number_format($value['total_production'],2)?></td>
                        <td><?php echo number_format($value['monthly_total_production'],2)?></td>
                        <td><?php echo number_format($value['yearly_total_production'],2)?></td>
                    </tr>
                <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- sales -->
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header">
            <h4>Brand Wise Sales Report</h4>
        </div>
        <div class="box-body">
            <table id="brand_list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Daily(<?php echo $date?>)</th>
                        <th>Monthly(<?php echo $month?>)</th>
                        <th>Yearly(<?php echo $year?>)</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($brand as $key => $value) {?>
                    <tr>
                        <td><?php echo $value['brand_name']?></td>
                        <td><?php echo number_format($value['total_sales'],2)?></td>
                        <td><?php echo number_format($value['monthly_total_sales'],2)?></td>
                        <td><?php echo number_format($value['yearly_total_sales'],2)?></td>
                    </tr>
                <?php }?>
                </tbody><tfoot>
                    <tr>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header">
            <h4>Sales/Production Summary</h4>
        </div>
        <div class="box-body">
            <table id="sales_production_list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Opening</th>
                        <th>Production</th>
                        <th>Sales</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($opening_stock as $key => $value) {?>
                    <tr>
                        <td><?php echo $value['brand_type']?></td>
                        <td><?php echo number_format($value['opening'], 2)?></td>
                        <td><?php echo number_format($value['production'], 2)?></td>
                        <td><?php echo number_format($value['sales'], 2)?></td>
                        <td><?php echo number_format($value['opening'] + $value['production'] - $value['sales'], 2)?></td>
                    </tr>
                <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(function(){
        $('#brand_list,#brand_type_list').DataTable({
            // "order":[[0, "desc"]],
            paging:false,
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                for(i = 1; i < 4; i++){
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                     // Total over all pages
                    total = api
                        .column( i )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    // Update footer
                    $( api.column( i ).footer() ).html(
                        // total.toLocaleString(undefined, {maximumFractionDigits:2})
                         numberWithCommas(total.toFixed(2))
                    );
                }
            }
        });

        $('#sales_production_list').DataTable({
            // "order":[[0, "desc"]],
            paging:false,
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                for(i = 1; i < 5; i++){
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                     // Total over all pages
                    total = api
                        .column( i )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    // Update footer
                    $( api.column( i ).footer() ).html(
                        // total.toLocaleString(undefined, {maximumFractionDigits:2})
                        numberWithCommas(total.toFixed(2))
                    );
                }
            }
        });

        function numberWithCommas(n) {
            var parts=n.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }
    });
</script>