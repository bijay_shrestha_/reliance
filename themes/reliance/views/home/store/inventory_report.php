<div class="row">
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->

<!-- for brand type -->
<!-- production -->
<?php foreach ($categories as $index => $val) {
    $diesel_exist = 0;
    ?>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h4><?php echo ucwords($val['category_name'])?> Stock Report</h4>
            </div>
            <div class="box-body">
                <table id="list" class="table table-bordered table-striped inventory_table">
                    <thead>
                        <tr>
                            <th>Material</th>
                            <th>Opening</th>
                            <th>Purchase</th>
                            <th>Issue</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rows[$val['category_id']] as $key => $value) {?>
                        <tr>
                            <td <?php if(strtoupper($value['material_name']) == 'DIESEL'){
                                    $diesel_exist = 1;
                                    echo 'class="details-control"';
                                }?>>
                                <?php echo $value['material_name'];?></td>
                            <?php 
                                $opening = (array_key_exists('opening_purchase', $value))?$value['opening_purchase']:0;
                                if(array_key_exists('opening_dispatch', $value)){
                                    $opening = $opening - $value['opening_dispatch'];
                                } 
                            ?>
                            <td><?php echo number_format($opening, 2)?></td>
                            <td><?php echo number_format((array_key_exists('purchase', $value))?$value['purchase']:0 , 2)?></td>
                            <td><?php echo number_format((array_key_exists('dispatch', $value))?$value['dispatch']:0 , 2)?></td>
                            <?php $balance = $opening +((array_key_exists('purchase', $value))?$value['purchase']:0) - ((array_key_exists('dispatch', $value))?$value['dispatch']:0)?>
                            <td><?php echo number_format($balance , 2)?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
                <?php if($diesel_exist == 1 && count($diesel_consumption) > 0){?>
                <h4>Diesel Consumption</h4>
                    <table id="tbl_diesel" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Departments</th>
                                <th>Consumption</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($diesel_consumption as $dept){?>
                            <tr>
                                <td><?php echo $dept['name']?></td>
                                <td><?php echo $dept['consumption']?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                <?php }?>
            </div>
        </div>
    </div>
<?php }?>
</div>
<script type="text/javascript">


    $('#tbl_diesel').dataTable({
        paging:false,
    });
    var table = $('.inventory_table').DataTable({
            // "order":[[0, "desc"]],
            paging:false,
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                for(i = 1; i < 5; i++){
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                     // Total over all pages
                    total = api
                        .column( i )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    // Update footer
                    $( api.column( i ).footer() ).html(
                        // total.toFixed(2)
                         numberWithCommas(total.toFixed(2))
                    );
                }
            }
        });

        function numberWithCommas(n) {
            var parts=n.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }
   
</script>