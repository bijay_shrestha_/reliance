<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->

<!-- for brand type -->
<!-- production -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4>Electric Report</h4>
            </div>
            <div class="box-body">
                <table id="electric_report" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Shift</th>
                                <th>Power Cut</th>
                                <th>Breakdown</th>
                                <th>Cement Mill Operation (NEA)</th>
                                <th>Cement Mill Operation (DG)</th>
                                <th>Cement Mill Operation (DG)</th>
                                <th>Only Lightining NEA</th>
                                <th>Reason</th>
                                <th>Total Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rows as $key => $value) {?>
                        <tr>
                            <td><?php echo $value['shift']?></td>
                                <td><?php echo $value['power_cut']?></td>
                                <td><?php echo $value['cement_mill_operation_nea']?></td>
                                <td><?php echo $value['cement_mill_operation_dg']?></td>
                                <td><?php echo $value['only_lighiting_nea']?></td>
                                <td><?php echo $value['breakdown']?></td>
                                <td><?php echo $value['only_lighiting_nea']?></td>
                                <td><?php echo $value['reason']?></td>
                                <td><?php echo $value['total']?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                    <!-- <tfoot>
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot> -->
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function numberWithCommas(n) {
            var parts=n.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }
</script>
<script type="text/javascript">
    $('#electric_report').DataTable({
            // "order":[[0, "desc"]],
            paging:false,
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
            
        });
</script>