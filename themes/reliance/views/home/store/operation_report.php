<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->

<!-- for brand type -->
<!-- production -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h4>Operational Report(Plant Running Hrs.)</h4>
            </div>
            <div class="box-body">
                <table id="brand_type_list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Plant</th>
                            <th>Start Hour </th>
                            <th>Finish Hour</th>
                            <th>Today's Running Hour</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rows as $key => $value) {?>
                        <tr>
                            <td><?php echo $value['name']?></td>
                            <td><?php echo number_format($value['start_hour'], 2)?></td>
                            <td><?php echo number_format($value['finish_hour'], 2)?></td>
                            <td><?php echo number_format($value['finish_hour'] - $value['start_hour'], 2)?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function numberWithCommas(n) {
            var parts=n.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }
</script>