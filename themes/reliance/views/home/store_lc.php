<div class="row">
	<div class="col-md-2">Ordered Quantity:</div>
	<div class="col-md-10"><?php echo number_format($lc['quantity'],3)?></div>

	<?php 
	if(count($lc_track) > 0){
	?>

		<div class="col-md-2">ETA Kolkata:</div>
		<div class="col-md-4"><?php echo $lc_track['eta_kolkata']?></div>
		<div class="col-md-2">Reached Kolkata:</div>
		<div class="col-md-4"><?php echo ($lc_track['reached_kolkata'])?'Yes':'No'?></div>

		<div class="col-md-2">Reached Nepal:</div>
		<div class="col-md-10"><?php echo ($lc_track['reached_nepal'])?'Yes':'No'?></div>
		
		<div class="col-md-2">Reached Factory:</div>
		<div class="col-md-2"><?php echo ($lc_track['reached_factory'])?'Yes':'No'?></div>
		<div class="col-md-2">Date Nepali:</div>
		<div class="col-md-2"><?php echo $lc_track['date_np']?></div>
		<div class="col-md-2">Date English:</div>
		<div class="col-md-2"><?php echo $lc_track['date_en']?></div>

		<div class="col-md-2">current_status:</div>
		<div class="col-md-10"><?php echo $lc_track['current_status']?></div>
	
	<?php }else{?>
		<div class="col-md-12">No Tracking Report</div>
	<?php }?>

</div>