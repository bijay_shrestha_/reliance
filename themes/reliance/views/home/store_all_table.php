<!-- DataTables -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
<div>
<table id="all-purchase-dispatch" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Material</th>
            <th>Opning</th>
            <th>Purchase</th>
            <th>Dispatch</th>
            <th>Closing</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materials as $material) { ?>
            <tr>
                <td><?php echo $material['material_name'] ?></td>
                <td><?php echo number_format(($material['opening'])?$material['opening']:0,3)?></td>
                <td><?php echo number_format(($material['purchases'])?$material['purchases']:0,3) ?></td>
                <td><?php echo number_format(($material['dispatches'])?$material['dispatches']:0,3) ?></td>
                <td><?php echo number_format($material['opening']+$material['purchases']-$material['dispatches'],3) ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
</div>

<!-- DataTables -->
      <script src="<?php echo theme_url()?>assets/js/datatables/jquery.dataTables.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.min.js"></script>
<input type='hidden' value="<?php echo date('Y-m-d')?>" id="nepaliDate91">
<script type="text/javascript">
    $('#nepaliDate91').val(AD2BS($('#nepaliDate91').val()));
        var date = $('#nepaliDate91').val();
        $('#all-purchase-dispatch').dataTable({
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    // customize: function(xlsx) {
                    //     var sheet = xlsx.xl.worksheets['sheet1.xml'];
         
                    //     // Loop over the cells in column `H`
                    //     $('row c[r^="H"]', sheet).each( function () {
                    //         // Get the value and strip the non numeric characters
                    //         if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 <= 15 ) {
                    //             $(this).attr( 's', '35' );
                    //         }
                    //     });
                    // }
                },
                {
                    extend: 'pdfHtml5',
                    
                    customize: function ( doc ) {
                        doc.content.splice( 0, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            image: '<?php echo $this->pdf_logo?>',/*64 bit encoded image in public controller*/
                        })
                        doc.content.splice( 2, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Date : '+date
                        })
                        doc.content.splice( 3, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Report Type : Purchase Dispatch Report'
                        })
                    }
                },
            ],
        });
</script>