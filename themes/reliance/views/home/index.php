<!-- for date -->
<div class="row form-group">
	<form method="post" action="<?php echo site_url("home/pdf")?>">
		<div class="col-md-6">
			<input type="text" name="date_np" value='<?php echo $date?>' class="form-control" id="nepaliDate92">
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<input type="submit" id="pdf" class="dt-button buttons-excel buttons-html5 pull-right" value="PDF">
			</div>
		</div>
	</form>
</div>

<div id='report'></div>

<?php
if($this->session->userdata('department_id') == 7 || $dep == 3){
	$index = 'getStoreReport';
}
?>

<script type="text/javascript">
$(function(){
	$('#nepaliDate92').nepaliDatePicker({
		onChange: function(){
			date = $('#nepaliDate92').val();
	    	<?php if(!isset($month)){?>
		    $.post('<?php echo site_url("home/".$index)?>',{date:date},function(data){
		    	$('#report').empty();
		    	$('#report').append(data);
			    },'html');
		    <?php }else{?>
		    	$.post('<?php echo site_url("home/".$index)?>',{date:date,month:<?php echo $month?>},function(data){
			    	$('#report').empty();
			    	$('#report').append(data);
			    },'html');
			<?php }?>
		}
	});
	$('#nepaliDate92').val(AD2BS($('#nepaliDate92').val()));
    var date = $('#nepaliDate92').val();

    <?php if(!isset($month)){?>
	    $.post('<?php echo site_url("home/".$index)?>',{date:date},function(data){
	    	$('#report').empty();
	    	$('#report').append(data);
	    },'html');
    <?php }else{?>
    	$.post('<?php echo site_url("home/".$index)?>',{date:date,month:<?php echo $month?>},function(data){
	    	$('#report').empty();
	    	$('#report').append(data);
	    },'html');
	<?php }?>
	$('#date-np').nepaliDatePicker({
        ndpEnglishInput: 'date-en'
    });
})
</script>