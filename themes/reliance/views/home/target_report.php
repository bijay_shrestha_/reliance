<link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
<table id="daily-dispatch-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <?php if($department_id == 2){?>
                <th>Dimension</th>
                <th>Type</th>
                <th>Brand</th>
                <th>Output</th>
                <th>Target</th>
                <th>Difference</th>
            <?php }else if($department_id == 3){?>
                <th>Dimension</th>
                <th>Input</th>
                <th>Output</th>
                <th>Ratio</th>
                <th>Target</th>
                <th>Difference</th>
            <?php }else if($department_id == 5){?>
                <th>Brand</th>
                <th>Production</th>
                <th>Net Weight</th>
                <th>Weight Ratio</th>
                <th>Target Ratio</th>
                <th>Difference</th>
            <?php }?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($targets as $target){?>
            <?php if($department_id == 2){?>
                <tr>
                    <td><?php echo $target['dimension_name']?></td>
                    <td><?php echo $target['quality_name'] ?></td>
                    <td><?php echo $target['brand_name'] ?></td>
                    <?php 
                    $total_net_weight = 0;
                    $total_length = 0;
                    foreach($reports[$target['target_id']] as $report){
                        $total_net_weight += $report['net_weight'];
                        $total_length += $report['total_length'];
                    }
                    $output = ($total_length != 0)?($total_net_weight/$total_length):0;
                    ?>
                    <td><?php echo number_format($output,3) ?></td>
                    <td><?php echo number_format($target['target'],3) ?></td>
                    <?php $difference = $output - $target['target'];?>
                    <td style="background :<?php echo ($difference >= 0)?'#f2dede':'#dff0d8'?>" ><?php echo ($output != 0)?number_format($difference,3):'N/A' ?></td>
                </tr>
            <?php }else if($department_id == 3){?>
                <tr>
                    <td><?php echo $target['dimension_name']?></td>
                    <?php 
                    $total_input = 0;
                    $total_output = 0;
                    foreach($reports[$target['id']] as $report){
                        $total_input += $report['total_input'];
                        $total_output += $report['total_output'];
                    }
                    $ratio = ($total_output != 0)?$total_output/$total_input:0;
                    ?>
                    <td><?php echo number_format($total_input,3) ?></td>
                    <td><?php echo number_format($total_output,3) ?></td>
                    <td><?php echo number_format($ratio,3) ?></td>
                    <td><?php echo number_format($target['target'],3) ?></td>
                    <?php $difference = $ratio - $target['target'];?>
                    <td style="background :<?php echo ($difference >= 0)?'#f2dede':'#dff0d8'?>" ><?php echo ($total_output != 0)?number_format($difference,3):'N/A' ?></td>
                </tr>
            <?php }else if($department_id == 5){?>
                <tr>
                    <td><?php echo $target['brand_name']?></td>
                    <?php 
                    $total_production = 0;
                    $total_net_wt = 0;
                    if(array_key_exists($target['id'], $reports)){
                        foreach($reports[$target['id']] as $report){
                            $total_production += $report['total_production'];
                            $total_net_wt += $report['total_net_wt'];
                        }
                    }
                    $ratio = ($total_net_wt != 0)?$total_production/$total_net_wt:0;
                    ?>
                    <td><?php echo number_format($total_production,3) ?></td>
                    <td><?php echo number_format($total_net_wt,3) ?></td>
                    <td><?php echo number_format($ratio,3) ?></td>
                    <td><?php echo number_format($target['target'],3) ?></td>
                    <?php $difference = $ratio - $target['target'];?>
                    <td style="background :<?php echo ($difference >= 0)?'#f2dede':'#dff0d8'?>" ><?php echo ($total_net_wt != 0)?number_format($difference,3):'N/A' ?></td>
                </tr>
            <?php }?>
        <?php }?>
    </tbody>
</table>