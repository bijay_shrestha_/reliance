<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>

<?php       
$daily_total=0;
$monthly_total=0;
$yearly_total=0;
foreach($reasons as $index => $res){
    $daily_total += $daily_loss[$index]['total']['total'];
    $monthly_total += $monthly_loss[$index]['total']['total'];
    $yearly_total += $yearly_loss[$index]['total']['total'];
}
?>

<!-- for losshour calculation -->
<div class="box">
    <div class="box-header">
    	<h4>Output-Hours Calculations</h4>
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Last Entry Date(<?php echo $date?>)</th>
                    <th>This Month(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>This Year(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <th>Per Hour Output</th>
                <td><?php echo number_format($daily['net_weight']/24,3) ?></td>
                <td><?php echo number_format($monthly['net_weight']/(24*$working_days_for_timeloss['monthly_working_day']),3) ?></td>
                <td><?php echo number_format($yearly['net_weight']/(24*$working_days_for_timeloss['yearly_working_day']),3) ?></td>
            </tr>
            <tr>
            	<?php 
            		$daily_val = (24*60 - ($daily_total))/60;
            		$monthly_val = (24*60*$working_days_for_timeloss['monthly_working_day'] - ($monthly_total))/60;
            		$yearly_val = (24*60*$working_days_for_timeloss['yearly_working_day'] - ($yearly_total))/60;
                ?>
                <th>Per Work Hour Output</th>
                <td><?php echo number_format($daily['net_weight']/$daily_val,3) ?></td>
                <td><?php echo number_format($monthly['net_weight']/($monthly_val),3) ?></td>
                <td><?php echo number_format($yearly['net_weight']/($yearly_val),3) ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>