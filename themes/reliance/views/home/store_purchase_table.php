<!-- DataTables -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
<br>
<table id="daily-purchase-table" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Date</th>
            <th>Amount</th>
            <th>Dealers</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($purchases as $purchase) { ?>
            <tr>
                <td><?php echo $purchase['date_np'] ?></td>
                <td><?php echo number_format($purchase['added_qty'],3) ?></td>
                <td><?php echo $purchase['dealer_name'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <th>Total</th>
        <th></th>
        <th></th>
    </tfoot>
</table>

<!-- DataTables -->
      <script src="<?php echo theme_url()?>assets/js/datatables/jquery.dataTables.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.min.js"></script>

<input type='hidden' value="<?php echo date('Y-m-d')?>" id="nepaliDate92">
<script type="text/javascript">
$('#nepaliDate92').val(AD2BS($('#nepaliDate92').val()));
        var date = $('#nepaliDate92').val();
        $('#daily-purchase-table').dataTable({
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    // customize: function(xlsx) {
                    //     var sheet = xlsx.xl.worksheets['sheet1.xml'];
         
                    //     // Loop over the cells in column `H`
                    //     $('row c[r^="H"]', sheet).each( function () {
                    //         // Get the value and strip the non numeric characters
                    //         if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 <= 15 ) {
                    //             $(this).attr( 's', '35' );
                    //         }
                    //     });
                    // }
                },
                {
                    extend: 'pdfHtml5',
                    
                    customize: function ( doc ) {
                        doc.content.splice( 0, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'center',
                            image: '<?php echo $this->pdf_logo?>',/*64 bit encoded image in public controller*/
                        })
                        doc.content.splice( 2, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Date : '+date
                        })
                        doc.content.splice( 3, 0, {
                            margin: [ 0, 0, 0, 12 ],
                            alignment: 'left',
                            text: 'Report Type : Average Consumption Report'
                        })
                    }
                },
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                pageTotal = api
                    .column( 1, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 1 ).footer() ).html(
                    numberWithCommas(pageTotal.toFixed(3)) + ' (' + numberWithCommas(total.toFixed(3)) + ')'
                );
            },
        });
        function numberWithCommas(n) {
            var parts=n.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }
</script>