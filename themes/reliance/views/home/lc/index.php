    <?php //echo $date?>
    <div class="row margin">
<form action="<?php echo site_url()?>">
        <div class='col-md-4'>
            <select id='month' name="month" class="form-control">
                <option value="00">All Month</option>
                <option value="04">Shrawn</option>
                <option value="05">Bhadra</option>
                <option value="06">Ashwin</option>
                <option value="07">Kartik</option>
                <option value="08">Mangshir</option>
                <option value="09">Poush</option>
                <option value="10">Magh</option>
                <option value="11">Falgun</option>
                <option value="12">Chaitra</option>
                <option value="01">Baishak</option>
                <option value="02">Jesth</option>
                <option value="03">Ashad</option>
            </select>
        </div>
        <div class="col-md-1"><button class="btn btn-primary"><i class="fa fa-search"></i></button></div>
        <div class="col-md-4 pull-right">
</form>
<a href="<?php echo site_url('home/pdfLc/'.$date.'/'.$month)?>" class="pull-right dt-button buttons-excel buttons-html5" id="pdf">PDF</a></div>
    </div>

<div class="box">
    <!-- <div class="box-header">
        <h3 class="box-title">PP No.: <?php echo $row['pp_no']?></h3>
    </div> --><!-- /.box-header -->
    <!-- <button id="add-lc" class="btn btn-primary margin">Add</button> -->
    <div class="box-body">
    
    <div class="col-md-12">
        <table id="table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>PP No.</th>
                    <th>PP Date</th>
                    <th>Material Name</th>
                    <th>Quantity</th>
                    <th>Per Unit Cost</th>
                    <th>Price</th>
                    <th>price NRP</th>
                    <th>Total Price</th>
                    <th>Per KG Cost</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($rows as $i => $row) { ?>
                <?php foreach ($material[$i] as $key => $value) {?>
                    <tr>
                        <td><?php echo $row['pp_no']?></td>
                        <td><?php echo $row['cost_date_np']?></td>
                        <td><?php echo $value['material_name'] ?></td>
                        <td><?php echo number_format($value['quantity'],3) ?></td>
                        <td><?php echo number_format($value['price'],3) ?></td>
                        <td><?php echo number_format($value['quantity'] * $value['price'],3) ?></td>
                        <td><?php echo number_format($value['quantity'] * $value['price'] * $row['currency_exchange_rate'],3) ?></td>
                        <!-- <td><?php //echo number_format($total = $value['quantity']*$value['price']*$row['currency_exchange_rate']+$value['duty']+$value['excise'],3) ?></td> -->
                        <!-- <?php //$total = $row['total_charge']/$row['quantity'] * $value['quantity']?> -->
                        <?php $total = $row['total_charge']/$row['bill_amt'] * $value['quantity'] * $value['price'] * $row['currency_exchange_rate']?>
                        <td><?php echo number_format($total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise'],3) ?></td>
                        <td><?php echo number_format(($total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise']) / $value['quantity'],3) ?></td>
                    </tr>
                <?php }?>
            <?php } ?>
            </tbody>
        </table>
    </div>
        <hr>


    </div><!-- /.box-body -->
</div><!-- /.box -->

<div class="box">
    <!-- <div class="box-header">
        <h3 class="box-title">PP No.: <?php echo $row['pp_no']?></h3>
    </div> --><!-- /.box-header -->
    <!-- <button id="add-lc" class="btn btn-primary margin">Add</button> -->
    <div class="box-body">
    
    <div class="col-md-12">
        <table id="table-avg" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Material Name</th>
                    <th>Total Landed Price</th>
                    <th>Average Per KG Cost</th>
                    <th>Higest Per KG Cost</th>
                    <th>Lowest Per KG Cost</th>
                    <th>Last Per KG Cost</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($report_materials as $key => $value) {?>
                    <tr>
                        <td><?php echo $value['material_name'] ?></td>
                        <td><?php echo number_format($value['landed_cost'],3) ?></td>
                        <td><?php echo number_format($value['avg_cost']/$value['count'],3) ?></td>
                        <td><?php echo number_format($value['highest'],3) ?></td>
                        <td><?php echo number_format($value['lowest'],3) ?></td>
                        <td><?php echo number_format($value['last'],3) ?></td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
        <hr>

        
    </div><!-- /.box-body -->
</div><!-- /.box -->


<script type="text/javascript">
        $('#table').DataTable({
            "order": [
                [ 0, 'desc' ]
            ],
            dom: 'lBfrtip',
            buttons: [
            // 'copyHtml5',
            'excelHtml5',
            // 'csvHtml5',
            // 'pdfHtml5'
            ],
            "columnDefs": [
                {
                    "targets": [ 1 ],
                    "visible": false
                }
            ]
        });
        $('#table-avg').DataTable({
            "order": [
                [ 0, 'asc' ]
            ],
            dom: 'lBfrtip',
            buttons: [
            'excelHtml5',
            ]
        });
</script>