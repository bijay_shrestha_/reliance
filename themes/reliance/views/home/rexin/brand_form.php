<!-- DataTables -->
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
    <?php // echo $date?>
    <div class='row'>
    <div class='col-md-6'>
<div class="box">
    <div class="box-header">
        Monthly Report
    </div>
    <div class="box-body">
    	<select option name="month" id="month" class='form-control'>
    		<option value="">-- select month--</option>
    		<option value="04">Shrawan</option>
    		<option value="05">Bhadra</option>
    		<option value="06">Ashoj</option>
    		<option value="07">Kartik</option>
    		<option value="08">Mangsir</option>
    		<option value="09">Poush</option>
    		<option value="10">Magha</option>
    		<option value="11">Falgun</option>
    		<option value="12">Chaitra</option>
    		<option value="01">Baishak</option>
    		<option value="02">Jesth</option>
    		<option value="03">Ashad</option>
    		<option value="0">All</option>
    	</select>
    	<br>
    	<div id='month_report'></div>
    </div>
</div>
</div>
<div class='col-md-6'>
<div class="box">
    <div class="box-header">
        Monthly Brand Report
    </div>
    <div class="box-body">
    	<div class='row form-group'>
    		<div class="col-md-4">
	    		<label>Brand</label>
	    	</div>
	    	<div class="col-md-8">
		    	<select name="brand" class="form-control" id="brand" onchange='getReport()'>
		    		<option value="">-- select option --</option>
		    		<?php foreach($brands as $brand){?>
		    			<option value="<?php echo $brand['brand_id']?>"><?php echo $brand['brand_name']?></option>
					<?php }?>
		    	</select>
	    	</div>
    	</div>
    	<div class='row form-group'>
    		<div class="col-md-4">
	    		<label>Coat</label>
	    	</div>
	    	<div class="col-md-8">
		    	<select name="coat" class="form-control" id="coat" onchange='getReport()'>
		    		<option value="">-- select option --</option>
		    	</select>
	    	</div>
    	</div>
    	<div class='row form-group'>
    		<div class="col-md-4">
	    		<label>Month</label>
	    	</div>
	    	<div class="col-md-8">
		    	<select option name="month" id="report_month" class='form-control' onchange='getReport()'>
		    		<option value="">-- select month--</option>
		    		<option value="04">Shrawan</option>
		    		<option value="05">Bhadra</option>
		    		<option value="06">Ashoj</option>
		    		<option value="07">Kartik</option>
		    		<option value="08">Mangsir</option>
		    		<option value="09">Poush</option>
		    		<option value="10">Magha</option>
		    		<option value="11">Falgun</option>
		    		<option value="12">Chaitra</option>
		    		<option value="01">Baishak</option>
		    		<option value="02">Jesth</option>
		    		<option value="03">Ashad</option>
		    		<option value="0">All</option>
		    	</select>
    		</div>
    	</div>
    	<div id='brand_report'></div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
	$('#month').change(function(){
		var month = $(this).val();
		var date = '<?php echo $date?>';
		var department_id = 5;
		$.post('<?php echo site_url("home/monthlyReport")?>',{month:month,date:date,department_id:department_id},function(data){
			$('#month_report').empty();
			$('#month_report').append(data);
		},'html');
	});
</script>
<script type="text/javascript">
	$('#brand').change(function(){
		var brand_id = $(this).val();
		$.post('<?php echo site_url("material_property/getMaterialProperties")?>',{selected:brand_id, department_id:5, property:'coating_id', property_table:'COATING', selected_prop:'brand_id'},function(data){
			$('#coat').empty();
			$("#coat").append(
				$('<option>',{
                    value:'',
                    text: '-- Select option --'
                })
            );
			$.each(data,function(index,value){
				$("#coat").append(
					$('<option>',{
	                    value:value['coating_id'],
	                    text: value['coating_name']
	                })
                );
            });
		},'json');
	});
</script>
<script type="text/javascript">
	function getReport(){
		var report_month = $('#report_month').val();
		var date = '<?php echo $date?>';
		var department_id = 5;
		var brand_id = $('#brand').val();
		var coating_id = $('#coat').val();
		if(report_month != '' && brand_id != '' && coating_id != ''){
			$.post('<?php echo site_url("home/monthlyMixingBrandReport")?>',{month:report_month,date:date,department_id:department_id,coating_id:coating_id,brand_id:brand_id},function(data){$('#brand_report').empty();$('#brand_report').append(data);},'html');
		}
	}
</script>