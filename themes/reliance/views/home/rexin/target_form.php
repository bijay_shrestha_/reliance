<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
<br>
<?php // echo $date?>
<div class='row'>
    <div class='col-md-12'>
        <div class="box">
        <div class="box-header">
            Production/Net Weight Ratio
        </div>
        <div class="box-body">
            <select option name="month" id="target-month" class='form-control'>
                <option value="">-- select month--</option>
                <option value="04">Shrawan</option>
                <option value="05">Bhadra</option>
                <option value="06">Ashoj</option>
                <option value="07">Kartik</option>
                <option value="08">Mangsir</option>
                <option value="09">Poush</option>
                <option value="10">Magha</option>
                <option value="11">Falgun</option>
                <option value="12">Chaitra</option>
                <option value="01">Baishak</option>
                <option value="02">Jesth</option>
                <option value="03">Ashad</option>
                <option value="0">All</option>
            </select>
            <br>
        <div id='target_month_report'></div>
    </div>
</div>
</div>
</div>
<div class='row'>
    <div class='col-md-12'>
        <div class="box">
        <div class="box-header">
            Input Output Ratio
        </div>
        <div class="box-body">
            <select option name="month" id="io-target-month" class='form-control'>
                <option value="">-- select month--</option>
                <option value="04">Shrawan</option>
                <option value="05">Bhadra</option>
                <option value="06">Ashoj</option>
                <option value="07">Kartik</option>
                <option value="08">Mangsir</option>
                <option value="09">Poush</option>
                <option value="10">Magha</option>
                <option value="11">Falgun</option>
                <option value="12">Chaitra</option>
                <option value="01">Baishak</option>
                <option value="02">Jesth</option>
                <option value="03">Ashad</option>
                <option value="0">All</option>
            </select>
            <br>
        <div id='io_target_month_report'></div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $('#target-month').change(function(){
        var month = $(this).val();
        var date = '<?php echo $date?>';
        var department_id = 5;
        $.post('<?php echo site_url("home/target_report/".$department_id)?>',{month:month,date:date,department_id:department_id},function(data){
            $('#target_month_report').empty();
            $('#target_month_report').append(data);
        },'html');
    });
</script>
<script type="text/javascript">
    $('#io-target-month').change(function(){
        var month = $(this).val();
        var date = '<?php echo $date?>';
        var department_id = 5;
        $.post('<?php echo site_url("home/io_target_report/".$department_id)?>',{month:month,date:date,department_id:department_id},function(data){
            $('#io_target_month_report').empty();
            $('#io_target_month_report').append(data);
        },'html');
    });
</script>