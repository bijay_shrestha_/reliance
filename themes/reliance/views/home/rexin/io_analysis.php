<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$total['daily']['date_np']);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
<?php       
$daily_total=0;
$monthly_total=0;
$yearly_total=0;
foreach($reasons as $index => $res){
    $daily_total += $daily_loss[$index]['total']['total'];
    $monthly_total += $monthly_loss[$index]['total']['total'];
    $yearly_total += $yearly_loss[$index]['total']['total'];
}
?>
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
<div class="box">
    <div class="box-header">
        Input Output Analysis Report
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Last Entry(<?php echo $total['daily']['date_np']?>)</th>
                    <th>Monthly(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>Yearly(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Total Production (m)</th>
                    <td><?php echo number_format($total['daily']['total_production'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_production'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_production'],3) ?></td>
                </tr>
                <tr><td></td><td></td><td></td></tr>
                <tr>
                    <th>Net Weight</th>
                    <td><?php echo number_format($total['daily']['total_net_wt'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_net_wt'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_net_wt'],3) ?></td>
                </tr>
                <tr><td></td><td></td><td></td></tr>
                <tr>
                    <th>Top Coat</th>
                    <td><?php echo number_format($total['daily']['total_top_coat'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_top_coat'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_top_coat'],3) ?></td>
                </tr>
                <tr>
                    <th>Foam Coat</th>
                    <td><?php echo number_format($total['daily']['total_foam_coat'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_foam_coat'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_foam_coat'],3) ?></td>
                </tr>
                <tr>
                    <th>Adhesive Coat</th>
                    <td><?php echo number_format($total['daily']['total_adhesive_coat'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_adhesive_coat'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_adhesive_coat'],3) ?></td>
                </tr>
                <tr>
                    <th>Fabric weight</th>
                    <td><?php echo number_format($total['daily']['total_fabric_wt'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_fabric_wt'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_fabric_wt'],3) ?></td>
                </tr>
                <tr>
                    <th>Gross Weight</th>
                    <td><?php echo number_format($total['daily']['total_gross_wt'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_gross_wt'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_gross_wt'],3) ?></td>
                </tr>
                <tr>
                    <th></th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Output Input Ratio</th>
                    <td><?php echo number_format($total['daily']['total_gross_wt']/$total['daily']['total_net_wt'],3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_gross_wt']/$total['monthly']['total_net_wt'],3) ?></td>
                    <td><?php echo number_format($total['yearly']['total_gross_wt']/$total['yearly']['total_net_wt'],3) ?></td>
                </tr>
                
                <tr>
                    <th>Per Hour Output</th>
                    <td><?php echo number_format($total['daily']['total_net_wt']/24,3) ?></td>
                    <td><?php echo number_format($total['monthly']['total_net_wt']/(24*$working_days_for_timeloss['monthly_working_day']),3) ?></td>
	                <td><?php echo number_format($total['yearly']['total_net_wt']/(24*$working_days_for_timeloss['yearly_working_day']),3) ?></td>
	            </tr>
	            <tr>
	            	<?php 
	            		$daily_val = (24*60 - ($daily_total))/60;
	            		$monthly_val = (24*60*$working_days_for_timeloss['monthly_working_day'] - ($monthly_total))/60;
	            		$yearly_val = (24*60*$working_days_for_timeloss['yearly_working_day'] - ($yearly_total))/60;
	                ?>
	                <th>Per Work Hour Output</th>
	                <td><?php echo number_format($total['daily']['total_net_wt']/$daily_val,3) ?></td>
	                <td><?php echo number_format($total['monthly']['total_net_wt']/($monthly_val),3) ?></td>
	                <td><?php echo number_format($total['yearly']['total_net_wt']/($yearly_val),3) ?></td>
	            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
    
</div>
</div>
</div>