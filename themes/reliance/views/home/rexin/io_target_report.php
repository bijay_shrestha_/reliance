<link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
<table id="daily-dispatch-table" class="table table-bordered table-striped">
    <thead>
        <tr>
                <th>Brand</th>
                <th>Input</th>
                <th>Output</th>
                <th>I/O Ratio</th>
                <th>Target Ratio</th>
                <th>Difference</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($targets as $target){?>
            
                <tr>
                    <td><?php echo $target['brand_name']?></td>
                    <?php 
                    $total_gross_wt = 0;
                    $total_net_wt = 0;
                    if(array_key_exists($target['id'], $reports)){
                        foreach($reports[$target['id']] as $report){
                            $total_gross_wt += $report['total_gross_wt'];
                            $total_net_wt += $report['total_net_wt'];
                        }
                    }
                    $ratio = ($total_gross_wt != 0)?$total_net_wt/$total_gross_wt:0;
                    ?>
                    <td><?php echo number_format($total_gross_wt,3) ?></td>
                    <td><?php echo number_format($total_net_wt,3) ?></td>
                    <td><?php echo number_format($ratio,3) ?></td>
                    <td><?php echo number_format($target['target'],3) ?></td>
                    <?php $difference = $ratio - $target['target'];?>
                    <td style="background :<?php echo ($difference >= 0)?'#f2dede':'#dff0d8'?>" ><?php echo ($total_net_wt != 0)?number_format($difference,3):'N/A' ?></td>
                </tr>
        <?php }?>
    </tbody>
</table>