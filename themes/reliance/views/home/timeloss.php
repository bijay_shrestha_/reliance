<?php // echo $date?>
<!-- <pre>
	<?php 
		// print_r($working_days_for_timeloss)
	?>
</pre> -->
<?php 
    $months = array('Baishak','Jesth','Ashad','Shrawan','Bhadra','Asoj','Kartik','Mangsir','poush','Magh','Falgun','Chaitra');
    $dates = explode('-',$date);
    if(number_format($dates[1])>=4){
        $current_year = $dates[0].'/'.($dates[0]+1);
    }else{
        $current_year = ($dates[0]-1).'/'.$date[0];
    }
?>
    <link rel="stylesheet" href="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.css">
    <br>
    <?php foreach($reasons as $type => $reason){?>
<div class="box">
    <div class="box-header">
    	<h4><?php echo $type?> Reason %</h4>
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th rowspan='2'>Reason</th>
                    <th colspan='2'>Last Entry Date(<?php echo $date?>)</th>
                    <th colspan='2'>This Month(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th colspan='2'>This Year(<?php echo $current_year?>)</th>
                </tr>
                <tr>
                    <th>Total</th>
                    <th>%</th>
                    <th>Total</th>
                    <th>%</th>
                    <th>Total</th>
                    <th>%</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($reason as $res){?>
                <tr>
                    <th><?php echo $res['reason'] ?></th>
                    <td><?php echo (@$daily_loss[$type][$res['reason_id']])?(floor($daily_loss[$type][$res['reason_id']]['total']/60).'hr '.($daily_loss[$type][$res['reason_id']]['total']%60).'min'):'0hr 0min' ?></td>
                    <td><?php echo (@$daily_loss[$type][$res['reason_id']])?number_format(($daily_loss[$type][$res['reason_id']]['total']*100/$daily_loss[$type]['total']['total']),2).'%':'0%' ?></td>
                    <td><?php echo (@$monthly_loss[$type][$res['reason_id']])?(floor($monthly_loss[$type][$res['reason_id']]['total']/60).'hr '.($monthly_loss[$type][$res['reason_id']]['total']%60).'min'):'0hr 0min' ?></td>
                    <td><?php echo (@$monthly_loss[$type][$res['reason_id']])?number_format(($monthly_loss[$type][$res['reason_id']]['total']*100/$monthly_loss[$type]['total']['total']),2).'%':'0%' ?></td>
                    <td><?php echo (@$yearly_loss[$type][$res['reason_id']])?(floor($yearly_loss[$type][$res['reason_id']]['total']/60).'hr '.($yearly_loss[$type][$res['reason_id']]['total']%60).'min'):'0hr 0min' ?></td>
                    <td><?php echo (@$yearly_loss[$type][$res['reason_id']])?number_format(($yearly_loss[$type][$res['reason_id']]['total']*100/$yearly_loss[$type]['total']['total']),2).'%':'0%' ?></td>
                </tr>
            <?php }?>
            	<tr>
                    <th>Total</th>
                    <td><?php echo (@$daily_loss[$type])?(floor($daily_loss[$type]['total']['total']/60).'hr '.($daily_loss[$type]['total']['total']%60).'min') :'0hr 0min' ?></td>
                    <td><?php echo (@$daily_loss[$type] && $daily_loss[$type]['total']['total']>0)?$daily_loss[$type]['total']['total']*100/$daily_loss[$type]['total']['total'].'%':'0%' ?></td>
                    <td><?php echo (@$monthly_loss[$type])?(floor($monthly_loss[$type]['total']['total']/60).'hr '.($monthly_loss[$type]['total']['total']%60).'min') :'0hr 0min' ?></td>
                    <td><?php echo (@$monthly_loss[$type] && $monthly_loss[$type]['total']['total']>0)?$monthly_loss[$type]['total']['total']*100/$monthly_loss[$type]['total']['total'].'%':'0%' ?></td>
                    <td><?php echo (@$yearly_loss[$type])?(floor($yearly_loss[$type]['total']['total']/60).'hr '.($yearly_loss[$type]['total']['total']%60).'min') :'0hr 0min' ?></td>
                    <td><?php echo (@$yearly_loss[$type] && $yearly_loss[$type]['total']['total']>0)?$yearly_loss[$type]['total']['total']*100/$yearly_loss[$type]['total']['total'].'%':'0%' ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
    <?php }?>

    <!-- for losshour calculation -->
<div class="box">
    <div class="box-header">
    	<h4>Loss Hours Calculations</h4>
    </div>
    <div class="box-body">
        <table id="daily-dispatch-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Last Entry Date(<?php echo $date?>)</th>
                    <th>This Month(<?php echo $months[number_format($dates[1]-1)]?>)</th>
                    <th>This Year(<?php echo $current_year?>)</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $daily_total=0;
            $monthly_total=0;
            $yearly_total=0;
            ?>
            <?php foreach($reasons as $index => $res){?>
                <tr>
                	<?php 
                		$daily_total += $daily_loss[$index]['total']['total'];
                		$monthly_total += $monthly_loss[$index]['total']['total'];
                		$yearly_total += $yearly_loss[$index]['total']['total'];
                        // if($this->session->userdata('department_id') != 3){
                            $monthly_val = (@$working_days_for_timeloss['monthly_working_day'])?$monthly_loss[$index]['total']['total'] / $working_days_for_timeloss['monthly_working_day']:'0hr 0min';
                    		$yearly_val = $yearly_loss[$index]['total']['total'] / $working_days_for_timeloss['yearly_working_day'];
                        // }else{
                        //     $monthly_val = $monthly_loss[$index]['total']['total'] / $working_days['monthly_working_day'];
                        //     $yearly_val = $yearly_loss[$index]['total']['total'] / $working_days['yearly_working_day'];
                        // }
            		?>
                    <th><?php echo $index ?></th>
                    <td><?php echo (@$daily_loss[$index])?(floor($daily_loss[$index]['total']['total']/60).'hr '.($daily_loss[$index]['total']['total']%60)).'min':'0%' ?></td>
                    <td><?php echo (@$monthly_loss[$index])?(floor($monthly_val/60).'hr '.($monthly_val%60)).'min':'0%' ?></td>
                    <td><?php echo (@$yearly_loss[$index])?(floor($yearly_val/60).'hr '.($yearly_val%60)).'min':'0%' ?></td>
                </tr>
            <?php }?>
            <tr>
            	<?php 
            		$daily_val = 24*60 - ($daily_total);
            		$monthly_val = (@$working_days_for_timeloss['monthly_working_day'])?(24*60 - ($monthly_total / $working_days_for_timeloss['monthly_working_day'])):(24*60);
            		$yearly_val = 24*60 - ($yearly_total / $working_days_for_timeloss['yearly_working_day']);
            	?>
                <th>Worked Hour</th>
                <td><?php echo (@$daily_loss[$index])?(($daily_val/60 > 0)?floor($daily_val/60):ceil($daily_val/60).'hr '.($daily_val%60)).'min':'0%' ?></td>
                <td><?php echo (@$monthly_loss[$index])?(floor($monthly_val/60).'hr '.($monthly_val%60)).'min':'0%' ?></td>
                <td><?php echo (@$yearly_loss[$index])?(floor($yearly_val/60).'hr '.($yearly_val%60)).'min':'0%' ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>