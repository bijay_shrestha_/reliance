<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="row">
    	<div class="col-md-3"><input type="text" name="date" id="report_date" class="form-control margin-bottom"></div><div class="col-md-3"><button class='btn btn-primary' id="report_date_search">View Report</button></div>
    	<div class="col-md-6">
    		<a href="<?php echo site_url('office/excel')?>/<?php echo (array_key_exists(0, $attendences))?$attendences[0]['date_np']:$date?>" class="pull-right btn btn-primary" id="excel">Excel</a>
    		<a href="<?php echo site_url('office/pdf')?>/<?php echo (array_key_exists(0, $attendences))?$attendences[0]['date_np']:$date?>" class="pull-right btn btn-primary" id="pdf" style="margin-right: 5px">PDF</a>
		</div>
    </div>

    <div class="box">
    	<div class="box-header"><h4>Staff/Labour Record (<?php echo (array_key_exists(0, $attendences))?$attendences[0]['date_np']:$date?>)</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="example" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th>Department</th>
				                    <th>Day Staff</th>
				                    <th>Day Contract</th>
				                    <th>Night Staff</th>
				                    <th>Night Contract</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($attendences as $row) { ?>
				                    <tr>
				                        <!-- <td><?php echo $row['date_np']?></td> -->
				                        <th><?php echo $row['name'] ?></th>
				                        <td><?php echo ($row['day_staff'])?$row['day_staff']:'0' ?></td>
				                        <td><?php echo ($row['day_contract'])?$row['day_contract']:'0' ?></td>
				                        <td><?php echo ($row['night_staff'])?$row['night_staff']:'0'?></td>
				                        <td><?php echo ($row['night_contract'])?$row['night_contract']:'0'?></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				            <tfoot>
				            	<tr>
				            		<th>Total</th>
				            		<!-- <td></td> -->
				            		<td></td>
				            		<td></td>
				            		<td></td>
				            		<td></td>
				            	</tr>
				            </tfoot>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <div class="box">
    	<div class="box-header"><h4>Electric Report (<?php echo (array_key_exists(0, $attendences))?$attendences[0]['date_np']:$date?>)</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                    <th>R1</th>
				                    <th>R2</th>
				                    <th>R3</th>
				                    <th>Total</th>
				                </tr>
				            </thead>
				            <tbody>
				            <?php if(count($electric_reports)>0){?>
				                    <tr>
				                        <td><?php echo number_format($electric_reports['r1'],3) ?></td>
				                        <td><?php echo number_format($electric_reports['r2'],3) ?></td>
				                        <td><?php echo number_format($electric_reports['r3'],3) ?></td>
				                        <td><?php echo number_format($electric_reports['total'],3)?></td>
				                    </tr>
		                    <?php }else{?>
		                    	<tr>
		                    		<td>No matching records found</td>
		                    	</tr>
		                    <?php }?>
				            </tbody>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <!----------------------------------------------------genetator report------------------------------------------------------------>
    <div class="box">
    	<div class="box-header"><h4>Generator Report</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="generator-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th>Generator Set</th>
				                    <th>Running Hour</th>
				                    <th>Units</th>
				                    <th>Diesel Consumed</th>
				                    <th>Unit per liter</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($generator_reports as $row) { ?>
				                    <tr>
				                    	<?php
				                    		$hour = $row['total_hour'] + floor($row['total_min'] / 60);
				                    		$min = $row['total_min'] % 60;
				                    	?>
				                        <!-- <td><?php echo $row['date_np']?></td> -->
				                        <th><?php echo $row['name'] ?></th>
				                        <td><?php echo $hour . 'hour ' . $min . 'min'?></td>
				                        <td><?php echo number_format(($row['total_unit'])?$row['total_unit']:'0',3) ?></td>
				                        <td><?php echo number_format(($row['total_amount'])?$row['total_amount']:'0',3)?></td>
				                        <td><?php echo number_format(($row['total_amount'])?number_format($row['total_unit']/$row['total_amount'],3):'0',3)?></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				            <tfoot>
				            	<tr>
				            		<th>Total</th>
				            		<!-- <td></td> -->
				            		<th></th>
				            		<th></th>
				            		<th></th>
				            		<th></th>
				            	</tr>
				            </tfoot>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-6">
    		<!----------------------------------------------------diesel report------------------------------------------------------------>
		    <div class="box">
		    	<div class="box-header"><h4>Diesel Report</h4></div>
			    <div class="box-body">
			    	<div class="row">
			    		<div class="col-md-12">
			    			<div id="display-div">

								<table id="diesel-table" class="table table-bordered table-striped">
						            <thead>
						                <tr>
						                	<!-- <th>Date</th> -->
						                    <th>Opening Stock</th>
						                    <th>Purchase</th>
						                    <th>Total</th>
						                    <th>Issue/Consumed</th>
						                    <th>closing Stock</th>
						                </tr>
						            </thead>
						            <tbody>
					                    <tr>
					                        <!-- <td><?php echo $row['date_np']?></td> -->
					                        <td><?php echo number_format($diesel_reports['opening_stock'] ,3)?></td>
					                        <td><?php echo number_format($diesel_reports['purchase'],3)?></td>
					                        <td><?php echo number_format($diesel_reports['opening_stock'] + $diesel_reports['purchase'] ,3)?></td>
					                        <td><?php echo number_format($diesel_reports['dispatch'],3)?></td>
					                        <td><?php echo number_format($diesel_reports['opening_stock'] + $diesel_reports['purchase'] - $diesel_reports['dispatch'],3)?></td>
					                    </tr>
						            </tbody>
				        		</table>
			    	    	</div>
				        </div>
			        </div>
		        </div>
		    </div>
    	</div>
    	<div class="col-md-6">
    		<!----------------------------------------------------Husk report------------------------------------------------------------>
		    <div class="box">
		    	<div class="box-header"><h4>Husk Report</h4></div>
			    <div class="box-body">
			    	<div class="row">
			    		<div class="col-md-12">
			    			<div id="display-div">

								<table id="husk-table" class="table table-bordered table-striped">
						            <thead>
						                <tr>
						                	<!-- <th>Date</th> -->
						                    <th>Opening Stock</th>
						                    <th>Purchase</th>
						                    <th>Total</th>
						                    <th>Issue/Consumed</th>
						                    <th>closing Stock</th>
						                </tr>
						            </thead>
						            <tbody>
					                    <tr>
					                        <!-- <td><?php echo $row['date_np']?></td> -->
					                        <td><?php echo number_format($husk_reports['opening_stock'] ,3)?></td>
					                        <td><?php echo number_format($husk_reports['purchase'],3)?></td>
					                        <td><?php echo number_format($husk_reports['opening_stock'] + $husk_reports['purchase'] ,3)?></td>
					                        <td><?php echo number_format($husk_reports['dispatch'],3)?></td>
					                        <td><?php echo number_format($husk_reports['opening_stock'] + $husk_reports['purchase'] - $husk_reports['dispatch'],3)?></td>
					                    </tr>
						            </tbody>
				        		</table>
			    	    	</div>
				        </div>
			        </div>
		        </div>
		    </div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-6">
    <!----------------------------------------------------CT2YARD report------------------------------------------------------------>
		    <div class="box">
		    	<div class="box-header"><h4>CT 2 Yard Report</h4></div>
			    <div class="box-body">
			    	<div class="row">
			    		<div class="col-md-12">
			    			<div id="display-div">

								<table id="husk-table" class="table table-bordered table-striped">
						            <thead>
						                <tr>
						                	<!-- <th>Date</th> -->
						                    <th>Opening Stock</th>
						                    <th>Issue/Consumed</th>
						                    <th>closing Stock</th>
						                </tr>
						            </thead>
						            <tbody>
					                    <tr>
					                        <!-- <td><?php echo $row['date_np']?></td> -->
					                        <td><?php echo number_format($ct2yard_reports['opening_stock'] ,3)?></td>
					                        <td><?php echo number_format($ct2yard_reports['dispatch'],3)?></td>
					                        <td><?php echo number_format($ct2yard_reports['opening_stock'] + $ct2yard_reports['purchase'] - $ct2yard_reports['dispatch'],3)?></td>
					                    </tr>
						            </tbody>
				        		</table>
			    	    	</div>
				        </div>
			        </div>
		        </div>
		    </div>

	    </div>
	    <div class="col-md-6">
    	<!----------------------------------------------------CT 2 Meter report------------------------------------------------------------>
		    <div class="box">
		    	<div class="box-header"><h4>CT 2 Meter Report</h4></div>
			    <div class="box-body">
			    	<div class="row">
			    		<div class="col-md-12">
			    			<div id="display-div">

								<table id="husk-table" class="table table-bordered table-striped">
						            <thead>
						                <tr>
						                	<!-- <th>Date</th> -->
						                    <th>Opening Stock</th>
						                    <th>Issue/Consumed</th>
						                    <th>closing Stock</th>
						                </tr>
						            </thead>
						            <tbody>
					                    <tr>
					                        <!-- <td><?php echo $row['date_np']?></td> -->
					                        <td><?php echo number_format($ct2mt_reports['opening_stock'] ,3)?></td>
					                        <td><?php echo number_format($ct2mt_reports['dispatch'],3)?></td>
					                        <td><?php echo number_format($ct2mt_reports['opening_stock'] + $ct2mt_reports['purchase'] - $ct2mt_reports['dispatch'],3)?></td>
					                    </tr>
						            </tbody>
				        		</table>
			    	    	</div>
				        </div>
			        </div>
		        </div>
		    </div>
    
    	</div>
    </div>
    
    <!----------------------------------------------------production report------------------------------------------------------------>
    <div class="box">
    	<div class="box-header"><h4>Production Report</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="husk-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th>Item</th>
				                    <th>Unit</th>
				                    <th>Today</th>
				                    <th>This Month</th>
				                    <th>This Year</th>
				                </tr>
				            </thead>
				            <tbody>
				            <?php
				            $total_2 = 0;
				            $total_1 = 0;
				            $total_0 = 0;
				            foreach ($production_reports as $key => $value) {?>
			                    <tr>
			                        <th><?php echo $key ?></th>
			                        <!-- <td><?php echo $row['date_np']?></td> -->
			                        <td><?php echo $value['unit']?></td>
			                        <td><?php echo number_format($value[2],3) ;$total_2 += $value[2]?></td>
			                        <td><?php echo number_format($value[1],3) ;$total_1 += $value[1]?></td>
			                        <td><?php echo number_format($value[0],3) ;$total_0 += $value[0]?></td>
			                    </tr>
				            <?php }?>
				            	<tr>
			                        <th>Total</th>
			                        <!-- <td><?php echo $row['date_np']?></td> -->
			                        <td></td>
			                        <td><?php echo number_format($total_2,3)?></td>
			                        <td><?php echo number_format($total_1,3)?></td>
			                        <td><?php echo number_format($total_0,3)?></td>
			                    </tr>
				            </tbody>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <!----------------------------------------------------IO report------------------------------------------------------------>
    <div class="box">
    	<div class="box-header"><h4>Input/Output Report</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="husk-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th>Section</th>
				                    <th>Input</th>
				                    <th>Output</th>
				                    <th>Difference</th>
				                    <th>Loss %</th>
				                </tr>
				            </thead>
				            <tbody>
				            <?php
				            $input = 0;
				            $output = 0;
				            $difference = 0;
				            foreach ($io_reports as $key => $value) {?>
			                    <tr>
			                        <th><?php echo $key ?></th>
			                        <!-- <td><?php echo $row['date_np']?></td> -->
			                        <td><?php echo number_format($value['input'],3); $input += $value['input']?></td>
			                        <td><?php echo number_format($value['output'],3) ;$output += $value['output']?></td>
			                        <td><?php echo number_format($value['input'] - $value['output'],3) ;$difference += $value['input'] - $value['output']?></td>
			                        <td><?php if($value['input'] >0){echo (number_format(($value['input'] - $value['output'])*100/$value['input'],2));}else{echo '0.00';}?></td>
			                    </tr>
				            <?php }?>
				            	<tr>
			                        <th>Total</th>
			                        <!-- <td><?php echo $row['date_np']?></td> -->
			                        <td><?php echo number_format($input,3)?></td>
			                        <td><?php echo number_format($output,3)?></td>
			                        <td><?php echo number_format($difference,3)?></td>
			                        <td></td>
			                    </tr>
				            </tbody>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <!----------------------------------------------------Scrap report------------------------------------------------------------>
    <div class="box">
    	<div class="box-header"><h4>Scrap machine Production Report</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="husk-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th></th>
				                    <th>Amount</th>
				                    <th>Segregation Duration</th>
				                    <th>Running Duration</th>
				                </tr>
				            </thead>
				            <tbody>
				            <?php foreach($scrap_reports as $key => $value){?>
				            	<tr>
				            		<th><?php echo $key?></th>
				            		<td><?php echo number_format($value['total'],3)?></td>
				            		<td><?php echo $value['total_hr'].'hr '.$value['total_min'].'min'?></td>
				            		<td><?php echo $value['total_running_hr'].'hr '.$value['total_running_min'].'min'?></td>
				            	</tr>
			            	<?php }?>
				            </tbody>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <!----------------------------------------------------paper tube report------------------------------------------------------------>
    <div class="box">
    	<div class="box-header"><h4>Paper Tube Report</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="husk-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th>Size</th>
				                    <th>Opening Stock</th>
				                    <th>Production</th>
				                    <th>Issue to Store</th>
				                    <th>Balance</th>
				                </tr>
				            </thead>
				            <tbody>
				            <?php 
				            $opening = 0;
				            $production = 0;
				            $issue = 0;
				            $balance = 0;
				            foreach($paper_tube_reports as $key => $value){?>
				            	<tr>
				            		<th><?php echo $value['material_name']?></th>
				            		<td><?php echo number_format($value['opening_stock']['opening_stock'],3); $opening += $value['opening_stock']['opening_stock']?></td>
				            		<td><?php echo number_format($value['production'],3); $production += $value['production'];?></td>
				            		<td><?php echo number_format($value['opening_stock']['purchase'],3); $issue += $value['opening_stock']['purchase']?></td>
				            		<td><?php echo number_format($value['opening_stock']['opening_stock'] + $value['production'] - $value['opening_stock']['purchase'],3);
				            		$balance += $value['opening_stock']['opening_stock'] + $value['production'] - $value['opening_stock']['purchase']?></td>
				            	</tr>
			            	<?php }?>
			            	<tr>
			            		<th>Total</th>
			            		<td><?php echo number_format($opening,3)?></td>
			            		<td><?php echo number_format($production,3)?></td>
			            		<td><?php echo number_format($issue,3)?></td>
			            		<td><?php echo number_format($balance,3)?></td>
			            	</tr>
				            </tbody>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <!----------------------------------------------------paper tube working report------------------------------------------------------------>
    <div class="box">
    	<div class="box-header"><h4>Paper Tube Working Report</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="husk-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th></th>
				                    <!-- <th>Date</th> -->
				                    <th>Duration</th>
				                </tr>
				            </thead>
				            <tbody>
				            <?php 
				            foreach($paper_tube_working_reports as $key => $value){?>
				            	<tr>
				            		<th><?php echo $key?></th>
				            		<!-- <td><?php echo (count($value)>0)?$value['date_np']:'--'?></td> -->
				            		<td><?php echo (count($value)>0)?(($value['hour']<10)?'0':'').$value['hour'].':'.(($value['min']<10)?'0':'').$value['min']:'00:00'?></td>
				            	</tr>
			            	<?php }?>
				            </tbody>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
    <!----------------------------------------------------time loss report------------------------------------------------------------>
    <div class="box">
    	<div class="box-header"><h4>Time Loss Report</h4></div>
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div id="display-div">

						<table id="tl-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<!-- <th>Date</th> -->
				                    <th rowspan="2">Department</th>
				                    <th colspan="3">Last Entry Date</th>
				                    <th colspan="3">This Month</th>
				                    <th colspan="3">This Year</th>
				                </tr>
				                <tr>
				                	<th>BreakDown</th>
				                	<th>LossHour</th>
				                	<th>Worked Hour</th>
				                	<th>BreakDown</th>
				                	<th>LossHour</th>
				                	<th>Worked Hour</th>
				                	<th>BreakDown</th>
				                	<th>LossHour</th>
				                	<th>Worked Hour</th>
				                </tr>
				            </thead>
				            <tbody>
				            <?php 
				            $opening = 0;
				            $production = 0;
				            $issue = 0;
				            $balance = 0;
				            foreach($timeLossReport as $key => $value){?>
				            	<tr>
				            		<th><?php echo $value['name']?></th>
				            		<td><?php echo date('H:i', mktime(0, $value['daily_loss']['BreakDown']['total']['total']))?></td>
				            		<td><?php echo date('H:i', mktime(0, $value['daily_loss']['LossHour']['total']['total']))?></td>
				            		<td><?php 
				            			if($value['daily_loss']['BreakDown']['total']['total'] != 0 || $value['daily_loss']['LossHour']['total']['total'] != 0){
				            				echo date('H:i', mktime(0, 60*24 - $value['daily_loss']['LossHour']['total']['total'] -$value['daily_loss']['BreakDown']['total']['total']));
				            			}else{
				            				echo '24:00';
				            			}
				            		?></td>
				            		<td><?php echo date('H:i', mktime(0, $value['monthly_loss']['BreakDown']['total']['total']/$value['working_days_for_timeloss']['monthly_working_day']))?></td>
				            		<td><?php echo date('H:i', mktime(0, $value['monthly_loss']['LossHour']['total']['total']/$value['working_days_for_timeloss']['monthly_working_day']))?></td>
				            		<td><?php 
				            			if($value['monthly_loss']['BreakDown']['total']['total'] != 0 || $value['monthly_loss']['LossHour']['total']['total'] != 0){
				            				echo date('H:i', mktime(0, (60*24*$value['working_days_for_timeloss']['monthly_working_day'] - $value['monthly_loss']['LossHour']['total']['total'] -$value['monthly_loss']['BreakDown']['total']['total'])/$value['working_days_for_timeloss']['monthly_working_day']));
				            			}else{
				            				echo '24:00';
				            			}
		            				?></td>
				            		<td><?php echo date('H:i', mktime(0, $value['yearly_loss']['BreakDown']['total']['total']/$value['working_days_for_timeloss']['yearly_working_day']))?></td>
				            		<td><?php echo date('H:i', mktime(0, $value['yearly_loss']['LossHour']['total']['total']/$value['working_days_for_timeloss']['yearly_working_day']))?></td>
				            		<td><?php 
				            			if ($value['yearly_loss']['BreakDown']['total']['total'] != 0 || $value['yearly_loss']['LossHour']['total']['total'] != 0) {
				            				echo date('H:i', mktime(0, (60*24*$value['working_days_for_timeloss']['yearly_working_day'] - $value['yearly_loss']['LossHour']['total']['total'] -$value['yearly_loss']['BreakDown']['total']['total'])/$value['working_days_for_timeloss']['yearly_working_day']));
				            			}else{
				            				echo '24:00';
				            			}
		            				?></td>
				            		
				            	</tr>
			            	<?php }?>
			            	<!-- <tr>
			            		<th>Total</th>
			            		<td><?php echo number_format($opening,3)?></td>
			            		<td><?php echo number_format($production,3)?></td>
			            		<td><?php echo number_format($issue,3)?></td>
			            		<td><?php echo number_format($balance,3)?></td>
			            	</tr> -->
				            </tbody>
		        		</table>
	    	    	</div>
		        </div>
	        </div>
        </div>
    </div>
</section>

	        <script type="text/javascript">
	        $(function(){
	        	$('#report_date').nepaliDatePicker();
	        	$('#example').dataTable({
					"order": [[0,'asc'],[1,'asc']],
					dom: 'Bfrtip',
		            buttons: [
		                {
		                    extend: 'excelHtml5',
		                },
		            ],
		            paging: false,

		            "footerCallback": function ( row, data, start, end, display ) {
			            var api = this.api(), data;
			 
			            // Remove the formatting to get integer data for summation
			            var intVal = function ( i ) {
			                return typeof i === 'string' ?
			                    i.replace(/[\$,]/g, '')*1 :
			                    typeof i === 'number' ?
			                        i : 0;
			            };
			 
			            // Total over all pages
			            total1 = api
			                .column( 1 )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Total over this page
			            pageTotal1 = api
			                .column( 1, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Update footer
			            $( api.column( 1 ).footer() ).html(
			                pageTotal1 
			            );

			            // Total over all pages
			            total2 = api
			                .column( 2 )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Total over this page
			            pageTotal2 = api
			                .column( 2, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Update footer
			            $( api.column( 2 ).footer() ).html(
			                pageTotal2
			            );

			            // Total over all pages
			            total3 = api
			                .column( 3 )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Total over this page
			            pageTotal3 = api
			                .column( 3, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Update footer
			            $( api.column( 3 ).footer() ).html(
			                pageTotal3
			            );

			            // Total over all pages
			            total4 = api
			                .column( 4 )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Total over this page
			            pageTotal4 = api
			                .column( 4, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Update footer
			            $( api.column( 4 ).footer() ).html(
			                pageTotal4
			            );
			        }
				});

				$('#generator-table').dataTable({
					dom: 'Bfrtip',
		            buttons: [
		                {
		                    extend: 'excelHtml5',
		                },
		            ],
		            paging: false,
		            "footerCallback": function ( row, data, start, end, display ) {
			            var api1 = this.api(), data;
			 
			            // Remove the formatting to get integer data for summation
			            var intVal = function ( i ) {
			                return typeof i === 'string' ?
			                    i.replace(/[\$,]/g, '')*1 :
			                    typeof i === 'number' ?
			                        i : 0;
			            };
			 
			            // Total over all pages
			            total_gen = api1
			                .column( 3 )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			            // Update footer
			            $( api1.column( 3 ).footer() ).html(
			                total_gen.toFixed(3)
			            );
			        }
		        });
	        });
	        </script>
<script type="text/javascript">
	$('#report_date_search').click(function(){
		var date = $('#report_date').val();
		window.location = "<?php echo site_url('office')?>/index/"+date;
	});
</script>
<script type="text/javascript">
	$("#excel").click(function(){
		// alert();
		// $.post('<?php echo site_url("office/export/test")?>');
	});
</script>
<script type="text/javascript">
	// $("#pdf").click(function(){
	// 	alert();
	// });
</script>