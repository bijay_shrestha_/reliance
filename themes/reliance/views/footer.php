      <footer>
        <p>&copy; Powered By <a href="http://pagodalabs.com" target="_blank">Pagoda Labs</a> <?php echo date('Y')?></p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    
    
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script> -->
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo theme_url()?>assets/js/bootstrap.min.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo theme_url()?>plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo theme_url()?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo theme_url()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo theme_url()?>assets/js/app.min.js"></script>

      <!-- DataTables -->
      <script src="<?php echo theme_url()?>assets/js/datatables/jquery.dataTables.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/dataTables.bootstrap.min.js"></script>

      
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="<?php echo theme_url()?>assets/js/pages/dashboard.js"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo theme_url()?>assets/js/demo.js"></script>
      <!-- Nepali Datepicker  -->
      <script src="<?php echo theme_url()?>assets/js/nepalidatepicker/nepalidatepicker.js"></script>
        
    <?php /*if($this->preference->item('activate_google_analytics')):?>
        
    <script type="text/javascript" src="<?php echo theme_uel('assets/js/nepalidatepicker/nepalidatepicker.js')?>"></script>
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', '<?php echo $this->preference->item('google_analytics_tracking_code')?>']);
      _gaq.push(['_trackPageview']);
    
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    <?php endif*/?>


      <script src="<?php echo theme_url()?>assets/js/datatables/dataTables.buttons.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/buttons.colVis.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/buttons.flash.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/pdfmake.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/jszip.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/vfs_fonts.js"></script>
      <script src="<?php echo theme_url()?>assets/js/datatables/buttons.html5.min.js"></script>
      <!-- for validation -->
      <!-- jquery.mockjax -->
      <script src="<?php echo theme_url()?>plugins/validator/attrvalidate.jquery.min.js"></script>
      <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

      <!-- for multiselector -->
      <script src="<?php echo base_url()?>assets/js/jquery.multi-select.js" type="text/javascript"></script>
      <!-- quick search -->
      <script src="<?php echo base_url()?>assets/js/jquery.quicksearch.js" type="text/javascript"></script>

  </body>
</html>
