<table id="brand_list" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Brand Name</th>
            <th>Type</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($rows as $key => $value) {?>
	    	<tr>
	    		<td><?php echo $value['brand_name']?></td>
	    		<td><?php echo $value['type_name']?></td>
	    	</tr>
    	<?php }?>
    </tbody>
</table>