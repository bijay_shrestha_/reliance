<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
  	<div class="row">
    	<div class="col-xs-12">
      		<div class="box">
        		<div class="box-header">
          			<h3 class="box-title"><?php echo $header?> Detail</h3>
        		</div><!-- /.box-header -->
        		<div class="box-body">
  					<button class="btn btn-primary" id="add-btn">Add</button>
        			<table id="party_list" class="table table-bordered table-striped">
        				<thead>
        					<tr>
	        					<th>Agent</th>
	        					<th>Action</th>
        					</tr>
        				</thead>
        				<tbody>
        				<?php foreach ($rows as $key => $value) {?>
        					<tr>
        						<td><?php echo $value['party']?></td>
        						<td>
        							<button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)" title="Edit"><i class="fa fa-pencil"></i></button>
        							<button class="btn btn-danger" onclick="delete_party(<?php echo ($value['id'])?>)" title="delete"><i class="fa fa-trash"></i></button>
        						</td>
        					</tr>
        				<?php }?>
        				</tbody>
        			</table>
        		</div>
        	</div>
    	</div>
	</div>
</section>

<!-- modal to add party -->
<div class="modal fade" id="party-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="party" class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="party" class="form-control" id="party" placeholder="party Name">
                                <div id="error-party" hidden style="color: red" class="error-div">Name is required</div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#party_list').DataTable({
			// "order":[[0, "desc"]],
			dom: 'lBfrtip',
	            buttons: [
	                {
	                    extend: 'excelHtml5',
	                },
	            ],
		});
	});
</script>
<script type="text/javascript">
	$('#add-btn').click(function(){
        $(".error-div").hide();
		$('input').val('');
		$('option').attr('selected', false);
		$('#party-dialog').modal();
	})
</script>
<script type="text/javascript">
	$('#form-submit').click(function(){
        var f = 0;
        
        if($('#party').val() == ''){
            $('#error-party').show();
            f = 1;
        }else{
            $('#error-party').hide();
        }

        if(f == 0){
    		$.post('<?php echo site_url("party/save")?>',$('#add-form').serializeArray(),function(data){
    			if(data.success){
    				alert('Success');
    				location.reload();
    			}else{
    				alert('Error occured')
    			}
    		},'json');
        }
	});
</script>
<script type="text/javascript">
	function form_edit(id){
        $(".error-div").hide();
		$.post('<?php echo site_url("party/get_value")?>',{id:id},function(data){
			$.each(data, function( index, value ) {
				$('#'+index).val(value);
			});
			$('#party-dialog').modal();
		},'json');
	}
</script>
<script type="text/javascript">
	function delete_party(id){
		$.post('<?php echo site_url("party/delete_json")?>',{id:id},function(){
			alert('Data Deleted');
			location.reload();
		},'json');
	}
</script>