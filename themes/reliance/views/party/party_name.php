<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
  	<div class="row">
    	<div class="col-xs-12">
      		<div class="box">
        		<div class="box-header">
          			<h3 class="box-title"><?php echo $header?> Detail</h3>
        		</div><!-- /.box-header -->
        		<div class="box-body">
  					<button class="btn btn-primary" id="add-btn">Add</button>
        			<table id="party_list" class="table table-bordered table-striped">
        				<thead>
        					<tr>
                                <th>Party Name</th>
	        					<th>Assigned Agent</th>
	        					<th>Action</th>
        					</tr>
        				</thead>
        				<tbody>
        				<?php foreach ($rows as $key => $value) {?>
        					<tr>
                                <td><?php echo $value['party_name']?></td>
        						<td><?php echo $value['party']?></td>
        						<td>
                                    <button class="btn btn-primary" onclick="view_brand(<?php echo $value['id']?>)" title="View Brands"><i class="fa fa-eye"></i></button>
                                    <button class="btn btn-primary" onclick="assign_brand(<?php echo $value['id']?>)" title="Assign Brands"><i class="fa fa-user-plus"></i></button>
        							<button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)" title="Edit Party"><i class="fa fa-pencil"></i></button>
        							<button class="btn btn-danger" onclick="delete_party(<?php echo ($value['id'])?>)" title="Delete Party"><i class="fa fa-trash"></i></button>
        						</td>
        					</tr>
        				<?php }?>
        				</tbody>
        			</table>
        		</div>
        	</div>
    	</div>
	</div>
</section>

<!-- modal to view brand list -->
<div class="modal fade" id="brand-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Brand List</h4>
            </div>
            <div class="modal-body">
                <div id="brand_list"></div>
            </div>
        </div>
    </div>
</div>

<!-- modal to assign brand -->
<div class="modal fade" id="assign-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Brand List</h4>
            </div>
            <form class="form-horizontal" id="assign-form" action="#">
                <div class="modal-body">
                        <?php foreach ($brand_types as $key => $value) {?>
                        <div class="row form-group">
                            <input type="hidden" name="type[]" value="<?php echo $value['id']?>">
                            <div class="col-sm-4"><label><?php echo $value['name']?></label></div>
                            <div class="col-sm-8">
                                <select class="form-control" name="brand[]" id="brand-<?php echo $value['id']?>">
                                    <?php foreach ($brand[$value['name']] as $index => $val) {?>
                                        <option value="<?php echo $val['id']?>"><?php echo $val['brand_name']?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <?php }?>
                        <input type="hidden" name="party_id" id="party_brand_id">
                </div>
                <div class="modal-footer">
                    <button type="button" id="form-brand-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal to add party -->
<div class="modal fade" id="party-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="party" class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="party_name" class="form-control" id="party_name" placeholder="party Name">
                                <div id="error-party_name" hidden style="color: red" class="error-div">Name is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="party" class="col-sm-4 control-label">Agents</label>
                            <div class="col-sm-8">
                                <select name="assigned_party_id[]" class="form-control" id="assigned_party_id" placeholder="party"  multiple='multiple'>
                                    <option value="" disabled>--Select party--</option>
                                    <?php foreach ($party as $key => $value) {?>
                                        <option value="<?php echo $value['id']?>" id="option-<?php echo $value['id']?>"><?php echo $value['party']?></option>
                                    <?php }?>
                                </select>
                                <div id="error-assigned_party_id" hidden style="color: red" class="error-div">Party is required</div>
                            </div>
                        </div>

                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
	$(function(){
		$('#party_list').DataTable({
			// "order":[[0, "desc"]],
			dom: 'lBfrtip',
	            buttons: [
	                {
	                    extend: 'excelHtml5',
	                },
	            ],
		});

        // for multiple select
        // with search
        $('#assigned_party_id').multiSelect({
          selectableHeader: "<div class='custom-header' style='background:#3c8dbc;color:white; text-align: center; font-size: 200%;'>Agents</div><br><input type='text' class='search-input form-control margin-bottom' autocomplete='off' placeholder='Search Department'>",
          selectionHeader: "<div class='custom-header' style='background:#3c8dbc;color:white; text-align: center; font-size: 200%;'>Selected</div></br><input type='text' class='search-input form-control margin-bottom' autocomplete='off' placeholder='Search Selected'>",
          afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
            .on('keydown', function(e){
              if (e.which === 40){
                that.$selectableUl.focus();
                return false;
              }
            });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
            .on('keydown', function(e){
              if (e.which == 40){
                that.$selectionUl.focus();
                return false;
              }
            });
          },
          afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
          },
          afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
          }
        });
        // multiSelect ends
        
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $(".error-div").hide();
        $('input').val('');
        $('#assigned_party_id').multiSelect('deselect_all');
        $('option').attr('selected', false);
        $('#party-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var f = 0;
        if($('#party_name').val() == ''){
            $('#error-party_name').show();
            f = 1;
        }else{
            $('#error-party_name').hide();
        }

        if($('#assigned_party_id').val() == null){
            $('#error-assigned_party_id').show();
            f = 1;
        }else{
            $('#error-assigned_party_id').hide();
        }

        if(f == 0){
            $.post('<?php echo site_url("party_name/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $(".error-div").hide()
        $.post('<?php echo site_url("party_name/get_value")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });
            $('#assigned_party_id').multiSelect('deselect_all');
            $.each(data.agent_ids, function(index, value){
                // $('#option-' + value.agent_id).attr('selected','selected');
                $('#assigned_party_id').multiSelect('select', value.agent_id);
            })
            $('#party-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_party(id){
        $.post('<?php echo site_url("party_name/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>
<script type="text/javascript">
    function view_brand(party_id){

        $.post('<?php echo site_url('party_brand/getAssignedBrand')?>',{party_id:party_id}, function(data){
            $('#brand_list').html(data);
        },'html');

        $('#brand-dialog').modal();
    }
</script>
<script type="text/javascript">
    function assign_brand(party_id){
        $.post('<?php echo site_url('party_brand/getAssignedBrandForm')?>',{party_id:party_id},function(data){
            $.each(data, function( index, value ) {
                $('#brand-'+value.brand_type_id).val(value.brand_id);
            });
        },'json');
        $('#party_brand_id').val(party_id);
        $('#assign-dialog').modal();
    }
</script>
<script type="text/javascript">
    $('#form-brand-submit').click(function(){
        $.post('<?php echo site_url('party_brand/saveAssignedBrand')?>',$('#assign-form').serializeArray(),function(){
            alert('Success');
            location.reload();
        });
    });
</script>