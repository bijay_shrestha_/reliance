<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Report Type</a></li>
                <li><a href="#tab_2" data-toggle="tab">Add Item To Report</a></li>
                <li><a href="#tab_3" data-toggle="tab">Report Setting</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <button id="add-type" class="btn btn-primary margin">Add</button>
                    <!-- dealer table -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Dealer Table</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Report Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($report_types as $report_type) { ?>
                                            <tr>
                                                <td><?php echo $report_type['type'] ?></td>
                                                <td><button type="button" class="btn btn-warning edit_type" id="report-type-<?php echo $report_type['rt_id'] ?>" value="<?php echo $report_type['rt_id'] ?>"> Edit</button></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    
                    <!-- dealer table end -->


                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <form id="material-dealer-form">
                        <div class="row">
                            <div class="col-md-10">
                                <select id="material-for-report" name="type_id" class="form-control">
                                    <option value="" id="select-option">-- select type --</option>
                                    <?php foreach ($report_types as $report_type) { ?>
                                        <option value="<?php echo $report_type['rt_id'] ?>"><?php echo $report_type['type'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-1 margin">
                                <!--<button type="button" id="material-dealer-btn" class="btn btn-primary">Show</button>-->
                                <button type="button" id="material-dealer-save" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        <div class="row" id="material-list">
                            <?php 
                            $i = 0;
                            foreach ($materials as $material) { ?>
                                <div class = "col-md-3 col-sm-6 col-xs-6" >
                                    <input type = "checkbox" class="check_material" name = "material[]" value="<?php echo $material['material_id']?>" id="material-<?php echo $material['material_id']?>">
                                    <?php echo $material['material_name'] ?>
                                </div>
                            <?php 
                            $i++;} ?>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    <form id="material-dealer-form">
                        <div class="row">
                            <div class="col-md-10">
                                <select id="category-report-order" name="type_id" class="form-control">
                                    <option value="" id="select-option">-- select type --</option>
                                    <?php foreach ($report_types as $report_type) { ?>
                                        <option value="<?php echo $report_type['rt_id'] ?>"><?php echo $report_type['type'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-1 margin">
                                <!--<button type="button" id="material-dealer-btn" class="btn btn-primary">Show</button>-->
                                <!-- <button type="button" id="category-report-save" class="btn btn-primary">Save</button> -->
                            </div>
                        </div>
                        <div class="row" id="category-list">
                            
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
                
        </div><!-- nav-tabs-custom -->
    </div>
</section>

<div class="modal fade" id="member-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="report-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="type" class="col-sm-4 control-label">Type*</label>
                            <div class="col-sm-8">
                                <input type="text" name="type" class="form-control" id="type" placeholder="Report Type">
                                <span id="error-type" style="color: red"></span>
                            </div>
                        </div>

                        <input type="hidden" name="rt_id" id="rt_id" value="">

                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $('#example2').DataTable();
        $('#created_date_np').nepaliDatePicker();
    });
</script>
<script>
    $("#form-submit").click(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('report_type/add') ?>",
            data: $("#report-form").serializeArray(),
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
                    if($('#type').val() == ''){
                        $('#error-type').html(data.msg);
                    }
                } else {
                    $('#error-type').html('');
                    $('#type').val('');
                    $('#rt_id').val('');
                    location.reload();
                }
            },
        });
    });
</script>
<script>
    $('#add-type').click(function () {
        $('#myModalLabel').text('Add Report Type');
        $('#error-type').html('');
        $('#type').val('');
        $('#rt_id').val('');
        $('#member-dialog').modal();
    });
</script>
<script>
    $(".edit_type").click(function () {
        id = $(this).val();
        $.post('<?php echo site_url() ?>report_type/getReportType', {id: id}, function (data) {
            $('#myModalLabel').text('Edit Report Type');
            $('#error-type').html('');
            
            $('#type').val(data.type);
            
            $('#rt_id').val(data.rt_id);
            $('#member-dialog').modal();
        }, 'json');
    });
</script>
<script>
    $("#material-for-report").change(function () {
        var id = $(this).val();
        $('.check_material').removeAttr('checked');
        $.post('<?php echo site_url("report_material/getformmaterial") ?>', {id: id}, function (data) {
            $.each(data.result,function(index,value){
                $("#material-"+value.material_id).prop('checked',true).attr('checked', 'checked');
            });

        },'json');
    })
</script>
<script>
    $("#material-dealer-save").click(function(){
        $.post('<?php echo site_url()?>report_material/saverm',$("#material-dealer-form").serializeArray(),function(data){
            alert(data.msg);
            $('#select-option').prop('selected', true);
            $('.check_material').prop('checked',false);
        },'json')
    });
</script>
<script type="text/javascript">
    $("#category-report-order").change(function(){
        var id = $(this).val();
        $('#category-list').empty();
        $.post('<?php echo site_url("report_material/getCategory")?>',{id:id},function(data){
            $('#category-list').html(data);
        },'html');
    });
</script>