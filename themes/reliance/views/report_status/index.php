<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="col-md-1"><label>Date</label></div>
            <div class="col-md-2"><input type="text" class="form-control" id="date-np"></div>
            <div class="col-md-1"><input type="hidden" id="date-en"></div>
            <div class="col-md-1"><button id="btn-status" class="btn btn-primary"  onclick="get_report_status()">View Status</button></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="info-box">
                <span class="info-box-icon bg-grey" id='department-1'><i class="fa fa-file-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Mixing</span>
                    <span class="info-box-number btn-pdf" id="btn-1"></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
        
        <div class="col-md-2">
            <div class="info-box">
                <span class="info-box-icon bg-grey"  id='department-2'><i class="fa fa-file-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Calendar</span>
                    <span class="info-box-number btn-pdf" id="btn-2"></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-2">
            <div class="info-box">
                <span class="info-box-icon bg-grey"  id='department-3'><i class="fa fa-file-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Printing</span>
                    <span class="info-box-number btn-pdf" id="btn-3"></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->

        <div class="col-md-2">
            <div class="info-box">
                <span class="info-box-icon bg-grey" id='department-4'><i class="fa fa-file-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Laminating</span>
                    <span class="info-box-number btn-pdf" id="btn-4"></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
        
        <div class="col-md-2">
            <div class="info-box">
                <span class="info-box-icon bg-grey" id='department-5'><i class="fa fa-file-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Rexin</span>
                    <span class="info-box-number btn-pdf" id="btn-5"></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
</section>
<script type="text/javascript">
$(function(){
    $('#date-np').nepaliDatePicker({
        ndpEnglishInput: 'date-en'
    });
})
</script>
<script type="text/javascript">
    function get_report_status(){
        $('#report_status').prop('checked',false);
        var date_en = $('#date-en').val();
        var date_np = $('#date-np').val();
        if(date_en){
            $.post("<?php echo site_url()?>report_status/get_all_report_status",{date_en:date_en},function(data){
                $('.info-box-icon').removeClass('bg-aqua');
                $('.info-box-icon').addClass('bg-grey');
                $('.btn-pdf').html('');
                if(data.row > 0){
                    $.each( data['value'], function( key, value ) {
                        if(value['status'] == 1){
                            // alert(value);
                            console.log(value['department_id']);
                            $('#department-'+value['department_id']).removeClass('bg-grey');
                            $('#department-'+value['department_id']).addClass('bg-aqua');
                            $('#btn-'+value['department_id']).html("<a href='<?php echo site_url()?>home/pdf/2/"+date_np
                                +"/"+value['department_id']+"' class='btn btn-primary'>PDF</a>");
                        }
                    });
                }
            },'json');
        }else{
            alert('Select Date');
            $('#report_status').prop('checked',false);
        }
    }
</script>