<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <button class="btn btn-primary" id="add-btn">Add</button>
                    <table id="brand_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Bank Name</th>
                                <th>Contact No.</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $value) {?>
                            <tr>
                                <td><?php echo $value['bank_name']?></td>
                                <td><?php echo $value['phone']?></td>
                                <td><?php echo $value['email']?></td>
                                <td>
                                    <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger" onclick="delete_brand(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="bank-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="bank_name" class="form-control" id="bank_name" placeholder="Bank Name">
                                <span class="error_div" id="error_bank_name" hidden style="color: red">Name is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Contact Number</label>
                            <div class="col-sm-8">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="Contact Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="text" name="email" class="form-control" id="email" placeholder="Email">
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            // "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('input').val('');
        $('option').attr('selected', false);
        $('#form-submit').attr('disabled',false);
        $('#bank-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        $('#form-submit').attr('disabled',true);
        var name = $('#bank_name').val();
        if(!name){
            $('#error_bank_name').show();
        }else{
            $.post('<?php echo site_url("bank/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
        $('#form-submit').attr('disabled',false);
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $.post('<?php echo site_url("bank/get_value")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });
            $('#bank-dialog').modal();
        },'json');
        $('#form-submit').attr('disabled',false);
    }
</script>
<script type="text/javascript">
    function delete_brand(id){
        $.post('<?php echo site_url("bank/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>