<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<button class='btn btn-primary margin' id='add'>Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="diesel-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date</th>
				                    <th>R1</th>
				                    <th>R2</th>
				                    <th>R3</th>
				                    <th>Total</th>
                                    <th>Action</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo number_format($row['r1'],3)?></td>
				                        <td><?php echo number_format($row['r2'],3) ?></td>
				                        <td><?php echo number_format($row['r3'],3)?></td>
                                        <td><?php echo number_format($row['r1'] +$row['r2'] + $row['r3'],3)?></td>
                                        <td><button class='btn btn-warning' onclick='edit(<?php echo $row['id']?>)'>Edit</button> <button class='btn btn-danger' onclick='delete_record(<?php echo $row['id']?>)'>Delete</button></td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of diesel consumption-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="diesel-form" method="post">
      	<div class="modal-body">
                        
            <table class="table table-striped" cellpadding="3px" >
                <tr>
                    <td><label>Date</label></td>
                    <td id='date-div'>
                        <input type="text" id="date-np" name="date_np" class='form-control'>
                        <span id="error-date">Date is required</span>
                    </td>
                </tr>
                <tr>
                    <td><label>R1</label></td>
                    <td><input type="number" id="r1" name="r1" class='form-control'></td>
                </tr>
                <tr>
                    <td><label>R2</label></td>
                    <td><input type="number" id="r2" name="r2" class='form-control' ></td>
                </tr>
                <tr>
                    <td><label>R3</label></td>
                    <td><input type="number" id="r3" name="r3" class='form-control'></td>
                </tr>
                <!-- <tr>
                    <td><label>Total</label></td>
                    <td><input type="number" id="total" name="min" min="0" max="59" class='form-control'></td>
                </tr> -->
                 
                    <?php /*<input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>">*/?>
                
                    <input type="hidden" id="id" name="id"/>
                </table>
        
        </div>
    
        <div class="modal-footer">
            <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#date-np').nepaliDatePicker({
			// ndpEnglishInput: 'date_en',
			// onChange: function(){
			// 	var date = $('#date-np').val();
			// 	var date_en = $('#date-ep').val();
			// 	$.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
			// 		$('#display-div').html(data);
			// 	},'html');
			// }
		});
		$('#diesel-table').dataTable({
            order: [[ 0, 'desc' ]],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
        $('#error-date').hide();
        $('#date-div').removeClass('has-error');
		$('#myModalLabel').text('Electric Form');
                $('#date-np').val('');
                $('#date-en').val('');
                $('#r1').val('');
                $('#r2').val('');
                $('#r3').val('');
                // $('#min').val('');
                $('#id').val('');
                
		$('#form-dialog').modal();
	});
</script>
<script type="text/javascript">
	$('#save').click(function(){
        var date = $('#date-np').val();
        var i = 0; 
        if(date == ''){
            $('#date-div').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date-div').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(i == 1){
    		$.post("<?php echo site_url('electric_report/save')?>",$("#diesel-form").serializeArray(),function($data){
                if($data.success){
                    alert('Data saved');
                    location.reload();
                }
            },'json');
        }
	});
</script>
<script type="text/javascript">
    function edit(id){
        $('#myModalLabel').text('Electric Form');
        $('#error-date').hide();
        $('#date-div').removeClass('has-error');
        $.post('<?php echo site_url('electric_report/getData')?>',{id:id},function(data){
            $('#date-np').val(data.date_np);
            $('#date-en').val(data.date_en);
            $('#r1').val(data.r1);
            $('#r2').val(data.r2);
            $('#r3').val(data.r3);
            // $('#min').val();
            $('#id').val(id);
                
        },'json');
        $('#form-dialog').modal();
    }
</script>
<script type="text/javascript">
    function delete_record(id){
        $.post('<?php echo site_url('electric_report/delete_json')?>',{id:id},function(data){
            alert(data.msg);
            if(data.success){
                location.reload();
            }
        },'json');
    }
</script>

