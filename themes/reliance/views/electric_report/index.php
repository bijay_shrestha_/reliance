<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <button class="btn btn-primary" id="add-btn">Add</button>
                    <table id="brand_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Shift</th>
                                <th>Power Cut</th>
                                <th>Cement Mill Operation (NEA)</th>
                                <th>Cement Mill Operation (DG)</th>
                                <th>Only Lightining NEA</th>
                                <th>Breakdown</th>
                                <th>Reason</th>
                                <th>Total Hours</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $key => $value) {?>
                            <tr>
                                <td><?php echo $value['date_np']?></td>
                                <td><?php echo $value['shift']?></td>
                                <td><?php echo floor($value['power_cut'] / 60) . ':' . (($value['power_cut'] % 60) < 10 ? '0' : '' ) . ($value['power_cut'] % 60)?></td>
                                <td><?php echo floor($value['cement_mill_operation_nea'] / 60) . ':' . (($value['cement_mill_operation_nea'] % 60) < 10 ? '0' : '' ) . ($value['cement_mill_operation_nea'] % 60)?></td>
                                <td><?php echo floor($value['cement_mill_operation_dg'] / 60) . ':' . (($value['cement_mill_operation_dg'] % 60) < 10 ? '0' : '' ) . ($value['cement_mill_operation_dg'] % 60)?></td>
                                <!-- <td><?php echo $value['only_lighiting_nea']?></td> -->
                                <td><?php echo floor($value['only_lighiting_nea'] / 60) . ':' . (($value['only_lighiting_nea'] % 60) < 10 ? '0' : '' ) . ($value['only_lighiting_nea'] % 60)?></td>
                                <td><?php echo floor($value['breakdown'] / 60) . ':' . (($value['breakdown'] % 60) < 10 ? '0' : '' ) . ($value['breakdown'] % 60)?></td>
                                <td><?php echo $value['reason']?></td>
                                <td><?php echo floor($value['total'] / 60) . ':' . (($value['total'] % 60) < 10 ? '0' : '' ) . ($value['total'] % 60)?></td>
                                <td>
                                    <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger" onclick="delete_data(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="electric_report-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="date_np" class="form-control" id="date_np" placeholder="Date">
                                <input type="hidden" name="date_en" class="form-control" id="date_en" placeholder="Date">
                                <script>
                                    $('#date_np').change(function(){
                                          $('#date_en').val(BS2AD($('#date_np').val()));
                                     });
                                </script>
                                <div id="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Shift</label>
                            <div class="col-sm-8">
                                <select class="form-control" name='shift' id="shift">
                                    <option value="">-- select shift --</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                </select>
                                <div id="error_shift" hidden style="color: red" class="error-div">Shift is required</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Power Cut</label>
                            <div class="col-sm-4">
                                <select name="power_cut_hr" id="power_cut_hr" class="form-control" onchange="check_value('power_cut')">
                                    <option value=0>-- select hour --</option>
                                    <?php for ($i=1; $i <= 12 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="power_cut_min" id="power_cut_min" class="form-control" onchange="check_value('power_cut')">
                                    <option value=0>-- select min --</option>
                                    <?php for ($i=1; $i <= 60 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                                <div class="col-md-12"><div id="error_power_cut" hidden style="color: red" class="error-div">Must be less than 12</div></div>
                        </div>

                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Cement Mill Operation NEA</label>
                            <div class="col-sm-4">
                                <select name="cement_mill_operation_nea_hr" id="cement_mill_operation_nea_hr" class="form-control" onchange="check_value('cement_mill_operation_nea')">
                                    <option value=0>-- select hour --</option>
                                    <?php for ($i=1; $i <= 12 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="cement_mill_operation_nea_min" id="cement_mill_operation_nea_min" class="form-control" onchange="check_value('cement_mill_operation_nea')">
                                    <option value=0>-- select min --</option>
                                    <?php for ($i=1; $i <= 60 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <!-- <div class="col-sm-8">
                                <input type="number" name="cement_mill_operation_nea" id="cement_mill_operation_nea" class="form-control" onchange="check_value('cement_mill_operation_nea')" onkeyup="check_value('cement_mill_operation_nea')" placeholder="Cement Mill Operation NEA">
                            </div> -->
                                <div class="col-md-12"><div id="error_cement_mill_operation_nea" hidden style="color: red" class="error-div">Must be less than 12</div></div>
                        </div>

                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Cement Mill Operation DG</label>
                            <div class="col-sm-4">
                                <select name="cement_mill_operation_dg_hr" id="cement_mill_operation_dg_hr" class="form-control" onchange="check_value('cement_mill_operation_dg')">
                                    <option value=0>-- select hour --</option>
                                    <?php for ($i=1; $i <= 12 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="cement_mill_operation_dg_min" id="cement_mill_operation_dg_min" class="form-control" onchange="check_value('cement_mill_operation_dg')">
                                    <option value=0>-- select min --</option>
                                    <?php for ($i=1; $i < 60 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <!-- <div class="col-sm-8">
                                <input type="number" name="cement_mill_operation_dg" id="cement_mill_operation_dg" class="form-control" onchange="check_value('cement_mill_operation_dg')" onkeyup="check_value('cement_mill_operation_dg')" placeholder="Cement Mill Operation DG">
                            </div> -->
                            <div class="col-md-12"><div id="error_cement_mill_operation_dg" hidden style="color: red" class="error-div">Must be less than 12</div></div>
                        </div>

                        <div class="form-group">
                            <label for="only_lighiting_nea" class="col-sm-4 control-label">Only NEA Light</label>
                            <div class="col-sm-4">
                                <select name="only_lighiting_nea_hr" id="only_lighiting_nea_hr" class="form-control" onchange="check_value('only_lighiting_nea')">
                                    <option value=0>-- select hour --</option>
                                    <?php for ($i=1; $i <= 12 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="only_lighiting_nea_min" id="only_lighiting_nea_min" class="form-control" onchange="check_value('only_lighiting_nea')">
                                    <option value=0>-- select min --</option>
                                    <?php for ($i=1; $i <= 60 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <!-- <div class="col-sm-8">
                                <input type="number" name="only_lighiting_nea" id="only_lighiting_nea" class="form-control" onchange="check_value('only_lighiting_nea')" onkeyup="check_value('only_lighiting_nea')" placeholder="Only NEA Light">
                            </div> -->
                                <div class="col-md-12"><div id="error_only_lighting_nea" hidden style="color: red" class="error-div">Must be less than 12</div></div>
                        </div>

                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Breakdown</label>
                            <div class="col-sm-4">
                                <select name="breakdown_hr" id="breakdown_hr" class="form-control" onchange="check_value('breakdown')">
                                    <option value=0>-- select hour --</option>
                                    <?php for ($i=1; $i <= 12 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select name="breakdown_min" id="breakdown_min" class="form-control" onchange="check_value('breakdown')">
                                    <option value=0>-- select min --</option>
                                    <?php for ($i=1; $i <= 60 ; $i++) {?>
                                        <option><?php echo $i?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <!-- <div class="col-sm-8">
                                <input type="number" name="breakdown" id="breakdown" class="form-control" onchange="check_value('breakdown')" onkeyup="check_value('breakdown')" placeholder="Breakdown">
                            </div> -->
                            <div class="col-md-12"><div id="error_breakdown" hidden style="color: red" class="error-div">Must be less than 12</div></div>
                        </div>

                        <div class="form-group">
                            <label for="reason" class="col-sm-4 control-label">Reason</label>
                            <div class="col-sm-8">
                                <!-- <textarea></textarea name="reason" id="reason"> -->
                                    <textarea name="reason" id="reason" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                <div id="error_reason" hidden style="color: red" class="error-div">Must be less than 12</div>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="brand_type" class="col-sm-4 control-label">Total</label>
                            <div class="col-sm-8">
                                <input type="text" name="total_display" id="total_display" class="form-control" onchange="check_value('total')" onkeyup="check_value('total')" placeholder="Total" disabled>
                                <input type="hidden" name="total" id="total" class="form-control" onchange="check_value('total')" onkeyup="check_value('total')" placeholder="Total" readonly>
                                <div id="error_total" hidden style="color: red" class="error-div">Must be equal to 12</div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#brand_list').DataTable({
            "order":[[0, "desc"]],
            dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                    },
                ],
        });

        // nepali date picker
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('input').val('');
        $('.error-div').hide();
        $('option').attr('selected', false);
        $('#myModalLabel').html('Add <?php echo $header?>');
        $('#electric_report-dialog').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var date_np = $('#date_np').val();
        var shift = $('#shift').val();
        var i = 0;

        if(date_np == ''){
            i = 1;
            $('#error_date_np').show();
        }else{
            $('#error_date_np').hide();
        }

        if(shift == ''){
            i = 1;
            $('#error_shift').show();
        }else{
            $('#error_shift').hide();
        }
        
        if(i == 0){
            $.post('<?php echo site_url("electric_report/save")?>',$('#add-form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.error-div').hide();
        $.post('<?php echo site_url("electric_report/get_value")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
                // console.log(value);
            });
            var power_cut_hr = Math.floor(data.power_cut / 60);
            var power_cut_min = Math.floor(data.power_cut % 60);
            $('#power_cut_hr').val(power_cut_hr);
            $('#power_cut_min').val(power_cut_min);

            var breakdown_hr = Math.floor(data.breakdown / 60);
            var breakdown_min = Math.floor(data.breakdown % 60);
            $('#breakdown_hr').val(breakdown_hr);
            $('#breakdown_min').val(breakdown_min);

            var cement_mill_operation_dg_hr = Math.floor(data.cement_mill_operation_dg / 60);
            var cement_mill_operation_dg_min = Math.floor(data.cement_mill_operation_dg % 60);
            $('#cement_mill_operation_dg_hr').val(cement_mill_operation_dg_hr);
            $('#cement_mill_operation_dg_min').val(cement_mill_operation_dg_min);

            var cement_mill_operation_nea_hr = Math.floor(data.cement_mill_operation_nea / 60);
            var cement_mill_operation_nea_min = Math.floor(data.cement_mill_operation_nea % 60);
            $('#cement_mill_operation_nea_hr').val(cement_mill_operation_nea_hr);
            $('#cement_mill_operation_nea_min').val(cement_mill_operation_nea_min);

            var only_lighiting_nea_hr = Math.floor(data.only_lighiting_nea / 60);
            var only_lighiting_nea_min = Math.floor(data.only_lighiting_nea % 60);
            $('#only_lighiting_nea_hr').val(only_lighiting_nea_hr);
            $('#only_lighiting_nea_min').val(only_lighiting_nea_min);

            $('#total_display').val('12:00');

            $('#myModalLabel').html('Edit <?php echo $header?>');
            $('#electric_report-dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_data(id){
        $.post('<?php echo site_url("electric_report/delete_json")?>',{id:id},function(){
            alert('Data Deleted');
            location.reload();
        },'json');
    }
</script>
<script type="text/javascript">
    function check_value(){
        var power_cut_hr = parseFloat(($('#power_cut_hr').val())?$('#power_cut_hr').val():0);
        var power_cut_min = parseFloat(($('#power_cut_min').val())?$('#power_cut_min').val():0);
        var breakdown_hr = parseFloat(($('#breakdown_hr').val())?$('#breakdown_hr').val():0);
        var breakdown_min = parseFloat(($('#breakdown_min').val())?$('#breakdown_min').val():0);
        var cement_mill_operation_nea_hr = parseFloat(($('#cement_mill_operation_nea_hr').val())?$('#cement_mill_operation_nea_hr').val():0);
        var cement_mill_operation_nea_min = parseFloat(($('#cement_mill_operation_nea_min').val())?$('#cement_mill_operation_nea_min').val():0);
        var cement_mill_operation_dg_hr = parseFloat(($('#cement_mill_operation_dg_hr').val())?$('#cement_mill_operation_dg_hr').val():0);
        var cement_mill_operation_dg_min = parseFloat(($('#cement_mill_operation_dg_min').val())?$('#cement_mill_operation_dg_min').val():0);
        var only_lighiting_nea_hr = parseFloat(($('#only_lighiting_nea_hr').val())?$('#only_lighiting_nea_hr').val():0);
        var only_lighiting_nea_min = parseFloat(($('#only_lighiting_nea_min').val())?$('#only_lighiting_nea_min').val():0);
        var total_hr = power_cut_hr + breakdown_hr + cement_mill_operation_nea_hr + cement_mill_operation_dg_hr + only_lighiting_nea_hr;
        var total_min = parseFloat(power_cut_min + breakdown_min + cement_mill_operation_nea_min + cement_mill_operation_dg_min + only_lighiting_nea_min);
        var total = total_hr * 60 + total_min;
        // console.log(total_hr);
        // console.log(total_min);
        // console.log(only_lighiting_nea_min);
        var i = 0;
        if(power_cut_hr*60 + power_cut_min > 720){
            $('#error_power_cut').show();
            i = 1;
        }else{
            $('#error_power_cut').hide();
        }
        if(breakdown_hr*60 + breakdown_min > 720){
            $('#error_breakdown').show();
            i = 1;
        }else{
            $('#error_breakdown').hide();
        }
        if(cement_mill_operation_nea_hr*60 + cement_mill_operation_nea_min > 720){
            $('#error_cement_mill_operation_nea').show();
            i = 1;
        }else{
            $('#error_cement_mill_operation_nea').hide();
        }
        if(cement_mill_operation_dg_hr*60 + cement_mill_operation_dg_min > 720){
            $('#error_cement_mill_operation_dg').show();
            i = 1;
        }else{
            $('#error_cement_mill_operation_dg').hide();
        }
        if(only_lighiting_nea_hr*60 + only_lighiting_nea_min > 720){
            $('#error_only_lighiting_nea').show();
            i = 1;
        }else{
            $('#error_only_lighiting_nea').hide();
        }

        var total_display =  Math.floor(total / 60) + ':' + ((total % 60) < 10 ? '0' : '' ) + (total % 60)
        $('#total').val(total);
        $('#total_display').val(total_display);
        console.log(total_display);
        if(total == 720){
            $('#error_total').hide();
        }else{
            $('#error_total').show();
            i = 1;
        }
        if(i){
            $('#form-submit').prop('disabled',true);
        }else{
            $('#form-submit').prop('disabled',false);
        }
        
    }
</script>