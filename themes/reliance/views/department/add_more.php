<tr>

    <?php /*<td>
        <select name="department_id[]" id="department_id-<?php echo $i?>" onchange="clearMaterialUnitStockAdd(<?php echo $i?>)" class="form-control" required>
            <option value="" disabled selected>Department</option>
            <option value="6">Resin</option>
            <option value="2">Calendar</option>
        </select>
    </td>*/?>
    <input type='hidden' value='1' name="department_id[]" id="department_id-<?php echo $i?>">

    <td>
        <select class="form-control" id="material_id-<?php echo $i?>" name="material_id[]" onchange="clearUnitStockAdd(<?php echo $i?>)" required>
            <option value="" selected disabled>Material</option>
            <?php foreach($materials as $material):?>
                <option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
            <?php endforeach;?>
        </select>
    </td>

    <td>
        <input type="text" valaue="" id="unit-<?php echo $i?>" name="unit[]" class="form-control" placeholder="Unit" required disabled>
    </td>

    <td>
        <input type="text" value="" id="stock-<?php echo $i?>" name="stock[]" class="form-control" placeholder="Stock" required disabled>
    </td>

    <td>
        <input type="number" id="quantity-<?php echo $i?>" name="quantity[]" class="form-control" placeholder="Quantity" required>
    </td>

    <td>
        <input type="text" id="nepali_date-<?php echo $i?>" name="nepali_date[]" class="form-control" placeholder="Nepali Date" required>
        <input type="hidden" id="english_date-<?php echo $i?>" name="english_date[]" class="form-control" value="">
    </td>

    <td>
        <button type="button" class="btn btn-danger delete_row_add" id="delete_row-<?php echo $i?>" onclick="deleteRowAdd(<?php echo $i?>)"><i class="fa fa-trash"></i></button>
    </td>

</tr>

<script>
        var i = <?php echo $i?>;
        $('#nepali_date-'+i).nepaliDatePicker({
            ndpEnglishInput: 'english_date-'+i
        });
    
</script>

<!--  DELETE row  -->
<script type="text/javascript">
        function deleteRowAdd(i){
            var r = confirm('Are sure to remove this row ?');
            if(r == true){
                $('#delete_row-'+i).closest('tr').remove();
            }else{
                return false;
            }
        };
</script>


<!-- LOAD unit and current stock -->
<script>
    function clearUnitStockAdd(i){
        var material_id=$('#material_id-'+i).val();
        var department_id=$('#department_id-'+i).val();
        $('#unit-'+i).val('');
        $('#stock-'+i).val('');
        $.post('<?php echo site_url('department/get_unit')?>', {'material_id':material_id},function(return_unit){
            $('#unit-'+i).val(return_unit.unit);
        },"json");
        $.post('<?php echo site_url('department/get_stock')?>', {'material_id':material_id,'department_id':department_id},function(return_stock){
            if(return_stock.amount){
                $('#stock-'+i).val(return_stock.amount);
            }else{
                $('#stock-'+i).val(0);
            }
        },"json");
    };
</script>


<!-- CLEAR fields -->
<script>
    function clearMaterialUnitStockAdd(i){
        $('#unit-'+i).val('');
        $('#stock-'+i).val('');
        $('#material_id-'+i).val('');
    };
</script>