<style>
    #table-form td{
        min-width: 150px;
    }
</style>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">

<hr>

    <div class="row">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Place Request</a></li>
                <li><a href="#tab_2" data-toggle="tab">Store Approvals &nbsp&nbsp <small class="label pull-right bg-blue"><?php echo $store_approvals_no?></small></a></li>
                <li><a href="#tab_3" data-toggle="tab">Pending Approvals &nbsp&nbsp <small class="label pull-right bg-yellow"><?php echo $pending_no?></small></a></li>
                <li><a href="#tab_4" data-toggle="tab">Approved &nbsp&nbsp <?php /*<small class="label pull-right bg-green"><?php echo $approved_no?></small>*/?></a></li>
                <li><a href="#tab_5" data-toggle="tab">Disapproved &nbsp&nbsp <?php /*<small class="label pull-right bg-red"><?php echo $disapproved_no?></small>*/?></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">

                    <?php $i = 0?>

                    <form method="post" id="demand_form" action="#">
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover table-striped table-bordered table-condensed" id="table-form">
                                            <tr>
                                                <th style='min-width: 400px'>Material Name</th>
                                                <th style='min-width: 50px'>Unit Type</th>
                                                <th>Current Stock</th>
                                                <th>Requested Quantity</th>
                                                <th>Requested Date</th>
                                                <th>Delete Row</th>
                                            </tr>
                                            <tr>

                                                <input type='hidden' value='1' name="department_id[]" id="department_id-<?php echo $i?>">

                                                <td style='min-width: 400px'>
                                                    <select class="form-control" id="material_id-<?php echo $i?>" name="material_id[]" onchange="clearUnitStock(<?php echo $i?>)" required>
                                                        <option value="" selected disabled>Material</option>
                                                        <?php foreach($materials as $material):?>
                                                            <option value="<?php echo $material['material_id']?>"><?php echo $material['material_name']?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </td>

                                                <td style='min-width: 50px'>
                                                    <input type="text" value="" id="unit-<?php echo $i?>" name="unit[]" class="form-control" placeholder="Unit" required disabled>
                                                </td>

                                                <td>
                                                    <input type="text" value="" id="stock-<?php echo $i?>" name="stock[]" class="form-control" placeholder="Stock" required disabled>
                                                </td>

                                                <td>
                                                    <input type="number" id="quantity-<?php echo $i?>" name="quantity[]" class="form-control" placeholder="Quantity" required>
                                                </td>

                                                <td>
                                                    <input type="text" id="nepali_date-<?php echo $i?>" name="nepali_date[]" class="form-control nepali-date-picker" placeholder="Nepali Date" required>
                                                    <input type="hidden" id="english_date-<?php echo $i?>" name="english_date[]" class="form-control" value="">
                                                </td>

                                                <td>
                                                    <button type="button" class="btn btn-danger delete_row" id="delete_row-<?php echo $i?>" onclick="deleteRow(<?php echo $i?>)"><i class="fa fa-trash"></i></button>
                                                </td>

                                            </tr>

                                            </table>
                                        </div><!-- box-body close -->
                                    </div><!-- box close -->
                                </div><!-- col-xs-12 close -->
                        </div><!-- row close -->

                        <div>
                            <button type="button" id="add-row" class="btn btn-primary margin">Add</button>
                            <button type='button' id='submit-form' class='btn btn-primary margin'>Send</button>
                        </div><!-- send button close -->

                    </form><!-- form close -->
                </div><!-- /.tab-pane -->



                <div class="tab-pane" id="tab_2">
                    <div class="row" align="center">

                        <div class="box col-md-10">
                            <div class="box-body">
                                <table id="approvals" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Material Name</th>
                                        <th>Unit Type</th>
                                        <th>Requested Date</th>
                                        <th>Requested Quantity</th>
<!--                                        <th>Accepted Date</th>-->
<!--                                        <th>Accepted Quantity</th>-->
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($store_approvals as $store_approval) { ?>
                                        <tr>
                                            <td><?php echo $store_approval['request_id'] ?></td>
                                            <td><?php echo $store_approval['material_name'] ?></td>
                                            <td><?php echo $store_approval['unit'] ?></td>
                                            <td><?php echo $store_approval['request_np_date'] ?></td>
                                            <td><?php echo number_format($store_approval['request_quantity'],3) ?></td>
                                            <td>
                                            <button type='button' class='btn btn-warning' onclick='editRequest(<?php echo $store_approval["request_id"]?>)'><i class='fa fa-pencil'></i></button>
                                            <button type='button' class='btn btn-danger' onclick='deleteRequest(<?php echo $store_approval["request_id"]?>)'><i class='fa fa-trash'></i></button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.row -->

                </div><!-- /.tab-pane -->

                <div class="tab-pane" id="tab_3">
                    <div class="row" align="center">

                        <div class="box col-md-10">
                            <div class="box-body">
                                <table id="pending-table" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Material Name</th>
                                        <th>Unit Type</th>
                                        <th>Requested Date</th>
                                        <th>Requested Quantity</th>
                                        <th>Accepted Date</th>
                                        <th>Accepted Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($pending_requests as $pending_request) { ?>
                                        <tr>
                                            <td><?php echo $pending_request['request_id'] ?></td>
                                            <td><?php echo $pending_request['material_name'] ?></td>
                                            <td><?php echo $pending_request['unit'] ?></td>
                                            <td><?php echo $pending_request['request_np_date'] ?></td>
                                            <td><?php echo number_format($pending_request['request_quantity'],3) ?></td>
                                            <td><?php echo $pending_request['store_confirm_np_date'] ?></td>
                                            <td><?php echo number_format($pending_request['accepted_quantity'],3) ?></td>
                                            <td><button class="btn btn-success btn-xs approve-request" title="Approve" id="approve-<?php echo $pending_request['request_id'];?>" value="<?php echo $pending_request['request_id']?>"><i class="fa fa-fw fa-check"></i></button> | <button class="btn btn-danger btn-xs disapprove-request" title="Disapprove" id="disapprove-<?php echo $pending_request['request_id'];?>" value="<?php echo $pending_request['request_id']?>"><i class="fa fa-fw fa-close"></i></button></td>
                                            <input type="hidden" id="department-english-date" value="<?php echo date('Y-m-d')?>">
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.row -->

                </div><!-- /.tab-pane -->


                <div class="tab-pane" id="tab_4">
                    <div class="row" align="center">

                        <div class="box col-md-10">
                            <div class="box-body">
                                <table id="approved-table" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Material Name</th>
                                        <th>Unit Type</th>
                                        <th>Requested Date</th>
                                        <th>Requested Quantity</th>
                                        <th>Accepted Date</th>
                                        <th>Accepted Quantity</th>
                                        <th>Dept. Approved Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($approved_requests as $approved_request) { ?>
                                        <tr>
                                            <td><?php echo $approved_request['request_id'] ?></td>
                                            <td><?php echo $approved_request['material_name'] ?></td>
                                            <td><?php echo $approved_request['unit'] ?></td>
                                            <td><?php echo $approved_request['request_np_date'] ?></td>
                                            <td><?php echo number_format($approved_request['request_quantity'],3) ?></td>
                                            <td><?php echo $approved_request['store_confirm_np_date'] ?></td>
                                            <td><?php echo number_format($approved_request['accepted_quantity'],3) ?></td>
                                            <td><?php echo $approved_request['store_confirm_np_date'] ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.row -->

                </div><!-- /.tab-pane -->


                <div class="tab-pane" id="tab_5">
                    <div class="row" align="center">

                        <div class="box col-md-10">
                            <div class="box-body">
                                <table id="disapproved-table" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Material Name</th>
                                        <th>Unit Type</th>
                                        <th>Requested Date</th>
                                        <th>Requested Quantity</th>
                                        <th>Rejected Date</th>
                                        <?php /*<th>Dept. Disapproved Date</th>
                                        <th>Resend</th>*/?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($disapproved_requests as $disapproved_request) { ?>
                                        <tr>
                                            <td><?php echo $disapproved_request['request_id'] ?></td>
                                            <td><?php echo $disapproved_request['material_name'] ?></td>
                                            <td><?php echo $disapproved_request['unit'] ?></td>
                                            <td><?php echo $disapproved_request['request_np_date'] ?></td>
                                            <td><?php echo number_format($disapproved_request['request_quantity'],3) ?></td>
                                            <td><?php echo $disapproved_request['store_confirm_np_date'] ?></td>
                                            <?php /*<td><?php echo $disapproved_request['dept_confirm_np_date'] ?></td>
                                            <td><button class='btn btn-warning btn-sm' title='Resend'><i class='fa fa-fw fa-refresh'></i></button></td>*/?>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.row -->

                </div><!-- /.tab-pane -->


            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->

    </div>

</section>

<div class="modal fade" id="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="edit-request-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="material_name" class="col-sm-4 control-label">Name*</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="material-name-<?php echo $i?>" name="material_id" onchange="clearUnitStock(<?php echo $i?>)" required>
                                    <option value="" selected disabled>Material</option>
                                    <?php foreach($materials as $material):?>
                                        <option value="<?php echo $material['material_id']?>" id='material-id-<?php echo $material['material_id']?>'><?php echo $material['material_name']?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Name is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="requested_date" class="col-sm-4 control-label">Requested Date*</label>
                            <div class="col-sm-8">
                                <input type='text' name='edit_requested_date' id='edit-requested-date' class='form-control'>
                                <input type='hidden' name='edit_requested_en_date' id='edit-requested-en-date' class='form-control'>
                                <span class="error_message" id="error-category" style="color: red" hidden="true">Category is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="requested_date" class="col-sm-4 control-label">Requested Quantity*</label>
                            <div class="col-sm-8">
                                <input type='number' name='requested_qty' id='edit-requested-qty' class='form-control'>
                                <span class="error_message" id="error-category" style="color: red" hidden="true">Category is required</span>
                            </div>
                        </div>
                        <input type="hidden" name="request_id" id="request_id">


                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="edit-form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
$('.approve-request').click(function(){
    var r = confirm('Do you want to accept?');
    if(r){
        var dept_english_date = $('#department-english-date').val();
        var dept_nepali_date = AD2BS($('#department-english-date').val());
        var id = $(this).val();
        $.post('<?php echo site_url('dept_request/approve_request')?>', {'id':id, 'dept_english_date':dept_english_date, 'dept_nepali_date':dept_nepali_date}, function(data){
            if(data == true){
                location.reload();
            }
            else{
                alert('Action failed !! Please Try Again.');
            }
        },'json');
    }
});
</script>

<script>
    $('.disapprove-request').click(function(){
        var r = confirm('Do you want to reject?');
        if(r){
            var dept_english_date = $('#department-english-date').val();
            var dept_nepali_date = AD2BS($('#department-english-date').val());
            var id = $(this).val();
            $.post('<?php echo site_url('dept_request/disapprove_request')?>', {'id':id, 'dept_english_date':dept_english_date, 'dept_nepali_date':dept_nepali_date}, function(data){
                if(data == true){
                    location.reload();
                }
                else{
                    alert('Action failed !! Please Try Again.');
                }
            },'json');
        }
    });
</script>

<script>

    var i = 0;
    //  <!-- Send button -->
    $('#submit-form').click(function(){
        material_id = $('#material_id-'+i).val();
        request_qty = $('#quantity-'+i).val();
        date = $('#nepali_date-'+i).val();
        // alert(date);
        if(material_id == '' || request_qty == '' || date == '' ){
            alert('Please enter all values');
        }else{
            $.post('<?php echo site_url('dept_request/save_demand')?>',$("#demand_form").serializeArray(),function(data){
                if(data == true){
                    alert('Request sent successfully.');
                    $('#table-form').empty();
                    // window.location.href='<?php echo site_url('department')?>';
                    location.reload();
                }else{
                    alert('Sending failed.');
                }
        },'json');
        }
    });
</script>

<script>

//  <!-- ADD button -->
    $('#add-row').click(function(){
        material_id = $('#material_id-'+i).val();
        request_qty = $('#quantity-'+i).val();
        date = $('#nepali_date-'+i).val();
        if(material_id != '' && request_qty != '' && date != '' ){
            $.post('<?php echo site_url('department/add_row')?>',{index:++i},function(data){
                $('#table-form').append(data);
            },'html');
        }else{
            alert('Please enter all required fields');
        }
    });

</script>

<script>

//  <!-- DELETE row -->
    function deleteRow(i){
        var r = confirm('Are sure to remove this row ?');
        if(r == true){
            $('#delete_row-'+i).closest('tr').remove();
        }else{
            return false;
        }
    };

</script>

<script>

//  <!-- LOAD unit and current stock -->
    function clearUnitStock(i){
        var material_id=$('#material_id-'+i).val();
        var department_id= 1;//session id of department
        $('#unit-'+i).val('');
        $('#stock-'+i).val(0);
        $.post('<?php echo site_url('department/get_unit')?>', {'material_id':material_id},function(return_unit){
            $('#unit-'+i).val(return_unit.unit);
        },"json");
        $.post('<?php echo site_url('department/get_stock')?>', {'material_id':material_id,'department_id':department_id},function(return_stock){
            if(return_stock.amount){
                $('#stock-'+i).val(return_stock.amount);
            }else{
                $('#stock-'+i).val(0);
            }
        },"json");
    };

</script>

<script>
        //    <!-- CLEAR fields -->
    function clearMaterialUnitStock(i){
        $('#unit-'+i).val('');
        $('#stock-'+i).val('');
        $('#material_id-'+i).val('');
    };

</script>

<script>

//  Load DATA to the table
//    function show_Data() {
//        $("#approvals").dataTable({
//            "destroy": true,
//            "ajax": {
//                "url": "<?php //echo site_url('dept_request/showData')?>//",
//                "data": {
//                    "inquiry-date": $('#search-date').val()
//                },
//                "type": "post"
//            },
//            "columns": [
//                {"data": 'request_id'},
//                {"data": 'material_name'},
//                {"data": 'unit'},
//                {"data": 'request_np_date'},
//                {"data": 'request_quantity'},
//                {"data": 'store_confirm_np_date'},
//                {"data": 'accepted_quantity'},
//                {"data": ''} // this is "THIS"
//            ],
//            "columnDefs": [
//                {
//                    "targets": -1, // first from the right i.e. "THIS"
//                    "data": null, // nullify "THIS" | remove what ever exists in "THIS"
//                    "defaultContent": "<button class='btn btn-success btn-xs' title='Approve'><i class='fa fa-fw fa-check'></i></button> | <button class='btn btn-danger btn-xs' title='Disapprove'><i class='fa fa-fw fa-close'></i></button>"
//                }
//            ]
//        });
//    };

</script>


<script>
    $(function () {
        $('#pending-table, #approved-table, #disapproved-table, #approvals').dataTable({
            //"order" : [[0, 'desc']]
            "aaSorting": [ [3,'desc'], [0,'desc']],
            "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }]
        });

        $('#nepali_date-'+i).nepaliDatePicker({
            ndpEnglishInput: 'english_date-'+i
        });
        $('#edit-requested-date').nepaliDatePicker({
           ndpEnglishInput: 'edit-requested-en-date' 
        })
    });
</script>

<script type="text/javascript">
    function editRequest(id){
        $.post('<?php echo site_url("dept_request/getRequest")?>',{id:id},function(data){
            $('#myModalLabel').text('Edit Material');
            $(".error_message").hide();
            $('#material_name').val(data.material_name);
            $('#material-id-'+data.material_id).prop('selected',true);
            $('#request_id').val(data.request_id);
            $('#edit-requested-qty').val(data.request_quantity);
            $('#edit-requested-date').val(data.request_np_date);
            $('#edit-requested-en-date').val(data.request_en_date);
            $('#dialog').modal();
        },'json');
    }
    dialog
</script>

<script type="text/javascript">
    $('#edit-form-submit').click(function(){
        var r = confirm('Do you want to delete request');
        if(r){
            $.post('<?php echo site_url("dept_request/editRequest")?>',$('#edit-request-form').serializeArray(),function($success){
                if($success){
                    alert('Request updated');
                    location.reload();
                }else{
                    alert('Please try again');
                }

            },'json');
        }
    });
</script>

<script type="text/javascript">
    function deleteRequest(id){
        var r = confirm('Do you want to delete request');
        if(r){
            $.post('<?php echo site_url("dept_request/deleteRequest")?>',{id:id},function(data){
                if(data != 2){
                    alert(data);return false;
                    if(data){
                        alert('Request deleted');
                        location.reload();
                    }else{
                        alert('Please try again');
                    }
                }else{
                    alert('Store has approved the requested');
                    location.reload();
                }
            });
        }
    }
</script>

<script>
<!---->
<!--    //  Load ALL DATA to the table-->
//    function show_All_Data() {
//        $("#approvals").dataTable({
//            "destroy": true,
//            "ajax": {//
//                "url": "--><?php //echo site_url('dept_request/showAllData')?>//",
//                "type": "post"
//            },
//            "columns": [
//                {"data": 'request_id'},
//                {"data": 'material_name'},
//                {"data": 'unit'},
//                {"data": 'request_np_date'},
//                {"data": 'store_confirm_np_date'},
//                {"data": 'request_quantity'},
//                {"data": 'accepted_quantity'},
//                {"data": ''} // this is "THIS"
//            ],
//            "columnDefs":
//                if(data.department_status == 0){
//            [
//                {
//                    "targets": -1, // first from the right i.e. "THIS"
//                    "data": null, // nullify "THIS" | remove what ever exists in "THIS"
//                    "defaultContent": "<!--<button class='btn btn-success btn-xs' title='Approve'><i class='fa fa-fw fa-check'></i></button> | <button class='btn btn-danger btn-xs' title='Disapprove'><i class='fa fa-fw fa-close'></i></button>-->"
//                }
//            ]
//        } else {
//            [
//                {
//                    "targets": -1, // first from the right i.e. "THIS"
//                    "data": null, // nullify "THIS" | remove what ever exists in "THIS"
//                    "defaultContent": "<p>Approved</p>"
//                }
//            ]
//        }
//
//        });
//    };
//
</script>
