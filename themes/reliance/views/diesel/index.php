<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Start box) -->
    <div class="box">
	    <div class="box-body">
	    	<div class="row">
	    		<div class="col-md-3">
	    			<button class='btn btn-primary margin' id='add'>Add</button>
	    		</div>
	    		<div class="col-md-12">
	    			<div id="display-div">
	    				<table id="diesel-table" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                	<th>Date</th>
				                    <th>Department</th>
				                    <th>Amount</th>
				                    <th>Units</th>
				                    <th>Running Hour</th>
				                    <th>Running Minutes</th>
				                    <th>Action</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($rows as $row) { ?>
				                    <tr>
				                    	<td><?php echo $row['date_np']?></td>
				                        <td><?php echo $row['name'] ?></td>
				                        <td><?php echo number_format($row['amount'],3) ?></td>
				                        <td><?php echo number_format($row['unit'],3) ?></td>
				                        <td><?php echo $row['hour']?></td>
				                        <td><?php echo $row['min']?></td>
				                        <td>
				                        	<button class='btn btn-warning' onclick='edit(<?php echo $row['report_id']?>)'>Edit</button>
				                        	<button class='btn btn-danger' onclick='delete_report(<?php echo $row['report_id']?>)'>Delete</button>
				                        </td>
				                    </tr>
				                <?php } ?>
				            </tbody>
				        </table>
	    			</div>
	    		</div>
    		</div>
	    </div>
    </div>
</section>

<!--modal for form of diesel consumption-->

<div class="modal fade" id="form-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <form id="diesel-form" method="post">
      	<div class="modal-body">
                        
            <table class="table table-striped" cellpadding="3px" >
                <tr>
                    <td><label>Date</label></td>
                    <td id='date-div'>
                    	<input type="text" id="date-np" name="date_np" class='form-control'>
                    	<span id='error-date'>Date is required</span>
                	</td>
                    <td><input type="hidden" id="date-en" name="date_en" class='form-control'></td>
                </tr>
                <tr>
                    <td><label>Department</label></td>
                    <td id='department-div'>
                    	<select name="department_id" class='form-control' id="department-id">
                    		<option value="">
                    			--select department--
                    		</option>
                    		<?php foreach($departments as $department){?>
                    			<option value="<?php echo $department['department_id']?>"><?php echo $department['name']?></option>
                    		<?php }?>
                    	</select>
                    	<span id='error-department'>Department is required</span>
                	</td>
                    <!-- <input type="text" id="department" name="department_id" class='form-control'></td> -->
                </tr>
                <tr>
                    <td><label>Amount</label></td>
                    <td id='amount-div'>
                    	<input type="text" id="amount" name="amount" class='form-control'>
                    	<span id='error-amount'>Amount is required</span>
                	</td>
                </tr>
                <tr>
                    <td><label>Unit</label></td>
                    <td><input type="number" id="unit" name="unit" class='form-control'></td>
                </tr>
                <tr>
                    <td><label>Running Hour</label></td>
                    <td>
                        <select name="hour" style="max-height: 200px" class="form-control" id="hour">
                            <option value="">--select hour--</option>
                            <?php for($i = 0; $i < 24; $i++){?>}
                            <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php }?>
                        </select>
                        <!-- <input type="number" id="hour" name="hour" class='form-control'> -->
                    </td>
                </tr>
                <tr>
                    <td><label>Running Min</label></td>
                    <td>
                        <select name="min" style="max-height: 200px" class="form-control" id="min">
                            <option value="">--select hour--</option>
                            <?php for($i = 0; $i < 60; $i++){?>}
                            <option value="<?php echo $i?>"><?php echo $i?></option>
                            <?php }?>
                        </select>
                        <!-- <input type="number" id="min" name="min" min="0" max="59" class='form-control'> -->
                    </td>
                </tr>
                 
                    <?php /*<input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>">*/?>
                
                    <input type="hidden" id="id" name="id"/>
                </table>
        
        </div>
    
        <div class="modal-footer">
            <span id="edit_btn"></span>
          <button type="button" id='save' class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function(){
		$('#date-np').nepaliDatePicker({
			// ndpEnglishInput: 'date_en',
			// onChange: function(){
			// 	var date = $('#date-np').val();
			// 	var date_en = $('#date-ep').val();
			// 	$.post('<?php echo site_url('attendance/json')?>',{date:date,date_en:date_en},function(data){
			// 		$('#display-div').html(data);
			// 	},'html');
			// }
		});
		$('#diesel-table').dataTable({
			order: [0,'desc'],
			dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                },
            ],
		});
	});
</script>
<script type="text/javascript">
	$('#add').click(function(){
		$('#error-date, #error-amount, #error-department').hide();
		$('#date-div, #amount-div, #department-div').removeClass('has-error');

		$('#myModalLabel').text('Diesel Consumption Form');
                $('#date-np').val('');
                $('#date-en').val('');
                $('#amount').val('');
                $('#unit').val('');
                $('#department-id').val('');
                $('#hour').val('');
                $('#min').val('');
                $('#id').val('');
                
		$('#form-dialog').modal();
	});
</script>
<script type="text/javascript">
	$('#save').click(function(){
	var date = $('#date-np').val();
    var amount = $('#amount').val();
    var department = $('#department-id').val();
    var i = 0;

    	if(date == ''){
            $('#date-div').addClass('has-error');
            $('#error-date').show();
        }else{
            $('#date-div').removeClass('has-error');
            $('#error-date').hide();
            i++;
        }
        if(amount == ''){
            $('#amount-div').addClass('has-error');
            $('#error-amount').show();
        }else{
            $('#amount-div').removeClass('has-error');
            $('#error-amount').hide();
            i++;
        }
        if(department == ''){
            $('#department-div').addClass('has-error');
            $('#error-department').show();
        }else{
            $('#department-div').removeClass('has-error');
            $('#error-department').hide();
            i++;
        }
        if(i == 3){
				$.post("<?php echo site_url('diesel/save')?>",$("#diesel-form").serializeArray(),function(){
					location.reload();
				},'json');
        }
	});
</script>
<script type="text/javascript">
	$('#department-id').change(function(){
		var department_id = $(this).val();
		if(department_id == 11){
			$('#hour, #min, #unit').prop('disabled',true);
		}else{

			$('#hour, #min, #unit').prop('disabled',false);
		}
	});
</script>
<script type="text/javascript">
	function edit(id){
        $('#myModalLabel').text('Diesel Consumption Form');
		$('#error-date, #error-amount, #error-department').hide();
		$('#date-div, #amount-div, #department-div').removeClass('has-error');

		$.post('<?php echo site_url('diesel/getValue')?>',{id:id},function(data){
			$('#date-np').val(data.date_np);
			$('#date-en').val(data.date_en);
            $('#amount').val(data.amount);
            $('#unit').val(data.unit);
            $('#department-id').val(data.department_id);
            $('#hour').val(data.hour);
            $('#min').val(data.min);
            $('#id').val(data.id);
		},'json');
		$('#form-dialog').modal();
		// alert(id);
	}
</script>
<script type="text/javascript">
	function delete_report(id){
		$.post('<?php echo site_url('diesel/delete_json')?>',{id:id},function(data){
			alert(data.msg);
			if(data.success){
				location.reload();
			}
		},'json');
	}
</script>

