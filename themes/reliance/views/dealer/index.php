<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Dealer</a></li>
                <li><a href="#tab_2" data-toggle="tab">Add Item To Dealer</a></li>
                <li><a href="#tab_3" data-toggle="tab">Materials Suppliers</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <button id="add-dealer" class="btn btn-primary margin">Add</button>
                    <!-- dealer table -->
                    <?php if (count($dealers) > 0) { ?>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Dealer Table</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Dealer Name</th>
                                            <th>Contact Phone</th>
                                            <th>Mobile</th>
                                            <th>Address</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($dealers as $dealer) { ?>
                                            <tr>
                                                <td><?php echo $dealer['dealer_name'] ?></td>
                                                <td><?php echo $dealer['contact'] ?></td>
                                                <td><?php echo $dealer['mobile'] ?></td>
                                                <td><?php echo $dealer['dealer_address'] ?></td>
                                                <td><button type="button" class="btn btn-warning edit_dealer" id="dealer-<?php echo $dealer['dealer_id'] ?>" value="<?php echo $dealer['dealer_id'] ?>"> Edit</button></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    <?php } else { ?>
                        <div class="callout callout-warning">
                            <h4>No Dealer</h4>
                        </div>
                    <?php } ?>
                    <!-- dealer table end -->


                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <form id="material-dealer-form">
                        <div class="row">
                            <div class="col-md-10">
                                <select id="material-for-dealer" name="dealer_id" class="form-control">
                                    <option value="" id="select-option">-- select dealer --</option>
                                    <?php foreach ($dealers as $dealer) { ?>
                                        <option value="<?php echo $dealer['dealer_id'] ?>"><?php echo $dealer['dealer_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-1 margin">
                                <!--<button type="button" id="material-dealer-btn" class="btn btn-primary">Show</button>-->
                                <button type="button" id="material-dealer-save" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        <div class="row" id="material-list">
                            <?php 
                            $i = 0;
                            foreach ($materials as $material) { ?>
                                <div class = "col-md-3 col-sm-6 col-xs-6" >
                                    <input type = "checkbox" class="check_material" name = "material[]" value="<?php echo $material['material_id']?>" id="material-<?php echo $material['material_id']?>">
                                    <?php echo $material['material_name'] ?>
                                </div>
                            <?php 
                            $i++;} ?>
                        </div>
                    </form>
                </div><!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    <div class='row'>
                        <div class='col-md-10'>
                            <select id='material_id_for_dealer' class='form-control'>
                            <option value=''>-- select --</option>
                            <?php foreach($materials as $material){?>
                                <option value='<?php echo $material["material_id"]?>'><?php echo $material['material_name']?></option>
                            <?php }?>
                            </select>
                        </div>
                        <div class='col-md-12'>
                            <div id='dealer_material_table'>
                        </div>
                    </div>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
    </div>
</section>

<div class="modal fade" id="member-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="dealer-form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="dealer_name" class="col-sm-4 control-label">Name*</label>
                            <div class="col-sm-8">
                                <input type="text" name="dealer_name" class="form-control" id="dealer_name" placeholder="Dealer Name">
                                <span id="error-dealer-name" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dealer_address" class="col-sm-4 control-label">Address</label>
                            <div class="col-sm-8">
                                <input type="text" name="dealer_address" class="form-control" id="dealer_address" placeholder="Dealer Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dealer_landline" class="col-sm-4 control-label">Contact*</label>
                            <div class="col-sm-8">
                                <input type="text" name="contact" class="form-control" id="dealer_contact" placeholder="Contact">
                                <span id="error-contact" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dealer_mobile" class="col-sm-4 control-label">Mobile*</label>
                            <div class="col-sm-8">
                                <input type="text" name="mobile" class="form-control" id="dealer_mobile" placeholder="Mobile">
                                <span id="error-mobile" style="color: red"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="created_date_np" class="col-sm-4 control-label">Created Date(Nepali)</label>
                            <div class="col-sm-8">
                                <input type="text" name="created_date_np" class="form-control" id="created_date_np" placeholder="Created Date(Nepali)">
                            </div>
                        </div>
                        <?php /*<div class="form-group">
                            <label for="craeted_date_en" class="col-sm-4 control-label">Created Date(English)</label>
                            <div class="col-sm-8">
                                <input type="text" name="created_date_en" class="form-control" id="created_date_en" placeholder="Created Date(English)">
                            </div>
                        </div>*/?>
                        <input type="hidden" name="dealer_id" id="dealer_id" value="">
                        <input type="hidden" name="status" id="status" value="1">

<!--                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Status:</label> &nbsp;&nbsp;
                                    </div>
                                    <div class="col-md-8">
                                        <input type="radio" id="active" name="status" id="status1" value="1"> Active&nbsp;&nbsp;
                                        <input type="radio" id="inactive" name="status" id="status2" value="0"> Inactive
                                    </div>
                                </div>
                            </td>
                        </tr>-->

                        <?php /* <input type="hidden" id="agent_id" name="agent_id" value="<?php echo $this->session->userdata('id')?>"> */ ?>

                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="form-submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- DataTables -->
<script src="<?php echo theme_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $('#example2').DataTable();
        $('#created_date_np').nepaliDatePicker();
    });
</script>
<script>
    $("#form-submit").click(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('dealer/add') ?>",
            data: $("#dealer-form").serializeArray(),
            dataType: 'json',
            success: function (data) {
                if (!data.success) {
                    if($('#dealer_name').val() == ''){
                        $('#error-dealer-name').html(data.msg.dealer_name);
                    }else{
                        $('#error-dealer-name').html('');
                    }
                    if($('#dealer_contact').val() == ''){
                        $('#error-contact').html(data.msg.contact);
                    }else{
                        $('#error-contact').html('');
                    }
                    if($('#dealer_mobile').val() == ''){
                        $('#error-mobile').html(data.msg.mobile);
                    }else{
                        $('#error-mobile').html('');
                    }
                } else {
                    $('#error-dealer-name').html('');
                    $('#error-contact').html('');
                    $('#error-mobile').html('');
                    $('#dealer_name').val('');
                    $('#dealer_address').val('');
                    $('#email').val('');
                    $('#dealer_contact').val('');
                    $('#dealer_mobile').val('');
                    $('#created_date_np').val('');
                    $('#created_date_en').val('');
                    $('#status').val('');
                    $('#dealer_id').val('');
                    $('#member-dialog').modal('hide');
                    location.reload();
                }
            },
        });
    });
</script>
<script>
    $('#add-dealer').click(function () {
        $('#myModalLabel').text('Add Dealer');
        $('#error-dealer-name').html('');
        $('#error-contact').html('');
        $('#error-mobile').html('');
        $('#dealer_name').val('');
        $('#dealer_address').val('');
        $('#email').val('');
        $('#dealer_contact').val('');
        $('#dealer_mobile').val('');
        $('#created_date_np').val('');
        $('#created_date_en').val('');
        $('#status').val('');
        $('#dealer_id').val('');
        $('#member-dialog').modal();
    });
</script>
<script>
    $(".edit_dealer").click(function () {
        id = $(this).val();
        $.post('<?php echo site_url() ?>dealer/getDealer', {dealer_id: id}, function (data) {
            $('#myModalLabel').text('Edit Dealer');
            console.log(data.dealer_name);
            $('#error-dealer-name').html('');
            $('#error-contact').html('');
            $('#error-mobile').html('');
            $('#dealer_name').val(data.dealer_name);
            $('#dealer_address').val(data.dealer_address);
            $('#email').val(data.email);
            $('#dealer_contact').val(data.contact);
            $('#dealer_mobile').val(data.mobile);
            $('#created_date_np').val(data.created_date_np);
            $('#created_date_en').val(data.created_date_en);
            $('#status').val(data.status);
            $('#dealer_id').val(data.dealer_id);
            $('#member-dialog').modal();
        }, 'json');
    });
</script>
<script>
    $("#material-for-dealer").change(function () {
        var id = $(this).val();
        $('.check_material').removeAttr('checked');
        $.post('<?php echo site_url("material_dealer/getformmaterial") ?>', {id: id}, function (data) {
            $.each(data.result,function(index,value){
                $("#material-"+value.material_id).prop('checked',true).attr('checked', 'checked');
            });

        },'json');
    })
</script>
<script>
    $("#material-dealer-save").click(function(){
        $.post('<?php echo site_url()?>material_dealer/savemd',$("#material-dealer-form").serializeArray(),function(data){
            alert(data.msg);
            $('#select-option').prop('selected', true);
            $('.check_material').prop('checked',false);
        },'json')
    });
</script>
<script>
    $('#material_id_for_dealer').change(function(){
        var id = $(this).val();
        $.post('<?php echo site_url()?>material_dealer/getDealer',{id:id},function(data){
            if(data.length > 0){
                var table_data = '';
                console.log(data);
                $.each(data,function(index,value){
                    table_data = table_data+
                                        '<tr>'+'<td>'+value.dealer_name+'</td>'+
                                            '<td>'+value.contact+'</td>'+
                                            '<td>'+value.mobile+'</td>'+
                                            '<td>'+value.dealer_address+'</td>'+
                                        '</tr>';
                });
            $('#dealer_material_table').html('');
            $('#dealer_material_table').append('<div class="box">'+
                            '<div class="box-header">'+
                                '<h3 class="box-title">Dealer Table</h3>'+
                            '</div><!-- /.box-header -->'+
                            '<div class="box-body">'+
                                '<table id="example1" class="table table-bordered table-striped">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Dealer Name</th>'+
                                            '<th>Contact</th>'+
                                            '<th>Mobile</th>'+
                                            '<th>Address</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                            table_data+
                                    '</tbody>'+
                                '</table>'+
                            '</div><!-- /.box-body -->'+
                        '</div><!-- /.box -->');
            $('#example1').DataTable();
            }else{
            $('#dealer_material_table').html('');
                $('#dealer_material_table').append('<div class="callout callout-warning">'+
                '<h4>There No Dealer</h4>'+
              '</div>');
            }
        },'json')
    });
</script>