<!-- DataTables -->
<style type="text/css">
    td.details-control {
        background: url('<?php echo theme_url()?>plugins/datatables/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?php echo theme_url()?>plugins/datatables/images/details_close.png') no-repeat center center;
    }
</style>

<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
        <!-- lc table -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">LC Cost Table</h3>
                </div><!-- /.box-header -->
                <!-- <button id="add-lc" class="btn btn-primary margin">Add</button> -->
                <a href="<?php echo site_url('cost_sheet/excel')?>" id="excel" class="dt-button buttons-excel buttons-html5 margin">Excel</a>
                <div class="box-body">
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>LC no.</th>
                                <th>PP no.</th>
                                <th>Dealer</th>
                                <th>Total Cost</th>
                                <th>Bank</th>
                                <th>Status</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($rows as $row) { ?>
                            <!-- <pre><?php print_r($row)?></pre> -->
                                <tr>
                                    <td class="details-control"></td>
                                    <td><?php echo $row['dd_lc_no'] ?></td>
                                    <td><?php 
                                    foreach ($row['costsheets'] as $key => $value) {
                                        echo 'pp_no:'.$value['pp_no'].'('.$value['date_np'].')<br>';
                                    }
                                    // echo $row['dd_lc_no'] 
                                    ?></td>
                                    <td><?php echo $row['dealer_name'] ?></td>
                                    <td><?php echo number_format($row['total_lc_value'],3) ?></td>
                                    <td><?php echo $row['bank_name'] ?></td>
                                    <td><?php echo ($row['closed'])?'Closed':'Open'?></td>
                                    <!-- <td> -->
                                        <!-- <button type="button" class="btn btn-primary view-lc" id="lc-<?php echo $row['id']?>" value="<?php echo $row['id']?>">View Detail</button> -->
                                        <!-- <button type="button" class="btn btn-primary add-lc-cost" id="add-lc-cost" value="<?php echo $row['id']?>">Add Costing</button> -->
                                        <!-- <button type="button" class="btn btn-danger delete-lc" id="delete-lc-<?php echo $row['id']?>" value="<?php echo $row['id']?>">Remove Data</button> -->
                                    <!-- </td> -->
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        <!-- dealer table end -->
                    
</section>

<div class="modal fade" id="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="width: 1200px; margin-left: -150px ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="lc_no" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-4">
                                <input type="text" name="date_np" id="date_np" class="form-control" placeholder="Nepali Date">
                                <span class="error_message" id="error-date" style="color: red" hidden="true">Please select the unit</span>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="date_en" class="form-control" id="date_en" placeholder="English Date">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lc_no" class="col-sm-2 control-label">LC Number</label>
                            <div class="col-sm-4">
                                <input type="text" name="lc_name" id="lc_name" class="form-control" disabled>
                                <input type="hidden" name="lc_id" id="lc_id" class="form-control">
                                <span class="error_message" id="error-unit" style="color: red" hidden="true">Please select the unit</span>
                            </div>
                            <label for="pp_number" class="col-sm-2 control-label">PP#</label>
                            <div class="col-sm-4">
                                <input type="text" name="pp_no" class="form-control" id="pp_no" placeholder="PP Number">
                                <span class="error_message" id="error-pp_no" style="color: red" hidden="true">PP number is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lc_no" class="col-sm-2 control-label">Currency</label>
                            <div class="col-sm-4">
                                <input type="text" name="currency" id="currency" class="form-control" disabled="">
                                <span class="error_message" id="error-unit" style="color: red" hidden="true">Please select the unit</span>
                            </div>
                            <label for="pp_number" class="col-sm-2 control-label">Currency Exchange Rate</label>
                            <div class="col-sm-4">
                                <input type="number" name="currency_exchange_rate" class="form-control" id="currency_exchange_rate" placeholder="Currency Exchange Rate">
                                <span class="error_message" id="error-rate" style="color: red" hidden="true">Rate is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class='col-md-2' style="text-align: center;"><label>Material</label></div>
                            <div class='col-md-2' style="text-align: center;"><label>Quantity</label></div>
                            <div class='col-md-2' style="text-align: center;"><label>Unit Price</label></div>
                            <div class='col-md-2' style="text-align: center;"><label>Total Cost</label></div>
                            <div class='col-md-2' style="text-align: center;"><label>Duty Amount</label></div>
                            <div class='col-md-2' style="text-align: center;"><label>EXCISE Amount</label></div>
                            <div id="material-input"></div>
                        </div>
                        <div class="form-group">
                            <label for="pp_number" class="col-sm-2 control-label">Bank Charge</label>
                            <div class="col-sm-4">
                                <input type="number" name="bank_charge" class="form-control" id="bank_charge" placeholder="Bank Charge">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">PP number is required</span>
                            </div>
                            <label for="bank" class="col-sm-2 control-label">Insurance</label>
                            <div class="col-sm-4">
                                <input type="number" name="insurance" class="form-control" id="insurance" placeholder="Insurance">
                                <span class="error_message" id="error-insurance" style="color: red" hidden="true">Please select the bank</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="currency" class="col-sm-2 control-label">Transport Charge</label>
                            <div class="col-sm-4">
                                <input type="number" name="transport_charge" class="form-control" id="transport_charge" placeholder="Transport Charge">
                                <span class="error_message" id="error-transport_charge" style="color: red" hidden="true">Transport Charge is required</span>
                            </div>
                            <label for="bank" class="col-sm-2 control-label">Local Transport Charge</label>
                            <div class="col-sm-4">
                                <input type="number" name="local_transport" class="form-control" id="local_transport" placeholder="Local Transport Charge">
                                <span class="error_message" id="error-insurance" style="color: red" hidden="true">Please select the bank</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dd_lc_no" class="col-sm-2 control-label">Custom Fee</label>
                            <div class="col-sm-4">
                                <input type="number" name="custom_fee" class="form-control" id="custom_fee" placeholder="Custom Fee">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Custom fee is required</span>
                            </div>
                            <label for="additional_charge" class="col-sm-2 control-label">Agency Fee</label>
                            <div class="col-sm-4">
                                <input type="number" name="agency_fee" class="form-control" id="agency_fee" placeholder="Agency Fee">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="csf" class="col-sm-2 control-label">CSF</label>
                            <div class="col-sm-4">
                                <input type="number" name="csf" class="form-control" id="csf" placeholder="CSF">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Custom fee is required</span>
                            </div>
                            <!-- <label for="freight" class="col-sm-2 control-label">Freight</label>
                            <div class="col-sm-4">
                                <input type="number" name="freight" class="form-control" id="freight" placeholder="Freight">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Custom fee is required</span>
                            </div> -->
                            <label for="fee_fine" class="col-sm-2 control-label">Fee/Fine</label>
                            <div class="col-sm-4">
                                <input type="number" name="fee_fine" class="form-control" id="fee_fine" placeholder="Fee/Fine">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total_lc_value" class="col-sm-2 control-label">Dry Port Fee</label>
                            <div class="col-sm-4">
                                <input type="number" name="dry_port_fee" class="form-control" id="dry_port_fee" placeholder="Dry Port Fee">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Total LC Value is required</span>
                            </div>
                            <label for="total_lc_value" class="col-sm-2 control-label">Additional Fee</label>
                            <div class="col-sm-4">
                                <input type="number" name="addition_fee" class="form-control" id="addition_fee" placeholder="Additional Fee">
                                <span class="error_message" id="error-name" style="color: red" hidden="true">Total LC Value is required</span>
                            </div>
                        </div>
                        <div class="form-group">
                            
                        </div>

                        <input type="hidden" id="id" name="id"/>
                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" id="save" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
        $('#date_np').change(function(){
            $('#date_en').val(BS2AD($('#date_np').val()));
        });
        $('#date_en').datetimepicker({
            format: 'YYYY-MM-DD'
        }).on('dp.change', function (e) {
                $('#date_np').val(AD2BS($('#date_en').val()));
        });
        var table = $('#table').DataTable({
            "order": [[ 1, 'desc' ]]
        });
        
        // Add event listener for opening and closing details
        $('#table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
     
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                var i = row.data()
                // console.log(i[1]);
                $.post('<?php echo site_url("cost_sheet/getAllValue")?>',{'id':i[1]},function(data){
                    row.child( '<div class="row"><div class="col-md-12"><button type="button" class="btn btn-primary add-lc-cost" id="add-lc-cost" value="'+i[1]+'" onclick="addModal(\''+i[1]+'\')">Add Costing</button></div></div>'+data ).show();
                    tr.addClass('shown');
                },'html');
            }
        } );
    })
</script>
<script type="text/javascript">
    /* Formatting function for row details - modify as you need */
    function format ( d ) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
                '<td>Full name:</td>'+
                '<td>'+d.name+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extension number:</td>'+
                '<td>'+d.extn+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>Extra info:</td>'+
                '<td>And any further details here (images etc)...</td>'+
            '</tr>'+
        '</table>';
    }

    $('.edit-lc').click(function(){
        $('#myModalLabel').html('Cost Sheet form');
        $('#error-rate').hide();
        $('#error-pp_no').hide();
        $('#form').trigger("reset");
        var id = $(this).val();
        $.post('<?php echo site_url("cost_sheet/getValue")?>',{id:id},function(data){
            $('#lc_id').val(data.lc.id);
            $('#lc_name').val(data.lc.dd_lc_no);
            if(data.lc_cost){
                $.each(data.lc_cost, function( index, value ) {
                    $('#'+index).val(value);
                });
            }
            $('#dialog').modal();
        },'json');

    });

    function addModal(id){
        // alert(id);
        $('#myModalLabel').html('Cost Sheet form');
        $('#error-rate').hide();
        $('#error-pp_no').hide();
        $('#form').trigger("reset");
        // var id = $(this).val();
        $.post('<?php echo site_url("cost_sheet/getValue")?>',{id:id,index:'lcs.dd_lc_no'},function(data){
            // alert(data.lc.lcid);
            $('#lc_id').val(data.lc.lcid);
            $('#lc_name').val(data.lc.dd_lc_no);
            $('#currency').val(data.lc.currency);
            // if(data.materials){
                    // alert('here');
            $('#material-input').html('');
                i = 0;
                $.each(data.materials, function( index, value ) {
                    var row = '<div class="form-group" style="padding-left:10px;padding;padding-right:10px;padding">'+
                            '<div class="col-md-2"><input type="text" class="form-control" name="material_name[]" disabled value="'+value.material_name+'"><input type="hidden" class="form-control" name="material_id[]"  value="'+value.material_id+'"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="quantity[]" onchange="total_price('+i+')" onkeyup="total_price('+i+')" value="'+value.quantity+'" id="quantity-'+i+'"></div>'+
                            '<div class="col-md-2"><input type="text" class="form-control" disabled value="'+value.price+'" id="price-'+i+'"></div><input type="hidden" class="form-control" name="price[]" value="'+value.price+'" >'+
                            '<div class="col-md-2"><input type="text" class="form-control" name="totalprice[]" disabled value="'+value.price * value.quantity +'" id="total-price-'+i+'"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="duty_amount[]"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="excise[]"></div>'+
                        '</div>'
                    $('#material-input').append(row);
                    i++;
                });
            // }
            $('#dialog').modal();
        },'json');
    }
    $('.add-lc-cost').click(function(){
        $('#myModalLabel').html('Cost Sheet form')

        $('#form').trigger("reset");
        var id = $(this).val();
        // alert(id);
        $.post('<?php echo site_url("cost_sheet/getValue")?>',{id:id,index:'lcs.id'},function(data){
            $('#lc_id').val(id);
            $('#lc_name').val(data.lc.dd_lc_no);
            $('#currency').val(data.lc.currency);
            // if(data.materials){
                    // alert('here');
            $('#material-input').html('');
                i = 0;
                $.each(data.materials, function( index, value ) {
                    var row = '<div class="form-group" style="padding-left:10px;padding;padding-right:10px;padding">'+
                            '<div class="col-md-2"><input type="text" class="form-control" name="material_name[]" disabled value="'+value.material_name+'"><input type="hidden" class="form-control" name="material_id[]"  value="'+value.material_id+'"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="quantity[]" onchange="total_price('+i+')" onkeyup="total_price('+i+')" value="'+value.quantity+'" id="quantity-'+i+'"></div>'+
                            '<div class="col-md-2"><input type="text" class="form-control" disabled value="'+value.price+'" id="price-'+i+'"></div><input type="hidden" class="form-control" name="price[]" value="'+value.price+'" >'+
                            '<div class="col-md-2"><input type="text" class="form-control" name="totalprice[]" disabled value="'+value.price * value.quantity +'" id="total-price-'+i+'"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="duty_amount[]"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="excise[]"></div>'+
                        '</div>'
                    $('#material-input').append(row);
                    i++;
                });
            // }
            $('#dialog').modal();
        },'json');

    });
</script>
<script type="text/javascript">
    $('#save').click(function(){
        var pp_no = $('#pp_no').val();
        var rate = $('#currency_exchange_rate').val();
        if(pp_no != '' && rate != ''){
            $.post('<?php echo site_url("cost_sheet/save")?>',$('#form').serializeArray(),function(data){
                if(data.success){
                    location.reload();
                }else{
                    alert('Error occured');
                }
            },'json');
        }else{
            if(pp_no == ''){
                $('#error-pp_no').show();
            }else{
                $('#error-pp_no').hide();
            }
            if(rate == ''){
                $('#error-rate').show();
            }else{
                $('#error-rate').hide();
            }
        }
    })
</script>
<script type="text/javascript">
    $('.delete-lc').click(function(){
        id = $(this).val();
        if(confirm('do you want to remove data?')){
            $.post('<?php echo site_url("cost_sheet/delete_json")?>',{id:id},function(data){
                if(data.success){
                    alert('Data removed');
                    location.reload();
                }else{
                    alert('Error occured');
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function edit(id){
        $('#myModalLabel').html('Cost Sheet form')
        $('.error_message').hide();
        $('#form').trigger("reset");
        $.post('<?php echo site_url("cost_sheet/getValue")?>',{id:id,index:'pp_no'},function(data){
            $('#lc_id').val(data.lc.lcid);
            $('#lc_name').val(data.lc.dd_lc_no);
            $('#currency').val(data.lc.currency);
            if(data.lc_cost){
                $.each(data.lc_cost, function( index, value ) {
                    $('#'+index).val(value);
                });
            }
             $('#material-input').html('');
                i = 0;
                $.each(data.materials, function( index, value ) {
                    console.log(value);
                    var row = '<div class="form-group" style="padding-left:10px;padding;padding-right:10px;padding">'+
                            '<div class="col-md-2"><input type="text" class="form-control" name="material_name[]" disabled value="'+value.material_name+'"><input type="hidden" class="form-control" name="material_id[]"  value="'+value.material_id+'"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="quantity[]" onchange="total_price('+i+')" onkeyup="total_price('+i+')" value="'+data.material_detail[value.material_id]['quantity']+'" id="quantity-'+i+'"></div>'+
                            '<div class="col-md-2"><input type="text" class="form-control" disabled value="'+value.price+'"  id="price-'+i+'"></div><input type="hidden" class="form-control" name="price[]" value="'+value.price+'">'+
                            '<div class="col-md-2"><input type="text" class="form-control" name="totalprice[]" disabled value="'+value.price * data.material_detail[value.material_id]['quantity'] +'" id="total-price-'+i+'"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="duty_amount[]" value="'+data.material_detail[value.material_id]['duty']+'"></div>'+
                            '<div class="col-md-2"><input type="number" class="form-control" name="excise[]" value="'+data.material_detail[value.material_id]['excise']+'"></div>'+
                        '</div>'
                    $('#material-input').append(row);
                    i++;
                });
            $('#dialog').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function deleteCost(id) {
        if(confirm('Do you want to delete this record?')){
            $.post('<?php echo site_url("cost_sheet/delete_json")?>',{id:id},function(data){
                if(data.success){
                    location.reload();
                }else{
                    alert('Error occured');
                }
            },'json');
        }
    }
</script>
<script type="text/javascript">
    function total_price(i) {
        var qty = $('#quantity-'+i).val()
        var price = $('#price-'+i).val()
        // alert(price);
        $('#total-price-'+i).val(qty * price);
    }
</script>