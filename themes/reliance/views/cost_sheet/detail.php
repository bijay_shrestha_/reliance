<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content">
    <!-- Small boxes (Stat box) -->
        <!-- lc table -->
        <?php foreach ($rows as $i => $row) { ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">PP No.: <?php echo $row['pp_no']?></h3>
                    <h5>Date: <?php echo $row['cost_date_np']?></h5>
                    <button type="button" class="btn btn-danger pull-right" id="delete-lc-<?php echo $row['id']?>" value="<?php echo $row['id']?>" style="margin-left: 10px" onclick="deleteCost(<?php echo $row['cost_id']?>)">Delete</button>
                    <button type="button" class="btn btn-warning pull-right" id="add-lc-cost" value="<?php echo $row['id']?>" style="margin-left: 10px" onclick="edit(<?php echo $row['pp_no']?>)">Edit</button>
                </div><!-- /.box-header -->
                <!-- <button id="add-lc" class="btn btn-primary margin">Add</button> -->
                <div class="box-body">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-8"><label>Currency</label></div>
                        <div class="col-md-4"><?php echo $row['currency']?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Exchange Rate</label></div>
                        <div class="col-md-4"><?php echo number_format($row['currency_exchange_rate'],3)?></div>
                    </div>

                    <div class="row">
                        <div class="col-md-8"><label>Total Duty Amount</label></div>
                        <div class="col-md-4"><?php echo number_format($row['total_duty'],3)?></div>
                    </div>

                    <div class="row">
                        <div class="col-md-8"><label>Total Excise</label></div>
                        <div class="col-md-4"><?php echo number_format($row['total_excise'],3)?></div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-8"><label>Duty Exchange Rate</label></div>
                        <div class="col-md-4"><?php echo number_format($row['duty_exchange_rate'],3)?></div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-8"><label>Bank Charge</label></div>
                        <div class="col-md-4"><?php echo number_format($row['bank_charge'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Insurance</label></div>
                        <div class="col-md-4"><?php echo number_format($row['insurance'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Transport Charge</label></div>
                        <div class="col-md-4"><?php echo number_format($row['transport_charge'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Local Transport Charge</label></div>
                        <div class="col-md-4"><?php echo number_format($row['local_transport'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Custom Fee</label></div>
                        <div class="col-md-4"><?php echo number_format($row['custom_fee'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Agency Fee</label></div>
                        <div class="col-md-4"><?php echo number_format($row['agency_fee'],3)?></div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-8"><label>Freight</label></div>
                        <div class="col-md-4"><?php echo number_format($row['freight'],3)?></div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-8"><label>Fee/Fine</label></div>
                        <div class="col-md-4"><?php echo number_format($row['fee_fine'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Dry Port Fee</label></div>
                        <div class="col-md-4"><?php echo number_format($row['dry_port_fee'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>CSF</label></div>
                        <div class="col-md-4"><?php echo number_format($row['csf'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Additional Fee</label></div>
                        <div class="col-md-4"><?php echo number_format($row['addition_fee'],3)?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8"><label>Total Additional Cost</label></div>
                        <?php $total_additional = $row['total_duty'] + $row['total_excise'] + $row['bank_charge'] + $row['insurance'] + $row['transport_charge'] + $row['local_transport'] + $row['custom_fee'] + $row['agency_fee'] + $row['freight'] + $row['fee_fine'] + $row['dry_port_fee'] + $row['csf'] + $row['addition_fee'];?>
                        <div class="col-md-4"><?php echo number_format($total_additional,3)?></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Material Name</th>
                                <th>Quantity</th>
                                <th>Per Unit Cost</th>
                                <th>Price</th>
                                <th>Price NRP</th>
                                <th>Total Landed Price</th>
                                <th>Per KG Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($material[$i] as $key => $value) {?>
                                <tr>
                                    <td><?php echo $value['material_name'] ?></td>
                                    <td><?php echo number_format($value['quantity'],3) ?></td>
                                    <td><?php echo number_format($value['price'],3) ?></td>
                                    <td><?php echo number_format($value['quantity'] * $value['price'],3) ?></td>
                                    <td><?php echo number_format($value['quantity'] * $value['price'] * $row['currency_exchange_rate'],3) ?></td>
                                    <!-- <td><?php echo number_format($total = $value['quantity']*$value['price']*$row['currency_exchange_rate']+$value['duty']+$value['excise'],3) ?></td> -->
                                    <!-- <?php $total = $row['total_charge']/$row['quantity'] * $value['quantity']?> -->
                                    <?php $total = $row['total_charge']/$row['bill_amt'] * $value['quantity'] * $value['price'] * $row['currency_exchange_rate']?>
                                    <td><?php echo number_format($total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise'],3) ?></td>
                                    <td><?php echo number_format(($total + $value['quantity']*$value['price']*$row['currency_exchange_rate'] + $value['duty'] + $value['excise']) / $value['quantity'],3) ?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
                    <hr>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        <?php } ?>
        <!-- dealer table end -->
                    
</section>
