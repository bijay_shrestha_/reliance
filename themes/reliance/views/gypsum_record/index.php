<!-- DataTables -->
<link rel="stylesheet" href="<?php echo theme_url() ?>plugins/datatables/dataTables.bootstrap.css">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $header?> Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <button class="btn btn-primary" id="add-btn"  style="float: right;">Add</button>
                    </br>
                </br>
            </div>
            <table id="gypsum_record" class="table table-bordered table-striped">
                <thead>
                    <tr>
                     <?php foreach ($rows[0] as $key => $value):?>
                        <th><?php echo $key ?></th>
                    <?php endforeach; ?>
                    <th>Action</th>
                </thead>
                <tbody>
                    <?php foreach ($rows as $key => $value):?>
                        <tr>
                            <?php foreach ($value as $k => $v):?>
                                <td><?php echo $v; ?></td>
                            <?php endforeach; ?>
                            <td>
                                <button class="btn btn-warning" onclick="form_edit(<?php echo ($value['id'])?>)"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger" onclick="delete_data(<?php echo ($value['id'])?>)"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</section>

<!-- modal to add brand -->
<div class="modal fade" id="add_gypsum_records" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form class="form-horizontal" id="add-gypsum_record_form" action="#">
                <div class="modal-body">
                    <table class="table table-striped" cellpadding="3px" >
                        <div class="form-group">
                            <label for="brand" class="col-sm-4 control-label">Date of Sample</label>
                            <div class="col-sm-8">
                                <input type="text" name="sample_date_np" class="form-control" id="sample_date_np" placeholder="Sample Date">
                                <input type="hidden" name="sample_date_en" class="form-control" id="sample_date_en" placeholder="Sample Date">
                                <script>
                                    $('#sample_date_np').change(function(){
                                      $('#sample_date_en').val(BS2AD($('#sample_date_np').val()));
                                  });
                              </script>
                              <div class="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                          </div>
                      </div>
                      <div class="form-group">
                        <label for="brand" class="col-sm-4 control-label">Date of Testing</label>
                        <div class="col-sm-8">
                            <input type="text" name="testing_date_np" class="form-control" id="testing_date_np" placeholder="Testing Date">
                            <input type="hidden" name="testing_date_en" class="form-control" id="testing_date_en" placeholder="Testing Date">
                            <script>
                                $('#testing_date_np').change(function(){
                                  $('#testing_date_en').val(BS2AD($('#testing_date_np').val()));
                              });
                          </script>
                          <div class="error_date_np" hidden style="color: red" class="error-div">Date is required</div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="company" class="col-sm-4 control-label">Company</label>
                    <div class="col-sm-8">
                        <select class="form-control" name='company_id' id="company_id">
                            <option value="">-- Select company --</option>
                            <?php foreach ($company as $key => $value) {?>
                            <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                            <?php }?>
                        </select>
                        <div id="error_company_id" hidden style="color: red" class="error-div">Company is required</div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="supplier" class="col-sm-4 control-label">Supplier</label>
                    <div class="col-sm-8">
                        <select class="form-control" name='supplier_id' id="supplier_id">
                            <option value="">-- Select supplier --</option>
                            <?php foreach ($suppliers as $key => $value) {?>
                            <option value="<?php echo $value['id']?>"><?php echo $value['name']?></option>
                            <?php }?>
                        </select>
                        <div id="error_supplier_id" hidden style="color: red" class="error-div">Supplier is required</div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="moisture" class="col-sm-4 control-label">Mositure</label>
                    <div class="col-sm-8">
                        <input type="text" name="moisture" id="moisture" class="form-control" placeholder="Moisture">
                    </div>
                    <div id="error_moisture" hidden style="color: red" class="error-div">Moisture</div>
                </div>
                <?php foreach ($input_fields as $key => $value) : ?>
                    <div class="form-group">
                        <label for="{$value['chemical_name']" class="col-sm-4 control-label"><?php echo $value['chemical_name'];?></label>
                        <div class="col-sm-8">
                            <input type="text" name="<?php echo $value['chemical_id'];?>" id="<?php echo $value['chemical_name'];?>" class="form-control" placeholder="<?php echo $value['chemical_name'];?>">
                        </div>
                        <div id="error_ir" hidden style="color: red" class="error-div"><?php echo $value['chemical_name'];?> is required</div>
                    </div>
                <?php endforeach; ?>
                <div class="form-group">
                    <label for="purity" class="col-sm-4 control-label">Purity</label>
                    <div class="col-sm-8">
                        <input type="text" name="purity" id="purity" class="form-control" placeholder="Purity">
                    </div>
                    <div id="error_purity" hidden style="color: red" class="error-div">Purity is required</div>
                </div>
                <div class="form-group">
                    <label for="remarks" class="col-sm-4 control-label">Remarks</label>
                    <div class="col-sm-8">
                        <input type="textarea" name="remarks" id="remarks" class="form-control" placeholder="Remarks">
                    </div>
                </div>                        
                <input type="hidden" name="id" id="id" value="">
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" id="form-submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </form>
</div>
</div>
</div>

<script type="text/javascript">
    $(function(){
        $('#gypsum_record').DataTable({
            "order":[[0, "desc"]],
            dom: 'Bfrtip',
            buttons: [
            {
                extend: 'excelHtml5',
            },
            ],
        });

        // nepali date picker
        $('#sample_date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });

        $('#testing_date_np').nepaliDatePicker({
            ndpEnglishInput: 'date_en'
        });
    });
</script>
<script type="text/javascript">
    $('#add-btn').click(function(){
        $('input').val('');
        $('.error-div').hide();
        $('option').attr('selected', false);
        $('#myModalLabel').html('Add Gypsum Record');
        $('#add_gypsum_records').modal();
    })
</script>
<script type="text/javascript">
    $('#form-submit').click(function(){
        var sample_date_np = $('#sample_date_np').val();
        var testing_date_np = $('#testing_date_np').val();
        var company_id = $('#company_id').val();
        var supplier_id = $('#supplier_id').val();
        var moisture = $('#moisture').val();
        var ir = $('#ir').val();
        var so = $('#so').val();
        var purity = $('#purity').val();
        var i = 0;

        if(sample_date_np == ''){
            i = 1;
            $('.error_date_np').show();
        }else{
            $('.error_date_np').hide();
        }
        if(testing_date_np == ''){
            i = 1;
            $('.error_date_np').show();
        }else{
            $('.error_date_np').hide();
        }

        if(company_id == ''){
            i = 1;
            $('#error_company_id').show();
        }else{
            $('#error_company_id').hide();
        }
        if(supplier_id == ''){
            i = 1;
            $('#error_supplier_id').show();
        }else{
            $('#error_supplier_id').hide();
        }
        if(moisture == ''){
            i = 1;
            $('#error_moisture').show();
        }else{
            $('#error_moisture').hide();
        }
        if(ir == ''){
            i = 1;
            $('#error_ir').show();
        }else{
            $('#error_ir').hide();
        }
        if(so == ''){
            i = 1;
            $('#error_so').show();
        }else{
            $('#error_so').hide();
        }
        if(purity == ''){
            i = 1;
            $('#error_purity').show();
        }else{
            $('#error_purity').hide();
        }
        if(i == 0){
            $.post('<?php echo site_url("gypsum_record/save")?>',$('#add-gypsum_record_form').serializeArray(),function(data){
                if(data.success){
                    alert('Success');
                    location.reload();
                }else{
                    alert('Error occured')
                }
            },'json');
        }
    });
</script>
<script type="text/javascript">
    function form_edit(id){
        $('.error-div').hide();
        $.post('<?php echo site_url("gypsum_record/get_gypsum_records")?>',{id:id},function(data){
            $.each(data, function( index, value ) {
                $('#'+index).val(value);
            });
            $('#company_id').val(data.company);
            $('#supplier_id').val(data.source_of_supplier);
            $('#sample_date_np').val(data.date_of_sample);
            $('#testing_date_np').val(data.date_of_testing);
            $('#myModalLabel').html('Edit Gypsum Records');
            $('#add_gypsum_records').modal();
        },'json');
    }
</script>
<script type="text/javascript">
    function delete_data(id){
        if(confirm('Are You Sure You Want To Delete?'))
        {
            $.post('<?php echo site_url("gypsum_record/delete_json")?>',{id:id},function(){
                alert('Data Deleted');
                location.reload();
            },'json');
        }
    }
</script>
